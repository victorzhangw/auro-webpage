DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();
var L10n = ej.base.L10n;
const chartitems = (document.getElementById('dataChart'));
const pointRange = [
  { 'id': 0, 'minpoint': 0, 'maxpoint': 0 },
  { 'id': 1, 'minpoint': 1, 'maxpoint': 100 },
  { 'id': 2, 'minpoint': 101, 'maxpoint': 200 },
  { 'id': 3, 'minpoint': 201, 'maxpoint': 300 },
  { 'id': 4, 'minpoint': 301, 'maxpoint': 400 },
  { 'id': 5, 'minpoint': 401, 'maxpoint': 500 },
  { 'id': 6, 'minpoint': 501, 'maxpoint': 600 },
  { 'id': 7, 'minpoint': 601, 'maxpoint': 700 },
  { 'id': 8, 'minpoint': 701, 'maxpoint': 800 },
  { 'id': 9, 'minpoint': 801, 'maxpoint': 900 },
  { 'id': 10, 'minpoint': 901, 'maxpoint': 1000 },
  { 'id': 11, 'minpoint': 1001, 'maxpoint': 1100 },
  { 'id': 12, 'minpoint': 1101, 'maxpoint': 1200 },
  { 'id': 13, 'minpoint': 1201, 'maxpoint': 1300 },
  { 'id': 14, 'minpoint': 1301, 'maxpoint': 1400 },
  { 'id': 15, 'minpoint': 1401, 'maxpoint': 1500 },
  { 'id': 16, 'minpoint': 1501, 'maxpoint': 1600 },
  { 'id': 17, 'minpoint': 1601, 'maxpoint': 1700 },
  { 'id': 18, 'minpoint': 1701, 'maxpoint': 1800 },
  { 'id': 19, 'minpoint': 1801, 'maxpoint': 1900 },
  { 'id': 20, 'minpoint': 1901, 'maxpoint': 2000 },
  { 'id': 21, 'minpoint': 2001, 'maxpoint': 999999999 },

];

var customerpoints = [


  {
    "RANGERANK": 0,
    "RANGERANKDESC": '0',
    "RANGECOUNT": 53859,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 1,
    "RANGERANKDESC": '1-100',
    "RANGECOUNT": 16421,
    "GRADESUM": 1806310,
    "GRADEAVG": 110
  },
  {
    "RANGERANK": 2,
    "RANGERANKDESC": "101-200",
    "RANGECOUNT": 2636,
    "GRADESUM": 579920,
    "GRADEAVG": 220
  },
  {
    "RANGERANK": 3,
    "RANGERANKDESC": "201-300",
    "RANGECOUNT": 1272,
    "GRADESUM": 419760,
    "GRADEAVG": 330
  },
  {
    "RANGERANK": 4,
    "RANGERANKDESC": "301-400",
    "RANGECOUNT": 924,
    "GRADESUM": 406560,
    "GRADEAVG": 440
  },
  {
    "RANGERANK": 5,
    "RANGERANKDESC": "401-500",
    "RANGECOUNT": 1601,
    "GRADESUM": 880550,
    "GRADEAVG": 550
  },
  {
    "RANGERANK": 6,
    "RANGERANKDESC": "501-600",
    "RANGECOUNT": 1804,
    "GRADESUM": 1190640,
    "GRADEAVG": 660
  },
  {
    "RANGERANK": 7,
    "RANGERANKDESC": "601-700",
    "RANGECOUNT": 897,
    "GRADESUM": 690690,
    "GRADEAVG": 770
  },
  {
    "RANGERANK": 8,
    "RANGERANKDESC": "701-800",
    "RANGECOUNT": 464,
    "GRADESUM": 408320,
    "GRADEAVG": 880
  },
  {
    "RANGERANK": 9,
    "RANGERANKDESC": "801-900",
    "RANGECOUNT": 355,
    "GRADESUM": 351450,
    "GRADEAVG": 990
  },
  {
    "RANGERANK": 10,
    "RANGERANKDESC": "901-1000",
    "RANGECOUNT": 801,
    "GRADESUM": 881100,
    "GRADEAVG": 1100
  },
  {
    "RANGERANK": 11,
    "RANGERANKDESC": "1001-1100",
    "RANGECOUNT": 621,
    "GRADESUM": 751410,
    "GRADEAVG": 1210
  },
  {
    "RANGERANK": 12,
    "RANGERANKDESC": "1101-1200",
    "RANGECOUNT": 99792,
    "GRADESUM": 131725440,
    "GRADEAVG": 1320
  },
  {
    "RANGERANK": 13,
    "RANGERANKDESC": "1201-1300",
    "RANGECOUNT": 214,
    "GRADESUM": 306020,
    "GRADEAVG": 1430
  },
  {
    "RANGERANK": 14,
    "RANGERANKDESC": "1301-1400",
    "RANGECOUNT": 171,
    "GRADESUM": 263340,
    "GRADEAVG": 1540
  },
  {
    "RANGERANK": 15,
    "RANGERANKDESC": "1401-1500",
    "RANGECOUNT": 347,
    "GRADESUM": 572550,
    "GRADEAVG": 1650
  },
  {
    "RANGERANK": 16,
    "RANGERANKDESC": "1501-1600",
    "RANGECOUNT": 242,
    "GRADESUM": 425920,
    "GRADEAVG": 1760
  },
  {
    "RANGERANK": 17,
    "RANGERANKDESC": "1601-1700",
    "RANGECOUNT": 127,
    "GRADESUM": 237490,
    "GRADEAVG": 1870
  },
  {
    "RANGERANK": 18,
    "RANGERANKDESC": "1701-1800",
    "RANGECOUNT": 142,
    "GRADESUM": 281160,
    "GRADEAVG": 1980
  },
  {
    "RANGERANK": 19,
    "RANGERANKDESC": "1801-1900",
    "RANGECOUNT": 113,
    "GRADESUM": 236170,
    "GRADEAVG": 2090
  },
  {
    "RANGERANK": 20,
    "RANGERANKDESC": "1901-2000",
    "RANGECOUNT": 230,
    "GRADESUM": 506000,
    "GRADEAVG": 2200
  },
  {
    "RANGERANK": 21,
    "RANGERANKDESC": "2001-",
    "RANGECOUNT": 1788,
    "GRADESUM": 4130280,
    "GRADEAVG": 2310
  }

];
var customerpointsAry = [[
  [53859, 0, "0"],
  [16421, 1806310, "1-100"],
  [2636, 579920, "101-200"],
  [1272, 419760, "201-300"],
  [924, 406560, "301-400"],
  [1601, 880550, "401-500"],
  [1804, 1190640, "501-600"],
  [897, 690690, "601-700"],
  [464, 408320, "701-800"],
  [355, 351450, "801-900"],
  [801, 881100, "901-1000"],
  [621, 751410, "1001-1100"],
  [99792, 131725440, "1101-1200"],
  [214, 306020, "1201-1300"],
  [171, 263340, "1301-1400"],
  [347, 572550, "1401-1500"],
  [242, 425920, "1501-1600"],
  [127, 237490, "1601-1700"],
  [142, 281160, "1701-1800"],
  [113, 236170, "1801-1900"],
  [230, 506000, "1901-2000"],
  [1788, 4130280, "2001-"]

]];
var weekcustomerpoints = [
  {
    "WEEKRANGE": "第一週",
    "WEEKRANGEDESC": "2021-09-13 ~ 2021-09-19",
    "WEEKRANGECOUNT": 2358,
    "GRADESUM": 3050,
    "GRADEAVG": 1
  },
  {
    "WEEKRANGE": "第二週",
    "WEEKRANGEDESC": "2021-09-20 ~ 2021-09-26",
    "WEEKRANGECOUNT": 15505,
    "GRADESUM": 17217,
    "GRADEAVG": 1
  },
  {
    "WEEKRANGE": "第三週",
    "WEEKRANGEDESC": "2021-09-27 ~ 2021-10-03",
    "WEEKRANGECOUNT": 16898,
    "GRADESUM": 21562,
    "GRADEAVG": 1
  },
  {
    "WEEKRANGE": "第四週",
    "WEEKRANGEDESC": "2021-10-04 ~ 2021-10-10",
    "WEEKRANGECOUNT": 136273,
    "GRADESUM": 1264372,
    "GRADEAVG": 9
  },
  {
    "WEEKRANGE": "本週目前",
    "WEEKRANGEDESC": "2021-10-11 ~ 2021-10-13",
    "WEEKRANGECOUNT": 13787,
    "GRADESUM": 30627,
    "GRADEAVG": 2
  }
]
var weekcustomerpointsAry = [[
  [3050, 2358, '第一週'], [17217, 15505, '第二週'], [21562, 16898, '第三週'], [1264372, 136273, '第四週'], [30627, 13787, '本週目前']
]];
var tlcustomerpoints = [
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "ASUS",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 4
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "HTC",
    "OS": "ANDROID",
    "Location": "台中市",
    "cfPoint": 2
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "HTC",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 6
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "KOOBEE",
    "OS": "ANDROID",
    "Location": "新北市",
    "cfPoint": 1
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "LG",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 3
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "OPPO",
    "OS": "ANDROID",
    "Location": "台中市",
    "cfPoint": 1
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "OPPO",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 15
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "OPPO",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 2
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "SAMSUNG",
    "OS": "ANDROID",
    "Location": "彰化縣",
    "cfPoint": 1
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "SAMSUNG",
    "OS": "ANDROID",
    "Location": "苗栗縣",
    "cfPoint": 1
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "SAMSUNG",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 5
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "SAMSUNG",
    "OS": "ANDROID",
    "Location": "桃園市",
    "cfPoint": 1
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "SONY",
    "OS": "ANDROID",
    "Location": "台中市",
    "cfPoint": 1
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "SONY",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 10
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "SONY",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 1
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "XIAOMI",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 5
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 1
  },
  {
    "BrowserName": "SAFARI",
    "DeviceLogo": "APPLE",
    "OS": "IOS",
    "Location": "台中市",
    "cfPoint": 1
  },
  {
    "BrowserName": "SAFARI",
    "DeviceLogo": "APPLE",
    "OS": "IOS",
    "Location": "台北市",
    "cfPoint": 12
  },
  {
    "BrowserName": "SAFARI",
    "DeviceLogo": "APPLE",
    "OS": "IOS",
    "Location": "桃園市",
    "cfPoint": 1
  },
  {
    "BrowserName": "SAMSUNG BROWSER",
    "DeviceLogo": "SAMSUNG",
    "OS": "ANDROID",
    "Location": "南投縣",
    "cfPoint": 1
  },
  {
    "BrowserName": "SAMSUNG BROWSER",
    "DeviceLogo": "SAMSUNG",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 26
  }
];
const gridColumn1 = [
  {
    field: 'RANGERANK',
    width: 20,
    textAlign: 'left',
    headerText: '級距編號',
    format: 'N'
  },
  {
    field: 'RANGERANKDESC',
    headerText: '點數區間',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'RANGECOUNT',
    headerText: '區間人次',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'GRADESUM',
    headerText: '區間點持有數合計',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'GRADEAVG',
    headerText: '平均持有點數',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  }

];
const gridColumn2 = [
  {
    field: 'WEEKRANGE',
    width: 20,
    textAlign: 'left',
    headerText: '週次',
    format: 'N'
  },
  {
    field: 'WEEKRANGEDESC',
    headerText: '日期區間',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'WEEKRANGECOUNT',
    headerText: '區間人次',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'GRADESUM',
    headerText: '區間點數合計',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'GRADEAVG',
    headerText: '平均持有點數',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  }

];
const gridColumn3 = [
  {
    field: 'BrowserName',
    width: 20,
    textAlign: 'left',
    headerText: '瀏覽器',
    format: 'N'
  },
  {
    field: 'DeviceLogo',
    headerText: '品牌',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'OS',
    headerText: '作業系統',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'Location',
    headerText: '地理位置',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'cfPoint',
    headerText: '擁有點數',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  }

];
var initPointsPage = {

  chart: function () {
    let chartMetaData = [
      { id: 'chart1', size: 'col-md-12' },
      { id: 'chart2', size: 'col-md-12' },
      { id: 'chart3', size: 'col-md-12' },
      { id: 'chart4', size: 'col-md-6' },
      { id: 'chart5', size: 'col-md-6' },




    ];
    let getChartString = ej.base.compile('<div class="col-sm-12 ${size} mb-1 d-flex"> <div id=${id} class="card card_animation flex-fill"> <div class="card-header widget-head"> </div><div class="charts card-body chart-height"> </div> </div>  </div>');
    while (chartitems.firstChild) {
      chartitems.removeChild(chartitems.firstChild);
    }
    chartMetaData.forEach(data => {
      chartitems.appendChild(getChartString(data)[0]);
    });
  },


};
var PointsCharts = {

  chart1: function (data, location, rowMax, header, chartid) {
    let setting = {
      cardHead: header,
      titleText: "",
      value: customerpoints,
      color: "",
      allowResizing: true,
      gridColumn: gridColumn1,
      chartid: 'chart1'
    };
    let chart = chartSetting.gridChart(setting);
    //let chart2 = chartSetting.doubleBarChartVertical(setting2);

    chart.appendTo('#chart1 .card-body');
  },
  chart2: function (data, location, rowMax, header, chartid) {
    let setting = {
      cardHead: header,
      titleText: "",
      value: weekcustomerpoints,
      color: "",
      allowResizing: true,
      gridColumn: gridColumn2,
      chartid: 'chart2'
    };
    let chart = chartSetting.gridChart(setting);
    //let chart2 = chartSetting.doubleBarChartVertical(setting2);

    chart.appendTo('#chart2 .card-body');

  },
  chart3: function (data, location, rowMax, header, chartid) {
    let setting = {
      cardHead: header,
      titleText: "",
      value: tlcustomerpoints,
      color: "",
      allowResizing: true,
      gridColumn: gridColumn3,
      chartid: 'chart3'
    };
    let chart = chartSetting.gridChart(setting);
    //let chart2 = chartSetting.doubleBarChartVertical(setting2);

    chart.appendTo('#chart3 .card-body');

  },
  chart4: function (data, location, rowMax, header, chartid) {
    let params = {
      cardHead: header,
      title: '各級距氣泡圖',
      name: '級距',
      xAxisName: '總人數',
      yAxisName: '總點數',
      data: customerpointsAry

    };
    let chart4 = echarts.init(document.getElementById("chart4").getElementsByClassName('card-body')[0]);

    let option = chartSetting.bubbleCahrt(params);
    console.log(option)
    if (option && typeof option === "object") {
      chart4.setOption(option, true);
      $("#chart4").children(".card-header").text(params.cardHead);
    }
  },
  chart5: function (data, location, rowMax, header, chartid) {
    let params = {
      cardHead: header,
      title: '各周氣泡圖',
      name: '級距',
      xAxisName: '總點數',
      yAxisName: '總人數',
      data: weekcustomerpointsAry

    };
    let chart = echarts.init(document.getElementById("chart5").getElementsByClassName('card-body')[0]);

    let option = chartSetting.bubbleCahrt(params);

    if (option && typeof option === "object") {
      chart.setOption(option, true);
      $("#chart5").children(".card-header").text(params.cardHead);
    }
  }

};

$(document).ready(function () {
  let sdt = moment().subtract(1, 'days').format("YY-MM-DD");
  let edt = moment().subtract(1, 'days').format("YY-MM-DD");
  dp = DramaCore.createDatePicker(sdt, edt);
  initPointsPage.chart();
  PointsCharts.chart1();
  PointsCharts.chart2();
  PointsCharts.chart3();
  PointsCharts.chart4();
  PointsCharts.chart5();

});


// url --> remote url ; type:post or get ; para:parameters

