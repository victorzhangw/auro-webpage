DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();

const chartitems = (document.getElementById('dataChart'));
const prefix = sessionStorage.getItem("sitePrefix");
const appName = sessionStorage.getItem("appName")
const ApiremoteUrl = sessionStorage.getItem("ApiremoteUrl");
var L10n = ej.base.L10n;
var dp, DataObj, Dataobj1;
const gridColumn = [
  {
    field: 'campaign',
    width: 300,
    textAlign: 'left',
    headerText: '訂單',
    format: 'N'
  },
  {
    field: 'strategy',
    headerText: '策略',
    textAlign: 'left',
    width: 170,
    format: 'N'
  },
  {
    field: 'creative',
    headerText: '素材',
    textAlign: 'left',
    width: 170,
    format: 'N'
  },
  {
    field: 'budget',
    headerText: '花費',
    textAlign: 'right',
    width: 90,
    format: 'N'
  },
  {
    field: 'impress',
    headerText: '曝光',
    textAlign: 'right',
    width: 90,
    format: 'N'
  },
  {
    field: 'click',
    headerText: '點擊',
    textAlign: 'right',
    width: 90,
    format: 'N'
  },
  {
    field: 'clickrate',
    headerText: '點擊率',
    textAlign: 'right',
    width: 90,
    format: 'N'
  },
  {
    field: 'ecpm',
    headerText: 'ECPM',
    textAlign: 'right',
    width: 90,
    format: 'N'
  },
  {
    field: 'ecpc',
    headerText: 'ECPC',
    textAlign: 'right',
    width: 90,
    format: 'N'
  }
];

$(document).ready(function () {
  let sdt = moment().subtract(1, 'days').format("YY-MM-DD");
  let edt = moment().subtract(1, 'days').format("YY-MM-DD");
  DramaCore.validateTokenCall(validateSetting());
  let siteKey = prefix + "mf-" + sdt + "-" + edt;
  let dt = moment().subtract(1, 'days').format("YY-MM-DD");
  dp = DramaCore.createDatePicker(dt, dt);
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({
    "StartDateTime": sdt

  });
  initMFPage.chart();
  $(".footer").hide();
  $("#dataChart").hide();
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };
  if (!localStorage.getItem(siteKey)) {

    fetch(ApiremoteUrl + "MultiForceEvent/MFDailyReport", requestOptions)
      .then(response => response.text())
      .then(result => {
        report = JSON.parse(result);
        if (report == null) {
          alert("本日無資料");
          localStorage.setItem(siteKey, "0");
        } else {
          localStorage.setItem(siteKey, result);
          $(".footer").show();
          $("#dataChart").show();
          //initMFPage.chart();
          DramaCore.renderMFChart(siteKey);

        }

        //spinner.setAttribute('hidden', '');

        $("body").removeClass("loading");

      })
      .catch(error => console.log('error', error));
  } else {

    let key = JSON.parse(localStorage.getItem(siteKey));

    if (key.report == null) {
      //alert("本日無資料");
      $(".footer").hide();
      $("#dataChart").hide();
    } else {
      $(".footer").show();
      $("#dataChart").show();
      //initMFPage.chart();
      console.log(siteKey);
      DramaCore.renderMFChart(siteKey);
    }

  }


  dp.addEventListener("change", function () {
    let dayRange = this.getSelectedRange();

    if (dayRange.daySpan > 0) {
      let _sd = moment(this.startDate).format("YY-MM-DD");
      let _ed = moment(this.endDate).format("YY-MM-DD");
      let _parsesd = moment(this.startDate).format("YYYY-MM-DD");
      let _parseed = moment(this.endDate).format("YYYY-MM-DD");

      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      var raw = JSON.stringify({
        "StartDateTime": _sd,
        "EndDateTime": _ed

      });

      var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
      };
      if (_sd && _ed) {

        if (_sd == _ed) {
          fetch(ApiremoteUrl + "MultiForceEvent/MFDailyReport", requestOptions)
            .then(response => response.text())
            .then(result => {
              report = JSON.parse(result);
              if (report == null) {
                alert("設定日期無資料")
                localStorage.setItem(siteKey, "0");
              } else {
                localStorage.setItem(siteKey, result);
                // initMFPage.chart();
                $(".footer").show();
                $("#dataChart").show();
                DramaCore.renderMFChart(siteKey);
              }
              $("body").removeClass("loading");

            })
            .catch(error => console.log('error', error));
        }

        if (moment(_parsesd, "YYYY-MM-DD").isAfter(_parseed, "YYYY-MM-DD")) {
          alert("起始日期要在結束日期之前!");
          //返回false
          return false;
        }
        if (moment(_parseed, "YYYY-MM-DD").isAfter(_parsesd, "YYYY-MM-DD")) {

          fetch(ApiremoteUrl + "MultiForceEvent/MFMonthlyReport", requestOptions)
            .then(response => response.text())
            .then(result => {
              report = JSON.parse(result);
              if (report == null) {

                alert("設定日期無資料")
              } else {
                siteKey = prefix + "mf-" + _parsesd + "-" + _parseed;
                localStorage.setItem(siteKey, result);
                //  initMFPage.chart();
                $(".footer").show();
                $("#dataChart").show();
                DramaCore.renderMFChart(siteKey);
              }
              $("body").removeClass("loading");

            })
            .catch(error => console.log('error', error));




        }
      }
    }
  });

});


// url --> remote url ; type:post or get ; para:parameters
var initMFPage = {
  widget: function () {



  },
  chart: function () {
    let chartMetaData = [
      { id: 'chart1', size: 'col-md-12' },
      { id: 'chart2', size: 'col-md-12' },
      { id: 'chart3', size: 'col-md-12' },
      { id: 'chart4', size: 'col-md-6' },
      { id: 'chart5', size: 'col-md-6' },
      { id: 'chart6', size: 'col-md-6' },
      { id: 'chart7', size: 'col-md-6' }


    ];
    let getChartString = ej.base.compile('<div class="col-sm-12 ${size} mb-1 d-flex"> <div id=${id} class="card card_animation flex-fill"> <div class="card-header widget-head"> </div><div class="charts card-body chart-height"> </div> </div>  </div>');
    while (chartitems.firstChild) {
      chartitems.removeChild(chartitems.firstChild);
    }
    chartMetaData.forEach(data => {
      chartitems.appendChild(getChartString(data)[0]);
    });
  },


};


// 客製化表格欄位值

var MFCharts = {

  chart1: function (data, location, rowMax, header, chartid) {
    let rst = data.report,
      cates = [],
      list = [],
      metric1 = [],
      metric2 = [],
      metric3 = [],
      metric4 = [],
      metric5 = [];

    var result = [];  // 存最終資料結果
    var result2 = [];  // 存最終資料結果

    // 資料按照 campaign進行歸類
    var campaignContainer = {}; // 針對鍵name進行歸類的容器
    rst.forEach(item => {
      campaignContainer[item.campaign] = campaignContainer[item.campaign] || [];
      campaignContainer[item.campaign].push(item);
    });

    var campaignName = Object.keys(campaignContainer); // 獲取活動種類
    campaignName.forEach(nameItem => {
      let budget = 0;
      let click = 0;
      let impress = 0;
      campaignContainer[nameItem].forEach(item => {
        budget += Number(item.budget);
        click += Number(item.click);
        impress += Number(item.impress);

      });
      clickrate = roundToTwo((click / impress) * 100);
      ecpm = roundToTwo((budget / impress) * 1000);
      ecpc = roundToTwo((budget / click));
      result.push({ 'campaign': nameItem, 'budget': roundToTwo(budget), 'impress': impress, 'click': click, 'clickrate': clickrate, 'ecpm': ecpm, 'ecpc': ecpc });
    });
    result2 = groupAndSum(rst, ['campaign', 'data_time'], ['budget', 'click', 'impress']);

    console.log(result2);
    for (let i = 0; i < result2.length; i++) {
      result2[i].clickrate = roundToTwo((result2[i].click / result2[i].impress) * 100);
      result2[i].ecpm = roundToTwo((result2[i].budget / result2[i].impress) * 1000);
      result2[i].ecpc = roundToTwo((result2[i].budget / result2[i].click));
      if (result2[i].impress == 0) {
        result2[i].clickrate = 0;
        result2[i].ecpm = 0;
      }
      if (result2[i].click == 0) {
        result2[i].ecpc = 0;
      }


    }
    // console.log(result2)
    result2.forEach(function (ele) {

      cates.push(ele.data_time);
      metric1.push(ele.click)
      metric2.push(ele.impress)
      metric3.push(ele.budget)
      metric4.push(ele.clickrate)
      metric5.push(ele.ecpc)
    });
    /*
    var result2 = Enumerable.from(rst)
      .groupBy((x) => x.campaign_id + ':' + x.campaign + ':' + x.data_time)
      .select((x) => {
        return {
          campaign_id: x.first().campaign_id,
          campaign: x.first().campaign,
          data_time: x.first().data_time,
          clickrate: x.average((y) => (Number(y.clickrate)) | 0),
          ecpm: x.average((y) => (Number(y.ecpm)) | 0),
          ecpc: x.average((y) => (Number(y.ecpc)) | 0),
          budget: x.sum((y) => Number(y.budget) | 0),
          click: x.sum((y) => Number(y.click) | 0),
          impress: x.sum((y) => Number(y.impress) | 0)
        };
      })
      .toArray();*/

    let setting = {
      cardHead: header,
      titleText: "",
      value: result,
      color: "",
      allowResizing: true,
      gridColumn: gridColumn,
      chartid: chartid
    };
    /*
    let setting2 = {
      cardHead: "柱狀圖",
      titleText: "",
      legend: ["點擊", "曝光"],
      value1: result.click,
      value2: result.impress,
      category: [],
      color: "",
      gridColumn: gridColumn,
      chartid: "chart4"
    };*/

    let setting2 = {
      cardHead: "日期期間點擊數圖",
      titleText: "",

      value: metric1,

      category: cates,
      color: ["#3259B8"],
      gridColumn: gridColumn,
      chartid: "chart4"
    };
    let setting3 = {
      cardHead: "日期期間 點擊/花費 比較",
      titleText: "",

      legend: ["點擊", "花費"],
      value1: metric1,
      value2: metric3,

      category: cates,
      color: ["#4A90E2"],
      gridColumn: gridColumn,
      chartid: "chart5"
    };
    let setting4 = {
      cardHead: "日期期間 ECPM",
      titleText: "",

      value: metric4,

      category: cates,
      color: ["#3259B8"],
      gridColumn: gridColumn,
      chartid: "chart6"
    };
    let setting5 = {
      cardHead: "日期期間 曝光/花費 比較",
      titleText: "",
      legend: ["曝光", "花費"],
      value1: metric2,
      value2: metric3,
      category: cates,
      color: ["#4A90E2"],
      gridColumn: gridColumn,
      chartid: "chart7"
    };
    let chart = chartSetting.gridChart(setting);
    //let chart2 = chartSetting.doubleBarChartVertical(setting2);

    chart.appendTo('#chart1 .card-body');
    let chart2 = echarts.init(document.getElementById("chart4").getElementsByClassName('card-body')[0]);
    let chart3 = echarts.init(document.getElementById("chart5").getElementsByClassName('card-body')[0]);
    let chart4 = echarts.init(document.getElementById("chart6").getElementsByClassName('card-body')[0]);
    let chart5 = echarts.init(document.getElementById("chart7").getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithAvgLine(setting2);
    let option2 = chartSetting.doubleBarChart(setting3);
    let option3 = chartSetting.singleBarChartwithAvgLine(setting4);
    let option4 = chartSetting.doubleBarChart(setting5);


    if (option && typeof option === "object") {

      chart2.setOption(option, true);
      chart3.setOption(option2, true);
      chart4.setOption(option3, true);
      chart5.setOption(option4, true);
      $("#chart4").children(".card-header").text(setting2.cardHead);
      $("#chart5").children(".card-header").text(setting3.cardHead);
      $("#chart6").children(".card-header").text(setting4.cardHead);
      $("#chart7").children(".card-header").text(setting5.cardHead);

    }
    $("#" + chartid).children(".card-header").text(setting.cardHead);

  },
  chart2: function (data, location, rowMax, header, chartid) {


    let rst = data.report,
      chartObj = {},
      list = [],
      metric = [];

    /*
       var result = Enumerable.from(rst)
         .groupBy((x) => x.campaign_id + ':' + x.campaign + ':' + x.strategy)
         .select((x) => {
           return {
             campaign_id: x.first().campaign_id,
             campaign: x.first().campaign,
             clickrate: x.average((y) => (Number(y.clickrate)) | 0),
             ecpm: x.average((y) => (Number(y.ecpm)) | 0),
             ecpc: x.average((y) => (Number(y.ecpc)) | 0),
             budget: x.sum((y) => Number(y.budget) | 0),
             click: x.sum((y) => Number(y.click) | 0),
             impress: x.sum((y) => Number(y.impress) | 0)
           };
         })
         .toArray();*/
    let result;
    result = groupAndSum(rst, ['campaign', 'strategy'], ['budget', 'click', 'impress']);
    console.log('result', result);

    for (let i = 0; i < result.length; i++) {
      result[i].clickrate = roundToTwo((result[i].click / result[i].impress) * 100);
      result[i].ecpm = roundToTwo((result[i].budget / result[i].impress) * 1000);
      result[i].ecpc = roundToTwo((result[i].budget / result[i].click));
      if (result[i].impress == 0) {
        result[i].clickrate = 0;
        result[i].ecpm = 0;
      }
      if (result[i].click == 0) {
        result[i].ecpc = 0;
      }


    }
    let setting = {
      cardHead: header,
      titleText: "",
      value: result,
      color: "",
      gridColumn: gridColumn,
      allowResizing: true,
      chartid: chartid
    };
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart2 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart3: function (data, location, rowMax, header, chartid) {
    let rst = data.report,
      chartObj = {},
      list = [],
      metric = [];

    /*
        var result = Enumerable.from(rst)
          .groupBy((x) => x.campaign_id + ':' + x.campaign + ':' + x.strategy + ':' + x.creative)
          .select((x) => {
            return {
              campaign_id: x.first().campaign_id,
              campaign: x.first().campaign,
              clickrate: x.average((y) => (Number(y.clickrate)) | 0),
              ecpm: x.average((y) => (Number(y.ecpm)) | 0),
              ecpc: x.average((y) => (Number(y.ecpc)) | 0),
              budget: x.sum((y) => Number(y.budget) | 0),
              click: x.sum((y) => Number(y.click) | 0),
              impress: x.sum((y) => Number(y.impress) | 0)
    
            };
          })
          .toArray();
    
    */
    let result;
    result = groupAndSum(rst, ['campaign', 'strategy', 'creative'], ['budget', 'click', 'impress']);


    for (let i = 0; i < result.length; i++) {
      result[i].clickrate = roundToTwo((result[i].click / result[i].impress) * 100);
      result[i].ecpm = roundToTwo((result[i].budget / result[i].impress) * 1000);
      result[i].ecpc = roundToTwo((result[i].budget / result[i].click));
      if (result[i].impress == 0) {
        result[i].clickrate = 0;
        result[i].ecpm = 0;
      }
      if (result[i].click == 0) {
        result[i].ecpc = 0;
      }

    }
    let setting = {
      cardHead: header,
      titleText: "",
      value: result,
      color: "",
      allowResizing: true,
      gridColumn: gridColumn,
      chartid: chartid
    };
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart3 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  }

};

function validateSetting() {
  let bearerToken = sessionStorage.getItem("token");
  let urlTo = AuthremoteUrl + "GetUserName";
  let Setting = {
    async: true,
    type: "GET",
    url: urlTo,
    headers: {
      Authorization: "Bearer" + " " + bearerToken

    }
  };
  return Setting;
}
function loadingScreen(responseTime) {
  var html = '<div id="load"></div>';
  $('#lodinghint').append(html);
  setTimeout(function () {
    $('#load').remove();
  }, responseTime);
}
function groupAndSum(arr, groupKeys, sumKeys) {
  return Object.values(
    arr.reduce((acc, curr) => {
      const group = groupKeys.map(k => curr[k]).join('-');
      acc[group] = acc[group] || Object.fromEntries(groupKeys.map(k => [k, curr[k]]).concat(sumKeys.map(k => [k, 0])));
      sumKeys.forEach(k => acc[group][k] += Number(curr[k]));
      return acc;
    }, {})
  );
}
function roundToTwo(num) {
  return +(Math.round(num + "e+2") + "e-2");
}