/*
const GA_Dimensions1 = ["date","sourceMedium"]; // dimension 空白表示為日期合計
const GA_Metrics1 = ["uniquePageviews", "bounceRate", "users", "newUsers", "sessions", "pageviewsPerSession", "avgSessionDuration", "avgTimeOnPage"];
const GA_Dimensions2 = ["adGroup"];
const GA_Metrics2 = ["impressions", "adClicks", "adCost", "CPM", "CPC", "costPerGoalConversion", "costPerTransaction", "RPC", "ROAS"];
const GA_Dimensions3 = ["sourceMedium"];
const GA_Metrics3 = ["transactions", "transactionRevenue", "transactionsPerSession", "revenuePerTransaction", "goal1Completions", "goal2Completions", "goal3Completions", "goal4Completions"];
const GA_Dimensions4 = ["campaign"];
const GA_Metrics4 = ["transactions", "transactionRevenue", "transactionsPerSession", "revenuePerTransaction", "goal1Completions", "goal2Completions", "goal3Completions", "goal4Completions"];
const GA_Dimensions5 = ["sourceMedium","campaign"];
const GA_Metrics5 = ["impressions", "adClicks", "adCost", "CPM", "CPC", "costPerGoalConversion", "costPerTransaction", "RPC", "ROAS"];
const defaultStartDate = "defaultStartDate";
const defaultEndDate = "defaultEndDate";
const loginID = "loginID";
const pagesets="pagesetting"*/

const GA_Dimensions1 = []; // dimension 空白表示為日期合計
const GA_Metrics1 = ["uniquePageviews", "bounceRate", "users", "newUsers", "sessions", "pageviewsPerSession", "avgSessionDuration", "avgTimeOnPage"];
const GA_Dimensions2 = ["userType"];
const GA_Metrics2 = ["uniquePageviews", "bounceRate", "users", "newUsers", "sessions", "pageviewsPerSession", "avgSessionDuration", "avgTimeOnPage"];
const GA_Dimensions3 = ["sourceMedium"];
const GA_Metrics3 = ["uniquePageviews", "bounceRate", "users", "newUsers", "sessions", "pageviewsPerSession", "avgSessionDuration", "avgTimeOnPage", "exits", "hits"];
const GA_Dimensions4 = ["campaign"];
const GA_Metrics4 = ["uniquePageviews", "bounceRate", "users", "newUsers", "sessions", "pageviewsPerSession", "avgSessionDuration", "avgTimeOnPage", "exits", "hits"];
const GA_Dimensions5 = ["sourceMedium", "campaign"];
const GA_Metrics5 = ["uniquePageviews", "bounceRate", "users", "newUsers", "sessions", "pageviewsPerSession", "avgSessionDuration", "avgTimeOnPage"];
const defaultStartDate = "defaultStartDate";
const defaultEndDate = "defaultEndDate";
const loginID = "loginID";
const pagesets = "pagesetting"


$(document).ready(function () {


});

function defineApp(appName) {
    console.log(siteDefine);
    const app = siteDefine.find(element => element.name == appName);
    let para = {};
    try {
        console.log(app.ViewId);
        sitePrefix = app.prefix;
        let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
        sessionStorage.setItem('appName', appName);
        sessionStorage.setItem('sitePrefix', sitePrefix);
        sessionStorage.setItem('memberKey', app.memberKey);
        sessionStorage.setItem('ViewId', app.ViewId);
        sessionStorage.setItem('GoogleremoteUrl', app.GoogleremoteUrl);
        sessionStorage.setItem('localurl', app.localurl);
        sessionStorage.setItem('AuthremoteUrl', app.AuthremoteUrl);
        sessionStorage.setItem('TealeafremoteUrl', app.TealeafremoteUrl);
        sessionStorage.setItem('LabelremoteUrl', app.LabelremoteUrl);
        sessionStorage.setItem('ApiremoteUrl', app.ApiremoteUrl);
        sessionStorage.setItem(defaultStartDate, dt);
        sessionStorage.setItem(defaultEndDate, dt);
        sessionStorage.setItem('daySpan', "1");

        var key = sitePrefix + "-" + "default-" + dt + "-" + dt + "-" + "1";
        para = {
            "MemberKey": app.memberKey,
            "ViewId": app.ViewId,
            "StartDate": dt,
            "EndDate": dt,
            "LoginId": sessionStorage.getItem("loginID"),
            "ReqDimension1": GA_Dimensions1.join(),
            "ReqMetric1": GA_Metrics1.join(),
            "ReqDimension2": GA_Dimensions2.join(),
            "ReqMetric2": GA_Metrics2.join(),
            "ReqDimension3": GA_Dimensions3.join(),
            "ReqMetric3": GA_Metrics3.join(),
            "ReqDimension4": GA_Dimensions4.join(),
            "ReqMetric4": GA_Metrics4.join(),
            "ReqDimension5": GA_Dimensions5.join(),
            "ReqMetric5": GA_Metrics5.join(),
        };
    } catch (e) {
        console.log(e);
        sitePrefix = "";
    }
    if (sitePrefix != "") {
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

        let urlencoded = new URLSearchParams();
        urlencoded.append("MemberKey", para.MemberKey);
        urlencoded.append("StartDate", para.StartDate);
        urlencoded.append("EndDate", para.EndDate);
        urlencoded.append("LoginId", para.LoginId);
        urlencoded.append("ReqDimension1", para.ReqDimension1);
        urlencoded.append("ReqMetric1", para.ReqMetric1);
        urlencoded.append("ReqDimension2", para.ReqDimension2);
        urlencoded.append("ReqMetric2", para.ReqMetric2);
        urlencoded.append("ReqDimension3", para.ReqDimension3);
        urlencoded.append("ReqMetric3", para.ReqMetric3);
        urlencoded.append("ReqDimension4", para.ReqDimension4);
        urlencoded.append("ReqMetric4", para.ReqMetric4);
        urlencoded.append("ReqDimension5", para.ReqDimension5);
        urlencoded.append("ReqMetric5", para.ReqMetric5);
        urlencoded.append("ViewId", para.ViewId);
        let authRequestOptions = {
            method: 'POST',

        };
        let googleRequestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded,
            redirect: 'follow'
        };

        spinner.removeAttribute('hidden');
        fetch(sessionStorage.getItem("AuthremoteUrl") + "BasicSetting?siteName=" + sitePrefix, authRequestOptions)
            .then(response => response.text())
            .then(function (result) {
                localStorage.removeItem(pagesets);
                localStorage.setItem(pagesets, result);
                // let valueList=Object.values(jsonObj).map(item=>item.CDPOptions);
                // console.log(valueList);
                // return valueList.text();
            })
            .catch(function (e) {
                console.log(e);
                document.location.href = sessionStorage.getItem("localurl") + "login.html";
            });
        fetch((sessionStorage.getItem("GoogleremoteUrl") + "CustomGAReport"), googleRequestOptions)
            .then(response => response.text())
            .then(result => {
                localStorage.setItem(key, result);
                spinner.setAttribute('hidden', '');
                document.location.href = sessionStorage.getItem("localurl") + "default.html";
                //console.log(result);
            })
            .catch(error => console.log('error', error));
    }

}
function testFetch() {
    let authRequestOptions = {
        method: 'POST',

    };
    fetch(sessionStorage.getItem("AuthremoteUrl") + "BasicSetting?siteName=" + "caco", authRequestOptions)
        .then(response => response.text())
        .then(function (result) {
            localStorage.removeItem(pagesets);
            localStorage.setItem(pagesets, result);
            // let valueList=Object.values(jsonObj).map(item=>item.CDPOptions);
            // console.log(valueList);
            // return valueList.text();
        })

        .catch(error => console.log('error', error));

}
