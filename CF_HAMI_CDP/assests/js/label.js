var L10n = ej.base.L10n;
var isLabel = false; // check  label exist; if label is exist,the manual paging not work ! 
const ApiremoteUrl = sessionStorage.getItem("ApiremoteUrl");
DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();
const memberGridpageSize = 25;
window.countries =
    [
        {
            "Id": 1,

            "FullName": "會員輪廓", hasChild: true, expanded: true
        },
        {
            "Id": 2,

            "FullName": "會員行為", hasChild: true, expanded: true
        },
        {
            "Id": 3,

            "FullName": "會員價值", hasChild: true, expanded: true
        },
        {
            "Id": 4,

            "FullName": "興趣", hasChild: true, expanded: true
        },
        {
            "Id": 5,
            "ParentId": 1,
            "FullName": "人口屬性", hasChild: true, expanded: true
        },
        {
            "Id": 6,
            "ParentId": 1,
            "FullName": "會員資料", hasChild: true, expanded: true
        },
        {
            "Id": 7,
            "ParentId": 2,
            "FullName": "進站行為", hasChild: true, expanded: true
        },
        {
            "Id": 8,
            "ParentId": 2,
            "FullName": "瀏覽頁面分類", hasChild: true, expanded: true
        },
        {
            "Id": 9,
            "ParentId": 2,
            "FullName": "購買行為", hasChild: true, expanded: true
        },
        {
            "Id": 10,
            "ParentId": 2,
            "FullName": "關鍵字人群", hasChild: true, expanded: true
        },
        {
            "Id": 11,
            "ParentId": 3,
            "FullName": "價值分群", hasChild: true, expanded: true
        },
        {
            "Id": 12,
            "ParentId": 3,
            "FullName": "單價分群", hasChild: true, expanded: true
        },
        {
            "Id": 13,
            "ParentId": 3,
            "FullName": "回訪分群", hasChild: true, expanded: true
        },
        {
            "Id": 14,
            "ParentId": 4,
            "FullName": "文化展覽類", hasChild: true, expanded: true
        },
        {
            "Id": 15,
            "ParentId": 4,
            "FullName": "民生消費類", hasChild: true, expanded: true
        },
        {
            "Id": 16,
            "ParentId": 4,
            "FullName": "生活休閒類", hasChild: true, expanded: true
        },
        {
            "Id": 17,
            "ParentId": 4,
            "FullName": "交通運輸類", hasChild: true, expanded: true
        },
        {
            "Id": 18,
            "ParentId": 4,
            "FullName": "地區與國家", hasChild: true, expanded: true
        },
        {
            "Id": 19,
            "ParentId": 4,
            "FullName": "投資理財類", hasChild: true, expanded: true
        },
        {
            "Id": 20,
            "ParentId": 4,
            "FullName": "身體保健類", hasChild: true, expanded: true
        },
        {
            "Id": 21,
            "ParentId": 4,
            "FullName": "季節與節慶", hasChild: true, expanded: true
        },
        {
            "Id": 22,
            "ParentId": 4,
            "FullName": "居家生活類", hasChild: true, expanded: true
        },
        {
            "Id": 23,
            "ParentId": 4,
            "FullName": "法律相關類", hasChild: true, expanded: true
        },
        {
            "Id": 24,
            "ParentId": 4,
            "FullName": "建築房屋類", hasChild: true, expanded: true
        },
        {
            "Id": 25,
            "ParentId": 4,
            "FullName": "科技3C類", hasChild: true, expanded: true
        },
        {
            "Id": 26,
            "ParentId": 4,
            "FullName": "科技4C類", hasChild: true, expanded: true
        },
        {
            "Id": 27,
            "ParentId": 4,
            "FullName": "科技5C類", hasChild: true, expanded: true
        },
        {
            "Id": 28,
            "ParentId": 4,
            "FullName": "美食戶外旅遊類", hasChild: true, expanded: true
        },
        {
            "Id": 29,
            "ParentId": 4,
            "FullName": "家庭親子", hasChild: true, expanded: true
        },
        {
            "Id": 30,
            "ParentId": 4,
            "FullName": "消費購物類", hasChild: true, expanded: true
        },
        {
            "Id": 31,
            "ParentId": 4,
            "FullName": "財經資訊類", hasChild: true, expanded: true
        },
        {
            "Id": 32,
            "ParentId": 4,
            "FullName": "動漫電競類", hasChild: true, expanded: true
        },
        {
            "Id": 33,
            "ParentId": 4,
            "FullName": "彩妝保養類", hasChild: true, expanded: true
        },
        {
            "Id": 34,
            "ParentId": 4,
            "FullName": "媒體與時事新聞類", hasChild: true, expanded: true
        },
        {
            "Id": 35,
            "ParentId": 4,
            "FullName": "減重健身類", hasChild: true, expanded: true
        },
        {
            "Id": 36,
            "ParentId": 4,
            "FullName": "飲品食品類", hasChild: true, expanded: true
        },
        {
            "Id": 37,
            "ParentId": 4,
            "FullName": "運動體育類", hasChild: true, expanded: true
        },
        {
            "Id": 38,
            "ParentId": 4,
            "FullName": "精品時尚類", hasChild: true, expanded: true
        },
        {
            "Id": 39,
            "ParentId": 4,
            "FullName": "影視娛樂類", hasChild: true, expanded: true
        },
        {
            "Id": 40,
            "ParentId": 4,
            "FullName": "學習教育類", hasChild: true, expanded: true
        },
        {
            "Id": 41,
            "ParentId": 4,
            "FullName": "嬰幼兒保健類", hasChild: true, expanded: true
        },
        {
            "Id": 42,
            "ParentId": 4,
            "FullName": "寵物生活類", hasChild: true, expanded: true
        },
        {
            "Id": 43,
            "ParentId": 5,
            "FullName": "性別", hasChild: true, expanded: true
        },
        {
            "Id": 44,
            "ParentId": 5,
            "FullName": "年齡", hasChild: true, expanded: true
        },
        {
            "Id": 45,
            "ParentId": 5,
            "FullName": "裝置", hasChild: true, expanded: true
        },
        {
            "Id": 46,
            "ParentId": 5,
            "FullName": "居住地", hasChild: true, expanded: true
        },
        {
            "Id": 47,
            "ParentId": 6,
            "FullName": "身份", hasChild: true, expanded: true
        },
        {
            "Id": 48,
            "ParentId": 6,
            "FullName": "會員點數級距", hasChild: true, expanded: true
        },
        {
            "Id": 49,
            "ParentId": 7,
            "FullName": "進站頻率", hasChild: true, expanded: true
        },
        {
            "Id": 50,
            "ParentId": 7,
            "FullName": "交易頻率", hasChild: true, expanded: true
        },
        {
            "Id": 51,
            "ParentId": 8,
            "FullName": "一週內曾造訪特別企劃分頁"
        },
        {
            "Id": 52,
            "ParentId": 8,
            "FullName": "一週內曾造訪限時下殺分頁"
        },
        {
            "Id": 53,
            "ParentId": 8,
            "FullName": "一週內曾造訪美式賣場分頁"
        },
        {
            "Id": 54,
            "ParentId": 8,
            "FullName": "一週內曾造訪福利品分頁"
        },
        {
            "Id": 55,
            "ParentId": 8,
            "FullName": "一週內曾造訪3M旗艦館分頁"
        },
        {
            "Id": 56,
            "ParentId": 8,
            "FullName": "一週內曾造訪HomeKit專區分頁"
        },
        {
            "Id": 57,
            "ParentId": 8,
            "FullName": "一週內曾造訪美食/生鮮分頁"
        },
        {
            "Id": 58,
            "ParentId": 8,
            "FullName": "一週內曾造訪3C家電分頁"
        },
        {
            "Id": 59,
            "ParentId": 8,
            "FullName": "一週內曾造訪生活百貨分頁"
        },
        {
            "Id": 60,
            "ParentId": 8,
            "FullName": "一週內曾造訪票券/商品卡分頁"
        },
        {
            "Id": 61,
            "ParentId": 9,
            "FullName": "購買商品類別"
        },
        {
            "Id": 62,
            "ParentId": 9,
            "FullName": "加入購物車"
        },
        {
            "Id": 63,
            "ParentId": 9,
            "FullName": "結帳"
        },
        {
            "Id": 64,
            "ParentId": 9,
            "FullName": "付款方式", hasChild: true, expanded: true
        },
        {
            "Id": 65,
            "ParentId": 9,
            "FullName": "運送方式", hasChild: true, expanded: true
        },
        {
            "Id": 66,
            "ParentId": 10,
            "FullName": "搜尋後有點擊任額商品"
        },
        {
            "Id": 67,
            "ParentId": 10,
            "FullName": "搜尋後無點擊任何商品"
        },
        {
            "Id": 68,
            "ParentId": 11,
            "FullName": "高價值"
        },
        {
            "Id": 69,
            "ParentId": 11,
            "FullName": "中價值"
        },
        {
            "Id": 70,
            "ParentId": 11,
            "FullName": "低價值"
        },
        {
            "Id": 71,
            "ParentId": 12,
            "FullName": "高單價"
        },
        {
            "Id": 72,
            "ParentId": 12,
            "FullName": "中單價"
        },
        {
            "Id": 73,
            "ParentId": 12,
            "FullName": "低單價"
        },
        {
            "Id": 74,
            "ParentId": 13,
            "FullName": "常回購：180天 5次"
        },
        {
            "Id": 75,
            "ParentId": 13,
            "FullName": "一般回購率：180天 3-5次"
        },
        {
            "Id": 76,
            "ParentId": 13,
            "FullName": "極少回購：180天 低於3次"
        },
        {
            "Id": 77,
            "ParentId": 13,
            "FullName": "不回購：180天 0次"
        },
        {
            "Id": 78,
            "ParentId": 14,
            "FullName": "文化藝術訊息"
        },
        {
            "Id": 79,
            "ParentId": 14,
            "FullName": "展覽訊息"
        },
        {
            "Id": 80,
            "ParentId": 15,
            "FullName": "外食消費"
        },
        {
            "Id": 81,
            "ParentId": 15,
            "FullName": "家用 - 個人清潔用品"
        },
        {
            "Id": 82,
            "ParentId": 15,
            "FullName": "家用 - 個人養護用品"
        },
        {
            "Id": 83,
            "ParentId": 15,
            "FullName": "家用 - 家庭清潔用品"
        },
        {
            "Id": 84,
            "ParentId": 16,
            "FullName": "DIY手作工藝"
        },
        {
            "Id": 85,
            "ParentId": 16,
            "FullName": "命理資訊"
        },
        {
            "Id": 86,
            "ParentId": 16,
            "FullName": "軍事喜好"
        },
        {
            "Id": 87,
            "ParentId": 16,
            "FullName": "網路資訊"
        },
        {
            "Id": 88,
            "ParentId": 16,
            "FullName": "攝影資訊"
        },
        {
            "Id": 89,
            "ParentId": 17,
            "FullName": "汽車愛好族群"
        },
        {
            "Id": 90,
            "ParentId": 17,
            "FullName": "交通航空訊息"
        },
        {
            "Id": 91,
            "ParentId": 17,
            "FullName": "交通鐵公路訊息"
        },
        {
            "Id": 92,
            "ParentId": 17,
            "FullName": "汽車資訊"
        },
        {
            "Id": 93,
            "ParentId": 17,
            "FullName": "機車資訊"
        },
        {
            "Id": 94,
            "ParentId": 18,
            "FullName": "日本旅遊資訊"
        },
        {
            "Id": 95,
            "ParentId": 18,
            "FullName": "台灣中部旅遊資訊"
        },
        {
            "Id": 96,
            "ParentId": 18,
            "FullName": "台灣北部旅遊資訊"
        },
        {
            "Id": 97,
            "ParentId": 18,
            "FullName": "台灣東部旅遊資訊"
        },
        {
            "Id": 98,
            "ParentId": 18,
            "FullName": "台灣南部旅遊資訊"
        },
        {
            "Id": 99,
            "ParentId": 18,
            "FullName": "台灣離島旅遊資訊"
        },
        {
            "Id": 100,
            "ParentId": 18,
            "FullName": "東南亞旅遊"
        },
        {
            "Id": 101,
            "ParentId": 18,
            "FullName": "歐美旅遊"
        },
        {
            "Id": 102,
            "ParentId": 18,
            "FullName": "韓國旅遊"
        },
        {
            "Id": 103,
            "ParentId": 19,
            "FullName": "理財投資族群"
        },
        {
            "Id": 104,
            "ParentId": 19,
            "FullName": "股票投資"
        },
        {
            "Id": 105,
            "ParentId": 19,
            "FullName": "基金投資"
        },
        {
            "Id": 106,
            "ParentId": 20,
            "FullName": "口腔牙齒治療"
        },
        {
            "Id": 107,
            "ParentId": 20,
            "FullName": "口腔牙齒美容"
        },
        {
            "Id": 108,
            "ParentId": 20,
            "FullName": "健康保健"
        },
        {
            "Id": 109,
            "ParentId": 21,
            "FullName": "冬季活動"
        },
        {
            "Id": 110,
            "ParentId": 21,
            "FullName": "春季活動"
        },
        {
            "Id": 111,
            "ParentId": 21,
            "FullName": "秋季活動"
        },
        {
            "Id": 112,
            "ParentId": 21,
            "FullName": "夏季活動"
        },
        {
            "Id": 113,
            "ParentId": 21,
            "FullName": "節日慶典"
        },
        {
            "Id": 114,
            "ParentId": 22,
            "FullName": "居家生活族群"
        },
        {
            "Id": 115,
            "ParentId": 22,
            "FullName": "收納用品生活"
        },
        {
            "Id": 116,
            "ParentId": 22,
            "FullName": "家電用品生活"
        },
        {
            "Id": 117,
            "ParentId": 22,
            "FullName": "傢俱用品生活"
        },
        {
            "Id": 118,
            "ParentId": 22,
            "FullName": "照明用品生活"
        },
        {
            "Id": 119,
            "ParentId": 22,
            "FullName": "裝飾生活用品"
        },
        {
            "Id": 120,
            "ParentId": 22,
            "FullName": "寢具生活用品"
        },
        {
            "Id": 121,
            "ParentId": 23,
            "FullName": "法律服務"
        },
        {
            "Id": 122,
            "ParentId": 24,
            "FullName": "建築房產族群"
        },
        {
            "Id": 123,
            "ParentId": 24,
            "FullName": "建築修繕"
        },
        {
            "Id": 124,
            "ParentId": 24,
            "FullName": "租屋資訊"
        },
        {
            "Id": 125,
            "ParentId": 25,
            "FullName": "行動裝置發展"
        },
        {
            "Id": 126,
            "ParentId": 25,
            "FullName": "資訊科技資訊"
        },
        {
            "Id": 127,
            "ParentId": 25,
            "FullName": "科技3C族群"
        },
        {
            "Id": 128,
            "ParentId": 28,
            "FullName": "外食打拼族"
        },
        {
            "Id": 129,
            "ParentId": 28,
            "FullName": "素食愛好者"
        },
        {
            "Id": 130,
            "ParentId": 28,
            "FullName": "健康養生族"
        },
        {
            "Id": 131,
            "ParentId": 28,
            "FullName": "戶外休閒 - 釣魚"
        },
        {
            "Id": 132,
            "ParentId": 28,
            "FullName": "日本料理"
        },
        {
            "Id": 133,
            "ParentId": 28,
            "FullName": "台式料理"
        },
        {
            "Id": 134,
            "ParentId": 28,
            "FullName": "美式料理"
        },
        {
            "Id": 135,
            "ParentId": 28,
            "FullName": "旅遊族群"
        },
        {
            "Id": 136,
            "ParentId": 28,
            "FullName": "旅遊資訊 - 國內旅行"
        },
        {
            "Id": 137,
            "ParentId": 28,
            "FullName": "素食料理"
        },
        {
            "Id": 138,
            "ParentId": 28,
            "FullName": "烹飪"
        },
        {
            "Id": 139,
            "ParentId": 28,
            "FullName": "速食"
        },
        {
            "Id": 140,
            "ParentId": 29,
            "FullName": "年輕父母族群"
        },
        {
            "Id": 141,
            "ParentId": 29,
            "FullName": "孝親族群"
        },
        {
            "Id": 142,
            "ParentId": 30,
            "FullName": "購物愛好者"
        },
        {
            "Id": 143,
            "ParentId": 30,
            "FullName": "直銷服務"
        },
        {
            "Id": 144,
            "ParentId": 30,
            "FullName": "電視購物"
        },
        {
            "Id": 145,
            "ParentId": 31,
            "FullName": "網路購物"
        },
        {
            "Id": 146,
            "ParentId": 31,
            "FullName": "金融外匯交易服務"
        },
        {
            "Id": 147,
            "ParentId": 31,
            "FullName": "金融信用服務"
        },
        {
            "Id": 148,
            "ParentId": 31,
            "FullName": "金融貸款服務"
        },
        {
            "Id": 149,
            "ParentId": 31,
            "FullName": "金融開戶服務"
        },
        {
            "Id": 150,
            "ParentId": 31,
            "FullName": "商業經營"
        },
        {
            "Id": 151,
            "ParentId": 32,
            "FullName": "遊戲玩家族群"
        },
        {
            "Id": 152,
            "ParentId": 32,
            "FullName": "手機遊戲"
        },
        {
            "Id": 153,
            "ParentId": 32,
            "FullName": "家用主機"
        },
        {
            "Id": 154,
            "ParentId": 32,
            "FullName": "動漫遊戲"
        },
        {
            "Id": 155,
            "ParentId": 32,
            "FullName": "電玩遊戲"
        },
        {
            "Id": 156,
            "ParentId": 33,
            "FullName": "時尚美妝達人"
        },
        {
            "Id": 157,
            "ParentId": 33,
            "FullName": "開架美容保養"
        },
        {
            "Id": 158,
            "ParentId": 34,
            "FullName": "女性媒體"
        },
        {
            "Id": 159,
            "ParentId": 34,
            "FullName": "地方新聞"
        },
        {
            "Id": 160,
            "ParentId": 34,
            "FullName": "男性媒體"
        },
        {
            "Id": 161,
            "ParentId": 34,
            "FullName": "政治資訊"
        },
        {
            "Id": 162,
            "ParentId": 34,
            "FullName": "商業及經濟新聞"
        },
        {
            "Id": 163,
            "ParentId": 34,
            "FullName": "國際新聞"
        },
        {
            "Id": 164,
            "ParentId": 34,
            "FullName": "影視新聞"
        },
        {
            "Id": 165,
            "ParentId": 35,
            "FullName": "健康強化"
        },
        {
            "Id": 166,
            "ParentId": 35,
            "FullName": "運動習慣"
        },
        {
            "Id": 167,
            "ParentId": 36,
            "FullName": "食品消費"
        },
        {
            "Id": 168,
            "ParentId": 36,
            "FullName": "菸品消費"
        },
        {
            "Id": 169,
            "ParentId": 36,
            "FullName": "飲品消費 (包裝茶飲)"
        },
        {
            "Id": 170,
            "ParentId": 37,
            "FullName": "水上運動相關知識、種類"
        },
        {
            "Id": 171,
            "ParentId": 37,
            "FullName": "自行車相關知識、賽事"
        },
        {
            "Id": 172,
            "ParentId": 37,
            "FullName": "足球相關知識、賽事"
        },
        {
            "Id": 173,
            "ParentId": 37,
            "FullName": "格鬥與摔角"
        },
        {
            "Id": 174,
            "ParentId": 37,
            "FullName": "高爾夫球相關知識、賽事"
        },
        {
            "Id": 175,
            "ParentId": 37,
            "FullName": "棒球相關知識、賽事"
        },
        {
            "Id": 176,
            "ParentId": 37,
            "FullName": "游泳相關知識、賽事"
        },
        {
            "Id": 177,
            "ParentId": 37,
            "FullName": "跑步相關知識、賽事"
        },
        {
            "Id": 178,
            "ParentId": 37,
            "FullName": "網球相關知識、賽事"
        },
        {
            "Id": 179,
            "ParentId": 37,
            "FullName": "賽車相關知識、賽事"
        },
        {
            "Id": 180,
            "ParentId": 37,
            "FullName": "籃球相關知識、賽事"
        },
        {
            "Id": 181,
            "ParentId": 38,
            "FullName": "時尚精品"
        },
        {
            "Id": 182,
            "ParentId": 38,
            "FullName": "時尚潮流"
        },
        {
            "Id": 183,
            "ParentId": 39,
            "FullName": "日本音樂者"
        },
        {
            "Id": 184,
            "ParentId": 39,
            "FullName": "日本電視節目"
        },
        {
            "Id": 185,
            "ParentId": 39,
            "FullName": "日本潮流(哈日)"
        },
        {
            "Id": 186,
            "ParentId": 39,
            "FullName": "古典樂者"
        },
        {
            "Id": 187,
            "ParentId": 39,
            "FullName": "搖滾樂者"
        },
        {
            "Id": 188,
            "ParentId": 39,
            "FullName": "電影者"
        },
        {
            "Id": 189,
            "ParentId": 39,
            "FullName": "影視娛樂訊息"
        },
        {
            "Id": 190,
            "ParentId": 39,
            "FullName": "歐美電視節目"
        },
        {
            "Id": 191,
            "ParentId": 39,
            "FullName": "韓國音樂"
        },
        {
            "Id": 192,
            "ParentId": 39,
            "FullName": "韓國電視節目"
        },
        {
            "Id": 193,
            "ParentId": 39,
            "FullName": "韓國潮流(哈韓)"
        },
        {
            "Id": 194,
            "ParentId": 40,
            "FullName": "教育體制"
        },
        {
            "Id": 195,
            "ParentId": 41,
            "FullName": "母嬰幼兒資訊族群"
        },
        {
            "Id": 196,
            "ParentId": 41,
            "FullName": "嬰幼兒護理"
        },
        {
            "Id": 197,
            "ParentId": 42,
            "FullName": "寵物飼主族群", hasChild: true, expanded: true
        },
        {
            "Id": 198,
            "ParentId": 42,
            "FullName": "水族寵物", hasChild: true, expanded: true
        },
        {
            "Id": 199,
            "ParentId": 42,
            "FullName": "狗"
        },
        {
            "Id": 200,
            "ParentId": 42,
            "FullName": "貓"
        },
        {
            "Id": 201,
            "ParentId": 43,
            "FullName": "男"
        },
        {
            "Id": 202,
            "ParentId": 43,
            "FullName": "女"
        },
        {
            "Id": 203,
            "ParentId": 44,
            "FullName": "0~15"
        },
        {
            "Id": 204,
            "ParentId": 44,
            "FullName": "15~24"
        },
        {
            "Id": 205,
            "ParentId": 44,
            "FullName": "25~34"
        },
        {
            "Id": 206,
            "ParentId": 44,
            "FullName": "35~44"
        },
        {
            "Id": 207,
            "ParentId": 44,
            "FullName": "45~54"
        },
        {
            "Id": 208,
            "ParentId": 44,
            "FullName": "55以上"
        },
        {
            "Id": 209,
            "ParentId": 45,
            "FullName": "PC"
        },
        {
            "Id": 210,
            "ParentId": 45,
            "FullName": "Mobile"
        },
        {
            "Id": 211,
            "ParentId": 45,
            "FullName": "Tablet"
        },
        {
            "Id": 212,
            "ParentId": 45,
            "FullName": "其他"
        },
        {
            "Id": 213,
            "ParentId": 45,
            "FullName": "未知"
        },
        {
            "Id": 214,
            "ParentId": 46,
            "FullName": "台灣地區"
        },
        {
            "Id": 215,
            "ParentId": 46,
            "FullName": "海外地區"
        },
        {
            "Id": 216,
            "ParentId": 47,
            "FullName": "是會員"
        },
        {
            "Id": 217,
            "ParentId": 47,
            "FullName": "不是會員"
        },
        {
            "Id": 218,
            "ParentId": 47,
            "FullName": "有點數會員"
        },
        {
            "Id": 219,
            "ParentId": 47,
            "FullName": "無點數會員"
        },
        {
            "Id": 220,
            "ParentId": 48,
            "FullName": "500點內"
        },
        {
            "Id": 221,
            "ParentId": 48,
            "FullName": "500~1000點"
        },
        {
            "Id": 222,
            "ParentId": 48,
            "FullName": "1001~1500點"
        },
        {
            "Id": 223,
            "ParentId": 48,
            "FullName": "500點～2000點"
        },
        {
            "Id": 224,
            "ParentId": 48,
            "FullName": "2000點以上"
        },
        {
            "Id": 225,
            "ParentId": 49,
            "FullName": "重度使用者：180天 5次以上"
        },
        {
            "Id": 226,
            "ParentId": 49,
            "FullName": "中度使用者：180天 3-5次"
        },
        {
            "Id": 227,
            "ParentId": 49,
            "FullName": "輕度使用者：180天 低於3次"
        },
        {
            "Id": 228,
            "ParentId": 50,
            "FullName": "近三個月有交易過的"
        },
        {
            "Id": 229,
            "ParentId": 50,
            "FullName": "近六個月有交易過的"
        },
        {
            "Id": 230,
            "ParentId": 50,
            "FullName": "近九個月有交易過的"
        },
        {
            "Id": 231,
            "ParentId": 50,
            "FullName": "近一年有交易過的"
        },
        {
            "Id": 232,
            "ParentId": 50,
            "FullName": "一年以上未購買"
        },
        {
            "Id": 233,
            "ParentId": 64,
            "FullName": "貨到付款"
        },
        {
            "Id": 234,
            "ParentId": 64,
            "FullName": "信用卡"
        },
        {
            "Id": 235,
            "ParentId": 64,
            "FullName": "超商代碼"
        },
        {
            "Id": 236,
            "ParentId": 65,
            "FullName": "宅配"
        },
        {
            "Id": 237,
            "ParentId": 65,
            "FullName": "超商取貨"
        },
        {
            "Id": 238,
            "ParentId": 66,
            "FullName": "依照搜尋關鍵字做成"
        },
        {
            "Id": 239,
            "ParentId": 66,
            "FullName": "依照搜尋後回報沒有商品做成"
        },
        {
            "Id": 240,
            "ParentId": 214,
            "FullName": "台北"
        },
        {
            "Id": 241,
            "ParentId": 214,
            "FullName": "新北"
        },
        {
            "Id": 242,
            "ParentId": 214,
            "FullName": "基隆"
        },
        {
            "Id": 243,
            "ParentId": 214,
            "FullName": "桃園"
        },
        {
            "Id": 244,
            "ParentId": 214,
            "FullName": "新竹"
        },
        {
            "Id": 245,
            "ParentId": 214,
            "FullName": "苗栗"
        },
        {
            "Id": 246,
            "ParentId": 214,
            "FullName": "台中"
        },
        {
            "Id": 247,
            "ParentId": 214,
            "FullName": "彰化"
        },
        {
            "Id": 248,
            "ParentId": 214,
            "FullName": "雲林"
        },
        {
            "Id": 249,
            "ParentId": 214,
            "FullName": "南投"
        },
        {
            "Id": 250,
            "ParentId": 214,
            "FullName": "嘉義"
        },
        {
            "Id": 251,
            "ParentId": 214,
            "FullName": "台南"
        },
        {
            "Id": 252,
            "ParentId": 214,
            "FullName": "高雄"
        },
        {
            "Id": 253,
            "ParentId": 214,
            "FullName": "屏東"
        },
        {
            "Id": 254,
            "ParentId": 214,
            "FullName": "宜蘭"
        },
        {
            "Id": 255,
            "ParentId": 214,
            "FullName": "花蓮"
        },
        {
            "Id": 256,
            "ParentId": 214,
            "FullName": "台東"
        },
        {
            "Id": 257,
            "ParentId": 214,
            "FullName": "澎湖"
        },
        {
            "Id": 258,
            "ParentId": 214,
            "FullName": "金門"
        },
        {
            "Id": 259,
            "ParentId": 214,
            "FullName": "連江"
        },
        {
            "Id": 260,
            "ParentId": 214,
            "FullName": "綠島"
        },
        {
            "Id": 261,
            "ParentId": 214,
            "FullName": "蘭嶼"
        },
        {
            "Id": 262,
            "ParentId": 214,
            "FullName": "小琉球"
        },
        {
            "Id": 263,
            "ParentId": 214,
            "FullName": "未知"
        }

    ];

ej.grids.Grid.Inject(ej.grids.Page, ej.grids.Sort, ej.grids.Filter, ej.grids.Resize);
var tagArr = [],
    listViewData, customerData, acrdnObj, grid1;

var dashboard = new ej.layouts.DashboardLayout({
    columns: 16,
    allowDragging: false,
    cellSpacing: [2, 2],
    cellAspectRatio: 100 / 95,
    panels: [{
        'sizeX': 5,
        'sizeY': 1,
        'row': 0,
        'col': 0,
        content: '<br><div id=""><button id="secondSearch">標籤集群</button><button id="clearLabel" >清除選取</button></div>'
    },
    {
        'sizeX': 5,
        'sizeY': 12,
        'row': 1,
        'col': 0,
        header: '<div>標籤選擇器</div>',
        content: '<div id="selector2" class="chart-responsive" ><table id="pg3chkTabel" class=""></table>'
    },
    {
        'sizeX': 11,
        'sizeY': 13,
        'row': 0,
        'col': 5,
        header: '<div>清單列表</div>',
        content: '<div id="grid1" class="chart-responsive" ></div>'
    }



    ]
});
const gridUrl = ApiremoteUrl + "Customer" + "/CustomerTagList";
var labelCate = {
    "Source": "labelCategory",
    "Clause": "",
    "Limit": "0",
    "Offset": "0"
};
var memberData = {
    "Source": "memberlabel",
    "Clause": "",
    "Limit": memberGridpageSize,
    "Offset": "0"
};
var renderComponet = function renderDashBoardComponent() {
    try {
        acrdnObj = new ej.navigations.Accordion({
            expandMode: 'Single',
            width: '93%',
            items: getAccordinItems()

        });

        acrdnObj.appendTo('#pg3chkTabel');
        acrdnObj.addEventListener("expanded", function (e) {
            let isEmpty = document.getElementById(e.item.cssClass).innerHTML == "";
            //console.log(isEmpty);
            let i = e.item.cssClass.match(/\d+/g);
            if (isEmpty) {
                document.getElementById(e.item.cssClass).innerHTML = "<i></i>";
                //console.log(document.getElementById(e.item.cssClass));
                window['chklistObj' + i].appendTo('#' + e.item.cssClass);
            }

        });


        grid1 = new ej.grids.Grid({
            dataSource: customerData,
            //actionComplete: sort,
            allowExcelExport: true,
            toolbar: [{ text: '下載 CSV', tooltipText: 'Download CSV', prefixIcon: "e-csvexport", id: 'CsvExport' }, { text: '下載 Excel', tooltipText: 'Download excel', prefixIcon: "e-excelexport", id: 'ExcelExport' }],
            //toolbar: ["CsvExport"],
            Offline: true,
            columns: [
                {
                    field: 'tags',
                    headerText: '標籤',
                    textAlign: 'left',
                    width: 30,
                    format: 'string',
                    visible: true,
                    valueAccessor: tagsFormatter,
                    disableHtmlEncode: false

                },
                {
                    field: 'cftp',
                    headerText: 'CFTP 編號',
                    textAlign: 'left',
                    width: 40,
                    type: 'string'
                },
                {
                    field: 'cftuid',
                    width: 40,
                    headerText: 'CFTUID 編號',
                    type: 'string'
                },

                {
                    field: 'grade',
                    headerText: '點數',
                    textAlign: 'right',
                    width: 10,
                    format: 'string'
                },




            ],
            rowHeight: 28,
            height: "100%",
            allowPaging: true,
            pageSettings: {
                pageSize: memberGridpageSize
            },
            //allowSorting: true,
            allowTextWrap: true,
            textWrapSettings: {
                wrapMode: 'Content'
            },
            //queryCellInfo: customiseCell
        });

        grid1.dataBound = function () {
            // grid1.autoFitColumns(['cftp']);
            //let pkColumn = grid1.column[0];
            //pkColumn.isPrimaryKey='true';
        };
        //console.log(grid1);
        grid1.appendTo('#grid1');
        grid1.toolbarClick = function (args) {

            // console.log(grid1);
            if (args.item.id === 'CsvExport') {
                grid1.showSpinner();
                // console.log(grid1);
                grid1.columns[5].visible = false;
                let excelExportProperties = {
                    fileName: "資料時間:" + sessionStorage.getItem(defaultStartDate) + ".csv"
                };
                // grid1.excelExport(getExcelExportProperties());
                grid1.csvExport(excelExportProperties);

            }
            if (args.item.id === 'ExcelExport') {

                grid1.columns[5].visible = false;
                grid1.columns[6].visible = true;

                let excelExportProperties = {
                    fileName: "資料時間_" + sessionStorage.getItem(defaultStartDate) + ".xlsx"
                };
                // grid1.excelExport(getExcelExportProperties());
                grid1.excelExport(excelExportProperties);


            }

        };
        grid1.beforeExcelExport = () => {
            //console.log("before");

            grid1.showSpinner();
        };
        grid1.excelExportComplete = () => {
            grid1.hideSpinner();
            grid1.columns[5].visible = true;
            grid1.columns[6].visible = false;
        };
        grid1.csvExportComplete = () => {
            grid1.hideSpinner();
            grid1.columns[5].visible = true;
            grid1.columns[6].visible = false;
        };
        grid1.actionBegin = function (args) {
            console.log(args);
            switch (args.requestType) {
                case "paging":
                    if (!isLabel) {
                        let offset = (args.currentPage - 1) * memberGridpageSize;
                        // console.log(offset);
                        memberData.Offset = offset;
                        //customerData = JSON.parse(isSession(memberData).responseText);
                        // console.log(customerData);
                        grid1.dataSource = (customerData);
                    }

                    //grid1.refresh();
                    break;
            }
        };

    } catch (ex) {

    }


};
//Render initialized Accordion component

dashboard.appendTo('#editLayout');

$(document).ready(function () {
    let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
    let labelKey = "labelKey-" + dt + "-" + dt + "-" + "1";
    dp = DramaCore.createDatePicker(dt, dt);

    if (localStorage.getItem(labelKey)) {
        //listViewData = JSON.parse(localStorage.getItem(labelKey));
        customerData = JSON.parse(isSession(memberData).responseText);
        renderComponet();
    } else {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            redirect: 'follow'
        };
        customerData = JSON.parse(isSession(memberData).responseText);
        //global_Label_key = DramaCore.getRemoteData(gridUrl, "post", labelCate, labelKey, "Label");
        fetch(gridUrl, requestOptions)
            .then(response => response.text())
            .then(result => {
                customerData = JSON.parse(result);
                if (customerData == null) {
                    alert("設定日期無資料")
                    localStorage.setItem(siteKey, "0");
                } else {

                    // initFBPage.chart();
                    $(".footer").show();
                    $("#dataChart").show();
                    renderComponet();
                }
                $("body").removeClass("loading");

            })
            .catch(error => console.log('error', error));


    }



    button1 = new ej.buttons.Button({
        cssClass: 'e-warning'
    }, '#clearLabel');
    button2 = new ej.buttons.Button({
        cssClass: 'e-info'
    }, '#secondSearch');









});



function sort(args) {
    if (args.requestType === 'sorting') {
        for (var i = 0, a = grid.getColumns(); i < a.length; i++) {
            var columns = a[i];
            for (var j = 0, b = grid.sortSettings.columns; j < b.length; j++) {
                var sortcolumns = b[j];
                if (sortcolumns.field === columns.field) {
                    check(sortcolumns.field, true);
                    break;
                }
                else {
                    check(columns.field, false);
                }
            }
        }
    }
}
function isSession(data) {
    return $.ajax({
        type: "POST",
        url: gridUrl,
        data: data,
        dataType: "html",
        async: !1,
        error: function () {
            alert("資料庫維護中");
            document.location.href = localurl + loginUrl;

        }
    });
}
function getExcelExportProperties() {
    let Today = new Date();
    let exportDate = Today.getFullYear() + " 年 " + (Today.getMonth() + 1) + " 月 " + Today.getDate() + " 日";
    return {
        header: {
            headerRows: 3,
            rows: [{
                index: 1,
                cells: [{
                    index: 1,
                    colSpan: 7,
                    value: 'CACO',
                    style: {
                        fontColor: '#C25050',
                        fontSize: 25,
                        hAlign: 'Center',
                        bold: true
                    }
                }]
            },
            {
                index: 3,
                cells: [{
                    index: 1,
                    colSpan: 2,
                    value: "日期",
                    style: {
                        fontColor: '#C67878',
                        fontSize: 16,
                        bold: true
                    }
                },
                {
                    index: 4,
                    value: exportDate,
                    style: {
                        fontColor: '#C67878',
                        bold: true
                    }
                },

                ]
            },

            ]
        },



        fileName: "Member.xlsx"
    };
}

function tagsFormatter(field, data, column) {

    var tmp_Str = data.tags;
    var tmp_Str2 = '';
    var rtn_Str = '';
    try {
        if (data.tags == null) {
            data.tags = ''
        }

        var arr = tmp_Str.split(":");
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] !== "") {
                let tagcolor = "";

                switch (i % 3) {
                    case 0:
                        tagcolor = " tag-infoB";
                        break;
                    case 1:
                        tagcolor = " tag-infoY";
                        break;
                    case 2:
                        tagcolor = " tag-infoG";
                        break;

                }

                tmp_Str2 = '<span class="tagStyles  m-r-5 m-t-5' + ' ' + tagcolor + '">' + arr[i] + '</span>';
            }


            rtn_Str = tmp_Str2 + rtn_Str;
        }

    } catch (e) {
        console.log(e)
    }

    console.log(rtn_Str)
    return rtn_Str;
}

function customiseCell(args) {
    //console.log(args.cell);
    if (args.column.field === 'duration') {
        var tmp_Duration = args.data.duration + '%';
        args.cell.querySelector("#bounce").textContent = tmp_Duration;
        if (parseInt(args.data.duration) <= refIndex1) {
            args.cell.querySelector("#bounce").classList.add("bouncestatus");
        }

    }
}
$("#secondSearch").on("click", function () {
    tagArr = getCheckedValue();

    if (tagArr.length > 0) {
        memberData.Clause = "";
        memberData.Offset = 0;
        memberData.Limit = 0;
        var predicate = new ej.data.Predicate('label', 'contains', tagArr);
        console.log(predicate);
        /*
        if (tagArr.length > 1) {
            for (var i = 1; i < tagArr.length; i++) {
                
                predicate = predicate.or('label', 'contains', tagArr[i]);
            }
        }*/
        memberData.Clause = tagArr.join('|');
        customerData = JSON.parse(isSession(memberData).responseText);
        //var result = new ej.data.DataManager(customerData).executeLocal(new ej.data.Query().where(predicate));
        console.log(customerData[0].result);
        grid1.dataSource = customerData[0].result;
        grid1.allowSorting = true;
        grid1.goToPage(1);
        isLabel = true;
        memberData.Limit = memberGridpageSize;


    } else {
        memberData.Clause = "";
        memberData.Offset = 0;
        memberData.Limit = memberGridpageSize;
        isLabel = false;
        customerData = JSON.parse(isSession(memberData).responseText);
        grid1.dataSource = customerData[0];
        grid1.allowSorting = false;
    }
});
$("#clearLabel").on("click", function () {

    for (let i = 1; i <= listViewData.length; i++) {
        console.log(window['chklistObj' + i]);
        if (window['chklistObj' + i].isRendered === true) {
            window['chklistObj' + i].selectAll(false);
        }


    }
    //var result = new ej.data.DataManager(customerData).executeLocal(new ej.data.Query());
    //grid1.dataSource = result;
    memberData.Clause = "";
    memberData.Offset = 0;
    memberData.Limit = memberGridpageSize;
    isLabel = false;
    customerData = JSON.parse(isSession(memberData).responseText);
    grid1.dataSource = customerData[0];
    grid1.allowSorting = false;

});

function getCheckedValue() {

    let cbxTags = [];

    /*
    $('input:checkbox:checked[name="tag_active_col[]"]').each(function(i) {
         cbxTags[i] = this.value; 
        
    });*/
    for (let i = 1; i <= listViewData.length; i++) {
        if (typeof window['chklistObj' + i].value !== 'undefined' && window['chklistObj' + i].value.length > 0) {
            cbxTags = cbxTags.concat(window['chklistObj' + i].value);
            // console.log(window['chklistObj' + i].value);
        }


    }

    console.log(cbxTags);
    return cbxTags;
}

function getAccordinItems() {
    var checkList = new ej.dropdowns.DropDownTree({
        fields: { dataSource: window.countries, value: 'Id', parentValue: 'ParentId', text: 'FullName', hasChildren: 'hasChild', },
        showCheckBox: true,
        placeholder: '選擇標籤',
        popupHeight: '650px',
        mode: 'Custom',
        treeSettings: { autoCheck: true },
        customTemplate: "${value.length} 標籤 被選中",
    });
    checkList.appendTo('#selector2');
}
function sortArray(arr) {
    var tempArr = [], n;
    for (var i in arr) {
        tempArr[i] = arr[i].match(/([^0-9]+)|([0-9]+)/g);
        for (var j in tempArr[i]) {
            if (!isNaN(n = parseInt(tempArr[i][j]))) {
                tempArr[i][j] = n;
            }
        }
    }
    tempArr.sort(function (x, y) {
        for (var i in x) {
            if (y.length < i || x[i] < y[i]) {
                return -1; // x is longer
            }
            if (x[i] > y[i]) {
                return 1;
            }
        }
        return 0;
    });
    for (var i in tempArr) {
        arr[i] = tempArr[i].join('');
    }
    return arr;
}