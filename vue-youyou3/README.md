# 说明

```
一个vue-cli3.0 初始化项目
```
# 项目运行

```
# 克隆到本地
git@github.com:tonssss/VueFrame.git
```

```
# 进入文件夹
cd vueframe
```

```
# 安装依赖
yarn install
```

```
# 运行
yarn run serve
```

```
# 发布环境
yarn build
```
