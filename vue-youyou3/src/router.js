import Vue from 'vue'
import Router from 'vue-router'
import login from '@/views/login'
import reg from '@/views/reg'
import index from '@/views/index'
import my from '@/views/my'
import order from '@/views/order'
import news from '@/views/news'
import share from '@/views/share'
import notice from '@/views/notice'
import shownotice from '@/views/shownotice'
import paidan from '@/views/paidan'
import huidan from '@/views/huidan'
import rule from '@/views/rule'
import userinfo from '@/views/userinfo'
import changepassword from '@/views/changepassword'
import pwdReset from '@/views/pwdReset'
import ddppz from '@/views/ddppz'
import myinvitees from '@/views/myinvitees'
import exclusive from '@/views/exclusive'
import downpage from '@/views/downpage'
//new
import account from '@/views/account'
import caijie from '@/views/caijie'
import caijieRecon from '@/views/caijieRecon'
import canghuanRecon from '@/views/canghuanRecon'
import shengou from '@/views/shengou'
import yibeitong from '@/views/yibeitong'
import shengouRecon from '@/views/shengouRecon'
import shengoujin from '@/views/shengoujin'
import huoyizhi from '@/views/huoyizhi'
import jifenzhi from '@/views/jifenzhi'
import transDetail from '@/views/transDetail'
import zhishu from '@/views/zhishu'
import huiru from '@/views/huiru'
import pwdChange from '@/views/pwdChange'
import huiruRecon from '@/views/huiruRecon'
import welcome from '@/views/welcome'
import huiruDetail from '@/views/huiruDetail'
import customer from '@/views/customer'

Vue.use(Router)

export default new Router({
	routes: [
		{
			path: '/welcome',
			name: 'welcome',
			component: welcome,
		},
		{
			path: '/index',
			name: 'index',
			component: index,
			meta: {
				auth: true,
			}
		},
		{
			path: '/',
			component: index,
			meta: {
				auth: true,
			}
		},
		{
			path: '/login',
			name: 'login',
			component: login,
		},
		{
			path: '/reg',
			name: 'reg',
			component: reg,
		},
		{
			path: '/my',
			name: 'my',
			component: my,
			meta: {
				auth: true,
			}
		},
		{
			path: '/order',
			name: 'order',
			component: order,
			meta: {
				auth: true,
			}
		},
		{
			path: '/news',
			name: 'news',
			component: news,
			meta: {
				auth: true
			}
		},
		{
			path: '/share',
			name: 'share',
			component: share,
			meta: {
				auth: true
			}
		},
		{
			path: '/notice',
			name: 'notice',
			component: notice,
			meta: {
				auth: true
			}
		},
		{
			path: '/shownotice',
			name: 'shownotice',
			component: shownotice,
			meta: {
				auth: true
			}
		},
		{
			path: '/paidan',
			name: 'paidan',
			component: paidan,
			meta: {
				auth: true
			}
		},
		{
			path: '/huidan',
			name: 'huidan',
			component: huidan,
			meta: {
				auth: true
			}
		},
		{
			path: '/rule',
			name: 'rule',
			component: rule,
			meta: {
				auth: true
			}
		},
		{
			path: '/userinfo',
			name: 'userinfo',
			component: userinfo,
			meta: {
				auth: true
			}
		},
		{
			path: '/changepassword',
			name: 'changepassword',
			component: changepassword,
		},
		{
			path: '/ddppz',
			name: 'ddppz',
			component: ddppz,
			meta: {
				auth: true
			}
		},
		{
			path: '/myinvitees',
			name: 'myinvitees',
			component: myinvitees,
			meta: {
				auth: true
			}
		},
		{
			path: '/exclusive',
			name: 'exclusive',
			component: exclusive,
			meta: {
				auth: true
			}
		},
		{
			path: '/downpage',
			name: 'downpage',
			component: downpage,
		},
		{
			path: '/pwdReset',
			name: 'pwdReset',
			component: pwdReset,
			meta: {
			}
		},
		{
			path: '/account',
			name: 'account',
			component: account,
			meta: {
				auth: true
			}
		},
		{
			path: '/caijie',
			name: 'caijie',
			component: caijie,
			meta: {
				auth: true
			}
		},
		{
			path: '/caijieRecon',
			name: 'caijieRecon',
			component: caijieRecon,
			meta: {
				auth: true
			}
		},
		{
			path: '/canghuanRecon',
			name: 'canghuanRecon',
			component: canghuanRecon,
			meta: {
				auth: true
			}
		},
		{
			path: '/shengou',
			name: 'shengou',
			component: shengou,
			meta: {
				auth: true
			}
		},
		{
			path: '/yibeitong',
			name: 'yibeitong',
			component: yibeitong,
			meta: {
				auth: true
			}
		},
		{
			path: '/shengouRecon',
			name: 'shengouRecon',
			component: shengouRecon,
			meta: {
				auth: true
			}
		},
		{
			path: '/shengoujin',
			name: 'shengoujin',
			component: shengoujin,
			meta: {
				auth: true
			}
		},
		{
			path: '/huoyizhi',
			name: 'huoyizhi',
			component: huoyizhi,
			meta: {
				auth: true
			}
		},
		{
			path: '/jifenzhi',
			name: 'jifenzhi',
			component: jifenzhi,
			meta: {
				auth: true
			}
		},
		{
			path: '/transDetail',
			name: 'transDetail',
			component: transDetail,
			meta: {
				auth: true
			}
		},
		{
			path: '/zhishu',
			name: 'zhishu',
			component: zhishu,
			meta: {
				auth: true,
			}
		},
		{
			path: '/huiru',
			name: 'huiru',
			component: huiru,
			meta: {
				auth: true,
			}
		},
		{
			path: '/pwdChange',
			name: 'pwdChange',
			component: pwdChange,
			meta: {
				auth: true,
			}
		},
		{
			path: '/huiruRecon',
			name: 'huiruRecon',
			component: huiruRecon,
			meta: {
				auth: true,
			}
		},
		{
			path: '/huiruDetail',
			name: 'huiruDetail',
			component: huiruDetail,
			meta: {
				auth: true,
			}
		},
		{
			path: '/customer',
			name: 'customer',
			component: customer,
			meta: {
				auth: true,
			}
		}
	]
})
