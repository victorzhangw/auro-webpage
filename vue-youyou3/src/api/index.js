/** 
 * api接口的统一出口
 */
import article from '@/api/article';
import miniaccount from '@/api/miniaccount';

// 导出接口
export default {
	article,
	miniaccount
}
