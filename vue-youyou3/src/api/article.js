import base from './base'; // 导入接口域名列表
import axios from '@/common/js/http'; // 导入http中创建的axios实例
import qs from 'qs'; // 根据需求是否导入qs模块

const article = {
	// 新闻列表    
	articleList() {
		return axios.get(`${base.news}/list`);
	},
	// 新闻详情,演示    
	articleDetail(params) {
		return axios.get(`${base.news}/item`, {
			params: params
		});
	},
	// post提交
	login(params) {
		return axios.post(`${base.sq}/accesstoken`, qs.stringify(params));
	}
}

export default article;
