import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

let userguid = ''
let usertoken = ''

try {
	if (localStorage.guid) {
		userguid = localStorage.guid
	}
	if (localStorage.token) {
		usertoken = localStorage.token
	}
} catch (e) {

}

export default new Vuex.Store({
	state: {
		guid: userguid,
		token: usertoken,
		network: true,
		test: false, //是否测试环境
		url: 'yibei.urmini.com',
		version: '2.0.4' //版本号
	},
	mutations: {
		changeGuid(state, guid) {
			state.guid = guid
			try {
				localStorage.guid = guid
			} catch (e) {}
		},
		changePhone(state, phone) {
			state.phone = phone
			try {
				localStorage.phone = phone
			} catch (e) {}
		},
		changeToken(state, token) {
			state.token = token
			try {
				localStorage.token = token
			} catch (e) {}
		}
	},
	actions: {

	}
})
