import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Axios from 'axios'
import store from './store'
import '@/common/css/reset.css'
import '@/common/css/base.css'
import '@/common/css/iconfont.css'
import 'amfe-flexible/index.js'
import 'vant/lib/index.css'
import api from './api'
import Vant from 'vant';
import {
	Locale
} from 'vant';
import 'vant/lib/index.css';
import zhHK from 'vant/lib/locale/lang/zh-HK';
import VCharts from 'v-charts'
Vue.prototype.$api = api
Vue.prototype.$axios = Axios

if(store.state.test){
	Axios.defaults.baseURL = '/api'
}else{
	Axios.defaults.baseURL = store.state.url + '/api/'
}

Locale.use('zh-HK', zhHK);
Vue.use(Vant);
Vue.use(VCharts)


router.beforeEach((to, from, next) => {
	if (to.matched.some(m => m.meta.auth)) {
		if (!localStorage.guid) {
			next('/welcome')
		} else {
			next()
		}
	} else {
		next()
	}
})
Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')
