module.exports = {
	publicPath: './', // 默认'/'，部署应用包时的基本 URL
	outputDir: 'dist', // 'dist', 生产环境构建文件的目录
	assetsDir: '', // 相对于outputDir的静态资源(js、css、img、fonts)目录
	lintOnSave: false,
	runtimeCompiler: true, // 是否使用包含运行时编译器的 Vue 构建版本
	productionSourceMap: false, // 生产环境的 source map
	parallel: require('os').cpus().length > 1,
	pwa: {},
	devServer: {
		open: false,
		host: '0.0.0.0',
		port: 8000,
		https: false,
		hotOnly: false,
		proxy: {
			'/api': {
				target: 'http://yibei.urmini.com/api/',
				changeOrigin: true,
				pathRewrite: {
					'^/api': ''
				}
			}
		}
	},
	css: {
		modules: false,
		extract: false,  //生产环境要改为true
		sourceMap: false,
		loaderOptions: {
			sass: {
				// 向全局sass样式传入共享的全局变量
				data: `@import "@/common/css/index.scss";`
			}
		}
	}
};
