/**
 * Created by Victor on 2016/02/25.
 */
var jqueryNoConflict = jQuery;


//begin initial page function
jqueryNoConflict(document).ready(function () {
    retrieveData();
});
// 
$("#query").click(function () {
    postsearchdata();

});


//end main function
function postsearchdata() {
    request = $.ajax({
        url: "R_member.php",
        type: "post",
        data: {
            f0010: $('#f0010').val()

        }

    });
    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR) {
         $('#f0010').val("");
        var points_list = JSON.parse(response);
        var member_list = new Object({
            member: points_list
        });
        //console.log(member_list);
        retrieveData(member_list);
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown) {
        // Log the error to the console
        console.error(
            "The following error occurred: " +
            textStatus, errorThrown
        );
    });
}
// grab data
function retrieveData(datasource1) {
    var dataSource = datasource1
    renderDataVisualsTemplate(dataSource);
    //jqueryNoConflict.getJSON(dataSource, renderDataVisualsTemplate);
};





// render compiled handlebars template
function renderDataVisualsTemplate(data) {
    handlebarsDebugHelper();

    renderHandlebarsTemplate('template/m_table.hbs', '#temtable', data);
};



// render handlebars templates via ajax
function getTemplateAjax(path, callback) {
    var source, template;
    jqueryNoConflict.ajax({
        url: path,
        success: function (data) {
            source = data;
            template = Handlebars.compile(source);
            if (callback) callback(template);
        }
    });
};


// function to compile handlebars template
function renderHandlebarsTemplate(withTemplate, inElement, withData) {
    getTemplateAjax(withTemplate, function (template) {
        jqueryNoConflict(inElement).html(template(withData));
    })
};

// add handlebars debugger
function handlebarsDebugHelper() {
    Handlebars.registerHelper("debug", function (optionalValue) {
        console.log("Current Context");
        console.log("====================");
        console.log(this);
    });
};

