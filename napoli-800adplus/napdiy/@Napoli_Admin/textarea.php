<?php
    $editor_data = stripslashes($_POST[ 'editor1' ]);
   
?>
<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title></title>
    <link rel="shortcut icon" href="">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">
    <style>
        body {
            padding-top: 50px;
        }
        
        .starter-template {
            padding: 40px 15px;
            text-align: center;
        }
    </style>


    <!--[if IE]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <div class="container">
        <div class="row">
            <h2>畫面預覽：</h2>
        </div>
        <div class="row box">
            <div class="col-md-12 ">
                <div class="">
                    <?php
                   
                    echo  $editor_data;
               ?>
                </div>

            </div>
        </div>
        <div class="row">
            <br>
        </div>
        <div class="row">
           <button onclick="closeWin()">取消</button>
           <button onclick="goBack()">再修改</button>
           <br>
          
        </div>
        <div class="row">
            <br>
        </div>
         <div class="row">
            
              <form action="deploy.php" method="post" enctype="multipart/form-data">      
                 <input type="hidden" name="deploy" value='<?php echo $editor_data ?>' />
                <button type="submit" style="width:100px;">發佈</button>
            </form>
        </div>
        
        

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script>
        function closeWin() {
            window.close();
            opener.location.href = "http://pizzadiy.0800076666.com.tw/";
        }
    </script>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
</body>

</html>