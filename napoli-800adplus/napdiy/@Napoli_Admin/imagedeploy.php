<?php

  // Get array of all source files
    $files = scandir("../imguploads");

    // Identify directories
    $source = "../imguploads/";
    $destination = "../pic/";

    // Cycle through all source files
    foreach ( $files as $file ) {
        if (in_array($file, array(".",".."))) continue;

        // If we copied this successfully, mark it for deletion
        if (copy($source.$file, $destination.$file)) {
            $delete[] = $source.$file;
        }
    }
    // Delete all successfully-copied files
    foreach ( $delete as $file ) {
        unlink( $file );
    }

?>