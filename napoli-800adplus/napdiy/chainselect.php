<?php
header("Access-Control-Allow-Origin: *");
require_once('php/PHPMailerAutoload.php');
require_once('php/config.php'); /* Configuration File */

if (!empty($_GET["f3m"])) {    
    $con=mysqli_connect(DB_SERVER, DB_USER, DB_PASS,DB_NAME);
    mysqli_set_charset($con,"utf8");
// Check connection
    if (mysqli_connect_errno()) {
        $data = array('success' => false, "Failed to connect to MySQL: " . mysqli_connect_error());
        echo json_encode($data);
        exit; 
    }  
};
if ("East" == $_GET["f3m"]) {        
    $sql_query = "SELECT depid,depname FROM `napdiydep` WHERE area='東部'";
    $dep_set = mysqli_query($con,$sql_query);
    
    while($row =mysqli_fetch_array($dep_set))
    {
        $response[$row["depid"]] = $row["depname"];
    }   
    mysqli_close($con);
    };

if ("North" == $_GET["f3m"]) {        
    $sql_query = "SELECT depid,depname FROM `napdiydep` WHERE area='北部'";
    $dep_set = mysqli_query($con,$sql_query);
    
    while($row =mysqli_fetch_array($dep_set))
    {
        $response[$row["depid"]] = $row["depname"];    }
   
    mysqli_close($con);
};
if ("Middle" == $_GET["f3m"]){    
    $sql_query = "SELECT depid,depname FROM `napdiydep` WHERE area='中部'";
    $dep_set = mysqli_query($con,$sql_query);
    
    while($row =mysqli_fetch_array($dep_set))
    {
        $response[$row["depid"]] = $row["depname"];
    }
   
    mysqli_close($con);

};
if ("South" == $_GET["f3m"]){    

    $sql_query = "SELECT depid,depname FROM `napdiydep` WHERE area='南部'";
    $dep_set = mysqli_query($con,$sql_query);
    
    while($row =mysqli_fetch_array($dep_set))
    {
        $response[$row["depid"]] = $row["depname"];
    }
   
    mysqli_close($con); 
};
print json_encode($response);
?>