/* global $ */
$(document).ready(function () {

    $(document).bind('PgwModal::Close', function () {
        window.location.reload(true);
    });

    //  Get date

    $("#eventdate1").datepicker({
        minDate: -0
    });
    $("#eventdate2").datepicker({
        minDate: -0
    });
    $("#eventdate3").datepicker({
        minDate: -0
    });

    // Chain Select 

    $("#f3").remoteChained({
        parents: "#f3m",
        url: "http://800adplus.com/napdiy/chainselect.php",
        loading: "--"
    });

    //  Select to chang display
    $('#f4').on('change', function () {
        var persons = $('#f4').val();
        var i = 1;
        $('#f4').closest('#napform').find('#groups').remove();
        $('#btns').css('display', 'block');
        for (i; i <= persons; i++) {
            var j = i + 4;
            $('#btns').before('<span id="groups">' + i + '. ' + '小朋友姓名<input type="text" id="f' + j + '" name="f' + j + '" class="required"> <br>  大人是否一同參加?   <input type="radio" name="f' + j + 'g" value="YES" class="required"> 是  <input type="radio"name="f' + j + 'g" value="NO" > 否<br/><br>  </span>');
        }
    });

    // Only input number
    function ValidateNumber(e, pnumber) {
        if (!/^\d+$/.test(pnumber)) {
            $(e).val(/^\d+/.exec($(e).val()));
        }
        return false;
    }
    $("#napform").submit(function (event) {
        event.preventDefault();
    }).validate({

        submitHandler: function (form) {

            // Variable to hold request
            var request;

            // Abort any pending request
            if (request) {
                request.abort();
            }
            // setup some local variables
            var $form = $("#napform");

            // Let's select and cache all the fields
            var $inputs = $form.find("input, select, button, textarea");

            // Serialize the data in the form
            var serializedData = $form.serialize();
            console.log(serializedData);

            // Disabled form elements will not be serialized.
            $inputs.prop("disabled", true);

            // Fire off the request to /form.php
            request = $.ajax({
                url: "http://800adplus.com/napdiy/Send_Form.php",
                type: "post",
                data: serializedData
            });

            // Callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR) {
                console.log("it worked!");
                $.pgwModal({
                    target: '#modalContent',
                    maxWidth: 500
                });

            });

            // Callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown) {
                // Log the error to the console
                console.error(
                    "The following error occurred: " +
                    textStatus, errorThrown
                );
            });

            // Callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function () {
                // Reenable the inputs
                $inputs.prop("disabled", false);
            });
            return false;
        }
    });



});