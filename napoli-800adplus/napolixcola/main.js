﻿var app = angular.module('TEST_Proj', ['angularValidator']);

app.controller('storeCntrl', function ($scope, $http)
{
    $scope.countries = [];
    $http.get('store.json').success(function (data)
    {
        $scope.countries = data;
    });

});


app.filter('filter1', function ()
{
    return function (fit1)
    {
        return fit1.substring(1);
    };
});

 

app.controller('validCntrl', function ($scope, $http)
{
  
    var store;
    $scope.loader = {
        loading: false,
    };

    $scope.changeStore = function (item)
    {
        $scope.store = item;
        //console.log($scope.store);
    };

   
    
});


function submitTEST1()
{
    for (i = 0 ; i <= 2 - 1 ; i++)
    {
        $('input#entry.206835945').val('11');
        $('input#entry.1474448502').val('22');
        $('input#entry.1529768577').val('33');
        $('input#entry.1263575999').val('44');

        $('select#entry.1240224157').val('55');
        $('select#entry.1858524801').val('66');
        $('select#entry.1979950006').val('77');


        $('form#ss-form').submit();

    }

};


function submitTEST2()
{

    for (i = 1 ; i <= 100  ; i++)
    {
        $.ajax({
            url: 'https://docs.google.com/forms/d/e/1FAIpQLSfNr3lL9By8i-RlbhNhf-mGjw8cvrZa3G71V3njjLCbv6hY-A/formResponse',
            type: 'POST',
            data: {
                'entry.206835945': i,
                'entry.1474448502': makeid(),
                'entry.1529768577': makeid(),
                'entry.1263575999': makeid(),
                'entry.1240224157': '台中市',
                'entry.1858524801': '北屯區',
                'entry.1979950006': '北屯',
            },
            success: function (data)
            {
            }
        });
    }
};

function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
