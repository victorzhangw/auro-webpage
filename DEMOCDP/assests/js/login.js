
const loginID="loginID";
$(document).ready(function(){
    try{
        
        sessionStorage.clear();
        localStorage.clear();
    }
    catch(e){
        console.log(e);
    }
    

    
});
$("#signinBtn").click(function(){
    let _username,_password,settings;
    let urlTo=AuthremoteUrl+"SignIn";
    _username= $("#username").val();
    _password= $("#password").val();
    
    if(_username && _password){
        $("#errlbl").removeClass("error-lbl");
        $("#errlbl").addClass("correct-lbl");
        let loginObj={
            Username:_username,
            Password:_password
        };
        settings={
            async: true,
            type: "POST",
            url: urlTo,
            data:loginObj
        };
        sessionStorage.setItem('loginID',  _username);
        makeAjaxCall(settings);
        console.log(loginObj);
    }else{
        $("#errlbl").removeClass("correct-lbl");
        $("#errlbl").addClass("error-lbl");
    }
});

function makeAjaxCall(settings){
    var jqxhr = $.ajax(settings);
    //this section is executed when the server responds with no error 
    jqxhr.done(function(response){
        let _username =$("#username").val();
        sessionStorage.setItem('token', response);
        
        $("#username").val("");
        $("#password").val("");
        
        console.log(response);
        if (response==='empty'){
            document.location.href=localurl+"login.html";
        }else{
            document.location.href=localurl+"sites.html";
        }
       // document.location.href=localurl+"sites.html";
    });
    //this section is executed when the server responds with error
    jqxhr.fail(function(err){
        $("#errlbl").removeClass("correct-lbl");
        $("#errlbl").addClass("error-lbl");
        $("#username").val("");
        $("#password").val("");
        

    });
    //this section is always executed
    jqxhr.always(function(){
        //here is how to access the response header
        console.log("getting header " + jqxhr.getResponseHeader('testHeader'));
    });
}
function validateAjaxCall(settings){
    var jqxhr = $.ajax(settings);
    jqxhr.done(function(response){
       console.log(response);
       let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
       let key = "default-" + dt + "-" + dt + "-" + "1";
       let timeoutID = window.setInterval(function(){
      
        if (localStorage.getItem(key)){
            console.log(key);
            window.clearInterval(timeoutID);
            document.location.href=localurl+"sites.html";
        }
       },1000);
    });
    //this section is executed when the server responds with error
    jqxhr.fail(function(err){
     
        

    });
    //this section is always executed
    jqxhr.always(function(){
        if(jqxhr.status=="401"){
            document.location.href = localurl+loginUrl;
           
        }
    });
}

function getRemoteData(url, type, para,key) {
    let settings = {
        "async": true,
        "crossDomain": true,
        "url": url,
        "method": type,
        "headers": {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        "data": para
      };
  
      $.ajax(settings).done(function (response) {
        let obj = JSON.parse(response);
        if (Object.keys(obj).length > 0) {
          localStorage.setItem(key, response);
            console.log(obj);
        }
      });

  
}