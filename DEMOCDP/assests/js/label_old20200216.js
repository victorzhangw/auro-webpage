
var L10n = ej.base.L10n;
DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();
var dashboard = new ej.layouts.DashboardLayout({
    columns: 16,
    allowDragging:false,
    cellSpacing: [2, 2],
    cellAspectRatio: 100 / 80,
    panels: [
        {
            'sizeX': 4, 'sizeY': 4, 'row': 0, 'col': 0,
            header: '<div>分類選擇器</div>', content: '#selector1'
        },
        {
            'sizeX': 12, 'sizeY': 13, 'row': 0, 'col': 5,
            header: '<div>清單列表</div>', content: '<div id="grid1" class="chart-responsive" ></div>'
        },
        {
            'sizeX': 4, 'sizeY': 9, 'row': 6, 'col': 0,
            header: '<div>標籤選擇器</div>', content: '<div id="selector2" class="chart-responsive" ><table id="pg3chkTabel" class=""></table><hr><div id=""><button id="secondSearch">標籤集群</button></div>'
        },
        
       
    ]
});

dashboard.appendTo('#editLayout');
$(document).ready(function () {
    
    DramaCore.validateTokenCall(validateSetting());
    
    let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
    dp = DramaCore.createDatePicker(dt, dt);
    // age selector
    var listObj = new ej.dropdowns.MultiSelect({
        // set the placeholder to MultiSelect input element
        placeholder: 'Select Area ',
        // set the type of mode for how to visualized the selected items in input element.
        mode: 'Box'
    });
    
    checkbox = new ej.buttons.CheckBox({ label: '一般' , cssClass: 'e-small' });
    checkbox.appendTo('#checkbox1');
    checkbox = new ej.buttons.CheckBox({ label: 'VIP' , cssClass: 'e-small' });
    checkbox.appendTo('#checkbox2');
    checkbox = new ej.buttons.CheckBox({ label: 'SVIP' , cssClass: 'e-small' });
    checkbox.appendTo('#checkbox3');
    listObj.appendTo('#area');

    button1 = new ej.buttons.Button({ cssClass: 'e-info'}, '#primarySearch');
    button2 = new ej.buttons.Button({ cssClass: 'e-info'}, '#secondSearch');
    //var template='<div>${name}</div><div>${value}</div>';
    
    var template = '<tr><td class="col1"><input type="checkbox" class="check" id="square-checkbox-1" data-checkbox="icheckbox_square-blue" name="tag_active_col[]" value="${name}"></input><span class="p-l-10">${name}</span></td><td class="col2"><select id="conditionbox"><option></option><option>以及</option> <option>或者</option></select></td><td class="col3">${value}</td></tr>';
    
    var table = (document.getElementById('pg3chkTabel'));
    var compiledFunction = ej.base.compile(template);
    
    tagData.forEach(function(item, index, array){
        table.appendChild((compiledFunction(item)[0]));
      });
      
      grid1.appendTo('#grid1');
      //var predicate = new ej.data.Predicate('tags', 'contains', 'COUPON');
      //  predicate = predicate.or('tags', 'contains', '跨平台');
      //  predicate = predicate.or('tags', 'contains', 'T恤');
      //  var result = new ej.data.DataManager(customerData).executeLocal(new ej.data.Query().where(predicate));
      //  console.log(result);


 
});
function validateSetting(){
    let bearerToken =sessionStorage.getItem("token");
    let urlTo= AuthremoteUrl+"GetUserName";
    let Setting={
        async: true,
        type: "GET",
        url: urlTo,
        headers: {
            Authorization: "Bearer" +" "+ bearerToken
           
          }
    };
    return Setting;
}
ej.grids.Grid.Inject(ej.grids.Page, ej.grids.Sort, ej.grids.Filter,ej.grids.Resize);
var grid1 = new ej.grids.Grid({
    dataSource: customerData,
    Offline: true, 
    columns: [
                { field: 'memberid', headerText: '會員編號', textAlign: 'center', width: 200, type: 'string' },
                { field: 'membername', width: 80, headerText: '姓名', type: 'string' },
               
                { field: '', headerText: '性別', textAlign: 'left', width: 20, format: 'string' },
                { field: 'memberemail', headerText: '電子郵件', textAlign: 'left', width: 200, format: 'string' },
                { field: 'shippingaddress', headerText: '遞送地址', textAlign: 'left', width: 210, format: 'string' },
                { field: 'contactaddress', headerText: '通訊地址', textAlign: 'left', width: 210, format: 'string' },
                { field: 'label', headerText: '標籤', textAlign: 'left', width: 160, format: 'string',valueAccessor: tagsFormatter,disableHtmlEncode:false},
               
                
    ],
    rowHeight: 20,
    //enableVirtualization: true,
    //enableColumnVirtualization: true,
    height: 750,
    allowPaging: true,
    pageSettings: { pageSize: 30 },
    allowSorting: true,
    allowTextWrap:true,
    textWrapSettings:{wrapMode:'Content'},
    //queryCellInfo: customiseCell
});

grid1.dataBound=function(){
    grid1.autoFitColumns(['memberid','membername']);
    //let pkColumn = grid1.column[0];
    //pkColumn.isPrimaryKey='true';
};
function tagsFormatter(field, data, column){
    var tmp_Str = data.label;
    var tmp_Str2='';
    var rtn_Str='';
    var arr = tmp_Str.split(":");  
    for(var i=0;i<arr.length;i++){
        tmp_Str2='<span class="tagStyles tag-info  m-r-5 m-t-5">'+arr[i]+'</span>';
        
        rtn_Str=tmp_Str2+rtn_Str;
    }
    
    return rtn_Str;
}
function customiseCell(args){
    //console.log(args.cell);
    if(args.column.field==='duration'){
        var tmp_Duration =args.data.duration+'%';
        args.cell.querySelector("#bounce").textContent=tmp_Duration;
        if(parseInt(args.data.duration)<=refIndex1){
            args.cell.querySelector("#bounce").classList.add("bouncestatus");
        }
        
    }
}
$("#secondSearch").on("click",function(){  
    var tagArr = getCheckedValue();
    console.log(tagArr);
    if (tagArr.length>0){
        var predicate = new ej.data.Predicate('label', 'contains', tagArr[0]);
        if(tagArr.length>1){
            for(var i=1;i<tagArr.length;i++){
                predicate = predicate.or('label', 'contains',  tagArr[i]);
            }
        }
        var result = new ej.data.DataManager(customerData).executeLocal(new ej.data.Query().where(predicate));
        grid1.dataSource=result;
    }
});
function getCheckedValue(){
    var cbxTags = [];
    $('input:checkbox:checked[name="tag_active_col[]"]').each(function(i) {
         cbxTags[i] = this.value; 
        
    });
    return cbxTags;
}