$(document).ready(function () {
	$("#row1-carousel").owlCarousel({
		loop: true,
		items: 1,
		autoplay: true,
		responsiveClass: true,
		autoHeight: true,
		autoplayHoverPause: true,
		autoplaySpeed: 150
	});

	//$("#row1btn1").fadeIn(3000);
	setTimeout(function(){
		$("#row1btn3").fadeIn(200);
	},1);
	setTimeout(function(){
		$("#row1btn1").slideDown({ duration: 1500, easing: "easeOutBack" });
	},1);
	
	setTimeout(function(){
		$("#row1btn2").slideDown({ duration: 1500, easing: "easeOutBack" });
	},300);
	
	
});