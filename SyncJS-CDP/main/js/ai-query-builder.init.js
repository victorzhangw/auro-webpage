var initpainting = function(){
    //中文化
    var ajax = new ej.base.Ajax('../../assets/node_modules/ej2-base/dist/zh-CN.json', 'GET', false);
    ajax.onSuccess = function (value) {
        var obj = JSON.parse(value);
        ej.base.L10n.load(obj);
    };

    ajax.send();
    // Panel
    var splitObj1 = new ej.layouts.Splitter({
        height: '100%',
        paneSettings: [
            { size: '27%',max:'100%'},
            { size: '73%'}

        ],
        width: '100%'
        
    });
    splitObj1.appendTo('#horizontal');
    //Render initialized Tab component
    var tabObj = new ej.navigations.Tab(); 
    tabObj.appendTo('#tab_result_set');
};
var tab1 = function () {
    // Render categories List
    var ascClass = 'e-sort-icon-ascending';
    var desClass = 'e-sort-icon-descending';
    
    var categorydata = [
  {
    "id": 1001,
    "aitype": "Customer Behaviour",
    "aicount": 0
  },
  {
    "id": 1002,
    "aitype": "Customer Type",
    "aicount": 1
  },
  {
    "id": 1003,
    "aitype": "Customer Recommendation",
    "aicount": 1
  },
  {
    "id": 1004,
    "aitype": "Product Recommendation",
    "aicount": 0
  },
  {
    "id": 1005,
    "aitype": "Product Categoried",
    "aicount": 0
  },
  {
    "id": 1006,
    "aitype": "Sentimental Analysis",
    "aicount": 1
  },
  {
    "id": 1007,
    "aitype": "Advertised Categoried",
    "aicount": 0
  },
  {
    "id": 1008,
    "aitype": "Selling Analysis",
    "aicount": 0
  },
  {
    "id": 1009,
    "aitype": "Sale Predict",
    "aicount": 0
  }
];
            

    var listViewInstance = new ej.lists.ListView({
        //set the data to datasource property
        dataSource: categorydata.slice(),
        fields: { id: 'id' },
        height:'calc(100vh - 277px)',
        //set the template for list items
        template: '<div><div class="listgrid1"><span class="e-list-content ">${aitype}</span></div>'+
        '<div class="listgrid2"><button class=" e-control e-btn e-small e-round e-edit-btn e-primary e-icon-btn" data-ripple="true">'+
        '<span class="e-btn-icon e-icons edit-icon"></span></button><button class="delete e-control e-btn e-small e-round e-delete-btn e-primary e-icon-btn" data-ripple="true">'+
        '<span class="e-btn-icon e-icons delete-icon"></span></button></div>'+
        '<div class="listgrid3 categorycount">${aicount}</div></div>',
        
        //set sortOrder for list items
        sortOrder: 'Ascending',
        actionComplete: function() {
            wireEvents();
        }

    });

    //Render initialized ListView
    listViewInstance.appendTo('#element');

    var dialogObj = new ej.popups.Dialog({
        header: 'Add Categories',
        content: '<div id="listDialog"><div class="input_name"><label for="name">分類:'+
        '</label><input id="categoriesName" class="e-input" type="text" placeholder=""></div></div>',
        showCloseIcon: true,
        buttons: [{
            click: dlgButtonClick,
            buttonModel: { content: '加入', isPrimary: true }
        }],
        width: '350px',
        visible: false
    });
    function geteditDialogContent() {
            var  template = '<div id="listDialog"><div class="input_name"><label for="name">分類:'+
        '</label><input id="editCategoriesName" class="e-input" type="text" placeholder=""/></div></div>';
            return template;
        }
    var editdialogObj = new ej.popups.Dialog({
        header: '修改分類名',
        content: geteditDialogContent(),
        showCloseIcon: true,
        buttons: [{
            click: editdlgButtonClick,
            buttonModel: { content: '修改', isPrimary: true }
        }],
        width: '350px',
        visible: false,
        passvalue:''
    });

    function getaltDialogContent() {
            
        var  template = "<div class = 'dialog-content'><div class='msg-wrapper  col-lg-12'>" +
                            "<span  class='error-msg col-lg-12'>此分類中已有分組</span>" +
                            " </div></div>";
            return template;
        }
    
    var altDlgObj = new ej.popups.Dialog({
        header: '禁止刪除',
        content: getaltDialogContent(),
        showCloseIcon: true,
        visible: false,
        buttons: [{
            buttonModel: { isPrimary: false, content: '關閉' }, click:  function() {
                this.hide();
            }
        }],
        width: '350px',
        animationSettings: { effect: 'Zoom' }
        });
    dialogObj.appendTo('#tab1dialog');
    altDlgObj.appendTo('#tab1altdialog');
    editdialogObj.appendTo('#tab1editdialog');

    function addItem() {
        document.getElementById("categoriesName").value = "";
        
        dialogObj.show();
    }

    function wireEvents() {
        Array.prototype.forEach.call(document.getElementsByClassName('e-delete-btn'), function(ele) {
            ele.addEventListener('click', onDeleteBtnClick);
        });
        Array.prototype.forEach.call(document.getElementsByClassName('e-edit-btn'), function(ele) {
            ele.addEventListener('click', onEditBtnClick);
        });
        document.getElementById("add").addEventListener('click', addItem);
        document.getElementById("sort").addEventListener('click', sortItems);
        document.getElementById("searchicontab1").addEventListener("click", tab1onKeyUp);
    }

    //Here we are removing list item
    function onDeleteBtnClick(e) {
        e.stopPropagation();
        var li = ej.base.closest(e.currentTarget, '.e-list-item');
        
        var data = listViewInstance.findItem(li);
        if (parseInt(data.categoryCount) == 0 ) {
            
            listViewInstance.removeItem(data);
        }else{
            x=1;
            altDlgObj.show();
        }
        
        //new ej.data.DataManager(categorydata).remove('id', { id: data["catid"] });
    new ej.data.DataManager(categorydata).remove('id', { id: data.catid });
    }

    //Here we are Editing list item
    function onEditBtnClick(e) {
        e.stopPropagation();
        var li = ej.base.closest(e.currentTarget, '.e-list-item');
        
        var data = listViewInstance.findItem(li);
    
        editdialogObj.passvalue=data;
        document.getElementById('editCategoriesName').value =data.categoriesName;
        editdialogObj.show();
    }

    //Here we are adding list item
    function dlgButtonClick() {
        var name = (document.getElementById("categoriesName")).value;
        
        var id = Math.random() * 10000;
        listViewInstance.addItem([{ aitype: name, id: id, aicount: '0' }]);
        categorydata.push({ aitype: name, id: id, aicount: '0' });
        listViewInstance.getLiFromObjOrElement({ catid: id }).getElementsByClassName('e-delete-btn')[0].addEventListener('click', onDeleteBtnClick);
        dialogObj.hide();
    }
    function editdlgButtonClick(){
        
        //console.log(editdialogObj.passvalue.catid);
        
        //var editData = (listViewInstance.findItem(editdialogObj.passvalue));
        var editData = (listViewInstance.findItem({catid:editdialogObj.passvalue.catid}));
        
        editData.categoriesName = document.getElementById('editCategoriesName').value ;
        listViewInstance.refresh();
        
    }

    //Here we are sorting list item
    function sortItems() {
        var ele = document.getElementById("sort").firstElementChild;
        var des = ele.classList.contains(desClass) ? true : false;
        if (des) {
            ele.classList.remove(desClass);
            ele.classList.add(ascClass);
            listViewInstance.sortOrder = 'Ascending';
        } else {
            ele.classList.remove(ascClass);
            ele.classList.add(desClass);
            listViewInstance.sortOrder = 'Descending';
        }
        listViewInstance.dataBind();
        wireEvents();
    }

    //Here, the list items are filtered using the DataManager instance.
    function tab1onKeyUp(e) {
        var value = (document.getElementById("searchtab1")).value;
        var data = new ej.data.DataManager(categorydata).executeLocal(
            new ej.data.Query().where("categoriesName", "startswith", value, true)
        );

        if (!value) {
            listViewInstance.dataSource = categorydata.slice();
            
        } else {
        
            listViewInstance.dataSource = data;
            listViewInstance.dataBind();
        }
    }


          
   
};
var tab1searchbox = function(){
    var  _searchbox = document.getElementById("searchbox1");
    var  _searchguide = document.getElementById("tab1searchguide");
    var  _searchclose = document.getElementById("search-close");
    _searchguide.addEventListener("click",function(){baseFunction.slide(_searchbox,_searchguide,_searchclose);});
    _searchclose.addEventListener("click",function(){baseFunction.hide(_searchbox,_searchguide,_searchclose);});


};
var tab2 = function(){
    // Render custom query condiction List
     var listData={};
    // Render custom query condiction List
     var ajax = new ej.base.Ajax('../../assets/node_modules/ej2-base/dist/aimodel.json', 'GET', false);
     
     ajax.onSuccess = function (value) {
        
        console.log('1');
         listData = JSON.parse(value);
        
    };
    ajax.send();
// Initialize ListView component
    var listObj = new ej.lists.ListView({
        //Set defined data to dataSource property
        dataSource: listData,
        //Map the appropriate columns to fields property
        fields: {  id: "id" },
        height:'calc(100vh - 250px)',
        template: '<div><div class="listgrid1"><span class="e-list-content ">${ainame}</span></div>'+
        '<div class="listgrid2"><button class=" e-control e-btn e-small e-round e-edit-btn e-primary e-icon-btn" data-ripple="true">'+
        '<span class="e-btn-icon e-icons edit-icon"></span></button></div>'+
        '<div class="listgrid3 categorycount">${count}</div></div>',
        //Set sortOrder property
        sortOrder: "Ascending",
        actionComplete: function() {
            wireaddEvents();
        }
    });
//Render initialized ListView component
    listObj.appendTo("#querySet");
    document.getElementById("searchtab2").addEventListener("keyup", tab2onKeyUp);
    //Here we are handling filtering of list items using dataManager for ListView
    function tab2onKeyUp(e) {
        var value = document.getElementById("searchtab2").value;
        var data = new ej.data.DataManager(listData).executeLocal(new ej.data.Query().where("text", "startswith", value, true));
        if (!value) {
            listObj.dataSource = listData.slice();
        }
        else {
            listObj.dataSource = data;
        }
        listObj.dataBind();
    }
    var columnData = [
                { field: 'QuerySet', label: '查詢組合', type: 'string'},
                { field: 'Author', label: '作者', type: 'string' },
                { field: 'EmployeeID', label: '會員編號', type: 'number' },
                { field: 'FirstName', label: '姓名', type: 'string' },
                { field: 'gender', label: '性別', type: 'boolean', values: ['Male.', 'Female','Unknown'] },
                { field: 'TitleOfCourtesy', label: '會員等級', type: 'string', values: ['Golden.', 'Silver','Bronze']},
                { field: 'Title', label: '職業類型', type: 'string' },
                { field: 'HireDate', label: '入會日期', type: 'date', format: 'dd/MM/yyyy' },
                { field: 'Country', label: '國家', type: 'string' },
                { field: 'City', label: '城市', type: 'string' }
            ];
    var importRules = {
        'condition': 'and',
        'rules': [{
                'label': 'QuerySET',
                'field': 'QuerySet',
                'type': 'string',
                'operator': 'equal',
                'value': ''
            },
            {
                'label': 'Author',
                'field': 'Author',
                'type': 'string',
                'operator': 'equal',
                'value': ''
            }
        ]
    };
    var qryBldrObj = new ej.querybuilder.QueryBuilder({
        width: '100%',
        height:'100%',
        
        dataSource: window.expenseData,
        columns: columnData,
        rule: importRules,
        
    });
    qryBldrObj.appendTo('#querybuilder');   
    
   function wireaddEvents() {
       
        Array.prototype.forEach.call(document.getElementsByClassName('e-edit-btn'), function(ele) {
            ele.addEventListener('click', onaddmodelBtnClick);
        });
       
    }
    function onaddmodelBtnClick(e) {
          var li = ej.base.closest(e.currentTarget, '.e-list-item');
        
        var data = listObj.findItem(li);
        console.log(data);
        e.stopPropagation();
       $('#modelchosen').html(data.ainame);
    }
};
var tab2searchbox = function(){
    var  _searchbox = document.getElementById("searchbox2");
    var  _searchguide = document.getElementById("tab2searchguide");
    var  _searchclose = document.getElementById("tab2search-close");
    _searchguide.addEventListener("click",function(){baseFunction.slide(_searchbox,_searchguide,_searchclose);});
    _searchclose.addEventListener("click",function(){baseFunction.hide(_searchbox,_searchguide,_searchclose);});


};
var baseFunction={
    slide:function(arg1,arg2,arg3){
        arg1.style.visibility="visible";
        var animation = new ej.base.Animation({ 
        duration: 1000,
        begin:function(){
            if (arg2.style.visibility !="hidden"){
                arg3.style.visibility="visible";
                arg2.style.visibility="hidden";
                arg1.style.width="300px";}
        },

});
    //animation.animate('#searchbox1', { name: '' });
    animation.animate(arg1, { name: '' });
    },
        
    hide:function(arg1,arg2,arg3){
        if (arg2.style.visibility =="hidden"){
            arg1.style.visibility="hidden";
    
        var animation = new ej.base.Animation({ 
        begin:function(){
            arg1.style.width="30px";
            arg3.style.visibility="hidden";
        },
        end:function(){
            arg2.style.visibility = "visible";
    },

    });
    animation.animate(arg1,{name:''});
 }

    }
    
};


initpainting();
tab1();
tab1searchbox();
tab2();
tab2searchbox();
