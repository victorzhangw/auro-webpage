var initpainting = function(){
    //中文化
    var ajax = new ej.base.Ajax('../../assets/node_modules/ej2-base/dist/zh-CN.json', 'GET', false);
    ajax.onSuccess = function (value) {
        var obj = JSON.parse(value);
        ej.base.L10n.load(obj);
    };

    ajax.send();
   
};

// Render initialized splitter


var aischedule= function(){
ej.base.enableRipple(true);

    var isReadOnly = function (endDate) {
        return (endDate < new Date(2019, 6, 31, 0, 0));
    };
    window.getRoomName = function (value) {
        return value.resourceData[value.resource.textField];
    };
    window.getRoomType = function (value) {
        return value.resourceData.type;
    };
    window.getRoomCapacity = function (value) {
        return value.resourceData.capacity;
    };
    var data = new ej.base.extend([], window.roomData, null, true);
    var scheduleObj = new ej.schedule.Schedule({
        width: '100%',
        height: '95%',
        selectedDate: new Date(2019, 2, 1),
        currentView: 'TimelineWeek',
        workHours: {
            start: '06:00',
            end: '21:00'
        },
        timeScale: {
            slotCount: 1,
            interval: 60
        },
        views: [
            { option: 'TimelineDay' },
            { option: 'TimelineWeek'},
          
        ],
        resourceHeaderTemplate: '#resource-template',
        group: {
            enableCompactView: false,
            resources: ['MeetingRoom']
        },
        resources: [{
            field: 'RoomId', title: 'Model Type',
            name: 'MeetingRoom', allowMultiple: true,
            dataSource: [
                { text: '已購買-同質產品推薦模型', id: 1, color: '#ea7a57', capacity: 1, type: 'Conference' },
                { text: '不同引流管道的客戶模型', id: 2, color: '#7fa900', capacity: 0, type: 'Cabin' },
                { text: '不同廣告素材的客戶模型', id: 3, color: '#5978ee', capacity: 0, type: 'Cabin' },
                { text: '分類產品推薦-站內熱門點擊', id: 4, color: '#5978ee', capacity: 0, type: 'Cabin' },
                { text: '分類產品推薦-站外熱議', id: 5, color: '#5978ee', capacity: 0, type: 'Cabin' },
                { text: '出清產品推薦模型', id: 6, color: '#5978ee', capacity: 0, type: 'Cabin' },
                { text: '未購買-同質產品推薦模型', id: 7, color: '#5978ee', capacity: 1, type: 'Cabin' },
                { text: '年齡推薦模型', id: 8, color: '#5978ee', capacity: 0, type: 'Cabin' },
                { text: '低存貨推薦模型', id: 9, color: '#5978ee', capacity: 0, type: 'Cabin' },
                { text: '低頻交易客戶模型', id: 10, color: '#5978ee', capacity: 0, type: 'Cabin' },
            
                
            ],
            textField: 'text', idField: 'id', colorField: 'color'
        }],
        eventSettings: {
            dataSource: data,
            fields: {
                id: 'Id',
                subject: { title: 'Project', name: 'Subject' },
                location: { title: 'Target', name: 'Location' },
                description: { title: 'Message Template', name: 'Description' },
                startTime: { title: 'From', name: 'StartTime' },
                endTime: { title: 'To', name: 'EndTime' }
            }
        },
        popupOpen: function (args) {
            var data = args.data;
          
            if (args.type === 'QuickInfo' || args.type === 'Editor' || args.type === 'RecurrenceAlert' || args.type === 'DeleteAlert') {
                var target = (args.type === 'RecurrenceAlert' ||
                    args.type === 'DeleteAlert') ? data.element[0] : args.target;
                if (!ej.base.isNullOrUndefined(target) && target.classList.contains('e-work-cells')) {
                    if ((target.classList.contains('e-read-only-cells')) ||
                        (!scheduleObj.isSlotAvailable(data.endTime, data.endTime, data.groupIndex))) {
                        args.cancel = true;
                    }
                }
                else if (!ej.base.isNullOrUndefined(target) && target.classList.contains('e-appointment') &&
                    (isReadOnly(data.EndTime))) {
                    args.cancel = true;
                }
            } 
            if (args.type === 'Editor') {
                // Create required custom elements in initial time
                if (!args.element.querySelector('.custom-field-row')) {
                    var row = new ej.base.createElement('div', { className: 'custom-field-row' });
                    var formElement = args.element.querySelector('.e-schedule-form');
                    formElement.firstChild.insertBefore(row, args.element.querySelector('.e-title-location-row'));
                    args.element.querySelector('.e-title-location-row').style.display='none';
                    var container = new ej.base.createElement('div', { className: 'custom-field-container' });
                    var inputEle = new ej.base.createElement('input', {
                        className: 'e-field', attrs: { name: 'EventType' }
                    });
                    container.appendChild(inputEle);
                    row.appendChild(container);
                    var drowDownList = new ej.dropdowns.DropDownList({
                        dataSource: [
                            { text: '20181004 APP Push - Admin', value: 'public-event' },
                            { text: '20181117 Email Marketing - Admin', value: 'maintenance' },
                            { text: '20190104 SMS - Admin', value: 'commercial-event' },
                            { text: '20190108 Email Marketing - Admin', value: 'family-event' }
                        ],
                        fields: { text: 'text', value: 'value' },
                        value: args.EventType,
                        floatLabelType: 'Always', placeholder: 'Query Sets'
                    });
                    drowDownList.appendTo(inputEle);
                    inputEle.setAttribute('name', 'EventType');
                }
            }
        },
        renderCell: function (args) {
            if (args.element.classList.contains('e-work-cells')) { 
                if (args.date < new Date(2018, 6, 31, 0, 0)) {
                    args.element.setAttribute('aria-readonly', 'true');
                    args.element.classList.add('e-read-only-cells');
                }
            }
            if (args.elementType === 'emptyCells' && args.element.classList.contains('e-resource-left-td')) {
                var target = args.element.querySelector('.e-resource-text');
                target.innerHTML = '<div class="name">Counts</div><div class="capacity">Projects</div>';
            }
        },
        eventRendered: function (args) {
            var data = args.data;
            if (isReadOnly(data.EndTime)) {
                args.element.setAttribute('aria-readonly', 'true');
                args.element.classList.add('e-read-only');
            }
        },
        actionBegin: function (args) {
            if (args.requestType === 'eventCreate' || args.requestType === 'eventChange') {
                var data = void 0;
                if (args.requestType === 'eventCreate') {
                    data = args.data[0];
                }
                else if (args.requestType === 'eventChange') {
                    data = args.data;
                }
                var groupIndex = scheduleObj.eventBase.getGroupIndexFromEvent(data);
                if (!scheduleObj.isSlotAvailable(data.StartTime, data.EndTime, groupIndex)) {
                    args.cancel = true;
                }
            }
        }
    });
    scheduleObj.appendTo('#Schedule');

};

initpainting();
aischedule();
