
var expertquerybuilder = function () {
var ajax = new ej.base.Ajax('../../assets/node_modules/ej2-base/dist/zh-CN.json', 'GET', false);
ajax.onSuccess = function (value) {
     var obj = JSON.parse(value);
     ej.base.L10n.load(obj);
};

ajax.send();


    // Panel
  
    
    var splitObj1 = new ej.layouts.Splitter({
        height: '100%',
        paneSettings: [
            { size: '25%',max:'100%'},
            { size: '75%'}

        ],
        width: '100%'
        
    });
    splitObj1.appendTo('#horizontal');
   

//Render initialized Tab component
    var tabObj = new ej.navigations.Tab(); 
    tabObj.appendTo('#tab_result_set');
//Render Categorires grid
//ej.grids.Grid.Inject(ej.grids.Page, ej.grids.Group, ej.grids.Sort, ej.grids.Filter, ej.grids.ContextMenu);
   
var data = [{ catid: 10248, categoriesName: '營業', categoryCount: '11' },
            { catid: 10249, categoriesName: '行銷', categoryCount: '12' },
            { catid: 10250, categoriesName: '運動', categoryCount: '17' },
            { catid: 10251, categoriesName: '美妝', categoryCount: '12' },
            { catid: 10252, categoriesName: '保健食品',  categoryCount: '4' },
            { catid: 10253, categoriesName: '雜貨', categoryCount: '17' },
            { catid: 10254, categoriesName: '家飾', categoryCount: '8' },
            { catid: 10255, categoriesName: '網路分析', categoryCount: '21' },
            { catid: 10256, categoriesName: '消費電子', categoryCount: '2' }];
            
   

// Render List
    var listData = [
    { text: "30 天內未消費", id: "list-01" },
    { text: "60 天內未消費", id: "list-02" },
    { text: "90 天內未消費", id: "list-03" },
    { text: "低於平均客單價", id: "list-04" },
    { text: "每週到站一次以上", id: "list-05" },
    { text: "平均消費金額 500 元", id: "list-06" },
    { text: "平均消費金額 1000 元", id: "list-06" },
    { text: "平均消費金額 1500 元", id: "list-06" },
    { text: "平均消費金額 2000 元", id: "list-06" },
];
// Initialize ListView component
    var listObj = new ej.lists.ListView({
        //Set defined data to dataSource property
        dataSource: listData,
        //Map the appropriate columns to fields property
        fields: { text: "text", id: "id" },
        //Set sortOrder property
        sortOrder: "Ascending"
    });
    //Render initialized ListView component
    listObj.appendTo("#list");
    document.getElementById("filterbox").addEventListener("keyup", onKeyUp);
    //Here we are handling filtering of list items using dataManager for ListView
    function onKeyUp(e) {
        var value = document.getElementById("textbox").value;
        var data = new ej.data.DataManager(listData).executeLocal(new ej.data.Query().where("text", "startswith", value, true));
        if (!value) {
            listObj.dataSource = listData.slice();
        }
        else {
            listObj.dataSource = data;
        }
        listObj.dataBind();
    }
    var columnData = [
                { field: 'EmployeeID', label: 'Employee ID', type: 'number' },
                { field: 'FirstName', label: 'First Name', type: 'string' },
                { field: 'TitleOfCourtesy', label: 'Title Of Courtesy', type: 'boolean', values: ['Mr.', 'Mrs.']},
                { field: 'Title', label: 'Title', type: 'string' },
                { field: 'HireDate', label: 'Hire Date', type: 'date', format: 'dd/MM/yyyy' },
                { field: 'Country', label: 'Country', type: 'string' },
                { field: 'City', label: 'City', type: 'string' }
            ];
            var importRules = {
                'condition': 'and',
                'rules': [{
                        'label': 'Employee ID',
                        'field': 'EmployeeID',
                        'type': 'number',
                        'operator': 'equal',
                        'value': 1001
                    },
                    {
                        'label': 'Title',
                        'field': 'Title',
                        'type': 'string',
                        'operator': 'equal',
                        'value': 'Sales Manager'
                    }
                ]
            };
            var qryBldrObj = new ej.querybuilder.QueryBuilder({
                width: '100%',
                dataSource: window.expenseData,
                columns: columnData,
                rule: importRules,
            });
            qryBldrObj.appendTo('#querybuilder');   
          
   
};
expertquerybuilder();
