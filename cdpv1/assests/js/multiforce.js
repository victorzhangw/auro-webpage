DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();

const chartitems = (document.getElementById('dataChart'));
const prefix = sessionStorage.getItem("sitePrefix");
const appName = sessionStorage.getItem("appName")
var refIndex1 = 30000; // 訪客數參考指標
var L10n = ej.base.L10n;
var dp, DataObj, Dataobj1;

const gridColumn = [
  {
    field: 'campaign',
    width: 420,
    headerText: '訂單',
    format: 'N'
  },
  {
    field: 'strategy',
    headerText: '策略',
    textAlign: 'left',
    width: 180,
    format: 'N'
  },
  {
    field: 'creative',
    headerText: '素材',
    textAlign: 'left',
    width: 180,
    format: 'N'
  },
  {
    field: 'budget',
    headerText: '花費',
    textAlign: 'left',
    width: 80,
    format: 'N'
  },
  {
    field: 'impress',
    headerText: '曝光',
    textAlign: 'left',
    width: 80,
    format: 'N'
  },
  {
    field: 'click',
    headerText: '點擊',
    textAlign: 'left',
    width: 80,
    format: 'N'
  },
];

$(document).ready(function () {
  let sdt = moment().subtract(1, 'days').format("YY-MM-DD");
  let edt = moment().subtract(1, 'days').format("YY-MM-DD");
  DramaCore.validateTokenCall(validateSetting());
  let siteKey = prefix + "mf-" + sdt + "-" + edt;
  let dt = moment().subtract(1, 'days').format("YY-MM-DD");
  dp = DramaCore.createDatePicker(dt, dt);
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = "";

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };

  fetch("https://localhost:44372/api/MultiForceEvent/MFDailyReport", requestOptions)
    .then(response => response.text())
    .then(result => {

      localStorage.setItem(siteKey, result);
      //spinner.setAttribute('hidden', '');
      initMFPage.chart();
      DramaCore.renderMFChart(siteKey);
      $("body").removeClass("loading");

    })
    .catch(error => console.log('error', error));



});


// url --> remote url ; type:post or get ; para:parameters
var initMFPage = {
  widget: function () {



  },
  chart: function () {
    let chartMetaData = [
      { id: 'chart1', size: 'col-md-12' },
      { id: 'chart2', size: 'col-md-12' },
      { id: 'chart3', size: 'col-md-12' },


    ];
    let getChartString = ej.base.compile('<div class="col-sm-12 ${size} mb-1 d-flex"> <div id=${id} class="card card_animation flex-fill"> <div class="card-header widget-head"> </div><div class="charts card-body chart-height"> </div> </div>  </div>');
    while (chartitems.firstChild) {
      chartitems.removeChild(chartitems.firstChild);
    }
    chartMetaData.forEach(data => {
      chartitems.appendChild(getChartString(data)[0]);
    });
  },


};


// 客製化表格欄位值

var MFCharts = {

  chart1: function (data, location, rowMax, header, chartid) {
    let rst = data.report,
      chartObj = {},
      list = [],
      metric = [];
    console.log(rst);
    /*
    for (let i = 0; i < rst.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj = {};
      chartObj.data_time = rst[i].data_time;
      chartObj.advertiser = rst[i].advertiser;
      chartObj.campaign = rst[i].campaign;
      chartObj.strategy = rst[i].strategy;
      chartObj.creative = rst[i].creative;
      chartObj.budget = Number(rst[i].budget);
      chartObj.impress = Number(rst[i].impress);
      chartObj.click = Number(rst[i].click);
      list.push(chartObj);

    }*/
    /*
    var query = Enumerable.from(rst)
      .groupBy(
        "{ Id: $.DistributorId, Name: $.DistributorName }",
        "{ Year: $.Year, Month: $.Month }",
        "{ DistributorId: $.Id, DistributorName: $.Name, PriceLists: $$.toArray() }",
        "String($.Id) + $.Name"
      )
      .toArray();*/
    var result = Enumerable.from(rst)
      .groupBy((x) => x.campaign_id + ':' + x.campaign)
      .select((x) => {
        return {
          campaign_id: x.first().campaign_id,
          campaign: x.first().campaign,
          budget: x.sum((y) => Number(y.budget) | 0),
          click: x.sum((y) => Number(y.click) | 0),
          impress: x.sum((y) => Number(y.impress) | 0),
        };
      })
      .toArray();

    console.log("result", result);
    let setting = {
      cardHead: header,
      titleText: "",
      value: result,
      color: "",
      gridColumn: gridColumn,
      chartid: chartid
    };
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart1 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart2: function (data, location, rowMax, header, chartid) {
    let rst = data.report,
      chartObj = {},
      list = [],
      metric = [];
    console.log(rst);

    var result = Enumerable.from(rst)
      .groupBy((x) => x.campaign_id + ':' + x.campaign + ':' + x.strategy)
      .select((x) => {
        return {
          campaign_id: x.first().campaign_id,
          campaign: x.first().campaign,
          strategy: x.first().strategy,
          budget: x.sum((y) => Number(y.budget) | 0),
          click: x.sum((y) => Number(y.click) | 0),
          impress: x.sum((y) => Number(y.impress) | 0),
        };
      })
      .toArray();

    console.log("result", result);
    let setting = {
      cardHead: header,
      titleText: "",
      value: result,
      color: "",
      gridColumn: gridColumn,
      chartid: chartid
    };
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart2 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart3: function (data, location, rowMax, header, chartid) {
    let rst = data.report,
      chartObj = {},
      list = [],
      metric = [];
    console.log(rst);

    var result = Enumerable.from(rst)
      .groupBy((x) => x.campaign_id + ':' + x.campaign + ':' + x.strategy + ':' + x.creative)
      .select((x) => {
        return {
          campaign_id: x.first().campaign_id,
          campaign: x.first().campaign,
          strategy: x.first().strategy,
          creative: x.first().creative,
          budget: x.sum((y) => Number(y.budget) | 0),
          click: x.sum((y) => Number(y.click) | 0),
          impress: x.sum((y) => Number(y.impress) | 0),
        };
      })
      .toArray();

    console.log("result", result);
    let setting = {
      cardHead: header,
      titleText: "",
      value: result,
      color: "",
      gridColumn: gridColumn,
      chartid: chartid
    };
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart3 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
};

function validateSetting() {
  let bearerToken = sessionStorage.getItem("token");
  let urlTo = AuthremoteUrl + "GetUserName";
  let Setting = {
    async: true,
    type: "GET",
    url: urlTo,
    headers: {
      Authorization: "Bearer" + " " + bearerToken

    }
  };
  return Setting;
}
function loadingScreen(responseTime) {
  var html = '<div id="load"></div>';
  $('#lodinghint').append(html);
  setTimeout(function () {
    $('#load').remove();
  }, responseTime);
}
