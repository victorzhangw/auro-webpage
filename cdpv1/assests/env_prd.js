/*
const AuthremoteUrl = "https://cdp.aurocore.com/api/AuthenticationService/";
const localurl="http://caco.aurocore.com/CF_HAMI_CDP/";
loginUrl="/login.html";
var sitePrefix = "";
var siteDefine = [{
        "name": "caco",
        "prefix": "caco",
        "memberKey": "45488F6FA45856B7FA5184B3676B8",
        "GoogleremoteUrl": "https://cdp.aurocore.com/api/GoogleService/",
        "localurl": "http://caco.aurocore.com/CF_HAMI_CDP/caco/",
        "AuthremoteUrl": "https://cdp.aurocore.com/api/AuthenticationService/",
        "TealeafremoteUrl": "https://cdp.aurocore.com/api/TealeafEvent/",
        "LabelremoteUrl": "https://cdp.aurocore.com/api/GridProvider/",
        "ApiremoteUrl": "https://cdp.aurocore.com/api/",

    },
    {
        "name": "waveshine",
        "prefix": "wave",
        "memberKey": "45488F6FA45856B7FA5184B3676B8",
        "GoogleremoteUrl": "http://waveshinecdp.aurocore.com/api/GoogleService/",
        "localurl": "http://caco.aurocore.com/CF_HAMI_CDP/waveshine/",
        "AuthremoteUrl": "http://waveshinecdp.aurocore.com/api/AuthenticationService/",
        "TealeafremoteUrl": "http://waveshinecdp.aurocore.com/api/TealeafEvent/",
        "LabelremoteUrl": "http://waveshinecdp.aurocore.com/api/GridProvider/",
        "ApiremoteUrl": "http://waveshinecdp.aurocore.com/api/",
    },
    {
        "name": "demosite",
        "prefix": "demo",
        "memberKey": "45488F6FA45856B7FA5184B3676B8",
        "GoogleremoteUrl": "http://demo.aurocore.com/api/GoogleService/",
        "localurl": "http://demo.aurocore.com/democdp/demosite/",
        "AuthremoteUrl": "http://demo.aurocore.com/api/AuthenticationService/",
        "TealeafremoteUrl": "http://demo.aurocore.com/api/TealeafEvent/",
        "LabelremoteUrl": "http://demo.aurocore.com/api/GridProvider/",
        "ApiremoteUrl": "http://demo.aurocore.com/api/",
    }

];
*/
loginUrl = "/login.html";
const AuthremoteUrl = "https://localhost:44372/api/AuthenticationService/";
const localurl = "http://127.0.0.1:44372/";
var sitePrefix = "";
var siteDefine = [{
    "viewId": "DefaultID",
    "name": "hamigo",
    "prefix": "hamigo",
    "memberKey": "45488F6FA45856B7FA5184B3676B8",
    "GoogleremoteUrl": "https://localhost:44372/api/GoogleService/",
    "localurl": "http://127.0.0.1:44372/hamigo/",
    "AuthremoteUrl": "https://localhost:44372/api/AuthenticationService/",
    "TealeafremoteUrl": "https://localhost:44372/api/TealeafEvent/",
    "ClickForcefremoteUrl": "https://localhost:44372/api/MultiForceEvent/",
    "ApiremoteUrl": "https://localhost:44372/api/",
},
{
    "viewId": "TwoID",
    "name": "hamipoint",
    "prefix": "hamipoint",
    "memberKey": "45488F6FA45856B7FA5184B3676B8",
    "GoogleremoteUrl": "https://localhost:44372/api/GoogleService/",
    "localurl": "http://127.0.0.1:44372/hamipoint/",
    "AuthremoteUrl": "https://localhost:44372/api/AuthenticationService/",
    "TealeafremoteUrl": "https://localhost:44372/api/TealeafEvent/",
    "ClickForcefremoteUrl": "https://localhost:44372/api/MultiForceEvent/",
    "ApiremoteUrl": "https://localhost:44372/api/",

},
{
    "viewId": "ThreeID",
    "name": "hamiliveshop",
    "prefix": "hamiliveshop",
    "memberKey": "45488F6FA45856B7FA5184B3676B8",
    "GoogleremoteUrl": "https://localhost:44372/api/GoogleService/",
    "localurl": "http://127.0.0.1:44372/hamiliveshop/",
    "AuthremoteUrl": "https://localhost:44372/api/AuthenticationService/",
    "TealeafremoteUrl": "https://localhost:44372/api/TealeafEvent/",
    "ClickForcefremoteUrl": "https://localhost:44372/api/MultiForceEvent/",
    "ApiremoteUrl": "https://localhost:44372/api/",

},
{
    "viewId": "FourID",
    "name": "hamimarket",
    "prefix": "hamimarket",
    "memberKey": "45488F6FA45856B7FA5184B3676B8",
    "GoogleremoteUrl": "https://localhost:44372/api/GoogleService/",
    "localurl": "http://127.0.0.1:44372/hamimarket/",
    "AuthremoteUrl": "https://localhost:44372/api/AuthenticationService/",
    "TealeafremoteUrl": "https://localhost:44372/api/TealeafEvent/",
    "ClickForcefremoteUrl": "https://localhost:44372/api/MultiForceEvent/",
    "ApiremoteUrl": "https://localhost:44372/api/",

}

];