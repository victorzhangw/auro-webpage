/**਍ ⨀ 䀀氀椀挀攀渀猀攀 䄀渀最甀氀愀爀䨀匀 瘀㄀⸀㈀⸀㠀ഀഀ
 * (c) 2010-2014 Google, Inc. http://angularjs.org਍ ⨀ 䰀椀挀攀渀猀攀㨀 䴀䤀吀ഀഀ
 */਍⠀昀甀渀挀琀椀漀渀⠀眀椀渀搀漀眀Ⰰ 搀漀挀甀洀攀渀琀Ⰰ 甀渀搀攀昀椀渀攀搀⤀ 笀✀甀猀攀 猀琀爀椀挀琀✀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @description਍ ⨀ഀഀ
 * This object provides a utility for producing rich Error messages within਍ ⨀ 䄀渀最甀氀愀爀⸀ 䤀琀 挀愀渀 戀攀 挀愀氀氀攀搀 愀猀 昀漀氀氀漀眀猀㨀ഀഀ
 *਍ ⨀ 瘀愀爀 攀砀愀洀瀀氀攀䴀椀渀䔀爀爀 㴀 洀椀渀䔀爀爀⠀✀攀砀愀洀瀀氀攀✀⤀㬀ഀഀ
 * throw exampleMinErr('one', 'This {0} is {1}', foo, bar);਍ ⨀ഀഀ
 * The above creates an instance of minErr in the example namespace. The਍ ⨀ 爀攀猀甀氀琀椀渀最 攀爀爀漀爀 眀椀氀氀 栀愀瘀攀 愀 渀愀洀攀猀瀀愀挀攀搀 攀爀爀漀爀 挀漀搀攀 漀昀 攀砀愀洀瀀氀攀⸀漀渀攀⸀  吀栀攀ഀഀ
 * resulting error will replace {0} with the value of foo, and {1} with the਍ ⨀ 瘀愀氀甀攀 漀昀 戀愀爀⸀ 吀栀攀 漀戀樀攀挀琀 椀猀 渀漀琀 爀攀猀琀爀椀挀琀攀搀 椀渀 琀栀攀 渀甀洀戀攀爀 漀昀 愀爀最甀洀攀渀琀猀 椀琀 挀愀渀ഀഀ
 * take.਍ ⨀ഀഀ
 * If fewer arguments are specified than necessary for interpolation, the extra਍ ⨀ 椀渀琀攀爀瀀漀氀愀琀椀漀渀 洀愀爀欀攀爀猀 眀椀氀氀 戀攀 瀀爀攀猀攀爀瘀攀搀 椀渀 琀栀攀 昀椀渀愀氀 猀琀爀椀渀最⸀ഀഀ
 *਍ ⨀ 匀椀渀挀攀 搀愀琀愀 眀椀氀氀 戀攀 瀀愀爀猀攀搀 猀琀愀琀椀挀愀氀氀礀 搀甀爀椀渀最 愀 戀甀椀氀搀 猀琀攀瀀Ⰰ 猀漀洀攀 爀攀猀琀爀椀挀琀椀漀渀猀ഀഀ
 * are applied with respect to how minErr instances are created and called.਍ ⨀ 䤀渀猀琀愀渀挀攀猀 猀栀漀甀氀搀 栀愀瘀攀 渀愀洀攀猀 漀昀 琀栀攀 昀漀爀洀 渀愀洀攀猀瀀愀挀攀䴀椀渀䔀爀爀 昀漀爀 愀 洀椀渀䔀爀爀 挀爀攀愀琀攀搀ഀഀ
 * using minErr('namespace') . Error codes, namespaces and template strings਍ ⨀ 猀栀漀甀氀搀 愀氀氀 戀攀 猀琀愀琀椀挀 猀琀爀椀渀最猀Ⰰ 渀漀琀 瘀愀爀椀愀戀氀攀猀 漀爀 最攀渀攀爀愀氀 攀砀瀀爀攀猀猀椀漀渀猀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 洀漀搀甀氀攀 吀栀攀 渀愀洀攀猀瀀愀挀攀 琀漀 甀猀攀 昀漀爀 琀栀攀 渀攀眀 洀椀渀䔀爀爀 椀渀猀琀愀渀挀攀⸀ഀഀ
 * @returns {function(string, string, ...): Error} instance਍ ⨀⼀ഀഀ
਍昀甀渀挀琀椀漀渀 洀椀渀䔀爀爀⠀洀漀搀甀氀攀⤀ 笀ഀഀ
  return function () {਍    瘀愀爀 挀漀搀攀 㴀 愀爀最甀洀攀渀琀猀嬀　崀Ⰰഀഀ
      prefix = '[' + (module ? module + ':' : '') + code + '] ',਍      琀攀洀瀀氀愀琀攀 㴀 愀爀最甀洀攀渀琀猀嬀㄀崀Ⰰഀഀ
      templateArgs = arguments,਍      猀琀爀椀渀最椀昀礀 㴀 昀甀渀挀琀椀漀渀 ⠀漀戀樀⤀ 笀ഀഀ
        if (typeof obj === 'function') {਍          爀攀琀甀爀渀 漀戀樀⸀琀漀匀琀爀椀渀最⠀⤀⸀爀攀瀀氀愀挀攀⠀⼀ 尀笀嬀尀猀尀匀崀⨀␀⼀Ⰰ ✀✀⤀㬀ഀഀ
        } else if (typeof obj === 'undefined') {਍          爀攀琀甀爀渀 ✀甀渀搀攀昀椀渀攀搀✀㬀ഀഀ
        } else if (typeof obj !== 'string') {਍          爀攀琀甀爀渀 䨀匀伀一⸀猀琀爀椀渀最椀昀礀⠀漀戀樀⤀㬀ഀഀ
        }਍        爀攀琀甀爀渀 漀戀樀㬀ഀഀ
      },਍      洀攀猀猀愀最攀Ⰰ 椀㬀ഀഀ
਍    洀攀猀猀愀最攀 㴀 瀀爀攀昀椀砀 ⬀ 琀攀洀瀀氀愀琀攀⸀爀攀瀀氀愀挀攀⠀⼀尀笀尀搀⬀尀紀⼀最Ⰰ 昀甀渀挀琀椀漀渀 ⠀洀愀琀挀栀⤀ 笀ഀഀ
      var index = +match.slice(1, -1), arg;਍ഀഀ
      if (index + 2 < templateArgs.length) {਍        愀爀最 㴀 琀攀洀瀀氀愀琀攀䄀爀最猀嬀椀渀搀攀砀 ⬀ ㈀崀㬀ഀഀ
        if (typeof arg === 'function') {਍          爀攀琀甀爀渀 愀爀最⸀琀漀匀琀爀椀渀最⠀⤀⸀爀攀瀀氀愀挀攀⠀⼀ 㼀尀笀嬀尀猀尀匀崀⨀␀⼀Ⰰ ✀✀⤀㬀ഀഀ
        } else if (typeof arg === 'undefined') {਍          爀攀琀甀爀渀 ✀甀渀搀攀昀椀渀攀搀✀㬀ഀഀ
        } else if (typeof arg !== 'string') {਍          爀攀琀甀爀渀 琀漀䨀猀漀渀⠀愀爀最⤀㬀ഀഀ
        }਍        爀攀琀甀爀渀 愀爀最㬀ഀഀ
      }਍      爀攀琀甀爀渀 洀愀琀挀栀㬀ഀഀ
    });਍ഀഀ
    message = message + '\nhttp://errors.angularjs.org/1.2.8/' +਍      ⠀洀漀搀甀氀攀 㼀 洀漀搀甀氀攀 ⬀ ✀⼀✀ 㨀 ✀✀⤀ ⬀ 挀漀搀攀㬀ഀഀ
    for (i = 2; i < arguments.length; i++) {਍      洀攀猀猀愀最攀 㴀 洀攀猀猀愀最攀 ⬀ ⠀椀 㴀㴀 ㈀ 㼀 ✀㼀✀ 㨀 ✀☀✀⤀ ⬀ ✀瀀✀ ⬀ ⠀椀ⴀ㈀⤀ ⬀ ✀㴀✀ ⬀ഀഀ
        encodeURIComponent(stringify(arguments[i]));਍    紀ഀഀ
਍    爀攀琀甀爀渀 渀攀眀 䔀爀爀漀爀⠀洀攀猀猀愀最攀⤀㬀ഀഀ
  };਍紀ഀഀ
਍⼀⨀ 圀攀 渀攀攀搀 琀漀 琀攀氀氀 樀猀栀椀渀琀 眀栀愀琀 瘀愀爀椀愀戀氀攀猀 愀爀攀 戀攀椀渀最 攀砀瀀漀爀琀攀搀 ⨀⼀ഀഀ
/* global਍    ⴀ愀渀最甀氀愀爀Ⰰഀഀ
    -msie,਍    ⴀ樀焀䰀椀琀攀Ⰰഀഀ
    -jQuery,਍    ⴀ猀氀椀挀攀Ⰰഀഀ
    -push,਍    ⴀ琀漀匀琀爀椀渀最Ⰰഀഀ
    -ngMinErr,਍    ⴀ开愀渀最甀氀愀爀Ⰰഀഀ
    -angularModule,਍    ⴀ渀漀搀攀一愀洀攀开Ⰰഀഀ
    -uid,਍ഀഀ
    -lowercase,਍    ⴀ甀瀀瀀攀爀挀愀猀攀Ⰰഀഀ
    -manualLowercase,਍    ⴀ洀愀渀甀愀氀唀瀀瀀攀爀挀愀猀攀Ⰰഀഀ
    -nodeName_,਍    ⴀ椀猀䄀爀爀愀礀䰀椀欀攀Ⰰഀഀ
    -forEach,਍    ⴀ猀漀爀琀攀搀䬀攀礀猀Ⰰഀഀ
    -forEachSorted,਍    ⴀ爀攀瘀攀爀猀攀倀愀爀愀洀猀Ⰰഀഀ
    -nextUid,਍    ⴀ猀攀琀䠀愀猀栀䬀攀礀Ⰰഀഀ
    -extend,਍    ⴀ椀渀琀Ⰰഀഀ
    -inherit,਍    ⴀ渀漀漀瀀Ⰰഀഀ
    -identity,਍    ⴀ瘀愀氀甀攀䘀渀Ⰰഀഀ
    -isUndefined,਍    ⴀ椀猀䐀攀昀椀渀攀搀Ⰰഀഀ
    -isObject,਍    ⴀ椀猀匀琀爀椀渀最Ⰰഀഀ
    -isNumber,਍    ⴀ椀猀䐀愀琀攀Ⰰഀഀ
    -isArray,਍    ⴀ椀猀䘀甀渀挀琀椀漀渀Ⰰഀഀ
    -isRegExp,਍    ⴀ椀猀圀椀渀搀漀眀Ⰰഀഀ
    -isScope,਍    ⴀ椀猀䘀椀氀攀Ⰰഀഀ
    -isBoolean,਍    ⴀ琀爀椀洀Ⰰഀഀ
    -isElement,਍    ⴀ洀愀欀攀䴀愀瀀Ⰰഀഀ
    -map,਍    ⴀ猀椀稀攀Ⰰഀഀ
    -includes,਍    ⴀ椀渀搀攀砀伀昀Ⰰഀഀ
    -arrayRemove,਍    ⴀ椀猀䰀攀愀昀一漀搀攀Ⰰഀഀ
    -copy,਍    ⴀ猀栀愀氀氀漀眀䌀漀瀀礀Ⰰഀഀ
    -equals,਍    ⴀ挀猀瀀Ⰰഀഀ
    -concat,਍    ⴀ猀氀椀挀攀䄀爀最猀Ⰰഀഀ
    -bind,਍    ⴀ琀漀䨀猀漀渀刀攀瀀氀愀挀攀爀Ⰰഀഀ
    -toJson,਍    ⴀ昀爀漀洀䨀猀漀渀Ⰰഀഀ
    -toBoolean,਍    ⴀ猀琀愀爀琀椀渀最吀愀最Ⰰഀഀ
    -tryDecodeURIComponent,਍    ⴀ瀀愀爀猀攀䬀攀礀嘀愀氀甀攀Ⰰഀഀ
    -toKeyValue,਍    ⴀ攀渀挀漀搀攀唀爀椀匀攀最洀攀渀琀Ⰰഀഀ
    -encodeUriQuery,਍    ⴀ愀渀最甀氀愀爀䤀渀椀琀Ⰰഀഀ
    -bootstrap,਍    ⴀ猀渀愀欀攀开挀愀猀攀Ⰰഀഀ
    -bindJQuery,਍    ⴀ愀猀猀攀爀琀䄀爀最Ⰰഀഀ
    -assertArgFn,਍    ⴀ愀猀猀攀爀琀一漀琀䠀愀猀伀眀渀倀爀漀瀀攀爀琀礀Ⰰഀഀ
    -getter,਍    ⴀ最攀琀䈀氀漀挀欀䔀氀攀洀攀渀琀猀Ⰰഀഀ
਍⨀⼀ഀഀ
਍⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀氀漀眀攀爀挀愀猀攀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description Converts the specified string to lowercase.਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 猀琀爀椀渀最 匀琀爀椀渀最 琀漀 戀攀 挀漀渀瘀攀爀琀攀搀 琀漀 氀漀眀攀爀挀愀猀攀⸀ഀഀ
 * @returns {string} Lowercased string.਍ ⨀⼀ഀഀ
var lowercase = function(string){return isString(string) ? string.toLowerCase() : string;};਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀甀瀀瀀攀爀挀愀猀攀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description Converts the specified string to uppercase.਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 猀琀爀椀渀最 匀琀爀椀渀最 琀漀 戀攀 挀漀渀瘀攀爀琀攀搀 琀漀 甀瀀瀀攀爀挀愀猀攀⸀ഀഀ
 * @returns {string} Uppercased string.਍ ⨀⼀ഀഀ
var uppercase = function(string){return isString(string) ? string.toUpperCase() : string;};਍ഀഀ
਍瘀愀爀 洀愀渀甀愀氀䰀漀眀攀爀挀愀猀攀 㴀 昀甀渀挀琀椀漀渀⠀猀⤀ 笀ഀഀ
  /* jshint bitwise: false */਍  爀攀琀甀爀渀 椀猀匀琀爀椀渀最⠀猀⤀ഀഀ
      ? s.replace(/[A-Z]/g, function(ch) {return String.fromCharCode(ch.charCodeAt(0) | 32);})਍      㨀 猀㬀ഀഀ
};਍瘀愀爀 洀愀渀甀愀氀唀瀀瀀攀爀挀愀猀攀 㴀 昀甀渀挀琀椀漀渀⠀猀⤀ 笀ഀഀ
  /* jshint bitwise: false */਍  爀攀琀甀爀渀 椀猀匀琀爀椀渀最⠀猀⤀ഀഀ
      ? s.replace(/[a-z]/g, function(ch) {return String.fromCharCode(ch.charCodeAt(0) & ~32);})਍      㨀 猀㬀ഀഀ
};਍ഀഀ
਍⼀⼀ 匀琀爀椀渀最⌀琀漀䰀漀眀攀爀䌀愀猀攀 愀渀搀 匀琀爀椀渀最⌀琀漀唀瀀瀀攀爀䌀愀猀攀 搀漀渀✀琀 瀀爀漀搀甀挀攀 挀漀爀爀攀挀琀 爀攀猀甀氀琀猀 椀渀 戀爀漀眀猀攀爀猀 眀椀琀栀 吀甀爀欀椀猀栀ഀഀ
// locale, for this reason we need to detect this case and redefine lowercase/uppercase methods਍⼀⼀ 眀椀琀栀 挀漀爀爀攀挀琀 戀甀琀 猀氀漀眀攀爀 愀氀琀攀爀渀愀琀椀瘀攀猀⸀ഀഀ
if ('i' !== 'I'.toLowerCase()) {਍  氀漀眀攀爀挀愀猀攀 㴀 洀愀渀甀愀氀䰀漀眀攀爀挀愀猀攀㬀ഀഀ
  uppercase = manualUppercase;਍紀ഀഀ
਍ഀഀ
var /** holds major version number for IE or NaN for real browsers */਍    洀猀椀攀Ⰰഀഀ
    jqLite,           // delay binding since jQuery could be loaded after us.਍    樀儀甀攀爀礀Ⰰ           ⼀⼀ 搀攀氀愀礀 戀椀渀搀椀渀最ഀഀ
    slice             = [].slice,਍    瀀甀猀栀              㴀 嬀崀⸀瀀甀猀栀Ⰰഀഀ
    toString          = Object.prototype.toString,਍    渀最䴀椀渀䔀爀爀          㴀 洀椀渀䔀爀爀⠀✀渀最✀⤀Ⰰഀഀ
਍ഀഀ
    _angular          = window.angular,਍    ⼀⨀⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀 ⨀⼀ഀഀ
    angular           = window.angular || (window.angular = {}),਍    愀渀最甀氀愀爀䴀漀搀甀氀攀Ⰰഀഀ
    nodeName_,਍    甀椀搀               㴀 嬀✀　✀Ⰰ ✀　✀Ⰰ ✀　✀崀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * IE 11 changed the format of the UserAgent string.਍ ⨀ 匀攀攀 栀琀琀瀀㨀⼀⼀洀猀搀渀⸀洀椀挀爀漀猀漀昀琀⸀挀漀洀⼀攀渀ⴀ甀猀⼀氀椀戀爀愀爀礀⼀洀猀㔀㌀㜀㔀　㌀⸀愀猀瀀砀ഀഀ
 */਍洀猀椀攀 㴀 椀渀琀⠀⠀⼀洀猀椀攀 ⠀尀搀⬀⤀⼀⸀攀砀攀挀⠀氀漀眀攀爀挀愀猀攀⠀渀愀瘀椀最愀琀漀爀⸀甀猀攀爀䄀最攀渀琀⤀⤀ 簀簀 嬀崀⤀嬀㄀崀⤀㬀ഀഀ
if (isNaN(msie)) {਍  洀猀椀攀 㴀 椀渀琀⠀⠀⼀琀爀椀搀攀渀琀尀⼀⸀⨀㬀 爀瘀㨀⠀尀搀⬀⤀⼀⸀攀砀攀挀⠀氀漀眀攀爀挀愀猀攀⠀渀愀瘀椀最愀琀漀爀⸀甀猀攀爀䄀最攀渀琀⤀⤀ 簀簀 嬀崀⤀嬀㄀崀⤀㬀ഀഀ
}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @private਍ ⨀ 䀀瀀愀爀愀洀 笀⨀紀 漀戀樀ഀഀ
 * @return {boolean} Returns true if `obj` is an array or array-like object (NodeList, Arguments,਍ ⨀                   匀琀爀椀渀最 ⸀⸀⸀⤀ഀഀ
 */਍昀甀渀挀琀椀漀渀 椀猀䄀爀爀愀礀䰀椀欀攀⠀漀戀樀⤀ 笀ഀഀ
  if (obj == null || isWindow(obj)) {਍    爀攀琀甀爀渀 昀愀氀猀攀㬀ഀഀ
  }਍ഀഀ
  var length = obj.length;਍ഀഀ
  if (obj.nodeType === 1 && length) {਍    爀攀琀甀爀渀 琀爀甀攀㬀ഀഀ
  }਍ഀഀ
  return isString(obj) || isArray(obj) || length === 0 ||਍         琀礀瀀攀漀昀 氀攀渀最琀栀 㴀㴀㴀 ✀渀甀洀戀攀爀✀ ☀☀ 氀攀渀最琀栀 㸀 　 ☀☀ ⠀氀攀渀最琀栀 ⴀ ㄀⤀ 椀渀 漀戀樀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
 * @name angular.forEach਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Invokes the `iterator` function once for each item in `obj` collection, which can be either an਍ ⨀ 漀戀樀攀挀琀 漀爀 愀渀 愀爀爀愀礀⸀ 吀栀攀 怀椀琀攀爀愀琀漀爀怀 昀甀渀挀琀椀漀渀 椀猀 椀渀瘀漀欀攀搀 眀椀琀栀 怀椀琀攀爀愀琀漀爀⠀瘀愀氀甀攀Ⰰ 欀攀礀⤀怀Ⰰ 眀栀攀爀攀 怀瘀愀氀甀攀怀ഀഀ
 * is the value of an object property or an array element and `key` is the object property key or਍ ⨀ 愀爀爀愀礀 攀氀攀洀攀渀琀 椀渀搀攀砀⸀ 匀瀀攀挀椀昀礀椀渀最 愀 怀挀漀渀琀攀砀琀怀 昀漀爀 琀栀攀 昀甀渀挀琀椀漀渀 椀猀 漀瀀琀椀漀渀愀氀⸀ഀഀ
 *਍ ⨀ 一漀琀攀㨀 琀栀椀猀 昀甀渀挀琀椀漀渀 眀愀猀 瀀爀攀瘀椀漀甀猀氀礀 欀渀漀眀渀 愀猀 怀愀渀最甀氀愀爀⸀昀漀爀攀愀挀栀怀⸀ഀഀ
 *਍   㰀瀀爀攀㸀ഀഀ
     var values = {name: 'misko', gender: 'male'};਍     瘀愀爀 氀漀最 㴀 嬀崀㬀ഀഀ
     angular.forEach(values, function(value, key){਍       琀栀椀猀⸀瀀甀猀栀⠀欀攀礀 ⬀ ✀㨀 ✀ ⬀ 瘀愀氀甀攀⤀㬀ഀഀ
     }, log);਍     攀砀瀀攀挀琀⠀氀漀最⤀⸀琀漀䔀焀甀愀氀⠀嬀✀渀愀洀攀㨀 洀椀猀欀漀✀Ⰰ ✀最攀渀搀攀爀㨀洀愀氀攀✀崀⤀㬀ഀഀ
   </pre>਍ ⨀ഀഀ
 * @param {Object|Array} obj Object to iterate over.਍ ⨀ 䀀瀀愀爀愀洀 笀䘀甀渀挀琀椀漀渀紀 椀琀攀爀愀琀漀爀 䤀琀攀爀愀琀漀爀 昀甀渀挀琀椀漀渀⸀ഀഀ
 * @param {Object=} context Object to become context (`this`) for the iterator function.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀伀戀樀攀挀琀簀䄀爀爀愀礀紀 刀攀昀攀爀攀渀挀攀 琀漀 怀漀戀樀怀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 昀漀爀䔀愀挀栀⠀漀戀樀Ⰰ 椀琀攀爀愀琀漀爀Ⰰ 挀漀渀琀攀砀琀⤀ 笀ഀഀ
  var key;਍  椀昀 ⠀漀戀樀⤀ 笀ഀഀ
    if (isFunction(obj)){਍      昀漀爀 ⠀欀攀礀 椀渀 漀戀樀⤀ 笀ഀഀ
        // Need to check if hasOwnProperty exists,਍        ⼀⼀ 愀猀 漀渀 䤀䔀㠀 琀栀攀 爀攀猀甀氀琀 漀昀 焀甀攀爀礀匀攀氀攀挀琀漀爀䄀氀氀 椀猀 愀渀 漀戀樀攀挀琀 眀椀琀栀漀甀琀 愀 栀愀猀伀眀渀倀爀漀瀀攀爀琀礀 昀甀渀挀琀椀漀渀ഀഀ
        if (key != 'prototype' && key != 'length' && key != 'name' && (!obj.hasOwnProperty || obj.hasOwnProperty(key))) {਍          椀琀攀爀愀琀漀爀⸀挀愀氀氀⠀挀漀渀琀攀砀琀Ⰰ 漀戀樀嬀欀攀礀崀Ⰰ 欀攀礀⤀㬀ഀഀ
        }਍      紀ഀഀ
    } else if (obj.forEach && obj.forEach !== forEach) {਍      漀戀樀⸀昀漀爀䔀愀挀栀⠀椀琀攀爀愀琀漀爀Ⰰ 挀漀渀琀攀砀琀⤀㬀ഀഀ
    } else if (isArrayLike(obj)) {਍      昀漀爀 ⠀欀攀礀 㴀 　㬀 欀攀礀 㰀 漀戀樀⸀氀攀渀最琀栀㬀 欀攀礀⬀⬀⤀ഀഀ
        iterator.call(context, obj[key], key);਍    紀 攀氀猀攀 笀ഀഀ
      for (key in obj) {਍        椀昀 ⠀漀戀樀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀欀攀礀⤀⤀ 笀ഀഀ
          iterator.call(context, obj[key], key);਍        紀ഀഀ
      }਍    紀ഀഀ
  }਍  爀攀琀甀爀渀 漀戀樀㬀ഀഀ
}਍ഀഀ
function sortedKeys(obj) {਍  瘀愀爀 欀攀礀猀 㴀 嬀崀㬀ഀഀ
  for (var key in obj) {਍    椀昀 ⠀漀戀樀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀欀攀礀⤀⤀ 笀ഀഀ
      keys.push(key);਍    紀ഀഀ
  }਍  爀攀琀甀爀渀 欀攀礀猀⸀猀漀爀琀⠀⤀㬀ഀഀ
}਍ഀഀ
function forEachSorted(obj, iterator, context) {਍  瘀愀爀 欀攀礀猀 㴀 猀漀爀琀攀搀䬀攀礀猀⠀漀戀樀⤀㬀ഀഀ
  for ( var i = 0; i < keys.length; i++) {਍    椀琀攀爀愀琀漀爀⸀挀愀氀氀⠀挀漀渀琀攀砀琀Ⰰ 漀戀樀嬀欀攀礀猀嬀椀崀崀Ⰰ 欀攀礀猀嬀椀崀⤀㬀ഀഀ
  }਍  爀攀琀甀爀渀 欀攀礀猀㬀ഀഀ
}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * when using forEach the params are value, key, but it is often useful to have key, value.਍ ⨀ 䀀瀀愀爀愀洀 笀昀甀渀挀琀椀漀渀⠀猀琀爀椀渀最Ⰰ ⨀⤀紀 椀琀攀爀愀琀漀爀䘀渀ഀഀ
 * @returns {function(*, string)}਍ ⨀⼀ഀഀ
function reverseParams(iteratorFn) {਍  爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀Ⰰ 欀攀礀⤀ 笀 椀琀攀爀愀琀漀爀䘀渀⠀欀攀礀Ⰰ 瘀愀氀甀攀⤀㬀 紀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䄀 挀漀渀猀椀猀琀攀渀琀 眀愀礀 漀昀 挀爀攀愀琀椀渀最 甀渀椀焀甀攀 䤀䐀猀 椀渀 愀渀最甀氀愀爀⸀ 吀栀攀 䤀䐀 椀猀 愀 猀攀焀甀攀渀挀攀 漀昀 愀氀瀀栀愀 渀甀洀攀爀椀挀ഀഀ
 * characters such as '012ABC'. The reason why we are not using simply a number counter is that਍ ⨀ 琀栀攀 渀甀洀戀攀爀 猀琀爀椀渀最 最攀琀猀 氀漀渀最攀爀 漀瘀攀爀 琀椀洀攀Ⰰ 愀渀搀 椀琀 挀愀渀 愀氀猀漀 漀瘀攀爀昀氀漀眀Ⰰ 眀栀攀爀攀 愀猀 琀栀攀 渀攀砀琀䤀搀ഀഀ
 * will grow much slower, it is a string, and it will never overflow.਍ ⨀ഀഀ
 * @returns an unique alpha-numeric string਍ ⨀⼀ഀഀ
function nextUid() {਍  瘀愀爀 椀渀搀攀砀 㴀 甀椀搀⸀氀攀渀最琀栀㬀ഀഀ
  var digit;਍ഀഀ
  while(index) {਍    椀渀搀攀砀ⴀⴀ㬀ഀഀ
    digit = uid[index].charCodeAt(0);਍    椀昀 ⠀搀椀最椀琀 㴀㴀 㔀㜀 ⼀⨀✀㤀✀⨀⼀⤀ 笀ഀഀ
      uid[index] = 'A';਍      爀攀琀甀爀渀 甀椀搀⸀樀漀椀渀⠀✀✀⤀㬀ഀഀ
    }਍    椀昀 ⠀搀椀最椀琀 㴀㴀 㤀　  ⼀⨀✀娀✀⨀⼀⤀ 笀ഀഀ
      uid[index] = '0';਍    紀 攀氀猀攀 笀ഀഀ
      uid[index] = String.fromCharCode(digit + 1);਍      爀攀琀甀爀渀 甀椀搀⸀樀漀椀渀⠀✀✀⤀㬀ഀഀ
    }਍  紀ഀഀ
  uid.unshift('0');਍  爀攀琀甀爀渀 甀椀搀⸀樀漀椀渀⠀✀✀⤀㬀ഀഀ
}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * Set or clear the hashkey for an object.਍ ⨀ 䀀瀀愀爀愀洀 漀戀樀 漀戀樀攀挀琀ഀഀ
 * @param h the hashkey (!truthy to delete the hashkey)਍ ⨀⼀ഀഀ
function setHashKey(obj, h) {਍  椀昀 ⠀栀⤀ 笀ഀഀ
    obj.$$hashKey = h;਍  紀ഀഀ
  else {਍    搀攀氀攀琀攀 漀戀樀⸀␀␀栀愀猀栀䬀攀礀㬀ഀഀ
  }਍紀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀攀砀琀攀渀搀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ 䔀砀琀攀渀搀猀 琀栀攀 搀攀猀琀椀渀愀琀椀漀渀 漀戀樀攀挀琀 怀搀猀琀怀 戀礀 挀漀瀀礀椀渀最 愀氀氀 漀昀 琀栀攀 瀀爀漀瀀攀爀琀椀攀猀 昀爀漀洀 琀栀攀 怀猀爀挀怀 漀戀樀攀挀琀⠀猀⤀ഀഀ
 * to `dst`. You can specify multiple `src` objects.਍ ⨀ഀഀ
 * @param {Object} dst Destination object.਍ ⨀ 䀀瀀愀爀愀洀 笀⸀⸀⸀伀戀樀攀挀琀紀 猀爀挀 匀漀甀爀挀攀 漀戀樀攀挀琀⠀猀⤀⸀ഀഀ
 * @returns {Object} Reference to `dst`.਍ ⨀⼀ഀഀ
function extend(dst) {਍  瘀愀爀 栀 㴀 搀猀琀⸀␀␀栀愀猀栀䬀攀礀㬀ഀഀ
  forEach(arguments, function(obj){਍    椀昀 ⠀漀戀樀 ℀㴀㴀 搀猀琀⤀ 笀ഀഀ
      forEach(obj, function(value, key){਍        搀猀琀嬀欀攀礀崀 㴀 瘀愀氀甀攀㬀ഀഀ
      });਍    紀ഀഀ
  });਍ഀഀ
  setHashKey(dst,h);਍  爀攀琀甀爀渀 搀猀琀㬀ഀഀ
}਍ഀഀ
function int(str) {਍  爀攀琀甀爀渀 瀀愀爀猀攀䤀渀琀⠀猀琀爀Ⰰ ㄀　⤀㬀ഀഀ
}਍ഀഀ
਍昀甀渀挀琀椀漀渀 椀渀栀攀爀椀琀⠀瀀愀爀攀渀琀Ⰰ 攀砀琀爀愀⤀ 笀ഀഀ
  return extend(new (extend(function() {}, {prototype:parent}))(), extra);਍紀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀渀漀漀瀀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ 䄀 昀甀渀挀琀椀漀渀 琀栀愀琀 瀀攀爀昀漀爀洀猀 渀漀 漀瀀攀爀愀琀椀漀渀猀⸀ 吀栀椀猀 昀甀渀挀琀椀漀渀 挀愀渀 戀攀 甀猀攀昀甀氀 眀栀攀渀 眀爀椀琀椀渀最 挀漀搀攀 椀渀 琀栀攀ഀഀ
 * functional style.਍   㰀瀀爀攀㸀ഀഀ
     function foo(callback) {਍       瘀愀爀 爀攀猀甀氀琀 㴀 挀愀氀挀甀氀愀琀攀刀攀猀甀氀琀⠀⤀㬀ഀഀ
       (callback || angular.noop)(result);਍     紀ഀഀ
   </pre>਍ ⨀⼀ഀഀ
function noop() {}਍渀漀漀瀀⸀␀椀渀樀攀挀琀 㴀 嬀崀㬀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
 * @name angular.identity਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * A function that returns its first argument. This function is useful when writing code in the਍ ⨀ 昀甀渀挀琀椀漀渀愀氀 猀琀礀氀攀⸀ഀഀ
 *਍   㰀瀀爀攀㸀ഀഀ
     function transformer(transformationFn, value) {਍       爀攀琀甀爀渀 ⠀琀爀愀渀猀昀漀爀洀愀琀椀漀渀䘀渀 簀簀 愀渀最甀氀愀爀⸀椀搀攀渀琀椀琀礀⤀⠀瘀愀氀甀攀⤀㬀ഀഀ
     };਍   㰀⼀瀀爀攀㸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 椀搀攀渀琀椀琀礀⠀␀⤀ 笀爀攀琀甀爀渀 ␀㬀紀ഀഀ
identity.$inject = [];਍ഀഀ
਍昀甀渀挀琀椀漀渀 瘀愀氀甀攀䘀渀⠀瘀愀氀甀攀⤀ 笀爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀⤀ 笀爀攀琀甀爀渀 瘀愀氀甀攀㬀紀㬀紀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀椀猀唀渀搀攀昀椀渀攀搀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ 䐀攀琀攀爀洀椀渀攀猀 椀昀 愀 爀攀昀攀爀攀渀挀攀 椀猀 甀渀搀攀昀椀渀攀搀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀⨀紀 瘀愀氀甀攀 刀攀昀攀爀攀渀挀攀 琀漀 挀栀攀挀欀⸀ഀഀ
 * @returns {boolean} True if `value` is undefined.਍ ⨀⼀ഀഀ
function isUndefined(value){return typeof value === 'undefined';}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀椀猀䐀攀昀椀渀攀搀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ 䐀攀琀攀爀洀椀渀攀猀 椀昀 愀 爀攀昀攀爀攀渀挀攀 椀猀 搀攀昀椀渀攀搀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀⨀紀 瘀愀氀甀攀 刀攀昀攀爀攀渀挀攀 琀漀 挀栀攀挀欀⸀ഀഀ
 * @returns {boolean} True if `value` is defined.਍ ⨀⼀ഀഀ
function isDefined(value){return typeof value !== 'undefined';}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀椀猀伀戀樀攀挀琀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ 䐀攀琀攀爀洀椀渀攀猀 椀昀 愀 爀攀昀攀爀攀渀挀攀 椀猀 愀渀 怀伀戀樀攀挀琀怀⸀ 唀渀氀椀欀攀 怀琀礀瀀攀漀昀怀 椀渀 䨀愀瘀愀匀挀爀椀瀀琀Ⰰ 怀渀甀氀氀怀猀 愀爀攀 渀漀琀ഀഀ
 * considered to be objects.਍ ⨀ഀഀ
 * @param {*} value Reference to check.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀戀漀漀氀攀愀渀紀 吀爀甀攀 椀昀 怀瘀愀氀甀攀怀 椀猀 愀渀 怀伀戀樀攀挀琀怀 戀甀琀 渀漀琀 怀渀甀氀氀怀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 椀猀伀戀樀攀挀琀⠀瘀愀氀甀攀⤀笀爀攀琀甀爀渀 瘀愀氀甀攀 ℀㴀 渀甀氀氀 ☀☀ 琀礀瀀攀漀昀 瘀愀氀甀攀 㴀㴀㴀 ✀漀戀樀攀挀琀✀㬀紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
 * @name angular.isString਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Determines if a reference is a `String`.਍ ⨀ഀഀ
 * @param {*} value Reference to check.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀戀漀漀氀攀愀渀紀 吀爀甀攀 椀昀 怀瘀愀氀甀攀怀 椀猀 愀 怀匀琀爀椀渀最怀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 椀猀匀琀爀椀渀最⠀瘀愀氀甀攀⤀笀爀攀琀甀爀渀 琀礀瀀攀漀昀 瘀愀氀甀攀 㴀㴀㴀 ✀猀琀爀椀渀最✀㬀紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
 * @name angular.isNumber਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Determines if a reference is a `Number`.਍ ⨀ഀഀ
 * @param {*} value Reference to check.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀戀漀漀氀攀愀渀紀 吀爀甀攀 椀昀 怀瘀愀氀甀攀怀 椀猀 愀 怀一甀洀戀攀爀怀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 椀猀一甀洀戀攀爀⠀瘀愀氀甀攀⤀笀爀攀琀甀爀渀 琀礀瀀攀漀昀 瘀愀氀甀攀 㴀㴀㴀 ✀渀甀洀戀攀爀✀㬀紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
 * @name angular.isDate਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Determines if a value is a date.਍ ⨀ഀഀ
 * @param {*} value Reference to check.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀戀漀漀氀攀愀渀紀 吀爀甀攀 椀昀 怀瘀愀氀甀攀怀 椀猀 愀 怀䐀愀琀攀怀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 椀猀䐀愀琀攀⠀瘀愀氀甀攀⤀笀ഀഀ
  return toString.call(value) === '[object Date]';਍紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
 * @name angular.isArray਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Determines if a reference is an `Array`.਍ ⨀ഀഀ
 * @param {*} value Reference to check.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀戀漀漀氀攀愀渀紀 吀爀甀攀 椀昀 怀瘀愀氀甀攀怀 椀猀 愀渀 怀䄀爀爀愀礀怀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 椀猀䄀爀爀愀礀⠀瘀愀氀甀攀⤀ 笀ഀഀ
  return toString.call(value) === '[object Array]';਍紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
 * @name angular.isFunction਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Determines if a reference is a `Function`.਍ ⨀ഀഀ
 * @param {*} value Reference to check.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀戀漀漀氀攀愀渀紀 吀爀甀攀 椀昀 怀瘀愀氀甀攀怀 椀猀 愀 怀䘀甀渀挀琀椀漀渀怀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 椀猀䘀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀笀爀攀琀甀爀渀 琀礀瀀攀漀昀 瘀愀氀甀攀 㴀㴀㴀 ✀昀甀渀挀琀椀漀渀✀㬀紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䐀攀琀攀爀洀椀渀攀猀 椀昀 愀 瘀愀氀甀攀 椀猀 愀 爀攀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 漀戀樀攀挀琀⸀ഀഀ
 *਍ ⨀ 䀀瀀爀椀瘀愀琀攀ഀഀ
 * @param {*} value Reference to check.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀戀漀漀氀攀愀渀紀 吀爀甀攀 椀昀 怀瘀愀氀甀攀怀 椀猀 愀 怀刀攀最䔀砀瀀怀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 椀猀刀攀最䔀砀瀀⠀瘀愀氀甀攀⤀ 笀ഀഀ
  return toString.call(value) === '[object RegExp]';਍紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䌀栀攀挀欀猀 椀昀 怀漀戀樀怀 椀猀 愀 眀椀渀搀漀眀 漀戀樀攀挀琀⸀ഀഀ
 *਍ ⨀ 䀀瀀爀椀瘀愀琀攀ഀഀ
 * @param {*} obj Object to check਍ ⨀ 䀀爀攀琀甀爀渀猀 笀戀漀漀氀攀愀渀紀 吀爀甀攀 椀昀 怀漀戀樀怀 椀猀 愀 眀椀渀搀漀眀 漀戀樀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 椀猀圀椀渀搀漀眀⠀漀戀樀⤀ 笀ഀഀ
  return obj && obj.document && obj.location && obj.alert && obj.setInterval;਍紀ഀഀ
਍ഀഀ
function isScope(obj) {਍  爀攀琀甀爀渀 漀戀樀 ☀☀ 漀戀樀⸀␀攀瘀愀氀䄀猀礀渀挀 ☀☀ 漀戀樀⸀␀眀愀琀挀栀㬀ഀഀ
}਍ഀഀ
਍昀甀渀挀琀椀漀渀 椀猀䘀椀氀攀⠀漀戀樀⤀ 笀ഀഀ
  return toString.call(obj) === '[object File]';਍紀ഀഀ
਍ഀഀ
function isBoolean(value) {਍  爀攀琀甀爀渀 琀礀瀀攀漀昀 瘀愀氀甀攀 㴀㴀㴀 ✀戀漀漀氀攀愀渀✀㬀ഀഀ
}਍ഀഀ
਍瘀愀爀 琀爀椀洀 㴀 ⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
  // native trim is way faster: http://jsperf.com/angular-trim-test਍  ⼀⼀ 戀甀琀 䤀䔀 搀漀攀猀渀✀琀 栀愀瘀攀 椀琀⸀⸀⸀ 㨀ⴀ⠀ഀഀ
  // TODO: we should move this into IE/ES5 polyfill਍  椀昀 ⠀℀匀琀爀椀渀最⸀瀀爀漀琀漀琀礀瀀攀⸀琀爀椀洀⤀ 笀ഀഀ
    return function(value) {਍      爀攀琀甀爀渀 椀猀匀琀爀椀渀最⠀瘀愀氀甀攀⤀ 㼀 瘀愀氀甀攀⸀爀攀瀀氀愀挀攀⠀⼀帀尀猀尀猀⨀⼀Ⰰ ✀✀⤀⸀爀攀瀀氀愀挀攀⠀⼀尀猀尀猀⨀␀⼀Ⰰ ✀✀⤀ 㨀 瘀愀氀甀攀㬀ഀഀ
    };਍  紀ഀഀ
  return function(value) {਍    爀攀琀甀爀渀 椀猀匀琀爀椀渀最⠀瘀愀氀甀攀⤀ 㼀 瘀愀氀甀攀⸀琀爀椀洀⠀⤀ 㨀 瘀愀氀甀攀㬀ഀഀ
  };਍紀⤀⠀⤀㬀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
 * @name angular.isElement਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Determines if a reference is a DOM element (or wrapped jQuery element).਍ ⨀ഀഀ
 * @param {*} value Reference to check.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀戀漀漀氀攀愀渀紀 吀爀甀攀 椀昀 怀瘀愀氀甀攀怀 椀猀 愀 䐀伀䴀 攀氀攀洀攀渀琀 ⠀漀爀 眀爀愀瀀瀀攀搀 樀儀甀攀爀礀 攀氀攀洀攀渀琀⤀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 椀猀䔀氀攀洀攀渀琀⠀渀漀搀攀⤀ 笀ഀഀ
  return !!(node &&਍    ⠀渀漀搀攀⸀渀漀搀攀一愀洀攀  ⼀⼀ 眀攀 愀爀攀 愀 搀椀爀攀挀琀 攀氀攀洀攀渀琀ഀഀ
    || (node.on && node.find)));  // we have an on and find method part of jQuery API਍紀ഀഀ
਍⼀⨀⨀ഀഀ
 * @param str 'key1,key2,...'਍ ⨀ 䀀爀攀琀甀爀渀猀 笀漀戀樀攀挀琀紀 椀渀 琀栀攀 昀漀爀洀 漀昀 笀欀攀礀㄀㨀琀爀甀攀Ⰰ 欀攀礀㈀㨀琀爀甀攀Ⰰ ⸀⸀⸀紀ഀഀ
 */਍昀甀渀挀琀椀漀渀 洀愀欀攀䴀愀瀀⠀猀琀爀⤀笀ഀഀ
  var obj = {}, items = str.split(","), i;਍  昀漀爀 ⠀ 椀 㴀 　㬀 椀 㰀 椀琀攀洀猀⸀氀攀渀最琀栀㬀 椀⬀⬀ ⤀ഀഀ
    obj[ items[i] ] = true;਍  爀攀琀甀爀渀 漀戀樀㬀ഀഀ
}਍ഀഀ
਍椀昀 ⠀洀猀椀攀 㰀 㤀⤀ 笀ഀഀ
  nodeName_ = function(element) {਍    攀氀攀洀攀渀琀 㴀 攀氀攀洀攀渀琀⸀渀漀搀攀一愀洀攀 㼀 攀氀攀洀攀渀琀 㨀 攀氀攀洀攀渀琀嬀　崀㬀ഀഀ
    return (element.scopeName && element.scopeName != 'HTML')਍      㼀 甀瀀瀀攀爀挀愀猀攀⠀攀氀攀洀攀渀琀⸀猀挀漀瀀攀一愀洀攀 ⬀ ✀㨀✀ ⬀ 攀氀攀洀攀渀琀⸀渀漀搀攀一愀洀攀⤀ 㨀 攀氀攀洀攀渀琀⸀渀漀搀攀一愀洀攀㬀ഀഀ
  };਍紀 攀氀猀攀 笀ഀഀ
  nodeName_ = function(element) {਍    爀攀琀甀爀渀 攀氀攀洀攀渀琀⸀渀漀搀攀一愀洀攀 㼀 攀氀攀洀攀渀琀⸀渀漀搀攀一愀洀攀 㨀 攀氀攀洀攀渀琀嬀　崀⸀渀漀搀攀一愀洀攀㬀ഀഀ
  };਍紀ഀഀ
਍ഀഀ
function map(obj, iterator, context) {਍  瘀愀爀 爀攀猀甀氀琀猀 㴀 嬀崀㬀ഀഀ
  forEach(obj, function(value, index, list) {਍    爀攀猀甀氀琀猀⸀瀀甀猀栀⠀椀琀攀爀愀琀漀爀⸀挀愀氀氀⠀挀漀渀琀攀砀琀Ⰰ 瘀愀氀甀攀Ⰰ 椀渀搀攀砀Ⰰ 氀椀猀琀⤀⤀㬀ഀഀ
  });਍  爀攀琀甀爀渀 爀攀猀甀氀琀猀㬀ഀഀ
}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @description਍ ⨀ 䐀攀琀攀爀洀椀渀攀猀 琀栀攀 渀甀洀戀攀爀 漀昀 攀氀攀洀攀渀琀猀 椀渀 愀渀 愀爀爀愀礀Ⰰ 琀栀攀 渀甀洀戀攀爀 漀昀 瀀爀漀瀀攀爀琀椀攀猀 愀渀 漀戀樀攀挀琀 栀愀猀Ⰰ 漀爀ഀഀ
 * the length of a string.਍ ⨀ഀഀ
 * Note: This function is used to augment the Object type in Angular expressions. See਍ ⨀ 笀䀀氀椀渀欀 愀渀最甀氀愀爀⸀伀戀樀攀挀琀紀 昀漀爀 洀漀爀攀 椀渀昀漀爀洀愀琀椀漀渀 愀戀漀甀琀 䄀渀最甀氀愀爀 愀爀爀愀礀猀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀伀戀樀攀挀琀簀䄀爀爀愀礀簀猀琀爀椀渀最紀 漀戀樀 伀戀樀攀挀琀Ⰰ 愀爀爀愀礀Ⰰ 漀爀 猀琀爀椀渀最 琀漀 椀渀猀瀀攀挀琀⸀ഀഀ
 * @param {boolean} [ownPropsOnly=false] Count only "own" properties in an object਍ ⨀ 䀀爀攀琀甀爀渀猀 笀渀甀洀戀攀爀紀 吀栀攀 猀椀稀攀 漀昀 怀漀戀樀怀 漀爀 怀　怀 椀昀 怀漀戀樀怀 椀猀 渀攀椀琀栀攀爀 愀渀 漀戀樀攀挀琀 渀漀爀 愀渀 愀爀爀愀礀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 猀椀稀攀⠀漀戀樀Ⰰ 漀眀渀倀爀漀瀀猀伀渀氀礀⤀ 笀ഀഀ
  var count = 0, key;਍ഀഀ
  if (isArray(obj) || isString(obj)) {਍    爀攀琀甀爀渀 漀戀樀⸀氀攀渀最琀栀㬀ഀഀ
  } else if (isObject(obj)){਍    昀漀爀 ⠀欀攀礀 椀渀 漀戀樀⤀ഀഀ
      if (!ownPropsOnly || obj.hasOwnProperty(key))਍        挀漀甀渀琀⬀⬀㬀ഀഀ
  }਍ഀഀ
  return count;਍紀ഀഀ
਍ഀഀ
function includes(array, obj) {਍  爀攀琀甀爀渀 椀渀搀攀砀伀昀⠀愀爀爀愀礀Ⰰ 漀戀樀⤀ ℀㴀 ⴀ㄀㬀ഀഀ
}਍ഀഀ
function indexOf(array, obj) {਍  椀昀 ⠀愀爀爀愀礀⸀椀渀搀攀砀伀昀⤀ 爀攀琀甀爀渀 愀爀爀愀礀⸀椀渀搀攀砀伀昀⠀漀戀樀⤀㬀ഀഀ
਍  昀漀爀 ⠀瘀愀爀 椀 㴀 　㬀 椀 㰀 愀爀爀愀礀⸀氀攀渀最琀栀㬀 椀⬀⬀⤀ 笀ഀഀ
    if (obj === array[i]) return i;਍  紀ഀഀ
  return -1;਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 愀爀爀愀礀刀攀洀漀瘀攀⠀愀爀爀愀礀Ⰰ 瘀愀氀甀攀⤀ 笀ഀഀ
  var index = indexOf(array, value);਍  椀昀 ⠀椀渀搀攀砀 㸀㴀　⤀ഀഀ
    array.splice(index, 1);਍  爀攀琀甀爀渀 瘀愀氀甀攀㬀ഀഀ
}਍ഀഀ
function isLeafNode (node) {਍  椀昀 ⠀渀漀搀攀⤀ 笀ഀഀ
    switch (node.nodeName) {਍    挀愀猀攀 ∀伀倀吀䤀伀一∀㨀ഀഀ
    case "PRE":਍    挀愀猀攀 ∀吀䤀吀䰀䔀∀㨀ഀഀ
      return true;਍    紀ഀഀ
  }਍  爀攀琀甀爀渀 昀愀氀猀攀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
 * @name angular.copy਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Creates a deep copy of `source`, which should be an object or an array.਍ ⨀ഀഀ
 * * If no destination is supplied, a copy of the object or array is created.਍ ⨀ ⨀ 䤀昀 愀 搀攀猀琀椀渀愀琀椀漀渀 椀猀 瀀爀漀瘀椀搀攀搀Ⰰ 愀氀氀 漀昀 椀琀猀 攀氀攀洀攀渀琀猀 ⠀昀漀爀 愀爀爀愀礀⤀ 漀爀 瀀爀漀瀀攀爀琀椀攀猀 ⠀昀漀爀 漀戀樀攀挀琀猀⤀ഀഀ
 *   are deleted and then all elements/properties from the source are copied to it.਍ ⨀ ⨀ 䤀昀 怀猀漀甀爀挀攀怀 椀猀 渀漀琀 愀渀 漀戀樀攀挀琀 漀爀 愀爀爀愀礀 ⠀椀渀挀⸀ 怀渀甀氀氀怀 愀渀搀 怀甀渀搀攀昀椀渀攀搀怀⤀Ⰰ 怀猀漀甀爀挀攀怀 椀猀 爀攀琀甀爀渀攀搀⸀ഀഀ
 * * If `source` is identical to 'destination' an exception will be thrown.਍ ⨀ഀഀ
 * @param {*} source The source that will be used to make a copy.਍ ⨀                   䌀愀渀 戀攀 愀渀礀 琀礀瀀攀Ⰰ 椀渀挀氀甀搀椀渀最 瀀爀椀洀椀琀椀瘀攀猀Ⰰ 怀渀甀氀氀怀Ⰰ 愀渀搀 怀甀渀搀攀昀椀渀攀搀怀⸀ഀഀ
 * @param {(Object|Array)=} destination Destination into which the source is copied. If਍ ⨀     瀀爀漀瘀椀搀攀搀Ⰰ 洀甀猀琀 戀攀 漀昀 琀栀攀 猀愀洀攀 琀礀瀀攀 愀猀 怀猀漀甀爀挀攀怀⸀ഀഀ
 * @returns {*} The copy or updated `destination`, if `destination` was specified.਍ ⨀ഀഀ
 * @example਍ 㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 <doc:source>਍ 㰀搀椀瘀 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀䌀漀渀琀爀漀氀氀攀爀∀㸀ഀഀ
 <form novalidate class="simple-form">਍ 一愀洀攀㨀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀琀攀砀琀∀ 渀最ⴀ洀漀搀攀氀㴀∀甀猀攀爀⸀渀愀洀攀∀ ⼀㸀㰀戀爀 ⼀㸀ഀഀ
 E-mail: <input type="email" ng-model="user.email" /><br />਍ 䜀攀渀搀攀爀㨀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀爀愀搀椀漀∀ 渀最ⴀ洀漀搀攀氀㴀∀甀猀攀爀⸀最攀渀搀攀爀∀ 瘀愀氀甀攀㴀∀洀愀氀攀∀ ⼀㸀洀愀氀攀ഀഀ
 <input type="radio" ng-model="user.gender" value="female" />female<br />਍ 㰀戀甀琀琀漀渀 渀最ⴀ挀氀椀挀欀㴀∀爀攀猀攀琀⠀⤀∀㸀刀䔀匀䔀吀㰀⼀戀甀琀琀漀渀㸀ഀഀ
 <button ng-click="update(user)">SAVE</button>਍ 㰀⼀昀漀爀洀㸀ഀഀ
 <pre>form = {{user | json}}</pre>਍ 㰀瀀爀攀㸀洀愀猀琀攀爀 㴀 笀笀洀愀猀琀攀爀 簀 樀猀漀渀紀紀㰀⼀瀀爀攀㸀ഀഀ
 </div>਍ഀഀ
 <script>਍ 昀甀渀挀琀椀漀渀 䌀漀渀琀爀漀氀氀攀爀⠀␀猀挀漀瀀攀⤀ 笀ഀഀ
    $scope.master= {};਍ഀഀ
    $scope.update = function(user) {਍      ⼀⼀ 䔀砀愀洀瀀氀攀 眀椀琀栀 ㄀ 愀爀最甀洀攀渀琀ഀഀ
      $scope.master= angular.copy(user);਍    紀㬀ഀഀ
਍    ␀猀挀漀瀀攀⸀爀攀猀攀琀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
      // Example with 2 arguments਍      愀渀最甀氀愀爀⸀挀漀瀀礀⠀␀猀挀漀瀀攀⸀洀愀猀琀攀爀Ⰰ ␀猀挀漀瀀攀⸀甀猀攀爀⤀㬀ഀഀ
    };਍ഀഀ
    $scope.reset();਍  紀ഀഀ
 </script>਍ 㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
 </doc:example>਍ ⨀⼀ഀഀ
function copy(source, destination){਍  椀昀 ⠀椀猀圀椀渀搀漀眀⠀猀漀甀爀挀攀⤀ 簀簀 椀猀匀挀漀瀀攀⠀猀漀甀爀挀攀⤀⤀ 笀ഀഀ
    throw ngMinErr('cpws',਍      ∀䌀愀渀✀琀 挀漀瀀礀℀ 䴀愀欀椀渀最 挀漀瀀椀攀猀 漀昀 圀椀渀搀漀眀 漀爀 匀挀漀瀀攀 椀渀猀琀愀渀挀攀猀 椀猀 渀漀琀 猀甀瀀瀀漀爀琀攀搀⸀∀⤀㬀ഀഀ
  }਍ഀഀ
  if (!destination) {਍    搀攀猀琀椀渀愀琀椀漀渀 㴀 猀漀甀爀挀攀㬀ഀഀ
    if (source) {਍      椀昀 ⠀椀猀䄀爀爀愀礀⠀猀漀甀爀挀攀⤀⤀ 笀ഀഀ
        destination = copy(source, []);਍      紀 攀氀猀攀 椀昀 ⠀椀猀䐀愀琀攀⠀猀漀甀爀挀攀⤀⤀ 笀ഀഀ
        destination = new Date(source.getTime());਍      紀 攀氀猀攀 椀昀 ⠀椀猀刀攀最䔀砀瀀⠀猀漀甀爀挀攀⤀⤀ 笀ഀഀ
        destination = new RegExp(source.source);਍      紀 攀氀猀攀 椀昀 ⠀椀猀伀戀樀攀挀琀⠀猀漀甀爀挀攀⤀⤀ 笀ഀഀ
        destination = copy(source, {});਍      紀ഀഀ
    }਍  紀 攀氀猀攀 笀ഀഀ
    if (source === destination) throw ngMinErr('cpi',਍      ∀䌀愀渀✀琀 挀漀瀀礀℀ 匀漀甀爀挀攀 愀渀搀 搀攀猀琀椀渀愀琀椀漀渀 愀爀攀 椀搀攀渀琀椀挀愀氀⸀∀⤀㬀ഀഀ
    if (isArray(source)) {਍      搀攀猀琀椀渀愀琀椀漀渀⸀氀攀渀最琀栀 㴀 　㬀ഀഀ
      for ( var i = 0; i < source.length; i++) {਍        搀攀猀琀椀渀愀琀椀漀渀⸀瀀甀猀栀⠀挀漀瀀礀⠀猀漀甀爀挀攀嬀椀崀⤀⤀㬀ഀഀ
      }਍    紀 攀氀猀攀 笀ഀഀ
      var h = destination.$$hashKey;਍      昀漀爀䔀愀挀栀⠀搀攀猀琀椀渀愀琀椀漀渀Ⰰ 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀Ⰰ 欀攀礀⤀笀ഀഀ
        delete destination[key];਍      紀⤀㬀ഀഀ
      for ( var key in source) {਍        搀攀猀琀椀渀愀琀椀漀渀嬀欀攀礀崀 㴀 挀漀瀀礀⠀猀漀甀爀挀攀嬀欀攀礀崀⤀㬀ഀഀ
      }਍      猀攀琀䠀愀猀栀䬀攀礀⠀搀攀猀琀椀渀愀琀椀漀渀Ⰰ栀⤀㬀ഀഀ
    }਍  紀ഀഀ
  return destination;਍紀ഀഀ
਍⼀⨀⨀ഀഀ
 * Create a shallow copy of an object਍ ⨀⼀ഀഀ
function shallowCopy(src, dst) {਍  搀猀琀 㴀 搀猀琀 簀簀 笀紀㬀ഀഀ
਍  昀漀爀⠀瘀愀爀 欀攀礀 椀渀 猀爀挀⤀ 笀ഀഀ
    // shallowCopy is only ever called by $compile nodeLinkFn, which has control over src਍    ⼀⼀ 猀漀 眀攀 搀漀渀✀琀 渀攀攀搀 琀漀 眀漀爀爀礀 愀戀漀甀琀 甀猀椀渀最 漀甀爀 挀甀猀琀漀洀 栀愀猀伀眀渀倀爀漀瀀攀爀琀礀 栀攀爀攀ഀഀ
    if (src.hasOwnProperty(key) && key.charAt(0) !== '$' && key.charAt(1) !== '$') {਍      搀猀琀嬀欀攀礀崀 㴀 猀爀挀嬀欀攀礀崀㬀ഀഀ
    }਍  紀ഀഀ
਍  爀攀琀甀爀渀 搀猀琀㬀ഀഀ
}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀攀焀甀愀氀猀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ 䐀攀琀攀爀洀椀渀攀猀 椀昀 琀眀漀 漀戀樀攀挀琀猀 漀爀 琀眀漀 瘀愀氀甀攀猀 愀爀攀 攀焀甀椀瘀愀氀攀渀琀⸀ 匀甀瀀瀀漀爀琀猀 瘀愀氀甀攀 琀礀瀀攀猀Ⰰ 爀攀最甀氀愀爀ഀഀ
 * expressions, arrays and objects.਍ ⨀ഀഀ
 * Two objects or values are considered equivalent if at least one of the following is true:਍ ⨀ഀഀ
 * * Both objects or values pass `===` comparison.਍ ⨀ ⨀ 䈀漀琀栀 漀戀樀攀挀琀猀 漀爀 瘀愀氀甀攀猀 愀爀攀 漀昀 琀栀攀 猀愀洀攀 琀礀瀀攀 愀渀搀 愀氀氀 漀昀 琀栀攀椀爀 瀀爀漀瀀攀爀琀椀攀猀 愀爀攀 攀焀甀愀氀 戀礀ഀഀ
 *   comparing them with `angular.equals`.਍ ⨀ ⨀ 䈀漀琀栀 瘀愀氀甀攀猀 愀爀攀 一愀一⸀ ⠀䤀渀 䨀愀瘀愀匀挀爀椀瀀琀Ⰰ 一愀一 㴀㴀 一愀一 㴀㸀 昀愀氀猀攀⸀ 䈀甀琀 眀攀 挀漀渀猀椀搀攀爀 琀眀漀 一愀一 愀猀 攀焀甀愀氀⤀ഀഀ
 * * Both values represent the same regular expression (In JavasScript,਍ ⨀   ⼀愀戀挀⼀ 㴀㴀 ⼀愀戀挀⼀ 㴀㸀 昀愀氀猀攀⸀ 䈀甀琀 眀攀 挀漀渀猀椀搀攀爀 琀眀漀 爀攀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀猀 愀猀 攀焀甀愀氀 眀栀攀渀 琀栀攀椀爀 琀攀砀琀甀愀氀ഀഀ
 *   representation matches).਍ ⨀ഀഀ
 * During a property comparison, properties of `function` type and properties with names਍ ⨀ 琀栀愀琀 戀攀最椀渀 眀椀琀栀 怀␀怀 愀爀攀 椀最渀漀爀攀搀⸀ഀഀ
 *਍ ⨀ 匀挀漀瀀攀 愀渀搀 䐀伀䴀圀椀渀搀漀眀 漀戀樀攀挀琀猀 愀爀攀 戀攀椀渀最 挀漀洀瀀愀爀攀搀 漀渀氀礀 戀礀 椀搀攀渀琀椀昀礀 ⠀怀㴀㴀㴀怀⤀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀⨀紀 漀㄀ 伀戀樀攀挀琀 漀爀 瘀愀氀甀攀 琀漀 挀漀洀瀀愀爀攀⸀ഀഀ
 * @param {*} o2 Object or value to compare.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀戀漀漀氀攀愀渀紀 吀爀甀攀 椀昀 愀爀最甀洀攀渀琀猀 愀爀攀 攀焀甀愀氀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 攀焀甀愀氀猀⠀漀㄀Ⰰ 漀㈀⤀ 笀ഀഀ
  if (o1 === o2) return true;਍  椀昀 ⠀漀㄀ 㴀㴀㴀 渀甀氀氀 簀簀 漀㈀ 㴀㴀㴀 渀甀氀氀⤀ 爀攀琀甀爀渀 昀愀氀猀攀㬀ഀഀ
  if (o1 !== o1 && o2 !== o2) return true; // NaN === NaN਍  瘀愀爀 琀㄀ 㴀 琀礀瀀攀漀昀 漀㄀Ⰰ 琀㈀ 㴀 琀礀瀀攀漀昀 漀㈀Ⰰ 氀攀渀最琀栀Ⰰ 欀攀礀Ⰰ 欀攀礀匀攀琀㬀ഀഀ
  if (t1 == t2) {਍    椀昀 ⠀琀㄀ 㴀㴀 ✀漀戀樀攀挀琀✀⤀ 笀ഀഀ
      if (isArray(o1)) {਍        椀昀 ⠀℀椀猀䄀爀爀愀礀⠀漀㈀⤀⤀ 爀攀琀甀爀渀 昀愀氀猀攀㬀ഀഀ
        if ((length = o1.length) == o2.length) {਍          昀漀爀⠀欀攀礀㴀　㬀 欀攀礀㰀氀攀渀最琀栀㬀 欀攀礀⬀⬀⤀ 笀ഀഀ
            if (!equals(o1[key], o2[key])) return false;਍          紀ഀഀ
          return true;਍        紀ഀഀ
      } else if (isDate(o1)) {਍        爀攀琀甀爀渀 椀猀䐀愀琀攀⠀漀㈀⤀ ☀☀ 漀㄀⸀最攀琀吀椀洀攀⠀⤀ 㴀㴀 漀㈀⸀最攀琀吀椀洀攀⠀⤀㬀ഀഀ
      } else if (isRegExp(o1) && isRegExp(o2)) {਍        爀攀琀甀爀渀 漀㄀⸀琀漀匀琀爀椀渀最⠀⤀ 㴀㴀 漀㈀⸀琀漀匀琀爀椀渀最⠀⤀㬀ഀഀ
      } else {਍        椀昀 ⠀椀猀匀挀漀瀀攀⠀漀㄀⤀ 簀簀 椀猀匀挀漀瀀攀⠀漀㈀⤀ 簀簀 椀猀圀椀渀搀漀眀⠀漀㄀⤀ 簀簀 椀猀圀椀渀搀漀眀⠀漀㈀⤀ 簀簀 椀猀䄀爀爀愀礀⠀漀㈀⤀⤀ 爀攀琀甀爀渀 昀愀氀猀攀㬀ഀഀ
        keySet = {};਍        昀漀爀⠀欀攀礀 椀渀 漀㄀⤀ 笀ഀഀ
          if (key.charAt(0) === '$' || isFunction(o1[key])) continue;਍          椀昀 ⠀℀攀焀甀愀氀猀⠀漀㄀嬀欀攀礀崀Ⰰ 漀㈀嬀欀攀礀崀⤀⤀ 爀攀琀甀爀渀 昀愀氀猀攀㬀ഀഀ
          keySet[key] = true;਍        紀ഀഀ
        for(key in o2) {਍          椀昀 ⠀℀欀攀礀匀攀琀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀欀攀礀⤀ ☀☀ഀഀ
              key.charAt(0) !== '$' &&਍              漀㈀嬀欀攀礀崀 ℀㴀㴀 甀渀搀攀昀椀渀攀搀 ☀☀ഀഀ
              !isFunction(o2[key])) return false;਍        紀ഀഀ
        return true;਍      紀ഀഀ
    }਍  紀ഀഀ
  return false;਍紀ഀഀ
਍ഀഀ
function csp() {਍  爀攀琀甀爀渀 ⠀搀漀挀甀洀攀渀琀⸀猀攀挀甀爀椀琀礀倀漀氀椀挀礀 ☀☀ 搀漀挀甀洀攀渀琀⸀猀攀挀甀爀椀琀礀倀漀氀椀挀礀⸀椀猀䄀挀琀椀瘀攀⤀ 簀簀ഀഀ
      (document.querySelector &&਍      ℀℀⠀搀漀挀甀洀攀渀琀⸀焀甀攀爀礀匀攀氀攀挀琀漀爀⠀✀嬀渀最ⴀ挀猀瀀崀✀⤀ 簀簀 搀漀挀甀洀攀渀琀⸀焀甀攀爀礀匀攀氀攀挀琀漀爀⠀✀嬀搀愀琀愀ⴀ渀最ⴀ挀猀瀀崀✀⤀⤀⤀㬀ഀഀ
}਍ഀഀ
਍昀甀渀挀琀椀漀渀 挀漀渀挀愀琀⠀愀爀爀愀礀㄀Ⰰ 愀爀爀愀礀㈀Ⰰ 椀渀搀攀砀⤀ 笀ഀഀ
  return array1.concat(slice.call(array2, index));਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 猀氀椀挀攀䄀爀最猀⠀愀爀最猀Ⰰ 猀琀愀爀琀䤀渀搀攀砀⤀ 笀ഀഀ
  return slice.call(args, startIndex || 0);਍紀ഀഀ
਍ഀഀ
/* jshint -W101 */਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀戀椀渀搀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ 刀攀琀甀爀渀猀 愀 昀甀渀挀琀椀漀渀 眀栀椀挀栀 挀愀氀氀猀 昀甀渀挀琀椀漀渀 怀昀渀怀 戀漀甀渀搀 琀漀 怀猀攀氀昀怀 ⠀怀猀攀氀昀怀 戀攀挀漀洀攀猀 琀栀攀 怀琀栀椀猀怀 昀漀爀ഀഀ
 * `fn`). You can supply optional `args` that are prebound to the function. This feature is also਍ ⨀ 欀渀漀眀渀 愀猀 嬀瀀愀爀琀椀愀氀 愀瀀瀀氀椀挀愀琀椀漀渀崀⠀栀琀琀瀀㨀⼀⼀攀渀⸀眀椀欀椀瀀攀搀椀愀⸀漀爀最⼀眀椀欀椀⼀倀愀爀琀椀愀氀开愀瀀瀀氀椀挀愀琀椀漀渀⤀Ⰰ 愀猀ഀഀ
 * distinguished from [function currying](http://en.wikipedia.org/wiki/Currying#Contrast_with_partial_function_application).਍ ⨀ഀഀ
 * @param {Object} self Context which `fn` should be evaluated in.਍ ⨀ 䀀瀀愀爀愀洀 笀昀甀渀挀琀椀漀渀⠀⤀紀 昀渀 䘀甀渀挀琀椀漀渀 琀漀 戀攀 戀漀甀渀搀⸀ഀഀ
 * @param {...*} args Optional arguments to be prebound to the `fn` function call.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀昀甀渀挀琀椀漀渀⠀⤀紀 䘀甀渀挀琀椀漀渀 琀栀愀琀 眀爀愀瀀猀 琀栀攀 怀昀渀怀 眀椀琀栀 愀氀氀 琀栀攀 猀瀀攀挀椀昀椀攀搀 戀椀渀搀椀渀最猀⸀ഀഀ
 */਍⼀⨀ 樀猀栀椀渀琀 ⬀圀㄀　㄀ ⨀⼀ഀഀ
function bind(self, fn) {਍  瘀愀爀 挀甀爀爀礀䄀爀最猀 㴀 愀爀最甀洀攀渀琀猀⸀氀攀渀最琀栀 㸀 ㈀ 㼀 猀氀椀挀攀䄀爀最猀⠀愀爀最甀洀攀渀琀猀Ⰰ ㈀⤀ 㨀 嬀崀㬀ഀഀ
  if (isFunction(fn) && !(fn instanceof RegExp)) {਍    爀攀琀甀爀渀 挀甀爀爀礀䄀爀最猀⸀氀攀渀最琀栀ഀഀ
      ? function() {਍          爀攀琀甀爀渀 愀爀最甀洀攀渀琀猀⸀氀攀渀最琀栀ഀഀ
            ? fn.apply(self, curryArgs.concat(slice.call(arguments, 0)))਍            㨀 昀渀⸀愀瀀瀀氀礀⠀猀攀氀昀Ⰰ 挀甀爀爀礀䄀爀最猀⤀㬀ഀഀ
        }਍      㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          return arguments.length਍            㼀 昀渀⸀愀瀀瀀氀礀⠀猀攀氀昀Ⰰ 愀爀最甀洀攀渀琀猀⤀ഀഀ
            : fn.call(self);਍        紀㬀ഀഀ
  } else {਍    ⼀⼀ 椀渀 䤀䔀Ⰰ 渀愀琀椀瘀攀 洀攀琀栀漀搀猀 愀爀攀 渀漀琀 昀甀渀挀琀椀漀渀猀 猀漀 琀栀攀礀 挀愀渀渀漀琀 戀攀 戀漀甀渀搀 ⠀渀漀琀攀㨀 琀栀攀礀 搀漀渀✀琀 渀攀攀搀 琀漀 戀攀⤀ഀഀ
    return fn;਍  紀ഀഀ
}਍ഀഀ
਍昀甀渀挀琀椀漀渀 琀漀䨀猀漀渀刀攀瀀氀愀挀攀爀⠀欀攀礀Ⰰ 瘀愀氀甀攀⤀ 笀ഀഀ
  var val = value;਍ഀഀ
  if (typeof key === 'string' && key.charAt(0) === '$') {਍    瘀愀氀 㴀 甀渀搀攀昀椀渀攀搀㬀ഀഀ
  } else if (isWindow(value)) {਍    瘀愀氀 㴀 ✀␀圀䤀一䐀伀圀✀㬀ഀഀ
  } else if (value &&  document === value) {਍    瘀愀氀 㴀 ✀␀䐀伀䌀唀䴀䔀一吀✀㬀ഀഀ
  } else if (isScope(value)) {਍    瘀愀氀 㴀 ✀␀匀䌀伀倀䔀✀㬀ഀഀ
  }਍ഀഀ
  return val;਍紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
 * @name angular.toJson਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Serializes input into a JSON-formatted string. Properties with leading $ characters will be਍ ⨀ 猀琀爀椀瀀瀀攀搀 猀椀渀挀攀 愀渀最甀氀愀爀 甀猀攀猀 琀栀椀猀 渀漀琀愀琀椀漀渀 椀渀琀攀爀渀愀氀氀礀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀伀戀樀攀挀琀簀䄀爀爀愀礀簀䐀愀琀攀簀猀琀爀椀渀最簀渀甀洀戀攀爀紀 漀戀樀 䤀渀瀀甀琀 琀漀 戀攀 猀攀爀椀愀氀椀稀攀搀 椀渀琀漀 䨀匀伀一⸀ഀഀ
 * @param {boolean=} pretty If set to true, the JSON output will contain newlines and whitespace.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀猀琀爀椀渀最簀甀渀搀攀昀椀渀攀搀紀 䨀匀伀一ⴀ椀昀椀攀搀 猀琀爀椀渀最 爀攀瀀爀攀猀攀渀琀椀渀最 怀漀戀樀怀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 琀漀䨀猀漀渀⠀漀戀樀Ⰰ 瀀爀攀琀琀礀⤀ 笀ഀഀ
  if (typeof obj === 'undefined') return undefined;਍  爀攀琀甀爀渀 䨀匀伀一⸀猀琀爀椀渀最椀昀礀⠀漀戀樀Ⰰ 琀漀䨀猀漀渀刀攀瀀氀愀挀攀爀Ⰰ 瀀爀攀琀琀礀 㼀 ✀  ✀ 㨀 渀甀氀氀⤀㬀ഀഀ
}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀昀爀漀洀䨀猀漀渀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ 䐀攀猀攀爀椀愀氀椀稀攀猀 愀 䨀匀伀一 猀琀爀椀渀最⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 樀猀漀渀 䨀匀伀一 猀琀爀椀渀最 琀漀 搀攀猀攀爀椀愀氀椀稀攀⸀ഀഀ
 * @returns {Object|Array|Date|string|number} Deserialized thingy.਍ ⨀⼀ഀഀ
function fromJson(json) {਍  爀攀琀甀爀渀 椀猀匀琀爀椀渀最⠀樀猀漀渀⤀ഀഀ
      ? JSON.parse(json)਍      㨀 樀猀漀渀㬀ഀഀ
}਍ഀഀ
਍昀甀渀挀琀椀漀渀 琀漀䈀漀漀氀攀愀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
  if (typeof value === 'function') {਍    瘀愀氀甀攀 㴀 琀爀甀攀㬀ഀഀ
  } else if (value && value.length !== 0) {਍    瘀愀爀 瘀 㴀 氀漀眀攀爀挀愀猀攀⠀∀∀ ⬀ 瘀愀氀甀攀⤀㬀ഀഀ
    value = !(v == 'f' || v == '0' || v == 'false' || v == 'no' || v == 'n' || v == '[]');਍  紀 攀氀猀攀 笀ഀഀ
    value = false;਍  紀ഀഀ
  return value;਍紀ഀഀ
਍⼀⨀⨀ഀഀ
 * @returns {string} Returns the string representation of the element.਍ ⨀⼀ഀഀ
function startingTag(element) {਍  攀氀攀洀攀渀琀 㴀 樀焀䰀椀琀攀⠀攀氀攀洀攀渀琀⤀⸀挀氀漀渀攀⠀⤀㬀ഀഀ
  try {਍    ⼀⼀ 琀甀爀渀猀 漀甀琀 䤀䔀 搀漀攀猀 渀漀琀 氀攀琀 礀漀甀 猀攀琀 ⸀栀琀洀氀⠀⤀ 漀渀 攀氀攀洀攀渀琀猀 眀栀椀挀栀ഀഀ
    // are not allowed to have children. So we just ignore it.਍    攀氀攀洀攀渀琀⸀攀洀瀀琀礀⠀⤀㬀ഀഀ
  } catch(e) {}਍  ⼀⼀ 䄀猀 倀攀爀 䐀伀䴀 匀琀愀渀搀愀爀搀猀ഀഀ
  var TEXT_NODE = 3;਍  瘀愀爀 攀氀攀洀䠀琀洀氀 㴀 樀焀䰀椀琀攀⠀✀㰀搀椀瘀㸀✀⤀⸀愀瀀瀀攀渀搀⠀攀氀攀洀攀渀琀⤀⸀栀琀洀氀⠀⤀㬀ഀഀ
  try {਍    爀攀琀甀爀渀 攀氀攀洀攀渀琀嬀　崀⸀渀漀搀攀吀礀瀀攀 㴀㴀㴀 吀䔀堀吀开一伀䐀䔀 㼀 氀漀眀攀爀挀愀猀攀⠀攀氀攀洀䠀琀洀氀⤀ 㨀ഀഀ
        elemHtml.਍          洀愀琀挀栀⠀⼀帀⠀㰀嬀帀㸀崀⬀㸀⤀⼀⤀嬀㄀崀⸀ഀഀ
          replace(/^<([\w\-]+)/, function(match, nodeName) { return '<' + lowercase(nodeName); });਍  紀 挀愀琀挀栀⠀攀⤀ 笀ഀഀ
    return lowercase(elemHtml);਍  紀ഀഀ
਍紀ഀഀ
਍ഀഀ
/////////////////////////////////////////////////਍ഀഀ
/**਍ ⨀ 吀爀椀攀猀 琀漀 搀攀挀漀搀攀 琀栀攀 唀刀䤀 挀漀洀瀀漀渀攀渀琀 眀椀琀栀漀甀琀 琀栀爀漀眀椀渀最 愀渀 攀砀挀攀瀀琀椀漀渀⸀ഀഀ
 *਍ ⨀ 䀀瀀爀椀瘀愀琀攀ഀഀ
 * @param str value potential URI component to check.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀戀漀漀氀攀愀渀紀 吀爀甀攀 椀昀 怀瘀愀氀甀攀怀 挀愀渀 戀攀 搀攀挀漀搀攀搀ഀഀ
 * with the decodeURIComponent function.਍ ⨀⼀ഀഀ
function tryDecodeURIComponent(value) {਍  琀爀礀 笀ഀഀ
    return decodeURIComponent(value);਍  紀 挀愀琀挀栀⠀攀⤀ 笀ഀഀ
    // Ignore any invalid uri component਍  紀ഀഀ
}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * Parses an escaped url query string into key-value pairs.਍ ⨀ 䀀爀攀琀甀爀渀猀 伀戀樀攀挀琀⸀㰀⠀猀琀爀椀渀最簀戀漀漀氀攀愀渀⤀㸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 瀀愀爀猀攀䬀攀礀嘀愀氀甀攀⠀⼀⨀⨀猀琀爀椀渀最⨀⼀欀攀礀嘀愀氀甀攀⤀ 笀ഀഀ
  var obj = {}, key_value, key;਍  昀漀爀䔀愀挀栀⠀⠀欀攀礀嘀愀氀甀攀 簀簀 ∀∀⤀⸀猀瀀氀椀琀⠀✀☀✀⤀Ⰰ 昀甀渀挀琀椀漀渀⠀欀攀礀嘀愀氀甀攀⤀笀ഀഀ
    if ( keyValue ) {਍      欀攀礀开瘀愀氀甀攀 㴀 欀攀礀嘀愀氀甀攀⸀猀瀀氀椀琀⠀✀㴀✀⤀㬀ഀഀ
      key = tryDecodeURIComponent(key_value[0]);਍      椀昀 ⠀ 椀猀䐀攀昀椀渀攀搀⠀欀攀礀⤀ ⤀ 笀ഀഀ
        var val = isDefined(key_value[1]) ? tryDecodeURIComponent(key_value[1]) : true;਍        椀昀 ⠀℀漀戀樀嬀欀攀礀崀⤀ 笀ഀഀ
          obj[key] = val;਍        紀 攀氀猀攀 椀昀⠀椀猀䄀爀爀愀礀⠀漀戀樀嬀欀攀礀崀⤀⤀ 笀ഀഀ
          obj[key].push(val);਍        紀 攀氀猀攀 笀ഀഀ
          obj[key] = [obj[key],val];਍        紀ഀഀ
      }਍    紀ഀഀ
  });਍  爀攀琀甀爀渀 漀戀樀㬀ഀഀ
}਍ഀഀ
function toKeyValue(obj) {਍  瘀愀爀 瀀愀爀琀猀 㴀 嬀崀㬀ഀഀ
  forEach(obj, function(value, key) {਍    椀昀 ⠀椀猀䄀爀爀愀礀⠀瘀愀氀甀攀⤀⤀ 笀ഀഀ
      forEach(value, function(arrayValue) {਍        瀀愀爀琀猀⸀瀀甀猀栀⠀攀渀挀漀搀攀唀爀椀儀甀攀爀礀⠀欀攀礀Ⰰ 琀爀甀攀⤀ ⬀ഀഀ
                   (arrayValue === true ? '' : '=' + encodeUriQuery(arrayValue, true)));਍      紀⤀㬀ഀഀ
    } else {਍    瀀愀爀琀猀⸀瀀甀猀栀⠀攀渀挀漀搀攀唀爀椀儀甀攀爀礀⠀欀攀礀Ⰰ 琀爀甀攀⤀ ⬀ഀഀ
               (value === true ? '' : '=' + encodeUriQuery(value, true)));਍    紀ഀഀ
  });਍  爀攀琀甀爀渀 瀀愀爀琀猀⸀氀攀渀最琀栀 㼀 瀀愀爀琀猀⸀樀漀椀渀⠀✀☀✀⤀ 㨀 ✀✀㬀ഀഀ
}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * We need our custom method because encodeURIComponent is too aggressive and doesn't follow਍ ⨀ 栀琀琀瀀㨀⼀⼀眀眀眀⸀椀攀琀昀⸀漀爀最⼀爀昀挀⼀爀昀挀㌀㤀㠀㘀⸀琀砀琀 眀椀琀栀 爀攀最愀爀搀猀 琀漀 琀栀攀 挀栀愀爀愀挀琀攀爀 猀攀琀 ⠀瀀挀栀愀爀⤀ 愀氀氀漀眀攀搀 椀渀 瀀愀琀栀ഀഀ
 * segments:਍ ⨀    猀攀最洀攀渀琀       㴀 ⨀瀀挀栀愀爀ഀഀ
 *    pchar         = unreserved / pct-encoded / sub-delims / ":" / "@"਍ ⨀    瀀挀琀ⴀ攀渀挀漀搀攀搀   㴀 ∀─∀ 䠀䔀堀䐀䤀䜀 䠀䔀堀䐀䤀䜀ഀഀ
 *    unreserved    = ALPHA / DIGIT / "-" / "." / "_" / "~"਍ ⨀    猀甀戀ⴀ搀攀氀椀洀猀    㴀 ∀℀∀ ⼀ ∀␀∀ ⼀ ∀☀∀ ⼀ ∀✀∀ ⼀ ∀⠀∀ ⼀ ∀⤀∀ഀഀ
 *                     / "*" / "+" / "," / ";" / "="਍ ⨀⼀ഀഀ
function encodeUriSegment(val) {਍  爀攀琀甀爀渀 攀渀挀漀搀攀唀爀椀儀甀攀爀礀⠀瘀愀氀Ⰰ 琀爀甀攀⤀⸀ഀഀ
             replace(/%26/gi, '&').਍             爀攀瀀氀愀挀攀⠀⼀─㌀䐀⼀最椀Ⰰ ✀㴀✀⤀⸀ഀഀ
             replace(/%2B/gi, '+');਍紀ഀഀ
਍ഀഀ
/**਍ ⨀ 吀栀椀猀 洀攀琀栀漀搀 椀猀 椀渀琀攀渀搀攀搀 昀漀爀 攀渀挀漀搀椀渀最 ⨀欀攀礀⨀ 漀爀 ⨀瘀愀氀甀攀⨀ 瀀愀爀琀猀 漀昀 焀甀攀爀礀 挀漀洀瀀漀渀攀渀琀⸀ 圀攀 渀攀攀搀 愀 挀甀猀琀漀洀ഀഀ
 * method because encodeURIComponent is too aggressive and encodes stuff that doesn't have to be਍ ⨀ 攀渀挀漀搀攀搀 瀀攀爀 栀琀琀瀀㨀⼀⼀琀漀漀氀猀⸀椀攀琀昀⸀漀爀最⼀栀琀洀氀⼀爀昀挀㌀㤀㠀㘀㨀ഀഀ
 *    query       = *( pchar / "/" / "?" )਍ ⨀    瀀挀栀愀爀         㴀 甀渀爀攀猀攀爀瘀攀搀 ⼀ 瀀挀琀ⴀ攀渀挀漀搀攀搀 ⼀ 猀甀戀ⴀ搀攀氀椀洀猀 ⼀ ∀㨀∀ ⼀ ∀䀀∀ഀഀ
 *    unreserved    = ALPHA / DIGIT / "-" / "." / "_" / "~"਍ ⨀    瀀挀琀ⴀ攀渀挀漀搀攀搀   㴀 ∀─∀ 䠀䔀堀䐀䤀䜀 䠀䔀堀䐀䤀䜀ഀഀ
 *    sub-delims    = "!" / "$" / "&" / "'" / "(" / ")"਍ ⨀                     ⼀ ∀⨀∀ ⼀ ∀⬀∀ ⼀ ∀Ⰰ∀ ⼀ ∀㬀∀ ⼀ ∀㴀∀ഀഀ
 */਍昀甀渀挀琀椀漀渀 攀渀挀漀搀攀唀爀椀儀甀攀爀礀⠀瘀愀氀Ⰰ 瀀挀琀䔀渀挀漀搀攀匀瀀愀挀攀猀⤀ 笀ഀഀ
  return encodeURIComponent(val).਍             爀攀瀀氀愀挀攀⠀⼀─㐀　⼀最椀Ⰰ ✀䀀✀⤀⸀ഀഀ
             replace(/%3A/gi, ':').਍             爀攀瀀氀愀挀攀⠀⼀─㈀㐀⼀最Ⰰ ✀␀✀⤀⸀ഀഀ
             replace(/%2C/gi, ',').਍             爀攀瀀氀愀挀攀⠀⼀─㈀　⼀最Ⰰ ⠀瀀挀琀䔀渀挀漀搀攀匀瀀愀挀攀猀 㼀 ✀─㈀　✀ 㨀 ✀⬀✀⤀⤀㬀ഀഀ
}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䄀瀀瀀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䄀一夀ഀഀ
 * @param {angular.Module} ngApp an optional application਍ ⨀   笀䀀氀椀渀欀 愀渀最甀氀愀爀⸀洀漀搀甀氀攀 洀漀搀甀氀攀紀 渀愀洀攀 琀漀 氀漀愀搀⸀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 *਍ ⨀ 唀猀攀 琀栀椀猀 搀椀爀攀挀琀椀瘀攀 琀漀 ⨀⨀愀甀琀漀ⴀ戀漀漀琀猀琀爀愀瀀⨀⨀ 愀渀 䄀渀最甀氀愀爀䨀匀 愀瀀瀀氀椀挀愀琀椀漀渀⸀ 吀栀攀 怀渀最䄀瀀瀀怀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * designates the **root element** of the application and is typically placed near the root element਍ ⨀ 漀昀 琀栀攀 瀀愀最攀 ⴀ 攀⸀最⸀ 漀渀 琀栀攀 怀㰀戀漀搀礀㸀怀 漀爀 怀㰀栀琀洀氀㸀怀 琀愀最猀⸀ഀഀ
 *਍ ⨀ 伀渀氀礀 漀渀攀 䄀渀最甀氀愀爀䨀匀 愀瀀瀀氀椀挀愀琀椀漀渀 挀愀渀 戀攀 愀甀琀漀ⴀ戀漀漀琀猀琀爀愀瀀瀀攀搀 瀀攀爀 䠀吀䴀䰀 搀漀挀甀洀攀渀琀⸀ 吀栀攀 昀椀爀猀琀 怀渀最䄀瀀瀀怀ഀഀ
 * found in the document will be used to define the root element to auto-bootstrap as an਍ ⨀ 愀瀀瀀氀椀挀愀琀椀漀渀⸀ 吀漀 爀甀渀 洀甀氀琀椀瀀氀攀 愀瀀瀀氀椀挀愀琀椀漀渀猀 椀渀 愀渀 䠀吀䴀䰀 搀漀挀甀洀攀渀琀 礀漀甀 洀甀猀琀 洀愀渀甀愀氀氀礀 戀漀漀琀猀琀爀愀瀀 琀栀攀洀 甀猀椀渀最ഀഀ
 * {@link angular.bootstrap} instead. AngularJS applications cannot be nested within each other.਍ ⨀ഀഀ
 * You can specify an **AngularJS module** to be used as the root module for the application.  This਍ ⨀ 洀漀搀甀氀攀 眀椀氀氀 戀攀 氀漀愀搀攀搀 椀渀琀漀 琀栀攀 笀䀀氀椀渀欀 䄀唀吀伀⸀␀椀渀樀攀挀琀漀爀紀 眀栀攀渀 琀栀攀 愀瀀瀀氀椀挀愀琀椀漀渀 椀猀 戀漀漀琀猀琀爀愀瀀瀀攀搀 愀渀搀ഀഀ
 * should contain the application code needed or have dependencies on other modules that will਍ ⨀ 挀漀渀琀愀椀渀 琀栀攀 挀漀搀攀⸀ 匀攀攀 笀䀀氀椀渀欀 愀渀最甀氀愀爀⸀洀漀搀甀氀攀紀 昀漀爀 洀漀爀攀 椀渀昀漀爀洀愀琀椀漀渀⸀ഀഀ
 *਍ ⨀ 䤀渀 琀栀攀 攀砀愀洀瀀氀攀 戀攀氀漀眀 椀昀 琀栀攀 怀渀最䄀瀀瀀怀 搀椀爀攀挀琀椀瘀攀 眀攀爀攀 渀漀琀 瀀氀愀挀攀搀 漀渀 琀栀攀 怀栀琀洀氀怀 攀氀攀洀攀渀琀 琀栀攀渀 琀栀攀ഀഀ
 * document would not be compiled, the `AppController` would not be instantiated and the `{{ a+b }}`਍ ⨀ 眀漀甀氀搀 渀漀琀 戀攀 爀攀猀漀氀瘀攀搀 琀漀 怀㌀怀⸀ഀഀ
 *਍ ⨀ 怀渀最䄀瀀瀀怀 椀猀 琀栀攀 攀愀猀椀攀猀琀Ⰰ 愀渀搀 洀漀猀琀 挀漀洀洀漀渀Ⰰ 眀愀礀 琀漀 戀漀漀琀猀琀爀愀瀀 愀渀 愀瀀瀀氀椀挀愀琀椀漀渀⸀ഀഀ
 *਍ 㰀攀砀愀洀瀀氀攀 洀漀搀甀氀攀㴀∀渀最䄀瀀瀀䐀攀洀漀∀㸀ഀഀ
   <file name="index.html">਍   㰀搀椀瘀 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀渀最䄀瀀瀀䐀攀洀漀䌀漀渀琀爀漀氀氀攀爀∀㸀ഀഀ
     I can add: {{a}} + {{b}} =  {{ a+b }}਍   㰀⼀昀椀氀攀㸀ഀഀ
   <file name="script.js">਍   愀渀最甀氀愀爀⸀洀漀搀甀氀攀⠀✀渀最䄀瀀瀀䐀攀洀漀✀Ⰰ 嬀崀⤀⸀挀漀渀琀爀漀氀氀攀爀⠀✀渀最䄀瀀瀀䐀攀洀漀䌀漀渀琀爀漀氀氀攀爀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀猀挀漀瀀攀⤀ 笀ഀഀ
     $scope.a = 1;਍     ␀猀挀漀瀀攀⸀戀 㴀 ㈀㬀ഀഀ
   });਍   㰀⼀昀椀氀攀㸀ഀഀ
 </example>਍ ⨀ഀഀ
 */਍昀甀渀挀琀椀漀渀 愀渀最甀氀愀爀䤀渀椀琀⠀攀氀攀洀攀渀琀Ⰰ 戀漀漀琀猀琀爀愀瀀⤀ 笀ഀഀ
  var elements = [element],਍      愀瀀瀀䔀氀攀洀攀渀琀Ⰰഀഀ
      module,਍      渀愀洀攀猀 㴀 嬀✀渀最㨀愀瀀瀀✀Ⰰ ✀渀最ⴀ愀瀀瀀✀Ⰰ ✀砀ⴀ渀最ⴀ愀瀀瀀✀Ⰰ ✀搀愀琀愀ⴀ渀最ⴀ愀瀀瀀✀崀Ⰰഀഀ
      NG_APP_CLASS_REGEXP = /\sng[:\-]app(:\s*([\w\d_]+);?)?\s/;਍ഀഀ
  function append(element) {਍    攀氀攀洀攀渀琀 ☀☀ 攀氀攀洀攀渀琀猀⸀瀀甀猀栀⠀攀氀攀洀攀渀琀⤀㬀ഀഀ
  }਍ഀഀ
  forEach(names, function(name) {਍    渀愀洀攀猀嬀渀愀洀攀崀 㴀 琀爀甀攀㬀ഀഀ
    append(document.getElementById(name));਍    渀愀洀攀 㴀 渀愀洀攀⸀爀攀瀀氀愀挀攀⠀✀㨀✀Ⰰ ✀尀尀㨀✀⤀㬀ഀഀ
    if (element.querySelectorAll) {਍      昀漀爀䔀愀挀栀⠀攀氀攀洀攀渀琀⸀焀甀攀爀礀匀攀氀攀挀琀漀爀䄀氀氀⠀✀⸀✀ ⬀ 渀愀洀攀⤀Ⰰ 愀瀀瀀攀渀搀⤀㬀ഀഀ
      forEach(element.querySelectorAll('.' + name + '\\:'), append);਍      昀漀爀䔀愀挀栀⠀攀氀攀洀攀渀琀⸀焀甀攀爀礀匀攀氀攀挀琀漀爀䄀氀氀⠀✀嬀✀ ⬀ 渀愀洀攀 ⬀ ✀崀✀⤀Ⰰ 愀瀀瀀攀渀搀⤀㬀ഀഀ
    }਍  紀⤀㬀ഀഀ
਍  昀漀爀䔀愀挀栀⠀攀氀攀洀攀渀琀猀Ⰰ 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀⤀ 笀ഀഀ
    if (!appElement) {਍      瘀愀爀 挀氀愀猀猀一愀洀攀 㴀 ✀ ✀ ⬀ 攀氀攀洀攀渀琀⸀挀氀愀猀猀一愀洀攀 ⬀ ✀ ✀㬀ഀഀ
      var match = NG_APP_CLASS_REGEXP.exec(className);਍      椀昀 ⠀洀愀琀挀栀⤀ 笀ഀഀ
        appElement = element;਍        洀漀搀甀氀攀 㴀 ⠀洀愀琀挀栀嬀㈀崀 簀簀 ✀✀⤀⸀爀攀瀀氀愀挀攀⠀⼀尀猀⬀⼀最Ⰰ ✀Ⰰ✀⤀㬀ഀഀ
      } else {਍        昀漀爀䔀愀挀栀⠀攀氀攀洀攀渀琀⸀愀琀琀爀椀戀甀琀攀猀Ⰰ 昀甀渀挀琀椀漀渀⠀愀琀琀爀⤀ 笀ഀഀ
          if (!appElement && names[attr.name]) {਍            愀瀀瀀䔀氀攀洀攀渀琀 㴀 攀氀攀洀攀渀琀㬀ഀഀ
            module = attr.value;਍          紀ഀഀ
        });਍      紀ഀഀ
    }਍  紀⤀㬀ഀഀ
  if (appElement) {਍    戀漀漀琀猀琀爀愀瀀⠀愀瀀瀀䔀氀攀洀攀渀琀Ⰰ 洀漀搀甀氀攀 㼀 嬀洀漀搀甀氀攀崀 㨀 嬀崀⤀㬀ഀഀ
  }਍紀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀戀漀漀琀猀琀爀愀瀀ഀഀ
 * @description਍ ⨀ 唀猀攀 琀栀椀猀 昀甀渀挀琀椀漀渀 琀漀 洀愀渀甀愀氀氀礀 猀琀愀爀琀 甀瀀 愀渀最甀氀愀爀 愀瀀瀀氀椀挀愀琀椀漀渀⸀ഀഀ
 *਍ ⨀ 匀攀攀㨀 笀䀀氀椀渀欀 最甀椀搀攀⼀戀漀漀琀猀琀爀愀瀀 䈀漀漀琀猀琀爀愀瀀紀ഀഀ
 *਍ ⨀ 一漀琀攀 琀栀愀琀 渀最匀挀攀渀愀爀椀漀ⴀ戀愀猀攀搀 攀渀搀ⴀ琀漀ⴀ攀渀搀 琀攀猀琀猀 挀愀渀渀漀琀 甀猀攀 琀栀椀猀 昀甀渀挀琀椀漀渀 琀漀 戀漀漀琀猀琀爀愀瀀 洀愀渀甀愀氀氀礀⸀ഀഀ
 * They must use {@link api/ng.directive:ngApp ngApp}.਍ ⨀ഀഀ
 * @param {Element} element DOM element which is the root of angular application.਍ ⨀ 䀀瀀愀爀愀洀 笀䄀爀爀愀礀㰀匀琀爀椀渀最簀䘀甀渀挀琀椀漀渀簀䄀爀爀愀礀㸀㴀紀 洀漀搀甀氀攀猀 愀渀 愀爀爀愀礀 漀昀 洀漀搀甀氀攀猀 琀漀 氀漀愀搀 椀渀琀漀 琀栀攀 愀瀀瀀氀椀挀愀琀椀漀渀⸀ഀഀ
 *     Each item in the array should be the name of a predefined module or a (DI annotated)਍ ⨀     昀甀渀挀琀椀漀渀 琀栀愀琀 眀椀氀氀 戀攀 椀渀瘀漀欀攀搀 戀礀 琀栀攀 椀渀樀攀挀琀漀爀 愀猀 愀 爀甀渀 戀氀漀挀欀⸀ഀഀ
 *     See: {@link angular.module modules}਍ ⨀ 䀀爀攀琀甀爀渀猀 笀䄀唀吀伀⸀␀椀渀樀攀挀琀漀爀紀 刀攀琀甀爀渀猀 琀栀攀 渀攀眀氀礀 挀爀攀愀琀攀搀 椀渀樀攀挀琀漀爀 昀漀爀 琀栀椀猀 愀瀀瀀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 戀漀漀琀猀琀爀愀瀀⠀攀氀攀洀攀渀琀Ⰰ 洀漀搀甀氀攀猀⤀ 笀ഀഀ
  var doBootstrap = function() {਍    攀氀攀洀攀渀琀 㴀 樀焀䰀椀琀攀⠀攀氀攀洀攀渀琀⤀㬀ഀഀ
਍    椀昀 ⠀攀氀攀洀攀渀琀⸀椀渀樀攀挀琀漀爀⠀⤀⤀ 笀ഀഀ
      var tag = (element[0] === document) ? 'document' : startingTag(element);਍      琀栀爀漀眀 渀最䴀椀渀䔀爀爀⠀✀戀琀猀琀爀瀀搀✀Ⰰ ∀䄀瀀瀀 䄀氀爀攀愀搀礀 䈀漀漀琀猀琀爀愀瀀瀀攀搀 眀椀琀栀 琀栀椀猀 䔀氀攀洀攀渀琀 ✀笀　紀✀∀Ⰰ 琀愀最⤀㬀ഀഀ
    }਍ഀഀ
    modules = modules || [];਍    洀漀搀甀氀攀猀⸀甀渀猀栀椀昀琀⠀嬀✀␀瀀爀漀瘀椀搀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀瀀爀漀瘀椀搀攀⤀ 笀ഀഀ
      $provide.value('$rootElement', element);਍    紀崀⤀㬀ഀഀ
    modules.unshift('ng');਍    瘀愀爀 椀渀樀攀挀琀漀爀 㴀 挀爀攀愀琀攀䤀渀樀攀挀琀漀爀⠀洀漀搀甀氀攀猀⤀㬀ഀഀ
    injector.invoke(['$rootScope', '$rootElement', '$compile', '$injector', '$animate',਍       昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 挀漀洀瀀椀氀攀Ⰰ 椀渀樀攀挀琀漀爀Ⰰ 愀渀椀洀愀琀攀⤀ 笀ഀഀ
        scope.$apply(function() {਍          攀氀攀洀攀渀琀⸀搀愀琀愀⠀✀␀椀渀樀攀挀琀漀爀✀Ⰰ 椀渀樀攀挀琀漀爀⤀㬀ഀഀ
          compile(element)(scope);਍        紀⤀㬀ഀഀ
      }]਍    ⤀㬀ഀഀ
    return injector;਍  紀㬀ഀഀ
਍  瘀愀爀 一䜀开䐀䔀䘀䔀刀开䈀伀伀吀匀吀刀䄀倀 㴀 ⼀帀一䜀开䐀䔀䘀䔀刀开䈀伀伀吀匀吀刀䄀倀℀⼀㬀ഀഀ
਍  椀昀 ⠀眀椀渀搀漀眀 ☀☀ ℀一䜀开䐀䔀䘀䔀刀开䈀伀伀吀匀吀刀䄀倀⸀琀攀猀琀⠀眀椀渀搀漀眀⸀渀愀洀攀⤀⤀ 笀ഀഀ
    return doBootstrap();਍  紀ഀഀ
਍  眀椀渀搀漀眀⸀渀愀洀攀 㴀 眀椀渀搀漀眀⸀渀愀洀攀⸀爀攀瀀氀愀挀攀⠀一䜀开䐀䔀䘀䔀刀开䈀伀伀吀匀吀刀䄀倀Ⰰ ✀✀⤀㬀ഀഀ
  angular.resumeBootstrap = function(extraModules) {਍    昀漀爀䔀愀挀栀⠀攀砀琀爀愀䴀漀搀甀氀攀猀Ⰰ 昀甀渀挀琀椀漀渀⠀洀漀搀甀氀攀⤀ 笀ഀഀ
      modules.push(module);਍    紀⤀㬀ഀഀ
    doBootstrap();਍  紀㬀ഀഀ
}਍ഀഀ
var SNAKE_CASE_REGEXP = /[A-Z]/g;਍昀甀渀挀琀椀漀渀 猀渀愀欀攀开挀愀猀攀⠀渀愀洀攀Ⰰ 猀攀瀀愀爀愀琀漀爀⤀笀ഀഀ
  separator = separator || '_';਍  爀攀琀甀爀渀 渀愀洀攀⸀爀攀瀀氀愀挀攀⠀匀一䄀䬀䔀开䌀䄀匀䔀开刀䔀䜀䔀堀倀Ⰰ 昀甀渀挀琀椀漀渀⠀氀攀琀琀攀爀Ⰰ 瀀漀猀⤀ 笀ഀഀ
    return (pos ? separator : '') + letter.toLowerCase();਍  紀⤀㬀ഀഀ
}਍ഀഀ
function bindJQuery() {਍  ⼀⼀ 戀椀渀搀 琀漀 樀儀甀攀爀礀 椀昀 瀀爀攀猀攀渀琀㬀ഀഀ
  jQuery = window.jQuery;਍  ⼀⼀ 爀攀猀攀琀 琀漀 樀儀甀攀爀礀 漀爀 搀攀昀愀甀氀琀 琀漀 甀猀⸀ഀഀ
  if (jQuery) {਍    樀焀䰀椀琀攀 㴀 樀儀甀攀爀礀㬀ഀഀ
    extend(jQuery.fn, {਍      猀挀漀瀀攀㨀 䨀儀䰀椀琀攀倀爀漀琀漀琀礀瀀攀⸀猀挀漀瀀攀Ⰰഀഀ
      isolateScope: JQLitePrototype.isolateScope,਍      挀漀渀琀爀漀氀氀攀爀㨀 䨀儀䰀椀琀攀倀爀漀琀漀琀礀瀀攀⸀挀漀渀琀爀漀氀氀攀爀Ⰰഀഀ
      injector: JQLitePrototype.injector,਍      椀渀栀攀爀椀琀攀搀䐀愀琀愀㨀 䨀儀䰀椀琀攀倀爀漀琀漀琀礀瀀攀⸀椀渀栀攀爀椀琀攀搀䐀愀琀愀ഀഀ
    });਍    ⼀⼀ 䴀攀琀栀漀搀 猀椀最渀愀琀甀爀攀㨀ഀഀ
    //     jqLitePatchJQueryRemove(name, dispatchThis, filterElems, getterIfNoArguments)਍    樀焀䰀椀琀攀倀愀琀挀栀䨀儀甀攀爀礀刀攀洀漀瘀攀⠀✀爀攀洀漀瘀攀✀Ⰰ 琀爀甀攀Ⰰ 琀爀甀攀Ⰰ 昀愀氀猀攀⤀㬀ഀഀ
    jqLitePatchJQueryRemove('empty', false, false, false);਍    樀焀䰀椀琀攀倀愀琀挀栀䨀儀甀攀爀礀刀攀洀漀瘀攀⠀✀栀琀洀氀✀Ⰰ 昀愀氀猀攀Ⰰ 昀愀氀猀攀Ⰰ 琀爀甀攀⤀㬀ഀഀ
  } else {਍    樀焀䰀椀琀攀 㴀 䨀儀䰀椀琀攀㬀ഀഀ
  }਍  愀渀最甀氀愀爀⸀攀氀攀洀攀渀琀 㴀 樀焀䰀椀琀攀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 琀栀爀漀眀 攀爀爀漀爀 椀昀 琀栀攀 愀爀最甀洀攀渀琀 椀猀 昀愀氀猀礀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 愀猀猀攀爀琀䄀爀最⠀愀爀最Ⰰ 渀愀洀攀Ⰰ 爀攀愀猀漀渀⤀ 笀ഀഀ
  if (!arg) {਍    琀栀爀漀眀 渀最䴀椀渀䔀爀爀⠀✀愀爀攀焀✀Ⰰ ∀䄀爀最甀洀攀渀琀 ✀笀　紀✀ 椀猀 笀㄀紀∀Ⰰ ⠀渀愀洀攀 簀簀 ✀㼀✀⤀Ⰰ ⠀爀攀愀猀漀渀 簀簀 ∀爀攀焀甀椀爀攀搀∀⤀⤀㬀ഀഀ
  }਍  爀攀琀甀爀渀 愀爀最㬀ഀഀ
}਍ഀഀ
function assertArgFn(arg, name, acceptArrayAnnotation) {਍  椀昀 ⠀愀挀挀攀瀀琀䄀爀爀愀礀䄀渀渀漀琀愀琀椀漀渀 ☀☀ 椀猀䄀爀爀愀礀⠀愀爀最⤀⤀ 笀ഀഀ
      arg = arg[arg.length - 1];਍  紀ഀഀ
਍  愀猀猀攀爀琀䄀爀最⠀椀猀䘀甀渀挀琀椀漀渀⠀愀爀最⤀Ⰰ 渀愀洀攀Ⰰ ✀渀漀琀 愀 昀甀渀挀琀椀漀渀Ⰰ 最漀琀 ✀ ⬀ഀഀ
      (arg && typeof arg == 'object' ? arg.constructor.name || 'Object' : typeof arg));਍  爀攀琀甀爀渀 愀爀最㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 琀栀爀漀眀 攀爀爀漀爀 椀昀 琀栀攀 渀愀洀攀 最椀瘀攀渀 椀猀 栀愀猀伀眀渀倀爀漀瀀攀爀琀礀ഀഀ
 * @param  {String} name    the name to test਍ ⨀ 䀀瀀愀爀愀洀  笀匀琀爀椀渀最紀 挀漀渀琀攀砀琀 琀栀攀 挀漀渀琀攀砀琀 椀渀 眀栀椀挀栀 琀栀攀 渀愀洀攀 椀猀 甀猀攀搀Ⰰ 猀甀挀栀 愀猀 洀漀搀甀氀攀 漀爀 搀椀爀攀挀琀椀瘀攀ഀഀ
 */਍昀甀渀挀琀椀漀渀 愀猀猀攀爀琀一漀琀䠀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀渀愀洀攀Ⰰ 挀漀渀琀攀砀琀⤀ 笀ഀഀ
  if (name === 'hasOwnProperty') {਍    琀栀爀漀眀 渀最䴀椀渀䔀爀爀⠀✀戀愀搀渀愀洀攀✀Ⰰ ∀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀 椀猀 渀漀琀 愀 瘀愀氀椀搀 笀　紀 渀愀洀攀∀Ⰰ 挀漀渀琀攀砀琀⤀㬀ഀഀ
  }਍紀ഀഀ
਍⼀⨀⨀ഀഀ
 * Return the value accessible from the object by path. Any undefined traversals are ignored਍ ⨀ 䀀瀀愀爀愀洀 笀伀戀樀攀挀琀紀 漀戀樀 猀琀愀爀琀椀渀最 漀戀樀攀挀琀ഀഀ
 * @param {string} path path to traverse਍ ⨀ 䀀瀀愀爀愀洀 笀戀漀漀氀攀愀渀㴀琀爀甀攀紀 戀椀渀搀䘀渀吀漀匀挀漀瀀攀ഀഀ
 * @returns value as accessible by path਍ ⨀⼀ഀഀ
//TODO(misko): this function needs to be removed਍昀甀渀挀琀椀漀渀 最攀琀琀攀爀⠀漀戀樀Ⰰ 瀀愀琀栀Ⰰ 戀椀渀搀䘀渀吀漀匀挀漀瀀攀⤀ 笀ഀഀ
  if (!path) return obj;਍  瘀愀爀 欀攀礀猀 㴀 瀀愀琀栀⸀猀瀀氀椀琀⠀✀⸀✀⤀㬀ഀഀ
  var key;਍  瘀愀爀 氀愀猀琀䤀渀猀琀愀渀挀攀 㴀 漀戀樀㬀ഀഀ
  var len = keys.length;਍ഀഀ
  for (var i = 0; i < len; i++) {਍    欀攀礀 㴀 欀攀礀猀嬀椀崀㬀ഀഀ
    if (obj) {਍      漀戀樀 㴀 ⠀氀愀猀琀䤀渀猀琀愀渀挀攀 㴀 漀戀樀⤀嬀欀攀礀崀㬀ഀഀ
    }਍  紀ഀഀ
  if (!bindFnToScope && isFunction(obj)) {਍    爀攀琀甀爀渀 戀椀渀搀⠀氀愀猀琀䤀渀猀琀愀渀挀攀Ⰰ 漀戀樀⤀㬀ഀഀ
  }਍  爀攀琀甀爀渀 漀戀樀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 刀攀琀甀爀渀 琀栀攀 䐀伀䴀 猀椀戀氀椀渀最猀 戀攀琀眀攀攀渀 琀栀攀 昀椀爀猀琀 愀渀搀 氀愀猀琀 渀漀搀攀 椀渀 琀栀攀 最椀瘀攀渀 愀爀爀愀礀⸀ഀഀ
 * @param {Array} array like object਍ ⨀ 䀀爀攀琀甀爀渀猀 樀儀氀椀琀攀 漀戀樀攀挀琀 挀漀渀琀愀椀渀椀渀最 琀栀攀 攀氀攀洀攀渀琀猀ഀഀ
 */਍昀甀渀挀琀椀漀渀 最攀琀䈀氀漀挀欀䔀氀攀洀攀渀琀猀⠀渀漀搀攀猀⤀ 笀ഀഀ
  var startNode = nodes[0],਍      攀渀搀一漀搀攀 㴀 渀漀搀攀猀嬀渀漀搀攀猀⸀氀攀渀最琀栀 ⴀ ㄀崀㬀ഀഀ
  if (startNode === endNode) {਍    爀攀琀甀爀渀 樀焀䰀椀琀攀⠀猀琀愀爀琀一漀搀攀⤀㬀ഀഀ
  }਍ഀഀ
  var element = startNode;਍  瘀愀爀 攀氀攀洀攀渀琀猀 㴀 嬀攀氀攀洀攀渀琀崀㬀ഀഀ
਍  搀漀 笀ഀഀ
    element = element.nextSibling;਍    椀昀 ⠀℀攀氀攀洀攀渀琀⤀ 戀爀攀愀欀㬀ഀഀ
    elements.push(element);਍  紀 眀栀椀氀攀 ⠀攀氀攀洀攀渀琀 ℀㴀㴀 攀渀搀一漀搀攀⤀㬀ഀഀ
਍  爀攀琀甀爀渀 樀焀䰀椀琀攀⠀攀氀攀洀攀渀琀猀⤀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 椀渀琀攀爀昀愀挀攀ഀഀ
 * @name angular.Module਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 *਍ ⨀ 䤀渀琀攀爀昀愀挀攀 昀漀爀 挀漀渀昀椀最甀爀椀渀最 愀渀最甀氀愀爀 笀䀀氀椀渀欀 愀渀最甀氀愀爀⸀洀漀搀甀氀攀 洀漀搀甀氀攀猀紀⸀ഀഀ
 */਍ഀഀ
function setupModuleLoader(window) {਍ഀഀ
  var $injectorMinErr = minErr('$injector');਍  瘀愀爀 渀最䴀椀渀䔀爀爀 㴀 洀椀渀䔀爀爀⠀✀渀最✀⤀㬀ഀഀ
਍  昀甀渀挀琀椀漀渀 攀渀猀甀爀攀⠀漀戀樀Ⰰ 渀愀洀攀Ⰰ 昀愀挀琀漀爀礀⤀ 笀ഀഀ
    return obj[name] || (obj[name] = factory());਍  紀ഀഀ
਍  瘀愀爀 愀渀最甀氀愀爀 㴀 攀渀猀甀爀攀⠀眀椀渀搀漀眀Ⰰ ✀愀渀最甀氀愀爀✀Ⰰ 伀戀樀攀挀琀⤀㬀ഀഀ
਍  ⼀⼀ 圀攀 渀攀攀搀 琀漀 攀砀瀀漀猀攀 怀愀渀最甀氀愀爀⸀␀␀洀椀渀䔀爀爀怀 琀漀 洀漀搀甀氀攀猀 猀甀挀栀 愀猀 怀渀最刀攀猀漀甀爀挀攀怀 琀栀愀琀 爀攀昀攀爀攀渀挀攀 椀琀 搀甀爀椀渀最 戀漀漀琀猀琀爀愀瀀ഀഀ
  angular.$$minErr = angular.$$minErr || minErr;਍ഀഀ
  return ensure(angular, 'module', function() {਍    ⼀⨀⨀ 䀀琀礀瀀攀 笀伀戀樀攀挀琀⸀㰀猀琀爀椀渀最Ⰰ 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀㸀紀 ⨀⼀ഀഀ
    var modules = {};਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
     * @name angular.module਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     *਍     ⨀ 吀栀攀 怀愀渀最甀氀愀爀⸀洀漀搀甀氀攀怀 椀猀 愀 最氀漀戀愀氀 瀀氀愀挀攀 昀漀爀 挀爀攀愀琀椀渀最Ⰰ 爀攀最椀猀琀攀爀椀渀最 愀渀搀 爀攀琀爀椀攀瘀椀渀最 䄀渀最甀氀愀爀ഀഀ
     * modules.਍     ⨀ 䄀氀氀 洀漀搀甀氀攀猀 ⠀愀渀最甀氀愀爀 挀漀爀攀 漀爀 ㌀爀搀 瀀愀爀琀礀⤀ 琀栀愀琀 猀栀漀甀氀搀 戀攀 愀瘀愀椀氀愀戀氀攀 琀漀 愀渀 愀瀀瀀氀椀挀愀琀椀漀渀 洀甀猀琀 戀攀ഀഀ
     * registered using this mechanism.਍     ⨀ഀഀ
     * When passed two or more arguments, a new module is created.  If passed only one argument, an਍     ⨀ 攀砀椀猀琀椀渀最 洀漀搀甀氀攀 ⠀琀栀攀 渀愀洀攀 瀀愀猀猀攀搀 愀猀 琀栀攀 昀椀爀猀琀 愀爀最甀洀攀渀琀 琀漀 怀洀漀搀甀氀攀怀⤀ 椀猀 爀攀琀爀椀攀瘀攀搀⸀ഀഀ
     *਍     ⨀ഀഀ
     * # Module਍     ⨀ഀഀ
     * A module is a collection of services, directives, filters, and configuration information.਍     ⨀ 怀愀渀最甀氀愀爀⸀洀漀搀甀氀攀怀 椀猀 甀猀攀搀 琀漀 挀漀渀昀椀最甀爀攀 琀栀攀 笀䀀氀椀渀欀 䄀唀吀伀⸀␀椀渀樀攀挀琀漀爀 ␀椀渀樀攀挀琀漀爀紀⸀ഀഀ
     *਍     ⨀ 㰀瀀爀攀㸀ഀഀ
     * // Create a new module਍     ⨀ 瘀愀爀 洀礀䴀漀搀甀氀攀 㴀 愀渀最甀氀愀爀⸀洀漀搀甀氀攀⠀✀洀礀䴀漀搀甀氀攀✀Ⰰ 嬀崀⤀㬀ഀഀ
     *਍     ⨀ ⼀⼀ 爀攀最椀猀琀攀爀 愀 渀攀眀 猀攀爀瘀椀挀攀ഀഀ
     * myModule.value('appName', 'MyCoolApp');਍     ⨀ഀഀ
     * // configure existing services inside initialization blocks.਍     ⨀ 洀礀䴀漀搀甀氀攀⸀挀漀渀昀椀最⠀昀甀渀挀琀椀漀渀⠀␀氀漀挀愀琀椀漀渀倀爀漀瘀椀搀攀爀⤀ 笀ഀഀ
     *   // Configure existing providers਍     ⨀   ␀氀漀挀愀琀椀漀渀倀爀漀瘀椀搀攀爀⸀栀愀猀栀倀爀攀昀椀砀⠀✀℀✀⤀㬀ഀഀ
     * });਍     ⨀ 㰀⼀瀀爀攀㸀ഀഀ
     *਍     ⨀ 吀栀攀渀 礀漀甀 挀愀渀 挀爀攀愀琀攀 愀渀 椀渀樀攀挀琀漀爀 愀渀搀 氀漀愀搀 礀漀甀爀 洀漀搀甀氀攀猀 氀椀欀攀 琀栀椀猀㨀ഀഀ
     *਍     ⨀ 㰀瀀爀攀㸀ഀഀ
     * var injector = angular.injector(['ng', 'MyModule'])਍     ⨀ 㰀⼀瀀爀攀㸀ഀഀ
     *਍     ⨀ 䠀漀眀攀瘀攀爀 椀琀✀猀 洀漀爀攀 氀椀欀攀氀礀 琀栀愀琀 礀漀甀✀氀氀 樀甀猀琀 甀猀攀ഀഀ
     * {@link ng.directive:ngApp ngApp} or਍     ⨀ 笀䀀氀椀渀欀 愀渀最甀氀愀爀⸀戀漀漀琀猀琀爀愀瀀紀 琀漀 猀椀洀瀀氀椀昀礀 琀栀椀猀 瀀爀漀挀攀猀猀 昀漀爀 礀漀甀⸀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀℀猀琀爀椀渀最紀 渀愀洀攀 吀栀攀 渀愀洀攀 漀昀 琀栀攀 洀漀搀甀氀攀 琀漀 挀爀攀愀琀攀 漀爀 爀攀琀爀椀攀瘀攀⸀ഀഀ
     * @param {Array.<string>=} requires If specified then new module is being created. If਍     ⨀        甀渀猀瀀攀挀椀昀椀攀搀 琀栀攀渀 琀栀攀 琀栀攀 洀漀搀甀氀攀 椀猀 戀攀椀渀最 爀攀琀爀椀攀瘀攀搀 昀漀爀 昀甀爀琀栀攀爀 挀漀渀昀椀最甀爀愀琀椀漀渀⸀ഀഀ
     * @param {Function} configFn Optional configuration function for the module. Same as਍     ⨀        笀䀀氀椀渀欀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀⌀洀攀琀栀漀搀猀开挀漀渀昀椀最 䴀漀搀甀氀攀⌀挀漀渀昀椀最⠀⤀紀⸀ഀഀ
     * @returns {module} new module with the {@link angular.Module} api.਍     ⨀⼀ഀഀ
    return function module(name, requires, configFn) {਍      瘀愀爀 愀猀猀攀爀琀一漀琀䠀愀猀伀眀渀倀爀漀瀀攀爀琀礀 㴀 昀甀渀挀琀椀漀渀⠀渀愀洀攀Ⰰ 挀漀渀琀攀砀琀⤀ 笀ഀഀ
        if (name === 'hasOwnProperty') {਍          琀栀爀漀眀 渀最䴀椀渀䔀爀爀⠀✀戀愀搀渀愀洀攀✀Ⰰ ✀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀 椀猀 渀漀琀 愀 瘀愀氀椀搀 笀　紀 渀愀洀攀✀Ⰰ 挀漀渀琀攀砀琀⤀㬀ഀഀ
        }਍      紀㬀ഀഀ
਍      愀猀猀攀爀琀一漀琀䠀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀渀愀洀攀Ⰰ ✀洀漀搀甀氀攀✀⤀㬀ഀഀ
      if (requires && modules.hasOwnProperty(name)) {਍        洀漀搀甀氀攀猀嬀渀愀洀攀崀 㴀 渀甀氀氀㬀ഀഀ
      }਍      爀攀琀甀爀渀 攀渀猀甀爀攀⠀洀漀搀甀氀攀猀Ⰰ 渀愀洀攀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
        if (!requires) {਍          琀栀爀漀眀 ␀椀渀樀攀挀琀漀爀䴀椀渀䔀爀爀⠀✀渀漀洀漀搀✀Ⰰ ∀䴀漀搀甀氀攀 ✀笀　紀✀ 椀猀 渀漀琀 愀瘀愀椀氀愀戀氀攀℀ 夀漀甀 攀椀琀栀攀爀 洀椀猀猀瀀攀氀氀攀搀 ∀ ⬀ഀഀ
             "the module name or forgot to load it. If registering a module ensure that you " +਍             ∀猀瀀攀挀椀昀礀 琀栀攀 搀攀瀀攀渀搀攀渀挀椀攀猀 愀猀 琀栀攀 猀攀挀漀渀搀 愀爀最甀洀攀渀琀⸀∀Ⰰ 渀愀洀攀⤀㬀ഀഀ
        }਍ഀഀ
        /** @type {!Array.<Array.<*>>} */਍        瘀愀爀 椀渀瘀漀欀攀儀甀攀甀攀 㴀 嬀崀㬀ഀഀ
਍        ⼀⨀⨀ 䀀琀礀瀀攀 笀℀䄀爀爀愀礀⸀㰀䘀甀渀挀琀椀漀渀㸀紀 ⨀⼀ഀഀ
        var runBlocks = [];਍ഀഀ
        var config = invokeLater('$injector', 'invoke');਍ഀഀ
        /** @type {angular.Module} */਍        瘀愀爀 洀漀搀甀氀攀䤀渀猀琀愀渀挀攀 㴀 笀ഀഀ
          // Private state਍          开椀渀瘀漀欀攀儀甀攀甀攀㨀 椀渀瘀漀欀攀儀甀攀甀攀Ⰰഀഀ
          _runBlocks: runBlocks,਍ഀഀ
          /**਍           ⨀ 䀀渀最搀漀挀 瀀爀漀瀀攀爀琀礀ഀഀ
           * @name angular.Module#requires਍           ⨀ 䀀瀀爀漀瀀攀爀琀礀伀昀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀ഀഀ
           * @returns {Array.<string>} List of module names which must be loaded before this module.਍           ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
           * Holds the list of modules which the injector will load before the current module is਍           ⨀ 氀漀愀搀攀搀⸀ഀഀ
           */਍          爀攀焀甀椀爀攀猀㨀 爀攀焀甀椀爀攀猀Ⰰഀഀ
਍          ⼀⨀⨀ഀഀ
           * @ngdoc property਍           ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀⌀渀愀洀攀ഀഀ
           * @propertyOf angular.Module਍           ⨀ 䀀爀攀琀甀爀渀猀 笀猀琀爀椀渀最紀 一愀洀攀 漀昀 琀栀攀 洀漀搀甀氀攀⸀ഀഀ
           * @description਍           ⨀⼀ഀഀ
          name: name,਍ഀഀ
਍          ⼀⨀⨀ഀഀ
           * @ngdoc method਍           ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀⌀瀀爀漀瘀椀搀攀爀ഀഀ
           * @methodOf angular.Module਍           ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀愀洀攀 猀攀爀瘀椀挀攀 渀愀洀攀ഀഀ
           * @param {Function} providerType Construction function for creating new instance of the਍           ⨀                                猀攀爀瘀椀挀攀⸀ഀഀ
           * @description਍           ⨀ 匀攀攀 笀䀀氀椀渀欀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀⌀瀀爀漀瘀椀搀攀爀 ␀瀀爀漀瘀椀搀攀⸀瀀爀漀瘀椀搀攀爀⠀⤀紀⸀ഀഀ
           */਍          瀀爀漀瘀椀搀攀爀㨀 椀渀瘀漀欀攀䰀愀琀攀爀⠀✀␀瀀爀漀瘀椀搀攀✀Ⰰ ✀瀀爀漀瘀椀搀攀爀✀⤀Ⰰഀഀ
਍          ⼀⨀⨀ഀഀ
           * @ngdoc method਍           ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀⌀昀愀挀琀漀爀礀ഀഀ
           * @methodOf angular.Module਍           ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀愀洀攀 猀攀爀瘀椀挀攀 渀愀洀攀ഀഀ
           * @param {Function} providerFunction Function for creating new instance of the service.਍           ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
           * See {@link AUTO.$provide#factory $provide.factory()}.਍           ⨀⼀ഀഀ
          factory: invokeLater('$provide', 'factory'),਍ഀഀ
          /**਍           ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
           * @name angular.Module#service਍           ⨀ 䀀洀攀琀栀漀搀伀昀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀ഀഀ
           * @param {string} name service name਍           ⨀ 䀀瀀愀爀愀洀 笀䘀甀渀挀琀椀漀渀紀 挀漀渀猀琀爀甀挀琀漀爀 䄀 挀漀渀猀琀爀甀挀琀漀爀 昀甀渀挀琀椀漀渀 琀栀愀琀 眀椀氀氀 戀攀 椀渀猀琀愀渀琀椀愀琀攀搀⸀ഀഀ
           * @description਍           ⨀ 匀攀攀 笀䀀氀椀渀欀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀⌀猀攀爀瘀椀挀攀 ␀瀀爀漀瘀椀搀攀⸀猀攀爀瘀椀挀攀⠀⤀紀⸀ഀഀ
           */਍          猀攀爀瘀椀挀攀㨀 椀渀瘀漀欀攀䰀愀琀攀爀⠀✀␀瀀爀漀瘀椀搀攀✀Ⰰ ✀猀攀爀瘀椀挀攀✀⤀Ⰰഀഀ
਍          ⼀⨀⨀ഀഀ
           * @ngdoc method਍           ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀⌀瘀愀氀甀攀ഀഀ
           * @methodOf angular.Module਍           ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀愀洀攀 猀攀爀瘀椀挀攀 渀愀洀攀ഀഀ
           * @param {*} object Service instance object.਍           ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
           * See {@link AUTO.$provide#value $provide.value()}.਍           ⨀⼀ഀഀ
          value: invokeLater('$provide', 'value'),਍ഀഀ
          /**਍           ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
           * @name angular.Module#constant਍           ⨀ 䀀洀攀琀栀漀搀伀昀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀ഀഀ
           * @param {string} name constant name਍           ⨀ 䀀瀀愀爀愀洀 笀⨀紀 漀戀樀攀挀琀 䌀漀渀猀琀愀渀琀 瘀愀氀甀攀⸀ഀഀ
           * @description਍           ⨀ 䈀攀挀愀甀猀攀 琀栀攀 挀漀渀猀琀愀渀琀 愀爀攀 昀椀砀攀搀Ⰰ 琀栀攀礀 最攀琀 愀瀀瀀氀椀攀搀 戀攀昀漀爀攀 漀琀栀攀爀 瀀爀漀瘀椀搀攀 洀攀琀栀漀搀猀⸀ഀഀ
           * See {@link AUTO.$provide#constant $provide.constant()}.਍           ⨀⼀ഀഀ
          constant: invokeLater('$provide', 'constant', 'unshift'),਍ഀഀ
          /**਍           ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
           * @name angular.Module#animation਍           ⨀ 䀀洀攀琀栀漀搀伀昀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀ഀഀ
           * @param {string} name animation name਍           ⨀ 䀀瀀愀爀愀洀 笀䘀甀渀挀琀椀漀渀紀 愀渀椀洀愀琀椀漀渀䘀愀挀琀漀爀礀 䘀愀挀琀漀爀礀 昀甀渀挀琀椀漀渀 昀漀爀 挀爀攀愀琀椀渀最 渀攀眀 椀渀猀琀愀渀挀攀 漀昀 愀渀ഀഀ
           *                                    animation.਍           ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
           *਍           ⨀ ⨀⨀一伀吀䔀⨀⨀㨀 愀渀椀洀愀琀椀漀渀猀 琀愀欀攀 攀昀昀攀挀琀 漀渀氀礀 椀昀 琀栀攀 ⨀⨀渀最䄀渀椀洀愀琀攀⨀⨀ 洀漀搀甀氀攀 椀猀 氀漀愀搀攀搀⸀ഀഀ
           *਍           ⨀ഀഀ
           * Defines an animation hook that can be later used with਍           ⨀ 笀䀀氀椀渀欀 渀最䄀渀椀洀愀琀攀⸀␀愀渀椀洀愀琀攀 ␀愀渀椀洀愀琀攀紀 猀攀爀瘀椀挀攀 愀渀搀 搀椀爀攀挀琀椀瘀攀猀 琀栀愀琀 甀猀攀 琀栀椀猀 猀攀爀瘀椀挀攀⸀ഀഀ
           *਍           ⨀ 㰀瀀爀攀㸀ഀഀ
           * module.animation('.animation-name', function($inject1, $inject2) {਍           ⨀   爀攀琀甀爀渀 笀ഀഀ
           *     eventName : function(element, done) {਍           ⨀       ⼀⼀挀漀搀攀 琀漀 爀甀渀 琀栀攀 愀渀椀洀愀琀椀漀渀ഀഀ
           *       //once complete, then run done()਍           ⨀       爀攀琀甀爀渀 昀甀渀挀琀椀漀渀 挀愀渀挀攀氀氀愀琀椀漀渀䘀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀⤀ 笀ഀഀ
           *         //code to cancel the animation਍           ⨀       紀ഀഀ
           *     }਍           ⨀   紀ഀഀ
           * })਍           ⨀ 㰀⼀瀀爀攀㸀ഀഀ
           *਍           ⨀ 匀攀攀 笀䀀氀椀渀欀 渀最䄀渀椀洀愀琀攀⸀␀愀渀椀洀愀琀攀倀爀漀瘀椀搀攀爀⌀爀攀最椀猀琀攀爀 ␀愀渀椀洀愀琀攀倀爀漀瘀椀搀攀爀⸀爀攀最椀猀琀攀爀⠀⤀紀 愀渀搀ഀഀ
           * {@link ngAnimate ngAnimate module} for more information.਍           ⨀⼀ഀഀ
          animation: invokeLater('$animateProvider', 'register'),਍ഀഀ
          /**਍           ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
           * @name angular.Module#filter਍           ⨀ 䀀洀攀琀栀漀搀伀昀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀ഀഀ
           * @param {string} name Filter name.਍           ⨀ 䀀瀀愀爀愀洀 笀䘀甀渀挀琀椀漀渀紀 昀椀氀琀攀爀䘀愀挀琀漀爀礀 䘀愀挀琀漀爀礀 昀甀渀挀琀椀漀渀 昀漀爀 挀爀攀愀琀椀渀最 渀攀眀 椀渀猀琀愀渀挀攀 漀昀 昀椀氀琀攀爀⸀ഀഀ
           * @description਍           ⨀ 匀攀攀 笀䀀氀椀渀欀 渀最⸀␀昀椀氀琀攀爀倀爀漀瘀椀搀攀爀⌀爀攀最椀猀琀攀爀 ␀昀椀氀琀攀爀倀爀漀瘀椀搀攀爀⸀爀攀最椀猀琀攀爀⠀⤀紀⸀ഀഀ
           */਍          昀椀氀琀攀爀㨀 椀渀瘀漀欀攀䰀愀琀攀爀⠀✀␀昀椀氀琀攀爀倀爀漀瘀椀搀攀爀✀Ⰰ ✀爀攀最椀猀琀攀爀✀⤀Ⰰഀഀ
਍          ⼀⨀⨀ഀഀ
           * @ngdoc method਍           ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀⌀挀漀渀琀爀漀氀氀攀爀ഀഀ
           * @methodOf angular.Module਍           ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最簀伀戀樀攀挀琀紀 渀愀洀攀 䌀漀渀琀爀漀氀氀攀爀 渀愀洀攀Ⰰ 漀爀 愀渀 漀戀樀攀挀琀 洀愀瀀 漀昀 挀漀渀琀爀漀氀氀攀爀猀 眀栀攀爀攀 琀栀攀ഀഀ
           *    keys are the names and the values are the constructors.਍           ⨀ 䀀瀀愀爀愀洀 笀䘀甀渀挀琀椀漀渀紀 挀漀渀猀琀爀甀挀琀漀爀 䌀漀渀琀爀漀氀氀攀爀 挀漀渀猀琀爀甀挀琀漀爀 昀甀渀挀琀椀漀渀⸀ഀഀ
           * @description਍           ⨀ 匀攀攀 笀䀀氀椀渀欀 渀最⸀␀挀漀渀琀爀漀氀氀攀爀倀爀漀瘀椀搀攀爀⌀爀攀最椀猀琀攀爀 ␀挀漀渀琀爀漀氀氀攀爀倀爀漀瘀椀搀攀爀⸀爀攀最椀猀琀攀爀⠀⤀紀⸀ഀഀ
           */਍          挀漀渀琀爀漀氀氀攀爀㨀 椀渀瘀漀欀攀䰀愀琀攀爀⠀✀␀挀漀渀琀爀漀氀氀攀爀倀爀漀瘀椀搀攀爀✀Ⰰ ✀爀攀最椀猀琀攀爀✀⤀Ⰰഀഀ
਍          ⼀⨀⨀ഀഀ
           * @ngdoc method਍           ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀⌀搀椀爀攀挀琀椀瘀攀ഀഀ
           * @methodOf angular.Module਍           ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最簀伀戀樀攀挀琀紀 渀愀洀攀 䐀椀爀攀挀琀椀瘀攀 渀愀洀攀Ⰰ 漀爀 愀渀 漀戀樀攀挀琀 洀愀瀀 漀昀 搀椀爀攀挀琀椀瘀攀猀 眀栀攀爀攀 琀栀攀ഀഀ
           *    keys are the names and the values are the factories.਍           ⨀ 䀀瀀愀爀愀洀 笀䘀甀渀挀琀椀漀渀紀 搀椀爀攀挀琀椀瘀攀䘀愀挀琀漀爀礀 䘀愀挀琀漀爀礀 昀甀渀挀琀椀漀渀 昀漀爀 挀爀攀愀琀椀渀最 渀攀眀 椀渀猀琀愀渀挀攀 漀昀ഀഀ
           * directives.਍           ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
           * See {@link ng.$compileProvider#methods_directive $compileProvider.directive()}.਍           ⨀⼀ഀഀ
          directive: invokeLater('$compileProvider', 'directive'),਍ഀഀ
          /**਍           ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
           * @name angular.Module#config਍           ⨀ 䀀洀攀琀栀漀搀伀昀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀ഀഀ
           * @param {Function} configFn Execute this function on module load. Useful for service਍           ⨀    挀漀渀昀椀最甀爀愀琀椀漀渀⸀ഀഀ
           * @description਍           ⨀ 唀猀攀 琀栀椀猀 洀攀琀栀漀搀 琀漀 爀攀最椀猀琀攀爀 眀漀爀欀 眀栀椀挀栀 渀攀攀搀猀 琀漀 戀攀 瀀攀爀昀漀爀洀攀搀 漀渀 洀漀搀甀氀攀 氀漀愀搀椀渀最⸀ഀഀ
           */਍          挀漀渀昀椀最㨀 挀漀渀昀椀最Ⰰഀഀ
਍          ⼀⨀⨀ഀഀ
           * @ngdoc method਍           ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀⌀爀甀渀ഀഀ
           * @methodOf angular.Module਍           ⨀ 䀀瀀愀爀愀洀 笀䘀甀渀挀琀椀漀渀紀 椀渀椀琀椀愀氀椀稀愀琀椀漀渀䘀渀 䔀砀攀挀甀琀攀 琀栀椀猀 昀甀渀挀琀椀漀渀 愀昀琀攀爀 椀渀樀攀挀琀漀爀 挀爀攀愀琀椀漀渀⸀ഀഀ
           *    Useful for application initialization.਍           ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
           * Use this method to register work which should be performed when the injector is done਍           ⨀ 氀漀愀搀椀渀最 愀氀氀 洀漀搀甀氀攀猀⸀ഀഀ
           */਍          爀甀渀㨀 昀甀渀挀琀椀漀渀⠀戀氀漀挀欀⤀ 笀ഀഀ
            runBlocks.push(block);਍            爀攀琀甀爀渀 琀栀椀猀㬀ഀഀ
          }਍        紀㬀ഀഀ
਍        椀昀 ⠀挀漀渀昀椀最䘀渀⤀ 笀ഀഀ
          config(configFn);਍        紀ഀഀ
਍        爀攀琀甀爀渀  洀漀搀甀氀攀䤀渀猀琀愀渀挀攀㬀ഀഀ
਍        ⼀⨀⨀ഀഀ
         * @param {string} provider਍         ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 洀攀琀栀漀搀ഀഀ
         * @param {String=} insertMethod਍         ⨀ 䀀爀攀琀甀爀渀猀 笀愀渀最甀氀愀爀⸀䴀漀搀甀氀攀紀ഀഀ
         */਍        昀甀渀挀琀椀漀渀 椀渀瘀漀欀攀䰀愀琀攀爀⠀瀀爀漀瘀椀搀攀爀Ⰰ 洀攀琀栀漀搀Ⰰ 椀渀猀攀爀琀䴀攀琀栀漀搀⤀ 笀ഀഀ
          return function() {਍            椀渀瘀漀欀攀儀甀攀甀攀嬀椀渀猀攀爀琀䴀攀琀栀漀搀 簀簀 ✀瀀甀猀栀✀崀⠀嬀瀀爀漀瘀椀搀攀爀Ⰰ 洀攀琀栀漀搀Ⰰ 愀爀最甀洀攀渀琀猀崀⤀㬀ഀഀ
            return moduleInstance;਍          紀㬀ഀഀ
        }਍      紀⤀㬀ഀഀ
    };਍  紀⤀㬀ഀഀ
਍紀ഀഀ
਍⼀⨀ 最氀漀戀愀氀ഀഀ
    angularModule: true,਍    瘀攀爀猀椀漀渀㨀 琀爀甀攀Ⰰഀഀ
    ਍    ␀䰀漀挀愀氀攀倀爀漀瘀椀搀攀爀Ⰰഀഀ
    $CompileProvider,਍    ഀഀ
    htmlAnchorDirective,਍    椀渀瀀甀琀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    inputDirective,਍    昀漀爀洀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    scriptDirective,਍    猀攀氀攀挀琀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    styleDirective,਍    漀瀀琀椀漀渀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    ngBindDirective,਍    渀最䈀椀渀搀䠀琀洀氀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    ngBindTemplateDirective,਍    渀最䌀氀愀猀猀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    ngClassEvenDirective,਍    渀最䌀氀愀猀猀伀搀搀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    ngCspDirective,਍    渀最䌀氀漀愀欀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    ngControllerDirective,਍    渀最䘀漀爀洀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    ngHideDirective,਍    渀最䤀昀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    ngIncludeDirective,਍    渀最䤀渀挀氀甀搀攀䘀椀氀氀䌀漀渀琀攀渀琀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    ngInitDirective,਍    渀最一漀渀䈀椀渀搀愀戀氀攀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    ngPluralizeDirective,਍    渀最刀攀瀀攀愀琀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    ngShowDirective,਍    渀最匀琀礀氀攀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    ngSwitchDirective,਍    渀最匀眀椀琀挀栀圀栀攀渀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    ngSwitchDefaultDirective,਍    渀最伀瀀琀椀漀渀猀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    ngTranscludeDirective,਍    渀最䴀漀搀攀氀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    ngListDirective,਍    渀最䌀栀愀渀最攀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    requiredDirective,਍    爀攀焀甀椀爀攀搀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
    ngValueDirective,਍    渀最䄀琀琀爀椀戀甀琀攀䄀氀椀愀猀䐀椀爀攀挀琀椀瘀攀猀Ⰰഀഀ
    ngEventDirectives,਍ഀഀ
    $AnchorScrollProvider,਍    ␀䄀渀椀洀愀琀攀倀爀漀瘀椀搀攀爀Ⰰഀഀ
    $BrowserProvider,਍    ␀䌀愀挀栀攀䘀愀挀琀漀爀礀倀爀漀瘀椀搀攀爀Ⰰഀഀ
    $ControllerProvider,਍    ␀䐀漀挀甀洀攀渀琀倀爀漀瘀椀搀攀爀Ⰰഀഀ
    $ExceptionHandlerProvider,਍    ␀䘀椀氀琀攀爀倀爀漀瘀椀搀攀爀Ⰰഀഀ
    $InterpolateProvider,਍    ␀䤀渀琀攀爀瘀愀氀倀爀漀瘀椀搀攀爀Ⰰഀഀ
    $HttpProvider,਍    ␀䠀琀琀瀀䈀愀挀欀攀渀搀倀爀漀瘀椀搀攀爀Ⰰഀഀ
    $LocationProvider,਍    ␀䰀漀最倀爀漀瘀椀搀攀爀Ⰰഀഀ
    $ParseProvider,਍    ␀刀漀漀琀匀挀漀瀀攀倀爀漀瘀椀搀攀爀Ⰰഀഀ
    $QProvider,਍    ␀␀匀愀渀椀琀椀稀攀唀爀椀倀爀漀瘀椀搀攀爀Ⰰഀഀ
    $SceProvider,਍    ␀匀挀攀䐀攀氀攀最愀琀攀倀爀漀瘀椀搀攀爀Ⰰഀഀ
    $SnifferProvider,਍    ␀吀攀洀瀀氀愀琀攀䌀愀挀栀攀倀爀漀瘀椀搀攀爀Ⰰഀഀ
    $TimeoutProvider,਍    ␀圀椀渀搀漀眀倀爀漀瘀椀搀攀爀ഀഀ
*/਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc property਍ ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀瘀攀爀猀椀漀渀ഀഀ
 * @description਍ ⨀ 䄀渀 漀戀樀攀挀琀 琀栀愀琀 挀漀渀琀愀椀渀猀 椀渀昀漀爀洀愀琀椀漀渀 愀戀漀甀琀 琀栀攀 挀甀爀爀攀渀琀 䄀渀最甀氀愀爀䨀匀 瘀攀爀猀椀漀渀⸀ 吀栀椀猀 漀戀樀攀挀琀 栀愀猀 琀栀攀ഀഀ
 * following properties:਍ ⨀ഀഀ
 * - `full` �� `{string}` �� Full version string, such as "0.9.18".਍ ⨀ ⴀ 怀洀愀樀漀爀怀 ﴀ﷿⃿怀笀渀甀洀戀攀爀紀怀 ﴀ﷿⃿䴀愀樀漀爀 瘀攀爀猀椀漀渀 渀甀洀戀攀爀Ⰰ 猀甀挀栀 愀猀 ∀　∀⸀ഀഀ
 * - `minor` �� `{number}` �� Minor version number, such as "9".਍ ⨀ ⴀ 怀搀漀琀怀 ﴀ﷿⃿怀笀渀甀洀戀攀爀紀怀 ﴀ﷿⃿䐀漀琀 瘀攀爀猀椀漀渀 渀甀洀戀攀爀Ⰰ 猀甀挀栀 愀猀 ∀㄀㠀∀⸀ഀഀ
 * - `codeName` �� `{string}` �� Code name of the release, such as "jiggling-armfat".਍ ⨀⼀ഀഀ
var version = {਍  昀甀氀氀㨀 ✀㄀⸀㈀⸀㠀✀Ⰰ    ⼀⼀ 愀氀氀 漀昀 琀栀攀猀攀 瀀氀愀挀攀栀漀氀搀攀爀 猀琀爀椀渀最猀 眀椀氀氀 戀攀 爀攀瀀氀愀挀攀搀 戀礀 最爀甀渀琀✀猀ഀഀ
  major: 1,    // package task਍  洀椀渀漀爀㨀 ㈀Ⰰഀഀ
  dot: 8,਍  挀漀搀攀一愀洀攀㨀 ✀椀渀琀攀爀搀椀洀攀渀猀椀漀渀愀氀ⴀ挀愀爀琀漀最爀愀瀀栀礀✀ഀഀ
};਍ഀഀ
਍昀甀渀挀琀椀漀渀 瀀甀戀氀椀猀栀䔀砀琀攀爀渀愀氀䄀倀䤀⠀愀渀最甀氀愀爀⤀笀ഀഀ
  extend(angular, {਍    ✀戀漀漀琀猀琀爀愀瀀✀㨀 戀漀漀琀猀琀爀愀瀀Ⰰഀഀ
    'copy': copy,਍    ✀攀砀琀攀渀搀✀㨀 攀砀琀攀渀搀Ⰰഀഀ
    'equals': equals,਍    ✀攀氀攀洀攀渀琀✀㨀 樀焀䰀椀琀攀Ⰰഀഀ
    'forEach': forEach,਍    ✀椀渀樀攀挀琀漀爀✀㨀 挀爀攀愀琀攀䤀渀樀攀挀琀漀爀Ⰰഀഀ
    'noop':noop,਍    ✀戀椀渀搀✀㨀戀椀渀搀Ⰰഀഀ
    'toJson': toJson,਍    ✀昀爀漀洀䨀猀漀渀✀㨀 昀爀漀洀䨀猀漀渀Ⰰഀഀ
    'identity':identity,਍    ✀椀猀唀渀搀攀昀椀渀攀搀✀㨀 椀猀唀渀搀攀昀椀渀攀搀Ⰰഀഀ
    'isDefined': isDefined,਍    ✀椀猀匀琀爀椀渀最✀㨀 椀猀匀琀爀椀渀最Ⰰഀഀ
    'isFunction': isFunction,਍    ✀椀猀伀戀樀攀挀琀✀㨀 椀猀伀戀樀攀挀琀Ⰰഀഀ
    'isNumber': isNumber,਍    ✀椀猀䔀氀攀洀攀渀琀✀㨀 椀猀䔀氀攀洀攀渀琀Ⰰഀഀ
    'isArray': isArray,਍    ✀瘀攀爀猀椀漀渀✀㨀 瘀攀爀猀椀漀渀Ⰰഀഀ
    'isDate': isDate,਍    ✀氀漀眀攀爀挀愀猀攀✀㨀 氀漀眀攀爀挀愀猀攀Ⰰഀഀ
    'uppercase': uppercase,਍    ✀挀愀氀氀戀愀挀欀猀✀㨀 笀挀漀甀渀琀攀爀㨀 　紀Ⰰഀഀ
    '$$minErr': minErr,਍    ✀␀␀挀猀瀀✀㨀 挀猀瀀ഀഀ
  });਍ഀഀ
  angularModule = setupModuleLoader(window);਍  琀爀礀 笀ഀഀ
    angularModule('ngLocale');਍  紀 挀愀琀挀栀 ⠀攀⤀ 笀ഀഀ
    angularModule('ngLocale', []).provider('$locale', $LocaleProvider);਍  紀ഀഀ
਍  愀渀最甀氀愀爀䴀漀搀甀氀攀⠀✀渀最✀Ⰰ 嬀✀渀最䰀漀挀愀氀攀✀崀Ⰰ 嬀✀␀瀀爀漀瘀椀搀攀✀Ⰰഀഀ
    function ngModule($provide) {਍      ⼀⼀ ␀␀猀愀渀椀琀椀稀攀唀爀椀倀爀漀瘀椀搀攀爀 渀攀攀搀猀 琀漀 戀攀 戀攀昀漀爀攀 ␀挀漀洀瀀椀氀攀倀爀漀瘀椀搀攀爀 愀猀 椀琀 椀猀 甀猀攀搀 戀礀 椀琀⸀ഀഀ
      $provide.provider({਍        ␀␀猀愀渀椀琀椀稀攀唀爀椀㨀 ␀␀匀愀渀椀琀椀稀攀唀爀椀倀爀漀瘀椀搀攀爀ഀഀ
      });਍      ␀瀀爀漀瘀椀搀攀⸀瀀爀漀瘀椀搀攀爀⠀✀␀挀漀洀瀀椀氀攀✀Ⰰ ␀䌀漀洀瀀椀氀攀倀爀漀瘀椀搀攀爀⤀⸀ഀഀ
        directive({਍            愀㨀 栀琀洀氀䄀渀挀栀漀爀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            input: inputDirective,਍            琀攀砀琀愀爀攀愀㨀 椀渀瀀甀琀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            form: formDirective,਍            猀挀爀椀瀀琀㨀 猀挀爀椀瀀琀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            select: selectDirective,਍            猀琀礀氀攀㨀 猀琀礀氀攀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            option: optionDirective,਍            渀最䈀椀渀搀㨀 渀最䈀椀渀搀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            ngBindHtml: ngBindHtmlDirective,਍            渀最䈀椀渀搀吀攀洀瀀氀愀琀攀㨀 渀最䈀椀渀搀吀攀洀瀀氀愀琀攀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            ngClass: ngClassDirective,਍            渀最䌀氀愀猀猀䔀瘀攀渀㨀 渀最䌀氀愀猀猀䔀瘀攀渀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            ngClassOdd: ngClassOddDirective,਍            渀最䌀氀漀愀欀㨀 渀最䌀氀漀愀欀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            ngController: ngControllerDirective,਍            渀最䘀漀爀洀㨀 渀最䘀漀爀洀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            ngHide: ngHideDirective,਍            渀最䤀昀㨀 渀最䤀昀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            ngInclude: ngIncludeDirective,਍            渀最䤀渀椀琀㨀 渀最䤀渀椀琀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            ngNonBindable: ngNonBindableDirective,਍            渀最倀氀甀爀愀氀椀稀攀㨀 渀最倀氀甀爀愀氀椀稀攀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            ngRepeat: ngRepeatDirective,਍            渀最匀栀漀眀㨀 渀最匀栀漀眀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            ngStyle: ngStyleDirective,਍            渀最匀眀椀琀挀栀㨀 渀最匀眀椀琀挀栀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            ngSwitchWhen: ngSwitchWhenDirective,਍            渀最匀眀椀琀挀栀䐀攀昀愀甀氀琀㨀 渀最匀眀椀琀挀栀䐀攀昀愀甀氀琀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            ngOptions: ngOptionsDirective,਍            渀最吀爀愀渀猀挀氀甀搀攀㨀 渀最吀爀愀渀猀挀氀甀搀攀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            ngModel: ngModelDirective,਍            渀最䰀椀猀琀㨀 渀最䰀椀猀琀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            ngChange: ngChangeDirective,਍            爀攀焀甀椀爀攀搀㨀 爀攀焀甀椀爀攀搀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
            ngRequired: requiredDirective,਍            渀最嘀愀氀甀攀㨀 渀最嘀愀氀甀攀䐀椀爀攀挀琀椀瘀攀ഀഀ
        }).਍        搀椀爀攀挀琀椀瘀攀⠀笀ഀഀ
          ngInclude: ngIncludeFillContentDirective਍        紀⤀⸀ഀഀ
        directive(ngAttributeAliasDirectives).਍        搀椀爀攀挀琀椀瘀攀⠀渀最䔀瘀攀渀琀䐀椀爀攀挀琀椀瘀攀猀⤀㬀ഀഀ
      $provide.provider({਍        ␀愀渀挀栀漀爀匀挀爀漀氀氀㨀 ␀䄀渀挀栀漀爀匀挀爀漀氀氀倀爀漀瘀椀搀攀爀Ⰰഀഀ
        $animate: $AnimateProvider,਍        ␀戀爀漀眀猀攀爀㨀 ␀䈀爀漀眀猀攀爀倀爀漀瘀椀搀攀爀Ⰰഀഀ
        $cacheFactory: $CacheFactoryProvider,਍        ␀挀漀渀琀爀漀氀氀攀爀㨀 ␀䌀漀渀琀爀漀氀氀攀爀倀爀漀瘀椀搀攀爀Ⰰഀഀ
        $document: $DocumentProvider,਍        ␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀㨀 ␀䔀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀倀爀漀瘀椀搀攀爀Ⰰഀഀ
        $filter: $FilterProvider,਍        ␀椀渀琀攀爀瀀漀氀愀琀攀㨀 ␀䤀渀琀攀爀瀀漀氀愀琀攀倀爀漀瘀椀搀攀爀Ⰰഀഀ
        $interval: $IntervalProvider,਍        ␀栀琀琀瀀㨀 ␀䠀琀琀瀀倀爀漀瘀椀搀攀爀Ⰰഀഀ
        $httpBackend: $HttpBackendProvider,਍        ␀氀漀挀愀琀椀漀渀㨀 ␀䰀漀挀愀琀椀漀渀倀爀漀瘀椀搀攀爀Ⰰഀഀ
        $log: $LogProvider,਍        ␀瀀愀爀猀攀㨀 ␀倀愀爀猀攀倀爀漀瘀椀搀攀爀Ⰰഀഀ
        $rootScope: $RootScopeProvider,਍        ␀焀㨀 ␀儀倀爀漀瘀椀搀攀爀Ⰰഀഀ
        $sce: $SceProvider,਍        ␀猀挀攀䐀攀氀攀最愀琀攀㨀 ␀匀挀攀䐀攀氀攀最愀琀攀倀爀漀瘀椀搀攀爀Ⰰഀഀ
        $sniffer: $SnifferProvider,਍        ␀琀攀洀瀀氀愀琀攀䌀愀挀栀攀㨀 ␀吀攀洀瀀氀愀琀攀䌀愀挀栀攀倀爀漀瘀椀搀攀爀Ⰰഀഀ
        $timeout: $TimeoutProvider,਍        ␀眀椀渀搀漀眀㨀 ␀圀椀渀搀漀眀倀爀漀瘀椀搀攀爀ഀഀ
      });਍    紀ഀഀ
  ]);਍紀ഀഀ
਍⼀⨀ 最氀漀戀愀氀ഀഀ
਍  ⴀ䨀儀䰀椀琀攀倀爀漀琀漀琀礀瀀攀Ⰰഀഀ
  -addEventListenerFn,਍  ⴀ爀攀洀漀瘀攀䔀瘀攀渀琀䰀椀猀琀攀渀攀爀䘀渀Ⰰഀഀ
  -BOOLEAN_ATTR਍⨀⼀ഀഀ
਍⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
//JQLite਍⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀攀氀攀洀攀渀琀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ 圀爀愀瀀猀 愀 爀愀眀 䐀伀䴀 攀氀攀洀攀渀琀 漀爀 䠀吀䴀䰀 猀琀爀椀渀最 愀猀 愀 嬀樀儀甀攀爀礀崀⠀栀琀琀瀀㨀⼀⼀樀焀甀攀爀礀⸀挀漀洀⤀ 攀氀攀洀攀渀琀⸀ഀഀ
 *਍ ⨀ 䤀昀 樀儀甀攀爀礀 椀猀 愀瘀愀椀氀愀戀氀攀Ⰰ 怀愀渀最甀氀愀爀⸀攀氀攀洀攀渀琀怀 椀猀 愀渀 愀氀椀愀猀 昀漀爀 琀栀攀ഀഀ
 * [jQuery](http://api.jquery.com/jQuery/) function. If jQuery is not available, `angular.element`਍ ⨀ 搀攀氀攀最愀琀攀猀 琀漀 䄀渀最甀氀愀爀✀猀 戀甀椀氀琀ⴀ椀渀 猀甀戀猀攀琀 漀昀 樀儀甀攀爀礀Ⰰ 挀愀氀氀攀搀 ∀樀儀甀攀爀礀 氀椀琀攀∀ 漀爀 ∀樀焀䰀椀琀攀⸀∀ഀഀ
 *਍ ⨀ 㰀搀椀瘀 挀氀愀猀猀㴀∀愀氀攀爀琀 愀氀攀爀琀ⴀ猀甀挀挀攀猀猀∀㸀樀焀䰀椀琀攀 椀猀 愀 琀椀渀礀Ⰰ 䄀倀䤀ⴀ挀漀洀瀀愀琀椀戀氀攀 猀甀戀猀攀琀 漀昀 樀儀甀攀爀礀 琀栀愀琀 愀氀氀漀眀猀ഀഀ
 * Angular to manipulate the DOM in a cross-browser compatible way. **jqLite** implements only the most਍ ⨀ 挀漀洀洀漀渀氀礀 渀攀攀搀攀搀 昀甀渀挀琀椀漀渀愀氀椀琀礀 眀椀琀栀 琀栀攀 最漀愀氀 漀昀 栀愀瘀椀渀最 愀 瘀攀爀礀 猀洀愀氀氀 昀漀漀琀瀀爀椀渀琀⸀㰀⼀搀椀瘀㸀ഀഀ
 *਍ ⨀ 吀漀 甀猀攀 樀儀甀攀爀礀Ⰰ 猀椀洀瀀氀礀 氀漀愀搀 椀琀 戀攀昀漀爀攀 怀䐀伀䴀䌀漀渀琀攀渀琀䰀漀愀搀攀搀怀 攀瘀攀渀琀 昀椀爀攀搀⸀ഀഀ
 *਍ ⨀ 㰀搀椀瘀 挀氀愀猀猀㴀∀愀氀攀爀琀∀㸀⨀⨀一漀琀攀㨀⨀⨀ 愀氀氀 攀氀攀洀攀渀琀 爀攀昀攀爀攀渀挀攀猀 椀渀 䄀渀最甀氀愀爀 愀爀攀 愀氀眀愀礀猀 眀爀愀瀀瀀攀搀 眀椀琀栀 樀儀甀攀爀礀 漀爀ഀഀ
 * jqLite; they are never raw DOM references.</div>਍ ⨀ഀഀ
 * ## Angular's jqLite਍ ⨀ 樀焀䰀椀琀攀 瀀爀漀瘀椀搀攀猀 漀渀氀礀 琀栀攀 昀漀氀氀漀眀椀渀最 樀儀甀攀爀礀 洀攀琀栀漀搀猀㨀ഀഀ
 *਍ ⨀ ⴀ 嬀怀愀搀搀䌀氀愀猀猀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀愀搀搀䌀氀愀猀猀⼀⤀ഀഀ
 * - [`after()`](http://api.jquery.com/after/)਍ ⨀ ⴀ 嬀怀愀瀀瀀攀渀搀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀愀瀀瀀攀渀搀⼀⤀ഀഀ
 * - [`attr()`](http://api.jquery.com/attr/)਍ ⨀ ⴀ 嬀怀戀椀渀搀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀漀渀⼀⤀ ⴀ 䐀漀攀猀 渀漀琀 猀甀瀀瀀漀爀琀 渀愀洀攀猀瀀愀挀攀猀Ⰰ 猀攀氀攀挀琀漀爀猀 漀爀 攀瘀攀渀琀䐀愀琀愀ഀഀ
 * - [`children()`](http://api.jquery.com/children/) - Does not support selectors਍ ⨀ ⴀ 嬀怀挀氀漀渀攀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀挀氀漀渀攀⼀⤀ഀഀ
 * - [`contents()`](http://api.jquery.com/contents/)਍ ⨀ ⴀ 嬀怀挀猀猀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀挀猀猀⼀⤀ഀഀ
 * - [`data()`](http://api.jquery.com/data/)਍ ⨀ ⴀ 嬀怀攀洀瀀琀礀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀攀洀瀀琀礀⼀⤀ഀഀ
 * - [`eq()`](http://api.jquery.com/eq/)਍ ⨀ ⴀ 嬀怀昀椀渀搀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀昀椀渀搀⼀⤀ ⴀ 䰀椀洀椀琀攀搀 琀漀 氀漀漀欀甀瀀猀 戀礀 琀愀最 渀愀洀攀ഀഀ
 * - [`hasClass()`](http://api.jquery.com/hasClass/)਍ ⨀ ⴀ 嬀怀栀琀洀氀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀栀琀洀氀⼀⤀ഀഀ
 * - [`next()`](http://api.jquery.com/next/) - Does not support selectors਍ ⨀ ⴀ 嬀怀漀渀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀漀渀⼀⤀ ⴀ 䐀漀攀猀 渀漀琀 猀甀瀀瀀漀爀琀 渀愀洀攀猀瀀愀挀攀猀Ⰰ 猀攀氀攀挀琀漀爀猀 漀爀 攀瘀攀渀琀䐀愀琀愀ഀഀ
 * - [`off()`](http://api.jquery.com/off/) - Does not support namespaces or selectors਍ ⨀ ⴀ 嬀怀漀渀攀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀漀渀攀⼀⤀ ⴀ 䐀漀攀猀 渀漀琀 猀甀瀀瀀漀爀琀 渀愀洀攀猀瀀愀挀攀猀 漀爀 猀攀氀攀挀琀漀爀猀ഀഀ
 * - [`parent()`](http://api.jquery.com/parent/) - Does not support selectors਍ ⨀ ⴀ 嬀怀瀀爀攀瀀攀渀搀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀瀀爀攀瀀攀渀搀⼀⤀ഀഀ
 * - [`prop()`](http://api.jquery.com/prop/)਍ ⨀ ⴀ 嬀怀爀攀愀搀礀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀爀攀愀搀礀⼀⤀ഀഀ
 * - [`remove()`](http://api.jquery.com/remove/)਍ ⨀ ⴀ 嬀怀爀攀洀漀瘀攀䄀琀琀爀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀爀攀洀漀瘀攀䄀琀琀爀⼀⤀ഀഀ
 * - [`removeClass()`](http://api.jquery.com/removeClass/)਍ ⨀ ⴀ 嬀怀爀攀洀漀瘀攀䐀愀琀愀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀爀攀洀漀瘀攀䐀愀琀愀⼀⤀ഀഀ
 * - [`replaceWith()`](http://api.jquery.com/replaceWith/)਍ ⨀ ⴀ 嬀怀琀攀砀琀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀琀攀砀琀⼀⤀ഀഀ
 * - [`toggleClass()`](http://api.jquery.com/toggleClass/)਍ ⨀ ⴀ 嬀怀琀爀椀最最攀爀䠀愀渀搀氀攀爀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀琀爀椀最最攀爀䠀愀渀搀氀攀爀⼀⤀ ⴀ 倀愀猀猀攀猀 愀 搀甀洀洀礀 攀瘀攀渀琀 漀戀樀攀挀琀 琀漀 栀愀渀搀氀攀爀猀⸀ഀഀ
 * - [`unbind()`](http://api.jquery.com/off/) - Does not support namespaces਍ ⨀ ⴀ 嬀怀瘀愀氀⠀⤀怀崀⠀栀琀琀瀀㨀⼀⼀愀瀀椀⸀樀焀甀攀爀礀⸀挀漀洀⼀瘀愀氀⼀⤀ഀഀ
 * - [`wrap()`](http://api.jquery.com/wrap/)਍ ⨀ഀഀ
 * ## jQuery/jqLite Extras਍ ⨀ 䄀渀最甀氀愀爀 愀氀猀漀 瀀爀漀瘀椀搀攀猀 琀栀攀 昀漀氀氀漀眀椀渀最 愀搀搀椀琀椀漀渀愀氀 洀攀琀栀漀搀猀 愀渀搀 攀瘀攀渀琀猀 琀漀 戀漀琀栀 樀儀甀攀爀礀 愀渀搀 樀焀䰀椀琀攀㨀ഀഀ
 *਍ ⨀ ⌀⌀⌀ 䔀瘀攀渀琀猀ഀഀ
 * - `$destroy` - AngularJS intercepts all jqLite/jQuery's DOM destruction apis and fires this event਍ ⨀    漀渀 愀氀氀 䐀伀䴀 渀漀搀攀猀 戀攀椀渀最 爀攀洀漀瘀攀搀⸀  吀栀椀猀 挀愀渀 戀攀 甀猀攀搀 琀漀 挀氀攀愀渀 甀瀀 愀渀礀 ㌀爀搀 瀀愀爀琀礀 戀椀渀搀椀渀最猀 琀漀 琀栀攀 䐀伀䴀ഀഀ
 *    element before it is removed.਍ ⨀ഀഀ
 * ### Methods਍ ⨀ ⴀ 怀挀漀渀琀爀漀氀氀攀爀⠀渀愀洀攀⤀怀 ⴀ 爀攀琀爀椀攀瘀攀猀 琀栀攀 挀漀渀琀爀漀氀氀攀爀 漀昀 琀栀攀 挀甀爀爀攀渀琀 攀氀攀洀攀渀琀 漀爀 椀琀猀 瀀愀爀攀渀琀⸀ 䈀礀 搀攀昀愀甀氀琀ഀഀ
 *   retrieves controller associated with the `ngController` directive. If `name` is provided as਍ ⨀   挀愀洀攀氀䌀愀猀攀 搀椀爀攀挀琀椀瘀攀 渀愀洀攀Ⰰ 琀栀攀渀 琀栀攀 挀漀渀琀爀漀氀氀攀爀 昀漀爀 琀栀椀猀 搀椀爀攀挀琀椀瘀攀 眀椀氀氀 戀攀 爀攀琀爀椀攀瘀攀搀 ⠀攀⸀最⸀ഀഀ
 *   `'ngModel'`).਍ ⨀ ⴀ 怀椀渀樀攀挀琀漀爀⠀⤀怀 ⴀ 爀攀琀爀椀攀瘀攀猀 琀栀攀 椀渀樀攀挀琀漀爀 漀昀 琀栀攀 挀甀爀爀攀渀琀 攀氀攀洀攀渀琀 漀爀 椀琀猀 瀀愀爀攀渀琀⸀ഀഀ
 * - `scope()` - retrieves the {@link api/ng.$rootScope.Scope scope} of the current਍ ⨀   攀氀攀洀攀渀琀 漀爀 椀琀猀 瀀愀爀攀渀琀⸀ഀഀ
 * - `isolateScope()` - retrieves an isolate {@link api/ng.$rootScope.Scope scope} if one is attached directly to the਍ ⨀   挀甀爀爀攀渀琀 攀氀攀洀攀渀琀⸀ 吀栀椀猀 最攀琀琀攀爀 猀栀漀甀氀搀 戀攀 甀猀攀搀 漀渀氀礀 漀渀 攀氀攀洀攀渀琀猀 琀栀愀琀 挀漀渀琀愀椀渀 愀 搀椀爀攀挀琀椀瘀攀 眀栀椀挀栀 猀琀愀爀琀猀 愀 渀攀眀 椀猀漀氀愀琀攀ഀഀ
 *   scope. Calling `scope()` on this element always returns the original non-isolate scope.਍ ⨀ ⴀ 怀椀渀栀攀爀椀琀攀搀䐀愀琀愀⠀⤀怀 ⴀ 猀愀洀攀 愀猀 怀搀愀琀愀⠀⤀怀Ⰰ 戀甀琀 眀愀氀欀猀 甀瀀 琀栀攀 䐀伀䴀 甀渀琀椀氀 愀 瘀愀氀甀攀 椀猀 昀漀甀渀搀 漀爀 琀栀攀 琀漀瀀ഀഀ
 *   parent element is reached.਍ ⨀ഀഀ
 * @param {string|DOMElement} element HTML string or DOMElement to be wrapped into jQuery.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀伀戀樀攀挀琀紀 樀儀甀攀爀礀 漀戀樀攀挀琀⸀ഀഀ
 */਍ഀഀ
var jqCache = JQLite.cache = {},਍    樀焀一愀洀攀 㴀 䨀儀䰀椀琀攀⸀攀砀瀀愀渀搀漀 㴀 ✀渀最ⴀ✀ ⬀ 渀攀眀 䐀愀琀攀⠀⤀⸀最攀琀吀椀洀攀⠀⤀Ⰰഀഀ
    jqId = 1,਍    愀搀搀䔀瘀攀渀琀䰀椀猀琀攀渀攀爀䘀渀 㴀 ⠀眀椀渀搀漀眀⸀搀漀挀甀洀攀渀琀⸀愀搀搀䔀瘀攀渀琀䰀椀猀琀攀渀攀爀ഀഀ
      ? function(element, type, fn) {element.addEventListener(type, fn, false);}਍      㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 琀礀瀀攀Ⰰ 昀渀⤀ 笀攀氀攀洀攀渀琀⸀愀琀琀愀挀栀䔀瘀攀渀琀⠀✀漀渀✀ ⬀ 琀礀瀀攀Ⰰ 昀渀⤀㬀紀⤀Ⰰഀഀ
    removeEventListenerFn = (window.document.removeEventListener਍      㼀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 琀礀瀀攀Ⰰ 昀渀⤀ 笀攀氀攀洀攀渀琀⸀爀攀洀漀瘀攀䔀瘀攀渀琀䰀椀猀琀攀渀攀爀⠀琀礀瀀攀Ⰰ 昀渀Ⰰ 昀愀氀猀攀⤀㬀 紀ഀഀ
      : function(element, type, fn) {element.detachEvent('on' + type, fn); });਍ഀഀ
function jqNextId() { return ++jqId; }਍ഀഀ
਍瘀愀爀 匀倀䔀䌀䤀䄀䰀开䌀䠀䄀刀匀开刀䔀䜀䔀堀倀 㴀 ⼀⠀嬀尀㨀尀ⴀ尀开崀⬀⠀⸀⤀⤀⼀最㬀ഀഀ
var MOZ_HACK_REGEXP = /^moz([A-Z])/;਍瘀愀爀 樀焀䰀椀琀攀䴀椀渀䔀爀爀 㴀 洀椀渀䔀爀爀⠀✀樀焀䰀椀琀攀✀⤀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * Converts snake_case to camelCase.਍ ⨀ 䄀氀猀漀 琀栀攀爀攀 椀猀 猀瀀攀挀椀愀氀 挀愀猀攀 昀漀爀 䴀漀稀 瀀爀攀昀椀砀 猀琀愀爀琀椀渀最 眀椀琀栀 甀瀀瀀攀爀 挀愀猀攀 氀攀琀琀攀爀⸀ഀഀ
 * @param name Name to normalize਍ ⨀⼀ഀഀ
function camelCase(name) {਍  爀攀琀甀爀渀 渀愀洀攀⸀ഀഀ
    replace(SPECIAL_CHARS_REGEXP, function(_, separator, letter, offset) {਍      爀攀琀甀爀渀 漀昀昀猀攀琀 㼀 氀攀琀琀攀爀⸀琀漀唀瀀瀀攀爀䌀愀猀攀⠀⤀ 㨀 氀攀琀琀攀爀㬀ഀഀ
    }).਍    爀攀瀀氀愀挀攀⠀䴀伀娀开䠀䄀䌀䬀开刀䔀䜀䔀堀倀Ⰰ ✀䴀漀稀␀㄀✀⤀㬀ഀഀ
}਍ഀഀ
/////////////////////////////////////////////਍⼀⼀ 樀儀甀攀爀礀 洀甀琀愀琀椀漀渀 瀀愀琀挀栀ഀഀ
//਍⼀⼀ 䤀渀 挀漀渀樀甀渀挀琀椀漀渀 眀椀琀栀 戀椀渀搀䨀儀甀攀爀礀 椀渀琀攀爀挀攀瀀琀猀 愀氀氀 樀儀甀攀爀礀✀猀 䐀伀䴀 搀攀猀琀爀甀挀琀椀漀渀 愀瀀椀猀 愀渀搀 昀椀爀攀猀 愀ഀഀ
// $destroy event on all DOM nodes being removed.਍⼀⼀ഀഀ
/////////////////////////////////////////////਍ഀഀ
function jqLitePatchJQueryRemove(name, dispatchThis, filterElems, getterIfNoArguments) {਍  瘀愀爀 漀爀椀最椀渀愀氀䨀焀䘀渀 㴀 樀儀甀攀爀礀⸀昀渀嬀渀愀洀攀崀㬀ഀഀ
  originalJqFn = originalJqFn.$original || originalJqFn;਍  爀攀洀漀瘀攀倀愀琀挀栀⸀␀漀爀椀最椀渀愀氀 㴀 漀爀椀最椀渀愀氀䨀焀䘀渀㬀ഀഀ
  jQuery.fn[name] = removePatch;਍ഀഀ
  function removePatch(param) {਍    ⼀⼀ 樀猀栀椀渀琀 ⴀ圀　㐀　ഀഀ
    var list = filterElems && param ? [this.filter(param)] : [this],਍        昀椀爀攀䔀瘀攀渀琀 㴀 搀椀猀瀀愀琀挀栀吀栀椀猀Ⰰഀഀ
        set, setIndex, setLength,਍        攀氀攀洀攀渀琀Ⰰ 挀栀椀氀搀䤀渀搀攀砀Ⰰ 挀栀椀氀搀䰀攀渀最琀栀Ⰰ 挀栀椀氀搀爀攀渀㬀ഀഀ
਍    椀昀 ⠀℀最攀琀琀攀爀䤀昀一漀䄀爀最甀洀攀渀琀猀 簀簀 瀀愀爀愀洀 ℀㴀 渀甀氀氀⤀ 笀ഀഀ
      while(list.length) {਍        猀攀琀 㴀 氀椀猀琀⸀猀栀椀昀琀⠀⤀㬀ഀഀ
        for(setIndex = 0, setLength = set.length; setIndex < setLength; setIndex++) {਍          攀氀攀洀攀渀琀 㴀 樀焀䰀椀琀攀⠀猀攀琀嬀猀攀琀䤀渀搀攀砀崀⤀㬀ഀഀ
          if (fireEvent) {਍            攀氀攀洀攀渀琀⸀琀爀椀最最攀爀䠀愀渀搀氀攀爀⠀✀␀搀攀猀琀爀漀礀✀⤀㬀ഀഀ
          } else {਍            昀椀爀攀䔀瘀攀渀琀 㴀 ℀昀椀爀攀䔀瘀攀渀琀㬀ഀഀ
          }਍          昀漀爀⠀挀栀椀氀搀䤀渀搀攀砀 㴀 　Ⰰ 挀栀椀氀搀䰀攀渀最琀栀 㴀 ⠀挀栀椀氀搀爀攀渀 㴀 攀氀攀洀攀渀琀⸀挀栀椀氀搀爀攀渀⠀⤀⤀⸀氀攀渀最琀栀㬀ഀഀ
              childIndex < childLength;਍              挀栀椀氀搀䤀渀搀攀砀⬀⬀⤀ 笀ഀഀ
            list.push(jQuery(children[childIndex]));਍          紀ഀഀ
        }਍      紀ഀഀ
    }਍    爀攀琀甀爀渀 漀爀椀最椀渀愀氀䨀焀䘀渀⸀愀瀀瀀氀礀⠀琀栀椀猀Ⰰ 愀爀最甀洀攀渀琀猀⤀㬀ഀഀ
  }਍紀ഀഀ
਍⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
function JQLite(element) {਍  椀昀 ⠀攀氀攀洀攀渀琀 椀渀猀琀愀渀挀攀漀昀 䨀儀䰀椀琀攀⤀ 笀ഀഀ
    return element;਍  紀ഀഀ
  if (!(this instanceof JQLite)) {਍    椀昀 ⠀椀猀匀琀爀椀渀最⠀攀氀攀洀攀渀琀⤀ ☀☀ 攀氀攀洀攀渀琀⸀挀栀愀爀䄀琀⠀　⤀ ℀㴀 ✀㰀✀⤀ 笀ഀഀ
      throw jqLiteMinErr('nosel', 'Looking up elements via selectors is not supported by jqLite! See: http://docs.angularjs.org/api/angular.element');਍    紀ഀഀ
    return new JQLite(element);਍  紀ഀഀ
਍  椀昀 ⠀椀猀匀琀爀椀渀最⠀攀氀攀洀攀渀琀⤀⤀ 笀ഀഀ
    var div = document.createElement('div');਍    ⼀⼀ 刀攀愀搀 愀戀漀甀琀 琀栀攀 一漀匀挀漀瀀攀 攀氀攀洀攀渀琀猀 栀攀爀攀㨀ഀഀ
    // http://msdn.microsoft.com/en-us/library/ms533897(VS.85).aspx਍    搀椀瘀⸀椀渀渀攀爀䠀吀䴀䰀 㴀 ✀㰀搀椀瘀㸀☀⌀㄀㘀　㬀㰀⼀搀椀瘀㸀✀ ⬀ 攀氀攀洀攀渀琀㬀 ⼀⼀ 䤀䔀 椀渀猀愀渀椀琀礀 琀漀 洀愀欀攀 一漀匀挀漀瀀攀 攀氀攀洀攀渀琀猀 眀漀爀欀℀ഀഀ
    div.removeChild(div.firstChild); // remove the superfluous div਍    樀焀䰀椀琀攀䄀搀搀一漀搀攀猀⠀琀栀椀猀Ⰰ 搀椀瘀⸀挀栀椀氀搀一漀搀攀猀⤀㬀ഀഀ
    var fragment = jqLite(document.createDocumentFragment());਍    昀爀愀最洀攀渀琀⸀愀瀀瀀攀渀搀⠀琀栀椀猀⤀㬀 ⼀⼀ 搀攀琀愀挀栀 琀栀攀 攀氀攀洀攀渀琀猀 昀爀漀洀 琀栀攀 琀攀洀瀀漀爀愀爀礀 䐀伀䴀 搀椀瘀⸀ഀഀ
  } else {਍    樀焀䰀椀琀攀䄀搀搀一漀搀攀猀⠀琀栀椀猀Ⰰ 攀氀攀洀攀渀琀⤀㬀ഀഀ
  }਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 樀焀䰀椀琀攀䌀氀漀渀攀⠀攀氀攀洀攀渀琀⤀ 笀ഀഀ
  return element.cloneNode(true);਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 樀焀䰀椀琀攀䐀攀愀氀漀挀⠀攀氀攀洀攀渀琀⤀笀ഀഀ
  jqLiteRemoveData(element);਍  昀漀爀 ⠀ 瘀愀爀 椀 㴀 　Ⰰ 挀栀椀氀搀爀攀渀 㴀 攀氀攀洀攀渀琀⸀挀栀椀氀搀一漀搀攀猀 簀簀 嬀崀㬀 椀 㰀 挀栀椀氀搀爀攀渀⸀氀攀渀最琀栀㬀 椀⬀⬀⤀ 笀ഀഀ
    jqLiteDealoc(children[i]);਍  紀ഀഀ
}਍ഀഀ
function jqLiteOff(element, type, fn, unsupported) {਍  椀昀 ⠀椀猀䐀攀昀椀渀攀搀⠀甀渀猀甀瀀瀀漀爀琀攀搀⤀⤀ 琀栀爀漀眀 樀焀䰀椀琀攀䴀椀渀䔀爀爀⠀✀漀昀昀愀爀最猀✀Ⰰ ✀樀焀䰀椀琀攀⌀漀昀昀⠀⤀ 搀漀攀猀 渀漀琀 猀甀瀀瀀漀爀琀 琀栀攀 怀猀攀氀攀挀琀漀爀怀 愀爀最甀洀攀渀琀✀⤀㬀ഀഀ
਍  瘀愀爀 攀瘀攀渀琀猀 㴀 樀焀䰀椀琀攀䔀砀瀀愀渀搀漀匀琀漀爀攀⠀攀氀攀洀攀渀琀Ⰰ ✀攀瘀攀渀琀猀✀⤀Ⰰഀഀ
      handle = jqLiteExpandoStore(element, 'handle');਍ഀഀ
  if (!handle) return; //no listeners registered਍ഀഀ
  if (isUndefined(type)) {਍    昀漀爀䔀愀挀栀⠀攀瘀攀渀琀猀Ⰰ 昀甀渀挀琀椀漀渀⠀攀瘀攀渀琀䠀愀渀搀氀攀爀Ⰰ 琀礀瀀攀⤀ 笀ഀഀ
      removeEventListenerFn(element, type, eventHandler);਍      搀攀氀攀琀攀 攀瘀攀渀琀猀嬀琀礀瀀攀崀㬀ഀഀ
    });਍  紀 攀氀猀攀 笀ഀഀ
    forEach(type.split(' '), function(type) {਍      椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀昀渀⤀⤀ 笀ഀഀ
        removeEventListenerFn(element, type, events[type]);਍        搀攀氀攀琀攀 攀瘀攀渀琀猀嬀琀礀瀀攀崀㬀ഀഀ
      } else {਍        愀爀爀愀礀刀攀洀漀瘀攀⠀攀瘀攀渀琀猀嬀琀礀瀀攀崀 簀簀 嬀崀Ⰰ 昀渀⤀㬀ഀഀ
      }਍    紀⤀㬀ഀഀ
  }਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 樀焀䰀椀琀攀刀攀洀漀瘀攀䐀愀琀愀⠀攀氀攀洀攀渀琀Ⰰ 渀愀洀攀⤀ 笀ഀഀ
  var expandoId = element[jqName],਍      攀砀瀀愀渀搀漀匀琀漀爀攀 㴀 樀焀䌀愀挀栀攀嬀攀砀瀀愀渀搀漀䤀搀崀㬀ഀഀ
਍  椀昀 ⠀攀砀瀀愀渀搀漀匀琀漀爀攀⤀ 笀ഀഀ
    if (name) {਍      搀攀氀攀琀攀 樀焀䌀愀挀栀攀嬀攀砀瀀愀渀搀漀䤀搀崀⸀搀愀琀愀嬀渀愀洀攀崀㬀ഀഀ
      return;਍    紀ഀഀ
਍    椀昀 ⠀攀砀瀀愀渀搀漀匀琀漀爀攀⸀栀愀渀搀氀攀⤀ 笀ഀഀ
      expandoStore.events.$destroy && expandoStore.handle({}, '$destroy');਍      樀焀䰀椀琀攀伀昀昀⠀攀氀攀洀攀渀琀⤀㬀ഀഀ
    }਍    搀攀氀攀琀攀 樀焀䌀愀挀栀攀嬀攀砀瀀愀渀搀漀䤀搀崀㬀ഀഀ
    element[jqName] = undefined; // ie does not allow deletion of attributes on elements.਍  紀ഀഀ
}਍ഀഀ
function jqLiteExpandoStore(element, key, value) {਍  瘀愀爀 攀砀瀀愀渀搀漀䤀搀 㴀 攀氀攀洀攀渀琀嬀樀焀一愀洀攀崀Ⰰഀഀ
      expandoStore = jqCache[expandoId || -1];਍ഀഀ
  if (isDefined(value)) {਍    椀昀 ⠀℀攀砀瀀愀渀搀漀匀琀漀爀攀⤀ 笀ഀഀ
      element[jqName] = expandoId = jqNextId();਍      攀砀瀀愀渀搀漀匀琀漀爀攀 㴀 樀焀䌀愀挀栀攀嬀攀砀瀀愀渀搀漀䤀搀崀 㴀 笀紀㬀ഀഀ
    }਍    攀砀瀀愀渀搀漀匀琀漀爀攀嬀欀攀礀崀 㴀 瘀愀氀甀攀㬀ഀഀ
  } else {਍    爀攀琀甀爀渀 攀砀瀀愀渀搀漀匀琀漀爀攀 ☀☀ 攀砀瀀愀渀搀漀匀琀漀爀攀嬀欀攀礀崀㬀ഀഀ
  }਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 樀焀䰀椀琀攀䐀愀琀愀⠀攀氀攀洀攀渀琀Ⰰ 欀攀礀Ⰰ 瘀愀氀甀攀⤀ 笀ഀഀ
  var data = jqLiteExpandoStore(element, 'data'),਍      椀猀匀攀琀琀攀爀 㴀 椀猀䐀攀昀椀渀攀搀⠀瘀愀氀甀攀⤀Ⰰഀഀ
      keyDefined = !isSetter && isDefined(key),਍      椀猀匀椀洀瀀氀攀䜀攀琀琀攀爀 㴀 欀攀礀䐀攀昀椀渀攀搀 ☀☀ ℀椀猀伀戀樀攀挀琀⠀欀攀礀⤀㬀ഀഀ
਍  椀昀 ⠀℀搀愀琀愀 ☀☀ ℀椀猀匀椀洀瀀氀攀䜀攀琀琀攀爀⤀ 笀ഀഀ
    jqLiteExpandoStore(element, 'data', data = {});਍  紀ഀഀ
਍  椀昀 ⠀椀猀匀攀琀琀攀爀⤀ 笀ഀഀ
    data[key] = value;਍  紀 攀氀猀攀 笀ഀഀ
    if (keyDefined) {਍      椀昀 ⠀椀猀匀椀洀瀀氀攀䜀攀琀琀攀爀⤀ 笀ഀഀ
        // don't create data in this case.਍        爀攀琀甀爀渀 搀愀琀愀 ☀☀ 搀愀琀愀嬀欀攀礀崀㬀ഀഀ
      } else {਍        攀砀琀攀渀搀⠀搀愀琀愀Ⰰ 欀攀礀⤀㬀ഀഀ
      }਍    紀 攀氀猀攀 笀ഀഀ
      return data;਍    紀ഀഀ
  }਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 樀焀䰀椀琀攀䠀愀猀䌀氀愀猀猀⠀攀氀攀洀攀渀琀Ⰰ 猀攀氀攀挀琀漀爀⤀ 笀ഀഀ
  if (!element.getAttribute) return false;਍  爀攀琀甀爀渀 ⠀⠀∀ ∀ ⬀ ⠀攀氀攀洀攀渀琀⸀最攀琀䄀琀琀爀椀戀甀琀攀⠀✀挀氀愀猀猀✀⤀ 簀簀 ✀✀⤀ ⬀ ∀ ∀⤀⸀爀攀瀀氀愀挀攀⠀⼀嬀尀渀尀琀崀⼀最Ⰰ ∀ ∀⤀⸀ഀഀ
      indexOf( " " + selector + " " ) > -1);਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 樀焀䰀椀琀攀刀攀洀漀瘀攀䌀氀愀猀猀⠀攀氀攀洀攀渀琀Ⰰ 挀猀猀䌀氀愀猀猀攀猀⤀ 笀ഀഀ
  if (cssClasses && element.setAttribute) {਍    昀漀爀䔀愀挀栀⠀挀猀猀䌀氀愀猀猀攀猀⸀猀瀀氀椀琀⠀✀ ✀⤀Ⰰ 昀甀渀挀琀椀漀渀⠀挀猀猀䌀氀愀猀猀⤀ 笀ഀഀ
      element.setAttribute('class', trim(਍          ⠀∀ ∀ ⬀ ⠀攀氀攀洀攀渀琀⸀最攀琀䄀琀琀爀椀戀甀琀攀⠀✀挀氀愀猀猀✀⤀ 簀簀 ✀✀⤀ ⬀ ∀ ∀⤀ഀഀ
          .replace(/[\n\t]/g, " ")਍          ⸀爀攀瀀氀愀挀攀⠀∀ ∀ ⬀ 琀爀椀洀⠀挀猀猀䌀氀愀猀猀⤀ ⬀ ∀ ∀Ⰰ ∀ ∀⤀⤀ഀഀ
      );਍    紀⤀㬀ഀഀ
  }਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 樀焀䰀椀琀攀䄀搀搀䌀氀愀猀猀⠀攀氀攀洀攀渀琀Ⰰ 挀猀猀䌀氀愀猀猀攀猀⤀ 笀ഀഀ
  if (cssClasses && element.setAttribute) {਍    瘀愀爀 攀砀椀猀琀椀渀最䌀氀愀猀猀攀猀 㴀 ⠀✀ ✀ ⬀ ⠀攀氀攀洀攀渀琀⸀最攀琀䄀琀琀爀椀戀甀琀攀⠀✀挀氀愀猀猀✀⤀ 簀簀 ✀✀⤀ ⬀ ✀ ✀⤀ഀഀ
                            .replace(/[\n\t]/g, " ");਍ഀഀ
    forEach(cssClasses.split(' '), function(cssClass) {਍      挀猀猀䌀氀愀猀猀 㴀 琀爀椀洀⠀挀猀猀䌀氀愀猀猀⤀㬀ഀഀ
      if (existingClasses.indexOf(' ' + cssClass + ' ') === -1) {਍        攀砀椀猀琀椀渀最䌀氀愀猀猀攀猀 ⬀㴀 挀猀猀䌀氀愀猀猀 ⬀ ✀ ✀㬀ഀഀ
      }਍    紀⤀㬀ഀഀ
਍    攀氀攀洀攀渀琀⸀猀攀琀䄀琀琀爀椀戀甀琀攀⠀✀挀氀愀猀猀✀Ⰰ 琀爀椀洀⠀攀砀椀猀琀椀渀最䌀氀愀猀猀攀猀⤀⤀㬀ഀഀ
  }਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 樀焀䰀椀琀攀䄀搀搀一漀搀攀猀⠀爀漀漀琀Ⰰ 攀氀攀洀攀渀琀猀⤀ 笀ഀഀ
  if (elements) {਍    攀氀攀洀攀渀琀猀 㴀 ⠀℀攀氀攀洀攀渀琀猀⸀渀漀搀攀一愀洀攀 ☀☀ 椀猀䐀攀昀椀渀攀搀⠀攀氀攀洀攀渀琀猀⸀氀攀渀最琀栀⤀ ☀☀ ℀椀猀圀椀渀搀漀眀⠀攀氀攀洀攀渀琀猀⤀⤀ഀഀ
      ? elements਍      㨀 嬀 攀氀攀洀攀渀琀猀 崀㬀ഀഀ
    for(var i=0; i < elements.length; i++) {਍      爀漀漀琀⸀瀀甀猀栀⠀攀氀攀洀攀渀琀猀嬀椀崀⤀㬀ഀഀ
    }਍  紀ഀഀ
}਍ഀഀ
function jqLiteController(element, name) {਍  爀攀琀甀爀渀 樀焀䰀椀琀攀䤀渀栀攀爀椀琀攀搀䐀愀琀愀⠀攀氀攀洀攀渀琀Ⰰ ✀␀✀ ⬀ ⠀渀愀洀攀 簀簀 ✀渀最䌀漀渀琀爀漀氀氀攀爀✀ ⤀ ⬀ ✀䌀漀渀琀爀漀氀氀攀爀✀⤀㬀ഀഀ
}਍ഀഀ
function jqLiteInheritedData(element, name, value) {਍  攀氀攀洀攀渀琀 㴀 樀焀䰀椀琀攀⠀攀氀攀洀攀渀琀⤀㬀ഀഀ
਍  ⼀⼀ 椀昀 攀氀攀洀攀渀琀 椀猀 琀栀攀 搀漀挀甀洀攀渀琀 漀戀樀攀挀琀 眀漀爀欀 眀椀琀栀 琀栀攀 栀琀洀氀 攀氀攀洀攀渀琀 椀渀猀琀攀愀搀ഀഀ
  // this makes $(document).scope() possible਍  椀昀⠀攀氀攀洀攀渀琀嬀　崀⸀渀漀搀攀吀礀瀀攀 㴀㴀 㤀⤀ 笀ഀഀ
    element = element.find('html');਍  紀ഀഀ
  var names = isArray(name) ? name : [name];਍ഀഀ
  while (element.length) {਍ഀഀ
    for (var i = 0, ii = names.length; i < ii; i++) {਍      椀昀 ⠀⠀瘀愀氀甀攀 㴀 攀氀攀洀攀渀琀⸀搀愀琀愀⠀渀愀洀攀猀嬀椀崀⤀⤀ ℀㴀㴀 甀渀搀攀昀椀渀攀搀⤀ 爀攀琀甀爀渀 瘀愀氀甀攀㬀ഀഀ
    }਍    攀氀攀洀攀渀琀 㴀 攀氀攀洀攀渀琀⸀瀀愀爀攀渀琀⠀⤀㬀ഀഀ
  }਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 樀焀䰀椀琀攀䔀洀瀀琀礀⠀攀氀攀洀攀渀琀⤀ 笀ഀഀ
  for (var i = 0, childNodes = element.childNodes; i < childNodes.length; i++) {਍    樀焀䰀椀琀攀䐀攀愀氀漀挀⠀挀栀椀氀搀一漀搀攀猀嬀椀崀⤀㬀ഀഀ
  }਍  眀栀椀氀攀 ⠀攀氀攀洀攀渀琀⸀昀椀爀猀琀䌀栀椀氀搀⤀ 笀ഀഀ
    element.removeChild(element.firstChild);਍  紀ഀഀ
}਍ഀഀ
//////////////////////////////////////////਍⼀⼀ 䘀甀渀挀琀椀漀渀猀 眀栀椀挀栀 愀爀攀 搀攀挀氀愀爀攀搀 搀椀爀攀挀琀氀礀⸀ഀഀ
//////////////////////////////////////////਍瘀愀爀 䨀儀䰀椀琀攀倀爀漀琀漀琀礀瀀攀 㴀 䨀儀䰀椀琀攀⸀瀀爀漀琀漀琀礀瀀攀 㴀 笀ഀഀ
  ready: function(fn) {਍    瘀愀爀 昀椀爀攀搀 㴀 昀愀氀猀攀㬀ഀഀ
਍    昀甀渀挀琀椀漀渀 琀爀椀最最攀爀⠀⤀ 笀ഀഀ
      if (fired) return;਍      昀椀爀攀搀 㴀 琀爀甀攀㬀ഀഀ
      fn();਍    紀ഀഀ
਍    ⼀⼀ 挀栀攀挀欀 椀昀 搀漀挀甀洀攀渀琀 愀氀爀攀愀搀礀 椀猀 氀漀愀搀攀搀ഀഀ
    if (document.readyState === 'complete'){਍      猀攀琀吀椀洀攀漀甀琀⠀琀爀椀最最攀爀⤀㬀ഀഀ
    } else {਍      琀栀椀猀⸀漀渀⠀✀䐀伀䴀䌀漀渀琀攀渀琀䰀漀愀搀攀搀✀Ⰰ 琀爀椀最最攀爀⤀㬀 ⼀⼀ 眀漀爀欀猀 昀漀爀 洀漀搀攀爀渀 戀爀漀眀猀攀爀猀 愀渀搀 䤀䔀㤀ഀഀ
      // we can not use jqLite since we are not done loading and jQuery could be loaded later.਍      ⼀⼀ 樀猀栀椀渀琀 ⴀ圀　㘀㐀ഀഀ
      JQLite(window).on('load', trigger); // fallback to window.onload for others਍      ⼀⼀ 樀猀栀椀渀琀 ⬀圀　㘀㐀ഀഀ
    }਍  紀Ⰰഀഀ
  toString: function() {਍    瘀愀爀 瘀愀氀甀攀 㴀 嬀崀㬀ഀഀ
    forEach(this, function(e){ value.push('' + e);});਍    爀攀琀甀爀渀 ✀嬀✀ ⬀ 瘀愀氀甀攀⸀樀漀椀渀⠀✀Ⰰ ✀⤀ ⬀ ✀崀✀㬀ഀഀ
  },਍ഀഀ
  eq: function(index) {਍      爀攀琀甀爀渀 ⠀椀渀搀攀砀 㸀㴀 　⤀ 㼀 樀焀䰀椀琀攀⠀琀栀椀猀嬀椀渀搀攀砀崀⤀ 㨀 樀焀䰀椀琀攀⠀琀栀椀猀嬀琀栀椀猀⸀氀攀渀最琀栀 ⬀ 椀渀搀攀砀崀⤀㬀ഀഀ
  },਍ഀഀ
  length: 0,਍  瀀甀猀栀㨀 瀀甀猀栀Ⰰഀഀ
  sort: [].sort,਍  猀瀀氀椀挀攀㨀 嬀崀⸀猀瀀氀椀挀攀ഀഀ
};਍ഀഀ
//////////////////////////////////////////਍⼀⼀ 䘀甀渀挀琀椀漀渀猀 椀琀攀爀愀琀椀渀最 最攀琀琀攀爀⼀猀攀琀琀攀爀猀⸀ഀഀ
// these functions return self on setter and਍⼀⼀ 瘀愀氀甀攀 漀渀 最攀琀⸀ഀഀ
//////////////////////////////////////////਍瘀愀爀 䈀伀伀䰀䔀䄀一开䄀吀吀刀 㴀 笀紀㬀ഀഀ
forEach('multiple,selected,checked,disabled,readOnly,required,open'.split(','), function(value) {਍  䈀伀伀䰀䔀䄀一开䄀吀吀刀嬀氀漀眀攀爀挀愀猀攀⠀瘀愀氀甀攀⤀崀 㴀 瘀愀氀甀攀㬀ഀഀ
});਍瘀愀爀 䈀伀伀䰀䔀䄀一开䔀䰀䔀䴀䔀一吀匀 㴀 笀紀㬀ഀഀ
forEach('input,select,option,textarea,button,form,details'.split(','), function(value) {਍  䈀伀伀䰀䔀䄀一开䔀䰀䔀䴀䔀一吀匀嬀甀瀀瀀攀爀挀愀猀攀⠀瘀愀氀甀攀⤀崀 㴀 琀爀甀攀㬀ഀഀ
});਍ഀഀ
function getBooleanAttrName(element, name) {਍  ⼀⼀ 挀栀攀挀欀 搀漀洀 氀愀猀琀 猀椀渀挀攀 眀攀 眀椀氀氀 洀漀猀琀 氀椀欀攀氀礀 昀愀椀氀 漀渀 渀愀洀攀ഀഀ
  var booleanAttr = BOOLEAN_ATTR[name.toLowerCase()];਍ഀഀ
  // booleanAttr is here twice to minimize DOM access਍  爀攀琀甀爀渀 戀漀漀氀攀愀渀䄀琀琀爀 ☀☀ 䈀伀伀䰀䔀䄀一开䔀䰀䔀䴀䔀一吀匀嬀攀氀攀洀攀渀琀⸀渀漀搀攀一愀洀攀崀 ☀☀ 戀漀漀氀攀愀渀䄀琀琀爀㬀ഀഀ
}਍ഀഀ
forEach({਍  搀愀琀愀㨀 樀焀䰀椀琀攀䐀愀琀愀Ⰰഀഀ
  inheritedData: jqLiteInheritedData,਍ഀഀ
  scope: function(element) {਍    ⼀⼀ 䌀愀渀✀琀 甀猀攀 樀焀䰀椀琀攀䐀愀琀愀 栀攀爀攀 搀椀爀攀挀琀氀礀 猀漀 眀攀 猀琀愀礀 挀漀洀瀀愀琀椀戀氀攀 眀椀琀栀 樀儀甀攀爀礀℀ഀഀ
    return jqLite(element).data('$scope') || jqLiteInheritedData(element.parentNode || element, ['$isolateScope', '$scope']);਍  紀Ⰰഀഀ
਍  椀猀漀氀愀琀攀匀挀漀瀀攀㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀⤀ 笀ഀഀ
    // Can't use jqLiteData here directly so we stay compatible with jQuery!਍    爀攀琀甀爀渀 樀焀䰀椀琀攀⠀攀氀攀洀攀渀琀⤀⸀搀愀琀愀⠀✀␀椀猀漀氀愀琀攀匀挀漀瀀攀✀⤀ 簀簀 樀焀䰀椀琀攀⠀攀氀攀洀攀渀琀⤀⸀搀愀琀愀⠀✀␀椀猀漀氀愀琀攀匀挀漀瀀攀一漀吀攀洀瀀氀愀琀攀✀⤀㬀ഀഀ
  },਍ഀഀ
  controller: jqLiteController ,਍ഀഀ
  injector: function(element) {਍    爀攀琀甀爀渀 樀焀䰀椀琀攀䤀渀栀攀爀椀琀攀搀䐀愀琀愀⠀攀氀攀洀攀渀琀Ⰰ ✀␀椀渀樀攀挀琀漀爀✀⤀㬀ഀഀ
  },਍ഀഀ
  removeAttr: function(element,name) {਍    攀氀攀洀攀渀琀⸀爀攀洀漀瘀攀䄀琀琀爀椀戀甀琀攀⠀渀愀洀攀⤀㬀ഀഀ
  },਍ഀഀ
  hasClass: jqLiteHasClass,਍ഀഀ
  css: function(element, name, value) {਍    渀愀洀攀 㴀 挀愀洀攀氀䌀愀猀攀⠀渀愀洀攀⤀㬀ഀഀ
਍    椀昀 ⠀椀猀䐀攀昀椀渀攀搀⠀瘀愀氀甀攀⤀⤀ 笀ഀഀ
      element.style[name] = value;਍    紀 攀氀猀攀 笀ഀഀ
      var val;਍ഀഀ
      if (msie <= 8) {਍        ⼀⼀ 琀栀椀猀 椀猀 猀漀洀攀 䤀䔀 猀瀀攀挀椀昀椀挀 眀攀椀爀搀渀攀猀猀 琀栀愀琀 樀儀甀攀爀礀 ㄀⸀㘀⸀㐀 搀漀攀猀 渀漀琀 猀甀爀攀 眀栀礀ഀഀ
        val = element.currentStyle && element.currentStyle[name];਍        椀昀 ⠀瘀愀氀 㴀㴀㴀 ✀✀⤀ 瘀愀氀 㴀 ✀愀甀琀漀✀㬀ഀഀ
      }਍ഀഀ
      val = val || element.style[name];਍ഀഀ
      if (msie <= 8) {਍        ⼀⼀ 樀焀甀攀爀礀 眀攀椀爀搀渀攀猀猀 㨀ⴀ⼀ഀഀ
        val = (val === '') ? undefined : val;਍      紀ഀഀ
਍      爀攀琀甀爀渀  瘀愀氀㬀ഀഀ
    }਍  紀Ⰰഀഀ
਍  愀琀琀爀㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 渀愀洀攀Ⰰ 瘀愀氀甀攀⤀笀ഀഀ
    var lowercasedName = lowercase(name);਍    椀昀 ⠀䈀伀伀䰀䔀䄀一开䄀吀吀刀嬀氀漀眀攀爀挀愀猀攀搀一愀洀攀崀⤀ 笀ഀഀ
      if (isDefined(value)) {਍        椀昀 ⠀℀℀瘀愀氀甀攀⤀ 笀ഀഀ
          element[name] = true;਍          攀氀攀洀攀渀琀⸀猀攀琀䄀琀琀爀椀戀甀琀攀⠀渀愀洀攀Ⰰ 氀漀眀攀爀挀愀猀攀搀一愀洀攀⤀㬀ഀഀ
        } else {਍          攀氀攀洀攀渀琀嬀渀愀洀攀崀 㴀 昀愀氀猀攀㬀ഀഀ
          element.removeAttribute(lowercasedName);਍        紀ഀഀ
      } else {਍        爀攀琀甀爀渀 ⠀攀氀攀洀攀渀琀嬀渀愀洀攀崀 簀簀ഀഀ
                 (element.attributes.getNamedItem(name)|| noop).specified)਍               㼀 氀漀眀攀爀挀愀猀攀搀一愀洀攀ഀഀ
               : undefined;਍      紀ഀഀ
    } else if (isDefined(value)) {਍      攀氀攀洀攀渀琀⸀猀攀琀䄀琀琀爀椀戀甀琀攀⠀渀愀洀攀Ⰰ 瘀愀氀甀攀⤀㬀ഀഀ
    } else if (element.getAttribute) {਍      ⼀⼀ 琀栀攀 攀砀琀爀愀 愀爀最甀洀攀渀琀 ∀㈀∀ 椀猀 琀漀 最攀琀 琀栀攀 爀椀最栀琀 琀栀椀渀最 昀漀爀 愀⸀栀爀攀昀 椀渀 䤀䔀Ⰰ 猀攀攀 樀儀甀攀爀礀 挀漀搀攀ഀഀ
      // some elements (e.g. Document) don't have get attribute, so return undefined਍      瘀愀爀 爀攀琀 㴀 攀氀攀洀攀渀琀⸀最攀琀䄀琀琀爀椀戀甀琀攀⠀渀愀洀攀Ⰰ ㈀⤀㬀ഀഀ
      // normalize non-existing attributes to undefined (as jQuery)਍      爀攀琀甀爀渀 爀攀琀 㴀㴀㴀 渀甀氀氀 㼀 甀渀搀攀昀椀渀攀搀 㨀 爀攀琀㬀ഀഀ
    }਍  紀Ⰰഀഀ
਍  瀀爀漀瀀㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 渀愀洀攀Ⰰ 瘀愀氀甀攀⤀ 笀ഀഀ
    if (isDefined(value)) {਍      攀氀攀洀攀渀琀嬀渀愀洀攀崀 㴀 瘀愀氀甀攀㬀ഀഀ
    } else {਍      爀攀琀甀爀渀 攀氀攀洀攀渀琀嬀渀愀洀攀崀㬀ഀഀ
    }਍  紀Ⰰഀഀ
਍  琀攀砀琀㨀 ⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var NODE_TYPE_TEXT_PROPERTY = [];਍    椀昀 ⠀洀猀椀攀 㰀 㤀⤀ 笀ഀഀ
      NODE_TYPE_TEXT_PROPERTY[1] = 'innerText';    /** Element **/਍      一伀䐀䔀开吀夀倀䔀开吀䔀堀吀开倀刀伀倀䔀刀吀夀嬀㌀崀 㴀 ✀渀漀搀攀嘀愀氀甀攀✀㬀    ⼀⨀⨀ 吀攀砀琀 ⨀⨀⼀ഀഀ
    } else {਍      一伀䐀䔀开吀夀倀䔀开吀䔀堀吀开倀刀伀倀䔀刀吀夀嬀㄀崀 㴀                 ⼀⨀⨀ 䔀氀攀洀攀渀琀 ⨀⨀⼀ഀഀ
      NODE_TYPE_TEXT_PROPERTY[3] = 'textContent';  /** Text **/਍    紀ഀഀ
    getText.$dv = '';਍    爀攀琀甀爀渀 最攀琀吀攀砀琀㬀ഀഀ
਍    昀甀渀挀琀椀漀渀 最攀琀吀攀砀琀⠀攀氀攀洀攀渀琀Ⰰ 瘀愀氀甀攀⤀ 笀ഀഀ
      var textProp = NODE_TYPE_TEXT_PROPERTY[element.nodeType];਍      椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀瘀愀氀甀攀⤀⤀ 笀ഀഀ
        return textProp ? element[textProp] : '';਍      紀ഀഀ
      element[textProp] = value;਍    紀ഀഀ
  })(),਍ഀഀ
  val: function(element, value) {਍    椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀瘀愀氀甀攀⤀⤀ 笀ഀഀ
      if (nodeName_(element) === 'SELECT' && element.multiple) {਍        瘀愀爀 爀攀猀甀氀琀 㴀 嬀崀㬀ഀഀ
        forEach(element.options, function (option) {਍          椀昀 ⠀漀瀀琀椀漀渀⸀猀攀氀攀挀琀攀搀⤀ 笀ഀഀ
            result.push(option.value || option.text);਍          紀ഀഀ
        });਍        爀攀琀甀爀渀 爀攀猀甀氀琀⸀氀攀渀最琀栀 㴀㴀㴀 　 㼀 渀甀氀氀 㨀 爀攀猀甀氀琀㬀ഀഀ
      }਍      爀攀琀甀爀渀 攀氀攀洀攀渀琀⸀瘀愀氀甀攀㬀ഀഀ
    }਍    攀氀攀洀攀渀琀⸀瘀愀氀甀攀 㴀 瘀愀氀甀攀㬀ഀഀ
  },਍ഀഀ
  html: function(element, value) {਍    椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀瘀愀氀甀攀⤀⤀ 笀ഀഀ
      return element.innerHTML;਍    紀ഀഀ
    for (var i = 0, childNodes = element.childNodes; i < childNodes.length; i++) {਍      樀焀䰀椀琀攀䐀攀愀氀漀挀⠀挀栀椀氀搀一漀搀攀猀嬀椀崀⤀㬀ഀഀ
    }਍    攀氀攀洀攀渀琀⸀椀渀渀攀爀䠀吀䴀䰀 㴀 瘀愀氀甀攀㬀ഀഀ
  },਍ഀഀ
  empty: jqLiteEmpty਍紀Ⰰ 昀甀渀挀琀椀漀渀⠀昀渀Ⰰ 渀愀洀攀⤀笀ഀഀ
  /**਍   ⨀ 倀爀漀瀀攀爀琀椀攀猀㨀 眀爀椀琀攀猀 爀攀琀甀爀渀 猀攀氀攀挀琀椀漀渀Ⰰ 爀攀愀搀猀 爀攀琀甀爀渀 昀椀爀猀琀 瘀愀氀甀攀ഀഀ
   */਍  䨀儀䰀椀琀攀⸀瀀爀漀琀漀琀礀瀀攀嬀渀愀洀攀崀 㴀 昀甀渀挀琀椀漀渀⠀愀爀最㄀Ⰰ 愀爀最㈀⤀ 笀ഀഀ
    var i, key;਍ഀഀ
    // jqLiteHasClass has only two arguments, but is a getter-only fn, so we need to special-case it਍    ⼀⼀ 椀渀 愀 眀愀礀 琀栀愀琀 猀甀爀瘀椀瘀攀猀 洀椀渀椀昀椀挀愀琀椀漀渀⸀ഀഀ
    // jqLiteEmpty takes no arguments but is a setter.਍    椀昀 ⠀昀渀 ℀㴀㴀 樀焀䰀椀琀攀䔀洀瀀琀礀 ☀☀ഀഀ
        (((fn.length == 2 && (fn !== jqLiteHasClass && fn !== jqLiteController)) ? arg1 : arg2) === undefined)) {਍      椀昀 ⠀椀猀伀戀樀攀挀琀⠀愀爀最㄀⤀⤀ 笀ഀഀ
਍        ⼀⼀ 眀攀 愀爀攀 愀 眀爀椀琀攀Ⰰ 戀甀琀 琀栀攀 漀戀樀攀挀琀 瀀爀漀瀀攀爀琀椀攀猀 愀爀攀 琀栀攀 欀攀礀⼀瘀愀氀甀攀猀ഀഀ
        for (i = 0; i < this.length; i++) {਍          椀昀 ⠀昀渀 㴀㴀㴀 樀焀䰀椀琀攀䐀愀琀愀⤀ 笀ഀഀ
            // data() takes the whole object in jQuery਍            昀渀⠀琀栀椀猀嬀椀崀Ⰰ 愀爀最㄀⤀㬀ഀഀ
          } else {਍            昀漀爀 ⠀欀攀礀 椀渀 愀爀最㄀⤀ 笀ഀഀ
              fn(this[i], key, arg1[key]);਍            紀ഀഀ
          }਍        紀ഀഀ
        // return self for chaining਍        爀攀琀甀爀渀 琀栀椀猀㬀ഀഀ
      } else {਍        ⼀⼀ 眀攀 愀爀攀 愀 爀攀愀搀Ⰰ 猀漀 爀攀愀搀 琀栀攀 昀椀爀猀琀 挀栀椀氀搀⸀ഀഀ
        var value = fn.$dv;਍        ⼀⼀ 伀渀氀礀 椀昀 眀攀 栀愀瘀攀 ␀搀瘀 搀漀 眀攀 椀琀攀爀愀琀攀 漀瘀攀爀 愀氀氀Ⰰ 漀琀栀攀爀眀椀猀攀 椀琀 椀猀 樀甀猀琀 琀栀攀 昀椀爀猀琀 攀氀攀洀攀渀琀⸀ഀഀ
        var jj = (value === undefined) ? Math.min(this.length, 1) : this.length;਍        昀漀爀 ⠀瘀愀爀 樀 㴀 　㬀 樀 㰀 樀樀㬀 樀⬀⬀⤀ 笀ഀഀ
          var nodeValue = fn(this[j], arg1, arg2);਍          瘀愀氀甀攀 㴀 瘀愀氀甀攀 㼀 瘀愀氀甀攀 ⬀ 渀漀搀攀嘀愀氀甀攀 㨀 渀漀搀攀嘀愀氀甀攀㬀ഀഀ
        }਍        爀攀琀甀爀渀 瘀愀氀甀攀㬀ഀഀ
      }਍    紀 攀氀猀攀 笀ഀഀ
      // we are a write, so apply to all children਍      昀漀爀 ⠀椀 㴀 　㬀 椀 㰀 琀栀椀猀⸀氀攀渀最琀栀㬀 椀⬀⬀⤀ 笀ഀഀ
        fn(this[i], arg1, arg2);਍      紀ഀഀ
      // return self for chaining਍      爀攀琀甀爀渀 琀栀椀猀㬀ഀഀ
    }਍  紀㬀ഀഀ
});਍ഀഀ
function createEventHandler(element, events) {਍  瘀愀爀 攀瘀攀渀琀䠀愀渀搀氀攀爀 㴀 昀甀渀挀琀椀漀渀 ⠀攀瘀攀渀琀Ⰰ 琀礀瀀攀⤀ 笀ഀഀ
    if (!event.preventDefault) {਍      攀瘀攀渀琀⸀瀀爀攀瘀攀渀琀䐀攀昀愀甀氀琀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
        event.returnValue = false; //ie਍      紀㬀ഀഀ
    }਍ഀഀ
    if (!event.stopPropagation) {਍      攀瘀攀渀琀⸀猀琀漀瀀倀爀漀瀀愀最愀琀椀漀渀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
        event.cancelBubble = true; //ie਍      紀㬀ഀഀ
    }਍ഀഀ
    if (!event.target) {਍      攀瘀攀渀琀⸀琀愀爀最攀琀 㴀 攀瘀攀渀琀⸀猀爀挀䔀氀攀洀攀渀琀 簀簀 搀漀挀甀洀攀渀琀㬀ഀഀ
    }਍ഀഀ
    if (isUndefined(event.defaultPrevented)) {਍      瘀愀爀 瀀爀攀瘀攀渀琀 㴀 攀瘀攀渀琀⸀瀀爀攀瘀攀渀琀䐀攀昀愀甀氀琀㬀ഀഀ
      event.preventDefault = function() {਍        攀瘀攀渀琀⸀搀攀昀愀甀氀琀倀爀攀瘀攀渀琀攀搀 㴀 琀爀甀攀㬀ഀഀ
        prevent.call(event);਍      紀㬀ഀഀ
      event.defaultPrevented = false;਍    紀ഀഀ
਍    攀瘀攀渀琀⸀椀猀䐀攀昀愀甀氀琀倀爀攀瘀攀渀琀攀搀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
      return event.defaultPrevented || event.returnValue === false;਍    紀㬀ഀഀ
਍    ⼀⼀ 䌀漀瀀礀 攀瘀攀渀琀 栀愀渀搀氀攀爀猀 椀渀 挀愀猀攀 攀瘀攀渀琀 栀愀渀搀氀攀爀猀 愀爀爀愀礀 椀猀 洀漀搀椀昀椀攀搀 搀甀爀椀渀最 攀砀攀挀甀琀椀漀渀⸀ഀഀ
    var eventHandlersCopy = shallowCopy(events[type || event.type] || []);਍ഀഀ
    forEach(eventHandlersCopy, function(fn) {਍      昀渀⸀挀愀氀氀⠀攀氀攀洀攀渀琀Ⰰ 攀瘀攀渀琀⤀㬀ഀഀ
    });਍ഀഀ
    // Remove monkey-patched methods (IE),਍    ⼀⼀ 愀猀 琀栀攀礀 眀漀甀氀搀 挀愀甀猀攀 洀攀洀漀爀礀 氀攀愀欀猀 椀渀 䤀䔀㠀⸀ഀഀ
    if (msie <= 8) {਍      ⼀⼀ 䤀䔀㜀⼀㠀 搀漀攀猀 渀漀琀 愀氀氀漀眀 琀漀 搀攀氀攀琀攀 瀀爀漀瀀攀爀琀礀 漀渀 渀愀琀椀瘀攀 漀戀樀攀挀琀ഀഀ
      event.preventDefault = null;਍      攀瘀攀渀琀⸀猀琀漀瀀倀爀漀瀀愀最愀琀椀漀渀 㴀 渀甀氀氀㬀ഀഀ
      event.isDefaultPrevented = null;਍    紀 攀氀猀攀 笀ഀഀ
      // It shouldn't affect normal browsers (native methods are defined on prototype).਍      搀攀氀攀琀攀 攀瘀攀渀琀⸀瀀爀攀瘀攀渀琀䐀攀昀愀甀氀琀㬀ഀഀ
      delete event.stopPropagation;਍      搀攀氀攀琀攀 攀瘀攀渀琀⸀椀猀䐀攀昀愀甀氀琀倀爀攀瘀攀渀琀攀搀㬀ഀഀ
    }਍  紀㬀ഀഀ
  eventHandler.elem = element;਍  爀攀琀甀爀渀 攀瘀攀渀琀䠀愀渀搀氀攀爀㬀ഀഀ
}਍ഀഀ
//////////////////////////////////////////਍⼀⼀ 䘀甀渀挀琀椀漀渀猀 椀琀攀爀愀琀椀渀最 琀爀愀瘀攀爀猀愀氀⸀ഀഀ
// These functions chain results into a single਍⼀⼀ 猀攀氀攀挀琀漀爀⸀ഀഀ
//////////////////////////////////////////਍昀漀爀䔀愀挀栀⠀笀ഀഀ
  removeData: jqLiteRemoveData,਍ഀഀ
  dealoc: jqLiteDealoc,਍ഀഀ
  on: function onFn(element, type, fn, unsupported){਍    椀昀 ⠀椀猀䐀攀昀椀渀攀搀⠀甀渀猀甀瀀瀀漀爀琀攀搀⤀⤀ 琀栀爀漀眀 樀焀䰀椀琀攀䴀椀渀䔀爀爀⠀✀漀渀愀爀最猀✀Ⰰ ✀樀焀䰀椀琀攀⌀漀渀⠀⤀ 搀漀攀猀 渀漀琀 猀甀瀀瀀漀爀琀 琀栀攀 怀猀攀氀攀挀琀漀爀怀 漀爀 怀攀瘀攀渀琀䐀愀琀愀怀 瀀愀爀愀洀攀琀攀爀猀✀⤀㬀ഀഀ
਍    瘀愀爀 攀瘀攀渀琀猀 㴀 樀焀䰀椀琀攀䔀砀瀀愀渀搀漀匀琀漀爀攀⠀攀氀攀洀攀渀琀Ⰰ ✀攀瘀攀渀琀猀✀⤀Ⰰഀഀ
        handle = jqLiteExpandoStore(element, 'handle');਍ഀഀ
    if (!events) jqLiteExpandoStore(element, 'events', events = {});਍    椀昀 ⠀℀栀愀渀搀氀攀⤀ 樀焀䰀椀琀攀䔀砀瀀愀渀搀漀匀琀漀爀攀⠀攀氀攀洀攀渀琀Ⰰ ✀栀愀渀搀氀攀✀Ⰰ 栀愀渀搀氀攀 㴀 挀爀攀愀琀攀䔀瘀攀渀琀䠀愀渀搀氀攀爀⠀攀氀攀洀攀渀琀Ⰰ 攀瘀攀渀琀猀⤀⤀㬀ഀഀ
਍    昀漀爀䔀愀挀栀⠀琀礀瀀攀⸀猀瀀氀椀琀⠀✀ ✀⤀Ⰰ 昀甀渀挀琀椀漀渀⠀琀礀瀀攀⤀笀ഀഀ
      var eventFns = events[type];਍ഀഀ
      if (!eventFns) {਍        椀昀 ⠀琀礀瀀攀 㴀㴀 ✀洀漀甀猀攀攀渀琀攀爀✀ 簀簀 琀礀瀀攀 㴀㴀 ✀洀漀甀猀攀氀攀愀瘀攀✀⤀ 笀ഀഀ
          var contains = document.body.contains || document.body.compareDocumentPosition ?਍          昀甀渀挀琀椀漀渀⠀ 愀Ⰰ 戀 ⤀ 笀ഀഀ
            // jshint bitwise: false਍            瘀愀爀 愀搀漀眀渀 㴀 愀⸀渀漀搀攀吀礀瀀攀 㴀㴀㴀 㤀 㼀 愀⸀搀漀挀甀洀攀渀琀䔀氀攀洀攀渀琀 㨀 愀Ⰰഀഀ
            bup = b && b.parentNode;਍            爀攀琀甀爀渀 愀 㴀㴀㴀 戀甀瀀 簀簀 ℀℀⠀ 戀甀瀀 ☀☀ 戀甀瀀⸀渀漀搀攀吀礀瀀攀 㴀㴀㴀 ㄀ ☀☀ ⠀ഀഀ
              adown.contains ?਍              愀搀漀眀渀⸀挀漀渀琀愀椀渀猀⠀ 戀甀瀀 ⤀ 㨀ഀഀ
              a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16਍              ⤀⤀㬀ഀഀ
            } :਍            昀甀渀挀琀椀漀渀⠀ 愀Ⰰ 戀 ⤀ 笀ഀഀ
              if ( b ) {਍                眀栀椀氀攀 ⠀ ⠀戀 㴀 戀⸀瀀愀爀攀渀琀一漀搀攀⤀ ⤀ 笀ഀഀ
                  if ( b === a ) {਍                    爀攀琀甀爀渀 琀爀甀攀㬀ഀഀ
                  }਍                紀ഀഀ
              }਍              爀攀琀甀爀渀 昀愀氀猀攀㬀ഀഀ
            };਍ഀഀ
          events[type] = [];਍ഀഀ
          // Refer to jQuery's implementation of mouseenter & mouseleave਍          ⼀⼀ 刀攀愀搀 愀戀漀甀琀 洀漀甀猀攀攀渀琀攀爀 愀渀搀 洀漀甀猀攀氀攀愀瘀攀㨀ഀഀ
          // http://www.quirksmode.org/js/events_mouse.html#link8਍          瘀愀爀 攀瘀攀渀琀洀愀瀀 㴀 笀 洀漀甀猀攀氀攀愀瘀攀 㨀 ∀洀漀甀猀攀漀甀琀∀Ⰰ 洀漀甀猀攀攀渀琀攀爀 㨀 ∀洀漀甀猀攀漀瘀攀爀∀紀㬀ഀഀ
਍          漀渀䘀渀⠀攀氀攀洀攀渀琀Ⰰ 攀瘀攀渀琀洀愀瀀嬀琀礀瀀攀崀Ⰰ 昀甀渀挀琀椀漀渀⠀攀瘀攀渀琀⤀ 笀ഀഀ
            var target = this, related = event.relatedTarget;਍            ⼀⼀ 䘀漀爀 洀漀甀猀攀渀琀攀爀⼀氀攀愀瘀攀 挀愀氀氀 琀栀攀 栀愀渀搀氀攀爀 椀昀 爀攀氀愀琀攀搀 椀猀 漀甀琀猀椀搀攀 琀栀攀 琀愀爀最攀琀⸀ഀഀ
            // NB: No relatedTarget if the mouse left/entered the browser window਍            椀昀 ⠀ ℀爀攀氀愀琀攀搀 簀簀 ⠀爀攀氀愀琀攀搀 ℀㴀㴀 琀愀爀最攀琀 ☀☀ ℀挀漀渀琀愀椀渀猀⠀琀愀爀最攀琀Ⰰ 爀攀氀愀琀攀搀⤀⤀ ⤀笀ഀഀ
              handle(event, type);਍            紀ഀഀ
          });਍ഀഀ
        } else {਍          愀搀搀䔀瘀攀渀琀䰀椀猀琀攀渀攀爀䘀渀⠀攀氀攀洀攀渀琀Ⰰ 琀礀瀀攀Ⰰ 栀愀渀搀氀攀⤀㬀ഀഀ
          events[type] = [];਍        紀ഀഀ
        eventFns = events[type];਍      紀ഀഀ
      eventFns.push(fn);਍    紀⤀㬀ഀഀ
  },਍ഀഀ
  off: jqLiteOff,਍ഀഀ
  one: function(element, type, fn) {਍    攀氀攀洀攀渀琀 㴀 樀焀䰀椀琀攀⠀攀氀攀洀攀渀琀⤀㬀ഀഀ
਍    ⼀⼀愀搀搀 琀栀攀 氀椀猀琀攀渀攀爀 琀眀椀挀攀 猀漀 琀栀愀琀 眀栀攀渀 椀琀 椀猀 挀愀氀氀攀搀ഀഀ
    //you can remove the original function and still be਍    ⼀⼀愀戀氀攀 琀漀 挀愀氀氀 攀氀攀洀攀渀琀⸀漀昀昀⠀攀瘀Ⰰ 昀渀⤀ 渀漀爀洀愀氀氀礀ഀഀ
    element.on(type, function onFn() {਍      攀氀攀洀攀渀琀⸀漀昀昀⠀琀礀瀀攀Ⰰ 昀渀⤀㬀ഀഀ
      element.off(type, onFn);਍    紀⤀㬀ഀഀ
    element.on(type, fn);਍  紀Ⰰഀഀ
਍  爀攀瀀氀愀挀攀圀椀琀栀㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 爀攀瀀氀愀挀攀一漀搀攀⤀ 笀ഀഀ
    var index, parent = element.parentNode;਍    樀焀䰀椀琀攀䐀攀愀氀漀挀⠀攀氀攀洀攀渀琀⤀㬀ഀഀ
    forEach(new JQLite(replaceNode), function(node){਍      椀昀 ⠀椀渀搀攀砀⤀ 笀ഀഀ
        parent.insertBefore(node, index.nextSibling);਍      紀 攀氀猀攀 笀ഀഀ
        parent.replaceChild(node, element);਍      紀ഀഀ
      index = node;਍    紀⤀㬀ഀഀ
  },਍ഀഀ
  children: function(element) {਍    瘀愀爀 挀栀椀氀搀爀攀渀 㴀 嬀崀㬀ഀഀ
    forEach(element.childNodes, function(element){਍      椀昀 ⠀攀氀攀洀攀渀琀⸀渀漀搀攀吀礀瀀攀 㴀㴀㴀 ㄀⤀ഀഀ
        children.push(element);਍    紀⤀㬀ഀഀ
    return children;਍  紀Ⰰഀഀ
਍  挀漀渀琀攀渀琀猀㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀⤀ 笀ഀഀ
    return element.childNodes || [];਍  紀Ⰰഀഀ
਍  愀瀀瀀攀渀搀㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 渀漀搀攀⤀ 笀ഀഀ
    forEach(new JQLite(node), function(child){਍      椀昀 ⠀攀氀攀洀攀渀琀⸀渀漀搀攀吀礀瀀攀 㴀㴀㴀 ㄀ 簀簀 攀氀攀洀攀渀琀⸀渀漀搀攀吀礀瀀攀 㴀㴀㴀 ㄀㄀⤀ 笀ഀഀ
        element.appendChild(child);਍      紀ഀഀ
    });਍  紀Ⰰഀഀ
਍  瀀爀攀瀀攀渀搀㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 渀漀搀攀⤀ 笀ഀഀ
    if (element.nodeType === 1) {਍      瘀愀爀 椀渀搀攀砀 㴀 攀氀攀洀攀渀琀⸀昀椀爀猀琀䌀栀椀氀搀㬀ഀഀ
      forEach(new JQLite(node), function(child){਍        攀氀攀洀攀渀琀⸀椀渀猀攀爀琀䈀攀昀漀爀攀⠀挀栀椀氀搀Ⰰ 椀渀搀攀砀⤀㬀ഀഀ
      });਍    紀ഀഀ
  },਍ഀഀ
  wrap: function(element, wrapNode) {਍    眀爀愀瀀一漀搀攀 㴀 樀焀䰀椀琀攀⠀眀爀愀瀀一漀搀攀⤀嬀　崀㬀ഀഀ
    var parent = element.parentNode;਍    椀昀 ⠀瀀愀爀攀渀琀⤀ 笀ഀഀ
      parent.replaceChild(wrapNode, element);਍    紀ഀഀ
    wrapNode.appendChild(element);਍  紀Ⰰഀഀ
਍  爀攀洀漀瘀攀㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀⤀ 笀ഀഀ
    jqLiteDealoc(element);਍    瘀愀爀 瀀愀爀攀渀琀 㴀 攀氀攀洀攀渀琀⸀瀀愀爀攀渀琀一漀搀攀㬀ഀഀ
    if (parent) parent.removeChild(element);਍  紀Ⰰഀഀ
਍  愀昀琀攀爀㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 渀攀眀䔀氀攀洀攀渀琀⤀ 笀ഀഀ
    var index = element, parent = element.parentNode;਍    昀漀爀䔀愀挀栀⠀渀攀眀 䨀儀䰀椀琀攀⠀渀攀眀䔀氀攀洀攀渀琀⤀Ⰰ 昀甀渀挀琀椀漀渀⠀渀漀搀攀⤀笀ഀഀ
      parent.insertBefore(node, index.nextSibling);਍      椀渀搀攀砀 㴀 渀漀搀攀㬀ഀഀ
    });਍  紀Ⰰഀഀ
਍  愀搀搀䌀氀愀猀猀㨀 樀焀䰀椀琀攀䄀搀搀䌀氀愀猀猀Ⰰഀഀ
  removeClass: jqLiteRemoveClass,਍ഀഀ
  toggleClass: function(element, selector, condition) {਍    椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀挀漀渀搀椀琀椀漀渀⤀⤀ 笀ഀഀ
      condition = !jqLiteHasClass(element, selector);਍    紀ഀഀ
    (condition ? jqLiteAddClass : jqLiteRemoveClass)(element, selector);਍  紀Ⰰഀഀ
਍  瀀愀爀攀渀琀㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀⤀ 笀ഀഀ
    var parent = element.parentNode;਍    爀攀琀甀爀渀 瀀愀爀攀渀琀 ☀☀ 瀀愀爀攀渀琀⸀渀漀搀攀吀礀瀀攀 ℀㴀㴀 ㄀㄀ 㼀 瀀愀爀攀渀琀 㨀 渀甀氀氀㬀ഀഀ
  },਍ഀഀ
  next: function(element) {਍    椀昀 ⠀攀氀攀洀攀渀琀⸀渀攀砀琀䔀氀攀洀攀渀琀匀椀戀氀椀渀最⤀ 笀ഀഀ
      return element.nextElementSibling;਍    紀ഀഀ
਍    ⼀⼀ 䤀䔀㠀 搀漀攀猀渀✀琀 栀愀瘀攀 渀攀砀琀䔀氀攀洀攀渀琀匀椀戀氀椀渀最ഀഀ
    var elm = element.nextSibling;਍    眀栀椀氀攀 ⠀攀氀洀 ℀㴀 渀甀氀氀 ☀☀ 攀氀洀⸀渀漀搀攀吀礀瀀攀 ℀㴀㴀 ㄀⤀ 笀ഀഀ
      elm = elm.nextSibling;਍    紀ഀഀ
    return elm;਍  紀Ⰰഀഀ
਍  昀椀渀搀㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 猀攀氀攀挀琀漀爀⤀ 笀ഀഀ
    if (element.getElementsByTagName) {਍      爀攀琀甀爀渀 攀氀攀洀攀渀琀⸀最攀琀䔀氀攀洀攀渀琀猀䈀礀吀愀最一愀洀攀⠀猀攀氀攀挀琀漀爀⤀㬀ഀഀ
    } else {਍      爀攀琀甀爀渀 嬀崀㬀ഀഀ
    }਍  紀Ⰰഀഀ
਍  挀氀漀渀攀㨀 樀焀䰀椀琀攀䌀氀漀渀攀Ⰰഀഀ
਍  琀爀椀最最攀爀䠀愀渀搀氀攀爀㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 攀瘀攀渀琀一愀洀攀Ⰰ 攀瘀攀渀琀䐀愀琀愀⤀ 笀ഀഀ
    var eventFns = (jqLiteExpandoStore(element, 'events') || {})[eventName];਍ഀഀ
    eventData = eventData || [];਍ഀഀ
    var event = [{਍      瀀爀攀瘀攀渀琀䐀攀昀愀甀氀琀㨀 渀漀漀瀀Ⰰഀഀ
      stopPropagation: noop਍    紀崀㬀ഀഀ
਍    昀漀爀䔀愀挀栀⠀攀瘀攀渀琀䘀渀猀Ⰰ 昀甀渀挀琀椀漀渀⠀昀渀⤀ 笀ഀഀ
      fn.apply(element, event.concat(eventData));਍    紀⤀㬀ഀഀ
  }਍紀Ⰰ 昀甀渀挀琀椀漀渀⠀昀渀Ⰰ 渀愀洀攀⤀笀ഀഀ
  /**਍   ⨀ 挀栀愀椀渀椀渀最 昀甀渀挀琀椀漀渀猀ഀഀ
   */਍  䨀儀䰀椀琀攀⸀瀀爀漀琀漀琀礀瀀攀嬀渀愀洀攀崀 㴀 昀甀渀挀琀椀漀渀⠀愀爀最㄀Ⰰ 愀爀最㈀Ⰰ 愀爀最㌀⤀ 笀ഀഀ
    var value;਍    昀漀爀⠀瘀愀爀 椀㴀　㬀 椀 㰀 琀栀椀猀⸀氀攀渀最琀栀㬀 椀⬀⬀⤀ 笀ഀഀ
      if (isUndefined(value)) {਍        瘀愀氀甀攀 㴀 昀渀⠀琀栀椀猀嬀椀崀Ⰰ 愀爀最㄀Ⰰ 愀爀最㈀Ⰰ 愀爀最㌀⤀㬀ഀഀ
        if (isDefined(value)) {਍          ⼀⼀ 愀渀礀 昀甀渀挀琀椀漀渀 眀栀椀挀栀 爀攀琀甀爀渀猀 愀 瘀愀氀甀攀 渀攀攀搀猀 琀漀 戀攀 眀爀愀瀀瀀攀搀ഀഀ
          value = jqLite(value);਍        紀ഀഀ
      } else {਍        樀焀䰀椀琀攀䄀搀搀一漀搀攀猀⠀瘀愀氀甀攀Ⰰ 昀渀⠀琀栀椀猀嬀椀崀Ⰰ 愀爀最㄀Ⰰ 愀爀最㈀Ⰰ 愀爀最㌀⤀⤀㬀ഀഀ
      }਍    紀ഀഀ
    return isDefined(value) ? value : this;਍  紀㬀ഀഀ
਍  ⼀⼀ 戀椀渀搀 氀攀最愀挀礀 戀椀渀搀⼀甀渀戀椀渀搀 琀漀 漀渀⼀漀昀昀ഀഀ
  JQLite.prototype.bind = JQLite.prototype.on;਍  䨀儀䰀椀琀攀⸀瀀爀漀琀漀琀礀瀀攀⸀甀渀戀椀渀搀 㴀 䨀儀䰀椀琀攀⸀瀀爀漀琀漀琀礀瀀攀⸀漀昀昀㬀ഀഀ
});਍ഀഀ
/**਍ ⨀ 䌀漀洀瀀甀琀攀猀 愀 栀愀猀栀 漀昀 愀渀 ✀漀戀樀✀⸀ഀഀ
 * Hash of a:਍ ⨀  猀琀爀椀渀最 椀猀 猀琀爀椀渀最ഀഀ
 *  number is number as string਍ ⨀  漀戀樀攀挀琀 椀猀 攀椀琀栀攀爀 爀攀猀甀氀琀 漀昀 挀愀氀氀椀渀最 ␀␀栀愀猀栀䬀攀礀 昀甀渀挀琀椀漀渀 漀渀 琀栀攀 漀戀樀攀挀琀 漀爀 甀渀椀焀甀攀氀礀 最攀渀攀爀愀琀攀搀 椀搀Ⰰഀഀ
 *         that is also assigned to the $$hashKey property of the object.਍ ⨀ഀഀ
 * @param obj਍ ⨀ 䀀爀攀琀甀爀渀猀 笀猀琀爀椀渀最紀 栀愀猀栀 猀琀爀椀渀最 猀甀挀栀 琀栀愀琀 琀栀攀 猀愀洀攀 椀渀瀀甀琀 眀椀氀氀 栀愀瘀攀 琀栀攀 猀愀洀攀 栀愀猀栀 猀琀爀椀渀最⸀ഀഀ
 *         The resulting string key is in 'type:hashKey' format.਍ ⨀⼀ഀഀ
function hashKey(obj) {਍  瘀愀爀 漀戀樀吀礀瀀攀 㴀 琀礀瀀攀漀昀 漀戀樀Ⰰഀഀ
      key;਍ഀഀ
  if (objType == 'object' && obj !== null) {਍    椀昀 ⠀琀礀瀀攀漀昀 ⠀欀攀礀 㴀 漀戀樀⸀␀␀栀愀猀栀䬀攀礀⤀ 㴀㴀 ✀昀甀渀挀琀椀漀渀✀⤀ 笀ഀഀ
      // must invoke on object to keep the right this਍      欀攀礀 㴀 漀戀樀⸀␀␀栀愀猀栀䬀攀礀⠀⤀㬀ഀഀ
    } else if (key === undefined) {਍      欀攀礀 㴀 漀戀樀⸀␀␀栀愀猀栀䬀攀礀 㴀 渀攀砀琀唀椀搀⠀⤀㬀ഀഀ
    }਍  紀 攀氀猀攀 笀ഀഀ
    key = obj;਍  紀ഀഀ
਍  爀攀琀甀爀渀 漀戀樀吀礀瀀攀 ⬀ ✀㨀✀ ⬀ 欀攀礀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䠀愀猀栀䴀愀瀀 眀栀椀挀栀 挀愀渀 甀猀攀 漀戀樀攀挀琀猀 愀猀 欀攀礀猀ഀഀ
 */਍昀甀渀挀琀椀漀渀 䠀愀猀栀䴀愀瀀⠀愀爀爀愀礀⤀笀ഀഀ
  forEach(array, this.put, this);਍紀ഀഀ
HashMap.prototype = {਍  ⼀⨀⨀ഀഀ
   * Store key value pair਍   ⨀ 䀀瀀愀爀愀洀 欀攀礀 欀攀礀 琀漀 猀琀漀爀攀 挀愀渀 戀攀 愀渀礀 琀礀瀀攀ഀഀ
   * @param value value to store can be any type਍   ⨀⼀ഀഀ
  put: function(key, value) {਍    琀栀椀猀嬀栀愀猀栀䬀攀礀⠀欀攀礀⤀崀 㴀 瘀愀氀甀攀㬀ഀഀ
  },਍ഀഀ
  /**਍   ⨀ 䀀瀀愀爀愀洀 欀攀礀ഀഀ
   * @returns the value for the key਍   ⨀⼀ഀഀ
  get: function(key) {਍    爀攀琀甀爀渀 琀栀椀猀嬀栀愀猀栀䬀攀礀⠀欀攀礀⤀崀㬀ഀഀ
  },਍ഀഀ
  /**਍   ⨀ 刀攀洀漀瘀攀 琀栀攀 欀攀礀⼀瘀愀氀甀攀 瀀愀椀爀ഀഀ
   * @param key਍   ⨀⼀ഀഀ
  remove: function(key) {਍    瘀愀爀 瘀愀氀甀攀 㴀 琀栀椀猀嬀欀攀礀 㴀 栀愀猀栀䬀攀礀⠀欀攀礀⤀崀㬀ഀഀ
    delete this[key];਍    爀攀琀甀爀渀 瘀愀氀甀攀㬀ഀഀ
  }਍紀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 愀渀最甀氀愀爀⸀椀渀樀攀挀琀漀爀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ 䌀爀攀愀琀攀猀 愀渀 椀渀樀攀挀琀漀爀 昀甀渀挀琀椀漀渀 琀栀愀琀 挀愀渀 戀攀 甀猀攀搀 昀漀爀 爀攀琀爀椀攀瘀椀渀最 猀攀爀瘀椀挀攀猀 愀猀 眀攀氀氀 愀猀 昀漀爀ഀഀ
 * dependency injection (see {@link guide/di dependency injection}).਍ ⨀ഀഀ
਍ ⨀ 䀀瀀愀爀愀洀 笀䄀爀爀愀礀⸀㰀猀琀爀椀渀最簀䘀甀渀挀琀椀漀渀㸀紀 洀漀搀甀氀攀猀 䄀 氀椀猀琀 漀昀 洀漀搀甀氀攀 昀甀渀挀琀椀漀渀猀 漀爀 琀栀攀椀爀 愀氀椀愀猀攀猀⸀ 匀攀攀ഀഀ
 *        {@link angular.module}. The `ng` module must be explicitly added.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀昀甀渀挀琀椀漀渀⠀⤀紀 䤀渀樀攀挀琀漀爀 昀甀渀挀琀椀漀渀⸀ 匀攀攀 笀䀀氀椀渀欀 䄀唀吀伀⸀␀椀渀樀攀挀琀漀爀 ␀椀渀樀攀挀琀漀爀紀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
 * Typical usage਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   // create an injector਍ ⨀   瘀愀爀 ␀椀渀樀攀挀琀漀爀 㴀 愀渀最甀氀愀爀⸀椀渀樀攀挀琀漀爀⠀嬀✀渀最✀崀⤀㬀ഀഀ
 *਍ ⨀   ⼀⼀ 甀猀攀 琀栀攀 椀渀樀攀挀琀漀爀 琀漀 欀椀挀欀 漀昀昀 礀漀甀爀 愀瀀瀀氀椀挀愀琀椀漀渀ഀഀ
 *   // use the type inference to auto inject arguments, or use implicit injection਍ ⨀   ␀椀渀樀攀挀琀漀爀⸀椀渀瘀漀欀攀⠀昀甀渀挀琀椀漀渀⠀␀爀漀漀琀匀挀漀瀀攀Ⰰ ␀挀漀洀瀀椀氀攀Ⰰ ␀搀漀挀甀洀攀渀琀⤀笀ഀഀ
 *     $compile($document)($rootScope);਍ ⨀     ␀爀漀漀琀匀挀漀瀀攀⸀␀搀椀最攀猀琀⠀⤀㬀ഀഀ
 *   });਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 匀漀洀攀琀椀洀攀猀 礀漀甀 眀愀渀琀 琀漀 最攀琀 愀挀挀攀猀猀 琀漀 琀栀攀 椀渀樀攀挀琀漀爀 漀昀 愀 挀甀爀爀攀渀琀氀礀 爀甀渀渀椀渀最 䄀渀最甀氀愀爀 愀瀀瀀ഀഀ
 * from outside Angular. Perhaps, you want to inject and compile some markup after the਍ ⨀ 愀瀀瀀氀椀挀愀琀椀漀渀 栀愀猀 戀攀攀渀 戀漀漀琀猀琀爀愀瀀瀀攀搀⸀ 夀漀甀 挀愀渀 搀漀 琀栀椀猀 甀猀椀渀最 攀砀琀爀愀 怀椀渀樀攀挀琀漀爀⠀⤀怀 愀搀搀攀搀ഀഀ
 * to JQuery/jqLite elements. See {@link angular.element}.਍ ⨀ഀഀ
 * *This is fairly rare but could be the case if a third party library is injecting the਍ ⨀ 洀愀爀欀甀瀀⸀⨀ഀഀ
 *਍ ⨀ 䤀渀 琀栀攀 昀漀氀氀漀眀椀渀最 攀砀愀洀瀀氀攀 愀 渀攀眀 戀氀漀挀欀 漀昀 䠀吀䴀䰀 挀漀渀琀愀椀渀椀渀最 愀 怀渀最ⴀ挀漀渀琀爀漀氀氀攀爀怀ഀഀ
 * directive is added to the end of the document body by JQuery. We then compile and link਍ ⨀ 椀琀 椀渀琀漀 琀栀攀 挀甀爀爀攀渀琀 䄀渀最甀氀愀爀䨀匀 猀挀漀瀀攀⸀ഀഀ
 *਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 * var $div = $('<div ng-controller="MyCtrl">{{content.label}}</div>');਍ ⨀ ␀⠀搀漀挀甀洀攀渀琀⸀戀漀搀礀⤀⸀愀瀀瀀攀渀搀⠀␀搀椀瘀⤀㬀ഀഀ
 *਍ ⨀ 愀渀最甀氀愀爀⸀攀氀攀洀攀渀琀⠀搀漀挀甀洀攀渀琀⤀⸀椀渀樀攀挀琀漀爀⠀⤀⸀椀渀瘀漀欀攀⠀昀甀渀挀琀椀漀渀⠀␀挀漀洀瀀椀氀攀⤀ 笀ഀഀ
 *   var scope = angular.element($div).scope();਍ ⨀   ␀挀漀洀瀀椀氀攀⠀␀搀椀瘀⤀⠀猀挀漀瀀攀⤀㬀ഀഀ
 * });਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 */਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc overview਍ ⨀ 䀀渀愀洀攀 䄀唀吀伀ഀഀ
 * @description਍ ⨀ഀഀ
 * Implicit module which gets automatically added to each {@link AUTO.$injector $injector}.਍ ⨀⼀ഀഀ
਍瘀愀爀 䘀一开䄀刀䜀匀 㴀 ⼀帀昀甀渀挀琀椀漀渀尀猀⨀嬀帀尀⠀崀⨀尀⠀尀猀⨀⠀嬀帀尀⤀崀⨀⤀尀⤀⼀洀㬀ഀഀ
var FN_ARG_SPLIT = /,/;਍瘀愀爀 䘀一开䄀刀䜀 㴀 ⼀帀尀猀⨀⠀开㼀⤀⠀尀匀⬀㼀⤀尀㄀尀猀⨀␀⼀㬀ഀഀ
var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;਍瘀愀爀 ␀椀渀樀攀挀琀漀爀䴀椀渀䔀爀爀 㴀 洀椀渀䔀爀爀⠀✀␀椀渀樀攀挀琀漀爀✀⤀㬀ഀഀ
function annotate(fn) {਍  瘀愀爀 ␀椀渀樀攀挀琀Ⰰഀഀ
      fnText,਍      愀爀最䐀攀挀氀Ⰰഀഀ
      last;਍ഀഀ
  if (typeof fn == 'function') {਍    椀昀 ⠀℀⠀␀椀渀樀攀挀琀 㴀 昀渀⸀␀椀渀樀攀挀琀⤀⤀ 笀ഀഀ
      $inject = [];਍      椀昀 ⠀昀渀⸀氀攀渀最琀栀⤀ 笀ഀഀ
        fnText = fn.toString().replace(STRIP_COMMENTS, '');਍        愀爀最䐀攀挀氀 㴀 昀渀吀攀砀琀⸀洀愀琀挀栀⠀䘀一开䄀刀䜀匀⤀㬀ഀഀ
        forEach(argDecl[1].split(FN_ARG_SPLIT), function(arg){਍          愀爀最⸀爀攀瀀氀愀挀攀⠀䘀一开䄀刀䜀Ⰰ 昀甀渀挀琀椀漀渀⠀愀氀氀Ⰰ 甀渀搀攀爀猀挀漀爀攀Ⰰ 渀愀洀攀⤀笀ഀഀ
            $inject.push(name);਍          紀⤀㬀ഀഀ
        });਍      紀ഀഀ
      fn.$inject = $inject;਍    紀ഀഀ
  } else if (isArray(fn)) {਍    氀愀猀琀 㴀 昀渀⸀氀攀渀最琀栀 ⴀ ㄀㬀ഀഀ
    assertArgFn(fn[last], 'fn');਍    ␀椀渀樀攀挀琀 㴀 昀渀⸀猀氀椀挀攀⠀　Ⰰ 氀愀猀琀⤀㬀ഀഀ
  } else {਍    愀猀猀攀爀琀䄀爀最䘀渀⠀昀渀Ⰰ ✀昀渀✀Ⰰ 琀爀甀攀⤀㬀ഀഀ
  }਍  爀攀琀甀爀渀 ␀椀渀樀攀挀琀㬀ഀഀ
}਍ഀഀ
///////////////////////////////////////਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name AUTO.$injector਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 *਍ ⨀ 怀␀椀渀樀攀挀琀漀爀怀 椀猀 甀猀攀搀 琀漀 爀攀琀爀椀攀瘀攀 漀戀樀攀挀琀 椀渀猀琀愀渀挀攀猀 愀猀 搀攀昀椀渀攀搀 戀礀ഀഀ
 * {@link AUTO.$provide provider}, instantiate types, invoke methods,਍ ⨀ 愀渀搀 氀漀愀搀 洀漀搀甀氀攀猀⸀ഀഀ
 *਍ ⨀ 吀栀攀 昀漀氀氀漀眀椀渀最 愀氀眀愀礀猀 栀漀氀搀猀 琀爀甀攀㨀ഀഀ
 *਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   var $injector = angular.injector();਍ ⨀   攀砀瀀攀挀琀⠀␀椀渀樀攀挀琀漀爀⸀最攀琀⠀✀␀椀渀樀攀挀琀漀爀✀⤀⤀⸀琀漀䈀攀⠀␀椀渀樀攀挀琀漀爀⤀㬀ഀഀ
 *   expect($injector.invoke(function($injector){਍ ⨀     爀攀琀甀爀渀 ␀椀渀樀攀挀琀漀爀㬀ഀഀ
 *   }).toBe($injector);਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ ⌀ 䤀渀樀攀挀琀椀漀渀 䘀甀渀挀琀椀漀渀 䄀渀渀漀琀愀琀椀漀渀ഀഀ
 *਍ ⨀ 䨀愀瘀愀匀挀爀椀瀀琀 搀漀攀猀 渀漀琀 栀愀瘀攀 愀渀渀漀琀愀琀椀漀渀猀Ⰰ 愀渀搀 愀渀渀漀琀愀琀椀漀渀猀 愀爀攀 渀攀攀搀攀搀 昀漀爀 搀攀瀀攀渀搀攀渀挀礀 椀渀樀攀挀琀椀漀渀⸀ 吀栀攀ഀഀ
 * following are all valid ways of annotating function with injection arguments and are equivalent.਍ ⨀ഀഀ
 * <pre>਍ ⨀   ⼀⼀ 椀渀昀攀爀爀攀搀 ⠀漀渀氀礀 眀漀爀欀猀 椀昀 挀漀搀攀 渀漀琀 洀椀渀椀昀椀攀搀⼀漀戀昀甀猀挀愀琀攀搀⤀ഀഀ
 *   $injector.invoke(function(serviceA){});਍ ⨀ഀഀ
 *   // annotated਍ ⨀   昀甀渀挀琀椀漀渀 攀砀瀀氀椀挀椀琀⠀猀攀爀瘀椀挀攀䄀⤀ 笀紀㬀ഀഀ
 *   explicit.$inject = ['serviceA'];਍ ⨀   ␀椀渀樀攀挀琀漀爀⸀椀渀瘀漀欀攀⠀攀砀瀀氀椀挀椀琀⤀㬀ഀഀ
 *਍ ⨀   ⼀⼀ 椀渀氀椀渀攀ഀഀ
 *   $injector.invoke(['serviceA', function(serviceA){}]);਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ ⌀⌀ 䤀渀昀攀爀攀渀挀攀ഀഀ
 *਍ ⨀ 䤀渀 䨀愀瘀愀匀挀爀椀瀀琀 挀愀氀氀椀渀最 怀琀漀匀琀爀椀渀最⠀⤀怀 漀渀 愀 昀甀渀挀琀椀漀渀 爀攀琀甀爀渀猀 琀栀攀 昀甀渀挀琀椀漀渀 搀攀昀椀渀椀琀椀漀渀⸀ 吀栀攀 搀攀昀椀渀椀琀椀漀渀ഀഀ
 * can then be parsed and the function arguments can be extracted. *NOTE:* This does not work with਍ ⨀ 洀椀渀椀昀椀挀愀琀椀漀渀Ⰰ 愀渀搀 漀戀昀甀猀挀愀琀椀漀渀 琀漀漀氀猀 猀椀渀挀攀 琀栀攀猀攀 琀漀漀氀猀 挀栀愀渀最攀 琀栀攀 愀爀最甀洀攀渀琀 渀愀洀攀猀⸀ഀഀ
 *਍ ⨀ ⌀⌀ 怀␀椀渀樀攀挀琀怀 䄀渀渀漀琀愀琀椀漀渀ഀഀ
 * By adding a `$inject` property onto a function the injection parameters can be specified.਍ ⨀ഀഀ
 * ## Inline਍ ⨀ 䄀猀 愀渀 愀爀爀愀礀 漀昀 椀渀樀攀挀琀椀漀渀 渀愀洀攀猀Ⰰ 眀栀攀爀攀 琀栀攀 氀愀猀琀 椀琀攀洀 椀渀 琀栀攀 愀爀爀愀礀 椀猀 琀栀攀 昀甀渀挀琀椀漀渀 琀漀 挀愀氀氀⸀ഀഀ
 */਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
 * @name AUTO.$injector#get਍ ⨀ 䀀洀攀琀栀漀搀伀昀 䄀唀吀伀⸀␀椀渀樀攀挀琀漀爀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Return an instance of the service.਍ ⨀ഀഀ
 * @param {string} name The name of the instance to retrieve.਍ ⨀ 䀀爀攀琀甀爀渀 笀⨀紀 吀栀攀 椀渀猀琀愀渀挀攀⸀ഀഀ
 */਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
 * @name AUTO.$injector#invoke਍ ⨀ 䀀洀攀琀栀漀搀伀昀 䄀唀吀伀⸀␀椀渀樀攀挀琀漀爀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Invoke the method and supply the method arguments from the `$injector`.਍ ⨀ഀഀ
 * @param {!function} fn The function to invoke. Function parameters are injected according to the਍ ⨀   笀䀀氀椀渀欀 最甀椀搀攀⼀搀椀 ␀椀渀樀攀挀琀 䄀渀渀漀琀愀琀椀漀渀紀 爀甀氀攀猀⸀ഀഀ
 * @param {Object=} self The `this` for the invoked method.਍ ⨀ 䀀瀀愀爀愀洀 笀伀戀樀攀挀琀㴀紀 氀漀挀愀氀猀 伀瀀琀椀漀渀愀氀 漀戀樀攀挀琀⸀ 䤀昀 瀀爀攀猀攀琀 琀栀攀渀 愀渀礀 愀爀最甀洀攀渀琀 渀愀洀攀猀 愀爀攀 爀攀愀搀 昀爀漀洀 琀栀椀猀ഀഀ
 *                         object first, before the `$injector` is consulted.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀⨀紀 琀栀攀 瘀愀氀甀攀 爀攀琀甀爀渀攀搀 戀礀 琀栀攀 椀渀瘀漀欀攀搀 怀昀渀怀 昀甀渀挀琀椀漀渀⸀ഀഀ
 */਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
 * @name AUTO.$injector#has਍ ⨀ 䀀洀攀琀栀漀搀伀昀 䄀唀吀伀⸀␀椀渀樀攀挀琀漀爀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Allows the user to query if the particular service exist.਍ ⨀ഀഀ
 * @param {string} Name of the service to query.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀戀漀漀氀攀愀渀紀 爀攀琀甀爀渀猀 琀爀甀攀 椀昀 椀渀樀攀挀琀漀爀 栀愀猀 最椀瘀攀渀 猀攀爀瘀椀挀攀⸀ഀഀ
 */਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
 * @name AUTO.$injector#instantiate਍ ⨀ 䀀洀攀琀栀漀搀伀昀 䄀唀吀伀⸀␀椀渀樀攀挀琀漀爀ഀഀ
 * @description਍ ⨀ 䌀爀攀愀琀攀 愀 渀攀眀 椀渀猀琀愀渀挀攀 漀昀 䨀匀 琀礀瀀攀⸀ 吀栀攀 洀攀琀栀漀搀 琀愀欀攀猀 愀 挀漀渀猀琀爀甀挀琀漀爀 昀甀渀挀琀椀漀渀 椀渀瘀漀欀攀猀 琀栀攀 渀攀眀ഀഀ
 * operator and supplies all of the arguments to the constructor function as specified by the਍ ⨀ 挀漀渀猀琀爀甀挀琀漀爀 愀渀渀漀琀愀琀椀漀渀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀昀甀渀挀琀椀漀渀紀 吀礀瀀攀 䄀渀渀漀琀愀琀攀搀 挀漀渀猀琀爀甀挀琀漀爀 昀甀渀挀琀椀漀渀⸀ഀഀ
 * @param {Object=} locals Optional object. If preset then any argument names are read from this਍ ⨀ 漀戀樀攀挀琀 昀椀爀猀琀Ⰰ 戀攀昀漀爀攀 琀栀攀 怀␀椀渀樀攀挀琀漀爀怀 椀猀 挀漀渀猀甀氀琀攀搀⸀ഀഀ
 * @returns {Object} new instance of `Type`.਍ ⨀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc method਍ ⨀ 䀀渀愀洀攀 䄀唀吀伀⸀␀椀渀樀攀挀琀漀爀⌀愀渀渀漀琀愀琀攀ഀഀ
 * @methodOf AUTO.$injector਍ ⨀ഀഀ
 * @description਍ ⨀ 刀攀琀甀爀渀猀 愀渀 愀爀爀愀礀 漀昀 猀攀爀瘀椀挀攀 渀愀洀攀猀 眀栀椀挀栀 琀栀攀 昀甀渀挀琀椀漀渀 椀猀 爀攀焀甀攀猀琀椀渀最 昀漀爀 椀渀樀攀挀琀椀漀渀⸀ 吀栀椀猀 䄀倀䤀 椀猀ഀഀ
 * used by the injector to determine which services need to be injected into the function when the਍ ⨀ 昀甀渀挀琀椀漀渀 椀猀 椀渀瘀漀欀攀搀⸀ 吀栀攀爀攀 愀爀攀 琀栀爀攀攀 眀愀礀猀 椀渀 眀栀椀挀栀 琀栀攀 昀甀渀挀琀椀漀渀 挀愀渀 戀攀 愀渀渀漀琀愀琀攀搀 眀椀琀栀 琀栀攀 渀攀攀搀攀搀ഀഀ
 * dependencies.਍ ⨀ഀഀ
 * # Argument names਍ ⨀ഀഀ
 * The simplest form is to extract the dependencies from the arguments of the function. This is done਍ ⨀ 戀礀 挀漀渀瘀攀爀琀椀渀最 琀栀攀 昀甀渀挀琀椀漀渀 椀渀琀漀 愀 猀琀爀椀渀最 甀猀椀渀最 怀琀漀匀琀爀椀渀最⠀⤀怀 洀攀琀栀漀搀 愀渀搀 攀砀琀爀愀挀琀椀渀最 琀栀攀 愀爀最甀洀攀渀琀ഀഀ
 * names.਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   // Given਍ ⨀   昀甀渀挀琀椀漀渀 䴀礀䌀漀渀琀爀漀氀氀攀爀⠀␀猀挀漀瀀攀Ⰰ ␀爀漀甀琀攀⤀ 笀ഀഀ
 *     // ...਍ ⨀   紀ഀഀ
 *਍ ⨀   ⼀⼀ 吀栀攀渀ഀഀ
 *   expect(injector.annotate(MyController)).toEqual(['$scope', '$route']);਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 吀栀椀猀 洀攀琀栀漀搀 搀漀攀猀 渀漀琀 眀漀爀欀 眀椀琀栀 挀漀搀攀 洀椀渀椀昀椀挀愀琀椀漀渀 ⼀ 漀戀昀甀猀挀愀琀椀漀渀⸀ 䘀漀爀 琀栀椀猀 爀攀愀猀漀渀 琀栀攀 昀漀氀氀漀眀椀渀最ഀഀ
 * annotation strategies are supported.਍ ⨀ഀഀ
 * # The `$inject` property਍ ⨀ഀഀ
 * If a function has an `$inject` property and its value is an array of strings, then the strings਍ ⨀ 爀攀瀀爀攀猀攀渀琀 渀愀洀攀猀 漀昀 猀攀爀瘀椀挀攀猀 琀漀 戀攀 椀渀樀攀挀琀攀搀 椀渀琀漀 琀栀攀 昀甀渀挀琀椀漀渀⸀ഀഀ
 * <pre>਍ ⨀   ⼀⼀ 䜀椀瘀攀渀ഀഀ
 *   var MyController = function(obfuscatedScope, obfuscatedRoute) {਍ ⨀     ⼀⼀ ⸀⸀⸀ഀഀ
 *   }਍ ⨀   ⼀⼀ 䐀攀昀椀渀攀 昀甀渀挀琀椀漀渀 搀攀瀀攀渀搀攀渀挀椀攀猀ഀഀ
 *   MyController['$inject'] = ['$scope', '$route'];਍ ⨀ഀഀ
 *   // Then਍ ⨀   攀砀瀀攀挀琀⠀椀渀樀攀挀琀漀爀⸀愀渀渀漀琀愀琀攀⠀䴀礀䌀漀渀琀爀漀氀氀攀爀⤀⤀⸀琀漀䔀焀甀愀氀⠀嬀✀␀猀挀漀瀀攀✀Ⰰ ✀␀爀漀甀琀攀✀崀⤀㬀ഀഀ
 * </pre>਍ ⨀ഀഀ
 * # The array notation਍ ⨀ഀഀ
 * It is often desirable to inline Injected functions and that's when setting the `$inject` property਍ ⨀ 椀猀 瘀攀爀礀 椀渀挀漀渀瘀攀渀椀攀渀琀⸀ 䤀渀 琀栀攀猀攀 猀椀琀甀愀琀椀漀渀猀 甀猀椀渀最 琀栀攀 愀爀爀愀礀 渀漀琀愀琀椀漀渀 琀漀 猀瀀攀挀椀昀礀 琀栀攀 搀攀瀀攀渀搀攀渀挀椀攀猀 椀渀ഀഀ
 * a way that survives minification is a better choice:਍ ⨀ഀഀ
 * <pre>਍ ⨀   ⼀⼀ 圀攀 眀椀猀栀 琀漀 眀爀椀琀攀 琀栀椀猀 ⠀渀漀琀 洀椀渀椀昀椀挀愀琀椀漀渀 ⼀ 漀戀昀甀猀挀愀琀椀漀渀 猀愀昀攀⤀ഀഀ
 *   injector.invoke(function($compile, $rootScope) {਍ ⨀     ⼀⼀ ⸀⸀⸀ഀഀ
 *   });਍ ⨀ഀഀ
 *   // We are forced to write break inlining਍ ⨀   瘀愀爀 琀洀瀀䘀渀 㴀 昀甀渀挀琀椀漀渀⠀漀戀昀甀猀挀愀琀攀搀䌀漀洀瀀椀氀攀Ⰰ 漀戀昀甀猀挀愀琀攀搀刀漀漀琀匀挀漀瀀攀⤀ 笀ഀഀ
 *     // ...਍ ⨀   紀㬀ഀഀ
 *   tmpFn.$inject = ['$compile', '$rootScope'];਍ ⨀   椀渀樀攀挀琀漀爀⸀椀渀瘀漀欀攀⠀琀洀瀀䘀渀⤀㬀ഀഀ
 *਍ ⨀   ⼀⼀ 吀漀 戀攀琀琀攀爀 猀甀瀀瀀漀爀琀 椀渀氀椀渀攀 昀甀渀挀琀椀漀渀 琀栀攀 椀渀氀椀渀攀 愀渀渀漀琀愀琀椀漀渀 椀猀 猀甀瀀瀀漀爀琀攀搀ഀഀ
 *   injector.invoke(['$compile', '$rootScope', function(obfCompile, obfRootScope) {਍ ⨀     ⼀⼀ ⸀⸀⸀ഀഀ
 *   }]);਍ ⨀ഀഀ
 *   // Therefore਍ ⨀   攀砀瀀攀挀琀⠀椀渀樀攀挀琀漀爀⸀愀渀渀漀琀愀琀攀⠀ഀഀ
 *      ['$compile', '$rootScope', function(obfus_$compile, obfus_$rootScope) {}])਍ ⨀    ⤀⸀琀漀䔀焀甀愀氀⠀嬀✀␀挀漀洀瀀椀氀攀✀Ⰰ ✀␀爀漀漀琀匀挀漀瀀攀✀崀⤀㬀ഀഀ
 * </pre>਍ ⨀ഀഀ
 * @param {function|Array.<string|Function>} fn Function for which dependent service names need to਍ ⨀ 戀攀 爀攀琀爀椀攀瘀攀搀 愀猀 搀攀猀挀爀椀戀攀搀 愀戀漀瘀攀⸀ഀഀ
 *਍ ⨀ 䀀爀攀琀甀爀渀猀 笀䄀爀爀愀礀⸀㰀猀琀爀椀渀最㸀紀 吀栀攀 渀愀洀攀猀 漀昀 琀栀攀 猀攀爀瘀椀挀攀猀 眀栀椀挀栀 琀栀攀 昀甀渀挀琀椀漀渀 爀攀焀甀椀爀攀猀⸀ഀഀ
 */਍ഀഀ
਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc object਍ ⨀ 䀀渀愀洀攀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 *਍ ⨀ 吀栀攀 笀䀀氀椀渀欀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀 ␀瀀爀漀瘀椀搀攀紀 猀攀爀瘀椀挀攀 栀愀猀 愀 渀甀洀戀攀爀 漀昀 洀攀琀栀漀搀猀 昀漀爀 爀攀最椀猀琀攀爀椀渀最 挀漀洀瀀漀渀攀渀琀猀ഀഀ
 * with the {@link AUTO.$injector $injector}. Many of these functions are also exposed on਍ ⨀ 笀䀀氀椀渀欀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀紀⸀ഀഀ
 *਍ ⨀ 䄀渀 䄀渀最甀氀愀爀 ⨀⨀猀攀爀瘀椀挀攀⨀⨀ 椀猀 愀 猀椀渀最氀攀琀漀渀 漀戀樀攀挀琀 挀爀攀愀琀攀搀 戀礀 愀 ⨀⨀猀攀爀瘀椀挀攀 昀愀挀琀漀爀礀⨀⨀⸀  吀栀攀猀攀 ⨀⨀猀攀爀瘀椀挀攀ഀഀ
 * factories** are functions which, in turn, are created by a **service provider**.਍ ⨀ 吀栀攀 ⨀⨀猀攀爀瘀椀挀攀 瀀爀漀瘀椀搀攀爀猀⨀⨀ 愀爀攀 挀漀渀猀琀爀甀挀琀漀爀 昀甀渀挀琀椀漀渀猀⸀ 圀栀攀渀 椀渀猀琀愀渀琀椀愀琀攀搀 琀栀攀礀 洀甀猀琀 挀漀渀琀愀椀渀 愀ഀഀ
 * property called `$get`, which holds the **service factory** function.਍ ⨀ഀഀ
 * When you request a service, the {@link AUTO.$injector $injector} is responsible for finding the਍ ⨀ 挀漀爀爀攀挀琀 ⨀⨀猀攀爀瘀椀挀攀 瀀爀漀瘀椀搀攀爀⨀⨀Ⰰ 椀渀猀琀愀渀琀椀愀琀椀渀最 椀琀 愀渀搀 琀栀攀渀 挀愀氀氀椀渀最 椀琀猀 怀␀最攀琀怀 ⨀⨀猀攀爀瘀椀挀攀 昀愀挀琀漀爀礀⨀⨀ഀഀ
 * function to get the instance of the **service**.਍ ⨀ഀഀ
 * Often services have no configuration options and there is no need to add methods to the service਍ ⨀ 瀀爀漀瘀椀搀攀爀⸀  吀栀攀 瀀爀漀瘀椀搀攀爀 眀椀氀氀 戀攀 渀漀 洀漀爀攀 琀栀愀渀 愀 挀漀渀猀琀爀甀挀琀漀爀 昀甀渀挀琀椀漀渀 眀椀琀栀 愀 怀␀最攀琀怀 瀀爀漀瀀攀爀琀礀⸀ 䘀漀爀ഀഀ
 * these cases the {@link AUTO.$provide $provide} service has additional helper methods to register਍ ⨀ 猀攀爀瘀椀挀攀猀 眀椀琀栀漀甀琀 猀瀀攀挀椀昀礀椀渀最 愀 瀀爀漀瘀椀搀攀爀⸀ഀഀ
 *਍ ⨀ ⨀ 笀䀀氀椀渀欀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀⌀洀攀琀栀漀搀猀开瀀爀漀瘀椀搀攀爀 瀀爀漀瘀椀搀攀爀⠀瀀爀漀瘀椀搀攀爀⤀紀 ⴀ 爀攀最椀猀琀攀爀猀 愀 ⨀⨀猀攀爀瘀椀挀攀 瀀爀漀瘀椀搀攀爀⨀⨀ 眀椀琀栀 琀栀攀ഀഀ
 *     {@link AUTO.$injector $injector}਍ ⨀ ⨀ 笀䀀氀椀渀欀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀⌀洀攀琀栀漀搀猀开挀漀渀猀琀愀渀琀 挀漀渀猀琀愀渀琀⠀漀戀樀⤀紀 ⴀ 爀攀最椀猀琀攀爀猀 愀 瘀愀氀甀攀⼀漀戀樀攀挀琀 琀栀愀琀 挀愀渀 戀攀 愀挀挀攀猀猀攀搀 戀礀ഀഀ
 *     providers and services.਍ ⨀ ⨀ 笀䀀氀椀渀欀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀⌀洀攀琀栀漀搀猀开瘀愀氀甀攀 瘀愀氀甀攀⠀漀戀樀⤀紀 ⴀ 爀攀最椀猀琀攀爀猀 愀 瘀愀氀甀攀⼀漀戀樀攀挀琀 琀栀愀琀 挀愀渀 漀渀氀礀 戀攀 愀挀挀攀猀猀攀搀 戀礀ഀഀ
 *     services, not providers.਍ ⨀ ⨀ 笀䀀氀椀渀欀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀⌀洀攀琀栀漀搀猀开昀愀挀琀漀爀礀 昀愀挀琀漀爀礀⠀昀渀⤀紀 ⴀ 爀攀最椀猀琀攀爀猀 愀 猀攀爀瘀椀挀攀 ⨀⨀昀愀挀琀漀爀礀 昀甀渀挀琀椀漀渀⨀⨀Ⰰ 怀昀渀怀Ⰰഀഀ
 *     that will be wrapped in a **service provider** object, whose `$get` property will contain the਍ ⨀     最椀瘀攀渀 昀愀挀琀漀爀礀 昀甀渀挀琀椀漀渀⸀ഀഀ
 * * {@link AUTO.$provide#methods_service service(class)} - registers a **constructor function**, `class` that਍ ⨀     琀栀愀琀 眀椀氀氀 戀攀 眀爀愀瀀瀀攀搀 椀渀 愀 ⨀⨀猀攀爀瘀椀挀攀 瀀爀漀瘀椀搀攀爀⨀⨀ 漀戀樀攀挀琀Ⰰ 眀栀漀猀攀 怀␀最攀琀怀 瀀爀漀瀀攀爀琀礀 眀椀氀氀 椀渀猀琀愀渀琀椀愀琀攀ഀഀ
 *      a new object using the given constructor function.਍ ⨀ഀഀ
 * See the individual methods for more information and examples.਍ ⨀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc method਍ ⨀ 䀀渀愀洀攀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀⌀瀀爀漀瘀椀搀攀爀ഀഀ
 * @methodOf AUTO.$provide਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 *਍ ⨀ 刀攀最椀猀琀攀爀 愀 ⨀⨀瀀爀漀瘀椀搀攀爀 昀甀渀挀琀椀漀渀⨀⨀ 眀椀琀栀 琀栀攀 笀䀀氀椀渀欀 䄀唀吀伀⸀␀椀渀樀攀挀琀漀爀 ␀椀渀樀攀挀琀漀爀紀⸀ 倀爀漀瘀椀搀攀爀 昀甀渀挀琀椀漀渀猀ഀഀ
 * are constructor functions, whose instances are responsible for "providing" a factory for a਍ ⨀ 猀攀爀瘀椀挀攀⸀ഀഀ
 *਍ ⨀ 匀攀爀瘀椀挀攀 瀀爀漀瘀椀搀攀爀 渀愀洀攀猀 猀琀愀爀琀 眀椀琀栀 琀栀攀 渀愀洀攀 漀昀 琀栀攀 猀攀爀瘀椀挀攀 琀栀攀礀 瀀爀漀瘀椀搀攀 昀漀氀氀漀眀攀搀 戀礀 怀倀爀漀瘀椀搀攀爀怀⸀ഀഀ
 * For example, the {@link ng.$log $log} service has a provider called਍ ⨀ 笀䀀氀椀渀欀 渀最⸀␀氀漀最倀爀漀瘀椀搀攀爀 ␀氀漀最倀爀漀瘀椀搀攀爀紀⸀ഀഀ
 *਍ ⨀ 匀攀爀瘀椀挀攀 瀀爀漀瘀椀搀攀爀 漀戀樀攀挀琀猀 挀愀渀 栀愀瘀攀 愀搀搀椀琀椀漀渀愀氀 洀攀琀栀漀搀猀 眀栀椀挀栀 愀氀氀漀眀 挀漀渀昀椀最甀爀愀琀椀漀渀 漀昀 琀栀攀 瀀爀漀瘀椀搀攀爀ഀഀ
 * and its service. Importantly, you can configure what kind of service is created by the `$get`਍ ⨀ 洀攀琀栀漀搀Ⰰ 漀爀 栀漀眀 琀栀愀琀 猀攀爀瘀椀挀攀 眀椀氀氀 愀挀琀⸀ 䘀漀爀 攀砀愀洀瀀氀攀Ⰰ 琀栀攀 笀䀀氀椀渀欀 渀最⸀␀氀漀最倀爀漀瘀椀搀攀爀 ␀氀漀最倀爀漀瘀椀搀攀爀紀 栀愀猀 愀ഀഀ
 * method {@link ng.$logProvider#debugEnabled debugEnabled}਍ ⨀ 眀栀椀挀栀 氀攀琀猀 礀漀甀 猀瀀攀挀椀昀礀 眀栀攀琀栀攀爀 琀栀攀 笀䀀氀椀渀欀 渀最⸀␀氀漀最 ␀氀漀最紀 猀攀爀瘀椀挀攀 眀椀氀氀 氀漀最 搀攀戀甀最 洀攀猀猀愀最攀猀 琀漀 琀栀攀ഀഀ
 * console or not.਍ ⨀ഀഀ
 * @param {string} name The name of the instance. NOTE: the provider will be available under `name +਍                        ✀倀爀漀瘀椀搀攀爀✀怀 欀攀礀⸀ഀഀ
 * @param {(Object|function())} provider If the provider is:਍ ⨀ഀഀ
 *   - `Object`: then it should have a `$get` method. The `$get` method will be invoked using਍ ⨀               笀䀀氀椀渀欀 䄀唀吀伀⸀␀椀渀樀攀挀琀漀爀⌀椀渀瘀漀欀攀 ␀椀渀樀攀挀琀漀爀⸀椀渀瘀漀欀攀⠀⤀紀 眀栀攀渀 愀渀 椀渀猀琀愀渀挀攀 渀攀攀搀猀 琀漀 戀攀ഀഀ
 *               created.਍ ⨀   ⴀ 怀䌀漀渀猀琀爀甀挀琀漀爀怀㨀 愀 渀攀眀 椀渀猀琀愀渀挀攀 漀昀 琀栀攀 瀀爀漀瘀椀搀攀爀 眀椀氀氀 戀攀 挀爀攀愀琀攀搀 甀猀椀渀最ഀഀ
 *               {@link AUTO.$injector#instantiate $injector.instantiate()}, then treated as਍ ⨀               怀漀戀樀攀挀琀怀⸀ഀഀ
 *਍ ⨀ 䀀爀攀琀甀爀渀猀 笀伀戀樀攀挀琀紀 爀攀最椀猀琀攀爀攀搀 瀀爀漀瘀椀搀攀爀 椀渀猀琀愀渀挀攀ഀഀ
਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
 *਍ ⨀ 吀栀攀 昀漀氀氀漀眀椀渀最 攀砀愀洀瀀氀攀 猀栀漀眀猀 栀漀眀 琀漀 挀爀攀愀琀攀 愀 猀椀洀瀀氀攀 攀瘀攀渀琀 琀爀愀挀欀椀渀最 猀攀爀瘀椀挀攀 愀渀搀 爀攀最椀猀琀攀爀 椀琀 甀猀椀渀最ഀഀ
 * {@link AUTO.$provide#methods_provider $provide.provider()}.਍ ⨀ഀഀ
 * <pre>਍ ⨀  ⼀⼀ 䐀攀昀椀渀攀 琀栀攀 攀瘀攀渀琀吀爀愀挀欀攀爀 瀀爀漀瘀椀搀攀爀ഀഀ
 *  function EventTrackerProvider() {਍ ⨀    瘀愀爀 琀爀愀挀欀椀渀最唀爀氀 㴀 ✀⼀琀爀愀挀欀✀㬀ഀഀ
 *਍ ⨀    ⼀⼀ 䄀 瀀爀漀瘀椀搀攀爀 洀攀琀栀漀搀 昀漀爀 挀漀渀昀椀最甀爀椀渀最 眀栀攀爀攀 琀栀攀 琀爀愀挀欀攀搀 攀瘀攀渀琀猀 猀栀漀甀氀搀 戀攀攀渀 猀愀瘀攀搀ഀഀ
 *    this.setTrackingUrl = function(url) {਍ ⨀      琀爀愀挀欀椀渀最唀爀氀 㴀 甀爀氀㬀ഀഀ
 *    };਍ ⨀ഀഀ
 *    // The service factory function਍ ⨀    琀栀椀猀⸀␀最攀琀 㴀 嬀✀␀栀琀琀瀀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀栀琀琀瀀⤀ 笀ഀഀ
 *      var trackedEvents = {};਍ ⨀      爀攀琀甀爀渀 笀ഀഀ
 *        // Call this to track an event਍ ⨀        攀瘀攀渀琀㨀 昀甀渀挀琀椀漀渀⠀攀瘀攀渀琀⤀ 笀ഀഀ
 *          var count = trackedEvents[event] || 0;਍ ⨀          挀漀甀渀琀 ⬀㴀 ㄀㬀ഀഀ
 *          trackedEvents[event] = count;਍ ⨀          爀攀琀甀爀渀 挀漀甀渀琀㬀ഀഀ
 *        },਍ ⨀        ⼀⼀ 䌀愀氀氀 琀栀椀猀 琀漀 猀愀瘀攀 琀栀攀 琀爀愀挀欀攀搀 攀瘀攀渀琀猀 琀漀 琀栀攀 琀爀愀挀欀椀渀最唀爀氀ഀഀ
 *        save: function() {਍ ⨀          ␀栀琀琀瀀⸀瀀漀猀琀⠀琀爀愀挀欀椀渀最唀爀氀Ⰰ 琀爀愀挀欀攀搀䔀瘀攀渀琀猀⤀㬀ഀഀ
 *        }਍ ⨀      紀㬀ഀഀ
 *    }];਍ ⨀  紀ഀഀ
 *਍ ⨀  搀攀猀挀爀椀戀攀⠀✀攀瘀攀渀琀吀爀愀挀欀攀爀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
 *    var postSpy;਍ ⨀ഀഀ
 *    beforeEach(module(function($provide) {਍ ⨀      ⼀⼀ 刀攀最椀猀琀攀爀 琀栀攀 攀瘀攀渀琀吀爀愀挀欀攀爀 瀀爀漀瘀椀搀攀爀ഀഀ
 *      $provide.provider('eventTracker', EventTrackerProvider);਍ ⨀    紀⤀⤀㬀ഀഀ
 *਍ ⨀    戀攀昀漀爀攀䔀愀挀栀⠀洀漀搀甀氀攀⠀昀甀渀挀琀椀漀渀⠀攀瘀攀渀琀吀爀愀挀欀攀爀倀爀漀瘀椀搀攀爀⤀ 笀ഀഀ
 *      // Configure eventTracker provider਍ ⨀      攀瘀攀渀琀吀爀愀挀欀攀爀倀爀漀瘀椀搀攀爀⸀猀攀琀吀爀愀挀欀椀渀最唀爀氀⠀✀⼀挀甀猀琀漀洀ⴀ琀爀愀挀欀✀⤀㬀ഀഀ
 *    }));਍ ⨀ഀഀ
 *    it('tracks events', inject(function(eventTracker) {਍ ⨀      攀砀瀀攀挀琀⠀攀瘀攀渀琀吀爀愀挀欀攀爀⸀攀瘀攀渀琀⠀✀氀漀最椀渀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀㄀⤀㬀ഀഀ
 *      expect(eventTracker.event('login')).toEqual(2);਍ ⨀    紀⤀⤀㬀ഀഀ
 *਍ ⨀    椀琀⠀✀猀愀瘀攀猀 琀漀 琀栀攀 琀爀愀挀欀椀渀最 甀爀氀✀Ⰰ 椀渀樀攀挀琀⠀昀甀渀挀琀椀漀渀⠀攀瘀攀渀琀吀爀愀挀欀攀爀Ⰰ ␀栀琀琀瀀⤀ 笀ഀഀ
 *      postSpy = spyOn($http, 'post');਍ ⨀      攀瘀攀渀琀吀爀愀挀欀攀爀⸀攀瘀攀渀琀⠀✀氀漀最椀渀✀⤀㬀ഀഀ
 *      eventTracker.save();਍ ⨀      攀砀瀀攀挀琀⠀瀀漀猀琀匀瀀礀⤀⸀琀漀䠀愀瘀攀䈀攀攀渀䌀愀氀氀攀搀⠀⤀㬀ഀഀ
 *      expect(postSpy.mostRecentCall.args[0]).not.toEqual('/track');਍ ⨀      攀砀瀀攀挀琀⠀瀀漀猀琀匀瀀礀⸀洀漀猀琀刀攀挀攀渀琀䌀愀氀氀⸀愀爀最猀嬀　崀⤀⸀琀漀䔀焀甀愀氀⠀✀⼀挀甀猀琀漀洀ⴀ琀爀愀挀欀✀⤀㬀ഀഀ
 *      expect(postSpy.mostRecentCall.args[1]).toEqual({ 'login': 1 });਍ ⨀    紀⤀⤀㬀ഀഀ
 *  });਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 */਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
 * @name AUTO.$provide#factory਍ ⨀ 䀀洀攀琀栀漀搀伀昀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀ഀഀ
 * @description਍ ⨀ഀഀ
 * Register a **service factory**, which will be called to return the service instance.਍ ⨀ 吀栀椀猀 椀猀 猀栀漀爀琀 昀漀爀 爀攀最椀猀琀攀爀椀渀最 愀 猀攀爀瘀椀挀攀 眀栀攀爀攀 椀琀猀 瀀爀漀瘀椀搀攀爀 挀漀渀猀椀猀琀猀 漀昀 漀渀氀礀 愀 怀␀最攀琀怀 瀀爀漀瀀攀爀琀礀Ⰰഀഀ
 * which is the given service factory function.਍ ⨀ 夀漀甀 猀栀漀甀氀搀 甀猀攀 笀䀀氀椀渀欀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀⌀昀愀挀琀漀爀礀 ␀瀀爀漀瘀椀搀攀⸀昀愀挀琀漀爀礀⠀最攀琀䘀渀⤀紀 椀昀 礀漀甀 搀漀 渀漀琀 渀攀攀搀 琀漀ഀഀ
 * configure your service in a provider.਍ ⨀ഀഀ
 * @param {string} name The name of the instance.਍ ⨀ 䀀瀀愀爀愀洀 笀昀甀渀挀琀椀漀渀⠀⤀紀 ␀最攀琀䘀渀 吀栀攀 ␀最攀琀䘀渀 昀漀爀 琀栀攀 椀渀猀琀愀渀挀攀 挀爀攀愀琀椀漀渀⸀ 䤀渀琀攀爀渀愀氀氀礀 琀栀椀猀 椀猀 愀 猀栀漀爀琀 栀愀渀搀ഀഀ
 *                            for `$provide.provider(name, {$get: $getFn})`.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀伀戀樀攀挀琀紀 爀攀最椀猀琀攀爀攀搀 瀀爀漀瘀椀搀攀爀 椀渀猀琀愀渀挀攀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
 * Here is an example of registering a service਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   $provide.factory('ping', ['$http', function($http) {਍ ⨀     爀攀琀甀爀渀 昀甀渀挀琀椀漀渀 瀀椀渀最⠀⤀ 笀ഀഀ
 *       return $http.send('/ping');਍ ⨀     紀㬀ഀഀ
 *   }]);਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 * You would then inject and use this service like this:਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   someModule.controller('Ctrl', ['ping', function(ping) {਍ ⨀     瀀椀渀最⠀⤀㬀ഀഀ
 *   }]);਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 */਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc method਍ ⨀ 䀀渀愀洀攀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀⌀猀攀爀瘀椀挀攀ഀഀ
 * @methodOf AUTO.$provide਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 *਍ ⨀ 刀攀最椀猀琀攀爀 愀 ⨀⨀猀攀爀瘀椀挀攀 挀漀渀猀琀爀甀挀琀漀爀⨀⨀Ⰰ 眀栀椀挀栀 眀椀氀氀 戀攀 椀渀瘀漀欀攀搀 眀椀琀栀 怀渀攀眀怀 琀漀 挀爀攀愀琀攀 琀栀攀 猀攀爀瘀椀挀攀ഀഀ
 * instance.਍ ⨀ 吀栀椀猀 椀猀 猀栀漀爀琀 昀漀爀 爀攀最椀猀琀攀爀椀渀最 愀 猀攀爀瘀椀挀攀 眀栀攀爀攀 椀琀猀 瀀爀漀瘀椀搀攀爀✀猀 怀␀最攀琀怀 瀀爀漀瀀攀爀琀礀 椀猀 琀栀攀 猀攀爀瘀椀挀攀ഀഀ
 * constructor function that will be used to instantiate the service instance.਍ ⨀ഀഀ
 * You should use {@link AUTO.$provide#methods_service $provide.service(class)} if you define your service਍ ⨀ 愀猀 愀 琀礀瀀攀⼀挀氀愀猀猀⸀ 吀栀椀猀 椀猀 挀漀洀洀漀渀 眀栀攀渀 甀猀椀渀最 笀䀀氀椀渀欀 栀琀琀瀀㨀⼀⼀挀漀昀昀攀攀猀挀爀椀瀀琀⸀漀爀最 䌀漀昀昀攀攀匀挀爀椀瀀琀紀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀愀洀攀 吀栀攀 渀愀洀攀 漀昀 琀栀攀 椀渀猀琀愀渀挀攀⸀ഀഀ
 * @param {Function} constructor A class (constructor function) that will be instantiated.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀伀戀樀攀挀琀紀 爀攀最椀猀琀攀爀攀搀 瀀爀漀瘀椀搀攀爀 椀渀猀琀愀渀挀攀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
 * Here is an example of registering a service using਍ ⨀ 笀䀀氀椀渀欀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀⌀洀攀琀栀漀搀猀开猀攀爀瘀椀挀攀 ␀瀀爀漀瘀椀搀攀⸀猀攀爀瘀椀挀攀⠀挀氀愀猀猀⤀紀 琀栀愀琀 椀猀 搀攀昀椀渀攀搀 愀猀 愀 䌀漀昀昀攀攀匀挀爀椀瀀琀 挀氀愀猀猀⸀ഀഀ
 * <pre>਍ ⨀   挀氀愀猀猀 倀椀渀最ഀഀ
 *     constructor: (@$http) ->਍ ⨀     猀攀渀搀㨀 ⠀⤀ 㴀㸀ഀഀ
 *       @$http.get('/ping')਍ ⨀ഀഀ
 *   $provide.service('ping', ['$http', Ping])਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 * You would then inject and use this service like this:਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   someModule.controller 'Ctrl', ['ping', (ping) ->਍ ⨀     瀀椀渀最⸀猀攀渀搀⠀⤀ഀഀ
 *   ]਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 */਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc method਍ ⨀ 䀀渀愀洀攀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀⌀瘀愀氀甀攀ഀഀ
 * @methodOf AUTO.$provide਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 *਍ ⨀ 刀攀最椀猀琀攀爀 愀 ⨀⨀瘀愀氀甀攀 猀攀爀瘀椀挀攀⨀⨀ 眀椀琀栀 琀栀攀 笀䀀氀椀渀欀 䄀唀吀伀⸀␀椀渀樀攀挀琀漀爀 ␀椀渀樀攀挀琀漀爀紀Ⰰ 猀甀挀栀 愀猀 愀 猀琀爀椀渀最Ⰰ 愀ഀഀ
 * number, an array, an object or a function.  This is short for registering a service where its਍ ⨀ 瀀爀漀瘀椀搀攀爀✀猀 怀␀最攀琀怀 瀀爀漀瀀攀爀琀礀 椀猀 愀 昀愀挀琀漀爀礀 昀甀渀挀琀椀漀渀 琀栀愀琀 琀愀欀攀猀 渀漀 愀爀最甀洀攀渀琀猀 愀渀搀 爀攀琀甀爀渀猀 琀栀攀 ⨀⨀瘀愀氀甀攀ഀഀ
 * service**.਍ ⨀ഀഀ
 * Value services are similar to constant services, except that they cannot be injected into a਍ ⨀ 洀漀搀甀氀攀 挀漀渀昀椀最甀爀愀琀椀漀渀 昀甀渀挀琀椀漀渀 ⠀猀攀攀 笀䀀氀椀渀欀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀⌀挀漀渀昀椀最紀⤀ 戀甀琀 琀栀攀礀 挀愀渀 戀攀 漀瘀攀爀爀椀搀搀攀渀 戀礀ഀഀ
 * an Angular਍ ⨀ 笀䀀氀椀渀欀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀⌀搀攀挀漀爀愀琀漀爀 搀攀挀漀爀愀琀漀爀紀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀愀洀攀 吀栀攀 渀愀洀攀 漀昀 琀栀攀 椀渀猀琀愀渀挀攀⸀ഀഀ
 * @param {*} value The value.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀伀戀樀攀挀琀紀 爀攀最椀猀琀攀爀攀搀 瀀爀漀瘀椀搀攀爀 椀渀猀琀愀渀挀攀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
 * Here are some examples of creating value services.਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   $provide.value('ADMIN_USER', 'admin');਍ ⨀ഀഀ
 *   $provide.value('RoleLookup', { admin: 0, writer: 1, reader: 2 });਍ ⨀ഀഀ
 *   $provide.value('halfOf', function(value) {਍ ⨀     爀攀琀甀爀渀 瘀愀氀甀攀 ⼀ ㈀㬀ഀഀ
 *   });਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 */਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc method਍ ⨀ 䀀渀愀洀攀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀⌀挀漀渀猀琀愀渀琀ഀഀ
 * @methodOf AUTO.$provide਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 *਍ ⨀ 刀攀最椀猀琀攀爀 愀 ⨀⨀挀漀渀猀琀愀渀琀 猀攀爀瘀椀挀攀⨀⨀Ⰰ 猀甀挀栀 愀猀 愀 猀琀爀椀渀最Ⰰ 愀 渀甀洀戀攀爀Ⰰ 愀渀 愀爀爀愀礀Ⰰ 愀渀 漀戀樀攀挀琀 漀爀 愀 昀甀渀挀琀椀漀渀Ⰰഀഀ
 * with the {@link AUTO.$injector $injector}. Unlike {@link AUTO.$provide#value value} it can be਍ ⨀ 椀渀樀攀挀琀攀搀 椀渀琀漀 愀 洀漀搀甀氀攀 挀漀渀昀椀最甀爀愀琀椀漀渀 昀甀渀挀琀椀漀渀 ⠀猀攀攀 笀䀀氀椀渀欀 愀渀最甀氀愀爀⸀䴀漀搀甀氀攀⌀挀漀渀昀椀最紀⤀ 愀渀搀 椀琀 挀愀渀渀漀琀ഀഀ
 * be overridden by an Angular {@link AUTO.$provide#decorator decorator}.਍ ⨀ഀഀ
 * @param {string} name The name of the constant.਍ ⨀ 䀀瀀愀爀愀洀 笀⨀紀 瘀愀氀甀攀 吀栀攀 挀漀渀猀琀愀渀琀 瘀愀氀甀攀⸀ഀഀ
 * @returns {Object} registered instance਍ ⨀ഀഀ
 * @example਍ ⨀ 䠀攀爀攀 愀 猀漀洀攀 攀砀愀洀瀀氀攀猀 漀昀 挀爀攀愀琀椀渀最 挀漀渀猀琀愀渀琀猀㨀ഀഀ
 * <pre>਍ ⨀   ␀瀀爀漀瘀椀搀攀⸀挀漀渀猀琀愀渀琀⠀✀匀䠀䄀刀䐀开䠀䔀䤀䜀䠀吀✀Ⰰ ㌀　㘀⤀㬀ഀഀ
 *਍ ⨀   ␀瀀爀漀瘀椀搀攀⸀挀漀渀猀琀愀渀琀⠀✀䴀夀开䌀伀䰀伀唀刀匀✀Ⰰ 嬀✀爀攀搀✀Ⰰ ✀戀氀甀攀✀Ⰰ ✀最爀攀礀✀崀⤀㬀ഀഀ
 *਍ ⨀   ␀瀀爀漀瘀椀搀攀⸀挀漀渀猀琀愀渀琀⠀✀搀漀甀戀氀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
 *     return value * 2;਍ ⨀   紀⤀㬀ഀഀ
 * </pre>਍ ⨀⼀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
 * @name AUTO.$provide#decorator਍ ⨀ 䀀洀攀琀栀漀搀伀昀 䄀唀吀伀⸀␀瀀爀漀瘀椀搀攀ഀഀ
 * @description਍ ⨀ഀഀ
 * Register a **service decorator** with the {@link AUTO.$injector $injector}. A service decorator਍ ⨀ 椀渀琀攀爀挀攀瀀琀猀 琀栀攀 挀爀攀愀琀椀漀渀 漀昀 愀 猀攀爀瘀椀挀攀Ⰰ 愀氀氀漀眀椀渀最 椀琀 琀漀 漀瘀攀爀爀椀搀攀 漀爀 洀漀搀椀昀礀 琀栀攀 戀攀栀愀瘀椀漀甀爀 漀昀 琀栀攀ഀഀ
 * service. The object returned by the decorator may be the original service, or a new service਍ ⨀ 漀戀樀攀挀琀 眀栀椀挀栀 爀攀瀀氀愀挀攀猀 漀爀 眀爀愀瀀猀 愀渀搀 搀攀氀攀最愀琀攀猀 琀漀 琀栀攀 漀爀椀最椀渀愀氀 猀攀爀瘀椀挀攀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀愀洀攀 吀栀攀 渀愀洀攀 漀昀 琀栀攀 猀攀爀瘀椀挀攀 琀漀 搀攀挀漀爀愀琀攀⸀ഀഀ
 * @param {function()} decorator This function will be invoked when the service needs to be਍ ⨀    椀渀猀琀愀渀琀椀愀琀攀搀 愀渀搀 猀栀漀甀氀搀 爀攀琀甀爀渀 琀栀攀 搀攀挀漀爀愀琀攀搀 猀攀爀瘀椀挀攀 椀渀猀琀愀渀挀攀⸀ 吀栀攀 昀甀渀挀琀椀漀渀 椀猀 挀愀氀氀攀搀 甀猀椀渀最ഀഀ
 *    the {@link AUTO.$injector#invoke injector.invoke} method and is therefore fully injectable.਍ ⨀    䰀漀挀愀氀 椀渀樀攀挀琀椀漀渀 愀爀最甀洀攀渀琀猀㨀ഀഀ
 *਍ ⨀    ⨀ 怀␀搀攀氀攀最愀琀攀怀 ⴀ 吀栀攀 漀爀椀最椀渀愀氀 猀攀爀瘀椀挀攀 椀渀猀琀愀渀挀攀Ⰰ 眀栀椀挀栀 挀愀渀 戀攀 洀漀渀欀攀礀 瀀愀琀挀栀攀搀Ⰰ 挀漀渀昀椀最甀爀攀搀Ⰰഀഀ
 *      decorated or delegated to.਍ ⨀ഀഀ
 * @example਍ ⨀ 䠀攀爀攀 眀攀 搀攀挀漀爀愀琀攀 琀栀攀 笀䀀氀椀渀欀 渀最⸀␀氀漀最 ␀氀漀最紀 猀攀爀瘀椀挀攀 琀漀 挀漀渀瘀攀爀琀 眀愀爀渀椀渀最猀 琀漀 攀爀爀漀爀猀 戀礀 椀渀琀攀爀挀攀瀀琀椀渀最ഀഀ
 * calls to {@link ng.$log#error $log.warn()}.਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   $provider.decorator('$log', ['$delegate', function($delegate) {਍ ⨀     ␀搀攀氀攀最愀琀攀⸀眀愀爀渀 㴀 ␀搀攀氀攀最愀琀攀⸀攀爀爀漀爀㬀ഀഀ
 *     return $delegate;਍ ⨀   紀崀⤀㬀ഀഀ
 * </pre>਍ ⨀⼀ഀഀ
਍ഀഀ
function createInjector(modulesToLoad) {਍  瘀愀爀 䤀一匀吀䄀一吀䤀䄀吀䤀一䜀 㴀 笀紀Ⰰഀഀ
      providerSuffix = 'Provider',਍      瀀愀琀栀 㴀 嬀崀Ⰰഀഀ
      loadedModules = new HashMap(),਍      瀀爀漀瘀椀搀攀爀䌀愀挀栀攀 㴀 笀ഀഀ
        $provide: {਍            瀀爀漀瘀椀搀攀爀㨀 猀甀瀀瀀漀爀琀伀戀樀攀挀琀⠀瀀爀漀瘀椀搀攀爀⤀Ⰰഀഀ
            factory: supportObject(factory),਍            猀攀爀瘀椀挀攀㨀 猀甀瀀瀀漀爀琀伀戀樀攀挀琀⠀猀攀爀瘀椀挀攀⤀Ⰰഀഀ
            value: supportObject(value),਍            挀漀渀猀琀愀渀琀㨀 猀甀瀀瀀漀爀琀伀戀樀攀挀琀⠀挀漀渀猀琀愀渀琀⤀Ⰰഀഀ
            decorator: decorator਍          紀ഀഀ
      },਍      瀀爀漀瘀椀搀攀爀䤀渀樀攀挀琀漀爀 㴀 ⠀瀀爀漀瘀椀搀攀爀䌀愀挀栀攀⸀␀椀渀樀攀挀琀漀爀 㴀ഀഀ
          createInternalInjector(providerCache, function() {਍            琀栀爀漀眀 ␀椀渀樀攀挀琀漀爀䴀椀渀䔀爀爀⠀✀甀渀瀀爀✀Ⰰ ∀唀渀欀渀漀眀渀 瀀爀漀瘀椀搀攀爀㨀 笀　紀∀Ⰰ 瀀愀琀栀⸀樀漀椀渀⠀✀ 㰀ⴀ ✀⤀⤀㬀ഀഀ
          })),਍      椀渀猀琀愀渀挀攀䌀愀挀栀攀 㴀 笀紀Ⰰഀഀ
      instanceInjector = (instanceCache.$injector =਍          挀爀攀愀琀攀䤀渀琀攀爀渀愀氀䤀渀樀攀挀琀漀爀⠀椀渀猀琀愀渀挀攀䌀愀挀栀攀Ⰰ 昀甀渀挀琀椀漀渀⠀猀攀爀瘀椀挀攀渀愀洀攀⤀ 笀ഀഀ
            var provider = providerInjector.get(servicename + providerSuffix);਍            爀攀琀甀爀渀 椀渀猀琀愀渀挀攀䤀渀樀攀挀琀漀爀⸀椀渀瘀漀欀攀⠀瀀爀漀瘀椀搀攀爀⸀␀最攀琀Ⰰ 瀀爀漀瘀椀搀攀爀⤀㬀ഀഀ
          }));਍ഀഀ
਍  昀漀爀䔀愀挀栀⠀氀漀愀搀䴀漀搀甀氀攀猀⠀洀漀搀甀氀攀猀吀漀䰀漀愀搀⤀Ⰰ 昀甀渀挀琀椀漀渀⠀昀渀⤀ 笀 椀渀猀琀愀渀挀攀䤀渀樀攀挀琀漀爀⸀椀渀瘀漀欀攀⠀昀渀 簀簀 渀漀漀瀀⤀㬀 紀⤀㬀ഀഀ
਍  爀攀琀甀爀渀 椀渀猀琀愀渀挀攀䤀渀樀攀挀琀漀爀㬀ഀഀ
਍  ⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
  // $provider਍  ⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
਍  昀甀渀挀琀椀漀渀 猀甀瀀瀀漀爀琀伀戀樀攀挀琀⠀搀攀氀攀最愀琀攀⤀ 笀ഀഀ
    return function(key, value) {਍      椀昀 ⠀椀猀伀戀樀攀挀琀⠀欀攀礀⤀⤀ 笀ഀഀ
        forEach(key, reverseParams(delegate));਍      紀 攀氀猀攀 笀ഀഀ
        return delegate(key, value);਍      紀ഀഀ
    };਍  紀ഀഀ
਍  昀甀渀挀琀椀漀渀 瀀爀漀瘀椀搀攀爀⠀渀愀洀攀Ⰰ 瀀爀漀瘀椀搀攀爀开⤀ 笀ഀഀ
    assertNotHasOwnProperty(name, 'service');਍    椀昀 ⠀椀猀䘀甀渀挀琀椀漀渀⠀瀀爀漀瘀椀搀攀爀开⤀ 簀簀 椀猀䄀爀爀愀礀⠀瀀爀漀瘀椀搀攀爀开⤀⤀ 笀ഀഀ
      provider_ = providerInjector.instantiate(provider_);਍    紀ഀഀ
    if (!provider_.$get) {਍      琀栀爀漀眀 ␀椀渀樀攀挀琀漀爀䴀椀渀䔀爀爀⠀✀瀀最攀琀✀Ⰰ ∀倀爀漀瘀椀搀攀爀 ✀笀　紀✀ 洀甀猀琀 搀攀昀椀渀攀 ␀最攀琀 昀愀挀琀漀爀礀 洀攀琀栀漀搀⸀∀Ⰰ 渀愀洀攀⤀㬀ഀഀ
    }਍    爀攀琀甀爀渀 瀀爀漀瘀椀搀攀爀䌀愀挀栀攀嬀渀愀洀攀 ⬀ 瀀爀漀瘀椀搀攀爀匀甀昀昀椀砀崀 㴀 瀀爀漀瘀椀搀攀爀开㬀ഀഀ
  }਍ഀഀ
  function factory(name, factoryFn) { return provider(name, { $get: factoryFn }); }਍ഀഀ
  function service(name, constructor) {਍    爀攀琀甀爀渀 昀愀挀琀漀爀礀⠀渀愀洀攀Ⰰ 嬀✀␀椀渀樀攀挀琀漀爀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀椀渀樀攀挀琀漀爀⤀ 笀ഀഀ
      return $injector.instantiate(constructor);਍    紀崀⤀㬀ഀഀ
  }਍ഀഀ
  function value(name, val) { return factory(name, valueFn(val)); }਍ഀഀ
  function constant(name, value) {਍    愀猀猀攀爀琀一漀琀䠀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀渀愀洀攀Ⰰ ✀挀漀渀猀琀愀渀琀✀⤀㬀ഀഀ
    providerCache[name] = value;਍    椀渀猀琀愀渀挀攀䌀愀挀栀攀嬀渀愀洀攀崀 㴀 瘀愀氀甀攀㬀ഀഀ
  }਍ഀഀ
  function decorator(serviceName, decorFn) {਍    瘀愀爀 漀爀椀最倀爀漀瘀椀搀攀爀 㴀 瀀爀漀瘀椀搀攀爀䤀渀樀攀挀琀漀爀⸀最攀琀⠀猀攀爀瘀椀挀攀一愀洀攀 ⬀ 瀀爀漀瘀椀搀攀爀匀甀昀昀椀砀⤀Ⰰഀഀ
        orig$get = origProvider.$get;਍ഀഀ
    origProvider.$get = function() {਍      瘀愀爀 漀爀椀最䤀渀猀琀愀渀挀攀 㴀 椀渀猀琀愀渀挀攀䤀渀樀攀挀琀漀爀⸀椀渀瘀漀欀攀⠀漀爀椀最␀最攀琀Ⰰ 漀爀椀最倀爀漀瘀椀搀攀爀⤀㬀ഀഀ
      return instanceInjector.invoke(decorFn, null, {$delegate: origInstance});਍    紀㬀ഀഀ
  }਍ഀഀ
  ////////////////////////////////////਍  ⼀⼀ 䴀漀搀甀氀攀 䰀漀愀搀椀渀最ഀഀ
  ////////////////////////////////////਍  昀甀渀挀琀椀漀渀 氀漀愀搀䴀漀搀甀氀攀猀⠀洀漀搀甀氀攀猀吀漀䰀漀愀搀⤀笀ഀഀ
    var runBlocks = [], moduleFn, invokeQueue, i, ii;਍    昀漀爀䔀愀挀栀⠀洀漀搀甀氀攀猀吀漀䰀漀愀搀Ⰰ 昀甀渀挀琀椀漀渀⠀洀漀搀甀氀攀⤀ 笀ഀഀ
      if (loadedModules.get(module)) return;਍      氀漀愀搀攀搀䴀漀搀甀氀攀猀⸀瀀甀琀⠀洀漀搀甀氀攀Ⰰ 琀爀甀攀⤀㬀ഀഀ
਍      琀爀礀 笀ഀഀ
        if (isString(module)) {਍          洀漀搀甀氀攀䘀渀 㴀 愀渀最甀氀愀爀䴀漀搀甀氀攀⠀洀漀搀甀氀攀⤀㬀ഀഀ
          runBlocks = runBlocks.concat(loadModules(moduleFn.requires)).concat(moduleFn._runBlocks);਍ഀഀ
          for(invokeQueue = moduleFn._invokeQueue, i = 0, ii = invokeQueue.length; i < ii; i++) {਍            瘀愀爀 椀渀瘀漀欀攀䄀爀最猀 㴀 椀渀瘀漀欀攀儀甀攀甀攀嬀椀崀Ⰰഀഀ
                provider = providerInjector.get(invokeArgs[0]);਍ഀഀ
            provider[invokeArgs[1]].apply(provider, invokeArgs[2]);਍          紀ഀഀ
        } else if (isFunction(module)) {਍            爀甀渀䈀氀漀挀欀猀⸀瀀甀猀栀⠀瀀爀漀瘀椀搀攀爀䤀渀樀攀挀琀漀爀⸀椀渀瘀漀欀攀⠀洀漀搀甀氀攀⤀⤀㬀ഀഀ
        } else if (isArray(module)) {਍            爀甀渀䈀氀漀挀欀猀⸀瀀甀猀栀⠀瀀爀漀瘀椀搀攀爀䤀渀樀攀挀琀漀爀⸀椀渀瘀漀欀攀⠀洀漀搀甀氀攀⤀⤀㬀ഀഀ
        } else {਍          愀猀猀攀爀琀䄀爀最䘀渀⠀洀漀搀甀氀攀Ⰰ ✀洀漀搀甀氀攀✀⤀㬀ഀഀ
        }਍      紀 挀愀琀挀栀 ⠀攀⤀ 笀ഀഀ
        if (isArray(module)) {਍          洀漀搀甀氀攀 㴀 洀漀搀甀氀攀嬀洀漀搀甀氀攀⸀氀攀渀最琀栀 ⴀ ㄀崀㬀ഀഀ
        }਍        椀昀 ⠀攀⸀洀攀猀猀愀最攀 ☀☀ 攀⸀猀琀愀挀欀 ☀☀ 攀⸀猀琀愀挀欀⸀椀渀搀攀砀伀昀⠀攀⸀洀攀猀猀愀最攀⤀ 㴀㴀 ⴀ㄀⤀ 笀ഀഀ
          // Safari & FF's stack traces don't contain error.message content਍          ⼀⼀ 甀渀氀椀欀攀 琀栀漀猀攀 漀昀 䌀栀爀漀洀攀 愀渀搀 䤀䔀ഀഀ
          // So if stack doesn't contain message, we create a new string that contains both.਍          ⼀⼀ 匀椀渀挀攀 攀爀爀漀爀⸀猀琀愀挀欀 椀猀 爀攀愀搀ⴀ漀渀氀礀 椀渀 匀愀昀愀爀椀Ⰰ 䤀✀洀 漀瘀攀爀爀椀搀椀渀最 攀 愀渀搀 渀漀琀 攀⸀猀琀愀挀欀 栀攀爀攀⸀ഀഀ
          /* jshint -W022 */਍          攀 㴀 攀⸀洀攀猀猀愀最攀 ⬀ ✀尀渀✀ ⬀ 攀⸀猀琀愀挀欀㬀ഀഀ
        }਍        琀栀爀漀眀 ␀椀渀樀攀挀琀漀爀䴀椀渀䔀爀爀⠀✀洀漀搀甀氀攀爀爀✀Ⰰ ∀䘀愀椀氀攀搀 琀漀 椀渀猀琀愀渀琀椀愀琀攀 洀漀搀甀氀攀 笀　紀 搀甀攀 琀漀㨀尀渀笀㄀紀∀Ⰰഀഀ
                  module, e.stack || e.message || e);਍      紀ഀഀ
    });਍    爀攀琀甀爀渀 爀甀渀䈀氀漀挀欀猀㬀ഀഀ
  }਍ഀഀ
  ////////////////////////////////////਍  ⼀⼀ 椀渀琀攀爀渀愀氀 䤀渀樀攀挀琀漀爀ഀഀ
  ////////////////////////////////////਍ഀഀ
  function createInternalInjector(cache, factory) {਍ഀഀ
    function getService(serviceName) {਍      椀昀 ⠀挀愀挀栀攀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀猀攀爀瘀椀挀攀一愀洀攀⤀⤀ 笀ഀഀ
        if (cache[serviceName] === INSTANTIATING) {਍          琀栀爀漀眀 ␀椀渀樀攀挀琀漀爀䴀椀渀䔀爀爀⠀✀挀搀攀瀀✀Ⰰ ✀䌀椀爀挀甀氀愀爀 搀攀瀀攀渀搀攀渀挀礀 昀漀甀渀搀㨀 笀　紀✀Ⰰ 瀀愀琀栀⸀樀漀椀渀⠀✀ 㰀ⴀ ✀⤀⤀㬀ഀഀ
        }਍        爀攀琀甀爀渀 挀愀挀栀攀嬀猀攀爀瘀椀挀攀一愀洀攀崀㬀ഀഀ
      } else {਍        琀爀礀 笀ഀഀ
          path.unshift(serviceName);਍          挀愀挀栀攀嬀猀攀爀瘀椀挀攀一愀洀攀崀 㴀 䤀一匀吀䄀一吀䤀䄀吀䤀一䜀㬀ഀഀ
          return cache[serviceName] = factory(serviceName);਍        紀 挀愀琀挀栀 ⠀攀爀爀⤀ 笀ഀഀ
          if (cache[serviceName] === INSTANTIATING) {਍            搀攀氀攀琀攀 挀愀挀栀攀嬀猀攀爀瘀椀挀攀一愀洀攀崀㬀ഀഀ
          }਍          琀栀爀漀眀 攀爀爀㬀ഀഀ
        } finally {਍          瀀愀琀栀⸀猀栀椀昀琀⠀⤀㬀ഀഀ
        }਍      紀ഀഀ
    }਍ഀഀ
    function invoke(fn, self, locals){਍      瘀愀爀 愀爀最猀 㴀 嬀崀Ⰰഀഀ
          $inject = annotate(fn),਍          氀攀渀最琀栀Ⰰ 椀Ⰰഀഀ
          key;਍ഀഀ
      for(i = 0, length = $inject.length; i < length; i++) {਍        欀攀礀 㴀 ␀椀渀樀攀挀琀嬀椀崀㬀ഀഀ
        if (typeof key !== 'string') {਍          琀栀爀漀眀 ␀椀渀樀攀挀琀漀爀䴀椀渀䔀爀爀⠀✀椀琀欀渀✀Ⰰഀഀ
                  'Incorrect injection token! Expected service name as string, got {0}', key);਍        紀ഀഀ
        args.push(਍          氀漀挀愀氀猀 ☀☀ 氀漀挀愀氀猀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀欀攀礀⤀ഀഀ
          ? locals[key]਍          㨀 最攀琀匀攀爀瘀椀挀攀⠀欀攀礀⤀ഀഀ
        );਍      紀ഀഀ
      if (!fn.$inject) {਍        ⼀⼀ 琀栀椀猀 洀攀愀渀猀 琀栀愀琀 眀攀 洀甀猀琀 戀攀 愀渀 愀爀爀愀礀⸀ഀഀ
        fn = fn[length];਍      紀ഀഀ
਍      ⼀⼀ 栀琀琀瀀㨀⼀⼀樀猀瀀攀爀昀⸀挀漀洀⼀愀渀最甀氀愀爀樀猀ⴀ椀渀瘀漀欀攀ⴀ愀瀀瀀氀礀ⴀ瘀猀ⴀ猀眀椀琀挀栀ഀഀ
      // #5388਍      爀攀琀甀爀渀 昀渀⸀愀瀀瀀氀礀⠀猀攀氀昀Ⰰ 愀爀最猀⤀㬀ഀഀ
    }਍ഀഀ
    function instantiate(Type, locals) {਍      瘀愀爀 䌀漀渀猀琀爀甀挀琀漀爀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀紀Ⰰഀഀ
          instance, returnedValue;਍ഀഀ
      // Check if Type is annotated and use just the given function at n-1 as parameter਍      ⼀⼀ 攀⸀最⸀ 猀漀洀攀䴀漀搀甀氀攀⸀昀愀挀琀漀爀礀⠀✀最爀攀攀琀攀爀✀Ⰰ 嬀✀␀眀椀渀搀漀眀✀Ⰰ 昀甀渀挀琀椀漀渀⠀爀攀渀愀洀攀搀␀眀椀渀搀漀眀⤀ 笀紀崀⤀㬀ഀഀ
      Constructor.prototype = (isArray(Type) ? Type[Type.length - 1] : Type).prototype;਍      椀渀猀琀愀渀挀攀 㴀 渀攀眀 䌀漀渀猀琀爀甀挀琀漀爀⠀⤀㬀ഀഀ
      returnedValue = invoke(Type, instance, locals);਍ഀഀ
      return isObject(returnedValue) || isFunction(returnedValue) ? returnedValue : instance;਍    紀ഀഀ
਍    爀攀琀甀爀渀 笀ഀഀ
      invoke: invoke,਍      椀渀猀琀愀渀琀椀愀琀攀㨀 椀渀猀琀愀渀琀椀愀琀攀Ⰰഀഀ
      get: getService,਍      愀渀渀漀琀愀琀攀㨀 愀渀渀漀琀愀琀攀Ⰰഀഀ
      has: function(name) {਍        爀攀琀甀爀渀 瀀爀漀瘀椀搀攀爀䌀愀挀栀攀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀渀愀洀攀 ⬀ 瀀爀漀瘀椀搀攀爀匀甀昀昀椀砀⤀ 簀簀 挀愀挀栀攀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀渀愀洀攀⤀㬀ഀഀ
      }਍    紀㬀ഀഀ
  }਍紀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 渀最⸀␀愀渀挀栀漀爀匀挀爀漀氀氀ഀഀ
 * @requires $window਍ ⨀ 䀀爀攀焀甀椀爀攀猀 ␀氀漀挀愀琀椀漀渀ഀഀ
 * @requires $rootScope਍ ⨀ഀഀ
 * @description਍ ⨀ 圀栀攀渀 挀愀氀氀攀搀Ⰰ 椀琀 挀栀攀挀欀猀 挀甀爀爀攀渀琀 瘀愀氀甀攀 漀昀 怀␀氀漀挀愀琀椀漀渀⸀栀愀猀栀⠀⤀怀 愀渀搀 猀挀爀漀氀氀 琀漀 爀攀氀愀琀攀搀 攀氀攀洀攀渀琀Ⰰഀഀ
 * according to rules specified in਍ ⨀ 笀䀀氀椀渀欀 栀琀琀瀀㨀⼀⼀搀攀瘀⸀眀㌀⸀漀爀最⼀栀琀洀氀㔀⼀猀瀀攀挀⼀伀瘀攀爀瘀椀攀眀⸀栀琀洀氀⌀琀栀攀ⴀ椀渀搀椀挀愀琀攀搀ⴀ瀀愀爀琀ⴀ漀昀ⴀ琀栀攀ⴀ搀漀挀甀洀攀渀琀 䠀琀洀氀㔀 猀瀀攀挀紀⸀ഀഀ
 *਍ ⨀ 䤀琀 愀氀猀漀 眀愀琀挀栀攀猀 琀栀攀 怀␀氀漀挀愀琀椀漀渀⸀栀愀猀栀⠀⤀怀 愀渀搀 猀挀爀漀氀氀猀 眀栀攀渀攀瘀攀爀 椀琀 挀栀愀渀最攀猀 琀漀 洀愀琀挀栀 愀渀礀 愀渀挀栀漀爀⸀ഀഀ
 * This can be disabled by calling `$anchorScrollProvider.disableAutoScrolling()`.਍ ⨀ ഀഀ
 * @example਍   㰀攀砀愀洀瀀氀攀㸀ഀഀ
     <file name="index.html">਍       㰀搀椀瘀 椀搀㴀∀猀挀爀漀氀氀䄀爀攀愀∀ 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀匀挀爀漀氀氀䌀琀爀氀∀㸀ഀഀ
         <a ng-click="gotoBottom()">Go to bottom</a>਍         㰀愀 椀搀㴀∀戀漀琀琀漀洀∀㸀㰀⼀愀㸀 夀漀甀✀爀攀 愀琀 琀栀攀 戀漀琀琀漀洀℀ഀഀ
       </div>਍     㰀⼀昀椀氀攀㸀ഀഀ
     <file name="script.js">਍       昀甀渀挀琀椀漀渀 匀挀爀漀氀氀䌀琀爀氀⠀␀猀挀漀瀀攀Ⰰ ␀氀漀挀愀琀椀漀渀Ⰰ ␀愀渀挀栀漀爀匀挀爀漀氀氀⤀ 笀ഀഀ
         $scope.gotoBottom = function (){਍           ⼀⼀ 猀攀琀 琀栀攀 氀漀挀愀琀椀漀渀⸀栀愀猀栀 琀漀 琀栀攀 椀搀 漀昀ഀഀ
           // the element you wish to scroll to.਍           ␀氀漀挀愀琀椀漀渀⸀栀愀猀栀⠀✀戀漀琀琀漀洀✀⤀㬀ഀഀ
           ਍           ⼀⼀ 挀愀氀氀 ␀愀渀挀栀漀爀匀挀爀漀氀氀⠀⤀ഀഀ
           $anchorScroll();਍         紀ഀഀ
       }਍     㰀⼀昀椀氀攀㸀ഀഀ
     <file name="style.css">਍       ⌀猀挀爀漀氀氀䄀爀攀愀 笀ഀഀ
         height: 350px;਍         漀瘀攀爀昀氀漀眀㨀 愀甀琀漀㬀ഀഀ
       }਍ഀഀ
       #bottom {਍         搀椀猀瀀氀愀礀㨀 戀氀漀挀欀㬀ഀഀ
         margin-top: 2000px;਍       紀ഀഀ
     </file>਍   㰀⼀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 ␀䄀渀挀栀漀爀匀挀爀漀氀氀倀爀漀瘀椀搀攀爀⠀⤀ 笀ഀഀ
਍  瘀愀爀 愀甀琀漀匀挀爀漀氀氀椀渀最䔀渀愀戀氀攀搀 㴀 琀爀甀攀㬀ഀഀ
਍  琀栀椀猀⸀搀椀猀愀戀氀攀䄀甀琀漀匀挀爀漀氀氀椀渀最 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    autoScrollingEnabled = false;਍  紀㬀ഀഀ
਍  琀栀椀猀⸀␀最攀琀 㴀 嬀✀␀眀椀渀搀漀眀✀Ⰰ ✀␀氀漀挀愀琀椀漀渀✀Ⰰ ✀␀爀漀漀琀匀挀漀瀀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀眀椀渀搀漀眀Ⰰ ␀氀漀挀愀琀椀漀渀Ⰰ ␀爀漀漀琀匀挀漀瀀攀⤀ 笀ഀഀ
    var document = $window.document;਍ഀഀ
    // helper function to get first anchor from a NodeList਍    ⼀⼀ 挀愀渀✀琀 甀猀攀 昀椀氀琀攀爀⸀昀椀氀琀攀爀Ⰰ 愀猀 椀琀 愀挀挀攀瀀琀猀 漀渀氀礀 椀渀猀琀愀渀挀攀猀 漀昀 䄀爀爀愀礀ഀഀ
    // and IE can't convert NodeList to an array using [].slice਍    ⼀⼀ 吀伀䐀伀⠀瘀漀樀琀愀⤀㨀 甀猀攀 昀椀氀琀攀爀 椀昀 眀攀 挀栀愀渀最攀 椀琀 琀漀 愀挀挀攀瀀琀 氀椀猀琀猀 愀猀 眀攀氀氀ഀഀ
    function getFirstAnchor(list) {਍      瘀愀爀 爀攀猀甀氀琀 㴀 渀甀氀氀㬀ഀഀ
      forEach(list, function(element) {਍        椀昀 ⠀℀爀攀猀甀氀琀 ☀☀ 氀漀眀攀爀挀愀猀攀⠀攀氀攀洀攀渀琀⸀渀漀搀攀一愀洀攀⤀ 㴀㴀㴀 ✀愀✀⤀ 爀攀猀甀氀琀 㴀 攀氀攀洀攀渀琀㬀ഀഀ
      });਍      爀攀琀甀爀渀 爀攀猀甀氀琀㬀ഀഀ
    }਍ഀഀ
    function scroll() {਍      瘀愀爀 栀愀猀栀 㴀 ␀氀漀挀愀琀椀漀渀⸀栀愀猀栀⠀⤀Ⰰ 攀氀洀㬀ഀഀ
਍      ⼀⼀ 攀洀瀀琀礀 栀愀猀栀Ⰰ 猀挀爀漀氀氀 琀漀 琀栀攀 琀漀瀀 漀昀 琀栀攀 瀀愀最攀ഀഀ
      if (!hash) $window.scrollTo(0, 0);਍ഀഀ
      // element with given id਍      攀氀猀攀 椀昀 ⠀⠀攀氀洀 㴀 搀漀挀甀洀攀渀琀⸀最攀琀䔀氀攀洀攀渀琀䈀礀䤀搀⠀栀愀猀栀⤀⤀⤀ 攀氀洀⸀猀挀爀漀氀氀䤀渀琀漀嘀椀攀眀⠀⤀㬀ഀഀ
਍      ⼀⼀ 昀椀爀猀琀 愀渀挀栀漀爀 眀椀琀栀 最椀瘀攀渀 渀愀洀攀 㨀ⴀ䐀ഀഀ
      else if ((elm = getFirstAnchor(document.getElementsByName(hash)))) elm.scrollIntoView();਍ഀഀ
      // no element and hash == 'top', scroll to the top of the page਍      攀氀猀攀 椀昀 ⠀栀愀猀栀 㴀㴀㴀 ✀琀漀瀀✀⤀ ␀眀椀渀搀漀眀⸀猀挀爀漀氀氀吀漀⠀　Ⰰ 　⤀㬀ഀഀ
    }਍ഀഀ
    // does not scroll when user clicks on anchor link that is currently on਍    ⼀⼀ ⠀渀漀 甀爀氀 挀栀愀渀最攀Ⰰ 渀漀 ␀氀漀挀愀琀椀漀渀⸀栀愀猀栀⠀⤀ 挀栀愀渀最攀⤀Ⰰ 戀爀漀眀猀攀爀 渀愀琀椀瘀攀 搀漀攀猀 猀挀爀漀氀氀ഀഀ
    if (autoScrollingEnabled) {਍      ␀爀漀漀琀匀挀漀瀀攀⸀␀眀愀琀挀栀⠀昀甀渀挀琀椀漀渀 愀甀琀漀匀挀爀漀氀氀圀愀琀挀栀⠀⤀ 笀爀攀琀甀爀渀 ␀氀漀挀愀琀椀漀渀⸀栀愀猀栀⠀⤀㬀紀Ⰰഀഀ
        function autoScrollWatchAction() {਍          ␀爀漀漀琀匀挀漀瀀攀⸀␀攀瘀愀氀䄀猀礀渀挀⠀猀挀爀漀氀氀⤀㬀ഀഀ
        });਍    紀ഀഀ
਍    爀攀琀甀爀渀 猀挀爀漀氀氀㬀ഀഀ
  }];਍紀ഀഀ
਍瘀愀爀 ␀愀渀椀洀愀琀攀䴀椀渀䔀爀爀 㴀 洀椀渀䔀爀爀⠀✀␀愀渀椀洀愀琀攀✀⤀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc object਍ ⨀ 䀀渀愀洀攀 渀最⸀␀愀渀椀洀愀琀攀倀爀漀瘀椀搀攀爀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Default implementation of $animate that doesn't perform any animations, instead just਍ ⨀ 猀礀渀挀栀爀漀渀漀甀猀氀礀 瀀攀爀昀漀爀洀猀 䐀伀䴀ഀഀ
 * updates and calls done() callbacks.਍ ⨀ഀഀ
 * In order to enable animations the ngAnimate module has to be loaded.਍ ⨀ഀഀ
 * To see the functional implementation check out src/ngAnimate/animate.js਍ ⨀⼀ഀഀ
var $AnimateProvider = ['$provide', function($provide) {਍ഀഀ
  ਍  琀栀椀猀⸀␀␀猀攀氀攀挀琀漀爀猀 㴀 笀紀㬀ഀഀ
਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
   * @name ng.$animateProvider#register਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀愀渀椀洀愀琀攀倀爀漀瘀椀搀攀爀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Registers a new injectable animation factory function. The factory function produces the਍   ⨀ 愀渀椀洀愀琀椀漀渀 漀戀樀攀挀琀 眀栀椀挀栀 挀漀渀琀愀椀渀猀 挀愀氀氀戀愀挀欀 昀甀渀挀琀椀漀渀猀 昀漀爀 攀愀挀栀 攀瘀攀渀琀 琀栀愀琀 椀猀 攀砀瀀攀挀琀攀搀 琀漀 戀攀ഀഀ
   * animated.਍   ⨀ഀഀ
   *   * `eventFn`: `function(Element, doneFunction)` The element to animate, the `doneFunction`਍   ⨀   洀甀猀琀 戀攀 挀愀氀氀攀搀 漀渀挀攀 琀栀攀 攀氀攀洀攀渀琀 愀渀椀洀愀琀椀漀渀 椀猀 挀漀洀瀀氀攀琀攀⸀ 䤀昀 愀 昀甀渀挀琀椀漀渀 椀猀 爀攀琀甀爀渀攀搀 琀栀攀渀 琀栀攀ഀഀ
   *   animation service will use this function to cancel the animation whenever a cancel event is਍   ⨀   琀爀椀最最攀爀攀搀⸀ഀഀ
   *਍   ⨀ഀഀ
   *<pre>਍   ⨀   爀攀琀甀爀渀 笀ഀഀ
     *     eventFn : function(element, done) {਍     ⨀       ⼀⼀挀漀搀攀 琀漀 爀甀渀 琀栀攀 愀渀椀洀愀琀椀漀渀ഀഀ
     *       //once complete, then run done()਍     ⨀       爀攀琀甀爀渀 昀甀渀挀琀椀漀渀 挀愀渀挀攀氀氀愀琀椀漀渀䘀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
     *         //code to cancel the animation਍     ⨀       紀ഀഀ
     *     }਍     ⨀   紀ഀഀ
   *</pre>਍   ⨀ഀഀ
   * @param {string} name The name of the animation.਍   ⨀ 䀀瀀愀爀愀洀 笀昀甀渀挀琀椀漀渀紀 昀愀挀琀漀爀礀 吀栀攀 昀愀挀琀漀爀礀 昀甀渀挀琀椀漀渀 琀栀愀琀 眀椀氀氀 戀攀 攀砀攀挀甀琀攀搀 琀漀 爀攀琀甀爀渀 琀栀攀 愀渀椀洀愀琀椀漀渀ഀഀ
   *                           object.਍   ⨀⼀ഀഀ
  this.register = function(name, factory) {਍    瘀愀爀 欀攀礀 㴀 渀愀洀攀 ⬀ ✀ⴀ愀渀椀洀愀琀椀漀渀✀㬀ഀഀ
    if (name && name.charAt(0) != '.') throw $animateMinErr('notcsel',਍        ∀䔀砀瀀攀挀琀椀渀最 挀氀愀猀猀 猀攀氀攀挀琀漀爀 猀琀愀爀琀椀渀最 眀椀琀栀 ✀⸀✀ 最漀琀 ✀笀　紀✀⸀∀Ⰰ 渀愀洀攀⤀㬀ഀഀ
    this.$$selectors[name.substr(1)] = key;਍    ␀瀀爀漀瘀椀搀攀⸀昀愀挀琀漀爀礀⠀欀攀礀Ⰰ 昀愀挀琀漀爀礀⤀㬀ഀഀ
  };਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
   * @name ng.$animateProvider#classNameFilter਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀愀渀椀洀愀琀攀倀爀漀瘀椀搀攀爀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Sets and/or returns the CSS class regular expression that is checked when performing਍   ⨀ 愀渀 愀渀椀洀愀琀椀漀渀⸀ 唀瀀漀渀 戀漀漀琀猀琀爀愀瀀 琀栀攀 挀氀愀猀猀一愀洀攀䘀椀氀琀攀爀 瘀愀氀甀攀 椀猀 渀漀琀 猀攀琀 愀琀 愀氀氀 愀渀搀 眀椀氀氀ഀഀ
   * therefore enable $animate to attempt to perform an animation on any element.਍   ⨀ 圀栀攀渀 猀攀琀琀椀渀最 琀栀攀 挀氀愀猀猀一愀洀攀䘀椀氀琀攀爀 瘀愀氀甀攀Ⰰ 愀渀椀洀愀琀椀漀渀猀 眀椀氀氀 漀渀氀礀 戀攀 瀀攀爀昀漀爀洀攀搀 漀渀 攀氀攀洀攀渀琀猀ഀഀ
   * that successfully match the filter expression. This in turn can boost performance਍   ⨀ 昀漀爀 氀漀眀ⴀ瀀漀眀攀爀攀搀 搀攀瘀椀挀攀猀 愀猀 眀攀氀氀 愀猀 愀瀀瀀氀椀挀愀琀椀漀渀猀 挀漀渀琀愀椀渀椀渀最 愀 氀漀琀 漀昀 猀琀爀甀挀琀甀爀愀氀 漀瀀攀爀愀琀椀漀渀猀⸀ഀഀ
   * @param {RegExp=} expression The className expression which will be checked against all animations਍   ⨀ 䀀爀攀琀甀爀渀 笀刀攀最䔀砀瀀紀 吀栀攀 挀甀爀爀攀渀琀 䌀匀匀 挀氀愀猀猀一愀洀攀 攀砀瀀爀攀猀猀椀漀渀 瘀愀氀甀攀⸀ 䤀昀 渀甀氀氀 琀栀攀渀 琀栀攀爀攀 椀猀 渀漀 攀砀瀀爀攀猀猀椀漀渀 瘀愀氀甀攀ഀഀ
   */਍  琀栀椀猀⸀挀氀愀猀猀一愀洀攀䘀椀氀琀攀爀 㴀 昀甀渀挀琀椀漀渀⠀攀砀瀀爀攀猀猀椀漀渀⤀ 笀ഀഀ
    if(arguments.length === 1) {਍      琀栀椀猀⸀␀␀挀氀愀猀猀一愀洀攀䘀椀氀琀攀爀 㴀 ⠀攀砀瀀爀攀猀猀椀漀渀 椀渀猀琀愀渀挀攀漀昀 刀攀最䔀砀瀀⤀ 㼀 攀砀瀀爀攀猀猀椀漀渀 㨀 渀甀氀氀㬀ഀഀ
    }਍    爀攀琀甀爀渀 琀栀椀猀⸀␀␀挀氀愀猀猀一愀洀攀䘀椀氀琀攀爀㬀ഀഀ
  };਍ഀഀ
  this.$get = ['$timeout', function($timeout) {਍ഀഀ
    /**਍     ⨀ഀഀ
     * @ngdoc object਍     ⨀ 䀀渀愀洀攀 渀最⸀␀愀渀椀洀愀琀攀ഀഀ
     * @description The $animate service provides rudimentary DOM manipulation functions to਍     ⨀ 椀渀猀攀爀琀Ⰰ 爀攀洀漀瘀攀 愀渀搀 洀漀瘀攀 攀氀攀洀攀渀琀猀 眀椀琀栀椀渀 琀栀攀 䐀伀䴀Ⰰ 愀猀 眀攀氀氀 愀猀 愀搀搀椀渀最 愀渀搀 爀攀洀漀瘀椀渀最 挀氀愀猀猀攀猀⸀ഀഀ
     * This service is the core service used by the ngAnimate $animator service which provides਍     ⨀ 栀椀最栀ⴀ氀攀瘀攀氀 愀渀椀洀愀琀椀漀渀 栀漀漀欀猀 昀漀爀 䌀匀匀 愀渀搀 䨀愀瘀愀匀挀爀椀瀀琀⸀ഀഀ
     *਍     ⨀ ␀愀渀椀洀愀琀攀 椀猀 愀瘀愀椀氀愀戀氀攀 椀渀 琀栀攀 䄀渀最甀氀愀爀䨀匀 挀漀爀攀Ⰰ 栀漀眀攀瘀攀爀Ⰰ 琀栀攀 渀最䄀渀椀洀愀琀攀 洀漀搀甀氀攀 洀甀猀琀 戀攀 椀渀挀氀甀搀攀搀ഀഀ
     * to enable full out animation support. Otherwise, $animate will only perform simple DOM਍     ⨀ 洀愀渀椀瀀甀氀愀琀椀漀渀 漀瀀攀爀愀琀椀漀渀猀⸀ഀഀ
     *਍     ⨀ 吀漀 氀攀愀爀渀 洀漀爀攀 愀戀漀甀琀 攀渀愀戀氀椀渀最 愀渀椀洀愀琀椀漀渀 猀甀瀀瀀漀爀琀Ⰰ 挀氀椀挀欀 栀攀爀攀 琀漀 瘀椀猀椀琀 琀栀攀 笀䀀氀椀渀欀 渀最䄀渀椀洀愀琀攀ഀഀ
     * ngAnimate module page} as well as the {@link ngAnimate.$animate ngAnimate $animate service਍     ⨀ 瀀愀最攀紀⸀ഀഀ
     */਍    爀攀琀甀爀渀 笀ഀഀ
਍      ⼀⨀⨀ഀഀ
       *਍       ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
       * @name ng.$animate#enter਍       ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀愀渀椀洀愀琀攀ഀഀ
       * @function਍       ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀 䤀渀猀攀爀琀猀 琀栀攀 攀氀攀洀攀渀琀 椀渀琀漀 琀栀攀 䐀伀䴀 攀椀琀栀攀爀 愀昀琀攀爀 琀栀攀 怀愀昀琀攀爀怀 攀氀攀洀攀渀琀 漀爀 眀椀琀栀椀渀ഀഀ
       *   the `parent` element. Once complete, the done() callback will be fired (if provided).਍       ⨀ 䀀瀀愀爀愀洀 笀樀儀甀攀爀礀⼀樀焀䰀椀琀攀 攀氀攀洀攀渀琀紀 攀氀攀洀攀渀琀 琀栀攀 攀氀攀洀攀渀琀 眀栀椀挀栀 眀椀氀氀 戀攀 椀渀猀攀爀琀攀搀 椀渀琀漀 琀栀攀 䐀伀䴀ഀഀ
       * @param {jQuery/jqLite element} parent the parent element which will append the element as਍       ⨀   愀 挀栀椀氀搀 ⠀椀昀 琀栀攀 愀昀琀攀爀 攀氀攀洀攀渀琀 椀猀 渀漀琀 瀀爀攀猀攀渀琀⤀ഀഀ
       * @param {jQuery/jqLite element} after the sibling element which will append the element਍       ⨀   愀昀琀攀爀 椀琀猀攀氀昀ഀഀ
       * @param {function=} done callback function that will be called after the element has been਍       ⨀   椀渀猀攀爀琀攀搀 椀渀琀漀 琀栀攀 䐀伀䴀ഀഀ
       */਍      攀渀琀攀爀 㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 瀀愀爀攀渀琀Ⰰ 愀昀琀攀爀Ⰰ 搀漀渀攀⤀ 笀ഀഀ
        if (after) {਍          愀昀琀攀爀⸀愀昀琀攀爀⠀攀氀攀洀攀渀琀⤀㬀ഀഀ
        } else {਍          椀昀 ⠀℀瀀愀爀攀渀琀 簀簀 ℀瀀愀爀攀渀琀嬀　崀⤀ 笀ഀഀ
            parent = after.parent();਍          紀ഀഀ
          parent.append(element);਍        紀ഀഀ
        done && $timeout(done, 0, false);਍      紀Ⰰഀഀ
਍      ⼀⨀⨀ഀഀ
       *਍       ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
       * @name ng.$animate#leave਍       ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀愀渀椀洀愀琀攀ഀഀ
       * @function਍       ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀 刀攀洀漀瘀攀猀 琀栀攀 攀氀攀洀攀渀琀 昀爀漀洀 琀栀攀 䐀伀䴀⸀ 伀渀挀攀 挀漀洀瀀氀攀琀攀Ⰰ 琀栀攀 搀漀渀攀⠀⤀ 挀愀氀氀戀愀挀欀 眀椀氀氀 戀攀ഀഀ
       *   fired (if provided).਍       ⨀ 䀀瀀愀爀愀洀 笀樀儀甀攀爀礀⼀樀焀䰀椀琀攀 攀氀攀洀攀渀琀紀 攀氀攀洀攀渀琀 琀栀攀 攀氀攀洀攀渀琀 眀栀椀挀栀 眀椀氀氀 戀攀 爀攀洀漀瘀攀搀 昀爀漀洀 琀栀攀 䐀伀䴀ഀഀ
       * @param {function=} done callback function that will be called after the element has been਍       ⨀   爀攀洀漀瘀攀搀 昀爀漀洀 琀栀攀 䐀伀䴀ഀഀ
       */਍      氀攀愀瘀攀 㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 搀漀渀攀⤀ 笀ഀഀ
        element.remove();਍        搀漀渀攀 ☀☀ ␀琀椀洀攀漀甀琀⠀搀漀渀攀Ⰰ 　Ⰰ 昀愀氀猀攀⤀㬀ഀഀ
      },਍ഀഀ
      /**਍       ⨀ഀഀ
       * @ngdoc function਍       ⨀ 䀀渀愀洀攀 渀最⸀␀愀渀椀洀愀琀攀⌀洀漀瘀攀ഀഀ
       * @methodOf ng.$animate਍       ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
       * @description Moves the position of the provided element within the DOM to be placed਍       ⨀ 攀椀琀栀攀爀 愀昀琀攀爀 琀栀攀 怀愀昀琀攀爀怀 攀氀攀洀攀渀琀 漀爀 椀渀猀椀搀攀 漀昀 琀栀攀 怀瀀愀爀攀渀琀怀 攀氀攀洀攀渀琀⸀ 伀渀挀攀 挀漀洀瀀氀攀琀攀Ⰰ 琀栀攀ഀഀ
       * done() callback will be fired (if provided).਍       ⨀ ഀഀ
       * @param {jQuery/jqLite element} element the element which will be moved around within the਍       ⨀   䐀伀䴀ഀഀ
       * @param {jQuery/jqLite element} parent the parent element where the element will be਍       ⨀   椀渀猀攀爀琀攀搀 椀渀琀漀 ⠀椀昀 琀栀攀 愀昀琀攀爀 攀氀攀洀攀渀琀 椀猀 渀漀琀 瀀爀攀猀攀渀琀⤀ഀഀ
       * @param {jQuery/jqLite element} after the sibling element where the element will be਍       ⨀   瀀漀猀椀琀椀漀渀攀搀 渀攀砀琀 琀漀ഀഀ
       * @param {function=} done the callback function (if provided) that will be fired after the਍       ⨀   攀氀攀洀攀渀琀 栀愀猀 戀攀攀渀 洀漀瘀攀搀 琀漀 椀琀猀 渀攀眀 瀀漀猀椀琀椀漀渀ഀഀ
       */਍      洀漀瘀攀 㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 瀀愀爀攀渀琀Ⰰ 愀昀琀攀爀Ⰰ 搀漀渀攀⤀ 笀ഀഀ
        // Do not remove element before insert. Removing will cause data associated with the਍        ⼀⼀ 攀氀攀洀攀渀琀 琀漀 戀攀 搀爀漀瀀瀀攀搀⸀ 䤀渀猀攀爀琀 眀椀氀氀 椀洀瀀氀椀挀椀琀氀礀 搀漀 琀栀攀 爀攀洀漀瘀攀⸀ഀഀ
        this.enter(element, parent, after, done);਍      紀Ⰰഀഀ
਍      ⼀⨀⨀ഀഀ
       *਍       ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
       * @name ng.$animate#addClass਍       ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀愀渀椀洀愀琀攀ഀഀ
       * @function਍       ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀 䄀搀搀猀 琀栀攀 瀀爀漀瘀椀搀攀搀 挀氀愀猀猀一愀洀攀 䌀匀匀 挀氀愀猀猀 瘀愀氀甀攀 琀漀 琀栀攀 瀀爀漀瘀椀搀攀搀 攀氀攀洀攀渀琀⸀ 伀渀挀攀ഀഀ
       * complete, the done() callback will be fired (if provided).਍       ⨀ 䀀瀀愀爀愀洀 笀樀儀甀攀爀礀⼀樀焀䰀椀琀攀 攀氀攀洀攀渀琀紀 攀氀攀洀攀渀琀 琀栀攀 攀氀攀洀攀渀琀 眀栀椀挀栀 眀椀氀氀 栀愀瘀攀 琀栀攀 挀氀愀猀猀一愀洀攀 瘀愀氀甀攀ഀഀ
       *   added to it਍       ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 挀氀愀猀猀一愀洀攀 琀栀攀 䌀匀匀 挀氀愀猀猀 眀栀椀挀栀 眀椀氀氀 戀攀 愀搀搀攀搀 琀漀 琀栀攀 攀氀攀洀攀渀琀ഀഀ
       * @param {function=} done the callback function (if provided) that will be fired after the਍       ⨀   挀氀愀猀猀一愀洀攀 瘀愀氀甀攀 栀愀猀 戀攀攀渀 愀搀搀攀搀 琀漀 琀栀攀 攀氀攀洀攀渀琀ഀഀ
       */਍      愀搀搀䌀氀愀猀猀 㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 挀氀愀猀猀一愀洀攀Ⰰ 搀漀渀攀⤀ 笀ഀഀ
        className = isString(className) ?਍                      挀氀愀猀猀一愀洀攀 㨀ഀഀ
                      isArray(className) ? className.join(' ') : '';਍        昀漀爀䔀愀挀栀⠀攀氀攀洀攀渀琀Ⰰ 昀甀渀挀琀椀漀渀 ⠀攀氀攀洀攀渀琀⤀ 笀ഀഀ
          jqLiteAddClass(element, className);਍        紀⤀㬀ഀഀ
        done && $timeout(done, 0, false);਍      紀Ⰰഀഀ
਍      ⼀⨀⨀ഀഀ
       *਍       ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
       * @name ng.$animate#removeClass਍       ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀愀渀椀洀愀琀攀ഀഀ
       * @function਍       ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀 刀攀洀漀瘀攀猀 琀栀攀 瀀爀漀瘀椀搀攀搀 挀氀愀猀猀一愀洀攀 䌀匀匀 挀氀愀猀猀 瘀愀氀甀攀 昀爀漀洀 琀栀攀 瀀爀漀瘀椀搀攀搀 攀氀攀洀攀渀琀⸀ഀഀ
       * Once complete, the done() callback will be fired (if provided).਍       ⨀ 䀀瀀愀爀愀洀 笀樀儀甀攀爀礀⼀樀焀䰀椀琀攀 攀氀攀洀攀渀琀紀 攀氀攀洀攀渀琀 琀栀攀 攀氀攀洀攀渀琀 眀栀椀挀栀 眀椀氀氀 栀愀瘀攀 琀栀攀 挀氀愀猀猀一愀洀攀 瘀愀氀甀攀ഀഀ
       *   removed from it਍       ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 挀氀愀猀猀一愀洀攀 琀栀攀 䌀匀匀 挀氀愀猀猀 眀栀椀挀栀 眀椀氀氀 戀攀 爀攀洀漀瘀攀搀 昀爀漀洀 琀栀攀 攀氀攀洀攀渀琀ഀഀ
       * @param {function=} done the callback function (if provided) that will be fired after the਍       ⨀   挀氀愀猀猀一愀洀攀 瘀愀氀甀攀 栀愀猀 戀攀攀渀 爀攀洀漀瘀攀搀 昀爀漀洀 琀栀攀 攀氀攀洀攀渀琀ഀഀ
       */਍      爀攀洀漀瘀攀䌀氀愀猀猀 㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 挀氀愀猀猀一愀洀攀Ⰰ 搀漀渀攀⤀ 笀ഀഀ
        className = isString(className) ?਍                      挀氀愀猀猀一愀洀攀 㨀ഀഀ
                      isArray(className) ? className.join(' ') : '';਍        昀漀爀䔀愀挀栀⠀攀氀攀洀攀渀琀Ⰰ 昀甀渀挀琀椀漀渀 ⠀攀氀攀洀攀渀琀⤀ 笀ഀഀ
          jqLiteRemoveClass(element, className);਍        紀⤀㬀ഀഀ
        done && $timeout(done, 0, false);਍      紀Ⰰഀഀ
਍      攀渀愀戀氀攀搀 㨀 渀漀漀瀀ഀഀ
    };਍  紀崀㬀ഀഀ
}];਍ഀഀ
/**਍ ⨀ ℀ 吀栀椀猀 椀猀 愀 瀀爀椀瘀愀琀攀 甀渀搀漀挀甀洀攀渀琀攀搀 猀攀爀瘀椀挀攀 ℀ഀഀ
 *਍ ⨀ 䀀渀愀洀攀 渀最⸀␀戀爀漀眀猀攀爀ഀഀ
 * @requires $log਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * This object has two goals:਍ ⨀ഀഀ
 * - hide all the global state in the browser caused by the window object਍ ⨀ ⴀ 愀戀猀琀爀愀挀琀 愀眀愀礀 愀氀氀 琀栀攀 戀爀漀眀猀攀爀 猀瀀攀挀椀昀椀挀 昀攀愀琀甀爀攀猀 愀渀搀 椀渀挀漀渀猀椀猀琀攀渀挀椀攀猀ഀഀ
 *਍ ⨀ 䘀漀爀 琀攀猀琀猀 眀攀 瀀爀漀瘀椀搀攀 笀䀀氀椀渀欀 渀最䴀漀挀欀⸀␀戀爀漀眀猀攀爀 洀漀挀欀 椀洀瀀氀攀洀攀渀琀愀琀椀漀渀紀 漀昀 琀栀攀 怀␀戀爀漀眀猀攀爀怀ഀഀ
 * service, which can be used for convenient testing of the application without the interaction with਍ ⨀ 琀栀攀 爀攀愀氀 戀爀漀眀猀攀爀 愀瀀椀猀⸀ഀഀ
 */਍⼀⨀⨀ഀഀ
 * @param {object} window The global window object.਍ ⨀ 䀀瀀愀爀愀洀 笀漀戀樀攀挀琀紀 搀漀挀甀洀攀渀琀 樀儀甀攀爀礀 眀爀愀瀀瀀攀搀 搀漀挀甀洀攀渀琀⸀ഀഀ
 * @param {function()} XHR XMLHttpRequest constructor.਍ ⨀ 䀀瀀愀爀愀洀 笀漀戀樀攀挀琀紀 ␀氀漀最 挀漀渀猀漀氀攀⸀氀漀最 漀爀 愀渀 漀戀樀攀挀琀 眀椀琀栀 琀栀攀 猀愀洀攀 椀渀琀攀爀昀愀挀攀⸀ഀഀ
 * @param {object} $sniffer $sniffer service਍ ⨀⼀ഀഀ
function Browser(window, document, $log, $sniffer) {਍  瘀愀爀 猀攀氀昀 㴀 琀栀椀猀Ⰰഀഀ
      rawDocument = document[0],਍      氀漀挀愀琀椀漀渀 㴀 眀椀渀搀漀眀⸀氀漀挀愀琀椀漀渀Ⰰഀഀ
      history = window.history,਍      猀攀琀吀椀洀攀漀甀琀 㴀 眀椀渀搀漀眀⸀猀攀琀吀椀洀攀漀甀琀Ⰰഀഀ
      clearTimeout = window.clearTimeout,਍      瀀攀渀搀椀渀最䐀攀昀攀爀䤀搀猀 㴀 笀紀㬀ഀഀ
਍  猀攀氀昀⸀椀猀䴀漀挀欀 㴀 昀愀氀猀攀㬀ഀഀ
਍  瘀愀爀 漀甀琀猀琀愀渀搀椀渀最刀攀焀甀攀猀琀䌀漀甀渀琀 㴀 　㬀ഀഀ
  var outstandingRequestCallbacks = [];਍ഀഀ
  // TODO(vojta): remove this temporary api਍  猀攀氀昀⸀␀␀挀漀洀瀀氀攀琀攀伀甀琀猀琀愀渀搀椀渀最刀攀焀甀攀猀琀 㴀 挀漀洀瀀氀攀琀攀伀甀琀猀琀愀渀搀椀渀最刀攀焀甀攀猀琀㬀ഀഀ
  self.$$incOutstandingRequestCount = function() { outstandingRequestCount++; };਍ഀഀ
  /**਍   ⨀ 䔀砀攀挀甀琀攀猀 琀栀攀 怀昀渀怀 昀甀渀挀琀椀漀渀⠀猀甀瀀瀀漀爀琀猀 挀甀爀爀礀椀渀最⤀ 愀渀搀 搀攀挀爀攀洀攀渀琀猀 琀栀攀 怀漀甀琀猀琀愀渀搀椀渀最刀攀焀甀攀猀琀䌀愀氀氀戀愀挀欀猀怀ഀഀ
   * counter. If the counter reaches 0, all the `outstandingRequestCallbacks` are executed.਍   ⨀⼀ഀഀ
  function completeOutstandingRequest(fn) {਍    琀爀礀 笀ഀഀ
      fn.apply(null, sliceArgs(arguments, 1));਍    紀 昀椀渀愀氀氀礀 笀ഀഀ
      outstandingRequestCount--;਍      椀昀 ⠀漀甀琀猀琀愀渀搀椀渀最刀攀焀甀攀猀琀䌀漀甀渀琀 㴀㴀㴀 　⤀ 笀ഀഀ
        while(outstandingRequestCallbacks.length) {਍          琀爀礀 笀ഀഀ
            outstandingRequestCallbacks.pop()();਍          紀 挀愀琀挀栀 ⠀攀⤀ 笀ഀഀ
            $log.error(e);਍          紀ഀഀ
        }਍      紀ഀഀ
    }਍  紀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @private਍   ⨀ 一漀琀攀㨀 琀栀椀猀 洀攀琀栀漀搀 椀猀 甀猀攀搀 漀渀氀礀 戀礀 猀挀攀渀愀爀椀漀 爀甀渀渀攀爀ഀഀ
   * TODO(vojta): prefix this method with $$ ?਍   ⨀ 䀀瀀愀爀愀洀 笀昀甀渀挀琀椀漀渀⠀⤀紀 挀愀氀氀戀愀挀欀 䘀甀渀挀琀椀漀渀 琀栀愀琀 眀椀氀氀 戀攀 挀愀氀氀攀搀 眀栀攀渀 渀漀 漀甀琀猀琀愀渀搀椀渀最 爀攀焀甀攀猀琀ഀഀ
   */਍  猀攀氀昀⸀渀漀琀椀昀礀圀栀攀渀一漀伀甀琀猀琀愀渀搀椀渀最刀攀焀甀攀猀琀猀 㴀 昀甀渀挀琀椀漀渀⠀挀愀氀氀戀愀挀欀⤀ 笀ഀഀ
    // force browser to execute all pollFns - this is needed so that cookies and other pollers fire਍    ⼀⼀ 愀琀 猀漀洀攀 搀攀琀攀爀洀椀渀椀猀琀椀挀 琀椀洀攀 椀渀 爀攀猀瀀攀挀琀 琀漀 琀栀攀 琀攀猀琀 爀甀渀渀攀爀✀猀 愀挀琀椀漀渀猀⸀ 䰀攀愀瘀椀渀最 琀栀椀渀最猀 甀瀀 琀漀 琀栀攀ഀഀ
    // regular poller would result in flaky tests.਍    昀漀爀䔀愀挀栀⠀瀀漀氀氀䘀渀猀Ⰰ 昀甀渀挀琀椀漀渀⠀瀀漀氀氀䘀渀⤀笀 瀀漀氀氀䘀渀⠀⤀㬀 紀⤀㬀ഀഀ
਍    椀昀 ⠀漀甀琀猀琀愀渀搀椀渀最刀攀焀甀攀猀琀䌀漀甀渀琀 㴀㴀㴀 　⤀ 笀ഀഀ
      callback();਍    紀 攀氀猀攀 笀ഀഀ
      outstandingRequestCallbacks.push(callback);਍    紀ഀഀ
  };਍ഀഀ
  //////////////////////////////////////////////////////////////਍  ⼀⼀ 倀漀氀氀 圀愀琀挀栀攀爀 䄀倀䤀ഀഀ
  //////////////////////////////////////////////////////////////਍  瘀愀爀 瀀漀氀氀䘀渀猀 㴀 嬀崀Ⰰഀഀ
      pollTimeout;਍ഀഀ
  /**਍   ⨀ 䀀渀愀洀攀 渀最⸀␀戀爀漀眀猀攀爀⌀愀搀搀倀漀氀氀䘀渀ഀഀ
   * @methodOf ng.$browser਍   ⨀ഀഀ
   * @param {function()} fn Poll function to add਍   ⨀ഀഀ
   * @description਍   ⨀ 䄀搀搀猀 愀 昀甀渀挀琀椀漀渀 琀漀 琀栀攀 氀椀猀琀 漀昀 昀甀渀挀琀椀漀渀猀 琀栀愀琀 瀀漀氀氀攀爀 瀀攀爀椀漀搀椀挀愀氀氀礀 攀砀攀挀甀琀攀猀Ⰰഀഀ
   * and starts polling if not started yet.਍   ⨀ഀഀ
   * @returns {function()} the added function਍   ⨀⼀ഀഀ
  self.addPollFn = function(fn) {਍    椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀瀀漀氀氀吀椀洀攀漀甀琀⤀⤀ 猀琀愀爀琀倀漀氀氀攀爀⠀㄀　　Ⰰ 猀攀琀吀椀洀攀漀甀琀⤀㬀ഀഀ
    pollFns.push(fn);਍    爀攀琀甀爀渀 昀渀㬀ഀഀ
  };਍ഀഀ
  /**਍   ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀紀 椀渀琀攀爀瘀愀氀 䠀漀眀 漀昀琀攀渀 猀栀漀甀氀搀 戀爀漀眀猀攀爀 挀愀氀氀 瀀漀氀氀 昀甀渀挀琀椀漀渀猀 ⠀洀猀⤀ഀഀ
   * @param {function()} setTimeout Reference to a real or fake `setTimeout` function.਍   ⨀ഀഀ
   * @description਍   ⨀ 䌀漀渀昀椀最甀爀攀猀 琀栀攀 瀀漀氀氀攀爀 琀漀 爀甀渀 椀渀 琀栀攀 猀瀀攀挀椀昀椀攀搀 椀渀琀攀爀瘀愀氀猀Ⰰ 甀猀椀渀最 琀栀攀 猀瀀攀挀椀昀椀攀搀ഀഀ
   * setTimeout fn and kicks it off.਍   ⨀⼀ഀഀ
  function startPoller(interval, setTimeout) {਍    ⠀昀甀渀挀琀椀漀渀 挀栀攀挀欀⠀⤀ 笀ഀഀ
      forEach(pollFns, function(pollFn){ pollFn(); });਍      瀀漀氀氀吀椀洀攀漀甀琀 㴀 猀攀琀吀椀洀攀漀甀琀⠀挀栀攀挀欀Ⰰ 椀渀琀攀爀瘀愀氀⤀㬀ഀഀ
    })();਍  紀ഀഀ
਍  ⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
  // URL API਍  ⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
਍  瘀愀爀 氀愀猀琀䈀爀漀眀猀攀爀唀爀氀 㴀 氀漀挀愀琀椀漀渀⸀栀爀攀昀Ⰰഀഀ
      baseElement = document.find('base'),਍      渀攀眀䰀漀挀愀琀椀漀渀 㴀 渀甀氀氀㬀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @name ng.$browser#url਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀戀爀漀眀猀攀爀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * GETTER:਍   ⨀ 圀椀琀栀漀甀琀 愀渀礀 愀爀最甀洀攀渀琀Ⰰ 琀栀椀猀 洀攀琀栀漀搀 樀甀猀琀 爀攀琀甀爀渀猀 挀甀爀爀攀渀琀 瘀愀氀甀攀 漀昀 氀漀挀愀琀椀漀渀⸀栀爀攀昀⸀ഀഀ
   *਍   ⨀ 匀䔀吀吀䔀刀㨀ഀഀ
   * With at least one argument, this method sets url to new value.਍   ⨀ 䤀昀 栀琀洀氀㔀 栀椀猀琀漀爀礀 愀瀀椀 猀甀瀀瀀漀爀琀攀搀Ⰰ 瀀甀猀栀匀琀愀琀攀⼀爀攀瀀氀愀挀攀匀琀愀琀攀 椀猀 甀猀攀搀Ⰰ 漀琀栀攀爀眀椀猀攀ഀഀ
   * location.href/location.replace is used.਍   ⨀ 刀攀琀甀爀渀猀 椀琀猀 漀眀渀 椀渀猀琀愀渀挀攀 琀漀 愀氀氀漀眀 挀栀愀椀渀椀渀最ഀഀ
   *਍   ⨀ 一伀吀䔀㨀 琀栀椀猀 愀瀀椀 椀猀 椀渀琀攀渀搀攀搀 昀漀爀 甀猀攀 漀渀氀礀 戀礀 琀栀攀 ␀氀漀挀愀琀椀漀渀 猀攀爀瘀椀挀攀⸀ 倀氀攀愀猀攀 甀猀攀 琀栀攀ഀഀ
   * {@link ng.$location $location service} to change url.਍   ⨀ഀഀ
   * @param {string} url New url (when used as setter)਍   ⨀ 䀀瀀愀爀愀洀 笀戀漀漀氀攀愀渀㴀紀 爀攀瀀氀愀挀攀 匀栀漀甀氀搀 渀攀眀 甀爀氀 爀攀瀀氀愀挀攀 挀甀爀爀攀渀琀 栀椀猀琀漀爀礀 爀攀挀漀爀搀 㼀ഀഀ
   */਍  猀攀氀昀⸀甀爀氀 㴀 昀甀渀挀琀椀漀渀⠀甀爀氀Ⰰ 爀攀瀀氀愀挀攀⤀ 笀ഀഀ
    // Android Browser BFCache causes location, history reference to become stale.਍    椀昀 ⠀氀漀挀愀琀椀漀渀 ℀㴀㴀 眀椀渀搀漀眀⸀氀漀挀愀琀椀漀渀⤀ 氀漀挀愀琀椀漀渀 㴀 眀椀渀搀漀眀⸀氀漀挀愀琀椀漀渀㬀ഀഀ
    if (history !== window.history) history = window.history;਍ഀഀ
    // setter਍    椀昀 ⠀甀爀氀⤀ 笀ഀഀ
      if (lastBrowserUrl == url) return;਍      氀愀猀琀䈀爀漀眀猀攀爀唀爀氀 㴀 甀爀氀㬀ഀഀ
      if ($sniffer.history) {਍        椀昀 ⠀爀攀瀀氀愀挀攀⤀ 栀椀猀琀漀爀礀⸀爀攀瀀氀愀挀攀匀琀愀琀攀⠀渀甀氀氀Ⰰ ✀✀Ⰰ 甀爀氀⤀㬀ഀഀ
        else {਍          栀椀猀琀漀爀礀⸀瀀甀猀栀匀琀愀琀攀⠀渀甀氀氀Ⰰ ✀✀Ⰰ 甀爀氀⤀㬀ഀഀ
          // Crazy Opera Bug: http://my.opera.com/community/forums/topic.dml?id=1185462਍          戀愀猀攀䔀氀攀洀攀渀琀⸀愀琀琀爀⠀✀栀爀攀昀✀Ⰰ 戀愀猀攀䔀氀攀洀攀渀琀⸀愀琀琀爀⠀✀栀爀攀昀✀⤀⤀㬀ഀഀ
        }਍      紀 攀氀猀攀 笀ഀഀ
        newLocation = url;਍        椀昀 ⠀爀攀瀀氀愀挀攀⤀ 笀ഀഀ
          location.replace(url);਍        紀 攀氀猀攀 笀ഀഀ
          location.href = url;਍        紀ഀഀ
      }਍      爀攀琀甀爀渀 猀攀氀昀㬀ഀഀ
    // getter਍    紀 攀氀猀攀 笀ഀഀ
      // - newLocation is a workaround for an IE7-9 issue with location.replace and location.href਍      ⼀⼀   洀攀琀栀漀搀猀 渀漀琀 甀瀀搀愀琀椀渀最 氀漀挀愀琀椀漀渀⸀栀爀攀昀 猀礀渀挀栀爀漀渀漀甀猀氀礀⸀ഀഀ
      // - the replacement is a workaround for https://bugzilla.mozilla.org/show_bug.cgi?id=407172਍      爀攀琀甀爀渀 渀攀眀䰀漀挀愀琀椀漀渀 簀簀 氀漀挀愀琀椀漀渀⸀栀爀攀昀⸀爀攀瀀氀愀挀攀⠀⼀─㈀㜀⼀最Ⰰ∀✀∀⤀㬀ഀഀ
    }਍  紀㬀ഀഀ
਍  瘀愀爀 甀爀氀䌀栀愀渀最攀䰀椀猀琀攀渀攀爀猀 㴀 嬀崀Ⰰഀഀ
      urlChangeInit = false;਍ഀഀ
  function fireUrlChange() {਍    渀攀眀䰀漀挀愀琀椀漀渀 㴀 渀甀氀氀㬀ഀഀ
    if (lastBrowserUrl == self.url()) return;਍ഀഀ
    lastBrowserUrl = self.url();਍    昀漀爀䔀愀挀栀⠀甀爀氀䌀栀愀渀最攀䰀椀猀琀攀渀攀爀猀Ⰰ 昀甀渀挀琀椀漀渀⠀氀椀猀琀攀渀攀爀⤀ 笀ഀഀ
      listener(self.url());਍    紀⤀㬀ഀഀ
  }਍ഀഀ
  /**਍   ⨀ 䀀渀愀洀攀 渀最⸀␀戀爀漀眀猀攀爀⌀漀渀唀爀氀䌀栀愀渀最攀ഀഀ
   * @methodOf ng.$browser਍   ⨀ 䀀吀伀䐀伀⠀瘀漀樀琀愀⤀㨀 爀攀昀愀挀琀漀爀 琀漀 甀猀攀 渀漀搀攀✀猀 猀礀渀琀愀砀 昀漀爀 攀瘀攀渀琀猀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Register callback function that will be called, when url changes.਍   ⨀ഀഀ
   * It's only called when the url is changed from outside of angular:਍   ⨀ ⴀ 甀猀攀爀 琀礀瀀攀猀 搀椀昀昀攀爀攀渀琀 甀爀氀 椀渀琀漀 愀搀搀爀攀猀猀 戀愀爀ഀഀ
   * - user clicks on history (forward/back) button਍   ⨀ ⴀ 甀猀攀爀 挀氀椀挀欀猀 漀渀 愀 氀椀渀欀ഀഀ
   *਍   ⨀ 䤀琀✀猀 渀漀琀 挀愀氀氀攀搀 眀栀攀渀 甀爀氀 椀猀 挀栀愀渀最攀搀 戀礀 ␀戀爀漀眀猀攀爀⸀甀爀氀⠀⤀ 洀攀琀栀漀搀ഀഀ
   *਍   ⨀ 吀栀攀 氀椀猀琀攀渀攀爀 最攀琀猀 挀愀氀氀攀搀 眀椀琀栀 渀攀眀 甀爀氀 愀猀 瀀愀爀愀洀攀琀攀爀⸀ഀഀ
   *਍   ⨀ 一伀吀䔀㨀 琀栀椀猀 愀瀀椀 椀猀 椀渀琀攀渀搀攀搀 昀漀爀 甀猀攀 漀渀氀礀 戀礀 琀栀攀 ␀氀漀挀愀琀椀漀渀 猀攀爀瘀椀挀攀⸀ 倀氀攀愀猀攀 甀猀攀 琀栀攀ഀഀ
   * {@link ng.$location $location service} to monitor url changes in angular apps.਍   ⨀ഀഀ
   * @param {function(string)} listener Listener function to be called when url changes.਍   ⨀ 䀀爀攀琀甀爀渀 笀昀甀渀挀琀椀漀渀⠀猀琀爀椀渀最⤀紀 刀攀琀甀爀渀猀 琀栀攀 爀攀最椀猀琀攀爀攀搀 氀椀猀琀攀渀攀爀 昀渀 ⴀ 栀愀渀搀礀 椀昀 琀栀攀 昀渀 椀猀 愀渀漀渀礀洀漀甀猀⸀ഀഀ
   */਍  猀攀氀昀⸀漀渀唀爀氀䌀栀愀渀最攀 㴀 昀甀渀挀琀椀漀渀⠀挀愀氀氀戀愀挀欀⤀ 笀ഀഀ
    if (!urlChangeInit) {਍      ⼀⼀ 圀攀 氀椀猀琀攀渀 漀渀 戀漀琀栀 ⠀栀愀猀栀挀栀愀渀最攀⼀瀀漀瀀猀琀愀琀攀⤀ 眀栀攀渀 愀瘀愀椀氀愀戀氀攀Ⰰ 愀猀 猀漀洀攀 戀爀漀眀猀攀爀猀 ⠀攀⸀最⸀ 伀瀀攀爀愀⤀ഀഀ
      // don't fire popstate when user change the address bar and don't fire hashchange when url਍      ⼀⼀ 挀栀愀渀最攀搀 戀礀 瀀甀猀栀⼀爀攀瀀氀愀挀攀匀琀愀琀攀ഀഀ
਍      ⼀⼀ 栀琀洀氀㔀 栀椀猀琀漀爀礀 愀瀀椀 ⴀ 瀀漀瀀猀琀愀琀攀 攀瘀攀渀琀ഀഀ
      if ($sniffer.history) jqLite(window).on('popstate', fireUrlChange);਍      ⼀⼀ 栀愀猀栀挀栀愀渀最攀 攀瘀攀渀琀ഀഀ
      if ($sniffer.hashchange) jqLite(window).on('hashchange', fireUrlChange);਍      ⼀⼀ 瀀漀氀氀椀渀最ഀഀ
      else self.addPollFn(fireUrlChange);਍ഀഀ
      urlChangeInit = true;਍    紀ഀഀ
਍    甀爀氀䌀栀愀渀最攀䰀椀猀琀攀渀攀爀猀⸀瀀甀猀栀⠀挀愀氀氀戀愀挀欀⤀㬀ഀഀ
    return callback;਍  紀㬀ഀഀ
਍  ⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
  // Misc API਍  ⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @name ng.$browser#baseHref਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀戀爀漀眀猀攀爀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Returns current <base href>਍   ⨀ ⠀愀氀眀愀礀猀 爀攀氀愀琀椀瘀攀 ⴀ 眀椀琀栀漀甀琀 搀漀洀愀椀渀⤀ഀഀ
   *਍   ⨀ 䀀爀攀琀甀爀渀猀 笀猀琀爀椀渀最㴀紀 挀甀爀爀攀渀琀 㰀戀愀猀攀 栀爀攀昀㸀ഀഀ
   */਍  猀攀氀昀⸀戀愀猀攀䠀爀攀昀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var href = baseElement.attr('href');਍    爀攀琀甀爀渀 栀爀攀昀 㼀 栀爀攀昀⸀爀攀瀀氀愀挀攀⠀⼀帀⠀栀琀琀瀀猀㼀尀㨀⤀㼀尀⼀尀⼀嬀帀尀⼀崀⨀⼀Ⰰ ✀✀⤀ 㨀 ✀✀㬀ഀഀ
  };਍ഀഀ
  //////////////////////////////////////////////////////////////਍  ⼀⼀ 䌀漀漀欀椀攀猀 䄀倀䤀ഀഀ
  //////////////////////////////////////////////////////////////਍  瘀愀爀 氀愀猀琀䌀漀漀欀椀攀猀 㴀 笀紀㬀ഀഀ
  var lastCookieString = '';਍  瘀愀爀 挀漀漀欀椀攀倀愀琀栀 㴀 猀攀氀昀⸀戀愀猀攀䠀爀攀昀⠀⤀㬀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @name ng.$browser#cookies਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀戀爀漀眀猀攀爀ഀഀ
   *਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 渀愀洀攀 䌀漀漀欀椀攀 渀愀洀攀ഀഀ
   * @param {string=} value Cookie value਍   ⨀ഀഀ
   * @description਍   ⨀ 吀栀攀 挀漀漀欀椀攀猀 洀攀琀栀漀搀 瀀爀漀瘀椀搀攀猀 愀 ✀瀀爀椀瘀愀琀攀✀ 氀漀眀 氀攀瘀攀氀 愀挀挀攀猀猀 琀漀 戀爀漀眀猀攀爀 挀漀漀欀椀攀猀⸀ഀഀ
   * It is not meant to be used directly, use the $cookie service instead.਍   ⨀ഀഀ
   * The return values vary depending on the arguments that the method was called with as follows:਍   ⨀ഀഀ
   * - cookies() -> hash of all cookies, this is NOT a copy of the internal state, so do not modify਍   ⨀   椀琀ഀഀ
   * - cookies(name, value) -> set name to value, if value is undefined delete the cookie਍   ⨀ ⴀ 挀漀漀欀椀攀猀⠀渀愀洀攀⤀ ⴀ㸀 琀栀攀 猀愀洀攀 愀猀 ⠀渀愀洀攀Ⰰ 甀渀搀攀昀椀渀攀搀⤀ 㴀㴀 䐀䔀䰀䔀吀䔀匀 ⠀渀漀 漀渀攀 挀愀氀氀猀 椀琀 爀椀最栀琀 渀漀眀 琀栀愀琀ഀഀ
   *   way)਍   ⨀ഀഀ
   * @returns {Object} Hash of all cookies (if called without any parameter)਍   ⨀⼀ഀഀ
  self.cookies = function(name, value) {਍    ⼀⨀ 最氀漀戀愀氀 攀猀挀愀瀀攀㨀 昀愀氀猀攀Ⰰ 甀渀攀猀挀愀瀀攀㨀 昀愀氀猀攀 ⨀⼀ഀഀ
    var cookieLength, cookieArray, cookie, i, index;਍ഀഀ
    if (name) {਍      椀昀 ⠀瘀愀氀甀攀 㴀㴀㴀 甀渀搀攀昀椀渀攀搀⤀ 笀ഀഀ
        rawDocument.cookie = escape(name) + "=;path=" + cookiePath +਍                                ∀㬀攀砀瀀椀爀攀猀㴀吀栀甀Ⰰ 　㄀ 䨀愀渀 ㄀㤀㜀　 　　㨀　　㨀　　 䜀䴀吀∀㬀ഀഀ
      } else {਍        椀昀 ⠀椀猀匀琀爀椀渀最⠀瘀愀氀甀攀⤀⤀ 笀ഀഀ
          cookieLength = (rawDocument.cookie = escape(name) + '=' + escape(value) +਍                                ✀㬀瀀愀琀栀㴀✀ ⬀ 挀漀漀欀椀攀倀愀琀栀⤀⸀氀攀渀最琀栀 ⬀ ㄀㬀ഀഀ
਍          ⼀⼀ 瀀攀爀 栀琀琀瀀㨀⼀⼀眀眀眀⸀椀攀琀昀⸀漀爀最⼀爀昀挀⼀爀昀挀㈀㄀　㤀⸀琀砀琀 戀爀漀眀猀攀爀 洀甀猀琀 愀氀氀漀眀 愀琀 洀椀渀椀洀甀洀㨀ഀഀ
          // - 300 cookies਍          ⼀⼀ ⴀ ㈀　 挀漀漀欀椀攀猀 瀀攀爀 甀渀椀焀甀攀 搀漀洀愀椀渀ഀഀ
          // - 4096 bytes per cookie਍          椀昀 ⠀挀漀漀欀椀攀䰀攀渀最琀栀 㸀 㐀　㤀㘀⤀ 笀ഀഀ
            $log.warn("Cookie '"+ name +਍              ∀✀ 瀀漀猀猀椀戀氀礀 渀漀琀 猀攀琀 漀爀 漀瘀攀爀昀氀漀眀攀搀 戀攀挀愀甀猀攀 椀琀 眀愀猀 琀漀漀 氀愀爀最攀 ⠀∀⬀ഀഀ
              cookieLength + " > 4096 bytes)!");਍          紀ഀഀ
        }਍      紀ഀഀ
    } else {਍      椀昀 ⠀爀愀眀䐀漀挀甀洀攀渀琀⸀挀漀漀欀椀攀 ℀㴀㴀 氀愀猀琀䌀漀漀欀椀攀匀琀爀椀渀最⤀ 笀ഀഀ
        lastCookieString = rawDocument.cookie;਍        挀漀漀欀椀攀䄀爀爀愀礀 㴀 氀愀猀琀䌀漀漀欀椀攀匀琀爀椀渀最⸀猀瀀氀椀琀⠀∀㬀 ∀⤀㬀ഀഀ
        lastCookies = {};਍ഀഀ
        for (i = 0; i < cookieArray.length; i++) {਍          挀漀漀欀椀攀 㴀 挀漀漀欀椀攀䄀爀爀愀礀嬀椀崀㬀ഀഀ
          index = cookie.indexOf('=');਍          椀昀 ⠀椀渀搀攀砀 㸀 　⤀ 笀 ⼀⼀椀最渀漀爀攀 渀愀洀攀氀攀猀猀 挀漀漀欀椀攀猀ഀഀ
            name = unescape(cookie.substring(0, index));਍            ⼀⼀ 琀栀攀 昀椀爀猀琀 瘀愀氀甀攀 琀栀愀琀 椀猀 猀攀攀渀 昀漀爀 愀 挀漀漀欀椀攀 椀猀 琀栀攀 洀漀猀琀ഀഀ
            // specific one.  values for the same cookie name that਍            ⼀⼀ 昀漀氀氀漀眀 愀爀攀 昀漀爀 氀攀猀猀 猀瀀攀挀椀昀椀挀 瀀愀琀栀猀⸀ഀഀ
            if (lastCookies[name] === undefined) {਍              氀愀猀琀䌀漀漀欀椀攀猀嬀渀愀洀攀崀 㴀 甀渀攀猀挀愀瀀攀⠀挀漀漀欀椀攀⸀猀甀戀猀琀爀椀渀最⠀椀渀搀攀砀 ⬀ ㄀⤀⤀㬀ഀഀ
            }਍          紀ഀഀ
        }਍      紀ഀഀ
      return lastCookies;਍    紀ഀഀ
  };਍ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @name ng.$browser#defer਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀戀爀漀眀猀攀爀ഀഀ
   * @param {function()} fn A function, who's execution should be deferred.਍   ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀㴀紀 嬀搀攀氀愀礀㴀　崀 漀昀 洀椀氀氀椀猀攀挀漀渀搀猀 琀漀 搀攀昀攀爀 琀栀攀 昀甀渀挀琀椀漀渀 攀砀攀挀甀琀椀漀渀⸀ഀഀ
   * @returns {*} DeferId that can be used to cancel the task via `$browser.defer.cancel()`.਍   ⨀ഀഀ
   * @description਍   ⨀ 䔀砀攀挀甀琀攀猀 愀 昀渀 愀猀礀渀挀栀爀漀渀漀甀猀氀礀 瘀椀愀 怀猀攀琀吀椀洀攀漀甀琀⠀昀渀Ⰰ 搀攀氀愀礀⤀怀⸀ഀഀ
   *਍   ⨀ 唀渀氀椀欀攀 眀栀攀渀 挀愀氀氀椀渀最 怀猀攀琀吀椀洀攀漀甀琀怀 搀椀爀攀挀琀氀礀Ⰰ 椀渀 琀攀猀琀 琀栀椀猀 昀甀渀挀琀椀漀渀 椀猀 洀漀挀欀攀搀 愀渀搀 椀渀猀琀攀愀搀 漀昀 甀猀椀渀最ഀഀ
   * `setTimeout` in tests, the fns are queued in an array, which can be programmatically flushed਍   ⨀ 瘀椀愀 怀␀戀爀漀眀猀攀爀⸀搀攀昀攀爀⸀昀氀甀猀栀⠀⤀怀⸀ഀഀ
   *਍   ⨀⼀ഀഀ
  self.defer = function(fn, delay) {਍    瘀愀爀 琀椀洀攀漀甀琀䤀搀㬀ഀഀ
    outstandingRequestCount++;਍    琀椀洀攀漀甀琀䤀搀 㴀 猀攀琀吀椀洀攀漀甀琀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
      delete pendingDeferIds[timeoutId];਍      挀漀洀瀀氀攀琀攀伀甀琀猀琀愀渀搀椀渀最刀攀焀甀攀猀琀⠀昀渀⤀㬀ഀഀ
    }, delay || 0);਍    瀀攀渀搀椀渀最䐀攀昀攀爀䤀搀猀嬀琀椀洀攀漀甀琀䤀搀崀 㴀 琀爀甀攀㬀ഀഀ
    return timeoutId;਍  紀㬀ഀഀ
਍ഀഀ
  /**਍   ⨀ 䀀渀愀洀攀 渀最⸀␀戀爀漀眀猀攀爀⌀搀攀昀攀爀⸀挀愀渀挀攀氀ഀഀ
   * @methodOf ng.$browser.defer਍   ⨀ഀഀ
   * @description਍   ⨀ 䌀愀渀挀攀氀猀 愀 搀攀昀攀爀爀攀搀 琀愀猀欀 椀搀攀渀琀椀昀椀攀搀 眀椀琀栀 怀搀攀昀攀爀䤀搀怀⸀ഀഀ
   *਍   ⨀ 䀀瀀愀爀愀洀 笀⨀紀 搀攀昀攀爀䤀搀 吀漀欀攀渀 爀攀琀甀爀渀攀搀 戀礀 琀栀攀 怀␀戀爀漀眀猀攀爀⸀搀攀昀攀爀怀 昀甀渀挀琀椀漀渀⸀ഀഀ
   * @returns {boolean} Returns `true` if the task hasn't executed yet and was successfully਍   ⨀                    挀愀渀挀攀氀攀搀⸀ഀഀ
   */਍  猀攀氀昀⸀搀攀昀攀爀⸀挀愀渀挀攀氀 㴀 昀甀渀挀琀椀漀渀⠀搀攀昀攀爀䤀搀⤀ 笀ഀഀ
    if (pendingDeferIds[deferId]) {਍      搀攀氀攀琀攀 瀀攀渀搀椀渀最䐀攀昀攀爀䤀搀猀嬀搀攀昀攀爀䤀搀崀㬀ഀഀ
      clearTimeout(deferId);਍      挀漀洀瀀氀攀琀攀伀甀琀猀琀愀渀搀椀渀最刀攀焀甀攀猀琀⠀渀漀漀瀀⤀㬀ഀഀ
      return true;਍    紀ഀഀ
    return false;਍  紀㬀ഀഀ
਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 ␀䈀爀漀眀猀攀爀倀爀漀瘀椀搀攀爀⠀⤀笀ഀഀ
  this.$get = ['$window', '$log', '$sniffer', '$document',਍      昀甀渀挀琀椀漀渀⠀ ␀眀椀渀搀漀眀Ⰰ   ␀氀漀最Ⰰ   ␀猀渀椀昀昀攀爀Ⰰ   ␀搀漀挀甀洀攀渀琀⤀笀ഀഀ
        return new Browser($window, $document, $log, $sniffer);਍      紀崀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name ng.$cacheFactory਍ ⨀ഀഀ
 * @description਍ ⨀ 䘀愀挀琀漀爀礀 琀栀愀琀 挀漀渀猀琀爀甀挀琀猀 挀愀挀栀攀 漀戀樀攀挀琀猀 愀渀搀 最椀瘀攀猀 愀挀挀攀猀猀 琀漀 琀栀攀洀⸀ഀഀ
 * ਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 * ਍ ⨀  瘀愀爀 挀愀挀栀攀 㴀 ␀挀愀挀栀攀䘀愀挀琀漀爀礀⠀✀挀愀挀栀攀䤀搀✀⤀㬀ഀഀ
 *  expect($cacheFactory.get('cacheId')).toBe(cache);਍ ⨀  攀砀瀀攀挀琀⠀␀挀愀挀栀攀䘀愀挀琀漀爀礀⸀最攀琀⠀✀渀漀匀甀挀栀䌀愀挀栀攀䤀搀✀⤀⤀⸀渀漀琀⸀琀漀䈀攀䐀攀昀椀渀攀搀⠀⤀㬀ഀഀ
 *਍ ⨀  挀愀挀栀攀⸀瀀甀琀⠀∀欀攀礀∀Ⰰ ∀瘀愀氀甀攀∀⤀㬀ഀഀ
 *  cache.put("another key", "another value");਍ ⨀ഀഀ
 *  // We've specified no options on creation਍ ⨀  攀砀瀀攀挀琀⠀挀愀挀栀攀⸀椀渀昀漀⠀⤀⤀⸀琀漀䔀焀甀愀氀⠀笀椀搀㨀 ✀挀愀挀栀攀䤀搀✀Ⰰ 猀椀稀攀㨀 ㈀紀⤀㬀 ഀഀ
 * ਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ഀഀ
 * @param {string} cacheId Name or id of the newly created cache.਍ ⨀ 䀀瀀愀爀愀洀 笀漀戀樀攀挀琀㴀紀 漀瀀琀椀漀渀猀 伀瀀琀椀漀渀猀 漀戀樀攀挀琀 琀栀愀琀 猀瀀攀挀椀昀椀攀猀 琀栀攀 挀愀挀栀攀 戀攀栀愀瘀椀漀爀⸀ 倀爀漀瀀攀爀琀椀攀猀㨀ഀഀ
 *਍ ⨀   ⴀ 怀笀渀甀洀戀攀爀㴀紀怀 怀挀愀瀀愀挀椀琀礀怀 ﴀ﷿⃿琀甀爀渀猀 琀栀攀 挀愀挀栀攀 椀渀琀漀 䰀刀唀 挀愀挀栀攀⸀ഀഀ
 *਍ ⨀ 䀀爀攀琀甀爀渀猀 笀漀戀樀攀挀琀紀 一攀眀氀礀 挀爀攀愀琀攀搀 挀愀挀栀攀 漀戀樀攀挀琀 眀椀琀栀 琀栀攀 昀漀氀氀漀眀椀渀最 猀攀琀 漀昀 洀攀琀栀漀搀猀㨀ഀഀ
 *਍ ⨀ ⴀ 怀笀漀戀樀攀挀琀紀怀 怀椀渀昀漀⠀⤀怀 ﴀ﷿⃿刀攀琀甀爀渀猀 椀搀Ⰰ 猀椀稀攀Ⰰ 愀渀搀 漀瀀琀椀漀渀猀 漀昀 挀愀挀栀攀⸀ഀഀ
 * - `{{*}}` `put({string} key, {*} value)` �� Puts a new key-value pair into the cache and returns਍ ⨀   椀琀⸀ഀഀ
 * - `{{*}}` `get({string} key)` �� Returns cached value for `key` or undefined for cache miss.਍ ⨀ ⴀ 怀笀瘀漀椀搀紀怀 怀爀攀洀漀瘀攀⠀笀猀琀爀椀渀最紀 欀攀礀⤀怀 ﴀ﷿⃿刀攀洀漀瘀攀猀 愀 欀攀礀ⴀ瘀愀氀甀攀 瀀愀椀爀 昀爀漀洀 琀栀攀 挀愀挀栀攀⸀ഀഀ
 * - `{void}` `removeAll()` �� Removes all cached values.਍ ⨀ ⴀ 怀笀瘀漀椀搀紀怀 怀搀攀猀琀爀漀礀⠀⤀怀 ﴀ﷿⃿刀攀洀漀瘀攀猀 爀攀昀攀爀攀渀挀攀猀 琀漀 琀栀椀猀 挀愀挀栀攀 昀爀漀洀 ␀挀愀挀栀攀䘀愀挀琀漀爀礀⸀ഀഀ
 *਍ ⨀⼀ഀഀ
function $CacheFactoryProvider() {਍ഀഀ
  this.$get = function() {਍    瘀愀爀 挀愀挀栀攀猀 㴀 笀紀㬀ഀഀ
਍    昀甀渀挀琀椀漀渀 挀愀挀栀攀䘀愀挀琀漀爀礀⠀挀愀挀栀攀䤀搀Ⰰ 漀瀀琀椀漀渀猀⤀ 笀ഀഀ
      if (cacheId in caches) {਍        琀栀爀漀眀 洀椀渀䔀爀爀⠀✀␀挀愀挀栀攀䘀愀挀琀漀爀礀✀⤀⠀✀椀椀搀✀Ⰰ ∀䌀愀挀栀攀䤀搀 ✀笀　紀✀ 椀猀 愀氀爀攀愀搀礀 琀愀欀攀渀℀∀Ⰰ 挀愀挀栀攀䤀搀⤀㬀ഀഀ
      }਍ഀഀ
      var size = 0,਍          猀琀愀琀猀 㴀 攀砀琀攀渀搀⠀笀紀Ⰰ 漀瀀琀椀漀渀猀Ⰰ 笀椀搀㨀 挀愀挀栀攀䤀搀紀⤀Ⰰഀഀ
          data = {},਍          挀愀瀀愀挀椀琀礀 㴀 ⠀漀瀀琀椀漀渀猀 ☀☀ 漀瀀琀椀漀渀猀⸀挀愀瀀愀挀椀琀礀⤀ 簀簀 一甀洀戀攀爀⸀䴀䄀堀开嘀䄀䰀唀䔀Ⰰഀഀ
          lruHash = {},਍          昀爀攀猀栀䔀渀搀 㴀 渀甀氀氀Ⰰഀഀ
          staleEnd = null;਍ഀഀ
      return caches[cacheId] = {਍ഀഀ
        put: function(key, value) {਍          瘀愀爀 氀爀甀䔀渀琀爀礀 㴀 氀爀甀䠀愀猀栀嬀欀攀礀崀 簀簀 ⠀氀爀甀䠀愀猀栀嬀欀攀礀崀 㴀 笀欀攀礀㨀 欀攀礀紀⤀㬀ഀഀ
਍          爀攀昀爀攀猀栀⠀氀爀甀䔀渀琀爀礀⤀㬀ഀഀ
਍          椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀瘀愀氀甀攀⤀⤀ 爀攀琀甀爀渀㬀ഀഀ
          if (!(key in data)) size++;਍          搀愀琀愀嬀欀攀礀崀 㴀 瘀愀氀甀攀㬀ഀഀ
਍          椀昀 ⠀猀椀稀攀 㸀 挀愀瀀愀挀椀琀礀⤀ 笀ഀഀ
            this.remove(staleEnd.key);਍          紀ഀഀ
਍          爀攀琀甀爀渀 瘀愀氀甀攀㬀ഀഀ
        },਍ഀഀ
਍        最攀琀㨀 昀甀渀挀琀椀漀渀⠀欀攀礀⤀ 笀ഀഀ
          var lruEntry = lruHash[key];਍ഀഀ
          if (!lruEntry) return;਍ഀഀ
          refresh(lruEntry);਍ഀഀ
          return data[key];਍        紀Ⰰഀഀ
਍ഀഀ
        remove: function(key) {਍          瘀愀爀 氀爀甀䔀渀琀爀礀 㴀 氀爀甀䠀愀猀栀嬀欀攀礀崀㬀ഀഀ
਍          椀昀 ⠀℀氀爀甀䔀渀琀爀礀⤀ 爀攀琀甀爀渀㬀ഀഀ
਍          椀昀 ⠀氀爀甀䔀渀琀爀礀 㴀㴀 昀爀攀猀栀䔀渀搀⤀ 昀爀攀猀栀䔀渀搀 㴀 氀爀甀䔀渀琀爀礀⸀瀀㬀ഀഀ
          if (lruEntry == staleEnd) staleEnd = lruEntry.n;਍          氀椀渀欀⠀氀爀甀䔀渀琀爀礀⸀渀Ⰰ氀爀甀䔀渀琀爀礀⸀瀀⤀㬀ഀഀ
਍          搀攀氀攀琀攀 氀爀甀䠀愀猀栀嬀欀攀礀崀㬀ഀഀ
          delete data[key];਍          猀椀稀攀ⴀⴀ㬀ഀഀ
        },਍ഀഀ
਍        爀攀洀漀瘀攀䄀氀氀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          data = {};਍          猀椀稀攀 㴀 　㬀ഀഀ
          lruHash = {};਍          昀爀攀猀栀䔀渀搀 㴀 猀琀愀氀攀䔀渀搀 㴀 渀甀氀氀㬀ഀഀ
        },਍ഀഀ
਍        搀攀猀琀爀漀礀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          data = null;਍          猀琀愀琀猀 㴀 渀甀氀氀㬀ഀഀ
          lruHash = null;਍          搀攀氀攀琀攀 挀愀挀栀攀猀嬀挀愀挀栀攀䤀搀崀㬀ഀഀ
        },਍ഀഀ
਍        椀渀昀漀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          return extend({}, stats, {size: size});਍        紀ഀഀ
      };਍ഀഀ
਍      ⼀⨀⨀ഀഀ
       * makes the `entry` the freshEnd of the LRU linked list਍       ⨀⼀ഀഀ
      function refresh(entry) {਍        椀昀 ⠀攀渀琀爀礀 ℀㴀 昀爀攀猀栀䔀渀搀⤀ 笀ഀഀ
          if (!staleEnd) {਍            猀琀愀氀攀䔀渀搀 㴀 攀渀琀爀礀㬀ഀഀ
          } else if (staleEnd == entry) {਍            猀琀愀氀攀䔀渀搀 㴀 攀渀琀爀礀⸀渀㬀ഀഀ
          }਍ഀഀ
          link(entry.n, entry.p);਍          氀椀渀欀⠀攀渀琀爀礀Ⰰ 昀爀攀猀栀䔀渀搀⤀㬀ഀഀ
          freshEnd = entry;਍          昀爀攀猀栀䔀渀搀⸀渀 㴀 渀甀氀氀㬀ഀഀ
        }਍      紀ഀഀ
਍ഀഀ
      /**਍       ⨀ 戀椀搀椀爀攀挀琀椀漀渀愀氀氀礀 氀椀渀欀猀 琀眀漀 攀渀琀爀椀攀猀 漀昀 琀栀攀 䰀刀唀 氀椀渀欀攀搀 氀椀猀琀ഀഀ
       */਍      昀甀渀挀琀椀漀渀 氀椀渀欀⠀渀攀砀琀䔀渀琀爀礀Ⰰ 瀀爀攀瘀䔀渀琀爀礀⤀ 笀ഀഀ
        if (nextEntry != prevEntry) {਍          椀昀 ⠀渀攀砀琀䔀渀琀爀礀⤀ 渀攀砀琀䔀渀琀爀礀⸀瀀 㴀 瀀爀攀瘀䔀渀琀爀礀㬀 ⼀⼀瀀 猀琀愀渀搀猀 昀漀爀 瀀爀攀瘀椀漀甀猀Ⰰ ✀瀀爀攀瘀✀ 搀椀搀渀✀琀 洀椀渀椀昀礀ഀഀ
          if (prevEntry) prevEntry.n = nextEntry; //n stands for next, 'next' didn't minify਍        紀ഀഀ
      }਍    紀ഀഀ
਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
   * @name ng.$cacheFactory#info਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀挀愀挀栀攀䘀愀挀琀漀爀礀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Get information about all the of the caches that have been created਍   ⨀ഀഀ
   * @returns {Object} - key-value map of `cacheId` to the result of calling `cache#info`਍   ⨀⼀ഀഀ
    cacheFactory.info = function() {਍      瘀愀爀 椀渀昀漀 㴀 笀紀㬀ഀഀ
      forEach(caches, function(cache, cacheId) {਍        椀渀昀漀嬀挀愀挀栀攀䤀搀崀 㴀 挀愀挀栀攀⸀椀渀昀漀⠀⤀㬀ഀഀ
      });਍      爀攀琀甀爀渀 椀渀昀漀㬀ഀഀ
    };਍ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc method਍   ⨀ 䀀渀愀洀攀 渀最⸀␀挀愀挀栀攀䘀愀挀琀漀爀礀⌀最攀琀ഀഀ
   * @methodOf ng.$cacheFactory਍   ⨀ഀഀ
   * @description਍   ⨀ 䜀攀琀 愀挀挀攀猀猀 琀漀 愀 挀愀挀栀攀 漀戀樀攀挀琀 戀礀 琀栀攀 怀挀愀挀栀攀䤀搀怀 甀猀攀搀 眀栀攀渀 椀琀 眀愀猀 挀爀攀愀琀攀搀⸀ഀഀ
   *਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 挀愀挀栀攀䤀搀 一愀洀攀 漀爀 椀搀 漀昀 愀 挀愀挀栀攀 琀漀 愀挀挀攀猀猀⸀ഀഀ
   * @returns {object} Cache object identified by the cacheId or undefined if no such cache.਍   ⨀⼀ഀഀ
    cacheFactory.get = function(cacheId) {਍      爀攀琀甀爀渀 挀愀挀栀攀猀嬀挀愀挀栀攀䤀搀崀㬀ഀഀ
    };਍ഀഀ
਍    爀攀琀甀爀渀 挀愀挀栀攀䘀愀挀琀漀爀礀㬀ഀഀ
  };਍紀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc object਍ ⨀ 䀀渀愀洀攀 渀最⸀␀琀攀洀瀀氀愀琀攀䌀愀挀栀攀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * The first time a template is used, it is loaded in the template cache for quick retrieval. You਍ ⨀ 挀愀渀 氀漀愀搀 琀攀洀瀀氀愀琀攀猀 搀椀爀攀挀琀氀礀 椀渀琀漀 琀栀攀 挀愀挀栀攀 椀渀 愀 怀猀挀爀椀瀀琀怀 琀愀最Ⰰ 漀爀 戀礀 挀漀渀猀甀洀椀渀最 琀栀攀ഀഀ
 * `$templateCache` service directly.਍ ⨀ ഀഀ
 * Adding via the `script` tag:਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 * <html ng-app>਍ ⨀ 㰀栀攀愀搀㸀ഀഀ
 * <script type="text/ng-template" id="templateId.html">਍ ⨀   吀栀椀猀 椀猀 琀栀攀 挀漀渀琀攀渀琀 漀昀 琀栀攀 琀攀洀瀀氀愀琀攀ഀഀ
 * </script>਍ ⨀ 㰀⼀栀攀愀搀㸀ഀഀ
 *   ...਍ ⨀ 㰀⼀栀琀洀氀㸀ഀഀ
 * </pre>਍ ⨀ ഀഀ
 * **Note:** the `script` tag containing the template does not need to be included in the `head` of਍ ⨀ 琀栀攀 搀漀挀甀洀攀渀琀Ⰰ 戀甀琀 椀琀 洀甀猀琀 戀攀 戀攀氀漀眀 琀栀攀 怀渀最ⴀ愀瀀瀀怀 搀攀昀椀渀椀琀椀漀渀⸀ഀഀ
 * ਍ ⨀ 䄀搀搀椀渀最 瘀椀愀 琀栀攀 ␀琀攀洀瀀氀愀琀攀䌀愀挀栀攀 猀攀爀瘀椀挀攀㨀ഀഀ
 * ਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 * var myApp = angular.module('myApp', []);਍ ⨀ 洀礀䄀瀀瀀⸀爀甀渀⠀昀甀渀挀琀椀漀渀⠀␀琀攀洀瀀氀愀琀攀䌀愀挀栀攀⤀ 笀ഀഀ
 *   $templateCache.put('templateId.html', 'This is the content of the template');਍ ⨀ 紀⤀㬀ഀഀ
 * </pre>਍ ⨀ ഀഀ
 * To retrieve the template later, simply use it in your HTML:਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 * <div ng-include=" 'templateId.html' "></div>਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 * ਍ ⨀ 漀爀 最攀琀 椀琀 瘀椀愀 䨀愀瘀愀猀挀爀椀瀀琀㨀ഀഀ
 * <pre>਍ ⨀ ␀琀攀洀瀀氀愀琀攀䌀愀挀栀攀⸀最攀琀⠀✀琀攀洀瀀氀愀琀攀䤀搀⸀栀琀洀氀✀⤀ഀഀ
 * </pre>਍ ⨀ ഀഀ
 * See {@link ng.$cacheFactory $cacheFactory}.਍ ⨀ഀഀ
 */਍昀甀渀挀琀椀漀渀 ␀吀攀洀瀀氀愀琀攀䌀愀挀栀攀倀爀漀瘀椀搀攀爀⠀⤀ 笀ഀഀ
  this.$get = ['$cacheFactory', function($cacheFactory) {਍    爀攀琀甀爀渀 ␀挀愀挀栀攀䘀愀挀琀漀爀礀⠀✀琀攀洀瀀氀愀琀攀猀✀⤀㬀ഀഀ
  }];਍紀ഀഀ
਍⼀⨀ ℀ 嘀䄀刀䤀䄀䈀䰀䔀⼀䘀唀一䌀吀䤀伀一 一䄀䴀䤀一䜀 䌀伀一嘀䔀一吀䤀伀一匀 吀䠀䄀吀 䄀倀倀䰀夀 吀伀 吀䠀䤀匀 䘀䤀䰀䔀℀ഀഀ
 *਍ ⨀ 䐀伀䴀ⴀ爀攀氀愀琀攀搀 瘀愀爀椀愀戀氀攀猀㨀ഀഀ
 *਍ ⨀ ⴀ ∀渀漀搀攀∀ ⴀ 䐀伀䴀 一漀搀攀ഀഀ
 * - "element" - DOM Element or Node਍ ⨀ ⴀ ∀␀渀漀搀攀∀ 漀爀 ∀␀攀氀攀洀攀渀琀∀ ⴀ 樀焀䰀椀琀攀ⴀ眀爀愀瀀瀀攀搀 渀漀搀攀 漀爀 攀氀攀洀攀渀琀ഀഀ
 *਍ ⨀ഀഀ
 * Compiler related stuff:਍ ⨀ഀഀ
 * - "linkFn" - linking fn of a single directive਍ ⨀ ⴀ ∀渀漀搀攀䰀椀渀欀䘀渀∀ ⴀ 昀甀渀挀琀椀漀渀 琀栀愀琀 愀最最爀攀最愀琀攀猀 愀氀氀 氀椀渀欀椀渀最 昀渀猀 昀漀爀 愀 瀀愀爀琀椀挀甀氀愀爀 渀漀搀攀ഀഀ
 * - "childLinkFn" -  function that aggregates all linking fns for child nodes of a particular node਍ ⨀ ⴀ ∀挀漀洀瀀漀猀椀琀攀䰀椀渀欀䘀渀∀ ⴀ 昀甀渀挀琀椀漀渀 琀栀愀琀 愀最最爀攀最愀琀攀猀 愀氀氀 氀椀渀欀椀渀最 昀渀猀 昀漀爀 愀 挀漀洀瀀椀氀愀琀椀漀渀 爀漀漀琀 ⠀渀漀搀攀䰀椀猀琀⤀ഀഀ
 */਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 渀最⸀␀挀漀洀瀀椀氀攀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ 䌀漀洀瀀椀氀攀猀 愀渀 䠀吀䴀䰀 猀琀爀椀渀最 漀爀 䐀伀䴀 椀渀琀漀 愀 琀攀洀瀀氀愀琀攀 愀渀搀 瀀爀漀搀甀挀攀猀 愀 琀攀洀瀀氀愀琀攀 昀甀渀挀琀椀漀渀Ⰰ 眀栀椀挀栀ഀഀ
 * can then be used to link {@link ng.$rootScope.Scope `scope`} and the template together.਍ ⨀ഀഀ
 * The compilation is a process of walking the DOM tree and matching DOM elements to਍ ⨀ 笀䀀氀椀渀欀 渀最⸀␀挀漀洀瀀椀氀攀倀爀漀瘀椀搀攀爀⌀洀攀琀栀漀搀猀开搀椀爀攀挀琀椀瘀攀 搀椀爀攀挀琀椀瘀攀猀紀⸀ഀഀ
 *਍ ⨀ 㰀搀椀瘀 挀氀愀猀猀㴀∀愀氀攀爀琀 愀氀攀爀琀ⴀ眀愀爀渀椀渀最∀㸀ഀഀ
 * **Note:** This document is an in-depth reference of all directive options.਍ ⨀ 䘀漀爀 愀 最攀渀琀氀攀 椀渀琀爀漀搀甀挀琀椀漀渀 琀漀 搀椀爀攀挀琀椀瘀攀猀 眀椀琀栀 攀砀愀洀瀀氀攀猀 漀昀 挀漀洀洀漀渀 甀猀攀 挀愀猀攀猀Ⰰഀഀ
 * see the {@link guide/directive directive guide}.਍ ⨀ 㰀⼀搀椀瘀㸀ഀഀ
 *਍ ⨀ ⌀⌀ 䌀漀洀瀀爀攀栀攀渀猀椀瘀攀 䐀椀爀攀挀琀椀瘀攀 䄀倀䤀ഀഀ
 *਍ ⨀ 吀栀攀爀攀 愀爀攀 洀愀渀礀 搀椀昀昀攀爀攀渀琀 漀瀀琀椀漀渀猀 昀漀爀 愀 搀椀爀攀挀琀椀瘀攀⸀ഀഀ
 *਍ ⨀ 吀栀攀 搀椀昀昀攀爀攀渀挀攀 爀攀猀椀搀攀猀 椀渀 琀栀攀 爀攀琀甀爀渀 瘀愀氀甀攀 漀昀 琀栀攀 昀愀挀琀漀爀礀 昀甀渀挀琀椀漀渀⸀ഀഀ
 * You can either return a "Directive Definition Object" (see below) that defines the directive properties,਍ ⨀ 漀爀 樀甀猀琀 琀栀攀 怀瀀漀猀琀䰀椀渀欀怀 昀甀渀挀琀椀漀渀 ⠀愀氀氀 漀琀栀攀爀 瀀爀漀瀀攀爀琀椀攀猀 眀椀氀氀 栀愀瘀攀 琀栀攀 搀攀昀愀甀氀琀 瘀愀氀甀攀猀⤀⸀ഀഀ
 *਍ ⨀ 㰀搀椀瘀 挀氀愀猀猀㴀∀愀氀攀爀琀 愀氀攀爀琀ⴀ猀甀挀挀攀猀猀∀㸀ഀഀ
 * **Best Practice:** It's recommended to use the "directive definition object" form.਍ ⨀ 㰀⼀搀椀瘀㸀ഀഀ
 *਍ ⨀ 䠀攀爀攀✀猀 愀渀 攀砀愀洀瀀氀攀 搀椀爀攀挀琀椀瘀攀 搀攀挀氀愀爀攀搀 眀椀琀栀 愀 䐀椀爀攀挀琀椀瘀攀 䐀攀昀椀渀椀琀椀漀渀 伀戀樀攀挀琀㨀ഀഀ
 *਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   var myModule = angular.module(...);਍ ⨀ഀഀ
 *   myModule.directive('directiveName', function factory(injectables) {਍ ⨀     瘀愀爀 搀椀爀攀挀琀椀瘀攀䐀攀昀椀渀椀琀椀漀渀伀戀樀攀挀琀 㴀 笀ഀഀ
 *       priority: 0,਍ ⨀       琀攀洀瀀氀愀琀攀㨀 ✀㰀搀椀瘀㸀㰀⼀搀椀瘀㸀✀Ⰰ ⼀⼀ 漀爀 ⼀⼀ 昀甀渀挀琀椀漀渀⠀琀䔀氀攀洀攀渀琀Ⰰ 琀䄀琀琀爀猀⤀ 笀 ⸀⸀⸀ 紀Ⰰഀഀ
 *       // or਍ ⨀       ⼀⼀ 琀攀洀瀀氀愀琀攀唀爀氀㨀 ✀搀椀爀攀挀琀椀瘀攀⸀栀琀洀氀✀Ⰰ ⼀⼀ 漀爀 ⼀⼀ 昀甀渀挀琀椀漀渀⠀琀䔀氀攀洀攀渀琀Ⰰ 琀䄀琀琀爀猀⤀ 笀 ⸀⸀⸀ 紀Ⰰഀഀ
 *       replace: false,਍ ⨀       琀爀愀渀猀挀氀甀搀攀㨀 昀愀氀猀攀Ⰰഀഀ
 *       restrict: 'A',਍ ⨀       猀挀漀瀀攀㨀 昀愀氀猀攀Ⰰഀഀ
 *       controller: function($scope, $element, $attrs, $transclude, otherInjectables) { ... },਍ ⨀       爀攀焀甀椀爀攀㨀 ✀猀椀戀氀椀渀最䐀椀爀攀挀琀椀瘀攀一愀洀攀✀Ⰰ ⼀⼀ 漀爀 ⼀⼀ 嬀✀帀瀀愀爀攀渀琀䐀椀爀攀挀琀椀瘀攀一愀洀攀✀Ⰰ ✀㼀漀瀀琀椀漀渀愀氀䐀椀爀攀挀琀椀瘀攀一愀洀攀✀Ⰰ ✀㼀帀漀瀀琀椀漀渀愀氀倀愀爀攀渀琀✀崀Ⰰഀഀ
 *       compile: function compile(tElement, tAttrs, transclude) {਍ ⨀         爀攀琀甀爀渀 笀ഀഀ
 *           pre: function preLink(scope, iElement, iAttrs, controller) { ... },਍ ⨀           瀀漀猀琀㨀 昀甀渀挀琀椀漀渀 瀀漀猀琀䰀椀渀欀⠀猀挀漀瀀攀Ⰰ 椀䔀氀攀洀攀渀琀Ⰰ 椀䄀琀琀爀猀Ⰰ 挀漀渀琀爀漀氀氀攀爀⤀ 笀 ⸀⸀⸀ 紀ഀഀ
 *         }਍ ⨀         ⼀⼀ 漀爀ഀഀ
 *         // return function postLink( ... ) { ... }਍ ⨀       紀Ⰰഀഀ
 *       // or਍ ⨀       ⼀⼀ 氀椀渀欀㨀 笀ഀഀ
 *       //  pre: function preLink(scope, iElement, iAttrs, controller) { ... },਍ ⨀       ⼀⼀  瀀漀猀琀㨀 昀甀渀挀琀椀漀渀 瀀漀猀琀䰀椀渀欀⠀猀挀漀瀀攀Ⰰ 椀䔀氀攀洀攀渀琀Ⰰ 椀䄀琀琀爀猀Ⰰ 挀漀渀琀爀漀氀氀攀爀⤀ 笀 ⸀⸀⸀ 紀ഀഀ
 *       // }਍ ⨀       ⼀⼀ 漀爀ഀഀ
 *       // link: function postLink( ... ) { ... }਍ ⨀     紀㬀ഀഀ
 *     return directiveDefinitionObject;਍ ⨀   紀⤀㬀ഀഀ
 * </pre>਍ ⨀ഀഀ
 * <div class="alert alert-warning">਍ ⨀ ⨀⨀一漀琀攀㨀⨀⨀ 䄀渀礀 甀渀猀瀀攀挀椀昀椀攀搀 漀瀀琀椀漀渀猀 眀椀氀氀 甀猀攀 琀栀攀 搀攀昀愀甀氀琀 瘀愀氀甀攀⸀ 夀漀甀 挀愀渀 猀攀攀 琀栀攀 搀攀昀愀甀氀琀 瘀愀氀甀攀猀 戀攀氀漀眀⸀ഀഀ
 * </div>਍ ⨀ഀഀ
 * Therefore the above can be simplified as:਍ ⨀ഀഀ
 * <pre>਍ ⨀   瘀愀爀 洀礀䴀漀搀甀氀攀 㴀 愀渀最甀氀愀爀⸀洀漀搀甀氀攀⠀⸀⸀⸀⤀㬀ഀഀ
 *਍ ⨀   洀礀䴀漀搀甀氀攀⸀搀椀爀攀挀琀椀瘀攀⠀✀搀椀爀攀挀琀椀瘀攀一愀洀攀✀Ⰰ 昀甀渀挀琀椀漀渀 昀愀挀琀漀爀礀⠀椀渀樀攀挀琀愀戀氀攀猀⤀ 笀ഀഀ
 *     var directiveDefinitionObject = {਍ ⨀       氀椀渀欀㨀 昀甀渀挀琀椀漀渀 瀀漀猀琀䰀椀渀欀⠀猀挀漀瀀攀Ⰰ 椀䔀氀攀洀攀渀琀Ⰰ 椀䄀琀琀爀猀⤀ 笀 ⸀⸀⸀ 紀ഀഀ
 *     };਍ ⨀     爀攀琀甀爀渀 搀椀爀攀挀琀椀瘀攀䐀攀昀椀渀椀琀椀漀渀伀戀樀攀挀琀㬀ഀഀ
 *     // or਍ ⨀     ⼀⼀ 爀攀琀甀爀渀 昀甀渀挀琀椀漀渀 瀀漀猀琀䰀椀渀欀⠀猀挀漀瀀攀Ⰰ 椀䔀氀攀洀攀渀琀Ⰰ 椀䄀琀琀爀猀⤀ 笀 ⸀⸀⸀ 紀ഀഀ
 *   });਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ഀഀ
 *਍ ⨀ ⌀⌀⌀ 䐀椀爀攀挀琀椀瘀攀 䐀攀昀椀渀椀琀椀漀渀 伀戀樀攀挀琀ഀഀ
 *਍ ⨀ 吀栀攀 搀椀爀攀挀琀椀瘀攀 搀攀昀椀渀椀琀椀漀渀 漀戀樀攀挀琀 瀀爀漀瘀椀搀攀猀 椀渀猀琀爀甀挀琀椀漀渀猀 琀漀 琀栀攀 笀䀀氀椀渀欀 愀瀀椀⼀渀最⸀␀挀漀洀瀀椀氀攀ഀഀ
 * compiler}. The attributes are:਍ ⨀ഀഀ
 * #### `priority`਍ ⨀ 圀栀攀渀 琀栀攀爀攀 愀爀攀 洀甀氀琀椀瀀氀攀 搀椀爀攀挀琀椀瘀攀猀 搀攀昀椀渀攀搀 漀渀 愀 猀椀渀最氀攀 䐀伀䴀 攀氀攀洀攀渀琀Ⰰ 猀漀洀攀琀椀洀攀猀 椀琀ഀഀ
 * is necessary to specify the order in which the directives are applied. The `priority` is used਍ ⨀ 琀漀 猀漀爀琀 琀栀攀 搀椀爀攀挀琀椀瘀攀猀 戀攀昀漀爀攀 琀栀攀椀爀 怀挀漀洀瀀椀氀攀怀 昀甀渀挀琀椀漀渀猀 最攀琀 挀愀氀氀攀搀⸀ 倀爀椀漀爀椀琀礀 椀猀 搀攀昀椀渀攀搀 愀猀 愀ഀഀ
 * number. Directives with greater numerical `priority` are compiled first. Pre-link functions਍ ⨀ 愀爀攀 愀氀猀漀 爀甀渀 椀渀 瀀爀椀漀爀椀琀礀 漀爀搀攀爀Ⰰ 戀甀琀 瀀漀猀琀ⴀ氀椀渀欀 昀甀渀挀琀椀漀渀猀 愀爀攀 爀甀渀 椀渀 爀攀瘀攀爀猀攀 漀爀搀攀爀⸀ 吀栀攀 漀爀搀攀爀ഀഀ
 * of directives with the same priority is undefined. The default priority is `0`.਍ ⨀ഀഀ
 * #### `terminal`਍ ⨀ 䤀昀 猀攀琀 琀漀 琀爀甀攀 琀栀攀渀 琀栀攀 挀甀爀爀攀渀琀 怀瀀爀椀漀爀椀琀礀怀 眀椀氀氀 戀攀 琀栀攀 氀愀猀琀 猀攀琀 漀昀 搀椀爀攀挀琀椀瘀攀猀ഀഀ
 * which will execute (any directives at the current priority will still execute਍ ⨀ 愀猀 琀栀攀 漀爀搀攀爀 漀昀 攀砀攀挀甀琀椀漀渀 漀渀 猀愀洀攀 怀瀀爀椀漀爀椀琀礀怀 椀猀 甀渀搀攀昀椀渀攀搀⤀⸀ഀഀ
 *਍ ⨀ ⌀⌀⌀⌀ 怀猀挀漀瀀攀怀ഀഀ
 * **If set to `true`,** then a new scope will be created for this directive. If multiple directives on the਍ ⨀ 猀愀洀攀 攀氀攀洀攀渀琀 爀攀焀甀攀猀琀 愀 渀攀眀 猀挀漀瀀攀Ⰰ 漀渀氀礀 漀渀攀 渀攀眀 猀挀漀瀀攀 椀猀 挀爀攀愀琀攀搀⸀ 吀栀攀 渀攀眀 猀挀漀瀀攀 爀甀氀攀 搀漀攀猀 渀漀琀ഀഀ
 * apply for the root of the template since the root of the template always gets a new scope.਍ ⨀ഀഀ
 * **If set to `{}` (object hash),** then a new "isolate" scope is created. The 'isolate' scope differs from਍ ⨀ 渀漀爀洀愀氀 猀挀漀瀀攀 椀渀 琀栀愀琀 椀琀 搀漀攀猀 渀漀琀 瀀爀漀琀漀琀礀瀀椀挀愀氀氀礀 椀渀栀攀爀椀琀 昀爀漀洀 琀栀攀 瀀愀爀攀渀琀 猀挀漀瀀攀⸀ 吀栀椀猀 椀猀 甀猀攀昀甀氀ഀഀ
 * when creating reusable components, which should not accidentally read or modify data in the਍ ⨀ 瀀愀爀攀渀琀 猀挀漀瀀攀⸀ഀഀ
 *਍ ⨀ 吀栀攀 ✀椀猀漀氀愀琀攀✀ 猀挀漀瀀攀 琀愀欀攀猀 愀渀 漀戀樀攀挀琀 栀愀猀栀 眀栀椀挀栀 搀攀昀椀渀攀猀 愀 猀攀琀 漀昀 氀漀挀愀氀 猀挀漀瀀攀 瀀爀漀瀀攀爀琀椀攀猀ഀഀ
 * derived from the parent scope. These local properties are useful for aliasing values for਍ ⨀ 琀攀洀瀀氀愀琀攀猀⸀ 䰀漀挀愀氀猀 搀攀昀椀渀椀琀椀漀渀 椀猀 愀 栀愀猀栀 漀昀 氀漀挀愀氀 猀挀漀瀀攀 瀀爀漀瀀攀爀琀礀 琀漀 椀琀猀 猀漀甀爀挀攀㨀ഀഀ
 *਍ ⨀ ⨀ 怀䀀怀 漀爀 怀䀀愀琀琀爀怀 ⴀ 戀椀渀搀 愀 氀漀挀愀氀 猀挀漀瀀攀 瀀爀漀瀀攀爀琀礀 琀漀 琀栀攀 瘀愀氀甀攀 漀昀 䐀伀䴀 愀琀琀爀椀戀甀琀攀⸀ 吀栀攀 爀攀猀甀氀琀 椀猀ഀഀ
 *   always a string since DOM attributes are strings. If no `attr` name is specified  then the਍ ⨀   愀琀琀爀椀戀甀琀攀 渀愀洀攀 椀猀 愀猀猀甀洀攀搀 琀漀 戀攀 琀栀攀 猀愀洀攀 愀猀 琀栀攀 氀漀挀愀氀 渀愀洀攀⸀ഀഀ
 *   Given `<widget my-attr="hello {{name}}">` and widget definition਍ ⨀   漀昀 怀猀挀漀瀀攀㨀 笀 氀漀挀愀氀一愀洀攀㨀✀䀀洀礀䄀琀琀爀✀ 紀怀Ⰰ 琀栀攀渀 眀椀搀最攀琀 猀挀漀瀀攀 瀀爀漀瀀攀爀琀礀 怀氀漀挀愀氀一愀洀攀怀 眀椀氀氀 爀攀昀氀攀挀琀ഀഀ
 *   the interpolated value of `hello {{name}}`. As the `name` attribute changes so will the਍ ⨀   怀氀漀挀愀氀一愀洀攀怀 瀀爀漀瀀攀爀琀礀 漀渀 琀栀攀 眀椀搀最攀琀 猀挀漀瀀攀⸀ 吀栀攀 怀渀愀洀攀怀 椀猀 爀攀愀搀 昀爀漀洀 琀栀攀 瀀愀爀攀渀琀 猀挀漀瀀攀 ⠀渀漀琀ഀഀ
 *   component scope).਍ ⨀ഀഀ
 * * `=` or `=attr` - set up bi-directional binding between a local scope property and the਍ ⨀   瀀愀爀攀渀琀 猀挀漀瀀攀 瀀爀漀瀀攀爀琀礀 漀昀 渀愀洀攀 搀攀昀椀渀攀搀 瘀椀愀 琀栀攀 瘀愀氀甀攀 漀昀 琀栀攀 怀愀琀琀爀怀 愀琀琀爀椀戀甀琀攀⸀ 䤀昀 渀漀 怀愀琀琀爀怀ഀഀ
 *   name is specified then the attribute name is assumed to be the same as the local name.਍ ⨀   䜀椀瘀攀渀 怀㰀眀椀搀最攀琀 洀礀ⴀ愀琀琀爀㴀∀瀀愀爀攀渀琀䴀漀搀攀氀∀㸀怀 愀渀搀 眀椀搀最攀琀 搀攀昀椀渀椀琀椀漀渀 漀昀ഀഀ
 *   `scope: { localModel:'=myAttr' }`, then widget scope property `localModel` will reflect the਍ ⨀   瘀愀氀甀攀 漀昀 怀瀀愀爀攀渀琀䴀漀搀攀氀怀 漀渀 琀栀攀 瀀愀爀攀渀琀 猀挀漀瀀攀⸀ 䄀渀礀 挀栀愀渀最攀猀 琀漀 怀瀀愀爀攀渀琀䴀漀搀攀氀怀 眀椀氀氀 戀攀 爀攀昀氀攀挀琀攀搀ഀഀ
 *   in `localModel` and any changes in `localModel` will reflect in `parentModel`. If the parent਍ ⨀   猀挀漀瀀攀 瀀爀漀瀀攀爀琀礀 搀漀攀猀渀✀琀 攀砀椀猀琀Ⰰ 椀琀 眀椀氀氀 琀栀爀漀眀 愀 一伀一开䄀匀匀䤀䜀一䄀䈀䰀䔀开䴀伀䐀䔀䰀开䔀堀倀刀䔀匀匀䤀伀一 攀砀挀攀瀀琀椀漀渀⸀ 夀漀甀ഀഀ
 *   can avoid this behavior using `=?` or `=?attr` in order to flag the property as optional.਍ ⨀ഀഀ
 * * `&` or `&attr` - provides a way to execute an expression in the context of the parent scope.਍ ⨀   䤀昀 渀漀 怀愀琀琀爀怀 渀愀洀攀 椀猀 猀瀀攀挀椀昀椀攀搀 琀栀攀渀 琀栀攀 愀琀琀爀椀戀甀琀攀 渀愀洀攀 椀猀 愀猀猀甀洀攀搀 琀漀 戀攀 琀栀攀 猀愀洀攀 愀猀 琀栀攀ഀഀ
 *   local name. Given `<widget my-attr="count = count + value">` and widget definition of਍ ⨀   怀猀挀漀瀀攀㨀 笀 氀漀挀愀氀䘀渀㨀✀☀洀礀䄀琀琀爀✀ 紀怀Ⰰ 琀栀攀渀 椀猀漀氀愀琀攀 猀挀漀瀀攀 瀀爀漀瀀攀爀琀礀 怀氀漀挀愀氀䘀渀怀 眀椀氀氀 瀀漀椀渀琀 琀漀ഀഀ
 *   a function wrapper for the `count = count + value` expression. Often it's desirable to਍ ⨀   瀀愀猀猀 搀愀琀愀 昀爀漀洀 琀栀攀 椀猀漀氀愀琀攀搀 猀挀漀瀀攀 瘀椀愀 愀渀 攀砀瀀爀攀猀猀椀漀渀 愀渀搀 琀漀 琀栀攀 瀀愀爀攀渀琀 猀挀漀瀀攀Ⰰ 琀栀椀猀 挀愀渀 戀攀ഀഀ
 *   done by passing a map of local variable names and values into the expression wrapper fn.਍ ⨀   䘀漀爀 攀砀愀洀瀀氀攀Ⰰ 椀昀 琀栀攀 攀砀瀀爀攀猀猀椀漀渀 椀猀 怀椀渀挀爀攀洀攀渀琀⠀愀洀漀甀渀琀⤀怀 琀栀攀渀 眀攀 挀愀渀 猀瀀攀挀椀昀礀 琀栀攀 愀洀漀甀渀琀 瘀愀氀甀攀ഀഀ
 *   by calling the `localFn` as `localFn({amount: 22})`.਍ ⨀ഀഀ
 *਍ ⨀ഀഀ
 * #### `controller`਍ ⨀ 䌀漀渀琀爀漀氀氀攀爀 挀漀渀猀琀爀甀挀琀漀爀 昀甀渀挀琀椀漀渀⸀ 吀栀攀 挀漀渀琀爀漀氀氀攀爀 椀猀 椀渀猀琀愀渀琀椀愀琀攀搀 戀攀昀漀爀攀 琀栀攀ഀഀ
 * pre-linking phase and it is shared with other directives (see਍ ⨀ 怀爀攀焀甀椀爀攀怀 愀琀琀爀椀戀甀琀攀⤀⸀ 吀栀椀猀 愀氀氀漀眀猀 琀栀攀 搀椀爀攀挀琀椀瘀攀猀 琀漀 挀漀洀洀甀渀椀挀愀琀攀 眀椀琀栀 攀愀挀栀 漀琀栀攀爀 愀渀搀 愀甀最洀攀渀琀ഀഀ
 * each other's behavior. The controller is injectable (and supports bracket notation) with the following locals:਍ ⨀ഀഀ
 * * `$scope` - Current scope associated with the element਍ ⨀ ⨀ 怀␀攀氀攀洀攀渀琀怀 ⴀ 䌀甀爀爀攀渀琀 攀氀攀洀攀渀琀ഀഀ
 * * `$attrs` - Current attributes object for the element਍ ⨀ ⨀ 怀␀琀爀愀渀猀挀氀甀搀攀怀 ⴀ 䄀 琀爀愀渀猀挀氀甀搀攀 氀椀渀欀椀渀最 昀甀渀挀琀椀漀渀 瀀爀攀ⴀ戀漀甀渀搀 琀漀 琀栀攀 挀漀爀爀攀挀琀 琀爀愀渀猀挀氀甀猀椀漀渀 猀挀漀瀀攀⸀ഀഀ
 *    The scope can be overridden by an optional first argument.਍ ⨀   怀昀甀渀挀琀椀漀渀⠀嬀猀挀漀瀀攀崀Ⰰ 挀氀漀渀攀䰀椀渀欀椀渀最䘀渀⤀怀⸀ഀഀ
 *਍ ⨀ഀഀ
 * #### `require`਍ ⨀ 刀攀焀甀椀爀攀 愀渀漀琀栀攀爀 搀椀爀攀挀琀椀瘀攀 愀渀搀 椀渀樀攀挀琀 椀琀猀 挀漀渀琀爀漀氀氀攀爀 愀猀 琀栀攀 昀漀甀爀琀栀 愀爀最甀洀攀渀琀 琀漀 琀栀攀 氀椀渀欀椀渀最 昀甀渀挀琀椀漀渀⸀ 吀栀攀ഀഀ
 * `require` takes a string name (or array of strings) of the directive(s) to pass in. If an array is used, the਍ ⨀ 椀渀樀攀挀琀攀搀 愀爀最甀洀攀渀琀 眀椀氀氀 戀攀 愀渀 愀爀爀愀礀 椀渀 挀漀爀爀攀猀瀀漀渀搀椀渀最 漀爀搀攀爀⸀ 䤀昀 渀漀 猀甀挀栀 搀椀爀攀挀琀椀瘀攀 挀愀渀 戀攀ഀഀ
 * found, or if the directive does not have a controller, then an error is raised. The name can be prefixed with:਍ ⨀ഀഀ
 * * (no prefix) - Locate the required controller on the current element. Throw an error if not found.਍ ⨀ ⨀ 怀㼀怀 ⴀ 䄀琀琀攀洀瀀琀 琀漀 氀漀挀愀琀攀 琀栀攀 爀攀焀甀椀爀攀搀 挀漀渀琀爀漀氀氀攀爀 漀爀 瀀愀猀猀 怀渀甀氀氀怀 琀漀 琀栀攀 怀氀椀渀欀怀 昀渀 椀昀 渀漀琀 昀漀甀渀搀⸀ഀഀ
 * * `^` - Locate the required controller by searching the element's parents. Throw an error if not found.਍ ⨀ ⨀ 怀㼀帀怀 ⴀ 䄀琀琀攀洀瀀琀 琀漀 氀漀挀愀琀攀 琀栀攀 爀攀焀甀椀爀攀搀 挀漀渀琀爀漀氀氀攀爀 戀礀 猀攀愀爀挀栀椀渀最 琀栀攀 攀氀攀洀攀渀琀✀猀 瀀愀爀攀渀琀猀 漀爀 瀀愀猀猀 怀渀甀氀氀怀 琀漀 琀栀攀ഀഀ
 *   `link` fn if not found.਍ ⨀ഀഀ
 *਍ ⨀ ⌀⌀⌀⌀ 怀挀漀渀琀爀漀氀氀攀爀䄀猀怀ഀഀ
 * Controller alias at the directive scope. An alias for the controller so it਍ ⨀ 挀愀渀 戀攀 爀攀昀攀爀攀渀挀攀搀 愀琀 琀栀攀 搀椀爀攀挀琀椀瘀攀 琀攀洀瀀氀愀琀攀⸀ 吀栀攀 搀椀爀攀挀琀椀瘀攀 渀攀攀搀猀 琀漀 搀攀昀椀渀攀 愀 猀挀漀瀀攀 昀漀爀 琀栀椀猀ഀഀ
 * configuration to be used. Useful in the case when directive is used as component.਍ ⨀ഀഀ
 *਍ ⨀ ⌀⌀⌀⌀ 怀爀攀猀琀爀椀挀琀怀ഀഀ
 * String of subset of `EACM` which restricts the directive to a specific directive਍ ⨀ 搀攀挀氀愀爀愀琀椀漀渀 猀琀礀氀攀⸀ 䤀昀 漀洀椀琀琀攀搀Ⰰ 琀栀攀 搀攀昀愀甀氀琀 ⠀愀琀琀爀椀戀甀琀攀猀 漀渀氀礀⤀ 椀猀 甀猀攀搀⸀ഀഀ
 *਍ ⨀ ⨀ 怀䔀怀 ⴀ 䔀氀攀洀攀渀琀 渀愀洀攀㨀 怀㰀洀礀ⴀ搀椀爀攀挀琀椀瘀攀㸀㰀⼀洀礀ⴀ搀椀爀攀挀琀椀瘀攀㸀怀ഀഀ
 * * `A` - Attribute (default): `<div my-directive="exp"></div>`਍ ⨀ ⨀ 怀䌀怀 ⴀ 䌀氀愀猀猀㨀 怀㰀搀椀瘀 挀氀愀猀猀㴀∀洀礀ⴀ搀椀爀攀挀琀椀瘀攀㨀 攀砀瀀㬀∀㸀㰀⼀搀椀瘀㸀怀ഀഀ
 * * `M` - Comment: `<!-- directive: my-directive exp -->`਍ ⨀ഀഀ
 *਍ ⨀ ⌀⌀⌀⌀ 怀琀攀洀瀀氀愀琀攀怀ഀഀ
 * replace the current element with the contents of the HTML. The replacement process਍ ⨀ 洀椀最爀愀琀攀猀 愀氀氀 漀昀 琀栀攀 愀琀琀爀椀戀甀琀攀猀 ⼀ 挀氀愀猀猀攀猀 昀爀漀洀 琀栀攀 漀氀搀 攀氀攀洀攀渀琀 琀漀 琀栀攀 渀攀眀 漀渀攀⸀ 匀攀攀 琀栀攀ഀഀ
 * {@link guide/directive#creating-custom-directives_creating-directives_template-expanding-directive਍ ⨀ 䐀椀爀攀挀琀椀瘀攀猀 䜀甀椀搀攀紀 昀漀爀 愀渀 攀砀愀洀瀀氀攀⸀ഀഀ
 *਍ ⨀ 夀漀甀 挀愀渀 猀瀀攀挀椀昀礀 怀琀攀洀瀀氀愀琀攀怀 愀猀 愀 猀琀爀椀渀最 爀攀瀀爀攀猀攀渀琀椀渀最 琀栀攀 琀攀洀瀀氀愀琀攀 漀爀 愀猀 愀 昀甀渀挀琀椀漀渀 眀栀椀挀栀 琀愀欀攀猀ഀഀ
 * two arguments `tElement` and `tAttrs` (described in the `compile` function api below) and਍ ⨀ 爀攀琀甀爀渀猀 愀 猀琀爀椀渀最 瘀愀氀甀攀 爀攀瀀爀攀猀攀渀琀椀渀最 琀栀攀 琀攀洀瀀氀愀琀攀⸀ഀഀ
 *਍ ⨀ഀഀ
 * #### `templateUrl`਍ ⨀ 匀愀洀攀 愀猀 怀琀攀洀瀀氀愀琀攀怀 戀甀琀 琀栀攀 琀攀洀瀀氀愀琀攀 椀猀 氀漀愀搀攀搀 昀爀漀洀 琀栀攀 猀瀀攀挀椀昀椀攀搀 唀刀䰀⸀ 䈀攀挀愀甀猀攀ഀഀ
 * the template loading is asynchronous the compilation/linking is suspended until the template਍ ⨀ 椀猀 氀漀愀搀攀搀⸀ഀഀ
 *਍ ⨀ 夀漀甀 挀愀渀 猀瀀攀挀椀昀礀 怀琀攀洀瀀氀愀琀攀唀爀氀怀 愀猀 愀 猀琀爀椀渀最 爀攀瀀爀攀猀攀渀琀椀渀最 琀栀攀 唀刀䰀 漀爀 愀猀 愀 昀甀渀挀琀椀漀渀 眀栀椀挀栀 琀愀欀攀猀 琀眀漀ഀഀ
 * arguments `tElement` and `tAttrs` (described in the `compile` function api below) and returns਍ ⨀ 愀 猀琀爀椀渀最 瘀愀氀甀攀 爀攀瀀爀攀猀攀渀琀椀渀最 琀栀攀 甀爀氀⸀  䤀渀 攀椀琀栀攀爀 挀愀猀攀Ⰰ 琀栀攀 琀攀洀瀀氀愀琀攀 唀刀䰀 椀猀 瀀愀猀猀攀搀 琀栀爀漀甀最栀 笀䀀氀椀渀欀ഀഀ
 * api/ng.$sce#methods_getTrustedResourceUrl $sce.getTrustedResourceUrl}.਍ ⨀ഀഀ
 *਍ ⨀ ⌀⌀⌀⌀ 怀爀攀瀀氀愀挀攀怀ഀഀ
 * specify where the template should be inserted. Defaults to `false`.਍ ⨀ഀഀ
 * * `true` - the template will replace the current element.਍ ⨀ ⨀ 怀昀愀氀猀攀怀 ⴀ 琀栀攀 琀攀洀瀀氀愀琀攀 眀椀氀氀 爀攀瀀氀愀挀攀 琀栀攀 挀漀渀琀攀渀琀猀 漀昀 琀栀攀 挀甀爀爀攀渀琀 攀氀攀洀攀渀琀⸀ഀഀ
 *਍ ⨀ഀഀ
 * #### `transclude`਍ ⨀ 挀漀洀瀀椀氀攀 琀栀攀 挀漀渀琀攀渀琀 漀昀 琀栀攀 攀氀攀洀攀渀琀 愀渀搀 洀愀欀攀 椀琀 愀瘀愀椀氀愀戀氀攀 琀漀 琀栀攀 搀椀爀攀挀琀椀瘀攀⸀ഀഀ
 * Typically used with {@link api/ng.directive:ngTransclude਍ ⨀ 渀最吀爀愀渀猀挀氀甀搀攀紀⸀ 吀栀攀 愀搀瘀愀渀琀愀最攀 漀昀 琀爀愀渀猀挀氀甀猀椀漀渀 椀猀 琀栀愀琀 琀栀攀 氀椀渀欀椀渀最 昀甀渀挀琀椀漀渀 爀攀挀攀椀瘀攀猀 愀ഀഀ
 * transclusion function which is pre-bound to the correct scope. In a typical setup the widget਍ ⨀ 挀爀攀愀琀攀猀 愀渀 怀椀猀漀氀愀琀攀怀 猀挀漀瀀攀Ⰰ 戀甀琀 琀栀攀 琀爀愀渀猀挀氀甀猀椀漀渀 椀猀 渀漀琀 愀 挀栀椀氀搀Ⰰ 戀甀琀 愀 猀椀戀氀椀渀最 漀昀 琀栀攀 怀椀猀漀氀愀琀攀怀ഀഀ
 * scope. This makes it possible for the widget to have private state, and the transclusion to਍ ⨀ 戀攀 戀漀甀渀搀 琀漀 琀栀攀 瀀愀爀攀渀琀 ⠀瀀爀攀ⴀ怀椀猀漀氀愀琀攀怀⤀ 猀挀漀瀀攀⸀ഀഀ
 *਍ ⨀ ⨀ 怀琀爀甀攀怀 ⴀ 琀爀愀渀猀挀氀甀搀攀 琀栀攀 挀漀渀琀攀渀琀 漀昀 琀栀攀 搀椀爀攀挀琀椀瘀攀⸀ഀഀ
 * * `'element'` - transclude the whole element including any directives defined at lower priority.਍ ⨀ഀഀ
 *਍ ⨀ ⌀⌀⌀⌀ 怀挀漀洀瀀椀氀攀怀ഀഀ
 *਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   function compile(tElement, tAttrs, transclude) { ... }਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 吀栀攀 挀漀洀瀀椀氀攀 昀甀渀挀琀椀漀渀 搀攀愀氀猀 眀椀琀栀 琀爀愀渀猀昀漀爀洀椀渀最 琀栀攀 琀攀洀瀀氀愀琀攀 䐀伀䴀⸀ 匀椀渀挀攀 洀漀猀琀 搀椀爀攀挀琀椀瘀攀猀 搀漀 渀漀琀 搀漀ഀഀ
 * template transformation, it is not used often. Examples that require compile functions are਍ ⨀ 搀椀爀攀挀琀椀瘀攀猀 琀栀愀琀 琀爀愀渀猀昀漀爀洀 琀攀洀瀀氀愀琀攀 䐀伀䴀Ⰰ 猀甀挀栀 愀猀 笀䀀氀椀渀欀ഀഀ
 * api/ng.directive:ngRepeat ngRepeat}, or load the contents਍ ⨀ 愀猀礀渀挀栀爀漀渀漀甀猀氀礀Ⰰ 猀甀挀栀 愀猀 笀䀀氀椀渀欀 愀瀀椀⼀渀最刀漀甀琀攀⸀搀椀爀攀挀琀椀瘀攀㨀渀最嘀椀攀眀 渀最嘀椀攀眀紀⸀ 吀栀攀ഀഀ
 * compile function takes the following arguments.਍ ⨀ഀഀ
 *   * `tElement` - template element - The element where the directive has been declared. It is਍ ⨀     猀愀昀攀 琀漀 搀漀 琀攀洀瀀氀愀琀攀 琀爀愀渀猀昀漀爀洀愀琀椀漀渀 漀渀 琀栀攀 攀氀攀洀攀渀琀 愀渀搀 挀栀椀氀搀 攀氀攀洀攀渀琀猀 漀渀氀礀⸀ഀഀ
 *਍ ⨀   ⨀ 怀琀䄀琀琀爀猀怀 ⴀ 琀攀洀瀀氀愀琀攀 愀琀琀爀椀戀甀琀攀猀 ⴀ 一漀爀洀愀氀椀稀攀搀 氀椀猀琀 漀昀 愀琀琀爀椀戀甀琀攀猀 搀攀挀氀愀爀攀搀 漀渀 琀栀椀猀 攀氀攀洀攀渀琀 猀栀愀爀攀搀ഀഀ
 *     between all directive compile functions.਍ ⨀ഀഀ
 *   * `transclude` -  [*DEPRECATED*!] A transclude linking function: `function(scope, cloneLinkingFn)`਍ ⨀ഀഀ
 * <div class="alert alert-warning">਍ ⨀ ⨀⨀一漀琀攀㨀⨀⨀ 吀栀攀 琀攀洀瀀氀愀琀攀 椀渀猀琀愀渀挀攀 愀渀搀 琀栀攀 氀椀渀欀 椀渀猀琀愀渀挀攀 洀愀礀 戀攀 搀椀昀昀攀爀攀渀琀 漀戀樀攀挀琀猀 椀昀 琀栀攀 琀攀洀瀀氀愀琀攀 栀愀猀ഀഀ
 * been cloned. For this reason it is **not** safe to do anything other than DOM transformations that਍ ⨀ 愀瀀瀀氀礀 琀漀 愀氀氀 挀氀漀渀攀搀 䐀伀䴀 渀漀搀攀猀 眀椀琀栀椀渀 琀栀攀 挀漀洀瀀椀氀攀 昀甀渀挀琀椀漀渀⸀ 匀瀀攀挀椀昀椀挀愀氀氀礀Ⰰ 䐀伀䴀 氀椀猀琀攀渀攀爀 爀攀最椀猀琀爀愀琀椀漀渀ഀഀ
 * should be done in a linking function rather than in a compile function.਍ ⨀ 㰀⼀搀椀瘀㸀ഀഀ
 *਍ ⨀ 㰀搀椀瘀 挀氀愀猀猀㴀∀愀氀攀爀琀 愀氀攀爀琀ⴀ攀爀爀漀爀∀㸀ഀഀ
 * **Note:** The `transclude` function that is passed to the compile function is deprecated, as it਍ ⨀   攀⸀最⸀ 搀漀攀猀 渀漀琀 欀渀漀眀 愀戀漀甀琀 琀栀攀 爀椀最栀琀 漀甀琀攀爀 猀挀漀瀀攀⸀ 倀氀攀愀猀攀 甀猀攀 琀栀攀 琀爀愀渀猀挀氀甀搀攀 昀甀渀挀琀椀漀渀 琀栀愀琀 椀猀 瀀愀猀猀攀搀ഀഀ
 *   to the link function instead.਍ ⨀ 㰀⼀搀椀瘀㸀ഀഀ
਍ ⨀ 䄀 挀漀洀瀀椀氀攀 昀甀渀挀琀椀漀渀 挀愀渀 栀愀瘀攀 愀 爀攀琀甀爀渀 瘀愀氀甀攀 眀栀椀挀栀 挀愀渀 戀攀 攀椀琀栀攀爀 愀 昀甀渀挀琀椀漀渀 漀爀 愀渀 漀戀樀攀挀琀⸀ഀഀ
 *਍ ⨀ ⨀ 爀攀琀甀爀渀椀渀最 愀 ⠀瀀漀猀琀ⴀ氀椀渀欀⤀ 昀甀渀挀琀椀漀渀 ⴀ 椀猀 攀焀甀椀瘀愀氀攀渀琀 琀漀 爀攀最椀猀琀攀爀椀渀最 琀栀攀 氀椀渀欀椀渀最 昀甀渀挀琀椀漀渀 瘀椀愀 琀栀攀ഀഀ
 *   `link` property of the config object when the compile function is empty.਍ ⨀ഀഀ
 * * returning an object with function(s) registered via `pre` and `post` properties - allows you to਍ ⨀   挀漀渀琀爀漀氀 眀栀攀渀 愀 氀椀渀欀椀渀最 昀甀渀挀琀椀漀渀 猀栀漀甀氀搀 戀攀 挀愀氀氀攀搀 搀甀爀椀渀最 琀栀攀 氀椀渀欀椀渀最 瀀栀愀猀攀⸀ 匀攀攀 椀渀昀漀 愀戀漀甀琀ഀഀ
 *   pre-linking and post-linking functions below.਍ ⨀ഀഀ
 *਍ ⨀ ⌀⌀⌀⌀ 怀氀椀渀欀怀ഀഀ
 * This property is used only if the `compile` property is not defined.਍ ⨀ഀഀ
 * <pre>਍ ⨀   昀甀渀挀琀椀漀渀 氀椀渀欀⠀猀挀漀瀀攀Ⰰ 椀䔀氀攀洀攀渀琀Ⰰ 椀䄀琀琀爀猀Ⰰ 挀漀渀琀爀漀氀氀攀爀Ⰰ 琀爀愀渀猀挀氀甀搀攀䘀渀⤀ 笀 ⸀⸀⸀ 紀ഀഀ
 * </pre>਍ ⨀ഀഀ
 * The link function is responsible for registering DOM listeners as well as updating the DOM. It is਍ ⨀ 攀砀攀挀甀琀攀搀 愀昀琀攀爀 琀栀攀 琀攀洀瀀氀愀琀攀 栀愀猀 戀攀攀渀 挀氀漀渀攀搀⸀ 吀栀椀猀 椀猀 眀栀攀爀攀 洀漀猀琀 漀昀 琀栀攀 搀椀爀攀挀琀椀瘀攀 氀漀最椀挀 眀椀氀氀 戀攀ഀഀ
 * put.਍ ⨀ഀഀ
 *   * `scope` - {@link api/ng.$rootScope.Scope Scope} - The scope to be used by the਍ ⨀     搀椀爀攀挀琀椀瘀攀 昀漀爀 爀攀最椀猀琀攀爀椀渀最 笀䀀氀椀渀欀 愀瀀椀⼀渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀眀愀琀挀栀 眀愀琀挀栀攀猀紀⸀ഀഀ
 *਍ ⨀   ⨀ 怀椀䔀氀攀洀攀渀琀怀 ⴀ 椀渀猀琀愀渀挀攀 攀氀攀洀攀渀琀 ⴀ 吀栀攀 攀氀攀洀攀渀琀 眀栀攀爀攀 琀栀攀 搀椀爀攀挀琀椀瘀攀 椀猀 琀漀 戀攀 甀猀攀搀⸀ 䤀琀 椀猀 猀愀昀攀 琀漀ഀഀ
 *     manipulate the children of the element only in `postLink` function since the children have਍ ⨀     愀氀爀攀愀搀礀 戀攀攀渀 氀椀渀欀攀搀⸀ഀഀ
 *਍ ⨀   ⨀ 怀椀䄀琀琀爀猀怀 ⴀ 椀渀猀琀愀渀挀攀 愀琀琀爀椀戀甀琀攀猀 ⴀ 一漀爀洀愀氀椀稀攀搀 氀椀猀琀 漀昀 愀琀琀爀椀戀甀琀攀猀 搀攀挀氀愀爀攀搀 漀渀 琀栀椀猀 攀氀攀洀攀渀琀 猀栀愀爀攀搀ഀഀ
 *     between all directive linking functions.਍ ⨀ഀഀ
 *   * `controller` - a controller instance - A controller instance if at least one directive on the਍ ⨀     攀氀攀洀攀渀琀 搀攀昀椀渀攀猀 愀 挀漀渀琀爀漀氀氀攀爀⸀ 吀栀攀 挀漀渀琀爀漀氀氀攀爀 椀猀 猀栀愀爀攀搀 愀洀漀渀最 愀氀氀 琀栀攀 搀椀爀攀挀琀椀瘀攀猀Ⰰ 眀栀椀挀栀 愀氀氀漀眀猀ഀഀ
 *     the directives to use the controllers as a communication channel.਍ ⨀ഀഀ
 *   * `transcludeFn` - A transclude linking function pre-bound to the correct transclusion scope.਍ ⨀     吀栀攀 猀挀漀瀀攀 挀愀渀 戀攀 漀瘀攀爀爀椀搀搀攀渀 戀礀 愀渀 漀瀀琀椀漀渀愀氀 昀椀爀猀琀 愀爀最甀洀攀渀琀⸀ 吀栀椀猀 椀猀 琀栀攀 猀愀洀攀 愀猀 琀栀攀 怀␀琀爀愀渀猀挀氀甀搀攀怀ഀഀ
 *     parameter of directive controllers.਍ ⨀     怀昀甀渀挀琀椀漀渀⠀嬀猀挀漀瀀攀崀Ⰰ 挀氀漀渀攀䰀椀渀欀椀渀最䘀渀⤀怀⸀ഀഀ
 *਍ ⨀ഀഀ
 * #### Pre-linking function਍ ⨀ഀഀ
 * Executed before the child elements are linked. Not safe to do DOM transformation since the਍ ⨀ 挀漀洀瀀椀氀攀爀 氀椀渀欀椀渀最 昀甀渀挀琀椀漀渀 眀椀氀氀 昀愀椀氀 琀漀 氀漀挀愀琀攀 琀栀攀 挀漀爀爀攀挀琀 攀氀攀洀攀渀琀猀 昀漀爀 氀椀渀欀椀渀最⸀ഀഀ
 *਍ ⨀ ⌀⌀⌀⌀ 倀漀猀琀ⴀ氀椀渀欀椀渀最 昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䔀砀攀挀甀琀攀搀 愀昀琀攀爀 琀栀攀 挀栀椀氀搀 攀氀攀洀攀渀琀猀 愀爀攀 氀椀渀欀攀搀⸀ 䤀琀 椀猀 猀愀昀攀 琀漀 搀漀 䐀伀䴀 琀爀愀渀猀昀漀爀洀愀琀椀漀渀 椀渀 琀栀攀 瀀漀猀琀ⴀ氀椀渀欀椀渀最 昀甀渀挀琀椀漀渀⸀ഀഀ
 *਍ ⨀ 㰀愀 渀愀洀攀㴀∀䄀琀琀爀椀戀甀琀攀猀∀㸀㰀⼀愀㸀ഀഀ
 * ### Attributes਍ ⨀ഀഀ
 * The {@link api/ng.$compile.directive.Attributes Attributes} object - passed as a parameter in the਍ ⨀ 怀氀椀渀欀⠀⤀怀 漀爀 怀挀漀洀瀀椀氀攀⠀⤀怀 昀甀渀挀琀椀漀渀猀⸀ 䤀琀 栀愀猀 愀 瘀愀爀椀攀琀礀 漀昀 甀猀攀猀⸀ഀഀ
 *਍ ⨀ 愀挀挀攀猀猀椀渀最 ⨀一漀爀洀愀氀椀稀攀搀 愀琀琀爀椀戀甀琀攀 渀愀洀攀猀㨀⨀ഀഀ
 * Directives like 'ngBind' can be expressed in many ways: 'ng:bind', `data-ng-bind`, or 'x-ng-bind'.਍ ⨀ 琀栀攀 愀琀琀爀椀戀甀琀攀猀 漀戀樀攀挀琀 愀氀氀漀眀猀 昀漀爀 渀漀爀洀愀氀椀稀攀搀 愀挀挀攀猀猀 琀漀ഀഀ
 *   the attributes.਍ ⨀ഀഀ
 * * *Directive inter-communication:* All directives share the same instance of the attributes਍ ⨀   漀戀樀攀挀琀 眀栀椀挀栀 愀氀氀漀眀猀 琀栀攀 搀椀爀攀挀琀椀瘀攀猀 琀漀 甀猀攀 琀栀攀 愀琀琀爀椀戀甀琀攀猀 漀戀樀攀挀琀 愀猀 椀渀琀攀爀 搀椀爀攀挀琀椀瘀攀ഀഀ
 *   communication.਍ ⨀ഀഀ
 * * *Supports interpolation:* Interpolation attributes are assigned to the attribute object਍ ⨀   愀氀氀漀眀椀渀最 漀琀栀攀爀 搀椀爀攀挀琀椀瘀攀猀 琀漀 爀攀愀搀 琀栀攀 椀渀琀攀爀瀀漀氀愀琀攀搀 瘀愀氀甀攀⸀ഀഀ
 *਍ ⨀ ⨀ ⨀伀戀猀攀爀瘀椀渀最 椀渀琀攀爀瀀漀氀愀琀攀搀 愀琀琀爀椀戀甀琀攀猀㨀⨀ 唀猀攀 怀␀漀戀猀攀爀瘀攀怀 琀漀 漀戀猀攀爀瘀攀 琀栀攀 瘀愀氀甀攀 挀栀愀渀最攀猀 漀昀 愀琀琀爀椀戀甀琀攀猀ഀഀ
 *   that contain interpolation (e.g. `src="{{bar}}"`). Not only is this very efficient but it's also਍ ⨀   琀栀攀 漀渀氀礀 眀愀礀 琀漀 攀愀猀椀氀礀 最攀琀 琀栀攀 愀挀琀甀愀氀 瘀愀氀甀攀 戀攀挀愀甀猀攀 搀甀爀椀渀最 琀栀攀 氀椀渀欀椀渀最 瀀栀愀猀攀 琀栀攀 椀渀琀攀爀瀀漀氀愀琀椀漀渀ഀഀ
 *   hasn't been evaluated yet and so the value is at this time set to `undefined`.਍ ⨀ഀഀ
 * <pre>਍ ⨀ 昀甀渀挀琀椀漀渀 氀椀渀欀椀渀最䘀渀⠀猀挀漀瀀攀Ⰰ 攀氀洀Ⰰ 愀琀琀爀猀Ⰰ 挀琀爀氀⤀ 笀ഀഀ
 *   // get the attribute value਍ ⨀   挀漀渀猀漀氀攀⸀氀漀最⠀愀琀琀爀猀⸀渀最䴀漀搀攀氀⤀㬀ഀഀ
 *਍ ⨀   ⼀⼀ 挀栀愀渀最攀 琀栀攀 愀琀琀爀椀戀甀琀攀ഀഀ
 *   attrs.$set('ngModel', 'new value');਍ ⨀ഀഀ
 *   // observe changes to interpolated attribute਍ ⨀   愀琀琀爀猀⸀␀漀戀猀攀爀瘀攀⠀✀渀最䴀漀搀攀氀✀Ⰰ 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
 *     console.log('ngModel has changed value to ' + value);਍ ⨀   紀⤀㬀ഀഀ
 * }਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 䈀攀氀漀眀 椀猀 愀渀 攀砀愀洀瀀氀攀 甀猀椀渀最 怀␀挀漀洀瀀椀氀攀倀爀漀瘀椀搀攀爀怀⸀ഀഀ
 *਍ ⨀ 㰀搀椀瘀 挀氀愀猀猀㴀∀愀氀攀爀琀 愀氀攀爀琀ⴀ眀愀爀渀椀渀最∀㸀ഀഀ
 * **Note**: Typically directives are registered with `module.directive`. The example below is਍ ⨀ 琀漀 椀氀氀甀猀琀爀愀琀攀 栀漀眀 怀␀挀漀洀瀀椀氀攀怀 眀漀爀欀猀⸀ഀഀ
 * </div>਍ ⨀ഀഀ
 <doc:example module="compile">਍   㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
    <script>਍      愀渀最甀氀愀爀⸀洀漀搀甀氀攀⠀✀挀漀洀瀀椀氀攀✀Ⰰ 嬀崀Ⰰ 昀甀渀挀琀椀漀渀⠀␀挀漀洀瀀椀氀攀倀爀漀瘀椀搀攀爀⤀ 笀ഀഀ
        // configure new 'compile' directive by passing a directive਍        ⼀⼀ 昀愀挀琀漀爀礀 昀甀渀挀琀椀漀渀⸀ 吀栀攀 昀愀挀琀漀爀礀 昀甀渀挀琀椀漀渀 椀渀樀攀挀琀猀 琀栀攀 ✀␀挀漀洀瀀椀氀攀✀ഀഀ
        $compileProvider.directive('compile', function($compile) {਍          ⼀⼀ 搀椀爀攀挀琀椀瘀攀 昀愀挀琀漀爀礀 挀爀攀愀琀攀猀 愀 氀椀渀欀 昀甀渀挀琀椀漀渀ഀഀ
          return function(scope, element, attrs) {਍            猀挀漀瀀攀⸀␀眀愀琀挀栀⠀ഀഀ
              function(scope) {਍                 ⼀⼀ 眀愀琀挀栀 琀栀攀 ✀挀漀洀瀀椀氀攀✀ 攀砀瀀爀攀猀猀椀漀渀 昀漀爀 挀栀愀渀最攀猀ഀഀ
                return scope.$eval(attrs.compile);਍              紀Ⰰഀഀ
              function(value) {਍                ⼀⼀ 眀栀攀渀 琀栀攀 ✀挀漀洀瀀椀氀攀✀ 攀砀瀀爀攀猀猀椀漀渀 挀栀愀渀最攀猀ഀഀ
                // assign it into the current DOM਍                攀氀攀洀攀渀琀⸀栀琀洀氀⠀瘀愀氀甀攀⤀㬀ഀഀ
਍                ⼀⼀ 挀漀洀瀀椀氀攀 琀栀攀 渀攀眀 䐀伀䴀 愀渀搀 氀椀渀欀 椀琀 琀漀 琀栀攀 挀甀爀爀攀渀琀ഀഀ
                // scope.਍                ⼀⼀ 一伀吀䔀㨀 眀攀 漀渀氀礀 挀漀洀瀀椀氀攀 ⸀挀栀椀氀搀一漀搀攀猀 猀漀 琀栀愀琀ഀഀ
                // we don't get into infinite loop compiling ourselves਍                ␀挀漀洀瀀椀氀攀⠀攀氀攀洀攀渀琀⸀挀漀渀琀攀渀琀猀⠀⤀⤀⠀猀挀漀瀀攀⤀㬀ഀഀ
              }਍            ⤀㬀ഀഀ
          };਍        紀⤀ഀഀ
      });਍ഀഀ
      function Ctrl($scope) {਍        ␀猀挀漀瀀攀⸀渀愀洀攀 㴀 ✀䄀渀最甀氀愀爀✀㬀ഀഀ
        $scope.html = 'Hello {{name}}';਍      紀ഀഀ
    </script>਍    㰀搀椀瘀 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀䌀琀爀氀∀㸀ഀഀ
      <input ng-model="name"> <br>਍      㰀琀攀砀琀愀爀攀愀 渀最ⴀ洀漀搀攀氀㴀∀栀琀洀氀∀㸀㰀⼀琀攀砀琀愀爀攀愀㸀 㰀戀爀㸀ഀഀ
      <div compile="html"></div>਍    㰀⼀搀椀瘀㸀ഀഀ
   </doc:source>਍   㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
     it('should auto compile', function() {਍       攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀搀椀瘀嬀挀漀洀瀀椀氀攀崀✀⤀⸀琀攀砀琀⠀⤀⤀⸀琀漀䈀攀⠀✀䠀攀氀氀漀 䄀渀最甀氀愀爀✀⤀㬀ഀഀ
       input('html').enter('{{name}}!');਍       攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀搀椀瘀嬀挀漀洀瀀椀氀攀崀✀⤀⸀琀攀砀琀⠀⤀⤀⸀琀漀䈀攀⠀✀䄀渀最甀氀愀爀℀✀⤀㬀ഀഀ
     });਍   㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
 </doc:example>਍ഀഀ
 *਍ ⨀ഀഀ
 * @param {string|DOMElement} element Element or HTML string to compile into a template function.਍ ⨀ 䀀瀀愀爀愀洀 笀昀甀渀挀琀椀漀渀⠀愀渀最甀氀愀爀⸀匀挀漀瀀攀嬀Ⰰ 挀氀漀渀攀䄀琀琀愀挀栀䘀渀崀紀 琀爀愀渀猀挀氀甀搀攀 昀甀渀挀琀椀漀渀 愀瘀愀椀氀愀戀氀攀 琀漀 搀椀爀攀挀琀椀瘀攀猀⸀ഀഀ
 * @param {number} maxPriority only apply directives lower then given priority (Only effects the਍ ⨀                 爀漀漀琀 攀氀攀洀攀渀琀⠀猀⤀Ⰰ 渀漀琀 琀栀攀椀爀 挀栀椀氀搀爀攀渀⤀ഀഀ
 * @returns {function(scope[, cloneAttachFn])} a link function which is used to bind template਍ ⨀ ⠀愀 䐀伀䴀 攀氀攀洀攀渀琀⼀琀爀攀攀⤀ 琀漀 愀 猀挀漀瀀攀⸀ 圀栀攀爀攀㨀ഀഀ
 *਍ ⨀  ⨀ 怀猀挀漀瀀攀怀 ⴀ 䄀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀 匀挀漀瀀攀紀 琀漀 戀椀渀搀 琀漀⸀ഀഀ
 *  * `cloneAttachFn` - If `cloneAttachFn` is provided, then the link function will clone the਍ ⨀  怀琀攀洀瀀氀愀琀攀怀 愀渀搀 挀愀氀氀 琀栀攀 怀挀氀漀渀攀䄀琀琀愀挀栀䘀渀怀 昀甀渀挀琀椀漀渀 愀氀氀漀眀椀渀最 琀栀攀 挀愀氀氀攀爀 琀漀 愀琀琀愀挀栀 琀栀攀ഀഀ
 *  cloned elements to the DOM document at the appropriate place. The `cloneAttachFn` is਍ ⨀  挀愀氀氀攀搀 愀猀㨀 㰀戀爀㸀 怀挀氀漀渀攀䄀琀琀愀挀栀䘀渀⠀挀氀漀渀攀搀䔀氀攀洀攀渀琀Ⰰ 猀挀漀瀀攀⤀怀 眀栀攀爀攀㨀ഀഀ
 *਍ ⨀      ⨀ 怀挀氀漀渀攀搀䔀氀攀洀攀渀琀怀 ⴀ 椀猀 愀 挀氀漀渀攀 漀昀 琀栀攀 漀爀椀最椀渀愀氀 怀攀氀攀洀攀渀琀怀 瀀愀猀猀攀搀 椀渀琀漀 琀栀攀 挀漀洀瀀椀氀攀爀⸀ഀഀ
 *      * `scope` - is the current scope with which the linking function is working with.਍ ⨀ഀഀ
 * Calling the linking function returns the element of the template. It is either the original਍ ⨀ 攀氀攀洀攀渀琀 瀀愀猀猀攀搀 椀渀Ⰰ 漀爀 琀栀攀 挀氀漀渀攀 漀昀 琀栀攀 攀氀攀洀攀渀琀 椀昀 琀栀攀 怀挀氀漀渀攀䄀琀琀愀挀栀䘀渀怀 椀猀 瀀爀漀瘀椀搀攀搀⸀ഀഀ
 *਍ ⨀ 䄀昀琀攀爀 氀椀渀欀椀渀最 琀栀攀 瘀椀攀眀 椀猀 渀漀琀 甀瀀搀愀琀攀搀 甀渀琀椀氀 愀昀琀攀爀 愀 挀愀氀氀 琀漀 ␀搀椀最攀猀琀 眀栀椀挀栀 琀礀瀀椀挀愀氀氀礀 椀猀 搀漀渀攀 戀礀ഀഀ
 * Angular automatically.਍ ⨀ഀഀ
 * If you need access to the bound view, there are two ways to do it:਍ ⨀ഀഀ
 * - If you are not asking the linking function to clone the template, create the DOM element(s)਍ ⨀   戀攀昀漀爀攀 礀漀甀 猀攀渀搀 琀栀攀洀 琀漀 琀栀攀 挀漀洀瀀椀氀攀爀 愀渀搀 欀攀攀瀀 琀栀椀猀 爀攀昀攀爀攀渀挀攀 愀爀漀甀渀搀⸀ഀഀ
 *   <pre>਍ ⨀     瘀愀爀 攀氀攀洀攀渀琀 㴀 ␀挀漀洀瀀椀氀攀⠀✀㰀瀀㸀笀笀琀漀琀愀氀紀紀㰀⼀瀀㸀✀⤀⠀猀挀漀瀀攀⤀㬀ഀഀ
 *   </pre>਍ ⨀ഀഀ
 * - if on the other hand, you need the element to be cloned, the view reference from the original਍ ⨀   攀砀愀洀瀀氀攀 眀漀甀氀搀 渀漀琀 瀀漀椀渀琀 琀漀 琀栀攀 挀氀漀渀攀Ⰰ 戀甀琀 爀愀琀栀攀爀 琀漀 琀栀攀 漀爀椀最椀渀愀氀 琀攀洀瀀氀愀琀攀 琀栀愀琀 眀愀猀 挀氀漀渀攀搀⸀ 䤀渀ഀഀ
 *   this case, you can access the clone via the cloneAttachFn:਍ ⨀   㰀瀀爀攀㸀ഀഀ
 *     var templateElement = angular.element('<p>{{total}}</p>'),਍ ⨀         猀挀漀瀀攀 㴀 ⸀⸀⸀⸀㬀ഀഀ
 *਍ ⨀     瘀愀爀 挀氀漀渀攀搀䔀氀攀洀攀渀琀 㴀 ␀挀漀洀瀀椀氀攀⠀琀攀洀瀀氀愀琀攀䔀氀攀洀攀渀琀⤀⠀猀挀漀瀀攀Ⰰ 昀甀渀挀琀椀漀渀⠀挀氀漀渀攀搀䔀氀攀洀攀渀琀Ⰰ 猀挀漀瀀攀⤀ 笀ഀഀ
 *       //attach the clone to DOM document at the right place਍ ⨀     紀⤀㬀ഀഀ
 *਍ ⨀     ⼀⼀渀漀眀 眀攀 栀愀瘀攀 爀攀昀攀爀攀渀挀攀 琀漀 琀栀攀 挀氀漀渀攀搀 䐀伀䴀 瘀椀愀 怀挀氀漀渀攀搀䔀氀攀洀攀渀琀怀ഀഀ
 *   </pre>਍ ⨀ഀഀ
 *਍ ⨀ 䘀漀爀 椀渀昀漀爀洀愀琀椀漀渀 漀渀 栀漀眀 琀栀攀 挀漀洀瀀椀氀攀爀 眀漀爀欀猀Ⰰ 猀攀攀 琀栀攀ഀഀ
 * {@link guide/compiler Angular HTML Compiler} section of the Developer Guide.਍ ⨀⼀ഀഀ
਍瘀愀爀 ␀挀漀洀瀀椀氀攀䴀椀渀䔀爀爀 㴀 洀椀渀䔀爀爀⠀✀␀挀漀洀瀀椀氀攀✀⤀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc service਍ ⨀ 䀀渀愀洀攀 渀最⸀␀挀漀洀瀀椀氀攀倀爀漀瘀椀搀攀爀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀⼀ഀഀ
$CompileProvider.$inject = ['$provide', '$$sanitizeUriProvider'];਍昀甀渀挀琀椀漀渀 ␀䌀漀洀瀀椀氀攀倀爀漀瘀椀搀攀爀⠀␀瀀爀漀瘀椀搀攀Ⰰ ␀␀猀愀渀椀琀椀稀攀唀爀椀倀爀漀瘀椀搀攀爀⤀ 笀ഀഀ
  var hasDirectives = {},਍      匀甀昀昀椀砀 㴀 ✀䐀椀爀攀挀琀椀瘀攀✀Ⰰഀഀ
      COMMENT_DIRECTIVE_REGEXP = /^\s*directive\:\s*([\d\w\-_]+)\s+(.*)$/,਍      䌀䰀䄀匀匀开䐀䤀刀䔀䌀吀䤀嘀䔀开刀䔀䜀䔀堀倀 㴀 ⼀⠀⠀嬀尀搀尀眀尀ⴀ开崀⬀⤀⠀㼀㨀尀㨀⠀嬀帀㬀崀⬀⤀⤀㼀㬀㼀⤀⼀㬀ഀഀ
਍  ⼀⼀ 刀攀昀㨀 栀琀琀瀀㨀⼀⼀搀攀瘀攀氀漀瀀攀爀猀⸀眀栀愀琀眀最⸀漀爀最⼀眀攀戀愀瀀瀀愀瀀椀猀⸀栀琀洀氀⌀攀瘀攀渀琀ⴀ栀愀渀搀氀攀爀ⴀ椀搀氀ⴀ愀琀琀爀椀戀甀琀攀猀ഀഀ
  // The assumption is that future DOM event attribute names will begin with਍  ⼀⼀ ✀漀渀✀ 愀渀搀 戀攀 挀漀洀瀀漀猀攀搀 漀昀 漀渀氀礀 䔀渀最氀椀猀栀 氀攀琀琀攀爀猀⸀ഀഀ
  var EVENT_HANDLER_ATTR_REGEXP = /^(on[a-z]+|formaction)$/;਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
   * @name ng.$compileProvider#directive਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀挀漀洀瀀椀氀攀倀爀漀瘀椀搀攀爀ഀഀ
   * @function਍   ⨀ഀഀ
   * @description਍   ⨀ 刀攀最椀猀琀攀爀 愀 渀攀眀 搀椀爀攀挀琀椀瘀攀 眀椀琀栀 琀栀攀 挀漀洀瀀椀氀攀爀⸀ഀഀ
   *਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最簀伀戀樀攀挀琀紀 渀愀洀攀 一愀洀攀 漀昀 琀栀攀 搀椀爀攀挀琀椀瘀攀 椀渀 挀愀洀攀氀ⴀ挀愀猀攀 ⠀椀⸀攀⸀ 㰀挀漀搀攀㸀渀最䈀椀渀搀㰀⼀挀漀搀攀㸀 眀栀椀挀栀ഀഀ
   *    will match as <code>ng-bind</code>), or an object map of directives where the keys are the਍   ⨀    渀愀洀攀猀 愀渀搀 琀栀攀 瘀愀氀甀攀猀 愀爀攀 琀栀攀 昀愀挀琀漀爀椀攀猀⸀ഀഀ
   * @param {function|Array} directiveFactory An injectable directive factory function. See਍   ⨀    笀䀀氀椀渀欀 最甀椀搀攀⼀搀椀爀攀挀琀椀瘀攀紀 昀漀爀 洀漀爀攀 椀渀昀漀⸀ഀഀ
   * @returns {ng.$compileProvider} Self for chaining.਍   ⨀⼀ഀഀ
   this.directive = function registerDirective(name, directiveFactory) {਍    愀猀猀攀爀琀一漀琀䠀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀渀愀洀攀Ⰰ ✀搀椀爀攀挀琀椀瘀攀✀⤀㬀ഀഀ
    if (isString(name)) {਍      愀猀猀攀爀琀䄀爀最⠀搀椀爀攀挀琀椀瘀攀䘀愀挀琀漀爀礀Ⰰ ✀搀椀爀攀挀琀椀瘀攀䘀愀挀琀漀爀礀✀⤀㬀ഀഀ
      if (!hasDirectives.hasOwnProperty(name)) {਍        栀愀猀䐀椀爀攀挀琀椀瘀攀猀嬀渀愀洀攀崀 㴀 嬀崀㬀ഀഀ
        $provide.factory(name + Suffix, ['$injector', '$exceptionHandler',਍          昀甀渀挀琀椀漀渀⠀␀椀渀樀攀挀琀漀爀Ⰰ ␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀⤀ 笀ഀഀ
            var directives = [];਍            昀漀爀䔀愀挀栀⠀栀愀猀䐀椀爀攀挀琀椀瘀攀猀嬀渀愀洀攀崀Ⰰ 昀甀渀挀琀椀漀渀⠀搀椀爀攀挀琀椀瘀攀䘀愀挀琀漀爀礀Ⰰ 椀渀搀攀砀⤀ 笀ഀഀ
              try {਍                瘀愀爀 搀椀爀攀挀琀椀瘀攀 㴀 ␀椀渀樀攀挀琀漀爀⸀椀渀瘀漀欀攀⠀搀椀爀攀挀琀椀瘀攀䘀愀挀琀漀爀礀⤀㬀ഀഀ
                if (isFunction(directive)) {਍                  搀椀爀攀挀琀椀瘀攀 㴀 笀 挀漀洀瀀椀氀攀㨀 瘀愀氀甀攀䘀渀⠀搀椀爀攀挀琀椀瘀攀⤀ 紀㬀ഀഀ
                } else if (!directive.compile && directive.link) {਍                  搀椀爀攀挀琀椀瘀攀⸀挀漀洀瀀椀氀攀 㴀 瘀愀氀甀攀䘀渀⠀搀椀爀攀挀琀椀瘀攀⸀氀椀渀欀⤀㬀ഀഀ
                }਍                搀椀爀攀挀琀椀瘀攀⸀瀀爀椀漀爀椀琀礀 㴀 搀椀爀攀挀琀椀瘀攀⸀瀀爀椀漀爀椀琀礀 簀簀 　㬀ഀഀ
                directive.index = index;਍                搀椀爀攀挀琀椀瘀攀⸀渀愀洀攀 㴀 搀椀爀攀挀琀椀瘀攀⸀渀愀洀攀 簀簀 渀愀洀攀㬀ഀഀ
                directive.require = directive.require || (directive.controller && directive.name);਍                搀椀爀攀挀琀椀瘀攀⸀爀攀猀琀爀椀挀琀 㴀 搀椀爀攀挀琀椀瘀攀⸀爀攀猀琀爀椀挀琀 簀簀 ✀䄀✀㬀ഀഀ
                directives.push(directive);਍              紀 挀愀琀挀栀 ⠀攀⤀ 笀ഀഀ
                $exceptionHandler(e);਍              紀ഀഀ
            });਍            爀攀琀甀爀渀 搀椀爀攀挀琀椀瘀攀猀㬀ഀഀ
          }]);਍      紀ഀഀ
      hasDirectives[name].push(directiveFactory);਍    紀 攀氀猀攀 笀ഀഀ
      forEach(name, reverseParams(registerDirective));਍    紀ഀഀ
    return this;਍  紀㬀ഀഀ
਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
   * @name ng.$compileProvider#aHrefSanitizationWhitelist਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀挀漀洀瀀椀氀攀倀爀漀瘀椀搀攀爀ഀഀ
   * @function਍   ⨀ഀഀ
   * @description਍   ⨀ 刀攀琀爀椀攀瘀攀猀 漀爀 漀瘀攀爀爀椀搀攀猀 琀栀攀 搀攀昀愀甀氀琀 爀攀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 琀栀愀琀 椀猀 甀猀攀搀 昀漀爀 眀栀椀琀攀氀椀猀琀椀渀最 漀昀 猀愀昀攀ഀഀ
   * urls during a[href] sanitization.਍   ⨀ഀഀ
   * The sanitization is a security measure aimed at prevent XSS attacks via html links.਍   ⨀ഀഀ
   * Any url about to be assigned to a[href] via data-binding is first normalized and turned into਍   ⨀ 愀渀 愀戀猀漀氀甀琀攀 甀爀氀⸀ 䄀昀琀攀爀眀愀爀搀猀Ⰰ 琀栀攀 甀爀氀 椀猀 洀愀琀挀栀攀搀 愀最愀椀渀猀琀 琀栀攀 怀愀䠀爀攀昀匀愀渀椀琀椀稀愀琀椀漀渀圀栀椀琀攀氀椀猀琀怀ഀഀ
   * regular expression. If a match is found, the original url is written into the dom. Otherwise,਍   ⨀ 琀栀攀 愀戀猀漀氀甀琀攀 甀爀氀 椀猀 瀀爀攀昀椀砀攀搀 眀椀琀栀 怀✀甀渀猀愀昀攀㨀✀怀 猀琀爀椀渀最 愀渀搀 漀渀氀礀 琀栀攀渀 椀猀 椀琀 眀爀椀琀琀攀渀 椀渀琀漀 琀栀攀 䐀伀䴀⸀ഀഀ
   *਍   ⨀ 䀀瀀愀爀愀洀 笀刀攀最䔀砀瀀㴀紀 爀攀最攀砀瀀 一攀眀 爀攀最攀砀瀀 琀漀 眀栀椀琀攀氀椀猀琀 甀爀氀猀 眀椀琀栀⸀ഀഀ
   * @returns {RegExp|ng.$compileProvider} Current RegExp if called without value or self for਍   ⨀    挀栀愀椀渀椀渀最 漀琀栀攀爀眀椀猀攀⸀ഀഀ
   */਍  琀栀椀猀⸀愀䠀爀攀昀匀愀渀椀琀椀稀愀琀椀漀渀圀栀椀琀攀氀椀猀琀 㴀 昀甀渀挀琀椀漀渀⠀爀攀最攀砀瀀⤀ 笀ഀഀ
    if (isDefined(regexp)) {਍      ␀␀猀愀渀椀琀椀稀攀唀爀椀倀爀漀瘀椀搀攀爀⸀愀䠀爀攀昀匀愀渀椀琀椀稀愀琀椀漀渀圀栀椀琀攀氀椀猀琀⠀爀攀最攀砀瀀⤀㬀ഀഀ
      return this;਍    紀 攀氀猀攀 笀ഀഀ
      return $$sanitizeUriProvider.aHrefSanitizationWhitelist();਍    紀ഀഀ
  };਍ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc function਍   ⨀ 䀀渀愀洀攀 渀最⸀␀挀漀洀瀀椀氀攀倀爀漀瘀椀搀攀爀⌀椀洀最匀爀挀匀愀渀椀琀椀稀愀琀椀漀渀圀栀椀琀攀氀椀猀琀ഀഀ
   * @methodOf ng.$compileProvider਍   ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Retrieves or overrides the default regular expression that is used for whitelisting of safe਍   ⨀ 甀爀氀猀 搀甀爀椀渀最 椀洀最嬀猀爀挀崀 猀愀渀椀琀椀稀愀琀椀漀渀⸀ഀഀ
   *਍   ⨀ 吀栀攀 猀愀渀椀琀椀稀愀琀椀漀渀 椀猀 愀 猀攀挀甀爀椀琀礀 洀攀愀猀甀爀攀 愀椀洀攀搀 愀琀 瀀爀攀瘀攀渀琀 堀匀匀 愀琀琀愀挀欀猀 瘀椀愀 栀琀洀氀 氀椀渀欀猀⸀ഀഀ
   *਍   ⨀ 䄀渀礀 甀爀氀 愀戀漀甀琀 琀漀 戀攀 愀猀猀椀最渀攀搀 琀漀 椀洀最嬀猀爀挀崀 瘀椀愀 搀愀琀愀ⴀ戀椀渀搀椀渀最 椀猀 昀椀爀猀琀 渀漀爀洀愀氀椀稀攀搀 愀渀搀 琀甀爀渀攀搀 椀渀琀漀ഀഀ
   * an absolute url. Afterwards, the url is matched against the `imgSrcSanitizationWhitelist`਍   ⨀ 爀攀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀⸀ 䤀昀 愀 洀愀琀挀栀 椀猀 昀漀甀渀搀Ⰰ 琀栀攀 漀爀椀最椀渀愀氀 甀爀氀 椀猀 眀爀椀琀琀攀渀 椀渀琀漀 琀栀攀 搀漀洀⸀ 伀琀栀攀爀眀椀猀攀Ⰰഀഀ
   * the absolute url is prefixed with `'unsafe:'` string and only then is it written into the DOM.਍   ⨀ഀഀ
   * @param {RegExp=} regexp New regexp to whitelist urls with.਍   ⨀ 䀀爀攀琀甀爀渀猀 笀刀攀最䔀砀瀀簀渀最⸀␀挀漀洀瀀椀氀攀倀爀漀瘀椀搀攀爀紀 䌀甀爀爀攀渀琀 刀攀最䔀砀瀀 椀昀 挀愀氀氀攀搀 眀椀琀栀漀甀琀 瘀愀氀甀攀 漀爀 猀攀氀昀 昀漀爀ഀഀ
   *    chaining otherwise.਍   ⨀⼀ഀഀ
  this.imgSrcSanitizationWhitelist = function(regexp) {਍    椀昀 ⠀椀猀䐀攀昀椀渀攀搀⠀爀攀最攀砀瀀⤀⤀ 笀ഀഀ
      $$sanitizeUriProvider.imgSrcSanitizationWhitelist(regexp);਍      爀攀琀甀爀渀 琀栀椀猀㬀ഀഀ
    } else {਍      爀攀琀甀爀渀 ␀␀猀愀渀椀琀椀稀攀唀爀椀倀爀漀瘀椀搀攀爀⸀椀洀最匀爀挀匀愀渀椀琀椀稀愀琀椀漀渀圀栀椀琀攀氀椀猀琀⠀⤀㬀ഀഀ
    }਍  紀㬀ഀഀ
਍  琀栀椀猀⸀␀最攀琀 㴀 嬀ഀഀ
            '$injector', '$interpolate', '$exceptionHandler', '$http', '$templateCache', '$parse',਍            ✀␀挀漀渀琀爀漀氀氀攀爀✀Ⰰ ✀␀爀漀漀琀匀挀漀瀀攀✀Ⰰ ✀␀搀漀挀甀洀攀渀琀✀Ⰰ ✀␀猀挀攀✀Ⰰ ✀␀愀渀椀洀愀琀攀✀Ⰰ ✀␀␀猀愀渀椀琀椀稀攀唀爀椀✀Ⰰഀഀ
    function($injector,   $interpolate,   $exceptionHandler,   $http,   $templateCache,   $parse,਍             ␀挀漀渀琀爀漀氀氀攀爀Ⰰ   ␀爀漀漀琀匀挀漀瀀攀Ⰰ   ␀搀漀挀甀洀攀渀琀Ⰰ   ␀猀挀攀Ⰰ   ␀愀渀椀洀愀琀攀Ⰰ   ␀␀猀愀渀椀琀椀稀攀唀爀椀⤀ 笀ഀഀ
਍    瘀愀爀 䄀琀琀爀椀戀甀琀攀猀 㴀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 愀琀琀爀⤀ 笀ഀഀ
      this.$$element = element;਍      琀栀椀猀⸀␀愀琀琀爀 㴀 愀琀琀爀 簀簀 笀紀㬀ഀഀ
    };਍ഀഀ
    Attributes.prototype = {਍      ␀渀漀爀洀愀氀椀稀攀㨀 搀椀爀攀挀琀椀瘀攀一漀爀洀愀氀椀稀攀Ⰰഀഀ
਍ഀഀ
      /**਍       ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
       * @name ng.$compile.directive.Attributes#$addClass਍       ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀挀漀洀瀀椀氀攀⸀搀椀爀攀挀琀椀瘀攀⸀䄀琀琀爀椀戀甀琀攀猀ഀഀ
       * @function਍       ⨀ഀഀ
       * @description਍       ⨀ 䄀搀搀猀 琀栀攀 䌀匀匀 挀氀愀猀猀 瘀愀氀甀攀 猀瀀攀挀椀昀椀攀搀 戀礀 琀栀攀 挀氀愀猀猀嘀愀氀 瀀愀爀愀洀攀琀攀爀 琀漀 琀栀攀 攀氀攀洀攀渀琀⸀ 䤀昀 愀渀椀洀愀琀椀漀渀猀ഀഀ
       * are enabled then an animation will be triggered for the class addition.਍       ⨀ഀഀ
       * @param {string} classVal The className value that will be added to the element਍       ⨀⼀ഀഀ
      $addClass : function(classVal) {਍        椀昀⠀挀氀愀猀猀嘀愀氀 ☀☀ 挀氀愀猀猀嘀愀氀⸀氀攀渀最琀栀 㸀 　⤀ 笀ഀഀ
          $animate.addClass(this.$$element, classVal);਍        紀ഀഀ
      },਍ഀഀ
      /**਍       ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
       * @name ng.$compile.directive.Attributes#$removeClass਍       ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀挀漀洀瀀椀氀攀⸀搀椀爀攀挀琀椀瘀攀⸀䄀琀琀爀椀戀甀琀攀猀ഀഀ
       * @function਍       ⨀ഀഀ
       * @description਍       ⨀ 刀攀洀漀瘀攀猀 琀栀攀 䌀匀匀 挀氀愀猀猀 瘀愀氀甀攀 猀瀀攀挀椀昀椀攀搀 戀礀 琀栀攀 挀氀愀猀猀嘀愀氀 瀀愀爀愀洀攀琀攀爀 昀爀漀洀 琀栀攀 攀氀攀洀攀渀琀⸀ 䤀昀ഀഀ
       * animations are enabled then an animation will be triggered for the class removal.਍       ⨀ഀഀ
       * @param {string} classVal The className value that will be removed from the element਍       ⨀⼀ഀഀ
      $removeClass : function(classVal) {਍        椀昀⠀挀氀愀猀猀嘀愀氀 ☀☀ 挀氀愀猀猀嘀愀氀⸀氀攀渀最琀栀 㸀 　⤀ 笀ഀഀ
          $animate.removeClass(this.$$element, classVal);਍        紀ഀഀ
      },਍ഀഀ
      /**਍       ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
       * @name ng.$compile.directive.Attributes#$updateClass਍       ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀挀漀洀瀀椀氀攀⸀搀椀爀攀挀琀椀瘀攀⸀䄀琀琀爀椀戀甀琀攀猀ഀഀ
       * @function਍       ⨀ഀഀ
       * @description਍       ⨀ 䄀搀搀猀 愀渀搀 爀攀洀漀瘀攀猀 琀栀攀 愀瀀瀀爀漀瀀爀椀愀琀攀 䌀匀匀 挀氀愀猀猀 瘀愀氀甀攀猀 琀漀 琀栀攀 攀氀攀洀攀渀琀 戀愀猀攀搀 漀渀 琀栀攀 搀椀昀昀攀爀攀渀挀攀ഀഀ
       * between the new and old CSS class values (specified as newClasses and oldClasses).਍       ⨀ഀഀ
       * @param {string} newClasses The current CSS className value਍       ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 漀氀搀䌀氀愀猀猀攀猀 吀栀攀 昀漀爀洀攀爀 䌀匀匀 挀氀愀猀猀一愀洀攀 瘀愀氀甀攀ഀഀ
       */਍      ␀甀瀀搀愀琀攀䌀氀愀猀猀 㨀 昀甀渀挀琀椀漀渀⠀渀攀眀䌀氀愀猀猀攀猀Ⰰ 漀氀搀䌀氀愀猀猀攀猀⤀ 笀ഀഀ
        this.$removeClass(tokenDifference(oldClasses, newClasses));਍        琀栀椀猀⸀␀愀搀搀䌀氀愀猀猀⠀琀漀欀攀渀䐀椀昀昀攀爀攀渀挀攀⠀渀攀眀䌀氀愀猀猀攀猀Ⰰ 漀氀搀䌀氀愀猀猀攀猀⤀⤀㬀ഀഀ
      },਍ഀഀ
      /**਍       ⨀ 匀攀琀 愀 渀漀爀洀愀氀椀稀攀搀 愀琀琀爀椀戀甀琀攀 漀渀 琀栀攀 攀氀攀洀攀渀琀 椀渀 愀 眀愀礀 猀甀挀栀 琀栀愀琀 愀氀氀 搀椀爀攀挀琀椀瘀攀猀ഀഀ
       * can share the attribute. This function properly handles boolean attributes.਍       ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 欀攀礀 一漀爀洀愀氀椀稀攀搀 欀攀礀⸀ ⠀椀攀 渀最䄀琀琀爀椀戀甀琀攀⤀ഀഀ
       * @param {string|boolean} value The value to set. If `null` attribute will be deleted.਍       ⨀ 䀀瀀愀爀愀洀 笀戀漀漀氀攀愀渀㴀紀 眀爀椀琀攀䄀琀琀爀 䤀昀 昀愀氀猀攀Ⰰ 搀漀攀猀 渀漀琀 眀爀椀琀攀 琀栀攀 瘀愀氀甀攀 琀漀 䐀伀䴀 攀氀攀洀攀渀琀 愀琀琀爀椀戀甀琀攀⸀ഀഀ
       *     Defaults to true.਍       ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 愀琀琀爀一愀洀攀 伀瀀琀椀漀渀愀氀 渀漀渀攀 渀漀爀洀愀氀椀稀攀搀 渀愀洀攀⸀ 䐀攀昀愀甀氀琀猀 琀漀 欀攀礀⸀ഀഀ
       */਍      ␀猀攀琀㨀 昀甀渀挀琀椀漀渀⠀欀攀礀Ⰰ 瘀愀氀甀攀Ⰰ 眀爀椀琀攀䄀琀琀爀Ⰰ 愀琀琀爀一愀洀攀⤀ 笀ഀഀ
        // TODO: decide whether or not to throw an error if "class"਍        ⼀⼀椀猀 猀攀琀 琀栀爀漀甀最栀 琀栀椀猀 昀甀渀挀琀椀漀渀 猀椀渀挀攀 椀琀 洀愀礀 挀愀甀猀攀 ␀甀瀀搀愀琀攀䌀氀愀猀猀 琀漀ഀഀ
        //become unstable.਍ഀഀ
        var booleanKey = getBooleanAttrName(this.$$element[0], key),਍            渀漀爀洀愀氀椀稀攀搀嘀愀氀Ⰰഀഀ
            nodeName;਍ഀഀ
        if (booleanKey) {਍          琀栀椀猀⸀␀␀攀氀攀洀攀渀琀⸀瀀爀漀瀀⠀欀攀礀Ⰰ 瘀愀氀甀攀⤀㬀ഀഀ
          attrName = booleanKey;਍        紀ഀഀ
਍        琀栀椀猀嬀欀攀礀崀 㴀 瘀愀氀甀攀㬀ഀഀ
਍        ⼀⼀ 琀爀愀渀猀氀愀琀攀 渀漀爀洀愀氀椀稀攀搀 欀攀礀 琀漀 愀挀琀甀愀氀 欀攀礀ഀഀ
        if (attrName) {਍          琀栀椀猀⸀␀愀琀琀爀嬀欀攀礀崀 㴀 愀琀琀爀一愀洀攀㬀ഀഀ
        } else {਍          愀琀琀爀一愀洀攀 㴀 琀栀椀猀⸀␀愀琀琀爀嬀欀攀礀崀㬀ഀഀ
          if (!attrName) {਍            琀栀椀猀⸀␀愀琀琀爀嬀欀攀礀崀 㴀 愀琀琀爀一愀洀攀 㴀 猀渀愀欀攀开挀愀猀攀⠀欀攀礀Ⰰ ✀ⴀ✀⤀㬀ഀഀ
          }਍        紀ഀഀ
਍        渀漀搀攀一愀洀攀 㴀 渀漀搀攀一愀洀攀开⠀琀栀椀猀⸀␀␀攀氀攀洀攀渀琀⤀㬀ഀഀ
਍        ⼀⼀ 猀愀渀椀琀椀稀攀 愀嬀栀爀攀昀崀 愀渀搀 椀洀最嬀猀爀挀崀 瘀愀氀甀攀猀ഀഀ
        if ((nodeName === 'A' && key === 'href') ||਍            ⠀渀漀搀攀一愀洀攀 㴀㴀㴀 ✀䤀䴀䜀✀ ☀☀ 欀攀礀 㴀㴀㴀 ✀猀爀挀✀⤀⤀ 笀ഀഀ
          this[key] = value = $$sanitizeUri(value, key === 'src');਍        紀ഀഀ
਍        椀昀 ⠀眀爀椀琀攀䄀琀琀爀 ℀㴀㴀 昀愀氀猀攀⤀ 笀ഀഀ
          if (value === null || value === undefined) {਍            琀栀椀猀⸀␀␀攀氀攀洀攀渀琀⸀爀攀洀漀瘀攀䄀琀琀爀⠀愀琀琀爀一愀洀攀⤀㬀ഀഀ
          } else {਍            琀栀椀猀⸀␀␀攀氀攀洀攀渀琀⸀愀琀琀爀⠀愀琀琀爀一愀洀攀Ⰰ 瘀愀氀甀攀⤀㬀ഀഀ
          }਍        紀ഀഀ
਍        ⼀⼀ 昀椀爀攀 漀戀猀攀爀瘀攀爀猀ഀഀ
        var $$observers = this.$$observers;਍        ␀␀漀戀猀攀爀瘀攀爀猀 ☀☀ 昀漀爀䔀愀挀栀⠀␀␀漀戀猀攀爀瘀攀爀猀嬀欀攀礀崀Ⰰ 昀甀渀挀琀椀漀渀⠀昀渀⤀ 笀ഀഀ
          try {਍            昀渀⠀瘀愀氀甀攀⤀㬀ഀഀ
          } catch (e) {਍            ␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀⠀攀⤀㬀ഀഀ
          }਍        紀⤀㬀ഀഀ
      },਍ഀഀ
਍      ⼀⨀⨀ഀഀ
       * @ngdoc function਍       ⨀ 䀀渀愀洀攀 渀最⸀␀挀漀洀瀀椀氀攀⸀搀椀爀攀挀琀椀瘀攀⸀䄀琀琀爀椀戀甀琀攀猀⌀␀漀戀猀攀爀瘀攀ഀഀ
       * @methodOf ng.$compile.directive.Attributes਍       ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
       *਍       ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
       * Observes an interpolated attribute.਍       ⨀ഀഀ
       * The observer function will be invoked once during the next `$digest` following਍       ⨀ 挀漀洀瀀椀氀愀琀椀漀渀⸀ 吀栀攀 漀戀猀攀爀瘀攀爀 椀猀 琀栀攀渀 椀渀瘀漀欀攀搀 眀栀攀渀攀瘀攀爀 琀栀攀 椀渀琀攀爀瀀漀氀愀琀攀搀 瘀愀氀甀攀ഀഀ
       * changes.਍       ⨀ഀഀ
       * @param {string} key Normalized key. (ie ngAttribute) .਍       ⨀ 䀀瀀愀爀愀洀 笀昀甀渀挀琀椀漀渀⠀椀渀琀攀爀瀀漀氀愀琀攀搀嘀愀氀甀攀⤀紀 昀渀 䘀甀渀挀琀椀漀渀 琀栀愀琀 眀椀氀氀 戀攀 挀愀氀氀攀搀 眀栀攀渀攀瘀攀爀ഀഀ
                the interpolated value of the attribute changes.਍       ⨀        匀攀攀 琀栀攀 笀䀀氀椀渀欀 最甀椀搀攀⼀搀椀爀攀挀琀椀瘀攀⌀䄀琀琀爀椀戀甀琀攀猀 䐀椀爀攀挀琀椀瘀攀猀紀 最甀椀搀攀 昀漀爀 洀漀爀攀 椀渀昀漀⸀ഀഀ
       * @returns {function()} the `fn` parameter.਍       ⨀⼀ഀഀ
      $observe: function(key, fn) {਍        瘀愀爀 愀琀琀爀猀 㴀 琀栀椀猀Ⰰഀഀ
            $$observers = (attrs.$$observers || (attrs.$$observers = {})),਍            氀椀猀琀攀渀攀爀猀 㴀 ⠀␀␀漀戀猀攀爀瘀攀爀猀嬀欀攀礀崀 簀簀 ⠀␀␀漀戀猀攀爀瘀攀爀猀嬀欀攀礀崀 㴀 嬀崀⤀⤀㬀ഀഀ
਍        氀椀猀琀攀渀攀爀猀⸀瀀甀猀栀⠀昀渀⤀㬀ഀഀ
        $rootScope.$evalAsync(function() {਍          椀昀 ⠀℀氀椀猀琀攀渀攀爀猀⸀␀␀椀渀琀攀爀⤀ 笀ഀഀ
            // no one registered attribute interpolation function, so lets call it manually਍            昀渀⠀愀琀琀爀猀嬀欀攀礀崀⤀㬀ഀഀ
          }਍        紀⤀㬀ഀഀ
        return fn;਍      紀ഀഀ
    };਍ഀഀ
    var startSymbol = $interpolate.startSymbol(),਍        攀渀搀匀礀洀戀漀氀 㴀 ␀椀渀琀攀爀瀀漀氀愀琀攀⸀攀渀搀匀礀洀戀漀氀⠀⤀Ⰰഀഀ
        denormalizeTemplate = (startSymbol == '{{' || endSymbol  == '}}')਍            㼀 椀搀攀渀琀椀琀礀ഀഀ
            : function denormalizeTemplate(template) {਍              爀攀琀甀爀渀 琀攀洀瀀氀愀琀攀⸀爀攀瀀氀愀挀攀⠀⼀尀笀尀笀⼀最Ⰰ 猀琀愀爀琀匀礀洀戀漀氀⤀⸀爀攀瀀氀愀挀攀⠀⼀紀紀⼀最Ⰰ 攀渀搀匀礀洀戀漀氀⤀㬀ഀഀ
        },਍        一䜀开䄀吀吀刀开䈀䤀一䐀䤀一䜀 㴀 ⼀帀渀最䄀琀琀爀嬀䄀ⴀ娀崀⼀㬀ഀഀ
਍ഀഀ
    return compile;਍ഀഀ
    //================================਍ഀഀ
    function compile($compileNodes, transcludeFn, maxPriority, ignoreDirective,਍                        瀀爀攀瘀椀漀甀猀䌀漀洀瀀椀氀攀䌀漀渀琀攀砀琀⤀ 笀ഀഀ
      if (!($compileNodes instanceof jqLite)) {਍        ⼀⼀ 樀焀甀攀爀礀 愀氀眀愀礀猀 爀攀眀爀愀瀀猀Ⰰ 眀栀攀爀攀愀猀 眀攀 渀攀攀搀 琀漀 瀀爀攀猀攀爀瘀攀 琀栀攀 漀爀椀最椀渀愀氀 猀攀氀攀挀琀漀爀 猀漀 琀栀愀琀 眀攀 挀愀渀ഀഀ
        // modify it.਍        ␀挀漀洀瀀椀氀攀一漀搀攀猀 㴀 樀焀䰀椀琀攀⠀␀挀漀洀瀀椀氀攀一漀搀攀猀⤀㬀ഀഀ
      }਍      ⼀⼀ 圀攀 挀愀渀 渀漀琀 挀漀洀瀀椀氀攀 琀漀瀀 氀攀瘀攀氀 琀攀砀琀 攀氀攀洀攀渀琀猀 猀椀渀挀攀 琀攀砀琀 渀漀搀攀猀 挀愀渀 戀攀 洀攀爀最攀搀 愀渀搀 眀攀 眀椀氀氀ഀഀ
      // not be able to attach scope data to them, so we will wrap them in <span>਍      昀漀爀䔀愀挀栀⠀␀挀漀洀瀀椀氀攀一漀搀攀猀Ⰰ 昀甀渀挀琀椀漀渀⠀渀漀搀攀Ⰰ 椀渀搀攀砀⤀笀ഀഀ
        if (node.nodeType == 3 /* text node */ && node.nodeValue.match(/\S+/) /* non-empty */ ) {਍          ␀挀漀洀瀀椀氀攀一漀搀攀猀嬀椀渀搀攀砀崀 㴀 渀漀搀攀 㴀 樀焀䰀椀琀攀⠀渀漀搀攀⤀⸀眀爀愀瀀⠀✀㰀猀瀀愀渀㸀㰀⼀猀瀀愀渀㸀✀⤀⸀瀀愀爀攀渀琀⠀⤀嬀　崀㬀ഀഀ
        }਍      紀⤀㬀ഀഀ
      var compositeLinkFn =਍              挀漀洀瀀椀氀攀一漀搀攀猀⠀␀挀漀洀瀀椀氀攀一漀搀攀猀Ⰰ 琀爀愀渀猀挀氀甀搀攀䘀渀Ⰰ ␀挀漀洀瀀椀氀攀一漀搀攀猀Ⰰഀഀ
                           maxPriority, ignoreDirective, previousCompileContext);਍      猀愀昀攀䄀搀搀䌀氀愀猀猀⠀␀挀漀洀瀀椀氀攀一漀搀攀猀Ⰰ ✀渀最ⴀ猀挀漀瀀攀✀⤀㬀ഀഀ
      return function publicLinkFn(scope, cloneConnectFn, transcludeControllers){਍        愀猀猀攀爀琀䄀爀最⠀猀挀漀瀀攀Ⰰ ✀猀挀漀瀀攀✀⤀㬀ഀഀ
        // important!!: we must call our jqLite.clone() since the jQuery one is trying to be smart਍        ⼀⼀ 愀渀搀 猀漀洀攀琀椀洀攀猀 挀栀愀渀最攀猀 琀栀攀 猀琀爀甀挀琀甀爀攀 漀昀 琀栀攀 䐀伀䴀⸀ഀഀ
        var $linkNode = cloneConnectFn਍          㼀 䨀儀䰀椀琀攀倀爀漀琀漀琀礀瀀攀⸀挀氀漀渀攀⸀挀愀氀氀⠀␀挀漀洀瀀椀氀攀一漀搀攀猀⤀ ⼀⼀ 䤀䴀倀伀刀吀䄀一吀℀℀℀ഀഀ
          : $compileNodes;਍ഀഀ
        forEach(transcludeControllers, function(instance, name) {਍          ␀氀椀渀欀一漀搀攀⸀搀愀琀愀⠀✀␀✀ ⬀ 渀愀洀攀 ⬀ ✀䌀漀渀琀爀漀氀氀攀爀✀Ⰰ 椀渀猀琀愀渀挀攀⤀㬀ഀഀ
        });਍ഀഀ
        // Attach scope only to non-text nodes.਍        昀漀爀⠀瘀愀爀 椀 㴀 　Ⰰ 椀椀 㴀 ␀氀椀渀欀一漀搀攀⸀氀攀渀最琀栀㬀 椀㰀椀椀㬀 椀⬀⬀⤀ 笀ഀഀ
          var node = $linkNode[i],਍              渀漀搀攀吀礀瀀攀 㴀 渀漀搀攀⸀渀漀搀攀吀礀瀀攀㬀ഀഀ
          if (nodeType === 1 /* element */ || nodeType === 9 /* document */) {਍            ␀氀椀渀欀一漀搀攀⸀攀焀⠀椀⤀⸀搀愀琀愀⠀✀␀猀挀漀瀀攀✀Ⰰ 猀挀漀瀀攀⤀㬀ഀഀ
          }਍        紀ഀഀ
਍        椀昀 ⠀挀氀漀渀攀䌀漀渀渀攀挀琀䘀渀⤀ 挀氀漀渀攀䌀漀渀渀攀挀琀䘀渀⠀␀氀椀渀欀一漀搀攀Ⰰ 猀挀漀瀀攀⤀㬀ഀഀ
        if (compositeLinkFn) compositeLinkFn(scope, $linkNode, $linkNode);਍        爀攀琀甀爀渀 ␀氀椀渀欀一漀搀攀㬀ഀഀ
      };਍    紀ഀഀ
਍    昀甀渀挀琀椀漀渀 猀愀昀攀䄀搀搀䌀氀愀猀猀⠀␀攀氀攀洀攀渀琀Ⰰ 挀氀愀猀猀一愀洀攀⤀ 笀ഀഀ
      try {਍        ␀攀氀攀洀攀渀琀⸀愀搀搀䌀氀愀猀猀⠀挀氀愀猀猀一愀洀攀⤀㬀ഀഀ
      } catch(e) {਍        ⼀⼀ 椀最渀漀爀攀Ⰰ 猀椀渀挀攀 椀琀 洀攀愀渀猀 琀栀愀琀 眀攀 愀爀攀 琀爀礀椀渀最 琀漀 猀攀琀 挀氀愀猀猀 漀渀ഀഀ
        // SVG element, where class name is read-only.਍      紀ഀഀ
    }਍ഀഀ
    /**਍     ⨀ 䌀漀洀瀀椀氀攀 昀甀渀挀琀椀漀渀 洀愀琀挀栀攀猀 攀愀挀栀 渀漀搀攀 椀渀 渀漀搀攀䰀椀猀琀 愀最愀椀渀猀琀 琀栀攀 搀椀爀攀挀琀椀瘀攀猀⸀ 伀渀挀攀 愀氀氀 搀椀爀攀挀琀椀瘀攀猀ഀഀ
     * for a particular node are collected their compile functions are executed. The compile਍     ⨀ 昀甀渀挀琀椀漀渀猀 爀攀琀甀爀渀 瘀愀氀甀攀猀 ⴀ 琀栀攀 氀椀渀欀椀渀最 昀甀渀挀琀椀漀渀猀 ⴀ 愀爀攀 挀漀洀戀椀渀攀搀 椀渀琀漀 愀 挀漀洀瀀漀猀椀琀攀 氀椀渀欀椀渀最ഀഀ
     * function, which is the a linking function for the node.਍     ⨀ഀഀ
     * @param {NodeList} nodeList an array of nodes or NodeList to compile਍     ⨀ 䀀瀀愀爀愀洀 笀昀甀渀挀琀椀漀渀⠀愀渀最甀氀愀爀⸀匀挀漀瀀攀嬀Ⰰ 挀氀漀渀攀䄀琀琀愀挀栀䘀渀崀紀 琀爀愀渀猀挀氀甀搀攀䘀渀 䄀 氀椀渀欀椀渀最 昀甀渀挀琀椀漀渀Ⰰ 眀栀攀爀攀 琀栀攀ഀഀ
     *        scope argument is auto-generated to the new child of the transcluded parent scope.਍     ⨀ 䀀瀀愀爀愀洀 笀䐀伀䴀䔀氀攀洀攀渀琀㴀紀 ␀爀漀漀琀䔀氀攀洀攀渀琀 䤀昀 琀栀攀 渀漀搀攀䰀椀猀琀 椀猀 琀栀攀 爀漀漀琀 漀昀 琀栀攀 挀漀洀瀀椀氀愀琀椀漀渀 琀爀攀攀 琀栀攀渀ഀഀ
     *        the rootElement must be set the jqLite collection of the compile root. This is਍     ⨀        渀攀攀搀攀搀 猀漀 琀栀愀琀 琀栀攀 樀焀䰀椀琀攀 挀漀氀氀攀挀琀椀漀渀 椀琀攀洀猀 挀愀渀 戀攀 爀攀瀀氀愀挀攀搀 眀椀琀栀 眀椀搀最攀琀猀⸀ഀഀ
     * @param {number=} maxPriority Max directive priority.਍     ⨀ 䀀爀攀琀甀爀渀猀 笀㼀昀甀渀挀琀椀漀渀紀 䄀 挀漀洀瀀漀猀椀琀攀 氀椀渀欀椀渀最 昀甀渀挀琀椀漀渀 漀昀 愀氀氀 漀昀 琀栀攀 洀愀琀挀栀攀搀 搀椀爀攀挀琀椀瘀攀猀 漀爀 渀甀氀氀⸀ഀഀ
     */਍    昀甀渀挀琀椀漀渀 挀漀洀瀀椀氀攀一漀搀攀猀⠀渀漀搀攀䰀椀猀琀Ⰰ 琀爀愀渀猀挀氀甀搀攀䘀渀Ⰰ ␀爀漀漀琀䔀氀攀洀攀渀琀Ⰰ 洀愀砀倀爀椀漀爀椀琀礀Ⰰ 椀最渀漀爀攀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
                            previousCompileContext) {਍      瘀愀爀 氀椀渀欀䘀渀猀 㴀 嬀崀Ⰰഀഀ
          attrs, directives, nodeLinkFn, childNodes, childLinkFn, linkFnFound;਍ഀഀ
      for (var i = 0; i < nodeList.length; i++) {਍        愀琀琀爀猀 㴀 渀攀眀 䄀琀琀爀椀戀甀琀攀猀⠀⤀㬀ഀഀ
਍        ⼀⼀ 眀攀 洀甀猀琀 愀氀眀愀礀猀 爀攀昀攀爀 琀漀 渀漀搀攀䰀椀猀琀嬀椀崀 猀椀渀挀攀 琀栀攀 渀漀搀攀猀 挀愀渀 戀攀 爀攀瀀氀愀挀攀搀 甀渀搀攀爀渀攀愀琀栀 甀猀⸀ഀഀ
        directives = collectDirectives(nodeList[i], [], attrs, i === 0 ? maxPriority : undefined,਍                                        椀最渀漀爀攀䐀椀爀攀挀琀椀瘀攀⤀㬀ഀഀ
਍        渀漀搀攀䰀椀渀欀䘀渀 㴀 ⠀搀椀爀攀挀琀椀瘀攀猀⸀氀攀渀最琀栀⤀ഀഀ
            ? applyDirectivesToNode(directives, nodeList[i], attrs, transcludeFn, $rootElement,਍                                      渀甀氀氀Ⰰ 嬀崀Ⰰ 嬀崀Ⰰ 瀀爀攀瘀椀漀甀猀䌀漀洀瀀椀氀攀䌀漀渀琀攀砀琀⤀ഀഀ
            : null;਍ഀഀ
        if (nodeLinkFn && nodeLinkFn.scope) {਍          猀愀昀攀䄀搀搀䌀氀愀猀猀⠀樀焀䰀椀琀攀⠀渀漀搀攀䰀椀猀琀嬀椀崀⤀Ⰰ ✀渀最ⴀ猀挀漀瀀攀✀⤀㬀ഀഀ
        }਍ഀഀ
        childLinkFn = (nodeLinkFn && nodeLinkFn.terminal ||਍                      ℀⠀挀栀椀氀搀一漀搀攀猀 㴀 渀漀搀攀䰀椀猀琀嬀椀崀⸀挀栀椀氀搀一漀搀攀猀⤀ 簀簀ഀഀ
                      !childNodes.length)਍            㼀 渀甀氀氀ഀഀ
            : compileNodes(childNodes,਍                 渀漀搀攀䰀椀渀欀䘀渀 㼀 渀漀搀攀䰀椀渀欀䘀渀⸀琀爀愀渀猀挀氀甀搀攀 㨀 琀爀愀渀猀挀氀甀搀攀䘀渀⤀㬀ഀഀ
਍        氀椀渀欀䘀渀猀⸀瀀甀猀栀⠀渀漀搀攀䰀椀渀欀䘀渀Ⰰ 挀栀椀氀搀䰀椀渀欀䘀渀⤀㬀ഀഀ
        linkFnFound = linkFnFound || nodeLinkFn || childLinkFn;਍        ⼀⼀甀猀攀 琀栀攀 瀀爀攀瘀椀漀甀猀 挀漀渀琀攀砀琀 漀渀氀礀 昀漀爀 琀栀攀 昀椀爀猀琀 攀氀攀洀攀渀琀 椀渀 琀栀攀 瘀椀爀琀甀愀氀 最爀漀甀瀀ഀഀ
        previousCompileContext = null;਍      紀ഀഀ
਍      ⼀⼀ 爀攀琀甀爀渀 愀 氀椀渀欀椀渀最 昀甀渀挀琀椀漀渀 椀昀 眀攀 栀愀瘀攀 昀漀甀渀搀 愀渀礀琀栀椀渀最Ⰰ 渀甀氀氀 漀琀栀攀爀眀椀猀攀ഀഀ
      return linkFnFound ? compositeLinkFn : null;਍ഀഀ
      function compositeLinkFn(scope, nodeList, $rootElement, boundTranscludeFn) {਍        瘀愀爀 渀漀搀攀䰀椀渀欀䘀渀Ⰰ 挀栀椀氀搀䰀椀渀欀䘀渀Ⰰ 渀漀搀攀Ⰰ ␀渀漀搀攀Ⰰ 挀栀椀氀搀匀挀漀瀀攀Ⰰ 挀栀椀氀搀吀爀愀渀猀挀氀甀搀攀䘀渀Ⰰ 椀Ⰰ 椀椀Ⰰ 渀㬀ഀഀ
਍        ⼀⼀ 挀漀瀀礀 渀漀搀攀䰀椀猀琀 猀漀 琀栀愀琀 氀椀渀欀椀渀最 搀漀攀猀渀✀琀 戀爀攀愀欀 搀甀攀 琀漀 氀椀瘀攀 氀椀猀琀 甀瀀搀愀琀攀猀⸀ഀഀ
        var nodeListLength = nodeList.length,਍            猀琀愀戀氀攀一漀搀攀䰀椀猀琀 㴀 渀攀眀 䄀爀爀愀礀⠀渀漀搀攀䰀椀猀琀䰀攀渀最琀栀⤀㬀ഀഀ
        for (i = 0; i < nodeListLength; i++) {਍          猀琀愀戀氀攀一漀搀攀䰀椀猀琀嬀椀崀 㴀 渀漀搀攀䰀椀猀琀嬀椀崀㬀ഀഀ
        }਍ഀഀ
        for(i = 0, n = 0, ii = linkFns.length; i < ii; n++) {਍          渀漀搀攀 㴀 猀琀愀戀氀攀一漀搀攀䰀椀猀琀嬀渀崀㬀ഀഀ
          nodeLinkFn = linkFns[i++];਍          挀栀椀氀搀䰀椀渀欀䘀渀 㴀 氀椀渀欀䘀渀猀嬀椀⬀⬀崀㬀ഀഀ
          $node = jqLite(node);਍ഀഀ
          if (nodeLinkFn) {਍            椀昀 ⠀渀漀搀攀䰀椀渀欀䘀渀⸀猀挀漀瀀攀⤀ 笀ഀഀ
              childScope = scope.$new();਍              ␀渀漀搀攀⸀搀愀琀愀⠀✀␀猀挀漀瀀攀✀Ⰰ 挀栀椀氀搀匀挀漀瀀攀⤀㬀ഀഀ
            } else {਍              挀栀椀氀搀匀挀漀瀀攀 㴀 猀挀漀瀀攀㬀ഀഀ
            }਍            挀栀椀氀搀吀爀愀渀猀挀氀甀搀攀䘀渀 㴀 渀漀搀攀䰀椀渀欀䘀渀⸀琀爀愀渀猀挀氀甀搀攀㬀ഀഀ
            if (childTranscludeFn || (!boundTranscludeFn && transcludeFn)) {਍              渀漀搀攀䰀椀渀欀䘀渀⠀挀栀椀氀搀䰀椀渀欀䘀渀Ⰰ 挀栀椀氀搀匀挀漀瀀攀Ⰰ 渀漀搀攀Ⰰ ␀爀漀漀琀䔀氀攀洀攀渀琀Ⰰഀഀ
                createBoundTranscludeFn(scope, childTranscludeFn || transcludeFn)਍              ⤀㬀ഀഀ
            } else {਍              渀漀搀攀䰀椀渀欀䘀渀⠀挀栀椀氀搀䰀椀渀欀䘀渀Ⰰ 挀栀椀氀搀匀挀漀瀀攀Ⰰ 渀漀搀攀Ⰰ ␀爀漀漀琀䔀氀攀洀攀渀琀Ⰰ 戀漀甀渀搀吀爀愀渀猀挀氀甀搀攀䘀渀⤀㬀ഀഀ
            }਍          紀 攀氀猀攀 椀昀 ⠀挀栀椀氀搀䰀椀渀欀䘀渀⤀ 笀ഀഀ
            childLinkFn(scope, node.childNodes, undefined, boundTranscludeFn);਍          紀ഀഀ
        }਍      紀ഀഀ
    }਍ഀഀ
    function createBoundTranscludeFn(scope, transcludeFn) {਍      爀攀琀甀爀渀 昀甀渀挀琀椀漀渀 戀漀甀渀搀吀爀愀渀猀挀氀甀搀攀䘀渀⠀琀爀愀渀猀挀氀甀搀攀搀匀挀漀瀀攀Ⰰ 挀氀漀渀攀䘀渀Ⰰ 挀漀渀琀爀漀氀氀攀爀猀⤀ 笀ഀഀ
        var scopeCreated = false;਍ഀഀ
        if (!transcludedScope) {਍          琀爀愀渀猀挀氀甀搀攀搀匀挀漀瀀攀 㴀 猀挀漀瀀攀⸀␀渀攀眀⠀⤀㬀ഀഀ
          transcludedScope.$$transcluded = true;਍          猀挀漀瀀攀䌀爀攀愀琀攀搀 㴀 琀爀甀攀㬀ഀഀ
        }਍ഀഀ
        var clone = transcludeFn(transcludedScope, cloneFn, controllers);਍        椀昀 ⠀猀挀漀瀀攀䌀爀攀愀琀攀搀⤀ 笀ഀഀ
          clone.on('$destroy', bind(transcludedScope, transcludedScope.$destroy));਍        紀ഀഀ
        return clone;਍      紀㬀ഀഀ
    }਍ഀഀ
    /**਍     ⨀ 䰀漀漀欀猀 昀漀爀 搀椀爀攀挀琀椀瘀攀猀 漀渀 琀栀攀 最椀瘀攀渀 渀漀搀攀 愀渀搀 愀搀搀猀 琀栀攀洀 琀漀 琀栀攀 搀椀爀攀挀琀椀瘀攀 挀漀氀氀攀挀琀椀漀渀 眀栀椀挀栀 椀猀ഀഀ
     * sorted.਍     ⨀ഀഀ
     * @param node Node to search.਍     ⨀ 䀀瀀愀爀愀洀 搀椀爀攀挀琀椀瘀攀猀 䄀渀 愀爀爀愀礀 琀漀 眀栀椀挀栀 琀栀攀 搀椀爀攀挀琀椀瘀攀猀 愀爀攀 愀搀搀攀搀 琀漀⸀ 吀栀椀猀 愀爀爀愀礀 椀猀 猀漀爀琀攀搀 戀攀昀漀爀攀ഀഀ
     *        the function returns.਍     ⨀ 䀀瀀愀爀愀洀 愀琀琀爀猀 吀栀攀 猀栀愀爀攀搀 愀琀琀爀猀 漀戀樀攀挀琀 眀栀椀挀栀 椀猀 甀猀攀搀 琀漀 瀀漀瀀甀氀愀琀攀 琀栀攀 渀漀爀洀愀氀椀稀攀搀 愀琀琀爀椀戀甀琀攀猀⸀ഀഀ
     * @param {number=} maxPriority Max directive priority.਍     ⨀⼀ഀഀ
    function collectDirectives(node, directives, attrs, maxPriority, ignoreDirective) {਍      瘀愀爀 渀漀搀攀吀礀瀀攀 㴀 渀漀搀攀⸀渀漀搀攀吀礀瀀攀Ⰰഀഀ
          attrsMap = attrs.$attr,਍          洀愀琀挀栀Ⰰഀഀ
          className;਍ഀഀ
      switch(nodeType) {਍        挀愀猀攀 ㄀㨀 ⼀⨀ 䔀氀攀洀攀渀琀 ⨀⼀ഀഀ
          // use the node name: <directive>਍          愀搀搀䐀椀爀攀挀琀椀瘀攀⠀搀椀爀攀挀琀椀瘀攀猀Ⰰഀഀ
              directiveNormalize(nodeName_(node).toLowerCase()), 'E', maxPriority, ignoreDirective);਍ഀഀ
          // iterate over the attributes਍          昀漀爀 ⠀瘀愀爀 愀琀琀爀Ⰰ 渀愀洀攀Ⰰ 渀一愀洀攀Ⰰ 渀最䄀琀琀爀一愀洀攀Ⰰ 瘀愀氀甀攀Ⰰ 渀䄀琀琀爀猀 㴀 渀漀搀攀⸀愀琀琀爀椀戀甀琀攀猀Ⰰഀഀ
                   j = 0, jj = nAttrs && nAttrs.length; j < jj; j++) {਍            瘀愀爀 愀琀琀爀匀琀愀爀琀一愀洀攀 㴀 昀愀氀猀攀㬀ഀഀ
            var attrEndName = false;਍ഀഀ
            attr = nAttrs[j];਍            椀昀 ⠀℀洀猀椀攀 簀簀 洀猀椀攀 㸀㴀 㠀 簀簀 愀琀琀爀⸀猀瀀攀挀椀昀椀攀搀⤀ 笀ഀഀ
              name = attr.name;਍              ⼀⼀ 猀甀瀀瀀漀爀琀 渀最䄀琀琀爀 愀琀琀爀椀戀甀琀攀 戀椀渀搀椀渀最ഀഀ
              ngAttrName = directiveNormalize(name);਍              椀昀 ⠀一䜀开䄀吀吀刀开䈀䤀一䐀䤀一䜀⸀琀攀猀琀⠀渀最䄀琀琀爀一愀洀攀⤀⤀ 笀ഀഀ
                name = snake_case(ngAttrName.substr(6), '-');਍              紀ഀഀ
਍              瘀愀爀 搀椀爀攀挀琀椀瘀攀一一愀洀攀 㴀 渀最䄀琀琀爀一愀洀攀⸀爀攀瀀氀愀挀攀⠀⼀⠀匀琀愀爀琀簀䔀渀搀⤀␀⼀Ⰰ ✀✀⤀㬀ഀഀ
              if (ngAttrName === directiveNName + 'Start') {਍                愀琀琀爀匀琀愀爀琀一愀洀攀 㴀 渀愀洀攀㬀ഀഀ
                attrEndName = name.substr(0, name.length - 5) + 'end';਍                渀愀洀攀 㴀 渀愀洀攀⸀猀甀戀猀琀爀⠀　Ⰰ 渀愀洀攀⸀氀攀渀最琀栀 ⴀ 㘀⤀㬀ഀഀ
              }਍ഀഀ
              nName = directiveNormalize(name.toLowerCase());਍              愀琀琀爀猀䴀愀瀀嬀渀一愀洀攀崀 㴀 渀愀洀攀㬀ഀഀ
              attrs[nName] = value = trim(attr.value);਍              椀昀 ⠀最攀琀䈀漀漀氀攀愀渀䄀琀琀爀一愀洀攀⠀渀漀搀攀Ⰰ 渀一愀洀攀⤀⤀ 笀ഀഀ
                attrs[nName] = true; // presence means true਍              紀ഀഀ
              addAttrInterpolateDirective(node, directives, value, nName);਍              愀搀搀䐀椀爀攀挀琀椀瘀攀⠀搀椀爀攀挀琀椀瘀攀猀Ⰰ 渀一愀洀攀Ⰰ ✀䄀✀Ⰰ 洀愀砀倀爀椀漀爀椀琀礀Ⰰ 椀最渀漀爀攀䐀椀爀攀挀琀椀瘀攀Ⰰ 愀琀琀爀匀琀愀爀琀一愀洀攀Ⰰഀഀ
                            attrEndName);਍            紀ഀഀ
          }਍ഀഀ
          // use class as directive਍          挀氀愀猀猀一愀洀攀 㴀 渀漀搀攀⸀挀氀愀猀猀一愀洀攀㬀ഀഀ
          if (isString(className) && className !== '') {਍            眀栀椀氀攀 ⠀洀愀琀挀栀 㴀 䌀䰀䄀匀匀开䐀䤀刀䔀䌀吀䤀嘀䔀开刀䔀䜀䔀堀倀⸀攀砀攀挀⠀挀氀愀猀猀一愀洀攀⤀⤀ 笀ഀഀ
              nName = directiveNormalize(match[2]);਍              椀昀 ⠀愀搀搀䐀椀爀攀挀琀椀瘀攀⠀搀椀爀攀挀琀椀瘀攀猀Ⰰ 渀一愀洀攀Ⰰ ✀䌀✀Ⰰ 洀愀砀倀爀椀漀爀椀琀礀Ⰰ 椀最渀漀爀攀䐀椀爀攀挀琀椀瘀攀⤀⤀ 笀ഀഀ
                attrs[nName] = trim(match[3]);਍              紀ഀഀ
              className = className.substr(match.index + match[0].length);਍            紀ഀഀ
          }਍          戀爀攀愀欀㬀ഀഀ
        case 3: /* Text Node */਍          愀搀搀吀攀砀琀䤀渀琀攀爀瀀漀氀愀琀攀䐀椀爀攀挀琀椀瘀攀⠀搀椀爀攀挀琀椀瘀攀猀Ⰰ 渀漀搀攀⸀渀漀搀攀嘀愀氀甀攀⤀㬀ഀഀ
          break;਍        挀愀猀攀 㠀㨀 ⼀⨀ 䌀漀洀洀攀渀琀 ⨀⼀ഀഀ
          try {਍            洀愀琀挀栀 㴀 䌀伀䴀䴀䔀一吀开䐀䤀刀䔀䌀吀䤀嘀䔀开刀䔀䜀䔀堀倀⸀攀砀攀挀⠀渀漀搀攀⸀渀漀搀攀嘀愀氀甀攀⤀㬀ഀഀ
            if (match) {਍              渀一愀洀攀 㴀 搀椀爀攀挀琀椀瘀攀一漀爀洀愀氀椀稀攀⠀洀愀琀挀栀嬀㄀崀⤀㬀ഀഀ
              if (addDirective(directives, nName, 'M', maxPriority, ignoreDirective)) {਍                愀琀琀爀猀嬀渀一愀洀攀崀 㴀 琀爀椀洀⠀洀愀琀挀栀嬀㈀崀⤀㬀ഀഀ
              }਍            紀ഀഀ
          } catch (e) {਍            ⼀⼀ 琀甀爀渀猀 漀甀琀 琀栀愀琀 甀渀搀攀爀 猀漀洀攀 挀椀爀挀甀洀猀琀愀渀挀攀猀 䤀䔀㤀 琀栀爀漀眀猀 攀爀爀漀爀猀 眀栀攀渀 漀渀攀 愀琀琀攀洀瀀琀猀 琀漀 爀攀愀搀ഀഀ
            // comment's node value.਍            ⼀⼀ 䨀甀猀琀 椀最渀漀爀攀 椀琀 愀渀搀 挀漀渀琀椀渀甀攀⸀ ⠀䌀愀渀✀琀 猀攀攀洀 琀漀 爀攀瀀爀漀搀甀挀攀 椀渀 琀攀猀琀 挀愀猀攀⸀⤀ഀഀ
          }਍          戀爀攀愀欀㬀ഀഀ
      }਍ഀഀ
      directives.sort(byPriority);਍      爀攀琀甀爀渀 搀椀爀攀挀琀椀瘀攀猀㬀ഀഀ
    }਍ഀഀ
    /**਍     ⨀ 䜀椀瘀攀渀 愀 渀漀搀攀 眀椀琀栀 愀渀 搀椀爀攀挀琀椀瘀攀ⴀ猀琀愀爀琀 椀琀 挀漀氀氀攀挀琀猀 愀氀氀 漀昀 琀栀攀 猀椀戀氀椀渀最猀 甀渀琀椀氀 椀琀 昀椀渀搀猀ഀഀ
     * directive-end.਍     ⨀ 䀀瀀愀爀愀洀 渀漀搀攀ഀഀ
     * @param attrStart਍     ⨀ 䀀瀀愀爀愀洀 愀琀琀爀䔀渀搀ഀഀ
     * @returns {*}਍     ⨀⼀ഀഀ
    function groupScan(node, attrStart, attrEnd) {਍      瘀愀爀 渀漀搀攀猀 㴀 嬀崀㬀ഀഀ
      var depth = 0;਍      椀昀 ⠀愀琀琀爀匀琀愀爀琀 ☀☀ 渀漀搀攀⸀栀愀猀䄀琀琀爀椀戀甀琀攀 ☀☀ 渀漀搀攀⸀栀愀猀䄀琀琀爀椀戀甀琀攀⠀愀琀琀爀匀琀愀爀琀⤀⤀ 笀ഀഀ
        var startNode = node;਍        搀漀 笀ഀഀ
          if (!node) {਍            琀栀爀漀眀 ␀挀漀洀瀀椀氀攀䴀椀渀䔀爀爀⠀✀甀琀攀爀搀椀爀✀Ⰰഀഀ
                      "Unterminated attribute, found '{0}' but no matching '{1}' found.",਍                      愀琀琀爀匀琀愀爀琀Ⰰ 愀琀琀爀䔀渀搀⤀㬀ഀഀ
          }਍          椀昀 ⠀渀漀搀攀⸀渀漀搀攀吀礀瀀攀 㴀㴀 ㄀ ⼀⨀⨀ 䔀氀攀洀攀渀琀 ⨀⨀⼀⤀ 笀ഀഀ
            if (node.hasAttribute(attrStart)) depth++;਍            椀昀 ⠀渀漀搀攀⸀栀愀猀䄀琀琀爀椀戀甀琀攀⠀愀琀琀爀䔀渀搀⤀⤀ 搀攀瀀琀栀ⴀⴀ㬀ഀഀ
          }਍          渀漀搀攀猀⸀瀀甀猀栀⠀渀漀搀攀⤀㬀ഀഀ
          node = node.nextSibling;਍        紀 眀栀椀氀攀 ⠀搀攀瀀琀栀 㸀 　⤀㬀ഀഀ
      } else {਍        渀漀搀攀猀⸀瀀甀猀栀⠀渀漀搀攀⤀㬀ഀഀ
      }਍ഀഀ
      return jqLite(nodes);਍    紀ഀഀ
਍    ⼀⨀⨀ഀഀ
     * Wrapper for linking function which converts normal linking function into a grouped਍     ⨀ 氀椀渀欀椀渀最 昀甀渀挀琀椀漀渀⸀ഀഀ
     * @param linkFn਍     ⨀ 䀀瀀愀爀愀洀 愀琀琀爀匀琀愀爀琀ഀഀ
     * @param attrEnd਍     ⨀ 䀀爀攀琀甀爀渀猀 笀䘀甀渀挀琀椀漀渀紀ഀഀ
     */਍    昀甀渀挀琀椀漀渀 最爀漀甀瀀䔀氀攀洀攀渀琀猀䰀椀渀欀䘀渀圀爀愀瀀瀀攀爀⠀氀椀渀欀䘀渀Ⰰ 愀琀琀爀匀琀愀爀琀Ⰰ 愀琀琀爀䔀渀搀⤀ 笀ഀഀ
      return function(scope, element, attrs, controllers, transcludeFn) {਍        攀氀攀洀攀渀琀 㴀 最爀漀甀瀀匀挀愀渀⠀攀氀攀洀攀渀琀嬀　崀Ⰰ 愀琀琀爀匀琀愀爀琀Ⰰ 愀琀琀爀䔀渀搀⤀㬀ഀഀ
        return linkFn(scope, element, attrs, controllers, transcludeFn);਍      紀㬀ഀഀ
    }਍ഀഀ
    /**਍     ⨀ 伀渀挀攀 琀栀攀 搀椀爀攀挀琀椀瘀攀猀 栀愀瘀攀 戀攀攀渀 挀漀氀氀攀挀琀攀搀Ⰰ 琀栀攀椀爀 挀漀洀瀀椀氀攀 昀甀渀挀琀椀漀渀猀 愀爀攀 攀砀攀挀甀琀攀搀⸀ 吀栀椀猀 洀攀琀栀漀搀ഀഀ
     * is responsible for inlining directive templates as well as terminating the application਍     ⨀ 漀昀 琀栀攀 搀椀爀攀挀琀椀瘀攀猀 椀昀 琀栀攀 琀攀爀洀椀渀愀氀 搀椀爀攀挀琀椀瘀攀 栀愀猀 戀攀攀渀 爀攀愀挀栀攀搀⸀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀䄀爀爀愀礀紀 搀椀爀攀挀琀椀瘀攀猀 䄀爀爀愀礀 漀昀 挀漀氀氀攀挀琀攀搀 搀椀爀攀挀琀椀瘀攀猀 琀漀 攀砀攀挀甀琀攀 琀栀攀椀爀 挀漀洀瀀椀氀攀 昀甀渀挀琀椀漀渀⸀ഀഀ
     *        this needs to be pre-sorted by priority order.਍     ⨀ 䀀瀀愀爀愀洀 笀一漀搀攀紀 挀漀洀瀀椀氀攀一漀搀攀 吀栀攀 爀愀眀 䐀伀䴀 渀漀搀攀 琀漀 愀瀀瀀氀礀 琀栀攀 挀漀洀瀀椀氀攀 昀甀渀挀琀椀漀渀猀 琀漀ഀഀ
     * @param {Object} templateAttrs The shared attribute function਍     ⨀ 䀀瀀愀爀愀洀 笀昀甀渀挀琀椀漀渀⠀愀渀最甀氀愀爀⸀匀挀漀瀀攀嬀Ⰰ 挀氀漀渀攀䄀琀琀愀挀栀䘀渀崀紀 琀爀愀渀猀挀氀甀搀攀䘀渀 䄀 氀椀渀欀椀渀最 昀甀渀挀琀椀漀渀Ⰰ 眀栀攀爀攀 琀栀攀ഀഀ
     *                                                  scope argument is auto-generated to the new਍     ⨀                                                  挀栀椀氀搀 漀昀 琀栀攀 琀爀愀渀猀挀氀甀搀攀搀 瀀愀爀攀渀琀 猀挀漀瀀攀⸀ഀഀ
     * @param {JQLite} jqCollection If we are working on the root of the compile tree then this਍     ⨀                              愀爀最甀洀攀渀琀 栀愀猀 琀栀攀 爀漀漀琀 樀焀䰀椀琀攀 愀爀爀愀礀 猀漀 琀栀愀琀 眀攀 挀愀渀 爀攀瀀氀愀挀攀 渀漀搀攀猀ഀഀ
     *                              on it.਍     ⨀ 䀀瀀愀爀愀洀 笀伀戀樀攀挀琀㴀紀 漀爀椀最椀渀愀氀刀攀瀀氀愀挀攀䐀椀爀攀挀琀椀瘀攀 䄀渀 漀瀀琀椀漀渀愀氀 搀椀爀攀挀琀椀瘀攀 琀栀愀琀 眀椀氀氀 戀攀 椀最渀漀爀攀搀 眀栀攀渀ഀഀ
     *                                           compiling the transclusion.਍     ⨀ 䀀瀀愀爀愀洀 笀䄀爀爀愀礀⸀㰀䘀甀渀挀琀椀漀渀㸀紀 瀀爀攀䰀椀渀欀䘀渀猀ഀഀ
     * @param {Array.<Function>} postLinkFns਍     ⨀ 䀀瀀愀爀愀洀 笀伀戀樀攀挀琀紀 瀀爀攀瘀椀漀甀猀䌀漀洀瀀椀氀攀䌀漀渀琀攀砀琀 䌀漀渀琀攀砀琀 甀猀攀搀 昀漀爀 瀀爀攀瘀椀漀甀猀 挀漀洀瀀椀氀愀琀椀漀渀 漀昀 琀栀攀 挀甀爀爀攀渀琀ഀഀ
     *                                        node਍     ⨀ 䀀爀攀琀甀爀渀猀 氀椀渀欀䘀渀ഀഀ
     */਍    昀甀渀挀琀椀漀渀 愀瀀瀀氀礀䐀椀爀攀挀琀椀瘀攀猀吀漀一漀搀攀⠀搀椀爀攀挀琀椀瘀攀猀Ⰰ 挀漀洀瀀椀氀攀一漀搀攀Ⰰ 琀攀洀瀀氀愀琀攀䄀琀琀爀猀Ⰰ 琀爀愀渀猀挀氀甀搀攀䘀渀Ⰰഀഀ
                                   jqCollection, originalReplaceDirective, preLinkFns, postLinkFns,਍                                   瀀爀攀瘀椀漀甀猀䌀漀洀瀀椀氀攀䌀漀渀琀攀砀琀⤀ 笀ഀഀ
      previousCompileContext = previousCompileContext || {};਍ഀഀ
      var terminalPriority = -Number.MAX_VALUE,਍          渀攀眀匀挀漀瀀攀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
          controllerDirectives = previousCompileContext.controllerDirectives,਍          渀攀眀䤀猀漀氀愀琀攀匀挀漀瀀攀䐀椀爀攀挀琀椀瘀攀 㴀 瀀爀攀瘀椀漀甀猀䌀漀洀瀀椀氀攀䌀漀渀琀攀砀琀⸀渀攀眀䤀猀漀氀愀琀攀匀挀漀瀀攀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
          templateDirective = previousCompileContext.templateDirective,਍          渀漀渀吀氀戀吀爀愀渀猀挀氀甀搀攀䐀椀爀攀挀琀椀瘀攀 㴀 瀀爀攀瘀椀漀甀猀䌀漀洀瀀椀氀攀䌀漀渀琀攀砀琀⸀渀漀渀吀氀戀吀爀愀渀猀挀氀甀搀攀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
          hasTranscludeDirective = false,਍          栀愀猀䔀氀攀洀攀渀琀吀爀愀渀猀挀氀甀搀攀䐀椀爀攀挀琀椀瘀攀 㴀 昀愀氀猀攀Ⰰഀഀ
          $compileNode = templateAttrs.$$element = jqLite(compileNode),਍          搀椀爀攀挀琀椀瘀攀Ⰰഀഀ
          directiveName,਍          ␀琀攀洀瀀氀愀琀攀Ⰰഀഀ
          replaceDirective = originalReplaceDirective,਍          挀栀椀氀搀吀爀愀渀猀挀氀甀搀攀䘀渀 㴀 琀爀愀渀猀挀氀甀搀攀䘀渀Ⰰഀഀ
          linkFn,਍          搀椀爀攀挀琀椀瘀攀嘀愀氀甀攀㬀ഀഀ
਍      ⼀⼀ 攀砀攀挀甀琀攀猀 愀氀氀 搀椀爀攀挀琀椀瘀攀猀 漀渀 琀栀攀 挀甀爀爀攀渀琀 攀氀攀洀攀渀琀ഀഀ
      for(var i = 0, ii = directives.length; i < ii; i++) {਍        搀椀爀攀挀琀椀瘀攀 㴀 搀椀爀攀挀琀椀瘀攀猀嬀椀崀㬀ഀഀ
        var attrStart = directive.$$start;਍        瘀愀爀 愀琀琀爀䔀渀搀 㴀 搀椀爀攀挀琀椀瘀攀⸀␀␀攀渀搀㬀ഀഀ
਍        ⼀⼀ 挀漀氀氀攀挀琀 洀甀氀琀椀戀氀漀挀欀 猀攀挀琀椀漀渀猀ഀഀ
        if (attrStart) {਍          ␀挀漀洀瀀椀氀攀一漀搀攀 㴀 最爀漀甀瀀匀挀愀渀⠀挀漀洀瀀椀氀攀一漀搀攀Ⰰ 愀琀琀爀匀琀愀爀琀Ⰰ 愀琀琀爀䔀渀搀⤀㬀ഀഀ
        }਍        ␀琀攀洀瀀氀愀琀攀 㴀 甀渀搀攀昀椀渀攀搀㬀ഀഀ
਍        椀昀 ⠀琀攀爀洀椀渀愀氀倀爀椀漀爀椀琀礀 㸀 搀椀爀攀挀琀椀瘀攀⸀瀀爀椀漀爀椀琀礀⤀ 笀ഀഀ
          break; // prevent further processing of directives਍        紀ഀഀ
਍        椀昀 ⠀搀椀爀攀挀琀椀瘀攀嘀愀氀甀攀 㴀 搀椀爀攀挀琀椀瘀攀⸀猀挀漀瀀攀⤀ 笀ഀഀ
          newScopeDirective = newScopeDirective || directive;਍ഀഀ
          // skip the check for directives with async templates, we'll check the derived sync਍          ⼀⼀ 搀椀爀攀挀琀椀瘀攀 眀栀攀渀 琀栀攀 琀攀洀瀀氀愀琀攀 愀爀爀椀瘀攀猀ഀഀ
          if (!directive.templateUrl) {਍            愀猀猀攀爀琀一漀䐀甀瀀氀椀挀愀琀攀⠀✀渀攀眀⼀椀猀漀氀愀琀攀搀 猀挀漀瀀攀✀Ⰰ 渀攀眀䤀猀漀氀愀琀攀匀挀漀瀀攀䐀椀爀攀挀琀椀瘀攀Ⰰ 搀椀爀攀挀琀椀瘀攀Ⰰഀഀ
                              $compileNode);਍            椀昀 ⠀椀猀伀戀樀攀挀琀⠀搀椀爀攀挀琀椀瘀攀嘀愀氀甀攀⤀⤀ 笀ഀഀ
              newIsolateScopeDirective = directive;਍            紀ഀഀ
          }਍        紀ഀഀ
਍        搀椀爀攀挀琀椀瘀攀一愀洀攀 㴀 搀椀爀攀挀琀椀瘀攀⸀渀愀洀攀㬀ഀഀ
਍        椀昀 ⠀℀搀椀爀攀挀琀椀瘀攀⸀琀攀洀瀀氀愀琀攀唀爀氀 ☀☀ 搀椀爀攀挀琀椀瘀攀⸀挀漀渀琀爀漀氀氀攀爀⤀ 笀ഀഀ
          directiveValue = directive.controller;਍          挀漀渀琀爀漀氀氀攀爀䐀椀爀攀挀琀椀瘀攀猀 㴀 挀漀渀琀爀漀氀氀攀爀䐀椀爀攀挀琀椀瘀攀猀 簀簀 笀紀㬀ഀഀ
          assertNoDuplicate("'" + directiveName + "' controller",਍              挀漀渀琀爀漀氀氀攀爀䐀椀爀攀挀琀椀瘀攀猀嬀搀椀爀攀挀琀椀瘀攀一愀洀攀崀Ⰰ 搀椀爀攀挀琀椀瘀攀Ⰰ ␀挀漀洀瀀椀氀攀一漀搀攀⤀㬀ഀഀ
          controllerDirectives[directiveName] = directive;਍        紀ഀഀ
਍        椀昀 ⠀搀椀爀攀挀琀椀瘀攀嘀愀氀甀攀 㴀 搀椀爀攀挀琀椀瘀攀⸀琀爀愀渀猀挀氀甀搀攀⤀ 笀ഀഀ
          hasTranscludeDirective = true;਍ഀഀ
          // Special case ngIf and ngRepeat so that we don't complain about duplicate transclusion.਍          ⼀⼀ 吀栀椀猀 漀瀀琀椀漀渀 猀栀漀甀氀搀 漀渀氀礀 戀攀 甀猀攀搀 戀礀 搀椀爀攀挀琀椀瘀攀猀 琀栀愀琀 欀渀漀眀 栀漀眀 琀漀 栀漀眀 琀漀 猀愀昀攀氀礀 栀愀渀搀氀攀 攀氀攀洀攀渀琀 琀爀愀渀猀挀氀甀猀椀漀渀Ⰰഀഀ
          // where the transcluded nodes are added or replaced after linking.਍          椀昀 ⠀℀搀椀爀攀挀琀椀瘀攀⸀␀␀琀氀戀⤀ 笀ഀഀ
            assertNoDuplicate('transclusion', nonTlbTranscludeDirective, directive, $compileNode);਍            渀漀渀吀氀戀吀爀愀渀猀挀氀甀搀攀䐀椀爀攀挀琀椀瘀攀 㴀 搀椀爀攀挀琀椀瘀攀㬀ഀഀ
          }਍ഀഀ
          if (directiveValue == 'element') {਍            栀愀猀䔀氀攀洀攀渀琀吀爀愀渀猀挀氀甀搀攀䐀椀爀攀挀琀椀瘀攀 㴀 琀爀甀攀㬀ഀഀ
            terminalPriority = directive.priority;਍            ␀琀攀洀瀀氀愀琀攀 㴀 最爀漀甀瀀匀挀愀渀⠀挀漀洀瀀椀氀攀一漀搀攀Ⰰ 愀琀琀爀匀琀愀爀琀Ⰰ 愀琀琀爀䔀渀搀⤀㬀ഀഀ
            $compileNode = templateAttrs.$$element =਍                樀焀䰀椀琀攀⠀搀漀挀甀洀攀渀琀⸀挀爀攀愀琀攀䌀漀洀洀攀渀琀⠀✀ ✀ ⬀ 搀椀爀攀挀琀椀瘀攀一愀洀攀 ⬀ ✀㨀 ✀ ⬀ഀഀ
                                              templateAttrs[directiveName] + ' '));਍            挀漀洀瀀椀氀攀一漀搀攀 㴀 ␀挀漀洀瀀椀氀攀一漀搀攀嬀　崀㬀ഀഀ
            replaceWith(jqCollection, jqLite(sliceArgs($template)), compileNode);਍ഀഀ
            childTranscludeFn = compile($template, transcludeFn, terminalPriority,਍                                        爀攀瀀氀愀挀攀䐀椀爀攀挀琀椀瘀攀 ☀☀ 爀攀瀀氀愀挀攀䐀椀爀攀挀琀椀瘀攀⸀渀愀洀攀Ⰰ 笀ഀഀ
                                          // Don't pass in:਍                                          ⼀⼀ ⴀ 挀漀渀琀爀漀氀氀攀爀䐀椀爀攀挀琀椀瘀攀猀 ⴀ 漀琀栀攀爀眀椀猀攀 眀攀✀氀氀 挀爀攀愀琀攀 搀甀瀀氀椀挀愀琀攀猀 挀漀渀琀爀漀氀氀攀爀猀ഀഀ
                                          // - newIsolateScopeDirective or templateDirective - combining templates with਍                                          ⼀⼀   攀氀攀洀攀渀琀 琀爀愀渀猀挀氀甀猀椀漀渀 搀漀攀猀渀✀琀 洀愀欀攀 猀攀渀猀攀⸀ഀഀ
                                          //਍                                          ⼀⼀ 圀攀 渀攀攀搀 漀渀氀礀 渀漀渀吀氀戀吀爀愀渀猀挀氀甀搀攀䐀椀爀攀挀琀椀瘀攀 猀漀 琀栀愀琀 眀攀 瀀爀攀瘀攀渀琀 瀀甀琀琀椀渀最 琀爀愀渀猀挀氀甀猀椀漀渀ഀഀ
                                          // on the same element more than once.਍                                          渀漀渀吀氀戀吀爀愀渀猀挀氀甀搀攀䐀椀爀攀挀琀椀瘀攀㨀 渀漀渀吀氀戀吀爀愀渀猀挀氀甀搀攀䐀椀爀攀挀琀椀瘀攀ഀഀ
                                        });਍          紀 攀氀猀攀 笀ഀഀ
            $template = jqLite(jqLiteClone(compileNode)).contents();਍            ␀挀漀洀瀀椀氀攀一漀搀攀⸀攀洀瀀琀礀⠀⤀㬀 ⼀⼀ 挀氀攀愀爀 挀漀渀琀攀渀琀猀ഀഀ
            childTranscludeFn = compile($template, transcludeFn);਍          紀ഀഀ
        }਍ഀഀ
        if (directive.template) {਍          愀猀猀攀爀琀一漀䐀甀瀀氀椀挀愀琀攀⠀✀琀攀洀瀀氀愀琀攀✀Ⰰ 琀攀洀瀀氀愀琀攀䐀椀爀攀挀琀椀瘀攀Ⰰ 搀椀爀攀挀琀椀瘀攀Ⰰ ␀挀漀洀瀀椀氀攀一漀搀攀⤀㬀ഀഀ
          templateDirective = directive;਍ഀഀ
          directiveValue = (isFunction(directive.template))਍              㼀 搀椀爀攀挀琀椀瘀攀⸀琀攀洀瀀氀愀琀攀⠀␀挀漀洀瀀椀氀攀一漀搀攀Ⰰ 琀攀洀瀀氀愀琀攀䄀琀琀爀猀⤀ഀഀ
              : directive.template;਍ഀഀ
          directiveValue = denormalizeTemplate(directiveValue);਍ഀഀ
          if (directive.replace) {਍            爀攀瀀氀愀挀攀䐀椀爀攀挀琀椀瘀攀 㴀 搀椀爀攀挀琀椀瘀攀㬀ഀഀ
            $template = jqLite('<div>' +਍                                 琀爀椀洀⠀搀椀爀攀挀琀椀瘀攀嘀愀氀甀攀⤀ ⬀ഀഀ
                               '</div>').contents();਍            挀漀洀瀀椀氀攀一漀搀攀 㴀 ␀琀攀洀瀀氀愀琀攀嬀　崀㬀ഀഀ
਍            椀昀 ⠀␀琀攀洀瀀氀愀琀攀⸀氀攀渀最琀栀 ℀㴀 ㄀ 簀簀 挀漀洀瀀椀氀攀一漀搀攀⸀渀漀搀攀吀礀瀀攀 ℀㴀㴀 ㄀⤀ 笀ഀഀ
              throw $compileMinErr('tplrt',਍                  ∀吀攀洀瀀氀愀琀攀 昀漀爀 搀椀爀攀挀琀椀瘀攀 ✀笀　紀✀ 洀甀猀琀 栀愀瘀攀 攀砀愀挀琀氀礀 漀渀攀 爀漀漀琀 攀氀攀洀攀渀琀⸀ 笀㄀紀∀Ⰰഀഀ
                  directiveName, '');਍            紀ഀഀ
਍            爀攀瀀氀愀挀攀圀椀琀栀⠀樀焀䌀漀氀氀攀挀琀椀漀渀Ⰰ ␀挀漀洀瀀椀氀攀一漀搀攀Ⰰ 挀漀洀瀀椀氀攀一漀搀攀⤀㬀ഀഀ
਍            瘀愀爀 渀攀眀吀攀洀瀀氀愀琀攀䄀琀琀爀猀 㴀 笀␀愀琀琀爀㨀 笀紀紀㬀ഀഀ
਍            ⼀⼀ 挀漀洀戀椀渀攀 搀椀爀攀挀琀椀瘀攀猀 昀爀漀洀 琀栀攀 漀爀椀最椀渀愀氀 渀漀搀攀 愀渀搀 昀爀漀洀 琀栀攀 琀攀洀瀀氀愀琀攀㨀ഀഀ
            // - take the array of directives for this element਍            ⼀⼀ ⴀ 猀瀀氀椀琀 椀琀 椀渀琀漀 琀眀漀 瀀愀爀琀猀Ⰰ 琀栀漀猀攀 琀栀愀琀 愀氀爀攀愀搀礀 愀瀀瀀氀椀攀搀 ⠀瀀爀漀挀攀猀猀攀搀⤀ 愀渀搀 琀栀漀猀攀 琀栀愀琀 眀攀爀攀渀✀琀 ⠀甀渀瀀爀漀挀攀猀猀攀搀⤀ഀഀ
            // - collect directives from the template and sort them by priority਍            ⼀⼀ ⴀ 挀漀洀戀椀渀攀 搀椀爀攀挀琀椀瘀攀猀 愀猀㨀 瀀爀漀挀攀猀猀攀搀 ⬀ 琀攀洀瀀氀愀琀攀 ⬀ 甀渀瀀爀漀挀攀猀猀攀搀ഀഀ
            var templateDirectives = collectDirectives(compileNode, [], newTemplateAttrs);਍            瘀愀爀 甀渀瀀爀漀挀攀猀猀攀搀䐀椀爀攀挀琀椀瘀攀猀 㴀 搀椀爀攀挀琀椀瘀攀猀⸀猀瀀氀椀挀攀⠀椀 ⬀ ㄀Ⰰ 搀椀爀攀挀琀椀瘀攀猀⸀氀攀渀最琀栀 ⴀ ⠀椀 ⬀ ㄀⤀⤀㬀ഀഀ
਍            椀昀 ⠀渀攀眀䤀猀漀氀愀琀攀匀挀漀瀀攀䐀椀爀攀挀琀椀瘀攀⤀ 笀ഀഀ
              markDirectivesAsIsolate(templateDirectives);਍            紀ഀഀ
            directives = directives.concat(templateDirectives).concat(unprocessedDirectives);਍            洀攀爀最攀吀攀洀瀀氀愀琀攀䄀琀琀爀椀戀甀琀攀猀⠀琀攀洀瀀氀愀琀攀䄀琀琀爀猀Ⰰ 渀攀眀吀攀洀瀀氀愀琀攀䄀琀琀爀猀⤀㬀ഀഀ
਍            椀椀 㴀 搀椀爀攀挀琀椀瘀攀猀⸀氀攀渀最琀栀㬀ഀഀ
          } else {਍            ␀挀漀洀瀀椀氀攀一漀搀攀⸀栀琀洀氀⠀搀椀爀攀挀琀椀瘀攀嘀愀氀甀攀⤀㬀ഀഀ
          }਍        紀ഀഀ
਍        椀昀 ⠀搀椀爀攀挀琀椀瘀攀⸀琀攀洀瀀氀愀琀攀唀爀氀⤀ 笀ഀഀ
          assertNoDuplicate('template', templateDirective, directive, $compileNode);਍          琀攀洀瀀氀愀琀攀䐀椀爀攀挀琀椀瘀攀 㴀 搀椀爀攀挀琀椀瘀攀㬀ഀഀ
਍          椀昀 ⠀搀椀爀攀挀琀椀瘀攀⸀爀攀瀀氀愀挀攀⤀ 笀ഀഀ
            replaceDirective = directive;਍          紀ഀഀ
਍          渀漀搀攀䰀椀渀欀䘀渀 㴀 挀漀洀瀀椀氀攀吀攀洀瀀氀愀琀攀唀爀氀⠀搀椀爀攀挀琀椀瘀攀猀⸀猀瀀氀椀挀攀⠀椀Ⰰ 搀椀爀攀挀琀椀瘀攀猀⸀氀攀渀最琀栀 ⴀ 椀⤀Ⰰ ␀挀漀洀瀀椀氀攀一漀搀攀Ⰰഀഀ
              templateAttrs, jqCollection, childTranscludeFn, preLinkFns, postLinkFns, {਍                挀漀渀琀爀漀氀氀攀爀䐀椀爀攀挀琀椀瘀攀猀㨀 挀漀渀琀爀漀氀氀攀爀䐀椀爀攀挀琀椀瘀攀猀Ⰰഀഀ
                newIsolateScopeDirective: newIsolateScopeDirective,਍                琀攀洀瀀氀愀琀攀䐀椀爀攀挀琀椀瘀攀㨀 琀攀洀瀀氀愀琀攀䐀椀爀攀挀琀椀瘀攀Ⰰഀഀ
                nonTlbTranscludeDirective: nonTlbTranscludeDirective਍              紀⤀㬀ഀഀ
          ii = directives.length;਍        紀 攀氀猀攀 椀昀 ⠀搀椀爀攀挀琀椀瘀攀⸀挀漀洀瀀椀氀攀⤀ 笀ഀഀ
          try {਍            氀椀渀欀䘀渀 㴀 搀椀爀攀挀琀椀瘀攀⸀挀漀洀瀀椀氀攀⠀␀挀漀洀瀀椀氀攀一漀搀攀Ⰰ 琀攀洀瀀氀愀琀攀䄀琀琀爀猀Ⰰ 挀栀椀氀搀吀爀愀渀猀挀氀甀搀攀䘀渀⤀㬀ഀഀ
            if (isFunction(linkFn)) {਍              愀搀搀䰀椀渀欀䘀渀猀⠀渀甀氀氀Ⰰ 氀椀渀欀䘀渀Ⰰ 愀琀琀爀匀琀愀爀琀Ⰰ 愀琀琀爀䔀渀搀⤀㬀ഀഀ
            } else if (linkFn) {਍              愀搀搀䰀椀渀欀䘀渀猀⠀氀椀渀欀䘀渀⸀瀀爀攀Ⰰ 氀椀渀欀䘀渀⸀瀀漀猀琀Ⰰ 愀琀琀爀匀琀愀爀琀Ⰰ 愀琀琀爀䔀渀搀⤀㬀ഀഀ
            }਍          紀 挀愀琀挀栀 ⠀攀⤀ 笀ഀഀ
            $exceptionHandler(e, startingTag($compileNode));਍          紀ഀഀ
        }਍ഀഀ
        if (directive.terminal) {਍          渀漀搀攀䰀椀渀欀䘀渀⸀琀攀爀洀椀渀愀氀 㴀 琀爀甀攀㬀ഀഀ
          terminalPriority = Math.max(terminalPriority, directive.priority);਍        紀ഀഀ
਍      紀ഀഀ
਍      渀漀搀攀䰀椀渀欀䘀渀⸀猀挀漀瀀攀 㴀 渀攀眀匀挀漀瀀攀䐀椀爀攀挀琀椀瘀攀 ☀☀ 渀攀眀匀挀漀瀀攀䐀椀爀攀挀琀椀瘀攀⸀猀挀漀瀀攀 㴀㴀㴀 琀爀甀攀㬀ഀഀ
      nodeLinkFn.transclude = hasTranscludeDirective && childTranscludeFn;਍ഀഀ
      // might be normal or delayed nodeLinkFn depending on if templateUrl is present਍      爀攀琀甀爀渀 渀漀搀攀䰀椀渀欀䘀渀㬀ഀഀ
਍      ⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
਍      昀甀渀挀琀椀漀渀 愀搀搀䰀椀渀欀䘀渀猀⠀瀀爀攀Ⰰ 瀀漀猀琀Ⰰ 愀琀琀爀匀琀愀爀琀Ⰰ 愀琀琀爀䔀渀搀⤀ 笀ഀഀ
        if (pre) {਍          椀昀 ⠀愀琀琀爀匀琀愀爀琀⤀ 瀀爀攀 㴀 最爀漀甀瀀䔀氀攀洀攀渀琀猀䰀椀渀欀䘀渀圀爀愀瀀瀀攀爀⠀瀀爀攀Ⰰ 愀琀琀爀匀琀愀爀琀Ⰰ 愀琀琀爀䔀渀搀⤀㬀ഀഀ
          pre.require = directive.require;਍          椀昀 ⠀渀攀眀䤀猀漀氀愀琀攀匀挀漀瀀攀䐀椀爀攀挀琀椀瘀攀 㴀㴀㴀 搀椀爀攀挀琀椀瘀攀 簀簀 搀椀爀攀挀琀椀瘀攀⸀␀␀椀猀漀氀愀琀攀匀挀漀瀀攀⤀ 笀ഀഀ
            pre = cloneAndAnnotateFn(pre, {isolateScope: true});਍          紀ഀഀ
          preLinkFns.push(pre);਍        紀ഀഀ
        if (post) {਍          椀昀 ⠀愀琀琀爀匀琀愀爀琀⤀ 瀀漀猀琀 㴀 最爀漀甀瀀䔀氀攀洀攀渀琀猀䰀椀渀欀䘀渀圀爀愀瀀瀀攀爀⠀瀀漀猀琀Ⰰ 愀琀琀爀匀琀愀爀琀Ⰰ 愀琀琀爀䔀渀搀⤀㬀ഀഀ
          post.require = directive.require;਍          椀昀 ⠀渀攀眀䤀猀漀氀愀琀攀匀挀漀瀀攀䐀椀爀攀挀琀椀瘀攀 㴀㴀㴀 搀椀爀攀挀琀椀瘀攀 簀簀 搀椀爀攀挀琀椀瘀攀⸀␀␀椀猀漀氀愀琀攀匀挀漀瀀攀⤀ 笀ഀഀ
            post = cloneAndAnnotateFn(post, {isolateScope: true});਍          紀ഀഀ
          postLinkFns.push(post);਍        紀ഀഀ
      }਍ഀഀ
਍      昀甀渀挀琀椀漀渀 最攀琀䌀漀渀琀爀漀氀氀攀爀猀⠀爀攀焀甀椀爀攀Ⰰ ␀攀氀攀洀攀渀琀Ⰰ 攀氀攀洀攀渀琀䌀漀渀琀爀漀氀氀攀爀猀⤀ 笀ഀഀ
        var value, retrievalMethod = 'data', optional = false;਍        椀昀 ⠀椀猀匀琀爀椀渀最⠀爀攀焀甀椀爀攀⤀⤀ 笀ഀഀ
          while((value = require.charAt(0)) == '^' || value == '?') {਍            爀攀焀甀椀爀攀 㴀 爀攀焀甀椀爀攀⸀猀甀戀猀琀爀⠀㄀⤀㬀ഀഀ
            if (value == '^') {਍              爀攀琀爀椀攀瘀愀氀䴀攀琀栀漀搀 㴀 ✀椀渀栀攀爀椀琀攀搀䐀愀琀愀✀㬀ഀഀ
            }਍            漀瀀琀椀漀渀愀氀 㴀 漀瀀琀椀漀渀愀氀 簀簀 瘀愀氀甀攀 㴀㴀 ✀㼀✀㬀ഀഀ
          }਍          瘀愀氀甀攀 㴀 渀甀氀氀㬀ഀഀ
਍          椀昀 ⠀攀氀攀洀攀渀琀䌀漀渀琀爀漀氀氀攀爀猀 ☀☀ 爀攀琀爀椀攀瘀愀氀䴀攀琀栀漀搀 㴀㴀㴀 ✀搀愀琀愀✀⤀ 笀ഀഀ
            value = elementControllers[require];਍          紀ഀഀ
          value = value || $element[retrievalMethod]('$' + require + 'Controller');਍ഀഀ
          if (!value && !optional) {਍            琀栀爀漀眀 ␀挀漀洀瀀椀氀攀䴀椀渀䔀爀爀⠀✀挀琀爀攀焀✀Ⰰഀഀ
                "Controller '{0}', required by directive '{1}', can't be found!",਍                爀攀焀甀椀爀攀Ⰰ 搀椀爀攀挀琀椀瘀攀一愀洀攀⤀㬀ഀഀ
          }਍          爀攀琀甀爀渀 瘀愀氀甀攀㬀ഀഀ
        } else if (isArray(require)) {਍          瘀愀氀甀攀 㴀 嬀崀㬀ഀഀ
          forEach(require, function(require) {਍            瘀愀氀甀攀⸀瀀甀猀栀⠀最攀琀䌀漀渀琀爀漀氀氀攀爀猀⠀爀攀焀甀椀爀攀Ⰰ ␀攀氀攀洀攀渀琀Ⰰ 攀氀攀洀攀渀琀䌀漀渀琀爀漀氀氀攀爀猀⤀⤀㬀ഀഀ
          });਍        紀ഀഀ
        return value;਍      紀ഀഀ
਍ഀഀ
      function nodeLinkFn(childLinkFn, scope, linkNode, $rootElement, boundTranscludeFn) {਍        瘀愀爀 愀琀琀爀猀Ⰰ ␀攀氀攀洀攀渀琀Ⰰ 椀Ⰰ 椀椀Ⰰ 氀椀渀欀䘀渀Ⰰ 挀漀渀琀爀漀氀氀攀爀Ⰰ 椀猀漀氀愀琀攀匀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀䌀漀渀琀爀漀氀氀攀爀猀 㴀 笀紀Ⰰ 琀爀愀渀猀挀氀甀搀攀䘀渀㬀ഀഀ
਍        椀昀 ⠀挀漀洀瀀椀氀攀一漀搀攀 㴀㴀㴀 氀椀渀欀一漀搀攀⤀ 笀ഀഀ
          attrs = templateAttrs;਍        紀 攀氀猀攀 笀ഀഀ
          attrs = shallowCopy(templateAttrs, new Attributes(jqLite(linkNode), templateAttrs.$attr));਍        紀ഀഀ
        $element = attrs.$$element;਍ഀഀ
        if (newIsolateScopeDirective) {਍          瘀愀爀 䰀伀䌀䄀䰀开刀䔀䜀䔀堀倀 㴀 ⼀帀尀猀⨀⠀嬀䀀㴀☀崀⤀⠀尀㼀㼀⤀尀猀⨀⠀尀眀⨀⤀尀猀⨀␀⼀㬀ഀഀ
          var $linkNode = jqLite(linkNode);਍ഀഀ
          isolateScope = scope.$new(true);਍ഀഀ
          if (templateDirective && (templateDirective === newIsolateScopeDirective.$$originalDirective)) {਍            ␀氀椀渀欀一漀搀攀⸀搀愀琀愀⠀✀␀椀猀漀氀愀琀攀匀挀漀瀀攀✀Ⰰ 椀猀漀氀愀琀攀匀挀漀瀀攀⤀ 㬀ഀഀ
          } else {਍            ␀氀椀渀欀一漀搀攀⸀搀愀琀愀⠀✀␀椀猀漀氀愀琀攀匀挀漀瀀攀一漀吀攀洀瀀氀愀琀攀✀Ⰰ 椀猀漀氀愀琀攀匀挀漀瀀攀⤀㬀ഀഀ
          }਍ഀഀ
਍ഀഀ
          safeAddClass($linkNode, 'ng-isolate-scope');਍ഀഀ
          forEach(newIsolateScopeDirective.scope, function(definition, scopeName) {਍            瘀愀爀 洀愀琀挀栀 㴀 搀攀昀椀渀椀琀椀漀渀⸀洀愀琀挀栀⠀䰀伀䌀䄀䰀开刀䔀䜀䔀堀倀⤀ 簀簀 嬀崀Ⰰഀഀ
                attrName = match[3] || scopeName,਍                漀瀀琀椀漀渀愀氀 㴀 ⠀洀愀琀挀栀嬀㈀崀 㴀㴀 ✀㼀✀⤀Ⰰഀഀ
                mode = match[1], // @, =, or &਍                氀愀猀琀嘀愀氀甀攀Ⰰഀഀ
                parentGet, parentSet, compare;਍ഀഀ
            isolateScope.$$isolateBindings[scopeName] = mode + attrName;਍ഀഀ
            switch (mode) {਍ഀഀ
              case '@':਍                愀琀琀爀猀⸀␀漀戀猀攀爀瘀攀⠀愀琀琀爀一愀洀攀Ⰰ 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
                  isolateScope[scopeName] = value;਍                紀⤀㬀ഀഀ
                attrs.$$observers[attrName].$$scope = scope;਍                椀昀⠀ 愀琀琀爀猀嬀愀琀琀爀一愀洀攀崀 ⤀ 笀ഀഀ
                  // If the attribute has been provided then we trigger an interpolation to ensure਍                  ⼀⼀ 琀栀攀 瘀愀氀甀攀 椀猀 琀栀攀爀攀 昀漀爀 甀猀攀 椀渀 琀栀攀 氀椀渀欀 昀渀ഀഀ
                  isolateScope[scopeName] = $interpolate(attrs[attrName])(scope);਍                紀ഀഀ
                break;਍ഀഀ
              case '=':਍                椀昀 ⠀漀瀀琀椀漀渀愀氀 ☀☀ ℀愀琀琀爀猀嬀愀琀琀爀一愀洀攀崀⤀ 笀ഀഀ
                  return;਍                紀ഀഀ
                parentGet = $parse(attrs[attrName]);਍                椀昀 ⠀瀀愀爀攀渀琀䜀攀琀⸀氀椀琀攀爀愀氀⤀ 笀ഀഀ
                  compare = equals;਍                紀 攀氀猀攀 笀ഀഀ
                  compare = function(a,b) { return a === b; };਍                紀ഀഀ
                parentSet = parentGet.assign || function() {਍                  ⼀⼀ 爀攀猀攀琀 琀栀攀 挀栀愀渀最攀Ⰰ 漀爀 眀攀 眀椀氀氀 琀栀爀漀眀 琀栀椀猀 攀砀挀攀瀀琀椀漀渀 漀渀 攀瘀攀爀礀 ␀搀椀最攀猀琀ഀഀ
                  lastValue = isolateScope[scopeName] = parentGet(scope);਍                  琀栀爀漀眀 ␀挀漀洀瀀椀氀攀䴀椀渀䔀爀爀⠀✀渀漀渀愀猀猀椀最渀✀Ⰰഀഀ
                      "Expression '{0}' used with directive '{1}' is non-assignable!",਍                      愀琀琀爀猀嬀愀琀琀爀一愀洀攀崀Ⰰ 渀攀眀䤀猀漀氀愀琀攀匀挀漀瀀攀䐀椀爀攀挀琀椀瘀攀⸀渀愀洀攀⤀㬀ഀഀ
                };਍                氀愀猀琀嘀愀氀甀攀 㴀 椀猀漀氀愀琀攀匀挀漀瀀攀嬀猀挀漀瀀攀一愀洀攀崀 㴀 瀀愀爀攀渀琀䜀攀琀⠀猀挀漀瀀攀⤀㬀ഀഀ
                isolateScope.$watch(function parentValueWatch() {਍                  瘀愀爀 瀀愀爀攀渀琀嘀愀氀甀攀 㴀 瀀愀爀攀渀琀䜀攀琀⠀猀挀漀瀀攀⤀㬀ഀഀ
                  if (!compare(parentValue, isolateScope[scopeName])) {਍                    ⼀⼀ 眀攀 愀爀攀 漀甀琀 漀昀 猀礀渀挀 愀渀搀 渀攀攀搀 琀漀 挀漀瀀礀ഀഀ
                    if (!compare(parentValue, lastValue)) {਍                      ⼀⼀ 瀀愀爀攀渀琀 挀栀愀渀最攀搀 愀渀搀 椀琀 栀愀猀 瀀爀攀挀攀搀攀渀挀攀ഀഀ
                      isolateScope[scopeName] = parentValue;਍                    紀 攀氀猀攀 笀ഀഀ
                      // if the parent can be assigned then do so਍                      瀀愀爀攀渀琀匀攀琀⠀猀挀漀瀀攀Ⰰ 瀀愀爀攀渀琀嘀愀氀甀攀 㴀 椀猀漀氀愀琀攀匀挀漀瀀攀嬀猀挀漀瀀攀一愀洀攀崀⤀㬀ഀഀ
                    }਍                  紀ഀഀ
                  return lastValue = parentValue;਍                紀Ⰰ 渀甀氀氀Ⰰ 瀀愀爀攀渀琀䜀攀琀⸀氀椀琀攀爀愀氀⤀㬀ഀഀ
                break;਍ഀഀ
              case '&':਍                瀀愀爀攀渀琀䜀攀琀 㴀 ␀瀀愀爀猀攀⠀愀琀琀爀猀嬀愀琀琀爀一愀洀攀崀⤀㬀ഀഀ
                isolateScope[scopeName] = function(locals) {਍                  爀攀琀甀爀渀 瀀愀爀攀渀琀䜀攀琀⠀猀挀漀瀀攀Ⰰ 氀漀挀愀氀猀⤀㬀ഀഀ
                };਍                戀爀攀愀欀㬀ഀഀ
਍              搀攀昀愀甀氀琀㨀ഀഀ
                throw $compileMinErr('iscp',਍                    ∀䤀渀瘀愀氀椀搀 椀猀漀氀愀琀攀 猀挀漀瀀攀 搀攀昀椀渀椀琀椀漀渀 昀漀爀 搀椀爀攀挀琀椀瘀攀 ✀笀　紀✀⸀∀ ⬀ഀഀ
                    " Definition: {... {1}: '{2}' ...}",਍                    渀攀眀䤀猀漀氀愀琀攀匀挀漀瀀攀䐀椀爀攀挀琀椀瘀攀⸀渀愀洀攀Ⰰ 猀挀漀瀀攀一愀洀攀Ⰰ 搀攀昀椀渀椀琀椀漀渀⤀㬀ഀഀ
            }਍          紀⤀㬀ഀഀ
        }਍        琀爀愀渀猀挀氀甀搀攀䘀渀 㴀 戀漀甀渀搀吀爀愀渀猀挀氀甀搀攀䘀渀 ☀☀ 挀漀渀琀爀漀氀氀攀爀猀䈀漀甀渀搀吀爀愀渀猀挀氀甀搀攀㬀ഀഀ
        if (controllerDirectives) {਍          昀漀爀䔀愀挀栀⠀挀漀渀琀爀漀氀氀攀爀䐀椀爀攀挀琀椀瘀攀猀Ⰰ 昀甀渀挀琀椀漀渀⠀搀椀爀攀挀琀椀瘀攀⤀ 笀ഀഀ
            var locals = {਍              ␀猀挀漀瀀攀㨀 搀椀爀攀挀琀椀瘀攀 㴀㴀㴀 渀攀眀䤀猀漀氀愀琀攀匀挀漀瀀攀䐀椀爀攀挀琀椀瘀攀 簀簀 搀椀爀攀挀琀椀瘀攀⸀␀␀椀猀漀氀愀琀攀匀挀漀瀀攀 㼀 椀猀漀氀愀琀攀匀挀漀瀀攀 㨀 猀挀漀瀀攀Ⰰഀഀ
              $element: $element,਍              ␀愀琀琀爀猀㨀 愀琀琀爀猀Ⰰഀഀ
              $transclude: transcludeFn਍            紀Ⰰ 挀漀渀琀爀漀氀氀攀爀䤀渀猀琀愀渀挀攀㬀ഀഀ
਍            挀漀渀琀爀漀氀氀攀爀 㴀 搀椀爀攀挀琀椀瘀攀⸀挀漀渀琀爀漀氀氀攀爀㬀ഀഀ
            if (controller == '@') {਍              挀漀渀琀爀漀氀氀攀爀 㴀 愀琀琀爀猀嬀搀椀爀攀挀琀椀瘀攀⸀渀愀洀攀崀㬀ഀഀ
            }਍ഀഀ
            controllerInstance = $controller(controller, locals);਍            ⼀⼀ 䘀漀爀 搀椀爀攀挀琀椀瘀攀猀 眀椀琀栀 攀氀攀洀攀渀琀 琀爀愀渀猀挀氀甀猀椀漀渀 琀栀攀 攀氀攀洀攀渀琀 椀猀 愀 挀漀洀洀攀渀琀Ⰰഀഀ
            // but jQuery .data doesn't support attaching data to comment nodes as it's hard to਍            ⼀⼀ 挀氀攀愀渀 甀瀀 ⠀栀琀琀瀀㨀⼀⼀戀甀最猀⸀樀焀甀攀爀礀⸀挀漀洀⼀琀椀挀欀攀琀⼀㠀㌀㌀㔀⤀⸀ഀഀ
            // Instead, we save the controllers for the element in a local hash and attach to .data਍            ⼀⼀ 氀愀琀攀爀Ⰰ 漀渀挀攀 眀攀 栀愀瘀攀 琀栀攀 愀挀琀甀愀氀 攀氀攀洀攀渀琀⸀ഀഀ
            elementControllers[directive.name] = controllerInstance;਍            椀昀 ⠀℀栀愀猀䔀氀攀洀攀渀琀吀爀愀渀猀挀氀甀搀攀䐀椀爀攀挀琀椀瘀攀⤀ 笀ഀഀ
              $element.data('$' + directive.name + 'Controller', controllerInstance);਍            紀ഀഀ
਍            椀昀 ⠀搀椀爀攀挀琀椀瘀攀⸀挀漀渀琀爀漀氀氀攀爀䄀猀⤀ 笀ഀഀ
              locals.$scope[directive.controllerAs] = controllerInstance;਍            紀ഀഀ
          });਍        紀ഀഀ
਍        ⼀⼀ 倀刀䔀䰀䤀一䬀䤀一䜀ഀഀ
        for(i = 0, ii = preLinkFns.length; i < ii; i++) {਍          琀爀礀 笀ഀഀ
            linkFn = preLinkFns[i];਍            氀椀渀欀䘀渀⠀氀椀渀欀䘀渀⸀椀猀漀氀愀琀攀匀挀漀瀀攀 㼀 椀猀漀氀愀琀攀匀挀漀瀀攀 㨀 猀挀漀瀀攀Ⰰ ␀攀氀攀洀攀渀琀Ⰰ 愀琀琀爀猀Ⰰഀഀ
                linkFn.require && getControllers(linkFn.require, $element, elementControllers), transcludeFn);਍          紀 挀愀琀挀栀 ⠀攀⤀ 笀ഀഀ
            $exceptionHandler(e, startingTag($element));਍          紀ഀഀ
        }਍ഀഀ
        // RECURSION਍        ⼀⼀ 圀攀 漀渀氀礀 瀀愀猀猀 琀栀攀 椀猀漀氀愀琀攀 猀挀漀瀀攀Ⰰ 椀昀 琀栀攀 椀猀漀氀愀琀攀 搀椀爀攀挀琀椀瘀攀 栀愀猀 愀 琀攀洀瀀氀愀琀攀Ⰰഀഀ
        // otherwise the child elements do not belong to the isolate directive.਍        瘀愀爀 猀挀漀瀀攀吀漀䌀栀椀氀搀 㴀 猀挀漀瀀攀㬀ഀഀ
        if (newIsolateScopeDirective && (newIsolateScopeDirective.template || newIsolateScopeDirective.templateUrl === null)) {਍          猀挀漀瀀攀吀漀䌀栀椀氀搀 㴀 椀猀漀氀愀琀攀匀挀漀瀀攀㬀ഀഀ
        }਍        挀栀椀氀搀䰀椀渀欀䘀渀 ☀☀ 挀栀椀氀搀䰀椀渀欀䘀渀⠀猀挀漀瀀攀吀漀䌀栀椀氀搀Ⰰ 氀椀渀欀一漀搀攀⸀挀栀椀氀搀一漀搀攀猀Ⰰ 甀渀搀攀昀椀渀攀搀Ⰰ 戀漀甀渀搀吀爀愀渀猀挀氀甀搀攀䘀渀⤀㬀ഀഀ
਍        ⼀⼀ 倀伀匀吀䰀䤀一䬀䤀一䜀ഀഀ
        for(i = postLinkFns.length - 1; i >= 0; i--) {਍          琀爀礀 笀ഀഀ
            linkFn = postLinkFns[i];਍            氀椀渀欀䘀渀⠀氀椀渀欀䘀渀⸀椀猀漀氀愀琀攀匀挀漀瀀攀 㼀 椀猀漀氀愀琀攀匀挀漀瀀攀 㨀 猀挀漀瀀攀Ⰰ ␀攀氀攀洀攀渀琀Ⰰ 愀琀琀爀猀Ⰰഀഀ
                linkFn.require && getControllers(linkFn.require, $element, elementControllers), transcludeFn);਍          紀 挀愀琀挀栀 ⠀攀⤀ 笀ഀഀ
            $exceptionHandler(e, startingTag($element));਍          紀ഀഀ
        }਍ഀഀ
        // This is the function that is injected as `$transclude`.਍        昀甀渀挀琀椀漀渀 挀漀渀琀爀漀氀氀攀爀猀䈀漀甀渀搀吀爀愀渀猀挀氀甀搀攀⠀猀挀漀瀀攀Ⰰ 挀氀漀渀攀䄀琀琀愀挀栀䘀渀⤀ 笀ഀഀ
          var transcludeControllers;਍ഀഀ
          // no scope passed਍          椀昀 ⠀愀爀最甀洀攀渀琀猀⸀氀攀渀最琀栀 㰀 ㈀⤀ 笀ഀഀ
            cloneAttachFn = scope;਍            猀挀漀瀀攀 㴀 甀渀搀攀昀椀渀攀搀㬀ഀഀ
          }਍ഀഀ
          if (hasElementTranscludeDirective) {਍            琀爀愀渀猀挀氀甀搀攀䌀漀渀琀爀漀氀氀攀爀猀 㴀 攀氀攀洀攀渀琀䌀漀渀琀爀漀氀氀攀爀猀㬀ഀഀ
          }਍ഀഀ
          return boundTranscludeFn(scope, cloneAttachFn, transcludeControllers);਍        紀ഀഀ
      }਍    紀ഀഀ
਍    昀甀渀挀琀椀漀渀 洀愀爀欀䐀椀爀攀挀琀椀瘀攀猀䄀猀䤀猀漀氀愀琀攀⠀搀椀爀攀挀琀椀瘀攀猀⤀ 笀ഀഀ
      // mark all directives as needing isolate scope.਍      昀漀爀 ⠀瘀愀爀 樀 㴀 　Ⰰ 樀樀 㴀 搀椀爀攀挀琀椀瘀攀猀⸀氀攀渀最琀栀㬀 樀 㰀 樀樀㬀 樀⬀⬀⤀ 笀ഀഀ
        directives[j] = inherit(directives[j], {$$isolateScope: true});਍      紀ഀഀ
    }਍ഀഀ
    /**਍     ⨀ 氀漀漀欀猀 甀瀀 琀栀攀 搀椀爀攀挀琀椀瘀攀 愀渀搀 搀攀挀漀爀愀琀攀猀 椀琀 眀椀琀栀 攀砀挀攀瀀琀椀漀渀 栀愀渀搀氀椀渀最 愀渀搀 瀀爀漀瀀攀爀 瀀愀爀愀洀攀琀攀爀猀⸀ 圀攀ഀഀ
     * call this the boundDirective.਍     ⨀ഀഀ
     * @param {string} name name of the directive to look up.਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 氀漀挀愀琀椀漀渀 吀栀攀 搀椀爀攀挀琀椀瘀攀 洀甀猀琀 戀攀 昀漀甀渀搀 椀渀 猀瀀攀挀椀昀椀挀 昀漀爀洀愀琀⸀ഀഀ
     *   String containing any of theses characters:਍     ⨀ഀഀ
     *   * `E`: element name਍     ⨀   ⨀ 怀䄀✀㨀 愀琀琀爀椀戀甀琀攀ഀഀ
     *   * `C`: class਍     ⨀   ⨀ 怀䴀怀㨀 挀漀洀洀攀渀琀ഀഀ
     * @returns true if directive was added.਍     ⨀⼀ഀഀ
    function addDirective(tDirectives, name, location, maxPriority, ignoreDirective, startAttrName,਍                          攀渀搀䄀琀琀爀一愀洀攀⤀ 笀ഀഀ
      if (name === ignoreDirective) return null;਍      瘀愀爀 洀愀琀挀栀 㴀 渀甀氀氀㬀ഀഀ
      if (hasDirectives.hasOwnProperty(name)) {਍        昀漀爀⠀瘀愀爀 搀椀爀攀挀琀椀瘀攀Ⰰ 搀椀爀攀挀琀椀瘀攀猀 㴀 ␀椀渀樀攀挀琀漀爀⸀最攀琀⠀渀愀洀攀 ⬀ 匀甀昀昀椀砀⤀Ⰰഀഀ
            i = 0, ii = directives.length; i<ii; i++) {਍          琀爀礀 笀ഀഀ
            directive = directives[i];਍            椀昀 ⠀ ⠀洀愀砀倀爀椀漀爀椀琀礀 㴀㴀㴀 甀渀搀攀昀椀渀攀搀 簀簀 洀愀砀倀爀椀漀爀椀琀礀 㸀 搀椀爀攀挀琀椀瘀攀⸀瀀爀椀漀爀椀琀礀⤀ ☀☀ഀഀ
                 directive.restrict.indexOf(location) != -1) {਍              椀昀 ⠀猀琀愀爀琀䄀琀琀爀一愀洀攀⤀ 笀ഀഀ
                directive = inherit(directive, {$$start: startAttrName, $$end: endAttrName});਍              紀ഀഀ
              tDirectives.push(directive);਍              洀愀琀挀栀 㴀 搀椀爀攀挀琀椀瘀攀㬀ഀഀ
            }਍          紀 挀愀琀挀栀⠀攀⤀ 笀 ␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀⠀攀⤀㬀 紀ഀഀ
        }਍      紀ഀഀ
      return match;਍    紀ഀഀ
਍ഀഀ
    /**਍     ⨀ 圀栀攀渀 琀栀攀 攀氀攀洀攀渀琀 椀猀 爀攀瀀氀愀挀攀搀 眀椀琀栀 䠀吀䴀䰀 琀攀洀瀀氀愀琀攀 琀栀攀渀 琀栀攀 渀攀眀 愀琀琀爀椀戀甀琀攀猀ഀഀ
     * on the template need to be merged with the existing attributes in the DOM.਍     ⨀ 吀栀攀 搀攀猀椀爀攀搀 攀昀昀攀挀琀 椀猀 琀漀 栀愀瘀攀 戀漀琀栀 漀昀 琀栀攀 愀琀琀爀椀戀甀琀攀猀 瀀爀攀猀攀渀琀⸀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀漀戀樀攀挀琀紀 搀猀琀 搀攀猀琀椀渀愀琀椀漀渀 愀琀琀爀椀戀甀琀攀猀 ⠀漀爀椀最椀渀愀氀 䐀伀䴀⤀ഀഀ
     * @param {object} src source attributes (from the directive template)਍     ⨀⼀ഀഀ
    function mergeTemplateAttributes(dst, src) {਍      瘀愀爀 猀爀挀䄀琀琀爀 㴀 猀爀挀⸀␀愀琀琀爀Ⰰഀഀ
          dstAttr = dst.$attr,਍          ␀攀氀攀洀攀渀琀 㴀 搀猀琀⸀␀␀攀氀攀洀攀渀琀㬀ഀഀ
਍      ⼀⼀ 爀攀愀瀀瀀氀礀 琀栀攀 漀氀搀 愀琀琀爀椀戀甀琀攀猀 琀漀 琀栀攀 渀攀眀 攀氀攀洀攀渀琀ഀഀ
      forEach(dst, function(value, key) {਍        椀昀 ⠀欀攀礀⸀挀栀愀爀䄀琀⠀　⤀ ℀㴀 ✀␀✀⤀ 笀ഀഀ
          if (src[key]) {਍            瘀愀氀甀攀 ⬀㴀 ⠀欀攀礀 㴀㴀㴀 ✀猀琀礀氀攀✀ 㼀 ✀㬀✀ 㨀 ✀ ✀⤀ ⬀ 猀爀挀嬀欀攀礀崀㬀ഀഀ
          }਍          搀猀琀⸀␀猀攀琀⠀欀攀礀Ⰰ 瘀愀氀甀攀Ⰰ 琀爀甀攀Ⰰ 猀爀挀䄀琀琀爀嬀欀攀礀崀⤀㬀ഀഀ
        }਍      紀⤀㬀ഀഀ
਍      ⼀⼀ 挀漀瀀礀 琀栀攀 渀攀眀 愀琀琀爀椀戀甀琀攀猀 漀渀 琀栀攀 漀氀搀 愀琀琀爀猀 漀戀樀攀挀琀ഀഀ
      forEach(src, function(value, key) {਍        椀昀 ⠀欀攀礀 㴀㴀 ✀挀氀愀猀猀✀⤀ 笀ഀഀ
          safeAddClass($element, value);਍          搀猀琀嬀✀挀氀愀猀猀✀崀 㴀 ⠀搀猀琀嬀✀挀氀愀猀猀✀崀 㼀 搀猀琀嬀✀挀氀愀猀猀✀崀 ⬀ ✀ ✀ 㨀 ✀✀⤀ ⬀ 瘀愀氀甀攀㬀ഀഀ
        } else if (key == 'style') {਍          ␀攀氀攀洀攀渀琀⸀愀琀琀爀⠀✀猀琀礀氀攀✀Ⰰ ␀攀氀攀洀攀渀琀⸀愀琀琀爀⠀✀猀琀礀氀攀✀⤀ ⬀ ✀㬀✀ ⬀ 瘀愀氀甀攀⤀㬀ഀഀ
          dst['style'] = (dst['style'] ? dst['style'] + ';' : '') + value;਍          ⼀⼀ 怀搀猀琀怀 眀椀氀氀 渀攀瘀攀爀 挀漀渀琀愀椀渀 栀愀猀伀眀渀倀爀漀瀀攀爀琀礀 愀猀 䐀伀䴀 瀀愀爀猀攀爀 眀漀渀✀琀 氀攀琀 椀琀⸀ഀഀ
          // You will get an "InvalidCharacterError: DOM Exception 5" error if you਍          ⼀⼀ 栀愀瘀攀 愀渀 愀琀琀爀椀戀甀琀攀 氀椀欀攀 ∀栀愀猀ⴀ漀眀渀ⴀ瀀爀漀瀀攀爀琀礀∀ 漀爀 ∀搀愀琀愀ⴀ栀愀猀ⴀ漀眀渀ⴀ瀀爀漀瀀攀爀琀礀∀Ⰰ 攀琀挀⸀ഀഀ
        } else if (key.charAt(0) != '$' && !dst.hasOwnProperty(key)) {਍          搀猀琀嬀欀攀礀崀 㴀 瘀愀氀甀攀㬀ഀഀ
          dstAttr[key] = srcAttr[key];਍        紀ഀഀ
      });਍    紀ഀഀ
਍ഀഀ
    function compileTemplateUrl(directives, $compileNode, tAttrs,਍        ␀爀漀漀琀䔀氀攀洀攀渀琀Ⰰ 挀栀椀氀搀吀爀愀渀猀挀氀甀搀攀䘀渀Ⰰ 瀀爀攀䰀椀渀欀䘀渀猀Ⰰ 瀀漀猀琀䰀椀渀欀䘀渀猀Ⰰ 瀀爀攀瘀椀漀甀猀䌀漀洀瀀椀氀攀䌀漀渀琀攀砀琀⤀ 笀ഀഀ
      var linkQueue = [],਍          愀昀琀攀爀吀攀洀瀀氀愀琀攀一漀搀攀䰀椀渀欀䘀渀Ⰰഀഀ
          afterTemplateChildLinkFn,਍          戀攀昀漀爀攀吀攀洀瀀氀愀琀攀䌀漀洀瀀椀氀攀一漀搀攀 㴀 ␀挀漀洀瀀椀氀攀一漀搀攀嬀　崀Ⰰഀഀ
          origAsyncDirective = directives.shift(),਍          ⼀⼀ 吀栀攀 昀愀挀琀 琀栀愀琀 眀攀 栀愀瘀攀 琀漀 挀漀瀀礀 愀渀搀 瀀愀琀挀栀 琀栀攀 搀椀爀攀挀琀椀瘀攀 猀攀攀洀猀 眀爀漀渀最℀ഀഀ
          derivedSyncDirective = extend({}, origAsyncDirective, {਍            琀攀洀瀀氀愀琀攀唀爀氀㨀 渀甀氀氀Ⰰ 琀爀愀渀猀挀氀甀搀攀㨀 渀甀氀氀Ⰰ 爀攀瀀氀愀挀攀㨀 渀甀氀氀Ⰰ ␀␀漀爀椀最椀渀愀氀䐀椀爀攀挀琀椀瘀攀㨀 漀爀椀最䄀猀礀渀挀䐀椀爀攀挀琀椀瘀攀ഀഀ
          }),਍          琀攀洀瀀氀愀琀攀唀爀氀 㴀 ⠀椀猀䘀甀渀挀琀椀漀渀⠀漀爀椀最䄀猀礀渀挀䐀椀爀攀挀琀椀瘀攀⸀琀攀洀瀀氀愀琀攀唀爀氀⤀⤀ഀഀ
              ? origAsyncDirective.templateUrl($compileNode, tAttrs)਍              㨀 漀爀椀最䄀猀礀渀挀䐀椀爀攀挀琀椀瘀攀⸀琀攀洀瀀氀愀琀攀唀爀氀㬀ഀഀ
਍      ␀挀漀洀瀀椀氀攀一漀搀攀⸀攀洀瀀琀礀⠀⤀㬀ഀഀ
਍      ␀栀琀琀瀀⸀最攀琀⠀␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀刀攀猀漀甀爀挀攀唀爀氀⠀琀攀洀瀀氀愀琀攀唀爀氀⤀Ⰰ 笀挀愀挀栀攀㨀 ␀琀攀洀瀀氀愀琀攀䌀愀挀栀攀紀⤀⸀ഀഀ
        success(function(content) {਍          瘀愀爀 挀漀洀瀀椀氀攀一漀搀攀Ⰰ 琀攀洀瀀吀攀洀瀀氀愀琀攀䄀琀琀爀猀Ⰰ ␀琀攀洀瀀氀愀琀攀Ⰰ 挀栀椀氀搀䈀漀甀渀搀吀爀愀渀猀挀氀甀搀攀䘀渀㬀ഀഀ
਍          挀漀渀琀攀渀琀 㴀 搀攀渀漀爀洀愀氀椀稀攀吀攀洀瀀氀愀琀攀⠀挀漀渀琀攀渀琀⤀㬀ഀഀ
਍          椀昀 ⠀漀爀椀最䄀猀礀渀挀䐀椀爀攀挀琀椀瘀攀⸀爀攀瀀氀愀挀攀⤀ 笀ഀഀ
            $template = jqLite('<div>' + trim(content) + '</div>').contents();਍            挀漀洀瀀椀氀攀一漀搀攀 㴀 ␀琀攀洀瀀氀愀琀攀嬀　崀㬀ഀഀ
਍            椀昀 ⠀␀琀攀洀瀀氀愀琀攀⸀氀攀渀最琀栀 ℀㴀 ㄀ 簀簀 挀漀洀瀀椀氀攀一漀搀攀⸀渀漀搀攀吀礀瀀攀 ℀㴀㴀 ㄀⤀ 笀ഀഀ
              throw $compileMinErr('tplrt',਍                  ∀吀攀洀瀀氀愀琀攀 昀漀爀 搀椀爀攀挀琀椀瘀攀 ✀笀　紀✀ 洀甀猀琀 栀愀瘀攀 攀砀愀挀琀氀礀 漀渀攀 爀漀漀琀 攀氀攀洀攀渀琀⸀ 笀㄀紀∀Ⰰഀഀ
                  origAsyncDirective.name, templateUrl);਍            紀ഀഀ
਍            琀攀洀瀀吀攀洀瀀氀愀琀攀䄀琀琀爀猀 㴀 笀␀愀琀琀爀㨀 笀紀紀㬀ഀഀ
            replaceWith($rootElement, $compileNode, compileNode);਍            瘀愀爀 琀攀洀瀀氀愀琀攀䐀椀爀攀挀琀椀瘀攀猀 㴀 挀漀氀氀攀挀琀䐀椀爀攀挀琀椀瘀攀猀⠀挀漀洀瀀椀氀攀一漀搀攀Ⰰ 嬀崀Ⰰ 琀攀洀瀀吀攀洀瀀氀愀琀攀䄀琀琀爀猀⤀㬀ഀഀ
਍            椀昀 ⠀椀猀伀戀樀攀挀琀⠀漀爀椀最䄀猀礀渀挀䐀椀爀攀挀琀椀瘀攀⸀猀挀漀瀀攀⤀⤀ 笀ഀഀ
              markDirectivesAsIsolate(templateDirectives);਍            紀ഀഀ
            directives = templateDirectives.concat(directives);਍            洀攀爀最攀吀攀洀瀀氀愀琀攀䄀琀琀爀椀戀甀琀攀猀⠀琀䄀琀琀爀猀Ⰰ 琀攀洀瀀吀攀洀瀀氀愀琀攀䄀琀琀爀猀⤀㬀ഀഀ
          } else {਍            挀漀洀瀀椀氀攀一漀搀攀 㴀 戀攀昀漀爀攀吀攀洀瀀氀愀琀攀䌀漀洀瀀椀氀攀一漀搀攀㬀ഀഀ
            $compileNode.html(content);਍          紀ഀഀ
਍          搀椀爀攀挀琀椀瘀攀猀⸀甀渀猀栀椀昀琀⠀搀攀爀椀瘀攀搀匀礀渀挀䐀椀爀攀挀琀椀瘀攀⤀㬀ഀഀ
਍          愀昀琀攀爀吀攀洀瀀氀愀琀攀一漀搀攀䰀椀渀欀䘀渀 㴀 愀瀀瀀氀礀䐀椀爀攀挀琀椀瘀攀猀吀漀一漀搀攀⠀搀椀爀攀挀琀椀瘀攀猀Ⰰ 挀漀洀瀀椀氀攀一漀搀攀Ⰰ 琀䄀琀琀爀猀Ⰰഀഀ
              childTranscludeFn, $compileNode, origAsyncDirective, preLinkFns, postLinkFns,਍              瀀爀攀瘀椀漀甀猀䌀漀洀瀀椀氀攀䌀漀渀琀攀砀琀⤀㬀ഀഀ
          forEach($rootElement, function(node, i) {਍            椀昀 ⠀渀漀搀攀 㴀㴀 挀漀洀瀀椀氀攀一漀搀攀⤀ 笀ഀഀ
              $rootElement[i] = $compileNode[0];਍            紀ഀഀ
          });਍          愀昀琀攀爀吀攀洀瀀氀愀琀攀䌀栀椀氀搀䰀椀渀欀䘀渀 㴀 挀漀洀瀀椀氀攀一漀搀攀猀⠀␀挀漀洀瀀椀氀攀一漀搀攀嬀　崀⸀挀栀椀氀搀一漀搀攀猀Ⰰ 挀栀椀氀搀吀爀愀渀猀挀氀甀搀攀䘀渀⤀㬀ഀഀ
਍ഀഀ
          while(linkQueue.length) {਍            瘀愀爀 猀挀漀瀀攀 㴀 氀椀渀欀儀甀攀甀攀⸀猀栀椀昀琀⠀⤀Ⰰഀഀ
                beforeTemplateLinkNode = linkQueue.shift(),਍                氀椀渀欀刀漀漀琀䔀氀攀洀攀渀琀 㴀 氀椀渀欀儀甀攀甀攀⸀猀栀椀昀琀⠀⤀Ⰰഀഀ
                boundTranscludeFn = linkQueue.shift(),਍                氀椀渀欀一漀搀攀 㴀 ␀挀漀洀瀀椀氀攀一漀搀攀嬀　崀㬀ഀഀ
਍            椀昀 ⠀戀攀昀漀爀攀吀攀洀瀀氀愀琀攀䰀椀渀欀一漀搀攀 ℀㴀㴀 戀攀昀漀爀攀吀攀洀瀀氀愀琀攀䌀漀洀瀀椀氀攀一漀搀攀⤀ 笀ഀഀ
              // it was cloned therefore we have to clone as well.਍              氀椀渀欀一漀搀攀 㴀 樀焀䰀椀琀攀䌀氀漀渀攀⠀挀漀洀瀀椀氀攀一漀搀攀⤀㬀ഀഀ
              replaceWith(linkRootElement, jqLite(beforeTemplateLinkNode), linkNode);਍            紀ഀഀ
            if (afterTemplateNodeLinkFn.transclude) {਍              挀栀椀氀搀䈀漀甀渀搀吀爀愀渀猀挀氀甀搀攀䘀渀 㴀 挀爀攀愀琀攀䈀漀甀渀搀吀爀愀渀猀挀氀甀搀攀䘀渀⠀猀挀漀瀀攀Ⰰ 愀昀琀攀爀吀攀洀瀀氀愀琀攀一漀搀攀䰀椀渀欀䘀渀⸀琀爀愀渀猀挀氀甀搀攀⤀㬀ഀഀ
            } else {਍              挀栀椀氀搀䈀漀甀渀搀吀爀愀渀猀挀氀甀搀攀䘀渀 㴀 戀漀甀渀搀吀爀愀渀猀挀氀甀搀攀䘀渀㬀ഀഀ
            }਍            愀昀琀攀爀吀攀洀瀀氀愀琀攀一漀搀攀䰀椀渀欀䘀渀⠀愀昀琀攀爀吀攀洀瀀氀愀琀攀䌀栀椀氀搀䰀椀渀欀䘀渀Ⰰ 猀挀漀瀀攀Ⰰ 氀椀渀欀一漀搀攀Ⰰ ␀爀漀漀琀䔀氀攀洀攀渀琀Ⰰഀഀ
              childBoundTranscludeFn);਍          紀ഀഀ
          linkQueue = null;਍        紀⤀⸀ഀഀ
        error(function(response, code, headers, config) {਍          琀栀爀漀眀 ␀挀漀洀瀀椀氀攀䴀椀渀䔀爀爀⠀✀琀瀀氀漀愀搀✀Ⰰ ✀䘀愀椀氀攀搀 琀漀 氀漀愀搀 琀攀洀瀀氀愀琀攀㨀 笀　紀✀Ⰰ 挀漀渀昀椀最⸀甀爀氀⤀㬀ഀഀ
        });਍ഀഀ
      return function delayedNodeLinkFn(ignoreChildLinkFn, scope, node, rootElement, boundTranscludeFn) {਍        椀昀 ⠀氀椀渀欀儀甀攀甀攀⤀ 笀ഀഀ
          linkQueue.push(scope);਍          氀椀渀欀儀甀攀甀攀⸀瀀甀猀栀⠀渀漀搀攀⤀㬀ഀഀ
          linkQueue.push(rootElement);਍          氀椀渀欀儀甀攀甀攀⸀瀀甀猀栀⠀戀漀甀渀搀吀爀愀渀猀挀氀甀搀攀䘀渀⤀㬀ഀഀ
        } else {਍          愀昀琀攀爀吀攀洀瀀氀愀琀攀一漀搀攀䰀椀渀欀䘀渀⠀愀昀琀攀爀吀攀洀瀀氀愀琀攀䌀栀椀氀搀䰀椀渀欀䘀渀Ⰰ 猀挀漀瀀攀Ⰰ 渀漀搀攀Ⰰ 爀漀漀琀䔀氀攀洀攀渀琀Ⰰ 戀漀甀渀搀吀爀愀渀猀挀氀甀搀攀䘀渀⤀㬀ഀഀ
        }਍      紀㬀ഀഀ
    }਍ഀഀ
਍    ⼀⨀⨀ഀഀ
     * Sorting function for bound directives.਍     ⨀⼀ഀഀ
    function byPriority(a, b) {਍      瘀愀爀 搀椀昀昀 㴀 戀⸀瀀爀椀漀爀椀琀礀 ⴀ 愀⸀瀀爀椀漀爀椀琀礀㬀ഀഀ
      if (diff !== 0) return diff;਍      椀昀 ⠀愀⸀渀愀洀攀 ℀㴀㴀 戀⸀渀愀洀攀⤀ 爀攀琀甀爀渀 ⠀愀⸀渀愀洀攀 㰀 戀⸀渀愀洀攀⤀ 㼀 ⴀ㄀ 㨀 ㄀㬀ഀഀ
      return a.index - b.index;਍    紀ഀഀ
਍ഀഀ
    function assertNoDuplicate(what, previousDirective, directive, element) {਍      椀昀 ⠀瀀爀攀瘀椀漀甀猀䐀椀爀攀挀琀椀瘀攀⤀ 笀ഀഀ
        throw $compileMinErr('multidir', 'Multiple directives [{0}, {1}] asking for {2} on: {3}',਍            瀀爀攀瘀椀漀甀猀䐀椀爀攀挀琀椀瘀攀⸀渀愀洀攀Ⰰ 搀椀爀攀挀琀椀瘀攀⸀渀愀洀攀Ⰰ 眀栀愀琀Ⰰ 猀琀愀爀琀椀渀最吀愀最⠀攀氀攀洀攀渀琀⤀⤀㬀ഀഀ
      }਍    紀ഀഀ
਍ഀഀ
    function addTextInterpolateDirective(directives, text) {਍      瘀愀爀 椀渀琀攀爀瀀漀氀愀琀攀䘀渀 㴀 ␀椀渀琀攀爀瀀漀氀愀琀攀⠀琀攀砀琀Ⰰ 琀爀甀攀⤀㬀ഀഀ
      if (interpolateFn) {਍        搀椀爀攀挀琀椀瘀攀猀⸀瀀甀猀栀⠀笀ഀഀ
          priority: 0,਍          挀漀洀瀀椀氀攀㨀 瘀愀氀甀攀䘀渀⠀昀甀渀挀琀椀漀渀 琀攀砀琀䤀渀琀攀爀瀀漀氀愀琀攀䰀椀渀欀䘀渀⠀猀挀漀瀀攀Ⰰ 渀漀搀攀⤀ 笀ഀഀ
            var parent = node.parent(),਍                戀椀渀搀椀渀最猀 㴀 瀀愀爀攀渀琀⸀搀愀琀愀⠀✀␀戀椀渀搀椀渀最✀⤀ 簀簀 嬀崀㬀ഀഀ
            bindings.push(interpolateFn);਍            猀愀昀攀䄀搀搀䌀氀愀猀猀⠀瀀愀爀攀渀琀⸀搀愀琀愀⠀✀␀戀椀渀搀椀渀最✀Ⰰ 戀椀渀搀椀渀最猀⤀Ⰰ ✀渀最ⴀ戀椀渀搀椀渀最✀⤀㬀ഀഀ
            scope.$watch(interpolateFn, function interpolateFnWatchAction(value) {਍              渀漀搀攀嬀　崀⸀渀漀搀攀嘀愀氀甀攀 㴀 瘀愀氀甀攀㬀ഀഀ
            });਍          紀⤀ഀഀ
        });਍      紀ഀഀ
    }਍ഀഀ
਍    昀甀渀挀琀椀漀渀 最攀琀吀爀甀猀琀攀搀䌀漀渀琀攀砀琀⠀渀漀搀攀Ⰰ 愀琀琀爀一漀爀洀愀氀椀稀攀搀一愀洀攀⤀ 笀ഀഀ
      if (attrNormalizedName == "srcdoc") {਍        爀攀琀甀爀渀 ␀猀挀攀⸀䠀吀䴀䰀㬀ഀഀ
      }਍      瘀愀爀 琀愀最 㴀 渀漀搀攀一愀洀攀开⠀渀漀搀攀⤀㬀ഀഀ
      // maction[xlink:href] can source SVG.  It's not limited to <maction>.਍      椀昀 ⠀愀琀琀爀一漀爀洀愀氀椀稀攀搀一愀洀攀 㴀㴀 ∀砀氀椀渀欀䠀爀攀昀∀ 簀簀ഀഀ
          (tag == "FORM" && attrNormalizedName == "action") ||਍          ⠀琀愀最 ℀㴀 ∀䤀䴀䜀∀ ☀☀ ⠀愀琀琀爀一漀爀洀愀氀椀稀攀搀一愀洀攀 㴀㴀 ∀猀爀挀∀ 簀簀ഀഀ
                            attrNormalizedName == "ngSrc"))) {਍        爀攀琀甀爀渀 ␀猀挀攀⸀刀䔀匀伀唀刀䌀䔀开唀刀䰀㬀ഀഀ
      }਍    紀ഀഀ
਍ഀഀ
    function addAttrInterpolateDirective(node, directives, value, name) {਍      瘀愀爀 椀渀琀攀爀瀀漀氀愀琀攀䘀渀 㴀 ␀椀渀琀攀爀瀀漀氀愀琀攀⠀瘀愀氀甀攀Ⰰ 琀爀甀攀⤀㬀ഀഀ
਍      ⼀⼀ 渀漀 椀渀琀攀爀瀀漀氀愀琀椀漀渀 昀漀甀渀搀 ⴀ㸀 椀最渀漀爀攀ഀഀ
      if (!interpolateFn) return;਍ഀഀ
਍      椀昀 ⠀渀愀洀攀 㴀㴀㴀 ∀洀甀氀琀椀瀀氀攀∀ ☀☀ 渀漀搀攀一愀洀攀开⠀渀漀搀攀⤀ 㴀㴀㴀 ∀匀䔀䰀䔀䌀吀∀⤀ 笀ഀഀ
        throw $compileMinErr("selmulti",਍            ∀䈀椀渀搀椀渀最 琀漀 琀栀攀 ✀洀甀氀琀椀瀀氀攀✀ 愀琀琀爀椀戀甀琀攀 椀猀 渀漀琀 猀甀瀀瀀漀爀琀攀搀⸀ 䔀氀攀洀攀渀琀㨀 笀　紀∀Ⰰഀഀ
            startingTag(node));਍      紀ഀഀ
਍      搀椀爀攀挀琀椀瘀攀猀⸀瀀甀猀栀⠀笀ഀഀ
        priority: 100,਍        挀漀洀瀀椀氀攀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
            return {਍              瀀爀攀㨀 昀甀渀挀琀椀漀渀 愀琀琀爀䤀渀琀攀爀瀀漀氀愀琀攀倀爀攀䰀椀渀欀䘀渀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀⤀ 笀ഀഀ
                var $$observers = (attr.$$observers || (attr.$$observers = {}));਍ഀഀ
                if (EVENT_HANDLER_ATTR_REGEXP.test(name)) {਍                  琀栀爀漀眀 ␀挀漀洀瀀椀氀攀䴀椀渀䔀爀爀⠀✀渀漀搀漀洀攀瘀攀渀琀猀✀Ⰰഀഀ
                      "Interpolations for HTML DOM event attributes are disallowed.  Please use the " +਍                          ∀渀最ⴀ 瘀攀爀猀椀漀渀猀 ⠀猀甀挀栀 愀猀 渀最ⴀ挀氀椀挀欀 椀渀猀琀攀愀搀 漀昀 漀渀挀氀椀挀欀⤀ 椀渀猀琀攀愀搀⸀∀⤀㬀ഀഀ
                }਍ഀഀ
                // we need to interpolate again, in case the attribute value has been updated਍                ⼀⼀ ⠀攀⸀最⸀ 戀礀 愀渀漀琀栀攀爀 搀椀爀攀挀琀椀瘀攀✀猀 挀漀洀瀀椀氀攀 昀甀渀挀琀椀漀渀⤀ഀഀ
                interpolateFn = $interpolate(attr[name], true, getTrustedContext(node, name));਍ഀഀ
                // if attribute was updated so that there is no interpolation going on we don't want to਍                ⼀⼀ 爀攀最椀猀琀攀爀 愀渀礀 漀戀猀攀爀瘀攀爀猀ഀഀ
                if (!interpolateFn) return;਍ഀഀ
                // TODO(i): this should likely be attr.$set(name, iterpolateFn(scope) so that we reset the਍                ⼀⼀ 愀挀琀甀愀氀 愀琀琀爀 瘀愀氀甀攀ഀഀ
                attr[name] = interpolateFn(scope);਍                ⠀␀␀漀戀猀攀爀瘀攀爀猀嬀渀愀洀攀崀 簀簀 ⠀␀␀漀戀猀攀爀瘀攀爀猀嬀渀愀洀攀崀 㴀 嬀崀⤀⤀⸀␀␀椀渀琀攀爀 㴀 琀爀甀攀㬀ഀഀ
                (attr.$$observers && attr.$$observers[name].$$scope || scope).਍                  ␀眀愀琀挀栀⠀椀渀琀攀爀瀀漀氀愀琀攀䘀渀Ⰰ 昀甀渀挀琀椀漀渀 椀渀琀攀爀瀀漀氀愀琀攀䘀渀圀愀琀挀栀䄀挀琀椀漀渀⠀渀攀眀嘀愀氀甀攀Ⰰ 漀氀搀嘀愀氀甀攀⤀ 笀ഀഀ
                    //special case for class attribute addition + removal਍                    ⼀⼀猀漀 琀栀愀琀 挀氀愀猀猀 挀栀愀渀最攀猀 挀愀渀 琀愀瀀 椀渀琀漀 琀栀攀 愀渀椀洀愀琀椀漀渀ഀഀ
                    //hooks provided by the $animate service. Be sure to਍                    ⼀⼀猀欀椀瀀 愀渀椀洀愀琀椀漀渀猀 眀栀攀渀 琀栀攀 昀椀爀猀琀 搀椀最攀猀琀 漀挀挀甀爀猀 ⠀眀栀攀渀ഀഀ
                    //both the new and the old values are the same) since਍                    ⼀⼀琀栀攀 䌀匀匀 挀氀愀猀猀攀猀 愀爀攀 琀栀攀 渀漀渀ⴀ椀渀琀攀爀瀀漀氀愀琀攀搀 瘀愀氀甀攀猀ഀഀ
                    if(name === 'class' && newValue != oldValue) {਍                      愀琀琀爀⸀␀甀瀀搀愀琀攀䌀氀愀猀猀⠀渀攀眀嘀愀氀甀攀Ⰰ 漀氀搀嘀愀氀甀攀⤀㬀ഀഀ
                    } else {਍                      愀琀琀爀⸀␀猀攀琀⠀渀愀洀攀Ⰰ 渀攀眀嘀愀氀甀攀⤀㬀ഀഀ
                    }਍                  紀⤀㬀ഀഀ
              }਍            紀㬀ഀഀ
          }਍      紀⤀㬀ഀഀ
    }਍ഀഀ
਍    ⼀⨀⨀ഀഀ
     * This is a special jqLite.replaceWith, which can replace items which਍     ⨀ 栀愀瘀攀 渀漀 瀀愀爀攀渀琀猀Ⰰ 瀀爀漀瘀椀搀攀搀 琀栀愀琀 琀栀攀 挀漀渀琀愀椀渀椀渀最 樀焀䰀椀琀攀 挀漀氀氀攀挀琀椀漀渀 椀猀 瀀爀漀瘀椀搀攀搀⸀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀䨀焀䰀椀琀攀㴀紀 ␀爀漀漀琀䔀氀攀洀攀渀琀 吀栀攀 爀漀漀琀 漀昀 琀栀攀 挀漀洀瀀椀氀攀 琀爀攀攀⸀ 唀猀攀搀 猀漀 琀栀愀琀 眀攀 挀愀渀 爀攀瀀氀愀挀攀 渀漀搀攀猀ഀഀ
     *                               in the root of the tree.਍     ⨀ 䀀瀀愀爀愀洀 笀䨀焀䰀椀琀攀紀 攀氀攀洀攀渀琀猀吀漀刀攀洀漀瘀攀 吀栀攀 樀焀䰀椀琀攀 攀氀攀洀攀渀琀 眀栀椀挀栀 眀攀 愀爀攀 最漀椀渀最 琀漀 爀攀瀀氀愀挀攀⸀ 圀攀 欀攀攀瀀ഀഀ
     *                                  the shell, but replace its DOM node reference.਍     ⨀ 䀀瀀愀爀愀洀 笀一漀搀攀紀 渀攀眀一漀搀攀 吀栀攀 渀攀眀 䐀伀䴀 渀漀搀攀⸀ഀഀ
     */਍    昀甀渀挀琀椀漀渀 爀攀瀀氀愀挀攀圀椀琀栀⠀␀爀漀漀琀䔀氀攀洀攀渀琀Ⰰ 攀氀攀洀攀渀琀猀吀漀刀攀洀漀瘀攀Ⰰ 渀攀眀一漀搀攀⤀ 笀ഀഀ
      var firstElementToRemove = elementsToRemove[0],਍          爀攀洀漀瘀攀䌀漀甀渀琀 㴀 攀氀攀洀攀渀琀猀吀漀刀攀洀漀瘀攀⸀氀攀渀最琀栀Ⰰഀഀ
          parent = firstElementToRemove.parentNode,਍          椀Ⰰ 椀椀㬀ഀഀ
਍      椀昀 ⠀␀爀漀漀琀䔀氀攀洀攀渀琀⤀ 笀ഀഀ
        for(i = 0, ii = $rootElement.length; i < ii; i++) {਍          椀昀 ⠀␀爀漀漀琀䔀氀攀洀攀渀琀嬀椀崀 㴀㴀 昀椀爀猀琀䔀氀攀洀攀渀琀吀漀刀攀洀漀瘀攀⤀ 笀ഀഀ
            $rootElement[i++] = newNode;਍            昀漀爀 ⠀瘀愀爀 樀 㴀 椀Ⰰ 樀㈀ 㴀 樀 ⬀ 爀攀洀漀瘀攀䌀漀甀渀琀 ⴀ ㄀Ⰰഀഀ
                     jj = $rootElement.length;਍                 樀 㰀 樀樀㬀 樀⬀⬀Ⰰ 樀㈀⬀⬀⤀ 笀ഀഀ
              if (j2 < jj) {਍                ␀爀漀漀琀䔀氀攀洀攀渀琀嬀樀崀 㴀 ␀爀漀漀琀䔀氀攀洀攀渀琀嬀樀㈀崀㬀ഀഀ
              } else {਍                搀攀氀攀琀攀 ␀爀漀漀琀䔀氀攀洀攀渀琀嬀樀崀㬀ഀഀ
              }਍            紀ഀഀ
            $rootElement.length -= removeCount - 1;਍            戀爀攀愀欀㬀ഀഀ
          }਍        紀ഀഀ
      }਍ഀഀ
      if (parent) {਍        瀀愀爀攀渀琀⸀爀攀瀀氀愀挀攀䌀栀椀氀搀⠀渀攀眀一漀搀攀Ⰰ 昀椀爀猀琀䔀氀攀洀攀渀琀吀漀刀攀洀漀瘀攀⤀㬀ഀഀ
      }਍      瘀愀爀 昀爀愀最洀攀渀琀 㴀 搀漀挀甀洀攀渀琀⸀挀爀攀愀琀攀䐀漀挀甀洀攀渀琀䘀爀愀最洀攀渀琀⠀⤀㬀ഀഀ
      fragment.appendChild(firstElementToRemove);਍      渀攀眀一漀搀攀嬀樀焀䰀椀琀攀⸀攀砀瀀愀渀搀漀崀 㴀 昀椀爀猀琀䔀氀攀洀攀渀琀吀漀刀攀洀漀瘀攀嬀樀焀䰀椀琀攀⸀攀砀瀀愀渀搀漀崀㬀ഀഀ
      for (var k = 1, kk = elementsToRemove.length; k < kk; k++) {਍        瘀愀爀 攀氀攀洀攀渀琀 㴀 攀氀攀洀攀渀琀猀吀漀刀攀洀漀瘀攀嬀欀崀㬀ഀഀ
        jqLite(element).remove(); // must do this way to clean up expando਍        昀爀愀最洀攀渀琀⸀愀瀀瀀攀渀搀䌀栀椀氀搀⠀攀氀攀洀攀渀琀⤀㬀ഀഀ
        delete elementsToRemove[k];਍      紀ഀഀ
਍      攀氀攀洀攀渀琀猀吀漀刀攀洀漀瘀攀嬀　崀 㴀 渀攀眀一漀搀攀㬀ഀഀ
      elementsToRemove.length = 1;਍    紀ഀഀ
਍ഀഀ
    function cloneAndAnnotateFn(fn, annotation) {਍      爀攀琀甀爀渀 攀砀琀攀渀搀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀 爀攀琀甀爀渀 昀渀⸀愀瀀瀀氀礀⠀渀甀氀氀Ⰰ 愀爀最甀洀攀渀琀猀⤀㬀 紀Ⰰ 昀渀Ⰰ 愀渀渀漀琀愀琀椀漀渀⤀㬀ഀഀ
    }਍  紀崀㬀ഀഀ
}਍ഀഀ
var PREFIX_REGEXP = /^(x[\:\-_]|data[\:\-_])/i;਍⼀⨀⨀ഀഀ
 * Converts all accepted directives format into proper directive name.਍ ⨀ 䄀氀氀 漀昀 琀栀攀猀攀 眀椀氀氀 戀攀挀漀洀攀 ✀洀礀䐀椀爀攀挀琀椀瘀攀✀㨀ഀഀ
 *   my:Directive਍ ⨀   洀礀ⴀ搀椀爀攀挀琀椀瘀攀ഀഀ
 *   x-my-directive਍ ⨀   搀愀琀愀ⴀ洀礀㨀搀椀爀攀挀琀椀瘀攀ഀഀ
 *਍ ⨀ 䄀氀猀漀 琀栀攀爀攀 椀猀 猀瀀攀挀椀愀氀 挀愀猀攀 昀漀爀 䴀漀稀 瀀爀攀昀椀砀 猀琀愀爀琀椀渀最 眀椀琀栀 甀瀀瀀攀爀 挀愀猀攀 氀攀琀琀攀爀⸀ഀഀ
 * @param name Name to normalize਍ ⨀⼀ഀഀ
function directiveNormalize(name) {਍  爀攀琀甀爀渀 挀愀洀攀氀䌀愀猀攀⠀渀愀洀攀⸀爀攀瀀氀愀挀攀⠀倀刀䔀䘀䤀堀开刀䔀䜀䔀堀倀Ⰰ ✀✀⤀⤀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name ng.$compile.directive.Attributes਍ ⨀ഀഀ
 * @description਍ ⨀ 䄀 猀栀愀爀攀搀 漀戀樀攀挀琀 戀攀琀眀攀攀渀 搀椀爀攀挀琀椀瘀攀 挀漀洀瀀椀氀攀 ⼀ 氀椀渀欀椀渀最 昀甀渀挀琀椀漀渀猀 眀栀椀挀栀 挀漀渀琀愀椀渀猀 渀漀爀洀愀氀椀稀攀搀 䐀伀䴀ഀഀ
 * element attributes. The values reflect current binding state `{{ }}`. The normalization is਍ ⨀ 渀攀攀搀攀搀 猀椀渀挀攀 愀氀氀 漀昀 琀栀攀猀攀 愀爀攀 琀爀攀愀琀攀搀 愀猀 攀焀甀椀瘀愀氀攀渀琀 椀渀 䄀渀最甀氀愀爀㨀ഀഀ
 *਍ ⨀    㰀猀瀀愀渀 渀最㨀戀椀渀搀㴀∀愀∀ 渀最ⴀ戀椀渀搀㴀∀愀∀ 搀愀琀愀ⴀ渀最ⴀ戀椀渀搀㴀∀愀∀ 砀ⴀ渀最ⴀ戀椀渀搀㴀∀愀∀㸀ഀഀ
 */਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 瀀爀漀瀀攀爀琀礀ഀഀ
 * @name ng.$compile.directive.Attributes#$attr਍ ⨀ 䀀瀀爀漀瀀攀爀琀礀伀昀 渀最⸀␀挀漀洀瀀椀氀攀⸀搀椀爀攀挀琀椀瘀攀⸀䄀琀琀爀椀戀甀琀攀猀ഀഀ
 * @returns {object} A map of DOM element attribute names to the normalized name. This is਍ ⨀                   渀攀攀搀攀搀 琀漀 搀漀 爀攀瘀攀爀猀攀 氀漀漀欀甀瀀 昀爀漀洀 渀漀爀洀愀氀椀稀攀搀 渀愀洀攀 戀愀挀欀 琀漀 愀挀琀甀愀氀 渀愀洀攀⸀ഀഀ
 */਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 渀最⸀␀挀漀洀瀀椀氀攀⸀搀椀爀攀挀琀椀瘀攀⸀䄀琀琀爀椀戀甀琀攀猀⌀␀猀攀琀ഀഀ
 * @methodOf ng.$compile.directive.Attributes਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Set DOM element attribute value.਍ ⨀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀愀洀攀 一漀爀洀愀氀椀稀攀搀 攀氀攀洀攀渀琀 愀琀琀爀椀戀甀琀攀 渀愀洀攀 漀昀 琀栀攀 瀀爀漀瀀攀爀琀礀 琀漀 洀漀搀椀昀礀⸀ 吀栀攀 渀愀洀攀 椀猀ഀഀ
 *          reverse-translated using the {@link ng.$compile.directive.Attributes#$attr $attr}਍ ⨀          瀀爀漀瀀攀爀琀礀 琀漀 琀栀攀 漀爀椀最椀渀愀氀 渀愀洀攀⸀ഀഀ
 * @param {string} value Value to set the attribute to. The value can be an interpolated string.਍ ⨀⼀ഀഀ
਍ഀഀ
਍⼀⨀⨀ഀഀ
 * Closure compiler type information਍ ⨀⼀ഀഀ
਍昀甀渀挀琀椀漀渀 渀漀搀攀猀攀琀䰀椀渀欀椀渀最䘀渀⠀ഀഀ
  /* angular.Scope */ scope,਍  ⼀⨀ 一漀搀攀䰀椀猀琀 ⨀⼀ 渀漀搀攀䰀椀猀琀Ⰰഀഀ
  /* Element */ rootElement,਍  ⼀⨀ 昀甀渀挀琀椀漀渀⠀䘀甀渀挀琀椀漀渀⤀ ⨀⼀ 戀漀甀渀搀吀爀愀渀猀挀氀甀搀攀䘀渀ഀഀ
){}਍ഀഀ
function directiveLinkingFn(਍  ⼀⨀ 渀漀搀攀猀攀琀䰀椀渀欀椀渀最䘀渀 ⨀⼀ 渀漀搀攀猀攀琀䰀椀渀欀椀渀最䘀渀Ⰰഀഀ
  /* angular.Scope */ scope,਍  ⼀⨀ 一漀搀攀 ⨀⼀ 渀漀搀攀Ⰰഀഀ
  /* Element */ rootElement,਍  ⼀⨀ 昀甀渀挀琀椀漀渀⠀䘀甀渀挀琀椀漀渀⤀ ⨀⼀ 戀漀甀渀搀吀爀愀渀猀挀氀甀搀攀䘀渀ഀഀ
){}਍ഀഀ
function tokenDifference(str1, str2) {਍  瘀愀爀 瘀愀氀甀攀猀 㴀 ✀✀Ⰰഀഀ
      tokens1 = str1.split(/\s+/),਍      琀漀欀攀渀猀㈀ 㴀 猀琀爀㈀⸀猀瀀氀椀琀⠀⼀尀猀⬀⼀⤀㬀ഀഀ
਍  漀甀琀攀爀㨀ഀഀ
  for(var i = 0; i < tokens1.length; i++) {਍    瘀愀爀 琀漀欀攀渀 㴀 琀漀欀攀渀猀㄀嬀椀崀㬀ഀഀ
    for(var j = 0; j < tokens2.length; j++) {਍      椀昀⠀琀漀欀攀渀 㴀㴀 琀漀欀攀渀猀㈀嬀樀崀⤀ 挀漀渀琀椀渀甀攀 漀甀琀攀爀㬀ഀഀ
    }਍    瘀愀氀甀攀猀 ⬀㴀 ⠀瘀愀氀甀攀猀⸀氀攀渀最琀栀 㸀 　 㼀 ✀ ✀ 㨀 ✀✀⤀ ⬀ 琀漀欀攀渀㬀ഀഀ
  }਍  爀攀琀甀爀渀 瘀愀氀甀攀猀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name ng.$controllerProvider਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * The {@link ng.$controller $controller service} is used by Angular to create new਍ ⨀ 挀漀渀琀爀漀氀氀攀爀猀⸀ഀഀ
 *਍ ⨀ 吀栀椀猀 瀀爀漀瘀椀搀攀爀 愀氀氀漀眀猀 挀漀渀琀爀漀氀氀攀爀 爀攀最椀猀琀爀愀琀椀漀渀 瘀椀愀 琀栀攀ഀഀ
 * {@link ng.$controllerProvider#methods_register register} method.਍ ⨀⼀ഀഀ
function $ControllerProvider() {਍  瘀愀爀 挀漀渀琀爀漀氀氀攀爀猀 㴀 笀紀Ⰰഀഀ
      CNTRL_REG = /^(\S+)(\s+as\s+(\w+))?$/;਍ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc function਍   ⨀ 䀀渀愀洀攀 渀最⸀␀挀漀渀琀爀漀氀氀攀爀倀爀漀瘀椀搀攀爀⌀爀攀最椀猀琀攀爀ഀഀ
   * @methodOf ng.$controllerProvider਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最簀伀戀樀攀挀琀紀 渀愀洀攀 䌀漀渀琀爀漀氀氀攀爀 渀愀洀攀Ⰰ 漀爀 愀渀 漀戀樀攀挀琀 洀愀瀀 漀昀 挀漀渀琀爀漀氀氀攀爀猀 眀栀攀爀攀 琀栀攀 欀攀礀猀 愀爀攀ഀഀ
   *    the names and the values are the constructors.਍   ⨀ 䀀瀀愀爀愀洀 笀䘀甀渀挀琀椀漀渀簀䄀爀爀愀礀紀 挀漀渀猀琀爀甀挀琀漀爀 䌀漀渀琀爀漀氀氀攀爀 挀漀渀猀琀爀甀挀琀漀爀 昀渀 ⠀漀瀀琀椀漀渀愀氀氀礀 搀攀挀漀爀愀琀攀搀 眀椀琀栀 䐀䤀ഀഀ
   *    annotations in the array notation).਍   ⨀⼀ഀഀ
  this.register = function(name, constructor) {਍    愀猀猀攀爀琀一漀琀䠀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀渀愀洀攀Ⰰ ✀挀漀渀琀爀漀氀氀攀爀✀⤀㬀ഀഀ
    if (isObject(name)) {਍      攀砀琀攀渀搀⠀挀漀渀琀爀漀氀氀攀爀猀Ⰰ 渀愀洀攀⤀㬀ഀഀ
    } else {਍      挀漀渀琀爀漀氀氀攀爀猀嬀渀愀洀攀崀 㴀 挀漀渀猀琀爀甀挀琀漀爀㬀ഀഀ
    }਍  紀㬀ഀഀ
਍ഀഀ
  this.$get = ['$injector', '$window', function($injector, $window) {਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
     * @name ng.$controller਍     ⨀ 䀀爀攀焀甀椀爀攀猀 ␀椀渀樀攀挀琀漀爀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀䘀甀渀挀琀椀漀渀簀猀琀爀椀渀最紀 挀漀渀猀琀爀甀挀琀漀爀 䤀昀 挀愀氀氀攀搀 眀椀琀栀 愀 昀甀渀挀琀椀漀渀 琀栀攀渀 椀琀✀猀 挀漀渀猀椀搀攀爀攀搀 琀漀 戀攀 琀栀攀ഀഀ
     *    controller constructor function. Otherwise it's considered to be a string which is used਍     ⨀    琀漀 爀攀琀爀椀攀瘀攀 琀栀攀 挀漀渀琀爀漀氀氀攀爀 挀漀渀猀琀爀甀挀琀漀爀 甀猀椀渀最 琀栀攀 昀漀氀氀漀眀椀渀最 猀琀攀瀀猀㨀ഀഀ
     *਍     ⨀    ⨀ 挀栀攀挀欀 椀昀 愀 挀漀渀琀爀漀氀氀攀爀 眀椀琀栀 最椀瘀攀渀 渀愀洀攀 椀猀 爀攀最椀猀琀攀爀攀搀 瘀椀愀 怀␀挀漀渀琀爀漀氀氀攀爀倀爀漀瘀椀搀攀爀怀ഀഀ
     *    * check if evaluating the string on the current scope returns a constructor਍     ⨀    ⨀ 挀栀攀挀欀 怀眀椀渀搀漀眀嬀挀漀渀猀琀爀甀挀琀漀爀崀怀 漀渀 琀栀攀 最氀漀戀愀氀 怀眀椀渀搀漀眀怀 漀戀樀攀挀琀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀伀戀樀攀挀琀紀 氀漀挀愀氀猀 䤀渀樀攀挀琀椀漀渀 氀漀挀愀氀猀 昀漀爀 䌀漀渀琀爀漀氀氀攀爀⸀ഀഀ
     * @return {Object} Instance of given controller.਍     ⨀ഀഀ
     * @description਍     ⨀ 怀␀挀漀渀琀爀漀氀氀攀爀怀 猀攀爀瘀椀挀攀 椀猀 爀攀猀瀀漀渀猀椀戀氀攀 昀漀爀 椀渀猀琀愀渀琀椀愀琀椀渀最 挀漀渀琀爀漀氀氀攀爀猀⸀ഀഀ
     *਍     ⨀ 䤀琀✀猀 樀甀猀琀 愀 猀椀洀瀀氀攀 挀愀氀氀 琀漀 笀䀀氀椀渀欀 䄀唀吀伀⸀␀椀渀樀攀挀琀漀爀 ␀椀渀樀攀挀琀漀爀紀Ⰰ 戀甀琀 攀砀琀爀愀挀琀攀搀 椀渀琀漀ഀഀ
     * a service, so that one can override this service with {@link https://gist.github.com/1649788਍     ⨀ 䈀䌀 瘀攀爀猀椀漀渀紀⸀ഀഀ
     */਍    爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀攀砀瀀爀攀猀猀椀漀渀Ⰰ 氀漀挀愀氀猀⤀ 笀ഀഀ
      var instance, match, constructor, identifier;਍ഀഀ
      if(isString(expression)) {਍        洀愀琀挀栀 㴀 攀砀瀀爀攀猀猀椀漀渀⸀洀愀琀挀栀⠀䌀一吀刀䰀开刀䔀䜀⤀Ⰰഀഀ
        constructor = match[1],਍        椀搀攀渀琀椀昀椀攀爀 㴀 洀愀琀挀栀嬀㌀崀㬀ഀഀ
        expression = controllers.hasOwnProperty(constructor)਍            㼀 挀漀渀琀爀漀氀氀攀爀猀嬀挀漀渀猀琀爀甀挀琀漀爀崀ഀഀ
            : getter(locals.$scope, constructor, true) || getter($window, constructor, true);਍ഀഀ
        assertArgFn(expression, constructor, true);਍      紀ഀഀ
਍      椀渀猀琀愀渀挀攀 㴀 ␀椀渀樀攀挀琀漀爀⸀椀渀猀琀愀渀琀椀愀琀攀⠀攀砀瀀爀攀猀猀椀漀渀Ⰰ 氀漀挀愀氀猀⤀㬀ഀഀ
਍      椀昀 ⠀椀搀攀渀琀椀昀椀攀爀⤀ 笀ഀഀ
        if (!(locals && typeof locals.$scope == 'object')) {਍          琀栀爀漀眀 洀椀渀䔀爀爀⠀✀␀挀漀渀琀爀漀氀氀攀爀✀⤀⠀✀渀漀猀挀瀀✀Ⰰഀഀ
              "Cannot export controller '{0}' as '{1}'! No $scope object provided via `locals`.",਍              挀漀渀猀琀爀甀挀琀漀爀 簀簀 攀砀瀀爀攀猀猀椀漀渀⸀渀愀洀攀Ⰰ 椀搀攀渀琀椀昀椀攀爀⤀㬀ഀഀ
        }਍ഀഀ
        locals.$scope[identifier] = instance;਍      紀ഀഀ
਍      爀攀琀甀爀渀 椀渀猀琀愀渀挀攀㬀ഀഀ
    };਍  紀崀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name ng.$document਍ ⨀ 䀀爀攀焀甀椀爀攀猀 ␀眀椀渀搀漀眀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * A {@link angular.element jQuery or jqLite} wrapper for the browser's `window.document` object.਍ ⨀⼀ഀഀ
function $DocumentProvider(){਍  琀栀椀猀⸀␀最攀琀 㴀 嬀✀␀眀椀渀搀漀眀✀Ⰰ 昀甀渀挀琀椀漀渀⠀眀椀渀搀漀眀⤀笀ഀഀ
    return jqLite(window.document);਍  紀崀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
 * @name ng.$exceptionHandler਍ ⨀ 䀀爀攀焀甀椀爀攀猀 ␀氀漀最ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Any uncaught exception in angular expressions is delegated to this service.਍ ⨀ 吀栀攀 搀攀昀愀甀氀琀 椀洀瀀氀攀洀攀渀琀愀琀椀漀渀 猀椀洀瀀氀礀 搀攀氀攀最愀琀攀猀 琀漀 怀␀氀漀最⸀攀爀爀漀爀怀 眀栀椀挀栀 氀漀最猀 椀琀 椀渀琀漀ഀഀ
 * the browser console.਍ ⨀ ഀഀ
 * In unit tests, if `angular-mocks.js` is loaded, this service is overridden by਍ ⨀ 笀䀀氀椀渀欀 渀最䴀漀挀欀⸀␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀 洀漀挀欀 ␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀紀 眀栀椀挀栀 愀椀搀猀 椀渀 琀攀猀琀椀渀最⸀ഀഀ
 *਍ ⨀ ⌀⌀ 䔀砀愀洀瀀氀攀㨀ഀഀ
 * ਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   angular.module('exceptionOverride', []).factory('$exceptionHandler', function () {਍ ⨀     爀攀琀甀爀渀 昀甀渀挀琀椀漀渀 ⠀攀砀挀攀瀀琀椀漀渀Ⰰ 挀愀甀猀攀⤀ 笀ഀഀ
 *       exception.message += ' (caused by "' + cause + '")';਍ ⨀       琀栀爀漀眀 攀砀挀攀瀀琀椀漀渀㬀ഀഀ
 *     };਍ ⨀   紀⤀㬀ഀഀ
 * </pre>਍ ⨀ ഀഀ
 * This example will override the normal action of `$exceptionHandler`, to make angular਍ ⨀ 攀砀挀攀瀀琀椀漀渀猀 昀愀椀氀 栀愀爀搀 眀栀攀渀 琀栀攀礀 栀愀瀀瀀攀渀Ⰰ 椀渀猀琀攀愀搀 漀昀 樀甀猀琀 氀漀最最椀渀最 琀漀 琀栀攀 挀漀渀猀漀氀攀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀䔀爀爀漀爀紀 攀砀挀攀瀀琀椀漀渀 䔀砀挀攀瀀琀椀漀渀 愀猀猀漀挀椀愀琀攀搀 眀椀琀栀 琀栀攀 攀爀爀漀爀⸀ഀഀ
 * @param {string=} cause optional information about the context in which਍ ⨀       琀栀攀 攀爀爀漀爀 眀愀猀 琀栀爀漀眀渀⸀ഀഀ
 *਍ ⨀⼀ഀഀ
function $ExceptionHandlerProvider() {਍  琀栀椀猀⸀␀最攀琀 㴀 嬀✀␀氀漀最✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀氀漀最⤀ 笀ഀഀ
    return function(exception, cause) {਍      ␀氀漀最⸀攀爀爀漀爀⸀愀瀀瀀氀礀⠀␀氀漀最Ⰰ 愀爀最甀洀攀渀琀猀⤀㬀ഀഀ
    };਍  紀崀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 倀愀爀猀攀 栀攀愀搀攀爀猀 椀渀琀漀 欀攀礀 瘀愀氀甀攀 漀戀樀攀挀琀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 栀攀愀搀攀爀猀 刀愀眀 栀攀愀搀攀爀猀 愀猀 愀 猀琀爀椀渀最ഀഀ
 * @returns {Object} Parsed headers as key value object਍ ⨀⼀ഀഀ
function parseHeaders(headers) {਍  瘀愀爀 瀀愀爀猀攀搀 㴀 笀紀Ⰰ 欀攀礀Ⰰ 瘀愀氀Ⰰ 椀㬀ഀഀ
਍  椀昀 ⠀℀栀攀愀搀攀爀猀⤀ 爀攀琀甀爀渀 瀀愀爀猀攀搀㬀ഀഀ
਍  昀漀爀䔀愀挀栀⠀栀攀愀搀攀爀猀⸀猀瀀氀椀琀⠀✀尀渀✀⤀Ⰰ 昀甀渀挀琀椀漀渀⠀氀椀渀攀⤀ 笀ഀഀ
    i = line.indexOf(':');਍    欀攀礀 㴀 氀漀眀攀爀挀愀猀攀⠀琀爀椀洀⠀氀椀渀攀⸀猀甀戀猀琀爀⠀　Ⰰ 椀⤀⤀⤀㬀ഀഀ
    val = trim(line.substr(i + 1));਍ഀഀ
    if (key) {਍      椀昀 ⠀瀀愀爀猀攀搀嬀欀攀礀崀⤀ 笀ഀഀ
        parsed[key] += ', ' + val;਍      紀 攀氀猀攀 笀ഀഀ
        parsed[key] = val;਍      紀ഀഀ
    }਍  紀⤀㬀ഀഀ
਍  爀攀琀甀爀渀 瀀愀爀猀攀搀㬀ഀഀ
}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * Returns a function that provides access to parsed headers.਍ ⨀ഀഀ
 * Headers are lazy parsed when first requested.਍ ⨀ 䀀猀攀攀 瀀愀爀猀攀䠀攀愀搀攀爀猀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀⠀猀琀爀椀渀最簀伀戀樀攀挀琀⤀紀 栀攀愀搀攀爀猀 䠀攀愀搀攀爀猀 琀漀 瀀爀漀瘀椀搀攀 愀挀挀攀猀猀 琀漀⸀ഀഀ
 * @returns {function(string=)} Returns a getter function which if called with:਍ ⨀ഀഀ
 *   - if called with single an argument returns a single header value or null਍ ⨀   ⴀ 椀昀 挀愀氀氀攀搀 眀椀琀栀 渀漀 愀爀最甀洀攀渀琀猀 爀攀琀甀爀渀猀 愀渀 漀戀樀攀挀琀 挀漀渀琀愀椀渀椀渀最 愀氀氀 栀攀愀搀攀爀猀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 栀攀愀搀攀爀猀䜀攀琀琀攀爀⠀栀攀愀搀攀爀猀⤀ 笀ഀഀ
  var headersObj = isObject(headers) ? headers : undefined;਍ഀഀ
  return function(name) {਍    椀昀 ⠀℀栀攀愀搀攀爀猀伀戀樀⤀ 栀攀愀搀攀爀猀伀戀樀 㴀  瀀愀爀猀攀䠀攀愀搀攀爀猀⠀栀攀愀搀攀爀猀⤀㬀ഀഀ
਍    椀昀 ⠀渀愀洀攀⤀ 笀ഀഀ
      return headersObj[lowercase(name)] || null;਍    紀ഀഀ
਍    爀攀琀甀爀渀 栀攀愀搀攀爀猀伀戀樀㬀ഀഀ
  };਍紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䌀栀愀椀渀 愀氀氀 最椀瘀攀渀 昀甀渀挀琀椀漀渀猀ഀഀ
 *਍ ⨀ 吀栀椀猀 昀甀渀挀琀椀漀渀 椀猀 甀猀攀搀 昀漀爀 戀漀琀栀 爀攀焀甀攀猀琀 愀渀搀 爀攀猀瀀漀渀猀攀 琀爀愀渀猀昀漀爀洀椀渀最ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀⨀紀 搀愀琀愀 䐀愀琀愀 琀漀 琀爀愀渀猀昀漀爀洀⸀ഀഀ
 * @param {function(string=)} headers Http headers getter fn.਍ ⨀ 䀀瀀愀爀愀洀 笀⠀昀甀渀挀琀椀漀渀簀䄀爀爀愀礀⸀㰀昀甀渀挀琀椀漀渀㸀⤀紀 昀渀猀 䘀甀渀挀琀椀漀渀 漀爀 愀渀 愀爀爀愀礀 漀昀 昀甀渀挀琀椀漀渀猀⸀ഀഀ
 * @returns {*} Transformed data.਍ ⨀⼀ഀഀ
function transformData(data, headers, fns) {਍  椀昀 ⠀椀猀䘀甀渀挀琀椀漀渀⠀昀渀猀⤀⤀ഀഀ
    return fns(data, headers);਍ഀഀ
  forEach(fns, function(fn) {਍    搀愀琀愀 㴀 昀渀⠀搀愀琀愀Ⰰ 栀攀愀搀攀爀猀⤀㬀ഀഀ
  });਍ഀഀ
  return data;਍紀ഀഀ
਍ഀഀ
function isSuccess(status) {਍  爀攀琀甀爀渀 ㈀　　 㰀㴀 猀琀愀琀甀猀 ☀☀ 猀琀愀琀甀猀 㰀 ㌀　　㬀ഀഀ
}਍ഀഀ
਍昀甀渀挀琀椀漀渀 ␀䠀琀琀瀀倀爀漀瘀椀搀攀爀⠀⤀ 笀ഀഀ
  var JSON_START = /^\s*(\[|\{[^\{])/,਍      䨀匀伀一开䔀一䐀 㴀 ⼀嬀尀紀尀崀崀尀猀⨀␀⼀Ⰰഀഀ
      PROTECTION_PREFIX = /^\)\]\}',?\n/,਍      䌀伀一吀䔀一吀开吀夀倀䔀开䄀倀倀䰀䤀䌀䄀吀䤀伀一开䨀匀伀一 㴀 笀✀䌀漀渀琀攀渀琀ⴀ吀礀瀀攀✀㨀 ✀愀瀀瀀氀椀挀愀琀椀漀渀⼀樀猀漀渀㬀挀栀愀爀猀攀琀㴀甀琀昀ⴀ㠀✀紀㬀ഀഀ
਍  瘀愀爀 搀攀昀愀甀氀琀猀 㴀 琀栀椀猀⸀搀攀昀愀甀氀琀猀 㴀 笀ഀഀ
    // transform incoming response data਍    琀爀愀渀猀昀漀爀洀刀攀猀瀀漀渀猀攀㨀 嬀昀甀渀挀琀椀漀渀⠀搀愀琀愀⤀ 笀ഀഀ
      if (isString(data)) {਍        ⼀⼀ 猀琀爀椀瀀 樀猀漀渀 瘀甀氀渀攀爀愀戀椀氀椀琀礀 瀀爀漀琀攀挀琀椀漀渀 瀀爀攀昀椀砀ഀഀ
        data = data.replace(PROTECTION_PREFIX, '');਍        椀昀 ⠀䨀匀伀一开匀吀䄀刀吀⸀琀攀猀琀⠀搀愀琀愀⤀ ☀☀ 䨀匀伀一开䔀一䐀⸀琀攀猀琀⠀搀愀琀愀⤀⤀ഀഀ
          data = fromJson(data);਍      紀ഀഀ
      return data;਍    紀崀Ⰰഀഀ
਍    ⼀⼀ 琀爀愀渀猀昀漀爀洀 漀甀琀最漀椀渀最 爀攀焀甀攀猀琀 搀愀琀愀ഀഀ
    transformRequest: [function(d) {਍      爀攀琀甀爀渀 椀猀伀戀樀攀挀琀⠀搀⤀ ☀☀ ℀椀猀䘀椀氀攀⠀搀⤀ 㼀 琀漀䨀猀漀渀⠀搀⤀ 㨀 搀㬀ഀഀ
    }],਍ഀഀ
    // default headers਍    栀攀愀搀攀爀猀㨀 笀ഀഀ
      common: {਍        ✀䄀挀挀攀瀀琀✀㨀 ✀愀瀀瀀氀椀挀愀琀椀漀渀⼀樀猀漀渀Ⰰ 琀攀砀琀⼀瀀氀愀椀渀Ⰰ ⨀⼀⨀✀ഀഀ
      },਍      瀀漀猀琀㨀   䌀伀一吀䔀一吀开吀夀倀䔀开䄀倀倀䰀䤀䌀䄀吀䤀伀一开䨀匀伀一Ⰰഀഀ
      put:    CONTENT_TYPE_APPLICATION_JSON,਍      瀀愀琀挀栀㨀  䌀伀一吀䔀一吀开吀夀倀䔀开䄀倀倀䰀䤀䌀䄀吀䤀伀一开䨀匀伀一ഀഀ
    },਍ഀഀ
    xsrfCookieName: 'XSRF-TOKEN',਍    砀猀爀昀䠀攀愀搀攀爀一愀洀攀㨀 ✀堀ⴀ堀匀刀䘀ⴀ吀伀䬀䔀一✀ഀഀ
  };਍ഀഀ
  /**਍   ⨀ 䄀爀攀 漀爀搀攀爀攀搀 戀礀 爀攀焀甀攀猀琀Ⰰ 椀⸀攀⸀ 琀栀攀礀 愀爀攀 愀瀀瀀氀椀攀搀 椀渀 琀栀攀 猀愀洀攀 漀爀搀攀爀 愀猀 琀栀攀ഀഀ
   * array, on request, but reverse order, on response.਍   ⨀⼀ഀഀ
  var interceptorFactories = this.interceptors = [];਍ഀഀ
  /**਍   ⨀ 䘀漀爀 栀椀猀琀漀爀椀挀愀氀 爀攀愀猀漀渀猀Ⰰ 爀攀猀瀀漀渀猀攀 椀渀琀攀爀挀攀瀀琀漀爀猀 愀爀攀 漀爀搀攀爀攀搀 戀礀 琀栀攀 漀爀搀攀爀 椀渀 眀栀椀挀栀ഀഀ
   * they are applied to the response. (This is the opposite of interceptorFactories)਍   ⨀⼀ഀഀ
  var responseInterceptorFactories = this.responseInterceptors = [];਍ഀഀ
  this.$get = ['$httpBackend', '$browser', '$cacheFactory', '$rootScope', '$q', '$injector',਍      昀甀渀挀琀椀漀渀⠀␀栀琀琀瀀䈀愀挀欀攀渀搀Ⰰ ␀戀爀漀眀猀攀爀Ⰰ ␀挀愀挀栀攀䘀愀挀琀漀爀礀Ⰰ ␀爀漀漀琀匀挀漀瀀攀Ⰰ ␀焀Ⰰ ␀椀渀樀攀挀琀漀爀⤀ 笀ഀഀ
਍    瘀愀爀 搀攀昀愀甀氀琀䌀愀挀栀攀 㴀 ␀挀愀挀栀攀䘀愀挀琀漀爀礀⠀✀␀栀琀琀瀀✀⤀㬀ഀഀ
਍    ⼀⨀⨀ഀഀ
     * Interceptors stored in reverse order. Inner interceptors before outer interceptors.਍     ⨀ 吀栀攀 爀攀瘀攀爀猀愀氀 椀猀 渀攀攀搀攀搀 猀漀 琀栀愀琀 眀攀 挀愀渀 戀甀椀氀搀 甀瀀 琀栀攀 椀渀琀攀爀挀攀瀀琀椀漀渀 挀栀愀椀渀 愀爀漀甀渀搀 琀栀攀ഀഀ
     * server request.਍     ⨀⼀ഀഀ
    var reversedInterceptors = [];਍ഀഀ
    forEach(interceptorFactories, function(interceptorFactory) {਍      爀攀瘀攀爀猀攀搀䤀渀琀攀爀挀攀瀀琀漀爀猀⸀甀渀猀栀椀昀琀⠀椀猀匀琀爀椀渀最⠀椀渀琀攀爀挀攀瀀琀漀爀䘀愀挀琀漀爀礀⤀ഀഀ
          ? $injector.get(interceptorFactory) : $injector.invoke(interceptorFactory));਍    紀⤀㬀ഀഀ
਍    昀漀爀䔀愀挀栀⠀爀攀猀瀀漀渀猀攀䤀渀琀攀爀挀攀瀀琀漀爀䘀愀挀琀漀爀椀攀猀Ⰰ 昀甀渀挀琀椀漀渀⠀椀渀琀攀爀挀攀瀀琀漀爀䘀愀挀琀漀爀礀Ⰰ 椀渀搀攀砀⤀ 笀ഀഀ
      var responseFn = isString(interceptorFactory)਍          㼀 ␀椀渀樀攀挀琀漀爀⸀最攀琀⠀椀渀琀攀爀挀攀瀀琀漀爀䘀愀挀琀漀爀礀⤀ഀഀ
          : $injector.invoke(interceptorFactory);਍ഀഀ
      /**਍       ⨀ 刀攀猀瀀漀渀猀攀 椀渀琀攀爀挀攀瀀琀漀爀猀 最漀 戀攀昀漀爀攀 ∀愀爀漀甀渀搀∀ 椀渀琀攀爀挀攀瀀琀漀爀猀 ⠀渀漀 爀攀愀氀 爀攀愀猀漀渀Ⰰ 樀甀猀琀ഀഀ
       * had to pick one.) But they are already reversed, so we can't use unshift, hence਍       ⨀ 琀栀攀 猀瀀氀椀挀攀⸀ഀഀ
       */਍      爀攀瘀攀爀猀攀搀䤀渀琀攀爀挀攀瀀琀漀爀猀⸀猀瀀氀椀挀攀⠀椀渀搀攀砀Ⰰ 　Ⰰ 笀ഀഀ
        response: function(response) {਍          爀攀琀甀爀渀 爀攀猀瀀漀渀猀攀䘀渀⠀␀焀⸀眀栀攀渀⠀爀攀猀瀀漀渀猀攀⤀⤀㬀ഀഀ
        },਍        爀攀猀瀀漀渀猀攀䔀爀爀漀爀㨀 昀甀渀挀琀椀漀渀⠀爀攀猀瀀漀渀猀攀⤀ 笀ഀഀ
          return responseFn($q.reject(response));਍        紀ഀഀ
      });਍    紀⤀㬀ഀഀ
਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
     * @name ng.$http਍     ⨀ 䀀爀攀焀甀椀爀攀猀 ␀栀琀琀瀀䈀愀挀欀攀渀搀ഀഀ
     * @requires $browser਍     ⨀ 䀀爀攀焀甀椀爀攀猀 ␀挀愀挀栀攀䘀愀挀琀漀爀礀ഀഀ
     * @requires $rootScope਍     ⨀ 䀀爀攀焀甀椀爀攀猀 ␀焀ഀഀ
     * @requires $injector਍     ⨀ഀഀ
     * @description਍     ⨀ 吀栀攀 怀␀栀琀琀瀀怀 猀攀爀瘀椀挀攀 椀猀 愀 挀漀爀攀 䄀渀最甀氀愀爀 猀攀爀瘀椀挀攀 琀栀愀琀 昀愀挀椀氀椀琀愀琀攀猀 挀漀洀洀甀渀椀挀愀琀椀漀渀 眀椀琀栀 琀栀攀 爀攀洀漀琀攀ഀഀ
     * HTTP servers via the browser's {@link https://developer.mozilla.org/en/xmlhttprequest਍     ⨀ 堀䴀䰀䠀琀琀瀀刀攀焀甀攀猀琀紀 漀戀樀攀挀琀 漀爀 瘀椀愀 笀䀀氀椀渀欀 栀琀琀瀀㨀⼀⼀攀渀⸀眀椀欀椀瀀攀搀椀愀⸀漀爀最⼀眀椀欀椀⼀䨀匀伀一倀 䨀匀伀一倀紀⸀ഀഀ
     *਍     ⨀ 䘀漀爀 甀渀椀琀 琀攀猀琀椀渀最 愀瀀瀀氀椀挀愀琀椀漀渀猀 琀栀愀琀 甀猀攀 怀␀栀琀琀瀀怀 猀攀爀瘀椀挀攀Ⰰ 猀攀攀ഀഀ
     * {@link ngMock.$httpBackend $httpBackend mock}.਍     ⨀ഀഀ
     * For a higher level of abstraction, please check out the {@link ngResource.$resource਍     ⨀ ␀爀攀猀漀甀爀挀攀紀 猀攀爀瘀椀挀攀⸀ഀഀ
     *਍     ⨀ 吀栀攀 ␀栀琀琀瀀 䄀倀䤀 椀猀 戀愀猀攀搀 漀渀 琀栀攀 笀䀀氀椀渀欀 渀最⸀␀焀 搀攀昀攀爀爀攀搀⼀瀀爀漀洀椀猀攀 䄀倀䤀猀紀 攀砀瀀漀猀攀搀 戀礀ഀഀ
     * the $q service. While for simple usage patterns this doesn't matter much, for advanced usage਍     ⨀ 椀琀 椀猀 椀洀瀀漀爀琀愀渀琀 琀漀 昀愀洀椀氀椀愀爀椀稀攀 礀漀甀爀猀攀氀昀 眀椀琀栀 琀栀攀猀攀 䄀倀䤀猀 愀渀搀 琀栀攀 最甀愀爀愀渀琀攀攀猀 琀栀攀礀 瀀爀漀瘀椀搀攀⸀ഀഀ
     *਍     ⨀ഀഀ
     * # General usage਍     ⨀ 吀栀攀 怀␀栀琀琀瀀怀 猀攀爀瘀椀挀攀 椀猀 愀 昀甀渀挀琀椀漀渀 眀栀椀挀栀 琀愀欀攀猀 愀 猀椀渀最氀攀 愀爀最甀洀攀渀琀 ﴀ﷿⃿愀 挀漀渀昀椀最甀爀愀琀椀漀渀 漀戀樀攀挀琀 ﴀ﷿෿ഀ
     * that is used to generate an HTTP request and returns  a {@link ng.$q promise}਍     ⨀ 眀椀琀栀 琀眀漀 ␀栀琀琀瀀 猀瀀攀挀椀昀椀挀 洀攀琀栀漀搀猀㨀 怀猀甀挀挀攀猀猀怀 愀渀搀 怀攀爀爀漀爀怀⸀ഀഀ
     *਍     ⨀ 㰀瀀爀攀㸀ഀഀ
     *   $http({method: 'GET', url: '/someUrl'}).਍     ⨀     猀甀挀挀攀猀猀⠀昀甀渀挀琀椀漀渀⠀搀愀琀愀Ⰰ 猀琀愀琀甀猀Ⰰ 栀攀愀搀攀爀猀Ⰰ 挀漀渀昀椀最⤀ 笀ഀഀ
     *       // this callback will be called asynchronously਍     ⨀       ⼀⼀ 眀栀攀渀 琀栀攀 爀攀猀瀀漀渀猀攀 椀猀 愀瘀愀椀氀愀戀氀攀ഀഀ
     *     }).਍     ⨀     攀爀爀漀爀⠀昀甀渀挀琀椀漀渀⠀搀愀琀愀Ⰰ 猀琀愀琀甀猀Ⰰ 栀攀愀搀攀爀猀Ⰰ 挀漀渀昀椀最⤀ 笀ഀഀ
     *       // called asynchronously if an error occurs਍     ⨀       ⼀⼀ 漀爀 猀攀爀瘀攀爀 爀攀琀甀爀渀猀 爀攀猀瀀漀渀猀攀 眀椀琀栀 愀渀 攀爀爀漀爀 猀琀愀琀甀猀⸀ഀഀ
     *     });਍     ⨀ 㰀⼀瀀爀攀㸀ഀഀ
     *਍     ⨀ 匀椀渀挀攀 琀栀攀 爀攀琀甀爀渀攀搀 瘀愀氀甀攀 漀昀 挀愀氀氀椀渀最 琀栀攀 ␀栀琀琀瀀 昀甀渀挀琀椀漀渀 椀猀 愀 怀瀀爀漀洀椀猀攀怀Ⰰ 礀漀甀 挀愀渀 愀氀猀漀 甀猀攀ഀഀ
     * the `then` method to register callbacks, and these callbacks will receive a single argument ��਍     ⨀ 愀渀 漀戀樀攀挀琀 爀攀瀀爀攀猀攀渀琀椀渀最 琀栀攀 爀攀猀瀀漀渀猀攀⸀ 匀攀攀 琀栀攀 䄀倀䤀 猀椀最渀愀琀甀爀攀 愀渀搀 琀礀瀀攀 椀渀昀漀 戀攀氀漀眀 昀漀爀 洀漀爀攀ഀഀ
     * details.਍     ⨀ഀഀ
     * A response status code between 200 and 299 is considered a success status and਍     ⨀ 眀椀氀氀 爀攀猀甀氀琀 椀渀 琀栀攀 猀甀挀挀攀猀猀 挀愀氀氀戀愀挀欀 戀攀椀渀最 挀愀氀氀攀搀⸀ 一漀琀攀 琀栀愀琀 椀昀 琀栀攀 爀攀猀瀀漀渀猀攀 椀猀 愀 爀攀搀椀爀攀挀琀Ⰰഀഀ
     * XMLHttpRequest will transparently follow it, meaning that the error callback will not be਍     ⨀ 挀愀氀氀攀搀 昀漀爀 猀甀挀栀 爀攀猀瀀漀渀猀攀猀⸀ഀഀ
     *਍     ⨀ ⌀ 䌀愀氀氀椀渀最 ␀栀琀琀瀀 昀爀漀洀 漀甀琀猀椀搀攀 䄀渀最甀氀愀爀䨀匀ഀഀ
     * The `$http` service will not actually send the request until the next `$digest()` is਍     ⨀ 攀砀攀挀甀琀攀搀⸀ 一漀爀洀愀氀氀礀 琀栀椀猀 椀猀 渀漀琀 愀渀 椀猀猀甀攀Ⰰ 猀椀渀挀攀 愀氀洀漀猀琀 愀氀氀 琀栀攀 琀椀洀攀 礀漀甀爀 挀愀氀氀 琀漀 怀␀栀琀琀瀀怀 眀椀氀氀ഀഀ
     * be from within a `$apply()` block.਍     ⨀ 䤀昀 礀漀甀 愀爀攀 挀愀氀氀椀渀最 怀␀栀琀琀瀀怀 昀爀漀洀 漀甀琀猀椀搀攀 䄀渀最甀氀愀爀Ⰰ 琀栀攀渀 礀漀甀 猀栀漀甀氀搀 眀爀愀瀀 椀琀 椀渀 愀 挀愀氀氀 琀漀ഀഀ
     * `$apply` to cause a $digest to occur and also to handle errors in the block correctly.਍     ⨀ഀഀ
     * ```਍     ⨀ ␀猀挀漀瀀攀⸀␀愀瀀瀀氀礀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
     *   $http(...);਍     ⨀ 紀⤀㬀ഀഀ
     * ```਍     ⨀ഀഀ
     * # Writing Unit Tests that use $http਍     ⨀ 圀栀攀渀 甀渀椀琀 琀攀猀琀椀渀最 礀漀甀 愀爀攀 洀漀猀琀氀礀 爀攀猀瀀漀渀猀椀戀氀攀 昀漀爀 猀挀栀攀搀甀氀椀渀最 琀栀攀 怀␀搀椀最攀猀琀怀 挀礀挀氀攀⸀ 䤀昀 礀漀甀 搀漀ഀഀ
     * not trigger a `$digest` before calling `$httpBackend.flush()` then the request will not have਍     ⨀ 戀攀攀渀 洀愀搀攀 愀渀搀 怀␀栀琀琀瀀䈀愀挀欀攀渀搀⸀攀砀瀀攀挀琀⠀⸀⸀⸀⤀怀 攀砀瀀攀挀琀愀琀椀漀渀猀 眀椀氀氀 昀愀椀氀⸀  吀栀攀 猀漀氀甀琀椀漀渀 椀猀 琀漀 爀甀渀 琀栀攀ഀഀ
     * code that calls the `$http()` method inside a $apply block as explained in the previous਍     ⨀ 猀攀挀琀椀漀渀⸀ഀഀ
     *਍     ⨀ 怀怀怀ഀഀ
     * $httpBackend.expectGET(...);਍     ⨀ ␀猀挀漀瀀攀⸀␀愀瀀瀀氀礀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
     *   $http.get(...);਍     ⨀ 紀⤀㬀ഀഀ
     * $httpBackend.flush();਍     ⨀ 怀怀怀ഀഀ
     *਍     ⨀ ⌀ 匀栀漀爀琀挀甀琀 洀攀琀栀漀搀猀ഀഀ
     *਍     ⨀ 匀椀渀挀攀 愀氀氀 椀渀瘀漀挀愀琀椀漀渀猀 漀昀 琀栀攀 ␀栀琀琀瀀 猀攀爀瘀椀挀攀 爀攀焀甀椀爀攀 瀀愀猀猀椀渀最 椀渀 愀渀 䠀吀吀倀 洀攀琀栀漀搀 愀渀搀 唀刀䰀Ⰰ 愀渀搀ഀഀ
     * POST/PUT requests require request data to be provided as well, shortcut methods਍     ⨀ 眀攀爀攀 挀爀攀愀琀攀搀㨀ഀഀ
     *਍     ⨀ 㰀瀀爀攀㸀ഀഀ
     *   $http.get('/someUrl').success(successCallback);਍     ⨀   ␀栀琀琀瀀⸀瀀漀猀琀⠀✀⼀猀漀洀攀唀爀氀✀Ⰰ 搀愀琀愀⤀⸀猀甀挀挀攀猀猀⠀猀甀挀挀攀猀猀䌀愀氀氀戀愀挀欀⤀㬀ഀഀ
     * </pre>਍     ⨀ഀഀ
     * Complete list of shortcut methods:਍     ⨀ഀഀ
     * - {@link ng.$http#methods_get $http.get}਍     ⨀ ⴀ 笀䀀氀椀渀欀 渀最⸀␀栀琀琀瀀⌀洀攀琀栀漀搀猀开栀攀愀搀 ␀栀琀琀瀀⸀栀攀愀搀紀ഀഀ
     * - {@link ng.$http#methods_post $http.post}਍     ⨀ ⴀ 笀䀀氀椀渀欀 渀最⸀␀栀琀琀瀀⌀洀攀琀栀漀搀猀开瀀甀琀 ␀栀琀琀瀀⸀瀀甀琀紀ഀഀ
     * - {@link ng.$http#methods_delete $http.delete}਍     ⨀ ⴀ 笀䀀氀椀渀欀 渀最⸀␀栀琀琀瀀⌀洀攀琀栀漀搀猀开樀猀漀渀瀀 ␀栀琀琀瀀⸀樀猀漀渀瀀紀ഀഀ
     *਍     ⨀ഀഀ
     * # Setting HTTP Headers਍     ⨀ഀഀ
     * The $http service will automatically add certain HTTP headers to all requests. These defaults਍     ⨀ 挀愀渀 戀攀 昀甀氀氀礀 挀漀渀昀椀最甀爀攀搀 戀礀 愀挀挀攀猀猀椀渀最 琀栀攀 怀␀栀琀琀瀀倀爀漀瘀椀搀攀爀⸀搀攀昀愀甀氀琀猀⸀栀攀愀搀攀爀猀怀 挀漀渀昀椀最甀爀愀琀椀漀渀ഀഀ
     * object, which currently contains this default configuration:਍     ⨀ഀഀ
     * - `$httpProvider.defaults.headers.common` (headers that are common for all requests):਍     ⨀   ⴀ 怀䄀挀挀攀瀀琀㨀 愀瀀瀀氀椀挀愀琀椀漀渀⼀樀猀漀渀Ⰰ 琀攀砀琀⼀瀀氀愀椀渀Ⰰ ⨀ ⼀ ⨀怀ഀഀ
     * - `$httpProvider.defaults.headers.post`: (header defaults for POST requests)਍     ⨀   ⴀ 怀䌀漀渀琀攀渀琀ⴀ吀礀瀀攀㨀 愀瀀瀀氀椀挀愀琀椀漀渀⼀樀猀漀渀怀ഀഀ
     * - `$httpProvider.defaults.headers.put` (header defaults for PUT requests)਍     ⨀   ⴀ 怀䌀漀渀琀攀渀琀ⴀ吀礀瀀攀㨀 愀瀀瀀氀椀挀愀琀椀漀渀⼀樀猀漀渀怀ഀഀ
     *਍     ⨀ 吀漀 愀搀搀 漀爀 漀瘀攀爀眀爀椀琀攀 琀栀攀猀攀 搀攀昀愀甀氀琀猀Ⰰ 猀椀洀瀀氀礀 愀搀搀 漀爀 爀攀洀漀瘀攀 愀 瀀爀漀瀀攀爀琀礀 昀爀漀洀 琀栀攀猀攀 挀漀渀昀椀最甀爀愀琀椀漀渀ഀഀ
     * objects. To add headers for an HTTP method other than POST or PUT, simply add a new object਍     ⨀ 眀椀琀栀 琀栀攀 氀漀眀攀爀挀愀猀攀搀 䠀吀吀倀 洀攀琀栀漀搀 渀愀洀攀 愀猀 琀栀攀 欀攀礀Ⰰ 攀⸀最⸀ഀഀ
     * `$httpProvider.defaults.headers.get = { 'My-Header' : 'value' }.਍     ⨀ഀഀ
     * The defaults can also be set at runtime via the `$http.defaults` object in the same਍     ⨀ 昀愀猀栀椀漀渀⸀ 䘀漀爀 攀砀愀洀瀀氀攀㨀ഀഀ
     *਍     ⨀ 怀怀怀ഀഀ
     * module.run(function($http) {਍     ⨀   ␀栀琀琀瀀⸀搀攀昀愀甀氀琀猀⸀栀攀愀搀攀爀猀⸀挀漀洀洀漀渀⸀䄀甀琀栀攀渀琀椀挀愀琀椀漀渀 㴀 ✀䈀愀猀椀挀 夀洀嘀氀挀䐀瀀椀戀㈀㤀眀✀ഀഀ
     * });਍     ⨀ 怀怀怀ഀഀ
     *਍     ⨀ 䤀渀 愀搀搀椀琀椀漀渀Ⰰ 礀漀甀 挀愀渀 猀甀瀀瀀氀礀 愀 怀栀攀愀搀攀爀猀怀 瀀爀漀瀀攀爀琀礀 椀渀 琀栀攀 挀漀渀昀椀最 漀戀樀攀挀琀 瀀愀猀猀攀搀 眀栀攀渀ഀഀ
     * calling `$http(config)`, which overrides the defaults without changing them globally.਍     ⨀ഀഀ
     *਍     ⨀ ⌀ 吀爀愀渀猀昀漀爀洀椀渀最 刀攀焀甀攀猀琀猀 愀渀搀 刀攀猀瀀漀渀猀攀猀ഀഀ
     *਍     ⨀ 䈀漀琀栀 爀攀焀甀攀猀琀猀 愀渀搀 爀攀猀瀀漀渀猀攀猀 挀愀渀 戀攀 琀爀愀渀猀昀漀爀洀攀搀 甀猀椀渀最 琀爀愀渀猀昀漀爀洀 昀甀渀挀琀椀漀渀猀⸀ 䈀礀 搀攀昀愀甀氀琀Ⰰ 䄀渀最甀氀愀爀ഀഀ
     * applies these transformations:਍     ⨀ഀഀ
     * Request transformations:਍     ⨀ഀഀ
     * - If the `data` property of the request configuration object contains an object, serialize it਍     ⨀   椀渀琀漀 䨀匀伀一 昀漀爀洀愀琀⸀ഀഀ
     *਍     ⨀ 刀攀猀瀀漀渀猀攀 琀爀愀渀猀昀漀爀洀愀琀椀漀渀猀㨀ഀഀ
     *਍     ⨀  ⴀ 䤀昀 堀匀刀䘀 瀀爀攀昀椀砀 椀猀 搀攀琀攀挀琀攀搀Ⰰ 猀琀爀椀瀀 椀琀 ⠀猀攀攀 匀攀挀甀爀椀琀礀 䌀漀渀猀椀搀攀爀愀琀椀漀渀猀 猀攀挀琀椀漀渀 戀攀氀漀眀⤀⸀ഀഀ
     *  - If JSON response is detected, deserialize it using a JSON parser.਍     ⨀ഀഀ
     * To globally augment or override the default transforms, modify the਍     ⨀ 怀␀栀琀琀瀀倀爀漀瘀椀搀攀爀⸀搀攀昀愀甀氀琀猀⸀琀爀愀渀猀昀漀爀洀刀攀焀甀攀猀琀怀 愀渀搀 怀␀栀琀琀瀀倀爀漀瘀椀搀攀爀⸀搀攀昀愀甀氀琀猀⸀琀爀愀渀猀昀漀爀洀刀攀猀瀀漀渀猀攀怀ഀഀ
     * properties. These properties are by default an array of transform functions, which allows you਍     ⨀ 琀漀 怀瀀甀猀栀怀 漀爀 怀甀渀猀栀椀昀琀怀 愀 渀攀眀 琀爀愀渀猀昀漀爀洀愀琀椀漀渀 昀甀渀挀琀椀漀渀 椀渀琀漀 琀栀攀 琀爀愀渀猀昀漀爀洀愀琀椀漀渀 挀栀愀椀渀⸀ 夀漀甀 挀愀渀ഀഀ
     * also decide to completely override any default transformations by assigning your਍     ⨀ 琀爀愀渀猀昀漀爀洀愀琀椀漀渀 昀甀渀挀琀椀漀渀猀 琀漀 琀栀攀猀攀 瀀爀漀瀀攀爀琀椀攀猀 搀椀爀攀挀琀氀礀 眀椀琀栀漀甀琀 琀栀攀 愀爀爀愀礀 眀爀愀瀀瀀攀爀⸀  吀栀攀猀攀 搀攀昀愀甀氀琀猀ഀഀ
     * are again available on the $http factory at run-time, which may be useful if you have run-time ਍     ⨀ 猀攀爀瘀椀挀攀猀 礀漀甀 眀椀猀栀 琀漀 戀攀 椀渀瘀漀氀瘀攀搀 椀渀 礀漀甀爀 琀爀愀渀猀昀漀爀洀愀琀椀漀渀猀⸀ഀഀ
     *਍     ⨀ 匀椀洀椀氀愀爀氀礀Ⰰ 琀漀 氀漀挀愀氀氀礀 漀瘀攀爀爀椀搀攀 琀栀攀 爀攀焀甀攀猀琀⼀爀攀猀瀀漀渀猀攀 琀爀愀渀猀昀漀爀洀猀Ⰰ 愀甀最洀攀渀琀 琀栀攀ഀഀ
     * `transformRequest` and/or `transformResponse` properties of the configuration object passed਍     ⨀ 椀渀琀漀 怀␀栀琀琀瀀怀⸀ഀഀ
     *਍     ⨀ഀഀ
     * # Caching਍     ⨀ഀഀ
     * To enable caching, set the request configuration `cache` property to `true` (to use default਍     ⨀ 挀愀挀栀攀⤀ 漀爀 琀漀 愀 挀甀猀琀漀洀 挀愀挀栀攀 漀戀樀攀挀琀 ⠀戀甀椀氀琀 眀椀琀栀 笀䀀氀椀渀欀 渀最⸀␀挀愀挀栀攀䘀愀挀琀漀爀礀 怀␀挀愀挀栀攀䘀愀挀琀漀爀礀怀紀⤀⸀ഀഀ
     * When the cache is enabled, `$http` stores the response from the server in the specified਍     ⨀ 挀愀挀栀攀⸀ 吀栀攀 渀攀砀琀 琀椀洀攀 琀栀攀 猀愀洀攀 爀攀焀甀攀猀琀 椀猀 洀愀搀攀Ⰰ 琀栀攀 爀攀猀瀀漀渀猀攀 椀猀 猀攀爀瘀攀搀 昀爀漀洀 琀栀攀 挀愀挀栀攀 眀椀琀栀漀甀琀ഀഀ
     * sending a request to the server.਍     ⨀ഀഀ
     * Note that even if the response is served from cache, delivery of the data is asynchronous in਍     ⨀ 琀栀攀 猀愀洀攀 眀愀礀 琀栀愀琀 爀攀愀氀 爀攀焀甀攀猀琀猀 愀爀攀⸀ഀഀ
     *਍     ⨀ 䤀昀 琀栀攀爀攀 愀爀攀 洀甀氀琀椀瀀氀攀 䜀䔀吀 爀攀焀甀攀猀琀猀 昀漀爀 琀栀攀 猀愀洀攀 唀刀䰀 琀栀愀琀 猀栀漀甀氀搀 戀攀 挀愀挀栀攀搀 甀猀椀渀最 琀栀攀 猀愀洀攀ഀഀ
     * cache, but the cache is not populated yet, only one request to the server will be made and਍     ⨀ 琀栀攀 爀攀洀愀椀渀椀渀最 爀攀焀甀攀猀琀猀 眀椀氀氀 戀攀 昀甀氀昀椀氀氀攀搀 甀猀椀渀最 琀栀攀 爀攀猀瀀漀渀猀攀 昀爀漀洀 琀栀攀 昀椀爀猀琀 爀攀焀甀攀猀琀⸀ഀഀ
     *਍     ⨀ 夀漀甀 挀愀渀 挀栀愀渀最攀 琀栀攀 搀攀昀愀甀氀琀 挀愀挀栀攀 琀漀 愀 渀攀眀 漀戀樀攀挀琀 ⠀戀甀椀氀琀 眀椀琀栀ഀഀ
     * {@link ng.$cacheFactory `$cacheFactory`}) by updating the਍     ⨀ 笀䀀氀椀渀欀 渀最⸀␀栀琀琀瀀⌀瀀爀漀瀀攀爀琀椀攀猀开搀攀昀愀甀氀琀猀 怀␀栀琀琀瀀⸀搀攀昀愀甀氀琀猀⸀挀愀挀栀攀怀紀 瀀爀漀瀀攀爀琀礀⸀ 䄀氀氀 爀攀焀甀攀猀琀猀 眀栀漀 猀攀琀ഀഀ
     * their `cache` property to `true` will now use this cache object.਍     ⨀ഀഀ
     * If you set the default cache to `false` then only requests that specify their own custom਍     ⨀ 挀愀挀栀攀 漀戀樀攀挀琀 眀椀氀氀 戀攀 挀愀挀栀攀搀⸀ഀഀ
     *਍     ⨀ ⌀ 䤀渀琀攀爀挀攀瀀琀漀爀猀ഀഀ
     *਍     ⨀ 䈀攀昀漀爀攀 礀漀甀 猀琀愀爀琀 挀爀攀愀琀椀渀最 椀渀琀攀爀挀攀瀀琀漀爀猀Ⰰ 戀攀 猀甀爀攀 琀漀 甀渀搀攀爀猀琀愀渀搀 琀栀攀ഀഀ
     * {@link ng.$q $q and deferred/promise APIs}.਍     ⨀ഀഀ
     * For purposes of global error handling, authentication, or any kind of synchronous or਍     ⨀ 愀猀礀渀挀栀爀漀渀漀甀猀 瀀爀攀ⴀ瀀爀漀挀攀猀猀椀渀最 漀昀 爀攀焀甀攀猀琀 漀爀 瀀漀猀琀瀀爀漀挀攀猀猀椀渀最 漀昀 爀攀猀瀀漀渀猀攀猀Ⰰ 椀琀 椀猀 搀攀猀椀爀愀戀氀攀 琀漀 戀攀ഀഀ
     * able to intercept requests before they are handed to the server and਍     ⨀ 爀攀猀瀀漀渀猀攀猀 戀攀昀漀爀攀 琀栀攀礀 愀爀攀 栀愀渀搀攀搀 漀瘀攀爀 琀漀 琀栀攀 愀瀀瀀氀椀挀愀琀椀漀渀 挀漀搀攀 琀栀愀琀ഀഀ
     * initiated these requests. The interceptors leverage the {@link ng.$q਍     ⨀ 瀀爀漀洀椀猀攀 䄀倀䤀猀紀 琀漀 昀甀氀昀椀氀氀 琀栀椀猀 渀攀攀搀 昀漀爀 戀漀琀栀 猀礀渀挀栀爀漀渀漀甀猀 愀渀搀 愀猀礀渀挀栀爀漀渀漀甀猀 瀀爀攀ⴀ瀀爀漀挀攀猀猀椀渀最⸀ഀഀ
     *਍     ⨀ 吀栀攀 椀渀琀攀爀挀攀瀀琀漀爀猀 愀爀攀 猀攀爀瘀椀挀攀 昀愀挀琀漀爀椀攀猀 琀栀愀琀 愀爀攀 爀攀最椀猀琀攀爀攀搀 眀椀琀栀 琀栀攀 怀␀栀琀琀瀀倀爀漀瘀椀搀攀爀怀 戀礀ഀഀ
     * adding them to the `$httpProvider.interceptors` array. The factory is called and਍     ⨀ 椀渀樀攀挀琀攀搀 眀椀琀栀 搀攀瀀攀渀搀攀渀挀椀攀猀 ⠀椀昀 猀瀀攀挀椀昀椀攀搀⤀ 愀渀搀 爀攀琀甀爀渀猀 琀栀攀 椀渀琀攀爀挀攀瀀琀漀爀⸀ഀഀ
     *਍     ⨀ 吀栀攀爀攀 愀爀攀 琀眀漀 欀椀渀搀猀 漀昀 椀渀琀攀爀挀攀瀀琀漀爀猀 ⠀愀渀搀 琀眀漀 欀椀渀搀猀 漀昀 爀攀樀攀挀琀椀漀渀 椀渀琀攀爀挀攀瀀琀漀爀猀⤀㨀ഀഀ
     *਍     ⨀   ⨀ 怀爀攀焀甀攀猀琀怀㨀 椀渀琀攀爀挀攀瀀琀漀爀猀 最攀琀 挀愀氀氀攀搀 眀椀琀栀 栀琀琀瀀 怀挀漀渀昀椀最怀 漀戀樀攀挀琀⸀ 吀栀攀 昀甀渀挀琀椀漀渀 椀猀 昀爀攀攀 琀漀ഀഀ
     *     modify the `config` or create a new one. The function needs to return the `config`਍     ⨀     搀椀爀攀挀琀氀礀 漀爀 愀猀 愀 瀀爀漀洀椀猀攀⸀ഀഀ
     *   * `requestError`: interceptor gets called when a previous interceptor threw an error or਍     ⨀     爀攀猀漀氀瘀攀搀 眀椀琀栀 愀 爀攀樀攀挀琀椀漀渀⸀ഀഀ
     *   * `response`: interceptors get called with http `response` object. The function is free to਍     ⨀     洀漀搀椀昀礀 琀栀攀 怀爀攀猀瀀漀渀猀攀怀 漀爀 挀爀攀愀琀攀 愀 渀攀眀 漀渀攀⸀ 吀栀攀 昀甀渀挀琀椀漀渀 渀攀攀搀猀 琀漀 爀攀琀甀爀渀 琀栀攀 怀爀攀猀瀀漀渀猀攀怀ഀഀ
     *     directly or as a promise.਍     ⨀   ⨀ 怀爀攀猀瀀漀渀猀攀䔀爀爀漀爀怀㨀 椀渀琀攀爀挀攀瀀琀漀爀 最攀琀猀 挀愀氀氀攀搀 眀栀攀渀 愀 瀀爀攀瘀椀漀甀猀 椀渀琀攀爀挀攀瀀琀漀爀 琀栀爀攀眀 愀渀 攀爀爀漀爀 漀爀ഀഀ
     *     resolved with a rejection.਍     ⨀ഀഀ
     *਍     ⨀ 㰀瀀爀攀㸀ഀഀ
     *   // register the interceptor as a service਍     ⨀   ␀瀀爀漀瘀椀搀攀⸀昀愀挀琀漀爀礀⠀✀洀礀䠀琀琀瀀䤀渀琀攀爀挀攀瀀琀漀爀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀焀Ⰰ 搀攀瀀攀渀搀攀渀挀礀㄀Ⰰ 搀攀瀀攀渀搀攀渀挀礀㈀⤀ 笀ഀഀ
     *     return {਍     ⨀       ⼀⼀ 漀瀀琀椀漀渀愀氀 洀攀琀栀漀搀ഀഀ
     *       'request': function(config) {਍     ⨀         ⼀⼀ 搀漀 猀漀洀攀琀栀椀渀最 漀渀 猀甀挀挀攀猀猀ഀഀ
     *         return config || $q.when(config);਍     ⨀       紀Ⰰഀഀ
     *਍     ⨀       ⼀⼀ 漀瀀琀椀漀渀愀氀 洀攀琀栀漀搀ഀഀ
     *      'requestError': function(rejection) {਍     ⨀         ⼀⼀ 搀漀 猀漀洀攀琀栀椀渀最 漀渀 攀爀爀漀爀ഀഀ
     *         if (canRecover(rejection)) {਍     ⨀           爀攀琀甀爀渀 爀攀猀瀀漀渀猀攀伀爀一攀眀倀爀漀洀椀猀攀ഀഀ
     *         }਍     ⨀         爀攀琀甀爀渀 ␀焀⸀爀攀樀攀挀琀⠀爀攀樀攀挀琀椀漀渀⤀㬀ഀഀ
     *       },਍     ⨀ഀഀ
     *਍     ⨀ഀഀ
     *       // optional method਍     ⨀       ✀爀攀猀瀀漀渀猀攀✀㨀 昀甀渀挀琀椀漀渀⠀爀攀猀瀀漀渀猀攀⤀ 笀ഀഀ
     *         // do something on success਍     ⨀         爀攀琀甀爀渀 爀攀猀瀀漀渀猀攀 簀簀 ␀焀⸀眀栀攀渀⠀爀攀猀瀀漀渀猀攀⤀㬀ഀഀ
     *       },਍     ⨀ഀഀ
     *       // optional method਍     ⨀      ✀爀攀猀瀀漀渀猀攀䔀爀爀漀爀✀㨀 昀甀渀挀琀椀漀渀⠀爀攀樀攀挀琀椀漀渀⤀ 笀ഀഀ
     *         // do something on error਍     ⨀         椀昀 ⠀挀愀渀刀攀挀漀瘀攀爀⠀爀攀樀攀挀琀椀漀渀⤀⤀ 笀ഀഀ
     *           return responseOrNewPromise਍     ⨀         紀ഀഀ
     *         return $q.reject(rejection);਍     ⨀       紀ഀഀ
     *     };਍     ⨀   紀⤀㬀ഀഀ
     *਍     ⨀   ␀栀琀琀瀀倀爀漀瘀椀搀攀爀⸀椀渀琀攀爀挀攀瀀琀漀爀猀⸀瀀甀猀栀⠀✀洀礀䠀琀琀瀀䤀渀琀攀爀挀攀瀀琀漀爀✀⤀㬀ഀഀ
     *਍     ⨀ഀഀ
     *   // alternatively, register the interceptor via an anonymous factory਍     ⨀   ␀栀琀琀瀀倀爀漀瘀椀搀攀爀⸀椀渀琀攀爀挀攀瀀琀漀爀猀⸀瀀甀猀栀⠀昀甀渀挀琀椀漀渀⠀␀焀Ⰰ 搀攀瀀攀渀搀攀渀挀礀㄀Ⰰ 搀攀瀀攀渀搀攀渀挀礀㈀⤀ 笀ഀഀ
     *     return {਍     ⨀      ✀爀攀焀甀攀猀琀✀㨀 昀甀渀挀琀椀漀渀⠀挀漀渀昀椀最⤀ 笀ഀഀ
     *          // same as above਍     ⨀       紀Ⰰഀഀ
     *਍     ⨀       ✀爀攀猀瀀漀渀猀攀✀㨀 昀甀渀挀琀椀漀渀⠀爀攀猀瀀漀渀猀攀⤀ 笀ഀഀ
     *          // same as above਍     ⨀       紀ഀഀ
     *     };਍     ⨀   紀⤀㬀ഀഀ
     * </pre>਍     ⨀ഀഀ
     * # Response interceptors (DEPRECATED)਍     ⨀ഀഀ
     * Before you start creating interceptors, be sure to understand the਍     ⨀ 笀䀀氀椀渀欀 渀最⸀␀焀 ␀焀 愀渀搀 搀攀昀攀爀爀攀搀⼀瀀爀漀洀椀猀攀 䄀倀䤀猀紀⸀ഀഀ
     *਍     ⨀ 䘀漀爀 瀀甀爀瀀漀猀攀猀 漀昀 最氀漀戀愀氀 攀爀爀漀爀 栀愀渀搀氀椀渀最Ⰰ 愀甀琀栀攀渀琀椀挀愀琀椀漀渀 漀爀 愀渀礀 欀椀渀搀 漀昀 猀礀渀挀栀爀漀渀漀甀猀 漀爀ഀഀ
     * asynchronous preprocessing of received responses, it is desirable to be able to intercept਍     ⨀ 爀攀猀瀀漀渀猀攀猀 昀漀爀 栀琀琀瀀 爀攀焀甀攀猀琀猀 戀攀昀漀爀攀 琀栀攀礀 愀爀攀 栀愀渀搀攀搀 漀瘀攀爀 琀漀 琀栀攀 愀瀀瀀氀椀挀愀琀椀漀渀 挀漀搀攀 琀栀愀琀ഀഀ
     * initiated these requests. The response interceptors leverage the {@link ng.$q਍     ⨀ 瀀爀漀洀椀猀攀 愀瀀椀猀紀 琀漀 昀甀氀昀椀氀 琀栀椀猀 渀攀攀搀 昀漀爀 戀漀琀栀 猀礀渀挀栀爀漀渀漀甀猀 愀渀搀 愀猀礀渀挀栀爀漀渀漀甀猀 瀀爀攀瀀爀漀挀攀猀猀椀渀最⸀ഀഀ
     *਍     ⨀ 吀栀攀 椀渀琀攀爀挀攀瀀琀漀爀猀 愀爀攀 猀攀爀瘀椀挀攀 昀愀挀琀漀爀椀攀猀 琀栀愀琀 愀爀攀 爀攀最椀猀琀攀爀攀搀 眀椀琀栀 琀栀攀 ␀栀琀琀瀀倀爀漀瘀椀搀攀爀 戀礀ഀഀ
     * adding them to the `$httpProvider.responseInterceptors` array. The factory is called and਍     ⨀ 椀渀樀攀挀琀攀搀 眀椀琀栀 搀攀瀀攀渀搀攀渀挀椀攀猀 ⠀椀昀 猀瀀攀挀椀昀椀攀搀⤀ 愀渀搀 爀攀琀甀爀渀猀 琀栀攀 椀渀琀攀爀挀攀瀀琀漀爀  ﴀ﷿⃿愀 昀甀渀挀琀椀漀渀 琀栀愀琀ഀഀ
     * takes a {@link ng.$q promise} and returns the original or a new promise.਍     ⨀ഀഀ
     * <pre>਍     ⨀   ⼀⼀ 爀攀最椀猀琀攀爀 琀栀攀 椀渀琀攀爀挀攀瀀琀漀爀 愀猀 愀 猀攀爀瘀椀挀攀ഀഀ
     *   $provide.factory('myHttpInterceptor', function($q, dependency1, dependency2) {਍     ⨀     爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀瀀爀漀洀椀猀攀⤀ 笀ഀഀ
     *       return promise.then(function(response) {਍     ⨀         ⼀⼀ 搀漀 猀漀洀攀琀栀椀渀最 漀渀 猀甀挀挀攀猀猀ഀഀ
     *         return response;਍     ⨀       紀Ⰰ 昀甀渀挀琀椀漀渀⠀爀攀猀瀀漀渀猀攀⤀ 笀ഀഀ
     *         // do something on error਍     ⨀         椀昀 ⠀挀愀渀刀攀挀漀瘀攀爀⠀爀攀猀瀀漀渀猀攀⤀⤀ 笀ഀഀ
     *           return responseOrNewPromise਍     ⨀         紀ഀഀ
     *         return $q.reject(response);਍     ⨀       紀⤀㬀ഀഀ
     *     }਍     ⨀   紀⤀㬀ഀഀ
     *਍     ⨀   ␀栀琀琀瀀倀爀漀瘀椀搀攀爀⸀爀攀猀瀀漀渀猀攀䤀渀琀攀爀挀攀瀀琀漀爀猀⸀瀀甀猀栀⠀✀洀礀䠀琀琀瀀䤀渀琀攀爀挀攀瀀琀漀爀✀⤀㬀ഀഀ
     *਍     ⨀ഀഀ
     *   // register the interceptor via an anonymous factory਍     ⨀   ␀栀琀琀瀀倀爀漀瘀椀搀攀爀⸀爀攀猀瀀漀渀猀攀䤀渀琀攀爀挀攀瀀琀漀爀猀⸀瀀甀猀栀⠀昀甀渀挀琀椀漀渀⠀␀焀Ⰰ 搀攀瀀攀渀搀攀渀挀礀㄀Ⰰ 搀攀瀀攀渀搀攀渀挀礀㈀⤀ 笀ഀഀ
     *     return function(promise) {਍     ⨀       ⼀⼀ 猀愀洀攀 愀猀 愀戀漀瘀攀ഀഀ
     *     }਍     ⨀   紀⤀㬀ഀഀ
     * </pre>਍     ⨀ഀഀ
     *਍     ⨀ ⌀ 匀攀挀甀爀椀琀礀 䌀漀渀猀椀搀攀爀愀琀椀漀渀猀ഀഀ
     *਍     ⨀ 圀栀攀渀 搀攀猀椀最渀椀渀最 眀攀戀 愀瀀瀀氀椀挀愀琀椀漀渀猀Ⰰ 挀漀渀猀椀搀攀爀 猀攀挀甀爀椀琀礀 琀栀爀攀愀琀猀 昀爀漀洀㨀ഀഀ
     *਍     ⨀ ⴀ 笀䀀氀椀渀欀 栀琀琀瀀㨀⼀⼀栀愀愀挀欀攀搀⸀挀漀洀⼀愀爀挀栀椀瘀攀⼀㈀　　㠀⼀㄀㄀⼀㈀　⼀愀渀愀琀漀洀礀ⴀ漀昀ⴀ愀ⴀ猀甀戀琀氀攀ⴀ樀猀漀渀ⴀ瘀甀氀渀攀爀愀戀椀氀椀琀礀⸀愀猀瀀砀ഀഀ
     *   JSON vulnerability}਍     ⨀ ⴀ 笀䀀氀椀渀欀 栀琀琀瀀㨀⼀⼀攀渀⸀眀椀欀椀瀀攀搀椀愀⸀漀爀最⼀眀椀欀椀⼀䌀爀漀猀猀ⴀ猀椀琀攀开爀攀焀甀攀猀琀开昀漀爀最攀爀礀 堀匀刀䘀紀ഀഀ
     *਍     ⨀ 䈀漀琀栀 猀攀爀瘀攀爀 愀渀搀 琀栀攀 挀氀椀攀渀琀 洀甀猀琀 挀漀漀瀀攀爀愀琀攀 椀渀 漀爀搀攀爀 琀漀 攀氀椀洀椀渀愀琀攀 琀栀攀猀攀 琀栀爀攀愀琀猀⸀ 䄀渀最甀氀愀爀 挀漀洀攀猀ഀഀ
     * pre-configured with strategies that address these issues, but for this to work backend server਍     ⨀ 挀漀漀瀀攀爀愀琀椀漀渀 椀猀 爀攀焀甀椀爀攀搀⸀ഀഀ
     *਍     ⨀ ⌀⌀ 䨀匀伀一 嘀甀氀渀攀爀愀戀椀氀椀琀礀 倀爀漀琀攀挀琀椀漀渀ഀഀ
     *਍     ⨀ 䄀 笀䀀氀椀渀欀 栀琀琀瀀㨀⼀⼀栀愀愀挀欀攀搀⸀挀漀洀⼀愀爀挀栀椀瘀攀⼀㈀　　㠀⼀㄀㄀⼀㈀　⼀愀渀愀琀漀洀礀ⴀ漀昀ⴀ愀ⴀ猀甀戀琀氀攀ⴀ樀猀漀渀ⴀ瘀甀氀渀攀爀愀戀椀氀椀琀礀⸀愀猀瀀砀ഀഀ
     * JSON vulnerability} allows third party website to turn your JSON resource URL into਍     ⨀ 笀䀀氀椀渀欀 栀琀琀瀀㨀⼀⼀攀渀⸀眀椀欀椀瀀攀搀椀愀⸀漀爀最⼀眀椀欀椀⼀䨀匀伀一倀 䨀匀伀一倀紀 爀攀焀甀攀猀琀 甀渀搀攀爀 猀漀洀攀 挀漀渀搀椀琀椀漀渀猀⸀ 吀漀ഀഀ
     * counter this your server can prefix all JSON requests with following string `")]}',\n"`.਍     ⨀ 䄀渀最甀氀愀爀 眀椀氀氀 愀甀琀漀洀愀琀椀挀愀氀氀礀 猀琀爀椀瀀 琀栀攀 瀀爀攀昀椀砀 戀攀昀漀爀攀 瀀爀漀挀攀猀猀椀渀最 椀琀 愀猀 䨀匀伀一⸀ഀഀ
     *਍     ⨀ 䘀漀爀 攀砀愀洀瀀氀攀 椀昀 礀漀甀爀 猀攀爀瘀攀爀 渀攀攀搀猀 琀漀 爀攀琀甀爀渀㨀ഀഀ
     * <pre>਍     ⨀ 嬀✀漀渀攀✀Ⰰ✀琀眀漀✀崀ഀഀ
     * </pre>਍     ⨀ഀഀ
     * which is vulnerable to attack, your server can return:਍     ⨀ 㰀瀀爀攀㸀ഀഀ
     * )]}',਍     ⨀ 嬀✀漀渀攀✀Ⰰ✀琀眀漀✀崀ഀഀ
     * </pre>਍     ⨀ഀഀ
     * Angular will strip the prefix, before processing the JSON.਍     ⨀ഀഀ
     *਍     ⨀ ⌀⌀ 䌀爀漀猀猀 匀椀琀攀 刀攀焀甀攀猀琀 䘀漀爀最攀爀礀 ⠀堀匀刀䘀⤀ 倀爀漀琀攀挀琀椀漀渀ഀഀ
     *਍     ⨀ 笀䀀氀椀渀欀 栀琀琀瀀㨀⼀⼀攀渀⸀眀椀欀椀瀀攀搀椀愀⸀漀爀最⼀眀椀欀椀⼀䌀爀漀猀猀ⴀ猀椀琀攀开爀攀焀甀攀猀琀开昀漀爀最攀爀礀 堀匀刀䘀紀 椀猀 愀 琀攀挀栀渀椀焀甀攀 戀礀 眀栀椀挀栀ഀഀ
     * an unauthorized site can gain your user's private data. Angular provides a mechanism਍     ⨀ 琀漀 挀漀甀渀琀攀爀 堀匀刀䘀⸀ 圀栀攀渀 瀀攀爀昀漀爀洀椀渀最 堀䠀刀 爀攀焀甀攀猀琀猀Ⰰ 琀栀攀 ␀栀琀琀瀀 猀攀爀瘀椀挀攀 爀攀愀搀猀 愀 琀漀欀攀渀 昀爀漀洀 愀 挀漀漀欀椀攀ഀഀ
     * (by default, `XSRF-TOKEN`) and sets it as an HTTP header (`X-XSRF-TOKEN`). Since only਍     ⨀ 䨀愀瘀愀匀挀爀椀瀀琀 琀栀愀琀 爀甀渀猀 漀渀 礀漀甀爀 搀漀洀愀椀渀 挀漀甀氀搀 爀攀愀搀 琀栀攀 挀漀漀欀椀攀Ⰰ 礀漀甀爀 猀攀爀瘀攀爀 挀愀渀 戀攀 愀猀猀甀爀攀搀 琀栀愀琀ഀഀ
     * the XHR came from JavaScript running on your domain. The header will not be set for਍     ⨀ 挀爀漀猀猀ⴀ搀漀洀愀椀渀 爀攀焀甀攀猀琀猀⸀ഀഀ
     *਍     ⨀ 吀漀 琀愀欀攀 愀搀瘀愀渀琀愀最攀 漀昀 琀栀椀猀Ⰰ 礀漀甀爀 猀攀爀瘀攀爀 渀攀攀搀猀 琀漀 猀攀琀 愀 琀漀欀攀渀 椀渀 愀 䨀愀瘀愀匀挀爀椀瀀琀 爀攀愀搀愀戀氀攀 猀攀猀猀椀漀渀ഀഀ
     * cookie called `XSRF-TOKEN` on the first HTTP GET request. On subsequent XHR requests the਍     ⨀ 猀攀爀瘀攀爀 挀愀渀 瘀攀爀椀昀礀 琀栀愀琀 琀栀攀 挀漀漀欀椀攀 洀愀琀挀栀攀猀 怀堀ⴀ堀匀刀䘀ⴀ吀伀䬀䔀一怀 䠀吀吀倀 栀攀愀搀攀爀Ⰰ 愀渀搀 琀栀攀爀攀昀漀爀攀 戀攀 猀甀爀攀ഀഀ
     * that only JavaScript running on your domain could have sent the request. The token must be਍     ⨀ 甀渀椀焀甀攀 昀漀爀 攀愀挀栀 甀猀攀爀 愀渀搀 洀甀猀琀 戀攀 瘀攀爀椀昀椀愀戀氀攀 戀礀 琀栀攀 猀攀爀瘀攀爀 ⠀琀漀 瀀爀攀瘀攀渀琀 琀栀攀 䨀愀瘀愀匀挀爀椀瀀琀 昀爀漀洀ഀഀ
     * making up its own tokens). We recommend that the token is a digest of your site's਍     ⨀ 愀甀琀栀攀渀琀椀挀愀琀椀漀渀 挀漀漀欀椀攀 眀椀琀栀 愀 笀䀀氀椀渀欀 栀琀琀瀀猀㨀⼀⼀攀渀⸀眀椀欀椀瀀攀搀椀愀⸀漀爀最⼀眀椀欀椀⼀匀愀氀琀开⠀挀爀礀瀀琀漀最爀愀瀀栀礀⤀ 猀愀氀琀紀ഀഀ
     * for added security.਍     ⨀ഀഀ
     * The name of the headers can be specified using the xsrfHeaderName and xsrfCookieName਍     ⨀ 瀀爀漀瀀攀爀琀椀攀猀 漀昀 攀椀琀栀攀爀 ␀栀琀琀瀀倀爀漀瘀椀搀攀爀⸀搀攀昀愀甀氀琀猀 愀琀 挀漀渀昀椀最ⴀ琀椀洀攀Ⰰ ␀栀琀琀瀀⸀搀攀昀愀甀氀琀猀 愀琀 爀甀渀ⴀ琀椀洀攀Ⰰഀഀ
     * or the per-request config object.਍     ⨀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀漀戀樀攀挀琀紀 挀漀渀昀椀最 伀戀樀攀挀琀 搀攀猀挀爀椀戀椀渀最 琀栀攀 爀攀焀甀攀猀琀 琀漀 戀攀 洀愀搀攀 愀渀搀 栀漀眀 椀琀 猀栀漀甀氀搀 戀攀ഀഀ
     *    processed. The object has following properties:਍     ⨀ഀഀ
     *    - **method** �� `{string}` �� HTTP method (e.g. 'GET', 'POST', etc)਍     ⨀    ⴀ ⨀⨀甀爀氀⨀⨀ ﴀ﷿⃿怀笀猀琀爀椀渀最紀怀 ﴀ﷿⃿䄀戀猀漀氀甀琀攀 漀爀 爀攀氀愀琀椀瘀攀 唀刀䰀 漀昀 琀栀攀 爀攀猀漀甀爀挀攀 琀栀愀琀 椀猀 戀攀椀渀最 爀攀焀甀攀猀琀攀搀⸀ഀഀ
     *    - **params** �� `{Object.<string|Object>}` �� Map of strings or objects which will be turned਍     ⨀      琀漀 怀㼀欀攀礀㄀㴀瘀愀氀甀攀㄀☀欀攀礀㈀㴀瘀愀氀甀攀㈀怀 愀昀琀攀爀 琀栀攀 甀爀氀⸀ 䤀昀 琀栀攀 瘀愀氀甀攀 椀猀 渀漀琀 愀 猀琀爀椀渀最Ⰰ 椀琀 眀椀氀氀 戀攀ഀഀ
     *      JSONified.਍     ⨀    ⴀ ⨀⨀搀愀琀愀⨀⨀ ﴀ﷿⃿怀笀猀琀爀椀渀最簀伀戀樀攀挀琀紀怀 ﴀ﷿⃿䐀愀琀愀 琀漀 戀攀 猀攀渀琀 愀猀 琀栀攀 爀攀焀甀攀猀琀 洀攀猀猀愀最攀 搀愀琀愀⸀ഀഀ
     *    - **headers** �� `{Object}` �� Map of strings or functions which return strings representing਍     ⨀      䠀吀吀倀 栀攀愀搀攀爀猀 琀漀 猀攀渀搀 琀漀 琀栀攀 猀攀爀瘀攀爀⸀ 䤀昀 琀栀攀 爀攀琀甀爀渀 瘀愀氀甀攀 漀昀 愀 昀甀渀挀琀椀漀渀 椀猀 渀甀氀氀Ⰰ 琀栀攀ഀഀ
     *      header will not be sent.਍     ⨀    ⴀ ⨀⨀砀猀爀昀䠀攀愀搀攀爀一愀洀攀⨀⨀ ﴀ﷿⃿怀笀猀琀爀椀渀最紀怀 ﴀ﷿⃿一愀洀攀 漀昀 䠀吀吀倀 栀攀愀搀攀爀 琀漀 瀀漀瀀甀氀愀琀攀 眀椀琀栀 琀栀攀 堀匀刀䘀 琀漀欀攀渀⸀ഀഀ
     *    - **xsrfCookieName** �� `{string}` �� Name of cookie containing the XSRF token.਍     ⨀    ⴀ ⨀⨀琀爀愀渀猀昀漀爀洀刀攀焀甀攀猀琀⨀⨀ ﴀ﷿෿ഀ
     *      `{function(data, headersGetter)|Array.<function(data, headersGetter)>}` ��਍     ⨀      琀爀愀渀猀昀漀爀洀 昀甀渀挀琀椀漀渀 漀爀 愀渀 愀爀爀愀礀 漀昀 猀甀挀栀 昀甀渀挀琀椀漀渀猀⸀ 吀栀攀 琀爀愀渀猀昀漀爀洀 昀甀渀挀琀椀漀渀 琀愀欀攀猀 琀栀攀 栀琀琀瀀ഀഀ
     *      request body and headers and returns its transformed (typically serialized) version.਍     ⨀    ⴀ ⨀⨀琀爀愀渀猀昀漀爀洀刀攀猀瀀漀渀猀攀⨀⨀ ﴀ﷿෿ഀ
     *      `{function(data, headersGetter)|Array.<function(data, headersGetter)>}` ��਍     ⨀      琀爀愀渀猀昀漀爀洀 昀甀渀挀琀椀漀渀 漀爀 愀渀 愀爀爀愀礀 漀昀 猀甀挀栀 昀甀渀挀琀椀漀渀猀⸀ 吀栀攀 琀爀愀渀猀昀漀爀洀 昀甀渀挀琀椀漀渀 琀愀欀攀猀 琀栀攀 栀琀琀瀀ഀഀ
     *      response body and headers and returns its transformed (typically deserialized) version.਍     ⨀    ⴀ ⨀⨀挀愀挀栀攀⨀⨀ ﴀ﷿⃿怀笀戀漀漀氀攀愀渀簀䌀愀挀栀攀紀怀 ﴀ﷿⃿䤀昀 琀爀甀攀Ⰰ 愀 搀攀昀愀甀氀琀 ␀栀琀琀瀀 挀愀挀栀攀 眀椀氀氀 戀攀 甀猀攀搀 琀漀 挀愀挀栀攀 琀栀攀ഀഀ
     *      GET request, otherwise if a cache instance built with਍     ⨀      笀䀀氀椀渀欀 渀最⸀␀挀愀挀栀攀䘀愀挀琀漀爀礀 ␀挀愀挀栀攀䘀愀挀琀漀爀礀紀Ⰰ 琀栀椀猀 挀愀挀栀攀 眀椀氀氀 戀攀 甀猀攀搀 昀漀爀ഀഀ
     *      caching.਍     ⨀    ⴀ ⨀⨀琀椀洀攀漀甀琀⨀⨀ ﴀ﷿⃿怀笀渀甀洀戀攀爀簀倀爀漀洀椀猀攀紀怀 ﴀ﷿⃿琀椀洀攀漀甀琀 椀渀 洀椀氀氀椀猀攀挀漀渀搀猀Ⰰ 漀爀 笀䀀氀椀渀欀 渀最⸀␀焀 瀀爀漀洀椀猀攀紀ഀഀ
     *      that should abort the request when resolved.਍     ⨀    ⴀ ⨀⨀眀椀琀栀䌀爀攀搀攀渀琀椀愀氀猀⨀⨀ ⴀ 怀笀戀漀漀氀攀愀渀紀怀 ⴀ 眀栀攀琀栀攀爀 琀漀 琀漀 猀攀琀 琀栀攀 怀眀椀琀栀䌀爀攀搀攀渀琀椀愀氀猀怀 昀氀愀最 漀渀 琀栀攀ഀഀ
     *      XHR object. See {@link https://developer.mozilla.org/en/http_access_control#section_5਍     ⨀      爀攀焀甀攀猀琀猀 眀椀琀栀 挀爀攀搀攀渀琀椀愀氀猀紀 昀漀爀 洀漀爀攀 椀渀昀漀爀洀愀琀椀漀渀⸀ഀഀ
     *    - **responseType** - `{string}` - see {@link਍     ⨀      栀琀琀瀀猀㨀⼀⼀搀攀瘀攀氀漀瀀攀爀⸀洀漀稀椀氀氀愀⸀漀爀最⼀攀渀ⴀ唀匀⼀搀漀挀猀⼀䐀伀䴀⼀堀䴀䰀䠀琀琀瀀刀攀焀甀攀猀琀⌀爀攀猀瀀漀渀猀攀吀礀瀀攀 爀攀焀甀攀猀琀吀礀瀀攀紀⸀ഀഀ
     *਍     ⨀ 䀀爀攀琀甀爀渀猀 笀䠀琀琀瀀倀爀漀洀椀猀攀紀 刀攀琀甀爀渀猀 愀 笀䀀氀椀渀欀 渀最⸀␀焀 瀀爀漀洀椀猀攀紀 漀戀樀攀挀琀 眀椀琀栀 琀栀攀ഀഀ
     *   standard `then` method and two http specific methods: `success` and `error`. The `then`਍     ⨀   洀攀琀栀漀搀 琀愀欀攀猀 琀眀漀 愀爀最甀洀攀渀琀猀 愀 猀甀挀挀攀猀猀 愀渀搀 愀渀 攀爀爀漀爀 挀愀氀氀戀愀挀欀 眀栀椀挀栀 眀椀氀氀 戀攀 挀愀氀氀攀搀 眀椀琀栀 愀ഀഀ
     *   response object. The `success` and `error` methods take a single argument - a function that਍     ⨀   眀椀氀氀 戀攀 挀愀氀氀攀搀 眀栀攀渀 琀栀攀 爀攀焀甀攀猀琀 猀甀挀挀攀攀搀猀 漀爀 昀愀椀氀猀 爀攀猀瀀攀挀琀椀瘀攀氀礀⸀ 吀栀攀 愀爀最甀洀攀渀琀猀 瀀愀猀猀攀搀 椀渀琀漀ഀഀ
     *   these functions are destructured representation of the response object passed into the਍     ⨀   怀琀栀攀渀怀 洀攀琀栀漀搀⸀ 吀栀攀 爀攀猀瀀漀渀猀攀 漀戀樀攀挀琀 栀愀猀 琀栀攀猀攀 瀀爀漀瀀攀爀琀椀攀猀㨀ഀഀ
     *਍     ⨀   ⴀ ⨀⨀搀愀琀愀⨀⨀ ﴀ﷿⃿怀笀猀琀爀椀渀最簀伀戀樀攀挀琀紀怀 ﴀ﷿⃿吀栀攀 爀攀猀瀀漀渀猀攀 戀漀搀礀 琀爀愀渀猀昀漀爀洀攀搀 眀椀琀栀 琀栀攀 琀爀愀渀猀昀漀爀洀ഀഀ
     *     functions.਍     ⨀   ⴀ ⨀⨀猀琀愀琀甀猀⨀⨀ ﴀ﷿⃿怀笀渀甀洀戀攀爀紀怀 ﴀ﷿⃿䠀吀吀倀 猀琀愀琀甀猀 挀漀搀攀 漀昀 琀栀攀 爀攀猀瀀漀渀猀攀⸀ഀഀ
     *   - **headers** �� `{function([headerName])}` �� Header getter function.਍     ⨀   ⴀ ⨀⨀挀漀渀昀椀最⨀⨀ ﴀ﷿⃿怀笀伀戀樀攀挀琀紀怀 ﴀ﷿⃿吀栀攀 挀漀渀昀椀最甀爀愀琀椀漀渀 漀戀樀攀挀琀 琀栀愀琀 眀愀猀 甀猀攀搀 琀漀 最攀渀攀爀愀琀攀 琀栀攀 爀攀焀甀攀猀琀⸀ഀഀ
     *਍     ⨀ 䀀瀀爀漀瀀攀爀琀礀 笀䄀爀爀愀礀⸀㰀伀戀樀攀挀琀㸀紀 瀀攀渀搀椀渀最刀攀焀甀攀猀琀猀 䄀爀爀愀礀 漀昀 挀漀渀昀椀最 漀戀樀攀挀琀猀 昀漀爀 挀甀爀爀攀渀琀氀礀 瀀攀渀搀椀渀最ഀഀ
     *   requests. This is primarily meant to be used for debugging purposes.਍     ⨀ഀഀ
     *਍     ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
<example>਍㰀昀椀氀攀 渀愀洀攀㴀∀椀渀搀攀砀⸀栀琀洀氀∀㸀ഀഀ
  <div ng-controller="FetchCtrl">਍    㰀猀攀氀攀挀琀 渀最ⴀ洀漀搀攀氀㴀∀洀攀琀栀漀搀∀㸀ഀഀ
      <option>GET</option>਍      㰀漀瀀琀椀漀渀㸀䨀匀伀一倀㰀⼀漀瀀琀椀漀渀㸀ഀഀ
    </select>਍    㰀椀渀瀀甀琀 琀礀瀀攀㴀∀琀攀砀琀∀ 渀最ⴀ洀漀搀攀氀㴀∀甀爀氀∀ 猀椀稀攀㴀∀㠀　∀⼀㸀ഀഀ
    <button ng-click="fetch()">fetch</button><br>਍    㰀戀甀琀琀漀渀 渀最ⴀ挀氀椀挀欀㴀∀甀瀀搀愀琀攀䴀漀搀攀氀⠀✀䜀䔀吀✀Ⰰ ✀栀琀琀瀀ⴀ栀攀氀氀漀⸀栀琀洀氀✀⤀∀㸀匀愀洀瀀氀攀 䜀䔀吀㰀⼀戀甀琀琀漀渀㸀ഀഀ
    <button਍      渀最ⴀ挀氀椀挀欀㴀∀甀瀀搀愀琀攀䴀漀搀攀氀⠀✀䨀匀伀一倀✀Ⰰഀഀ
                    'http://angularjs.org/greet.php?callback=JSON_CALLBACK&name=Super%20Hero')">਍      匀愀洀瀀氀攀 䨀匀伀一倀ഀഀ
    </button>਍    㰀戀甀琀琀漀渀ഀഀ
      ng-click="updateModel('JSONP', 'http://angularjs.org/doesntexist&callback=JSON_CALLBACK')">਍        䤀渀瘀愀氀椀搀 䨀匀伀一倀ഀഀ
      </button>਍    㰀瀀爀攀㸀栀琀琀瀀 猀琀愀琀甀猀 挀漀搀攀㨀 笀笀猀琀愀琀甀猀紀紀㰀⼀瀀爀攀㸀ഀഀ
    <pre>http response data: {{data}}</pre>਍  㰀⼀搀椀瘀㸀ഀഀ
</file>਍㰀昀椀氀攀 渀愀洀攀㴀∀猀挀爀椀瀀琀⸀樀猀∀㸀ഀഀ
  function FetchCtrl($scope, $http, $templateCache) {਍    ␀猀挀漀瀀攀⸀洀攀琀栀漀搀 㴀 ✀䜀䔀吀✀㬀ഀഀ
    $scope.url = 'http-hello.html';਍ഀഀ
    $scope.fetch = function() {਍      ␀猀挀漀瀀攀⸀挀漀搀攀 㴀 渀甀氀氀㬀ഀഀ
      $scope.response = null;਍ഀഀ
      $http({method: $scope.method, url: $scope.url, cache: $templateCache}).਍        猀甀挀挀攀猀猀⠀昀甀渀挀琀椀漀渀⠀搀愀琀愀Ⰰ 猀琀愀琀甀猀⤀ 笀ഀഀ
          $scope.status = status;਍          ␀猀挀漀瀀攀⸀搀愀琀愀 㴀 搀愀琀愀㬀ഀഀ
        }).਍        攀爀爀漀爀⠀昀甀渀挀琀椀漀渀⠀搀愀琀愀Ⰰ 猀琀愀琀甀猀⤀ 笀ഀഀ
          $scope.data = data || "Request failed";਍          ␀猀挀漀瀀攀⸀猀琀愀琀甀猀 㴀 猀琀愀琀甀猀㬀ഀഀ
      });਍    紀㬀ഀഀ
਍    ␀猀挀漀瀀攀⸀甀瀀搀愀琀攀䴀漀搀攀氀 㴀 昀甀渀挀琀椀漀渀⠀洀攀琀栀漀搀Ⰰ 甀爀氀⤀ 笀ഀഀ
      $scope.method = method;਍      ␀猀挀漀瀀攀⸀甀爀氀 㴀 甀爀氀㬀ഀഀ
    };਍  紀ഀഀ
</file>਍㰀昀椀氀攀 渀愀洀攀㴀∀栀琀琀瀀ⴀ栀攀氀氀漀⸀栀琀洀氀∀㸀ഀഀ
  Hello, $http!਍㰀⼀昀椀氀攀㸀ഀഀ
<file name="scenario.js">਍  椀琀⠀✀猀栀漀甀氀搀 洀愀欀攀 愀渀 砀栀爀 䜀䔀吀 爀攀焀甀攀猀琀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    element(':button:contains("Sample GET")').click();਍    攀氀攀洀攀渀琀⠀✀㨀戀甀琀琀漀渀㨀挀漀渀琀愀椀渀猀⠀∀昀攀琀挀栀∀⤀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
    expect(binding('status')).toBe('200');਍    攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀搀愀琀愀✀⤀⤀⸀琀漀䴀愀琀挀栀⠀⼀䠀攀氀氀漀Ⰰ 尀␀栀琀琀瀀℀⼀⤀㬀ഀഀ
  });਍ഀഀ
  it('should make a JSONP request to angularjs.org', function() {਍    攀氀攀洀攀渀琀⠀✀㨀戀甀琀琀漀渀㨀挀漀渀琀愀椀渀猀⠀∀匀愀洀瀀氀攀 䨀匀伀一倀∀⤀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
    element(':button:contains("fetch")').click();਍    攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀猀琀愀琀甀猀✀⤀⤀⸀琀漀䈀攀⠀✀㈀　　✀⤀㬀ഀഀ
    expect(binding('data')).toMatch(/Super Hero!/);਍  紀⤀㬀ഀഀ
਍  椀琀⠀✀猀栀漀甀氀搀 洀愀欀攀 䨀匀伀一倀 爀攀焀甀攀猀琀 琀漀 椀渀瘀愀氀椀搀 唀刀䰀 愀渀搀 椀渀瘀漀欀攀 琀栀攀 攀爀爀漀爀 栀愀渀搀氀攀爀✀Ⰰഀഀ
      function() {਍    攀氀攀洀攀渀琀⠀✀㨀戀甀琀琀漀渀㨀挀漀渀琀愀椀渀猀⠀∀䤀渀瘀愀氀椀搀 䨀匀伀一倀∀⤀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
    element(':button:contains("fetch")').click();਍    攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀猀琀愀琀甀猀✀⤀⤀⸀琀漀䈀攀⠀✀　✀⤀㬀ഀഀ
    expect(binding('data')).toBe('Request failed');਍  紀⤀㬀ഀഀ
</file>਍㰀⼀攀砀愀洀瀀氀攀㸀ഀഀ
     */਍    昀甀渀挀琀椀漀渀 ␀栀琀琀瀀⠀爀攀焀甀攀猀琀䌀漀渀昀椀最⤀ 笀ഀഀ
      var config = {਍        琀爀愀渀猀昀漀爀洀刀攀焀甀攀猀琀㨀 搀攀昀愀甀氀琀猀⸀琀爀愀渀猀昀漀爀洀刀攀焀甀攀猀琀Ⰰഀഀ
        transformResponse: defaults.transformResponse਍      紀㬀ഀഀ
      var headers = mergeHeaders(requestConfig);਍ഀഀ
      extend(config, requestConfig);਍      挀漀渀昀椀最⸀栀攀愀搀攀爀猀 㴀 栀攀愀搀攀爀猀㬀ഀഀ
      config.method = uppercase(config.method);਍ഀഀ
      var xsrfValue = urlIsSameOrigin(config.url)਍          㼀 ␀戀爀漀眀猀攀爀⸀挀漀漀欀椀攀猀⠀⤀嬀挀漀渀昀椀最⸀砀猀爀昀䌀漀漀欀椀攀一愀洀攀 簀簀 搀攀昀愀甀氀琀猀⸀砀猀爀昀䌀漀漀欀椀攀一愀洀攀崀ഀഀ
          : undefined;਍      椀昀 ⠀砀猀爀昀嘀愀氀甀攀⤀ 笀ഀഀ
        headers[(config.xsrfHeaderName || defaults.xsrfHeaderName)] = xsrfValue;਍      紀ഀഀ
਍ഀഀ
      var serverRequest = function(config) {਍        栀攀愀搀攀爀猀 㴀 挀漀渀昀椀最⸀栀攀愀搀攀爀猀㬀ഀഀ
        var reqData = transformData(config.data, headersGetter(headers), config.transformRequest);਍ഀഀ
        // strip content-type if data is undefined਍        椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀挀漀渀昀椀最⸀搀愀琀愀⤀⤀ 笀ഀഀ
          forEach(headers, function(value, header) {਍            椀昀 ⠀氀漀眀攀爀挀愀猀攀⠀栀攀愀搀攀爀⤀ 㴀㴀㴀 ✀挀漀渀琀攀渀琀ⴀ琀礀瀀攀✀⤀ 笀ഀഀ
                delete headers[header];਍            紀ഀഀ
          });਍        紀ഀഀ
਍        椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀挀漀渀昀椀最⸀眀椀琀栀䌀爀攀搀攀渀琀椀愀氀猀⤀ ☀☀ ℀椀猀唀渀搀攀昀椀渀攀搀⠀搀攀昀愀甀氀琀猀⸀眀椀琀栀䌀爀攀搀攀渀琀椀愀氀猀⤀⤀ 笀ഀഀ
          config.withCredentials = defaults.withCredentials;਍        紀ഀഀ
਍        ⼀⼀ 猀攀渀搀 爀攀焀甀攀猀琀ഀഀ
        return sendReq(config, reqData, headers).then(transformResponse, transformResponse);਍      紀㬀ഀഀ
਍      瘀愀爀 挀栀愀椀渀 㴀 嬀猀攀爀瘀攀爀刀攀焀甀攀猀琀Ⰰ 甀渀搀攀昀椀渀攀搀崀㬀ഀഀ
      var promise = $q.when(config);਍ഀഀ
      // apply interceptors਍      昀漀爀䔀愀挀栀⠀爀攀瘀攀爀猀攀搀䤀渀琀攀爀挀攀瀀琀漀爀猀Ⰰ 昀甀渀挀琀椀漀渀⠀椀渀琀攀爀挀攀瀀琀漀爀⤀ 笀ഀഀ
        if (interceptor.request || interceptor.requestError) {਍          挀栀愀椀渀⸀甀渀猀栀椀昀琀⠀椀渀琀攀爀挀攀瀀琀漀爀⸀爀攀焀甀攀猀琀Ⰰ 椀渀琀攀爀挀攀瀀琀漀爀⸀爀攀焀甀攀猀琀䔀爀爀漀爀⤀㬀ഀഀ
        }਍        椀昀 ⠀椀渀琀攀爀挀攀瀀琀漀爀⸀爀攀猀瀀漀渀猀攀 簀簀 椀渀琀攀爀挀攀瀀琀漀爀⸀爀攀猀瀀漀渀猀攀䔀爀爀漀爀⤀ 笀ഀഀ
          chain.push(interceptor.response, interceptor.responseError);਍        紀ഀഀ
      });਍ഀഀ
      while(chain.length) {਍        瘀愀爀 琀栀攀渀䘀渀 㴀 挀栀愀椀渀⸀猀栀椀昀琀⠀⤀㬀ഀഀ
        var rejectFn = chain.shift();਍ഀഀ
        promise = promise.then(thenFn, rejectFn);਍      紀ഀഀ
਍      瀀爀漀洀椀猀攀⸀猀甀挀挀攀猀猀 㴀 昀甀渀挀琀椀漀渀⠀昀渀⤀ 笀ഀഀ
        promise.then(function(response) {਍          昀渀⠀爀攀猀瀀漀渀猀攀⸀搀愀琀愀Ⰰ 爀攀猀瀀漀渀猀攀⸀猀琀愀琀甀猀Ⰰ 爀攀猀瀀漀渀猀攀⸀栀攀愀搀攀爀猀Ⰰ 挀漀渀昀椀最⤀㬀ഀഀ
        });਍        爀攀琀甀爀渀 瀀爀漀洀椀猀攀㬀ഀഀ
      };਍ഀഀ
      promise.error = function(fn) {਍        瀀爀漀洀椀猀攀⸀琀栀攀渀⠀渀甀氀氀Ⰰ 昀甀渀挀琀椀漀渀⠀爀攀猀瀀漀渀猀攀⤀ 笀ഀഀ
          fn(response.data, response.status, response.headers, config);਍        紀⤀㬀ഀഀ
        return promise;਍      紀㬀ഀഀ
਍      爀攀琀甀爀渀 瀀爀漀洀椀猀攀㬀ഀഀ
਍      昀甀渀挀琀椀漀渀 琀爀愀渀猀昀漀爀洀刀攀猀瀀漀渀猀攀⠀爀攀猀瀀漀渀猀攀⤀ 笀ഀഀ
        // make a copy since the response must be cacheable਍        瘀愀爀 爀攀猀瀀 㴀 攀砀琀攀渀搀⠀笀紀Ⰰ 爀攀猀瀀漀渀猀攀Ⰰ 笀ഀഀ
          data: transformData(response.data, response.headers, config.transformResponse)਍        紀⤀㬀ഀഀ
        return (isSuccess(response.status))਍          㼀 爀攀猀瀀ഀഀ
          : $q.reject(resp);਍      紀ഀഀ
਍      昀甀渀挀琀椀漀渀 洀攀爀最攀䠀攀愀搀攀爀猀⠀挀漀渀昀椀最⤀ 笀ഀഀ
        var defHeaders = defaults.headers,਍            爀攀焀䠀攀愀搀攀爀猀 㴀 攀砀琀攀渀搀⠀笀紀Ⰰ 挀漀渀昀椀最⸀栀攀愀搀攀爀猀⤀Ⰰഀഀ
            defHeaderName, lowercaseDefHeaderName, reqHeaderName;਍ഀഀ
        defHeaders = extend({}, defHeaders.common, defHeaders[lowercase(config.method)]);਍ഀഀ
        // execute if header value is function਍        攀砀攀挀䠀攀愀搀攀爀猀⠀搀攀昀䠀攀愀搀攀爀猀⤀㬀ഀഀ
        execHeaders(reqHeaders);਍ഀഀ
        // using for-in instead of forEach to avoid unecessary iteration after header has been found਍        搀攀昀愀甀氀琀䠀攀愀搀攀爀猀䤀琀攀爀愀琀椀漀渀㨀ഀഀ
        for (defHeaderName in defHeaders) {਍          氀漀眀攀爀挀愀猀攀䐀攀昀䠀攀愀搀攀爀一愀洀攀 㴀 氀漀眀攀爀挀愀猀攀⠀搀攀昀䠀攀愀搀攀爀一愀洀攀⤀㬀ഀഀ
਍          昀漀爀 ⠀爀攀焀䠀攀愀搀攀爀一愀洀攀 椀渀 爀攀焀䠀攀愀搀攀爀猀⤀ 笀ഀഀ
            if (lowercase(reqHeaderName) === lowercaseDefHeaderName) {਍              挀漀渀琀椀渀甀攀 搀攀昀愀甀氀琀䠀攀愀搀攀爀猀䤀琀攀爀愀琀椀漀渀㬀ഀഀ
            }਍          紀ഀഀ
਍          爀攀焀䠀攀愀搀攀爀猀嬀搀攀昀䠀攀愀搀攀爀一愀洀攀崀 㴀 搀攀昀䠀攀愀搀攀爀猀嬀搀攀昀䠀攀愀搀攀爀一愀洀攀崀㬀ഀഀ
        }਍ഀഀ
        return reqHeaders;਍ഀഀ
        function execHeaders(headers) {਍          瘀愀爀 栀攀愀搀攀爀䌀漀渀琀攀渀琀㬀ഀഀ
਍          昀漀爀䔀愀挀栀⠀栀攀愀搀攀爀猀Ⰰ 昀甀渀挀琀椀漀渀⠀栀攀愀搀攀爀䘀渀Ⰰ 栀攀愀搀攀爀⤀ 笀ഀഀ
            if (isFunction(headerFn)) {਍              栀攀愀搀攀爀䌀漀渀琀攀渀琀 㴀 栀攀愀搀攀爀䘀渀⠀⤀㬀ഀഀ
              if (headerContent != null) {਍                栀攀愀搀攀爀猀嬀栀攀愀搀攀爀崀 㴀 栀攀愀搀攀爀䌀漀渀琀攀渀琀㬀ഀഀ
              } else {਍                搀攀氀攀琀攀 栀攀愀搀攀爀猀嬀栀攀愀搀攀爀崀㬀ഀഀ
              }਍            紀ഀഀ
          });਍        紀ഀഀ
      }਍    紀ഀഀ
਍    ␀栀琀琀瀀⸀瀀攀渀搀椀渀最刀攀焀甀攀猀琀猀 㴀 嬀崀㬀ഀഀ
਍    ⼀⨀⨀ഀഀ
     * @ngdoc method਍     ⨀ 䀀渀愀洀攀 渀最⸀␀栀琀琀瀀⌀最攀琀ഀഀ
     * @methodOf ng.$http਍     ⨀ഀഀ
     * @description਍     ⨀ 匀栀漀爀琀挀甀琀 洀攀琀栀漀搀 琀漀 瀀攀爀昀漀爀洀 怀䜀䔀吀怀 爀攀焀甀攀猀琀⸀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 甀爀氀 刀攀氀愀琀椀瘀攀 漀爀 愀戀猀漀氀甀琀攀 唀刀䰀 猀瀀攀挀椀昀礀椀渀最 琀栀攀 搀攀猀琀椀渀愀琀椀漀渀 漀昀 琀栀攀 爀攀焀甀攀猀琀ഀഀ
     * @param {Object=} config Optional configuration object਍     ⨀ 䀀爀攀琀甀爀渀猀 笀䠀琀琀瀀倀爀漀洀椀猀攀紀 䘀甀琀甀爀攀 漀戀樀攀挀琀ഀഀ
     */਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$http#delete਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀栀琀琀瀀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Shortcut method to perform `DELETE` request.਍     ⨀ഀഀ
     * @param {string} url Relative or absolute URL specifying the destination of the request਍     ⨀ 䀀瀀愀爀愀洀 笀伀戀樀攀挀琀㴀紀 挀漀渀昀椀最 伀瀀琀椀漀渀愀氀 挀漀渀昀椀最甀爀愀琀椀漀渀 漀戀樀攀挀琀ഀഀ
     * @returns {HttpPromise} Future object਍     ⨀⼀ഀഀ
਍    ⼀⨀⨀ഀഀ
     * @ngdoc method਍     ⨀ 䀀渀愀洀攀 渀最⸀␀栀琀琀瀀⌀栀攀愀搀ഀഀ
     * @methodOf ng.$http਍     ⨀ഀഀ
     * @description਍     ⨀ 匀栀漀爀琀挀甀琀 洀攀琀栀漀搀 琀漀 瀀攀爀昀漀爀洀 怀䠀䔀䄀䐀怀 爀攀焀甀攀猀琀⸀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 甀爀氀 刀攀氀愀琀椀瘀攀 漀爀 愀戀猀漀氀甀琀攀 唀刀䰀 猀瀀攀挀椀昀礀椀渀最 琀栀攀 搀攀猀琀椀渀愀琀椀漀渀 漀昀 琀栀攀 爀攀焀甀攀猀琀ഀഀ
     * @param {Object=} config Optional configuration object਍     ⨀ 䀀爀攀琀甀爀渀猀 笀䠀琀琀瀀倀爀漀洀椀猀攀紀 䘀甀琀甀爀攀 漀戀樀攀挀琀ഀഀ
     */਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$http#jsonp਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀栀琀琀瀀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Shortcut method to perform `JSONP` request.਍     ⨀ഀഀ
     * @param {string} url Relative or absolute URL specifying the destination of the request.਍     ⨀                     匀栀漀甀氀搀 挀漀渀琀愀椀渀 怀䨀匀伀一开䌀䄀䰀䰀䈀䄀䌀䬀怀 猀琀爀椀渀最⸀ഀഀ
     * @param {Object=} config Optional configuration object਍     ⨀ 䀀爀攀琀甀爀渀猀 笀䠀琀琀瀀倀爀漀洀椀猀攀紀 䘀甀琀甀爀攀 漀戀樀攀挀琀ഀഀ
     */਍    挀爀攀愀琀攀匀栀漀爀琀䴀攀琀栀漀搀猀⠀✀最攀琀✀Ⰰ ✀搀攀氀攀琀攀✀Ⰰ ✀栀攀愀搀✀Ⰰ ✀樀猀漀渀瀀✀⤀㬀ഀഀ
਍    ⼀⨀⨀ഀഀ
     * @ngdoc method਍     ⨀ 䀀渀愀洀攀 渀最⸀␀栀琀琀瀀⌀瀀漀猀琀ഀഀ
     * @methodOf ng.$http਍     ⨀ഀഀ
     * @description਍     ⨀ 匀栀漀爀琀挀甀琀 洀攀琀栀漀搀 琀漀 瀀攀爀昀漀爀洀 怀倀伀匀吀怀 爀攀焀甀攀猀琀⸀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 甀爀氀 刀攀氀愀琀椀瘀攀 漀爀 愀戀猀漀氀甀琀攀 唀刀䰀 猀瀀攀挀椀昀礀椀渀最 琀栀攀 搀攀猀琀椀渀愀琀椀漀渀 漀昀 琀栀攀 爀攀焀甀攀猀琀ഀഀ
     * @param {*} data Request content਍     ⨀ 䀀瀀愀爀愀洀 笀伀戀樀攀挀琀㴀紀 挀漀渀昀椀最 伀瀀琀椀漀渀愀氀 挀漀渀昀椀最甀爀愀琀椀漀渀 漀戀樀攀挀琀ഀഀ
     * @returns {HttpPromise} Future object਍     ⨀⼀ഀഀ
਍    ⼀⨀⨀ഀഀ
     * @ngdoc method਍     ⨀ 䀀渀愀洀攀 渀最⸀␀栀琀琀瀀⌀瀀甀琀ഀഀ
     * @methodOf ng.$http਍     ⨀ഀഀ
     * @description਍     ⨀ 匀栀漀爀琀挀甀琀 洀攀琀栀漀搀 琀漀 瀀攀爀昀漀爀洀 怀倀唀吀怀 爀攀焀甀攀猀琀⸀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 甀爀氀 刀攀氀愀琀椀瘀攀 漀爀 愀戀猀漀氀甀琀攀 唀刀䰀 猀瀀攀挀椀昀礀椀渀最 琀栀攀 搀攀猀琀椀渀愀琀椀漀渀 漀昀 琀栀攀 爀攀焀甀攀猀琀ഀഀ
     * @param {*} data Request content਍     ⨀ 䀀瀀愀爀愀洀 笀伀戀樀攀挀琀㴀紀 挀漀渀昀椀最 伀瀀琀椀漀渀愀氀 挀漀渀昀椀最甀爀愀琀椀漀渀 漀戀樀攀挀琀ഀഀ
     * @returns {HttpPromise} Future object਍     ⨀⼀ഀഀ
    createShortMethodsWithData('post', 'put');਍ഀഀ
        /**਍         ⨀ 䀀渀最搀漀挀 瀀爀漀瀀攀爀琀礀ഀഀ
         * @name ng.$http#defaults਍         ⨀ 䀀瀀爀漀瀀攀爀琀礀伀昀 渀最⸀␀栀琀琀瀀ഀഀ
         *਍         ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
         * Runtime equivalent of the `$httpProvider.defaults` property. Allows configuration of਍         ⨀ 搀攀昀愀甀氀琀 栀攀愀搀攀爀猀Ⰰ 眀椀琀栀䌀爀攀搀攀渀琀椀愀氀猀 愀猀 眀攀氀氀 愀猀 爀攀焀甀攀猀琀 愀渀搀 爀攀猀瀀漀渀猀攀 琀爀愀渀猀昀漀爀洀愀琀椀漀渀猀⸀ഀഀ
         *਍         ⨀ 匀攀攀 ∀匀攀琀琀椀渀最 䠀吀吀倀 䠀攀愀搀攀爀猀∀ 愀渀搀 ∀吀爀愀渀猀昀漀爀洀椀渀最 刀攀焀甀攀猀琀猀 愀渀搀 刀攀猀瀀漀渀猀攀猀∀ 猀攀挀琀椀漀渀猀 愀戀漀瘀攀⸀ഀഀ
         */਍    ␀栀琀琀瀀⸀搀攀昀愀甀氀琀猀 㴀 搀攀昀愀甀氀琀猀㬀ഀഀ
਍ഀഀ
    return $http;਍ഀഀ
਍    昀甀渀挀琀椀漀渀 挀爀攀愀琀攀匀栀漀爀琀䴀攀琀栀漀搀猀⠀渀愀洀攀猀⤀ 笀ഀഀ
      forEach(arguments, function(name) {਍        ␀栀琀琀瀀嬀渀愀洀攀崀 㴀 昀甀渀挀琀椀漀渀⠀甀爀氀Ⰰ 挀漀渀昀椀最⤀ 笀ഀഀ
          return $http(extend(config || {}, {਍            洀攀琀栀漀搀㨀 渀愀洀攀Ⰰഀഀ
            url: url਍          紀⤀⤀㬀ഀഀ
        };਍      紀⤀㬀ഀഀ
    }਍ഀഀ
਍    昀甀渀挀琀椀漀渀 挀爀攀愀琀攀匀栀漀爀琀䴀攀琀栀漀搀猀圀椀琀栀䐀愀琀愀⠀渀愀洀攀⤀ 笀ഀഀ
      forEach(arguments, function(name) {਍        ␀栀琀琀瀀嬀渀愀洀攀崀 㴀 昀甀渀挀琀椀漀渀⠀甀爀氀Ⰰ 搀愀琀愀Ⰰ 挀漀渀昀椀最⤀ 笀ഀഀ
          return $http(extend(config || {}, {਍            洀攀琀栀漀搀㨀 渀愀洀攀Ⰰഀഀ
            url: url,਍            搀愀琀愀㨀 搀愀琀愀ഀഀ
          }));਍        紀㬀ഀഀ
      });਍    紀ഀഀ
਍ഀഀ
    /**਍     ⨀ 䴀愀欀攀猀 琀栀攀 爀攀焀甀攀猀琀⸀ഀഀ
     *਍     ⨀ ℀℀℀ 䄀䌀䌀䔀匀匀䔀匀 䌀䰀伀匀唀刀䔀 嘀䄀刀匀㨀ഀഀ
     * $httpBackend, defaults, $log, $rootScope, defaultCache, $http.pendingRequests਍     ⨀⼀ഀഀ
    function sendReq(config, reqData, reqHeaders) {਍      瘀愀爀 搀攀昀攀爀爀攀搀 㴀 ␀焀⸀搀攀昀攀爀⠀⤀Ⰰഀഀ
          promise = deferred.promise,਍          挀愀挀栀攀Ⰰഀഀ
          cachedResp,਍          甀爀氀 㴀 戀甀椀氀搀唀爀氀⠀挀漀渀昀椀最⸀甀爀氀Ⰰ 挀漀渀昀椀最⸀瀀愀爀愀洀猀⤀㬀ഀഀ
਍      ␀栀琀琀瀀⸀瀀攀渀搀椀渀最刀攀焀甀攀猀琀猀⸀瀀甀猀栀⠀挀漀渀昀椀最⤀㬀ഀഀ
      promise.then(removePendingReq, removePendingReq);਍ഀഀ
਍      椀昀 ⠀⠀挀漀渀昀椀最⸀挀愀挀栀攀 簀簀 搀攀昀愀甀氀琀猀⸀挀愀挀栀攀⤀ ☀☀ 挀漀渀昀椀最⸀挀愀挀栀攀 ℀㴀㴀 昀愀氀猀攀 ☀☀ 挀漀渀昀椀最⸀洀攀琀栀漀搀 㴀㴀 ✀䜀䔀吀✀⤀ 笀ഀഀ
        cache = isObject(config.cache) ? config.cache਍              㨀 椀猀伀戀樀攀挀琀⠀搀攀昀愀甀氀琀猀⸀挀愀挀栀攀⤀ 㼀 搀攀昀愀甀氀琀猀⸀挀愀挀栀攀ഀഀ
              : defaultCache;਍      紀ഀഀ
਍      椀昀 ⠀挀愀挀栀攀⤀ 笀ഀഀ
        cachedResp = cache.get(url);਍        椀昀 ⠀椀猀䐀攀昀椀渀攀搀⠀挀愀挀栀攀搀刀攀猀瀀⤀⤀ 笀ഀഀ
          if (cachedResp.then) {਍            ⼀⼀ 挀愀挀栀攀搀 爀攀焀甀攀猀琀 栀愀猀 愀氀爀攀愀搀礀 戀攀攀渀 猀攀渀琀Ⰰ 戀甀琀 琀栀攀爀攀 椀猀 渀漀 爀攀猀瀀漀渀猀攀 礀攀琀ഀഀ
            cachedResp.then(removePendingReq, removePendingReq);਍            爀攀琀甀爀渀 挀愀挀栀攀搀刀攀猀瀀㬀ഀഀ
          } else {਍            ⼀⼀ 猀攀爀瘀椀渀最 昀爀漀洀 挀愀挀栀攀ഀഀ
            if (isArray(cachedResp)) {਍              爀攀猀漀氀瘀攀倀爀漀洀椀猀攀⠀挀愀挀栀攀搀刀攀猀瀀嬀㄀崀Ⰰ 挀愀挀栀攀搀刀攀猀瀀嬀　崀Ⰰ 挀漀瀀礀⠀挀愀挀栀攀搀刀攀猀瀀嬀㈀崀⤀⤀㬀ഀഀ
            } else {਍              爀攀猀漀氀瘀攀倀爀漀洀椀猀攀⠀挀愀挀栀攀搀刀攀猀瀀Ⰰ ㈀　　Ⰰ 笀紀⤀㬀ഀഀ
            }਍          紀ഀഀ
        } else {਍          ⼀⼀ 瀀甀琀 琀栀攀 瀀爀漀洀椀猀攀 昀漀爀 琀栀攀 渀漀渀ⴀ琀爀愀渀猀昀漀爀洀攀搀 爀攀猀瀀漀渀猀攀 椀渀琀漀 挀愀挀栀攀 愀猀 愀 瀀氀愀挀攀栀漀氀搀攀爀ഀഀ
          cache.put(url, promise);਍        紀ഀഀ
      }਍ഀഀ
      // if we won't have the response in cache, send the request to the backend਍      椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀挀愀挀栀攀搀刀攀猀瀀⤀⤀ 笀ഀഀ
        $httpBackend(config.method, url, reqData, done, reqHeaders, config.timeout,਍            挀漀渀昀椀最⸀眀椀琀栀䌀爀攀搀攀渀琀椀愀氀猀Ⰰ 挀漀渀昀椀最⸀爀攀猀瀀漀渀猀攀吀礀瀀攀⤀㬀ഀഀ
      }਍ഀഀ
      return promise;਍ഀഀ
਍      ⼀⨀⨀ഀഀ
       * Callback registered to $httpBackend():਍       ⨀  ⴀ 挀愀挀栀攀猀 琀栀攀 爀攀猀瀀漀渀猀攀 椀昀 搀攀猀椀爀攀搀ഀഀ
       *  - resolves the raw $http promise਍       ⨀  ⴀ 挀愀氀氀猀 ␀愀瀀瀀氀礀ഀഀ
       */਍      昀甀渀挀琀椀漀渀 搀漀渀攀⠀猀琀愀琀甀猀Ⰰ 爀攀猀瀀漀渀猀攀Ⰰ 栀攀愀搀攀爀猀匀琀爀椀渀最⤀ 笀ഀഀ
        if (cache) {਍          椀昀 ⠀椀猀匀甀挀挀攀猀猀⠀猀琀愀琀甀猀⤀⤀ 笀ഀഀ
            cache.put(url, [status, response, parseHeaders(headersString)]);਍          紀 攀氀猀攀 笀ഀഀ
            // remove promise from the cache਍            挀愀挀栀攀⸀爀攀洀漀瘀攀⠀甀爀氀⤀㬀ഀഀ
          }਍        紀ഀഀ
਍        爀攀猀漀氀瘀攀倀爀漀洀椀猀攀⠀爀攀猀瀀漀渀猀攀Ⰰ 猀琀愀琀甀猀Ⰰ 栀攀愀搀攀爀猀匀琀爀椀渀最⤀㬀ഀഀ
        if (!$rootScope.$$phase) $rootScope.$apply();਍      紀ഀഀ
਍ഀഀ
      /**਍       ⨀ 刀攀猀漀氀瘀攀猀 琀栀攀 爀愀眀 ␀栀琀琀瀀 瀀爀漀洀椀猀攀⸀ഀഀ
       */਍      昀甀渀挀琀椀漀渀 爀攀猀漀氀瘀攀倀爀漀洀椀猀攀⠀爀攀猀瀀漀渀猀攀Ⰰ 猀琀愀琀甀猀Ⰰ 栀攀愀搀攀爀猀⤀ 笀ഀഀ
        // normalize internal statuses to 0਍        猀琀愀琀甀猀 㴀 䴀愀琀栀⸀洀愀砀⠀猀琀愀琀甀猀Ⰰ 　⤀㬀ഀഀ
਍        ⠀椀猀匀甀挀挀攀猀猀⠀猀琀愀琀甀猀⤀ 㼀 搀攀昀攀爀爀攀搀⸀爀攀猀漀氀瘀攀 㨀 搀攀昀攀爀爀攀搀⸀爀攀樀攀挀琀⤀⠀笀ഀഀ
          data: response,਍          猀琀愀琀甀猀㨀 猀琀愀琀甀猀Ⰰഀഀ
          headers: headersGetter(headers),਍          挀漀渀昀椀最㨀 挀漀渀昀椀最ഀഀ
        });਍      紀ഀഀ
਍ഀഀ
      function removePendingReq() {਍        瘀愀爀 椀搀砀 㴀 椀渀搀攀砀伀昀⠀␀栀琀琀瀀⸀瀀攀渀搀椀渀最刀攀焀甀攀猀琀猀Ⰰ 挀漀渀昀椀最⤀㬀ഀഀ
        if (idx !== -1) $http.pendingRequests.splice(idx, 1);਍      紀ഀഀ
    }਍ഀഀ
਍    昀甀渀挀琀椀漀渀 戀甀椀氀搀唀爀氀⠀甀爀氀Ⰰ 瀀愀爀愀洀猀⤀ 笀ഀഀ
          if (!params) return url;਍          瘀愀爀 瀀愀爀琀猀 㴀 嬀崀㬀ഀഀ
          forEachSorted(params, function(value, key) {਍            椀昀 ⠀瘀愀氀甀攀 㴀㴀㴀 渀甀氀氀 簀簀 椀猀唀渀搀攀昀椀渀攀搀⠀瘀愀氀甀攀⤀⤀ 爀攀琀甀爀渀㬀ഀഀ
            if (!isArray(value)) value = [value];਍ഀഀ
            forEach(value, function(v) {਍              椀昀 ⠀椀猀伀戀樀攀挀琀⠀瘀⤀⤀ 笀ഀഀ
                v = toJson(v);਍              紀ഀഀ
              parts.push(encodeUriQuery(key) + '=' +਍                         攀渀挀漀搀攀唀爀椀儀甀攀爀礀⠀瘀⤀⤀㬀ഀഀ
            });਍          紀⤀㬀ഀഀ
          return url + ((url.indexOf('?') == -1) ? '?' : '&') + parts.join('&');਍        紀ഀഀ
਍ഀഀ
  }];਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 挀爀攀愀琀攀堀栀爀⠀洀攀琀栀漀搀⤀ 笀ഀഀ
  // IE8 doesn't support PATCH method, but the ActiveX object does਍  ⼀⨀ 最氀漀戀愀氀 䄀挀琀椀瘀攀堀伀戀樀攀挀琀 ⨀⼀ഀഀ
  return (msie <= 8 && lowercase(method) === 'patch')਍      㼀 渀攀眀 䄀挀琀椀瘀攀堀伀戀樀攀挀琀⠀✀䴀椀挀爀漀猀漀昀琀⸀堀䴀䰀䠀吀吀倀✀⤀ഀഀ
      : new window.XMLHttpRequest();਍紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name ng.$httpBackend਍ ⨀ 䀀爀攀焀甀椀爀攀猀 ␀戀爀漀眀猀攀爀ഀഀ
 * @requires $window਍ ⨀ 䀀爀攀焀甀椀爀攀猀 ␀搀漀挀甀洀攀渀琀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * HTTP backend used by the {@link ng.$http service} that delegates to਍ ⨀ 堀䴀䰀䠀琀琀瀀刀攀焀甀攀猀琀 漀戀樀攀挀琀 漀爀 䨀匀伀一倀 愀渀搀 搀攀愀氀猀 眀椀琀栀 戀爀漀眀猀攀爀 椀渀挀漀洀瀀愀琀椀戀椀氀椀琀椀攀猀⸀ഀഀ
 *਍ ⨀ 夀漀甀 猀栀漀甀氀搀 渀攀瘀攀爀 渀攀攀搀 琀漀 甀猀攀 琀栀椀猀 猀攀爀瘀椀挀攀 搀椀爀攀挀琀氀礀Ⰰ 椀渀猀琀攀愀搀 甀猀攀 琀栀攀 栀椀最栀攀爀ⴀ氀攀瘀攀氀 愀戀猀琀爀愀挀琀椀漀渀猀㨀ഀഀ
 * {@link ng.$http $http} or {@link ngResource.$resource $resource}.਍ ⨀ഀഀ
 * During testing this implementation is swapped with {@link ngMock.$httpBackend mock਍ ⨀ ␀栀琀琀瀀䈀愀挀欀攀渀搀紀 眀栀椀挀栀 挀愀渀 戀攀 琀爀愀椀渀攀搀 眀椀琀栀 爀攀猀瀀漀渀猀攀猀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 ␀䠀琀琀瀀䈀愀挀欀攀渀搀倀爀漀瘀椀搀攀爀⠀⤀ 笀ഀഀ
  this.$get = ['$browser', '$window', '$document', function($browser, $window, $document) {਍    爀攀琀甀爀渀 挀爀攀愀琀攀䠀琀琀瀀䈀愀挀欀攀渀搀⠀␀戀爀漀眀猀攀爀Ⰰ 挀爀攀愀琀攀堀栀爀Ⰰ ␀戀爀漀眀猀攀爀⸀搀攀昀攀爀Ⰰ ␀眀椀渀搀漀眀⸀愀渀最甀氀愀爀⸀挀愀氀氀戀愀挀欀猀Ⰰ ␀搀漀挀甀洀攀渀琀嬀　崀⤀㬀ഀഀ
  }];਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 挀爀攀愀琀攀䠀琀琀瀀䈀愀挀欀攀渀搀⠀␀戀爀漀眀猀攀爀Ⰰ 挀爀攀愀琀攀堀栀爀Ⰰ ␀戀爀漀眀猀攀爀䐀攀昀攀爀Ⰰ 挀愀氀氀戀愀挀欀猀Ⰰ 爀愀眀䐀漀挀甀洀攀渀琀⤀ 笀ഀഀ
  var ABORTED = -1;਍ഀഀ
  // TODO(vojta): fix the signature਍  爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀洀攀琀栀漀搀Ⰰ 甀爀氀Ⰰ 瀀漀猀琀Ⰰ 挀愀氀氀戀愀挀欀Ⰰ 栀攀愀搀攀爀猀Ⰰ 琀椀洀攀漀甀琀Ⰰ 眀椀琀栀䌀爀攀搀攀渀琀椀愀氀猀Ⰰ 爀攀猀瀀漀渀猀攀吀礀瀀攀⤀ 笀ഀഀ
    var status;਍    ␀戀爀漀眀猀攀爀⸀␀␀椀渀挀伀甀琀猀琀愀渀搀椀渀最刀攀焀甀攀猀琀䌀漀甀渀琀⠀⤀㬀ഀഀ
    url = url || $browser.url();਍ഀഀ
    if (lowercase(method) == 'jsonp') {਍      瘀愀爀 挀愀氀氀戀愀挀欀䤀搀 㴀 ✀开✀ ⬀ ⠀挀愀氀氀戀愀挀欀猀⸀挀漀甀渀琀攀爀⬀⬀⤀⸀琀漀匀琀爀椀渀最⠀㌀㘀⤀㬀ഀഀ
      callbacks[callbackId] = function(data) {਍        挀愀氀氀戀愀挀欀猀嬀挀愀氀氀戀愀挀欀䤀搀崀⸀搀愀琀愀 㴀 搀愀琀愀㬀ഀഀ
      };਍ഀഀ
      var jsonpDone = jsonpReq(url.replace('JSON_CALLBACK', 'angular.callbacks.' + callbackId),਍          昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
        if (callbacks[callbackId].data) {਍          挀漀洀瀀氀攀琀攀刀攀焀甀攀猀琀⠀挀愀氀氀戀愀挀欀Ⰰ ㈀　　Ⰰ 挀愀氀氀戀愀挀欀猀嬀挀愀氀氀戀愀挀欀䤀搀崀⸀搀愀琀愀⤀㬀ഀഀ
        } else {਍          挀漀洀瀀氀攀琀攀刀攀焀甀攀猀琀⠀挀愀氀氀戀愀挀欀Ⰰ 猀琀愀琀甀猀 簀簀 ⴀ㈀⤀㬀ഀഀ
        }਍        挀愀氀氀戀愀挀欀猀嬀挀愀氀氀戀愀挀欀䤀搀崀 㴀 愀渀最甀氀愀爀⸀渀漀漀瀀㬀ഀഀ
      });਍    紀 攀氀猀攀 笀ഀഀ
਍      瘀愀爀 砀栀爀 㴀 挀爀攀愀琀攀堀栀爀⠀洀攀琀栀漀搀⤀㬀ഀഀ
਍      砀栀爀⸀漀瀀攀渀⠀洀攀琀栀漀搀Ⰰ 甀爀氀Ⰰ 琀爀甀攀⤀㬀ഀഀ
      forEach(headers, function(value, key) {਍        椀昀 ⠀椀猀䐀攀昀椀渀攀搀⠀瘀愀氀甀攀⤀⤀ 笀ഀഀ
            xhr.setRequestHeader(key, value);਍        紀ഀഀ
      });਍ഀഀ
      // In IE6 and 7, this might be called synchronously when xhr.send below is called and the਍      ⼀⼀ 爀攀猀瀀漀渀猀攀 椀猀 椀渀 琀栀攀 挀愀挀栀攀⸀ 琀栀攀 瀀爀漀洀椀猀攀 愀瀀椀 眀椀氀氀 攀渀猀甀爀攀 琀栀愀琀 琀漀 琀栀攀 愀瀀瀀 挀漀搀攀 琀栀攀 愀瀀椀 椀猀ഀഀ
      // always async਍      砀栀爀⸀漀渀爀攀愀搀礀猀琀愀琀攀挀栀愀渀最攀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
        // onreadystatechange might get called multiple times with readyState === 4 on mobile webkit caused by਍        ⼀⼀ 砀栀爀猀 琀栀愀琀 愀爀攀 爀攀猀漀氀瘀攀搀 眀栀椀氀攀 琀栀攀 愀瀀瀀 椀猀 椀渀 琀栀攀 戀愀挀欀最爀漀甀渀搀 ⠀猀攀攀 ⌀㔀㐀㈀㘀⤀⸀ഀഀ
        // since calling completeRequest sets the `xhr` variable to null, we just check if it's not null before਍        ⼀⼀ 挀漀渀琀椀渀甀椀渀最ഀഀ
        //਍        ⼀⼀ 眀攀 挀愀渀✀琀 猀攀琀 砀栀爀⸀漀渀爀攀愀搀礀猀琀愀琀攀挀栀愀渀最攀 琀漀 甀渀搀攀昀椀渀攀搀 漀爀 搀攀氀攀琀攀 椀琀 戀攀挀愀甀猀攀 琀栀愀琀 戀爀攀愀欀猀 䤀䔀㠀 ⠀洀攀琀栀漀搀㴀倀䄀吀䌀䠀⤀ 愀渀搀ഀഀ
        // Safari respectively.਍        椀昀 ⠀砀栀爀 ☀☀ 砀栀爀⸀爀攀愀搀礀匀琀愀琀攀 㴀㴀 㐀⤀ 笀ഀഀ
          var responseHeaders = null,਍              爀攀猀瀀漀渀猀攀 㴀 渀甀氀氀㬀ഀഀ
਍          椀昀⠀猀琀愀琀甀猀 ℀㴀㴀 䄀䈀伀刀吀䔀䐀⤀ 笀ഀഀ
            responseHeaders = xhr.getAllResponseHeaders();਍ഀഀ
            // responseText is the old-school way of retrieving response (supported by IE8 & 9)਍            ⼀⼀ 爀攀猀瀀漀渀猀攀⼀爀攀猀瀀漀渀猀攀吀礀瀀攀 瀀爀漀瀀攀爀琀椀攀猀 眀攀爀攀 椀渀琀爀漀搀甀挀攀搀 椀渀 堀䠀刀 䰀攀瘀攀氀㈀ 猀瀀攀挀 ⠀猀甀瀀瀀漀爀琀攀搀 戀礀 䤀䔀㄀　⤀ഀഀ
            response = ('response' in xhr) ? xhr.response : xhr.responseText;਍          紀ഀഀ
਍          挀漀洀瀀氀攀琀攀刀攀焀甀攀猀琀⠀挀愀氀氀戀愀挀欀Ⰰഀഀ
              status || xhr.status,਍              爀攀猀瀀漀渀猀攀Ⰰഀഀ
              responseHeaders);਍        紀ഀഀ
      };਍ഀഀ
      if (withCredentials) {਍        砀栀爀⸀眀椀琀栀䌀爀攀搀攀渀琀椀愀氀猀 㴀 琀爀甀攀㬀ഀഀ
      }਍ഀഀ
      if (responseType) {਍        砀栀爀⸀爀攀猀瀀漀渀猀攀吀礀瀀攀 㴀 爀攀猀瀀漀渀猀攀吀礀瀀攀㬀ഀഀ
      }਍ഀഀ
      xhr.send(post || null);਍    紀ഀഀ
਍    椀昀 ⠀琀椀洀攀漀甀琀 㸀 　⤀ 笀ഀഀ
      var timeoutId = $browserDefer(timeoutRequest, timeout);਍    紀 攀氀猀攀 椀昀 ⠀琀椀洀攀漀甀琀 ☀☀ 琀椀洀攀漀甀琀⸀琀栀攀渀⤀ 笀ഀഀ
      timeout.then(timeoutRequest);਍    紀ഀഀ
਍ഀഀ
    function timeoutRequest() {਍      猀琀愀琀甀猀 㴀 䄀䈀伀刀吀䔀䐀㬀ഀഀ
      jsonpDone && jsonpDone();਍      砀栀爀 ☀☀ 砀栀爀⸀愀戀漀爀琀⠀⤀㬀ഀഀ
    }਍ഀഀ
    function completeRequest(callback, status, response, headersString) {਍      ⼀⼀ 挀愀渀挀攀氀 琀椀洀攀漀甀琀 愀渀搀 猀甀戀猀攀焀甀攀渀琀 琀椀洀攀漀甀琀 瀀爀漀洀椀猀攀 爀攀猀漀氀甀琀椀漀渀ഀഀ
      timeoutId && $browserDefer.cancel(timeoutId);਍      樀猀漀渀瀀䐀漀渀攀 㴀 砀栀爀 㴀 渀甀氀氀㬀ഀഀ
਍      ⼀⼀ 昀椀砀 猀琀愀琀甀猀 挀漀搀攀 眀栀攀渀 椀琀 椀猀 　 ⠀　 猀琀愀琀甀猀 椀猀 甀渀搀漀挀甀洀攀渀琀攀搀⤀⸀ഀഀ
      // Occurs when accessing file resources.਍      ⼀⼀ 伀渀 䄀渀搀爀漀椀搀 㐀⸀㄀ 猀琀漀挀欀 戀爀漀眀猀攀爀 椀琀 漀挀挀甀爀猀 眀栀椀氀攀 爀攀琀爀椀攀瘀椀渀最 昀椀氀攀猀 昀爀漀洀 愀瀀瀀氀椀挀愀琀椀漀渀 挀愀挀栀攀⸀ഀഀ
      status = (status === 0) ? (response ? 200 : 404) : status;਍ഀഀ
      // normalize IE bug (http://bugs.jquery.com/ticket/1450)਍      猀琀愀琀甀猀 㴀 猀琀愀琀甀猀 㴀㴀 ㄀㈀㈀㌀ 㼀 ㈀　㐀 㨀 猀琀愀琀甀猀㬀ഀഀ
਍      挀愀氀氀戀愀挀欀⠀猀琀愀琀甀猀Ⰰ 爀攀猀瀀漀渀猀攀Ⰰ 栀攀愀搀攀爀猀匀琀爀椀渀最⤀㬀ഀഀ
      $browser.$$completeOutstandingRequest(noop);਍    紀ഀഀ
  };਍ഀഀ
  function jsonpReq(url, done) {਍    ⼀⼀ 眀攀 挀愀渀✀琀 甀猀攀 樀儀甀攀爀礀⼀樀焀䰀椀琀攀 栀攀爀攀 戀攀挀愀甀猀攀 樀儀甀攀爀礀 搀漀攀猀 挀爀愀稀礀 猀栀椀琀 眀椀琀栀 猀挀爀椀瀀琀 攀氀攀洀攀渀琀猀Ⰰ 攀⸀最⸀㨀ഀഀ
    // - fetches local scripts via XHR and evals them਍    ⼀⼀ ⴀ 愀搀搀猀 愀渀搀 椀洀洀攀搀椀愀琀攀氀礀 爀攀洀漀瘀攀猀 猀挀爀椀瀀琀 攀氀攀洀攀渀琀猀 昀爀漀洀 琀栀攀 搀漀挀甀洀攀渀琀ഀഀ
    var script = rawDocument.createElement('script'),਍        搀漀渀攀圀爀愀瀀瀀攀爀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          script.onreadystatechange = script.onload = script.onerror = null;਍          爀愀眀䐀漀挀甀洀攀渀琀⸀戀漀搀礀⸀爀攀洀漀瘀攀䌀栀椀氀搀⠀猀挀爀椀瀀琀⤀㬀ഀഀ
          if (done) done();਍        紀㬀ഀഀ
਍    猀挀爀椀瀀琀⸀琀礀瀀攀 㴀 ✀琀攀砀琀⼀樀愀瘀愀猀挀爀椀瀀琀✀㬀ഀഀ
    script.src = url;਍ഀഀ
    if (msie && msie <= 8) {਍      猀挀爀椀瀀琀⸀漀渀爀攀愀搀礀猀琀愀琀攀挀栀愀渀最攀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
        if (/loaded|complete/.test(script.readyState)) {਍          搀漀渀攀圀爀愀瀀瀀攀爀⠀⤀㬀ഀഀ
        }਍      紀㬀ഀഀ
    } else {਍      猀挀爀椀瀀琀⸀漀渀氀漀愀搀 㴀 猀挀爀椀瀀琀⸀漀渀攀爀爀漀爀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
        doneWrapper();਍      紀㬀ഀഀ
    }਍ഀഀ
    rawDocument.body.appendChild(script);਍    爀攀琀甀爀渀 搀漀渀攀圀爀愀瀀瀀攀爀㬀ഀഀ
  }਍紀ഀഀ
਍瘀愀爀 ␀椀渀琀攀爀瀀漀氀愀琀攀䴀椀渀䔀爀爀 㴀 洀椀渀䔀爀爀⠀✀␀椀渀琀攀爀瀀漀氀愀琀攀✀⤀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc object਍ ⨀ 䀀渀愀洀攀 渀最⸀␀椀渀琀攀爀瀀漀氀愀琀攀倀爀漀瘀椀搀攀爀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ഀഀ
 * Used for configuring the interpolation markup. Defaults to `{{` and `}}`.਍ ⨀ഀഀ
 * @example਍㰀搀漀挀㨀攀砀愀洀瀀氀攀 洀漀搀甀氀攀㴀∀挀甀猀琀漀洀䤀渀琀攀爀瀀漀氀愀琀椀漀渀䄀瀀瀀∀㸀ഀഀ
<doc:source>਍㰀猀挀爀椀瀀琀㸀ഀഀ
  var customInterpolationApp = angular.module('customInterpolationApp', []);਍ഀഀ
  customInterpolationApp.config(function($interpolateProvider) {਍    ␀椀渀琀攀爀瀀漀氀愀琀攀倀爀漀瘀椀搀攀爀⸀猀琀愀爀琀匀礀洀戀漀氀⠀✀⼀⼀✀⤀㬀ഀഀ
    $interpolateProvider.endSymbol('//');਍  紀⤀㬀ഀഀ
਍ഀഀ
  customInterpolationApp.controller('DemoController', function DemoController() {਍      琀栀椀猀⸀氀愀戀攀氀 㴀 ∀吀栀椀猀 戀椀渀搀椀渀最 椀猀 戀爀漀甀最栀琀 礀漀甀 戀礀 ⼀⼀ 椀渀琀攀爀瀀漀氀愀琀椀漀渀 猀礀洀戀漀氀猀⸀∀㬀ഀഀ
  });਍㰀⼀猀挀爀椀瀀琀㸀ഀഀ
<div ng-app="App" ng-controller="DemoController as demo">਍    ⼀⼀搀攀洀漀⸀氀愀戀攀氀⼀⼀ഀഀ
</div>਍㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
<doc:scenario>਍ 椀琀⠀✀猀栀漀甀氀搀 椀渀琀攀爀瀀漀氀愀琀攀 戀椀渀搀椀渀最 眀椀琀栀 挀甀猀琀漀洀 猀礀洀戀漀氀猀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
  expect(binding('demo.label')).toBe('This binding is brought you by // interpolation symbols.');਍ 紀⤀㬀ഀഀ
</doc:scenario>਍㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 ␀䤀渀琀攀爀瀀漀氀愀琀攀倀爀漀瘀椀搀攀爀⠀⤀ 笀ഀഀ
  var startSymbol = '{{';਍  瘀愀爀 攀渀搀匀礀洀戀漀氀 㴀 ✀紀紀✀㬀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc method਍   ⨀ 䀀渀愀洀攀 渀最⸀␀椀渀琀攀爀瀀漀氀愀琀攀倀爀漀瘀椀搀攀爀⌀猀琀愀爀琀匀礀洀戀漀氀ഀഀ
   * @methodOf ng.$interpolateProvider਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Symbol to denote start of expression in the interpolated string. Defaults to `{{`.਍   ⨀ഀഀ
   * @param {string=} value new value to set the starting symbol to.਍   ⨀ 䀀爀攀琀甀爀渀猀 笀猀琀爀椀渀最簀猀攀氀昀紀 刀攀琀甀爀渀猀 琀栀攀 猀礀洀戀漀氀 眀栀攀渀 甀猀攀搀 愀猀 最攀琀琀攀爀 愀渀搀 猀攀氀昀 椀昀 甀猀攀搀 愀猀 猀攀琀琀攀爀⸀ഀഀ
   */਍  琀栀椀猀⸀猀琀愀爀琀匀礀洀戀漀氀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀笀ഀഀ
    if (value) {਍      猀琀愀爀琀匀礀洀戀漀氀 㴀 瘀愀氀甀攀㬀ഀഀ
      return this;਍    紀 攀氀猀攀 笀ഀഀ
      return startSymbol;਍    紀ഀഀ
  };਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
   * @name ng.$interpolateProvider#endSymbol਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀椀渀琀攀爀瀀漀氀愀琀攀倀爀漀瘀椀搀攀爀ഀഀ
   * @description਍   ⨀ 匀礀洀戀漀氀 琀漀 搀攀渀漀琀攀 琀栀攀 攀渀搀 漀昀 攀砀瀀爀攀猀猀椀漀渀 椀渀 琀栀攀 椀渀琀攀爀瀀漀氀愀琀攀搀 猀琀爀椀渀最⸀ 䐀攀昀愀甀氀琀猀 琀漀 怀紀紀怀⸀ഀഀ
   *਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 瘀愀氀甀攀 渀攀眀 瘀愀氀甀攀 琀漀 猀攀琀 琀栀攀 攀渀搀椀渀最 猀礀洀戀漀氀 琀漀⸀ഀഀ
   * @returns {string|self} Returns the symbol when used as getter and self if used as setter.਍   ⨀⼀ഀഀ
  this.endSymbol = function(value){਍    椀昀 ⠀瘀愀氀甀攀⤀ 笀ഀഀ
      endSymbol = value;਍      爀攀琀甀爀渀 琀栀椀猀㬀ഀഀ
    } else {਍      爀攀琀甀爀渀 攀渀搀匀礀洀戀漀氀㬀ഀഀ
    }਍  紀㬀ഀഀ
਍ഀഀ
  this.$get = ['$parse', '$exceptionHandler', '$sce', function($parse, $exceptionHandler, $sce) {਍    瘀愀爀 猀琀愀爀琀匀礀洀戀漀氀䰀攀渀最琀栀 㴀 猀琀愀爀琀匀礀洀戀漀氀⸀氀攀渀最琀栀Ⰰഀഀ
        endSymbolLength = endSymbol.length;਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
     * @name ng.$interpolate਍     ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
     *਍     ⨀ 䀀爀攀焀甀椀爀攀猀 ␀瀀愀爀猀攀ഀഀ
     * @requires $sce਍     ⨀ഀഀ
     * @description਍     ⨀ഀഀ
     * Compiles a string with markup into an interpolation function. This service is used by the਍     ⨀ 䠀吀䴀䰀 笀䀀氀椀渀欀 渀最⸀␀挀漀洀瀀椀氀攀 ␀挀漀洀瀀椀氀攀紀 猀攀爀瘀椀挀攀 昀漀爀 搀愀琀愀 戀椀渀搀椀渀最⸀ 匀攀攀ഀഀ
     * {@link ng.$interpolateProvider $interpolateProvider} for configuring the਍     ⨀ 椀渀琀攀爀瀀漀氀愀琀椀漀渀 洀愀爀欀甀瀀⸀ഀഀ
     *਍     ⨀ഀഀ
       <pre>਍         瘀愀爀 ␀椀渀琀攀爀瀀漀氀愀琀攀 㴀 ⸀⸀⸀㬀 ⼀⼀ 椀渀樀攀挀琀攀搀ഀഀ
         var exp = $interpolate('Hello {{name | uppercase}}!');਍         攀砀瀀攀挀琀⠀攀砀瀀⠀笀渀愀洀攀㨀✀䄀渀最甀氀愀爀✀紀⤀⸀琀漀䔀焀甀愀氀⠀✀䠀攀氀氀漀 䄀一䜀唀䰀䄀刀℀✀⤀㬀ഀഀ
       </pre>਍     ⨀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 琀攀砀琀 吀栀攀 琀攀砀琀 眀椀琀栀 洀愀爀欀甀瀀 琀漀 椀渀琀攀爀瀀漀氀愀琀攀⸀ഀഀ
     * @param {boolean=} mustHaveExpression if set to true then the interpolation string must have਍     ⨀    攀洀戀攀搀搀攀搀 攀砀瀀爀攀猀猀椀漀渀 椀渀 漀爀搀攀爀 琀漀 爀攀琀甀爀渀 愀渀 椀渀琀攀爀瀀漀氀愀琀椀漀渀 昀甀渀挀琀椀漀渀⸀ 匀琀爀椀渀最猀 眀椀琀栀 渀漀ഀഀ
     *    embedded expression will return null for the interpolation function.਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 琀爀甀猀琀攀搀䌀漀渀琀攀砀琀 眀栀攀渀 瀀爀漀瘀椀搀攀搀Ⰰ 琀栀攀 爀攀琀甀爀渀攀搀 昀甀渀挀琀椀漀渀 瀀愀猀猀攀猀 琀栀攀 椀渀琀攀爀瀀漀氀愀琀攀搀ഀഀ
     *    result through {@link ng.$sce#methods_getTrusted $sce.getTrusted(interpolatedResult,਍     ⨀    琀爀甀猀琀攀搀䌀漀渀琀攀砀琀⤀紀 戀攀昀漀爀攀 爀攀琀甀爀渀椀渀最 椀琀⸀  刀攀昀攀爀 琀漀 琀栀攀 笀䀀氀椀渀欀 渀最⸀␀猀挀攀 ␀猀挀攀紀 猀攀爀瘀椀挀攀 琀栀愀琀ഀഀ
     *    provides Strict Contextual Escaping for details.਍     ⨀ 䀀爀攀琀甀爀渀猀 笀昀甀渀挀琀椀漀渀⠀挀漀渀琀攀砀琀⤀紀 愀渀 椀渀琀攀爀瀀漀氀愀琀椀漀渀 昀甀渀挀琀椀漀渀 眀栀椀挀栀 椀猀 甀猀攀搀 琀漀 挀漀洀瀀甀琀攀 琀栀攀ഀഀ
     *    interpolated string. The function has these parameters:਍     ⨀ഀഀ
     *    * `context`: an object against which any expressions embedded in the strings are evaluated਍     ⨀      愀最愀椀渀猀琀⸀ഀഀ
     *਍     ⨀⼀ഀഀ
    function $interpolate(text, mustHaveExpression, trustedContext) {਍      瘀愀爀 猀琀愀爀琀䤀渀搀攀砀Ⰰഀഀ
          endIndex,਍          椀渀搀攀砀 㴀 　Ⰰഀഀ
          parts = [],਍          氀攀渀最琀栀 㴀 琀攀砀琀⸀氀攀渀最琀栀Ⰰഀഀ
          hasInterpolation = false,਍          昀渀Ⰰഀഀ
          exp,਍          挀漀渀挀愀琀 㴀 嬀崀㬀ഀഀ
਍      眀栀椀氀攀⠀椀渀搀攀砀 㰀 氀攀渀最琀栀⤀ 笀ഀഀ
        if ( ((startIndex = text.indexOf(startSymbol, index)) != -1) &&਍             ⠀⠀攀渀搀䤀渀搀攀砀 㴀 琀攀砀琀⸀椀渀搀攀砀伀昀⠀攀渀搀匀礀洀戀漀氀Ⰰ 猀琀愀爀琀䤀渀搀攀砀 ⬀ 猀琀愀爀琀匀礀洀戀漀氀䰀攀渀最琀栀⤀⤀ ℀㴀 ⴀ㄀⤀ ⤀ 笀ഀഀ
          (index != startIndex) && parts.push(text.substring(index, startIndex));਍          瀀愀爀琀猀⸀瀀甀猀栀⠀昀渀 㴀 ␀瀀愀爀猀攀⠀攀砀瀀 㴀 琀攀砀琀⸀猀甀戀猀琀爀椀渀最⠀猀琀愀爀琀䤀渀搀攀砀 ⬀ 猀琀愀爀琀匀礀洀戀漀氀䰀攀渀最琀栀Ⰰ 攀渀搀䤀渀搀攀砀⤀⤀⤀㬀ഀഀ
          fn.exp = exp;਍          椀渀搀攀砀 㴀 攀渀搀䤀渀搀攀砀 ⬀ 攀渀搀匀礀洀戀漀氀䰀攀渀最琀栀㬀ഀഀ
          hasInterpolation = true;਍        紀 攀氀猀攀 笀ഀഀ
          // we did not find anything, so we have to add the remainder to the parts array਍          ⠀椀渀搀攀砀 ℀㴀 氀攀渀最琀栀⤀ ☀☀ 瀀愀爀琀猀⸀瀀甀猀栀⠀琀攀砀琀⸀猀甀戀猀琀爀椀渀最⠀椀渀搀攀砀⤀⤀㬀ഀഀ
          index = length;਍        紀ഀഀ
      }਍ഀഀ
      if (!(length = parts.length)) {਍        ⼀⼀ 眀攀 愀搀搀攀搀Ⰰ 渀漀琀栀椀渀最Ⰰ 洀甀猀琀 栀愀瘀攀 戀攀攀渀 愀渀 攀洀瀀琀礀 猀琀爀椀渀最⸀ഀഀ
        parts.push('');਍        氀攀渀最琀栀 㴀 ㄀㬀ഀഀ
      }਍ഀഀ
      // Concatenating expressions makes it hard to reason about whether some combination of਍      ⼀⼀ 挀漀渀挀愀琀攀渀愀琀攀搀 瘀愀氀甀攀猀 愀爀攀 甀渀猀愀昀攀 琀漀 甀猀攀 愀渀搀 挀漀甀氀搀 攀愀猀椀氀礀 氀攀愀搀 琀漀 堀匀匀⸀  䈀礀 爀攀焀甀椀爀椀渀最 琀栀愀琀 愀ഀഀ
      // single expression be used for iframe[src], object[src], etc., we ensure that the value਍      ⼀⼀ 琀栀愀琀✀猀 甀猀攀搀 椀猀 愀猀猀椀最渀攀搀 漀爀 挀漀渀猀琀爀甀挀琀攀搀 戀礀 猀漀洀攀 䨀匀 挀漀搀攀 猀漀洀攀眀栀攀爀攀 琀栀愀琀 椀猀 洀漀爀攀 琀攀猀琀愀戀氀攀 漀爀ഀഀ
      // make it obvious that you bound the value to some user controlled value.  This helps reduce਍      ⼀⼀ 琀栀攀 氀漀愀搀 眀栀攀渀 愀甀搀椀琀椀渀最 昀漀爀 堀匀匀 椀猀猀甀攀猀⸀ഀഀ
      if (trustedContext && parts.length > 1) {਍          琀栀爀漀眀 ␀椀渀琀攀爀瀀漀氀愀琀攀䴀椀渀䔀爀爀⠀✀渀漀挀漀渀挀愀琀✀Ⰰഀഀ
              "Error while interpolating: {0}\nStrict Contextual Escaping disallows " +਍              ∀椀渀琀攀爀瀀漀氀愀琀椀漀渀猀 琀栀愀琀 挀漀渀挀愀琀攀渀愀琀攀 洀甀氀琀椀瀀氀攀 攀砀瀀爀攀猀猀椀漀渀猀 眀栀攀渀 愀 琀爀甀猀琀攀搀 瘀愀氀甀攀 椀猀 ∀ ⬀ഀഀ
              "required.  See http://docs.angularjs.org/api/ng.$sce", text);਍      紀ഀഀ
਍      椀昀 ⠀℀洀甀猀琀䠀愀瘀攀䔀砀瀀爀攀猀猀椀漀渀  簀簀 栀愀猀䤀渀琀攀爀瀀漀氀愀琀椀漀渀⤀ 笀ഀഀ
        concat.length = length;਍        昀渀 㴀 昀甀渀挀琀椀漀渀⠀挀漀渀琀攀砀琀⤀ 笀ഀഀ
          try {਍            昀漀爀⠀瘀愀爀 椀 㴀 　Ⰰ 椀椀 㴀 氀攀渀最琀栀Ⰰ 瀀愀爀琀㬀 椀㰀椀椀㬀 椀⬀⬀⤀ 笀ഀഀ
              if (typeof (part = parts[i]) == 'function') {਍                瀀愀爀琀 㴀 瀀愀爀琀⠀挀漀渀琀攀砀琀⤀㬀ഀഀ
                if (trustedContext) {਍                  瀀愀爀琀 㴀 ␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀⠀琀爀甀猀琀攀搀䌀漀渀琀攀砀琀Ⰰ 瀀愀爀琀⤀㬀ഀഀ
                } else {਍                  瀀愀爀琀 㴀 ␀猀挀攀⸀瘀愀氀甀攀伀昀⠀瀀愀爀琀⤀㬀ഀഀ
                }਍                椀昀 ⠀瀀愀爀琀 㴀㴀㴀 渀甀氀氀 簀簀 椀猀唀渀搀攀昀椀渀攀搀⠀瀀愀爀琀⤀⤀ 笀ഀഀ
                  part = '';਍                紀 攀氀猀攀 椀昀 ⠀琀礀瀀攀漀昀 瀀愀爀琀 ℀㴀 ✀猀琀爀椀渀最✀⤀ 笀ഀഀ
                  part = toJson(part);਍                紀ഀഀ
              }਍              挀漀渀挀愀琀嬀椀崀 㴀 瀀愀爀琀㬀ഀഀ
            }਍            爀攀琀甀爀渀 挀漀渀挀愀琀⸀樀漀椀渀⠀✀✀⤀㬀ഀഀ
          }਍          挀愀琀挀栀⠀攀爀爀⤀ 笀ഀഀ
            var newErr = $interpolateMinErr('interr', "Can't interpolate: {0}\n{1}", text,਍                攀爀爀⸀琀漀匀琀爀椀渀最⠀⤀⤀㬀ഀഀ
            $exceptionHandler(newErr);਍          紀ഀഀ
        };਍        昀渀⸀攀砀瀀 㴀 琀攀砀琀㬀ഀഀ
        fn.parts = parts;਍        爀攀琀甀爀渀 昀渀㬀ഀഀ
      }਍    紀ഀഀ
਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$interpolate#startSymbol਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀椀渀琀攀爀瀀漀氀愀琀攀ഀഀ
     * @description਍     ⨀ 匀礀洀戀漀氀 琀漀 搀攀渀漀琀攀 琀栀攀 猀琀愀爀琀 漀昀 攀砀瀀爀攀猀猀椀漀渀 椀渀 琀栀攀 椀渀琀攀爀瀀漀氀愀琀攀搀 猀琀爀椀渀最⸀ 䐀攀昀愀甀氀琀猀 琀漀 怀笀笀怀⸀ഀഀ
     *਍     ⨀ 唀猀攀 笀䀀氀椀渀欀 渀最⸀␀椀渀琀攀爀瀀漀氀愀琀攀倀爀漀瘀椀搀攀爀⌀猀琀愀爀琀匀礀洀戀漀氀 ␀椀渀琀攀爀瀀漀氀愀琀攀倀爀漀瘀椀搀攀爀⌀猀琀愀爀琀匀礀洀戀漀氀紀 琀漀 挀栀愀渀最攀ഀഀ
     * the symbol.਍     ⨀ഀഀ
     * @returns {string} start symbol.਍     ⨀⼀ഀഀ
    $interpolate.startSymbol = function() {਍      爀攀琀甀爀渀 猀琀愀爀琀匀礀洀戀漀氀㬀ഀഀ
    };਍ഀഀ
਍    ⼀⨀⨀ഀഀ
     * @ngdoc method਍     ⨀ 䀀渀愀洀攀 渀最⸀␀椀渀琀攀爀瀀漀氀愀琀攀⌀攀渀搀匀礀洀戀漀氀ഀഀ
     * @methodOf ng.$interpolate਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Symbol to denote the end of expression in the interpolated string. Defaults to `}}`.਍     ⨀ഀഀ
     * Use {@link ng.$interpolateProvider#endSymbol $interpolateProvider#endSymbol} to change਍     ⨀ 琀栀攀 猀礀洀戀漀氀⸀ഀഀ
     *਍     ⨀ 䀀爀攀琀甀爀渀猀 笀猀琀爀椀渀最紀 猀琀愀爀琀 猀礀洀戀漀氀⸀ഀഀ
     */਍    ␀椀渀琀攀爀瀀漀氀愀琀攀⸀攀渀搀匀礀洀戀漀氀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
      return endSymbol;਍    紀㬀ഀഀ
਍    爀攀琀甀爀渀 ␀椀渀琀攀爀瀀漀氀愀琀攀㬀ഀഀ
  }];਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 ␀䤀渀琀攀爀瘀愀氀倀爀漀瘀椀搀攀爀⠀⤀ 笀ഀഀ
  this.$get = ['$rootScope', '$window', '$q',਍       昀甀渀挀琀椀漀渀⠀␀爀漀漀琀匀挀漀瀀攀Ⰰ   ␀眀椀渀搀漀眀Ⰰ   ␀焀⤀ 笀ഀഀ
    var intervals = {};਍ഀഀ
਍     ⼀⨀⨀ഀഀ
      * @ngdoc function਍      ⨀ 䀀渀愀洀攀 渀最⸀␀椀渀琀攀爀瘀愀氀ഀഀ
      *਍      ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
      * Angular's wrapper for `window.setInterval`. The `fn` function is executed every `delay`਍      ⨀ 洀椀氀氀椀猀攀挀漀渀搀猀⸀ഀഀ
      *਍      ⨀ 吀栀攀 爀攀琀甀爀渀 瘀愀氀甀攀 漀昀 爀攀最椀猀琀攀爀椀渀最 愀渀 椀渀琀攀爀瘀愀氀 昀甀渀挀琀椀漀渀 椀猀 愀 瀀爀漀洀椀猀攀⸀ 吀栀椀猀 瀀爀漀洀椀猀攀 眀椀氀氀 戀攀ഀഀ
      * notified upon each tick of the interval, and will be resolved after `count` iterations, or਍      ⨀ 爀甀渀 椀渀搀攀昀椀渀椀琀攀氀礀 椀昀 怀挀漀甀渀琀怀 椀猀 渀漀琀 搀攀昀椀渀攀搀⸀ 吀栀攀 瘀愀氀甀攀 漀昀 琀栀攀 渀漀琀椀昀椀挀愀琀椀漀渀 眀椀氀氀 戀攀 琀栀攀ഀഀ
      * number of iterations that have run.਍      ⨀ 吀漀 挀愀渀挀攀氀 愀渀 椀渀琀攀爀瘀愀氀Ⰰ 挀愀氀氀 怀␀椀渀琀攀爀瘀愀氀⸀挀愀渀挀攀氀⠀瀀爀漀洀椀猀攀⤀怀⸀ഀഀ
      *਍      ⨀ 䤀渀 琀攀猀琀猀 礀漀甀 挀愀渀 甀猀攀 笀䀀氀椀渀欀 渀最䴀漀挀欀⸀␀椀渀琀攀爀瘀愀氀⌀洀攀琀栀漀搀猀开昀氀甀猀栀 怀␀椀渀琀攀爀瘀愀氀⸀昀氀甀猀栀⠀洀椀氀氀椀猀⤀怀紀 琀漀ഀഀ
      * move forward by `millis` milliseconds and trigger any functions scheduled to run in that਍      ⨀ 琀椀洀攀⸀ഀഀ
      * ਍      ⨀ 㰀搀椀瘀 挀氀愀猀猀㴀∀愀氀攀爀琀 愀氀攀爀琀ⴀ眀愀爀渀椀渀最∀㸀ഀഀ
      * **Note**: Intervals created by this service must be explicitly destroyed when you are finished਍      ⨀ 眀椀琀栀 琀栀攀洀⸀  䤀渀 瀀愀爀琀椀挀甀氀愀爀 琀栀攀礀 愀爀攀 渀漀琀 愀甀琀漀洀愀琀椀挀愀氀氀礀 搀攀猀琀爀漀礀攀搀 眀栀攀渀 愀 挀漀渀琀爀漀氀氀攀爀✀猀 猀挀漀瀀攀 漀爀 愀ഀഀ
      * directive's element are destroyed.਍      ⨀ 夀漀甀 猀栀漀甀氀搀 琀愀欀攀 琀栀椀猀 椀渀琀漀 挀漀渀猀椀搀攀爀愀琀椀漀渀 愀渀搀 洀愀欀攀 猀甀爀攀 琀漀 愀氀眀愀礀猀 挀愀渀挀攀氀 琀栀攀 椀渀琀攀爀瘀愀氀 愀琀 琀栀攀ഀഀ
      * appropriate moment.  See the example below for more details on how and when to do this.਍      ⨀ 㰀⼀搀椀瘀㸀ഀഀ
      *਍      ⨀ 䀀瀀愀爀愀洀 笀昀甀渀挀琀椀漀渀⠀⤀紀 昀渀 䄀 昀甀渀挀琀椀漀渀 琀栀愀琀 猀栀漀甀氀搀 戀攀 挀愀氀氀攀搀 爀攀瀀攀愀琀攀搀氀礀⸀ഀഀ
      * @param {number} delay Number of milliseconds between each function call.਍      ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀㴀紀 嬀挀漀甀渀琀㴀　崀 一甀洀戀攀爀 漀昀 琀椀洀攀猀 琀漀 爀攀瀀攀愀琀⸀ 䤀昀 渀漀琀 猀攀琀Ⰰ 漀爀 　Ⰰ 眀椀氀氀 爀攀瀀攀愀琀ഀഀ
      *   indefinitely.਍      ⨀ 䀀瀀愀爀愀洀 笀戀漀漀氀攀愀渀㴀紀 嬀椀渀瘀漀欀攀䄀瀀瀀氀礀㴀琀爀甀攀崀 䤀昀 猀攀琀 琀漀 怀昀愀氀猀攀怀 猀欀椀瀀猀 洀漀搀攀氀 搀椀爀琀礀 挀栀攀挀欀椀渀最Ⰰ 漀琀栀攀爀眀椀猀攀ഀഀ
      *   will invoke `fn` within the {@link ng.$rootScope.Scope#methods_$apply $apply} block.਍      ⨀ 䀀爀攀琀甀爀渀猀 笀瀀爀漀洀椀猀攀紀 䄀 瀀爀漀洀椀猀攀 眀栀椀挀栀 眀椀氀氀 戀攀 渀漀琀椀昀椀攀搀 漀渀 攀愀挀栀 椀琀攀爀愀琀椀漀渀⸀ഀഀ
      *਍      ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
      <doc:example module="time">਍        㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
          <script>਍            昀甀渀挀琀椀漀渀 䌀琀爀氀㈀⠀␀猀挀漀瀀攀Ⰰ␀椀渀琀攀爀瘀愀氀⤀ 笀ഀഀ
              $scope.format = 'M/d/yy h:mm:ss a';਍              ␀猀挀漀瀀攀⸀戀氀漀漀搀开㄀ 㴀 ㄀　　㬀ഀഀ
              $scope.blood_2 = 120;਍ഀഀ
              var stop;਍              ␀猀挀漀瀀攀⸀昀椀最栀琀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
                // Don't start a new fight if we are already fighting਍                椀昀 ⠀ 愀渀最甀氀愀爀⸀椀猀䐀攀昀椀渀攀搀⠀猀琀漀瀀⤀ ⤀ 爀攀琀甀爀渀㬀ഀഀ
਍                猀琀漀瀀 㴀 ␀椀渀琀攀爀瘀愀氀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
                  if ($scope.blood_1 > 0 && $scope.blood_2 > 0) {਍                      ␀猀挀漀瀀攀⸀戀氀漀漀搀开㄀ 㴀 ␀猀挀漀瀀攀⸀戀氀漀漀搀开㄀ ⴀ ㌀㬀ഀഀ
                      $scope.blood_2 = $scope.blood_2 - 4;਍                  紀 攀氀猀攀 笀ഀഀ
                      $scope.stopFight();਍                  紀ഀഀ
                }, 100);਍              紀㬀ഀഀ
਍              ␀猀挀漀瀀攀⸀猀琀漀瀀䘀椀最栀琀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
                if (angular.isDefined(stop)) {਍                  ␀椀渀琀攀爀瘀愀氀⸀挀愀渀挀攀氀⠀猀琀漀瀀⤀㬀ഀഀ
                  stop = undefined;਍                紀ഀഀ
              };਍ഀഀ
              $scope.resetFight = function() {਍                ␀猀挀漀瀀攀⸀戀氀漀漀搀开㄀ 㴀 ㄀　　㬀ഀഀ
                $scope.blood_2 = 120;਍              紀ഀഀ
਍              ␀猀挀漀瀀攀⸀␀漀渀⠀✀␀搀攀猀琀爀漀礀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
                // Make sure that the interval is destroyed too਍                ␀猀挀漀瀀攀⸀猀琀漀瀀䘀椀最栀琀⠀⤀㬀ഀഀ
              });਍            紀ഀഀ
਍            愀渀最甀氀愀爀⸀洀漀搀甀氀攀⠀✀琀椀洀攀✀Ⰰ 嬀崀⤀ഀഀ
              // Register the 'myCurrentTime' directive factory method.਍              ⼀⼀ 圀攀 椀渀樀攀挀琀 ␀椀渀琀攀爀瘀愀氀 愀渀搀 搀愀琀攀䘀椀氀琀攀爀 猀攀爀瘀椀挀攀 猀椀渀挀攀 琀栀攀 昀愀挀琀漀爀礀 洀攀琀栀漀搀 椀猀 䐀䤀⸀ഀഀ
              .directive('myCurrentTime', function($interval, dateFilter) {਍                ⼀⼀ 爀攀琀甀爀渀 琀栀攀 搀椀爀攀挀琀椀瘀攀 氀椀渀欀 昀甀渀挀琀椀漀渀⸀ ⠀挀漀洀瀀椀氀攀 昀甀渀挀琀椀漀渀 渀漀琀 渀攀攀搀攀搀⤀ഀഀ
                return function(scope, element, attrs) {਍                  瘀愀爀 昀漀爀洀愀琀Ⰰ  ⼀⼀ 搀愀琀攀 昀漀爀洀愀琀ഀഀ
                  stopTime; // so that we can cancel the time updates਍ഀഀ
                  // used to update the UI਍                  昀甀渀挀琀椀漀渀 甀瀀搀愀琀攀吀椀洀攀⠀⤀ 笀ഀഀ
                    element.text(dateFilter(new Date(), format));਍                  紀ഀഀ
਍                  ⼀⼀ 眀愀琀挀栀 琀栀攀 攀砀瀀爀攀猀猀椀漀渀Ⰰ 愀渀搀 甀瀀搀愀琀攀 琀栀攀 唀䤀 漀渀 挀栀愀渀最攀⸀ഀഀ
                  scope.$watch(attrs.myCurrentTime, function(value) {਍                    昀漀爀洀愀琀 㴀 瘀愀氀甀攀㬀ഀഀ
                    updateTime();਍                  紀⤀㬀ഀഀ
਍                  猀琀漀瀀吀椀洀攀 㴀 ␀椀渀琀攀爀瘀愀氀⠀甀瀀搀愀琀攀吀椀洀攀Ⰰ ㄀　　　⤀㬀ഀഀ
਍                  ⼀⼀ 氀椀猀琀攀渀 漀渀 䐀伀䴀 搀攀猀琀爀漀礀 ⠀爀攀洀漀瘀愀氀⤀ 攀瘀攀渀琀Ⰰ 愀渀搀 挀愀渀挀攀氀 琀栀攀 渀攀砀琀 唀䤀 甀瀀搀愀琀攀ഀഀ
                  // to prevent updating time ofter the DOM element was removed.਍                  攀氀攀洀攀渀琀⸀戀椀渀搀⠀✀␀搀攀猀琀爀漀礀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
                    $interval.cancel(stopTime);਍                  紀⤀㬀ഀഀ
                }਍              紀⤀㬀ഀഀ
          </script>਍ഀഀ
          <div>਍            㰀搀椀瘀 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀䌀琀爀氀㈀∀㸀ഀഀ
              Date format: <input ng-model="format"> <hr/>਍              䌀甀爀爀攀渀琀 琀椀洀攀 椀猀㨀 㰀猀瀀愀渀 洀礀ⴀ挀甀爀爀攀渀琀ⴀ琀椀洀攀㴀∀昀漀爀洀愀琀∀㸀㰀⼀猀瀀愀渀㸀ഀഀ
              <hr/>਍              䈀氀漀漀搀 ㄀ 㨀 㰀昀漀渀琀 挀漀氀漀爀㴀✀爀攀搀✀㸀笀笀戀氀漀漀搀开㄀紀紀㰀⼀昀漀渀琀㸀ഀഀ
              Blood 2 : <font color='red'>{{blood_2}}</font>਍              㰀戀甀琀琀漀渀 琀礀瀀攀㴀∀戀甀琀琀漀渀∀ 搀愀琀愀ⴀ渀最ⴀ挀氀椀挀欀㴀∀昀椀最栀琀⠀⤀∀㸀䘀椀最栀琀㰀⼀戀甀琀琀漀渀㸀ഀഀ
              <button type="button" data-ng-click="stopFight()">StopFight</button>਍              㰀戀甀琀琀漀渀 琀礀瀀攀㴀∀戀甀琀琀漀渀∀ 搀愀琀愀ⴀ渀最ⴀ挀氀椀挀欀㴀∀爀攀猀攀琀䘀椀最栀琀⠀⤀∀㸀爀攀猀攀琀䘀椀最栀琀㰀⼀戀甀琀琀漀渀㸀ഀഀ
            </div>਍          㰀⼀搀椀瘀㸀ഀഀ
਍        㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      </doc:example>਍      ⨀⼀ഀഀ
    function interval(fn, delay, count, invokeApply) {਍      瘀愀爀 猀攀琀䤀渀琀攀爀瘀愀氀 㴀 ␀眀椀渀搀漀眀⸀猀攀琀䤀渀琀攀爀瘀愀氀Ⰰഀഀ
          clearInterval = $window.clearInterval,਍          搀攀昀攀爀爀攀搀 㴀 ␀焀⸀搀攀昀攀爀⠀⤀Ⰰഀഀ
          promise = deferred.promise,਍          椀琀攀爀愀琀椀漀渀 㴀 　Ⰰഀഀ
          skipApply = (isDefined(invokeApply) && !invokeApply);਍      ഀഀ
      count = isDefined(count) ? count : 0,਍ഀഀ
      promise.then(null, null, fn);਍ഀഀ
      promise.$$intervalId = setInterval(function tick() {਍        搀攀昀攀爀爀攀搀⸀渀漀琀椀昀礀⠀椀琀攀爀愀琀椀漀渀⬀⬀⤀㬀ഀഀ
਍        椀昀 ⠀挀漀甀渀琀 㸀 　 ☀☀ 椀琀攀爀愀琀椀漀渀 㸀㴀 挀漀甀渀琀⤀ 笀ഀഀ
          deferred.resolve(iteration);਍          挀氀攀愀爀䤀渀琀攀爀瘀愀氀⠀瀀爀漀洀椀猀攀⸀␀␀椀渀琀攀爀瘀愀氀䤀搀⤀㬀ഀഀ
          delete intervals[promise.$$intervalId];਍        紀ഀഀ
਍        椀昀 ⠀℀猀欀椀瀀䄀瀀瀀氀礀⤀ ␀爀漀漀琀匀挀漀瀀攀⸀␀愀瀀瀀氀礀⠀⤀㬀ഀഀ
਍      紀Ⰰ 搀攀氀愀礀⤀㬀ഀഀ
਍      椀渀琀攀爀瘀愀氀猀嬀瀀爀漀洀椀猀攀⸀␀␀椀渀琀攀爀瘀愀氀䤀搀崀 㴀 搀攀昀攀爀爀攀搀㬀ഀഀ
਍      爀攀琀甀爀渀 瀀爀漀洀椀猀攀㬀ഀഀ
    }਍ഀഀ
਍     ⼀⨀⨀ഀഀ
      * @ngdoc function਍      ⨀ 䀀渀愀洀攀 渀最⸀␀椀渀琀攀爀瘀愀氀⌀挀愀渀挀攀氀ഀഀ
      * @methodOf ng.$interval਍      ⨀ഀഀ
      * @description਍      ⨀ 䌀愀渀挀攀氀猀 愀 琀愀猀欀 愀猀猀漀挀椀愀琀攀搀 眀椀琀栀 琀栀攀 怀瀀爀漀洀椀猀攀怀⸀ഀഀ
      *਍      ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀紀 瀀爀漀洀椀猀攀 倀爀漀洀椀猀攀 爀攀琀甀爀渀攀搀 戀礀 琀栀攀 怀␀椀渀琀攀爀瘀愀氀怀 昀甀渀挀琀椀漀渀⸀ഀഀ
      * @returns {boolean} Returns `true` if the task was successfully canceled.਍      ⨀⼀ഀഀ
    interval.cancel = function(promise) {਍      椀昀 ⠀瀀爀漀洀椀猀攀 ☀☀ 瀀爀漀洀椀猀攀⸀␀␀椀渀琀攀爀瘀愀氀䤀搀 椀渀 椀渀琀攀爀瘀愀氀猀⤀ 笀ഀഀ
        intervals[promise.$$intervalId].reject('canceled');਍        挀氀攀愀爀䤀渀琀攀爀瘀愀氀⠀瀀爀漀洀椀猀攀⸀␀␀椀渀琀攀爀瘀愀氀䤀搀⤀㬀ഀഀ
        delete intervals[promise.$$intervalId];਍        爀攀琀甀爀渀 琀爀甀攀㬀ഀഀ
      }਍      爀攀琀甀爀渀 昀愀氀猀攀㬀ഀഀ
    };਍ഀഀ
    return interval;਍  紀崀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name ng.$locale਍ ⨀ഀഀ
 * @description਍ ⨀ ␀氀漀挀愀氀攀 猀攀爀瘀椀挀攀 瀀爀漀瘀椀搀攀猀 氀漀挀愀氀椀稀愀琀椀漀渀 爀甀氀攀猀 昀漀爀 瘀愀爀椀漀甀猀 䄀渀最甀氀愀爀 挀漀洀瀀漀渀攀渀琀猀⸀ 䄀猀 漀昀 爀椀最栀琀 渀漀眀 琀栀攀ഀഀ
 * only public api is:਍ ⨀ഀഀ
 * * `id` �� `{string}` �� locale id formatted as `languageId-countryId` (e.g. `en-us`)਍ ⨀⼀ഀഀ
function $LocaleProvider(){਍  琀栀椀猀⸀␀最攀琀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    return {਍      椀搀㨀 ✀攀渀ⴀ甀猀✀Ⰰഀഀ
਍      一唀䴀䈀䔀刀开䘀伀刀䴀䄀吀匀㨀 笀ഀഀ
        DECIMAL_SEP: '.',਍        䜀刀伀唀倀开匀䔀倀㨀 ✀Ⰰ✀Ⰰഀഀ
        PATTERNS: [਍          笀 ⼀⼀ 䐀攀挀椀洀愀氀 倀愀琀琀攀爀渀ഀഀ
            minInt: 1,਍            洀椀渀䘀爀愀挀㨀 　Ⰰഀഀ
            maxFrac: 3,਍            瀀漀猀倀爀攀㨀 ✀✀Ⰰഀഀ
            posSuf: '',਍            渀攀最倀爀攀㨀 ✀ⴀ✀Ⰰഀഀ
            negSuf: '',਍            最匀椀稀攀㨀 ㌀Ⰰഀഀ
            lgSize: 3਍          紀Ⰰ笀 ⼀⼀䌀甀爀爀攀渀挀礀 倀愀琀琀攀爀渀ഀഀ
            minInt: 1,਍            洀椀渀䘀爀愀挀㨀 ㈀Ⰰഀഀ
            maxFrac: 2,਍            瀀漀猀倀爀攀㨀 ✀尀甀　　䄀㐀✀Ⰰഀഀ
            posSuf: '',਍            渀攀最倀爀攀㨀 ✀⠀尀甀　　䄀㐀✀Ⰰഀഀ
            negSuf: ')',਍            最匀椀稀攀㨀 ㌀Ⰰഀഀ
            lgSize: 3਍          紀ഀഀ
        ],਍        䌀唀刀刀䔀一䌀夀开匀夀䴀㨀 ✀␀✀ഀഀ
      },਍ഀഀ
      DATETIME_FORMATS: {਍        䴀伀一吀䠀㨀ഀഀ
            'January,February,March,April,May,June,July,August,September,October,November,December'਍            ⸀猀瀀氀椀琀⠀✀Ⰰ✀⤀Ⰰഀഀ
        SHORTMONTH:  'Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec'.split(','),਍        䐀䄀夀㨀 ✀匀甀渀搀愀礀Ⰰ䴀漀渀搀愀礀Ⰰ吀甀攀猀搀愀礀Ⰰ圀攀搀渀攀猀搀愀礀Ⰰ吀栀甀爀猀搀愀礀Ⰰ䘀爀椀搀愀礀Ⰰ匀愀琀甀爀搀愀礀✀⸀猀瀀氀椀琀⠀✀Ⰰ✀⤀Ⰰഀഀ
        SHORTDAY: 'Sun,Mon,Tue,Wed,Thu,Fri,Sat'.split(','),਍        䄀䴀倀䴀匀㨀 嬀✀䄀䴀✀Ⰰ✀倀䴀✀崀Ⰰഀഀ
        medium: 'MMM d, y h:mm:ss a',਍        猀栀漀爀琀㨀 ✀䴀⼀搀⼀礀礀 栀㨀洀洀 愀✀Ⰰഀഀ
        fullDate: 'EEEE, MMMM d, y',਍        氀漀渀最䐀愀琀攀㨀 ✀䴀䴀䴀䴀 搀Ⰰ 礀✀Ⰰഀഀ
        mediumDate: 'MMM d, y',਍        猀栀漀爀琀䐀愀琀攀㨀 ✀䴀⼀搀⼀礀礀✀Ⰰഀഀ
        mediumTime: 'h:mm:ss a',਍        猀栀漀爀琀吀椀洀攀㨀 ✀栀㨀洀洀 愀✀ഀഀ
      },਍ഀഀ
      pluralCat: function(num) {਍        椀昀 ⠀渀甀洀 㴀㴀㴀 ㄀⤀ 笀ഀഀ
          return 'one';਍        紀ഀഀ
        return 'other';਍      紀ഀഀ
    };਍  紀㬀ഀഀ
}਍ഀഀ
var PATH_MATCH = /^([^\?#]*)(\?([^#]*))?(#(.*))?$/,਍    䐀䔀䘀䄀唀䰀吀开倀伀刀吀匀 㴀 笀✀栀琀琀瀀✀㨀 㠀　Ⰰ ✀栀琀琀瀀猀✀㨀 㐀㐀㌀Ⰰ ✀昀琀瀀✀㨀 ㈀㄀紀㬀ഀഀ
var $locationMinErr = minErr('$location');਍ഀഀ
਍⼀⨀⨀ഀഀ
 * Encode path using encodeUriSegment, ignoring forward slashes਍ ⨀ഀഀ
 * @param {string} path Path to encode਍ ⨀ 䀀爀攀琀甀爀渀猀 笀猀琀爀椀渀最紀ഀഀ
 */਍昀甀渀挀琀椀漀渀 攀渀挀漀搀攀倀愀琀栀⠀瀀愀琀栀⤀ 笀ഀഀ
  var segments = path.split('/'),਍      椀 㴀 猀攀最洀攀渀琀猀⸀氀攀渀最琀栀㬀ഀഀ
਍  眀栀椀氀攀 ⠀椀ⴀⴀ⤀ 笀ഀഀ
    segments[i] = encodeUriSegment(segments[i]);਍  紀ഀഀ
਍  爀攀琀甀爀渀 猀攀最洀攀渀琀猀⸀樀漀椀渀⠀✀⼀✀⤀㬀ഀഀ
}਍ഀഀ
function parseAbsoluteUrl(absoluteUrl, locationObj, appBase) {਍  瘀愀爀 瀀愀爀猀攀搀唀爀氀 㴀 甀爀氀刀攀猀漀氀瘀攀⠀愀戀猀漀氀甀琀攀唀爀氀Ⰰ 愀瀀瀀䈀愀猀攀⤀㬀ഀഀ
਍  氀漀挀愀琀椀漀渀伀戀樀⸀␀␀瀀爀漀琀漀挀漀氀 㴀 瀀愀爀猀攀搀唀爀氀⸀瀀爀漀琀漀挀漀氀㬀ഀഀ
  locationObj.$$host = parsedUrl.hostname;਍  氀漀挀愀琀椀漀渀伀戀樀⸀␀␀瀀漀爀琀 㴀 椀渀琀⠀瀀愀爀猀攀搀唀爀氀⸀瀀漀爀琀⤀ 簀簀 䐀䔀䘀䄀唀䰀吀开倀伀刀吀匀嬀瀀愀爀猀攀搀唀爀氀⸀瀀爀漀琀漀挀漀氀崀 簀簀 渀甀氀氀㬀ഀഀ
}਍ഀഀ
਍昀甀渀挀琀椀漀渀 瀀愀爀猀攀䄀瀀瀀唀爀氀⠀爀攀氀愀琀椀瘀攀唀爀氀Ⰰ 氀漀挀愀琀椀漀渀伀戀樀Ⰰ 愀瀀瀀䈀愀猀攀⤀ 笀ഀഀ
  var prefixed = (relativeUrl.charAt(0) !== '/');਍  椀昀 ⠀瀀爀攀昀椀砀攀搀⤀ 笀ഀഀ
    relativeUrl = '/' + relativeUrl;਍  紀ഀഀ
  var match = urlResolve(relativeUrl, appBase);਍  氀漀挀愀琀椀漀渀伀戀樀⸀␀␀瀀愀琀栀 㴀 搀攀挀漀搀攀唀刀䤀䌀漀洀瀀漀渀攀渀琀⠀瀀爀攀昀椀砀攀搀 ☀☀ 洀愀琀挀栀⸀瀀愀琀栀渀愀洀攀⸀挀栀愀爀䄀琀⠀　⤀ 㴀㴀㴀 ✀⼀✀ 㼀ഀഀ
      match.pathname.substring(1) : match.pathname);਍  氀漀挀愀琀椀漀渀伀戀樀⸀␀␀猀攀愀爀挀栀 㴀 瀀愀爀猀攀䬀攀礀嘀愀氀甀攀⠀洀愀琀挀栀⸀猀攀愀爀挀栀⤀㬀ഀഀ
  locationObj.$$hash = decodeURIComponent(match.hash);਍ഀഀ
  // make sure path starts with '/';਍  椀昀 ⠀氀漀挀愀琀椀漀渀伀戀樀⸀␀␀瀀愀琀栀 ☀☀ 氀漀挀愀琀椀漀渀伀戀樀⸀␀␀瀀愀琀栀⸀挀栀愀爀䄀琀⠀　⤀ ℀㴀 ✀⼀✀⤀ 笀ഀഀ
    locationObj.$$path = '/' + locationObj.$$path;਍  紀ഀഀ
}਍ഀഀ
਍⼀⨀⨀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 戀攀最椀渀ഀഀ
 * @param {string} whole਍ ⨀ 䀀爀攀琀甀爀渀猀 笀猀琀爀椀渀最紀 爀攀琀甀爀渀猀 琀攀砀琀 昀爀漀洀 眀栀漀氀攀 愀昀琀攀爀 戀攀最椀渀 漀爀 甀渀搀攀昀椀渀攀搀 椀昀 椀琀 搀漀攀猀 渀漀琀 戀攀最椀渀 眀椀琀栀ഀഀ
 *                   expected string.਍ ⨀⼀ഀഀ
function beginsWith(begin, whole) {਍  椀昀 ⠀眀栀漀氀攀⸀椀渀搀攀砀伀昀⠀戀攀最椀渀⤀ 㴀㴀㴀 　⤀ 笀ഀഀ
    return whole.substr(begin.length);਍  紀ഀഀ
}਍ഀഀ
਍昀甀渀挀琀椀漀渀 猀琀爀椀瀀䠀愀猀栀⠀甀爀氀⤀ 笀ഀഀ
  var index = url.indexOf('#');਍  爀攀琀甀爀渀 椀渀搀攀砀 㴀㴀 ⴀ㄀ 㼀 甀爀氀 㨀 甀爀氀⸀猀甀戀猀琀爀⠀　Ⰰ 椀渀搀攀砀⤀㬀ഀഀ
}਍ഀഀ
਍昀甀渀挀琀椀漀渀 猀琀爀椀瀀䘀椀氀攀⠀甀爀氀⤀ 笀ഀഀ
  return url.substr(0, stripHash(url).lastIndexOf('/') + 1);਍紀ഀഀ
਍⼀⨀ 爀攀琀甀爀渀 琀栀攀 猀攀爀瘀攀爀 漀渀氀礀 ⠀猀挀栀攀洀攀㨀⼀⼀栀漀猀琀㨀瀀漀爀琀⤀ ⨀⼀ഀഀ
function serverBase(url) {਍  爀攀琀甀爀渀 甀爀氀⸀猀甀戀猀琀爀椀渀最⠀　Ⰰ 甀爀氀⸀椀渀搀攀砀伀昀⠀✀⼀✀Ⰰ 甀爀氀⸀椀渀搀攀砀伀昀⠀✀⼀⼀✀⤀ ⬀ ㈀⤀⤀㬀ഀഀ
}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * LocationHtml5Url represents an url਍ ⨀ 吀栀椀猀 漀戀樀攀挀琀 椀猀 攀砀瀀漀猀攀搀 愀猀 ␀氀漀挀愀琀椀漀渀 猀攀爀瘀椀挀攀 眀栀攀渀 䠀吀䴀䰀㔀 洀漀搀攀 椀猀 攀渀愀戀氀攀搀 愀渀搀 猀甀瀀瀀漀爀琀攀搀ഀഀ
 *਍ ⨀ 䀀挀漀渀猀琀爀甀挀琀漀爀ഀഀ
 * @param {string} appBase application base URL਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 戀愀猀攀倀爀攀昀椀砀 甀爀氀 瀀愀琀栀 瀀爀攀昀椀砀ഀഀ
 */਍昀甀渀挀琀椀漀渀 䰀漀挀愀琀椀漀渀䠀琀洀氀㔀唀爀氀⠀愀瀀瀀䈀愀猀攀Ⰰ 戀愀猀攀倀爀攀昀椀砀⤀ 笀ഀഀ
  this.$$html5 = true;਍  戀愀猀攀倀爀攀昀椀砀 㴀 戀愀猀攀倀爀攀昀椀砀 簀簀 ✀✀㬀ഀഀ
  var appBaseNoFile = stripFile(appBase);਍  瀀愀爀猀攀䄀戀猀漀氀甀琀攀唀爀氀⠀愀瀀瀀䈀愀猀攀Ⰰ 琀栀椀猀Ⰰ 愀瀀瀀䈀愀猀攀⤀㬀ഀഀ
਍ഀഀ
  /**਍   ⨀ 倀愀爀猀攀 最椀瘀攀渀 栀琀洀氀㔀 ⠀爀攀最甀氀愀爀⤀ 甀爀氀 猀琀爀椀渀最 椀渀琀漀 瀀爀漀瀀攀爀琀椀攀猀ഀഀ
   * @param {string} newAbsoluteUrl HTML5 url਍   ⨀ 䀀瀀爀椀瘀愀琀攀ഀഀ
   */਍  琀栀椀猀⸀␀␀瀀愀爀猀攀 㴀 昀甀渀挀琀椀漀渀⠀甀爀氀⤀ 笀ഀഀ
    var pathUrl = beginsWith(appBaseNoFile, url);਍    椀昀 ⠀℀椀猀匀琀爀椀渀最⠀瀀愀琀栀唀爀氀⤀⤀ 笀ഀഀ
      throw $locationMinErr('ipthprfx', 'Invalid url "{0}", missing path prefix "{1}".', url,਍          愀瀀瀀䈀愀猀攀一漀䘀椀氀攀⤀㬀ഀഀ
    }਍ഀഀ
    parseAppUrl(pathUrl, this, appBase);਍ഀഀ
    if (!this.$$path) {਍      琀栀椀猀⸀␀␀瀀愀琀栀 㴀 ✀⼀✀㬀ഀഀ
    }਍ഀഀ
    this.$$compose();਍  紀㬀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * Compose url and update `absUrl` property਍   ⨀ 䀀瀀爀椀瘀愀琀攀ഀഀ
   */਍  琀栀椀猀⸀␀␀挀漀洀瀀漀猀攀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var search = toKeyValue(this.$$search),਍        栀愀猀栀 㴀 琀栀椀猀⸀␀␀栀愀猀栀 㼀 ✀⌀✀ ⬀ 攀渀挀漀搀攀唀爀椀匀攀最洀攀渀琀⠀琀栀椀猀⸀␀␀栀愀猀栀⤀ 㨀 ✀✀㬀ഀഀ
਍    琀栀椀猀⸀␀␀甀爀氀 㴀 攀渀挀漀搀攀倀愀琀栀⠀琀栀椀猀⸀␀␀瀀愀琀栀⤀ ⬀ ⠀猀攀愀爀挀栀 㼀 ✀㼀✀ ⬀ 猀攀愀爀挀栀 㨀 ✀✀⤀ ⬀ 栀愀猀栀㬀ഀഀ
    this.$$absUrl = appBaseNoFile + this.$$url.substr(1); // first char is always '/'਍  紀㬀ഀഀ
਍  琀栀椀猀⸀␀␀爀攀眀爀椀琀攀 㴀 昀甀渀挀琀椀漀渀⠀甀爀氀⤀ 笀ഀഀ
    var appUrl, prevAppUrl;਍ഀഀ
    if ( (appUrl = beginsWith(appBase, url)) !== undefined ) {਍      瀀爀攀瘀䄀瀀瀀唀爀氀 㴀 愀瀀瀀唀爀氀㬀ഀഀ
      if ( (appUrl = beginsWith(basePrefix, appUrl)) !== undefined ) {਍        爀攀琀甀爀渀 愀瀀瀀䈀愀猀攀一漀䘀椀氀攀 ⬀ ⠀戀攀最椀渀猀圀椀琀栀⠀✀⼀✀Ⰰ 愀瀀瀀唀爀氀⤀ 簀簀 愀瀀瀀唀爀氀⤀㬀ഀഀ
      } else {਍        爀攀琀甀爀渀 愀瀀瀀䈀愀猀攀 ⬀ 瀀爀攀瘀䄀瀀瀀唀爀氀㬀ഀഀ
      }਍    紀 攀氀猀攀 椀昀 ⠀ ⠀愀瀀瀀唀爀氀 㴀 戀攀最椀渀猀圀椀琀栀⠀愀瀀瀀䈀愀猀攀一漀䘀椀氀攀Ⰰ 甀爀氀⤀⤀ ℀㴀㴀 甀渀搀攀昀椀渀攀搀 ⤀ 笀ഀഀ
      return appBaseNoFile + appUrl;਍    紀 攀氀猀攀 椀昀 ⠀愀瀀瀀䈀愀猀攀一漀䘀椀氀攀 㴀㴀 甀爀氀 ⬀ ✀⼀✀⤀ 笀ഀഀ
      return appBaseNoFile;਍    紀ഀഀ
  };਍紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䰀漀挀愀琀椀漀渀䠀愀猀栀戀愀渀最唀爀氀 爀攀瀀爀攀猀攀渀琀猀 甀爀氀ഀഀ
 * This object is exposed as $location service when developer doesn't opt into html5 mode.਍ ⨀ 䤀琀 愀氀猀漀 猀攀爀瘀攀猀 愀猀 琀栀攀 戀愀猀攀 挀氀愀猀猀 昀漀爀 栀琀洀氀㔀 洀漀搀攀 昀愀氀氀戀愀挀欀 漀渀 氀攀最愀挀礀 戀爀漀眀猀攀爀猀⸀ഀഀ
 *਍ ⨀ 䀀挀漀渀猀琀爀甀挀琀漀爀ഀഀ
 * @param {string} appBase application base URL਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 栀愀猀栀倀爀攀昀椀砀 栀愀猀栀戀愀渀最 瀀爀攀昀椀砀ഀഀ
 */਍昀甀渀挀琀椀漀渀 䰀漀挀愀琀椀漀渀䠀愀猀栀戀愀渀最唀爀氀⠀愀瀀瀀䈀愀猀攀Ⰰ 栀愀猀栀倀爀攀昀椀砀⤀ 笀ഀഀ
  var appBaseNoFile = stripFile(appBase);਍ഀഀ
  parseAbsoluteUrl(appBase, this, appBase);਍ഀഀ
਍  ⼀⨀⨀ഀഀ
   * Parse given hashbang url into properties਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 甀爀氀 䠀愀猀栀戀愀渀最 甀爀氀ഀഀ
   * @private਍   ⨀⼀ഀഀ
  this.$$parse = function(url) {਍    瘀愀爀 眀椀琀栀漀甀琀䈀愀猀攀唀爀氀 㴀 戀攀最椀渀猀圀椀琀栀⠀愀瀀瀀䈀愀猀攀Ⰰ 甀爀氀⤀ 簀簀 戀攀最椀渀猀圀椀琀栀⠀愀瀀瀀䈀愀猀攀一漀䘀椀氀攀Ⰰ 甀爀氀⤀㬀ഀഀ
    var withoutHashUrl = withoutBaseUrl.charAt(0) == '#'਍        㼀 戀攀最椀渀猀圀椀琀栀⠀栀愀猀栀倀爀攀昀椀砀Ⰰ 眀椀琀栀漀甀琀䈀愀猀攀唀爀氀⤀ഀഀ
        : (this.$$html5)਍          㼀 眀椀琀栀漀甀琀䈀愀猀攀唀爀氀ഀഀ
          : '';਍ഀഀ
    if (!isString(withoutHashUrl)) {਍      琀栀爀漀眀 ␀氀漀挀愀琀椀漀渀䴀椀渀䔀爀爀⠀✀椀栀猀栀瀀爀昀砀✀Ⰰ ✀䤀渀瘀愀氀椀搀 甀爀氀 ∀笀　紀∀Ⰰ 洀椀猀猀椀渀最 栀愀猀栀 瀀爀攀昀椀砀 ∀笀㄀紀∀⸀✀Ⰰ 甀爀氀Ⰰഀഀ
          hashPrefix);਍    紀ഀഀ
    parseAppUrl(withoutHashUrl, this, appBase);਍ഀഀ
    this.$$path = removeWindowsDriveName(this.$$path, withoutHashUrl, appBase);਍ഀഀ
    this.$$compose();਍ഀഀ
    /*਍     ⨀ 䤀渀 圀椀渀搀漀眀猀Ⰰ 漀渀 愀渀 愀渀挀栀漀爀 渀漀搀攀 漀渀 搀漀挀甀洀攀渀琀猀 氀漀愀搀攀搀 昀爀漀洀ഀഀ
     * the filesystem, the browser will return a pathname਍     ⨀ 瀀爀攀昀椀砀攀搀 眀椀琀栀 琀栀攀 搀爀椀瘀攀 渀愀洀攀 ⠀✀⼀䌀㨀⼀瀀愀琀栀✀⤀ 眀栀攀渀 愀ഀഀ
     * pathname without a drive is set:਍     ⨀  ⨀ 愀⸀猀攀琀䄀琀琀爀椀戀甀琀攀⠀✀栀爀攀昀✀Ⰰ ✀⼀昀漀漀✀⤀ഀഀ
     *   * a.pathname === '/C:/foo' //true਍     ⨀ഀഀ
     * Inside of Angular, we're always using pathnames that਍     ⨀ 搀漀 渀漀琀 椀渀挀氀甀搀攀 搀爀椀瘀攀 渀愀洀攀猀 昀漀爀 爀漀甀琀椀渀最⸀ഀഀ
     */਍    昀甀渀挀琀椀漀渀 爀攀洀漀瘀攀圀椀渀搀漀眀猀䐀爀椀瘀攀一愀洀攀 ⠀瀀愀琀栀Ⰰ 甀爀氀Ⰰ 戀愀猀攀⤀ 笀ഀഀ
      /*਍      䴀愀琀挀栀攀猀 瀀愀琀栀猀 昀漀爀 昀椀氀攀 瀀爀漀琀漀挀漀氀 漀渀 眀椀渀搀漀眀猀Ⰰഀഀ
      such as /C:/foo/bar, and captures only /foo/bar.਍      ⨀⼀ഀഀ
      var windowsFilePathExp = /^\/?.*?:(\/.*)/;਍ഀഀ
      var firstPathSegmentMatch;਍ഀഀ
      //Get the relative path from the input URL.਍      椀昀 ⠀甀爀氀⸀椀渀搀攀砀伀昀⠀戀愀猀攀⤀ 㴀㴀㴀 　⤀ 笀ഀഀ
        url = url.replace(base, '');਍      紀ഀഀ
਍      ⼀⨀ഀഀ
       * The input URL intentionally contains a਍       ⨀ 昀椀爀猀琀 瀀愀琀栀 猀攀最洀攀渀琀 琀栀愀琀 攀渀搀猀 眀椀琀栀 愀 挀漀氀漀渀⸀ഀഀ
       */਍      椀昀 ⠀眀椀渀搀漀眀猀䘀椀氀攀倀愀琀栀䔀砀瀀⸀攀砀攀挀⠀甀爀氀⤀⤀ 笀ഀഀ
        return path;਍      紀ഀഀ
਍      昀椀爀猀琀倀愀琀栀匀攀最洀攀渀琀䴀愀琀挀栀 㴀 眀椀渀搀漀眀猀䘀椀氀攀倀愀琀栀䔀砀瀀⸀攀砀攀挀⠀瀀愀琀栀⤀㬀ഀഀ
      return firstPathSegmentMatch ? firstPathSegmentMatch[1] : path;਍    紀ഀഀ
  };਍ഀഀ
  /**਍   ⨀ 䌀漀洀瀀漀猀攀 栀愀猀栀戀愀渀最 甀爀氀 愀渀搀 甀瀀搀愀琀攀 怀愀戀猀唀爀氀怀 瀀爀漀瀀攀爀琀礀ഀഀ
   * @private਍   ⨀⼀ഀഀ
  this.$$compose = function() {਍    瘀愀爀 猀攀愀爀挀栀 㴀 琀漀䬀攀礀嘀愀氀甀攀⠀琀栀椀猀⸀␀␀猀攀愀爀挀栀⤀Ⰰഀഀ
        hash = this.$$hash ? '#' + encodeUriSegment(this.$$hash) : '';਍ഀഀ
    this.$$url = encodePath(this.$$path) + (search ? '?' + search : '') + hash;਍    琀栀椀猀⸀␀␀愀戀猀唀爀氀 㴀 愀瀀瀀䈀愀猀攀 ⬀ ⠀琀栀椀猀⸀␀␀甀爀氀 㼀 栀愀猀栀倀爀攀昀椀砀 ⬀ 琀栀椀猀⸀␀␀甀爀氀 㨀 ✀✀⤀㬀ഀഀ
  };਍ഀഀ
  this.$$rewrite = function(url) {਍    椀昀⠀猀琀爀椀瀀䠀愀猀栀⠀愀瀀瀀䈀愀猀攀⤀ 㴀㴀 猀琀爀椀瀀䠀愀猀栀⠀甀爀氀⤀⤀ 笀ഀഀ
      return url;਍    紀ഀഀ
  };਍紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䰀漀挀愀琀椀漀渀䠀愀猀栀戀愀渀最唀爀氀 爀攀瀀爀攀猀攀渀琀猀 甀爀氀ഀഀ
 * This object is exposed as $location service when html5 history api is enabled but the browser਍ ⨀ 搀漀攀猀 渀漀琀 猀甀瀀瀀漀爀琀 椀琀⸀ഀഀ
 *਍ ⨀ 䀀挀漀渀猀琀爀甀挀琀漀爀ഀഀ
 * @param {string} appBase application base URL਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 栀愀猀栀倀爀攀昀椀砀 栀愀猀栀戀愀渀最 瀀爀攀昀椀砀ഀഀ
 */਍昀甀渀挀琀椀漀渀 䰀漀挀愀琀椀漀渀䠀愀猀栀戀愀渀最䤀渀䠀琀洀氀㔀唀爀氀⠀愀瀀瀀䈀愀猀攀Ⰰ 栀愀猀栀倀爀攀昀椀砀⤀ 笀ഀഀ
  this.$$html5 = true;਍  䰀漀挀愀琀椀漀渀䠀愀猀栀戀愀渀最唀爀氀⸀愀瀀瀀氀礀⠀琀栀椀猀Ⰰ 愀爀最甀洀攀渀琀猀⤀㬀ഀഀ
਍  瘀愀爀 愀瀀瀀䈀愀猀攀一漀䘀椀氀攀 㴀 猀琀爀椀瀀䘀椀氀攀⠀愀瀀瀀䈀愀猀攀⤀㬀ഀഀ
਍  琀栀椀猀⸀␀␀爀攀眀爀椀琀攀 㴀 昀甀渀挀琀椀漀渀⠀甀爀氀⤀ 笀ഀഀ
    var appUrl;਍ഀഀ
    if ( appBase == stripHash(url) ) {਍      爀攀琀甀爀渀 甀爀氀㬀ഀഀ
    } else if ( (appUrl = beginsWith(appBaseNoFile, url)) ) {਍      爀攀琀甀爀渀 愀瀀瀀䈀愀猀攀 ⬀ 栀愀猀栀倀爀攀昀椀砀 ⬀ 愀瀀瀀唀爀氀㬀ഀഀ
    } else if ( appBaseNoFile === url + '/') {਍      爀攀琀甀爀渀 愀瀀瀀䈀愀猀攀一漀䘀椀氀攀㬀ഀഀ
    }਍  紀㬀ഀഀ
}਍ഀഀ
਍䰀漀挀愀琀椀漀渀䠀愀猀栀戀愀渀最䤀渀䠀琀洀氀㔀唀爀氀⸀瀀爀漀琀漀琀礀瀀攀 㴀ഀഀ
  LocationHashbangUrl.prototype =਍  䰀漀挀愀琀椀漀渀䠀琀洀氀㔀唀爀氀⸀瀀爀漀琀漀琀礀瀀攀 㴀 笀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * Are we in html5 mode?਍   ⨀ 䀀瀀爀椀瘀愀琀攀ഀഀ
   */਍  ␀␀栀琀洀氀㔀㨀 昀愀氀猀攀Ⰰഀഀ
਍  ⼀⨀⨀ഀഀ
   * Has any change been replacing ?਍   ⨀ 䀀瀀爀椀瘀愀琀攀ഀഀ
   */਍  ␀␀爀攀瀀氀愀挀攀㨀 昀愀氀猀攀Ⰰഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc method਍   ⨀ 䀀渀愀洀攀 渀最⸀␀氀漀挀愀琀椀漀渀⌀愀戀猀唀爀氀ഀഀ
   * @methodOf ng.$location਍   ⨀ഀഀ
   * @description਍   ⨀ 吀栀椀猀 洀攀琀栀漀搀 椀猀 最攀琀琀攀爀 漀渀氀礀⸀ഀഀ
   *਍   ⨀ 刀攀琀甀爀渀 昀甀氀氀 甀爀氀 爀攀瀀爀攀猀攀渀琀愀琀椀漀渀 眀椀琀栀 愀氀氀 猀攀最洀攀渀琀猀 攀渀挀漀搀攀搀 愀挀挀漀爀搀椀渀最 琀漀 爀甀氀攀猀 猀瀀攀挀椀昀椀攀搀 椀渀ഀഀ
   * {@link http://www.ietf.org/rfc/rfc3986.txt RFC 3986}.਍   ⨀ഀഀ
   * @return {string} full url਍   ⨀⼀ഀഀ
  absUrl: locationGetter('$$absUrl'),਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
   * @name ng.$location#url਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀氀漀挀愀琀椀漀渀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * This method is getter / setter.਍   ⨀ഀഀ
   * Return url (e.g. `/path?a=b#hash`) when called without any parameter.਍   ⨀ഀഀ
   * Change path, search and hash, when called with parameter and return `$location`.਍   ⨀ഀഀ
   * @param {string=} url New url without base prefix (e.g. `/path?a=b#hash`)਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 爀攀瀀氀愀挀攀 吀栀攀 瀀愀琀栀 琀栀愀琀 眀椀氀氀 戀攀 挀栀愀渀最攀搀ഀഀ
   * @return {string} url਍   ⨀⼀ഀഀ
  url: function(url, replace) {਍    椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀甀爀氀⤀⤀ഀഀ
      return this.$$url;਍ഀഀ
    var match = PATH_MATCH.exec(url);਍    椀昀 ⠀洀愀琀挀栀嬀㄀崀⤀ 琀栀椀猀⸀瀀愀琀栀⠀搀攀挀漀搀攀唀刀䤀䌀漀洀瀀漀渀攀渀琀⠀洀愀琀挀栀嬀㄀崀⤀⤀㬀ഀഀ
    if (match[2] || match[1]) this.search(match[3] || '');਍    琀栀椀猀⸀栀愀猀栀⠀洀愀琀挀栀嬀㔀崀 簀簀 ✀✀Ⰰ 爀攀瀀氀愀挀攀⤀㬀ഀഀ
਍    爀攀琀甀爀渀 琀栀椀猀㬀ഀഀ
  },਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
   * @name ng.$location#protocol਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀氀漀挀愀琀椀漀渀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * This method is getter only.਍   ⨀ഀഀ
   * Return protocol of current url.਍   ⨀ഀഀ
   * @return {string} protocol of current url਍   ⨀⼀ഀഀ
  protocol: locationGetter('$$protocol'),਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
   * @name ng.$location#host਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀氀漀挀愀琀椀漀渀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * This method is getter only.਍   ⨀ഀഀ
   * Return host of current url.਍   ⨀ഀഀ
   * @return {string} host of current url.਍   ⨀⼀ഀഀ
  host: locationGetter('$$host'),਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
   * @name ng.$location#port਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀氀漀挀愀琀椀漀渀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * This method is getter only.਍   ⨀ഀഀ
   * Return port of current url.਍   ⨀ഀഀ
   * @return {Number} port਍   ⨀⼀ഀഀ
  port: locationGetter('$$port'),਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
   * @name ng.$location#path਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀氀漀挀愀琀椀漀渀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * This method is getter / setter.਍   ⨀ഀഀ
   * Return path of current url when called without any parameter.਍   ⨀ഀഀ
   * Change path when called with parameter and return `$location`.਍   ⨀ഀഀ
   * Note: Path should always begin with forward slash (/), this method will add the forward slash਍   ⨀ 椀昀 椀琀 椀猀 洀椀猀猀椀渀最⸀ഀഀ
   *਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 瀀愀琀栀 一攀眀 瀀愀琀栀ഀഀ
   * @return {string} path਍   ⨀⼀ഀഀ
  path: locationGetterSetter('$$path', function(path) {਍    爀攀琀甀爀渀 瀀愀琀栀⸀挀栀愀爀䄀琀⠀　⤀ 㴀㴀 ✀⼀✀ 㼀 瀀愀琀栀 㨀 ✀⼀✀ ⬀ 瀀愀琀栀㬀ഀഀ
  }),਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
   * @name ng.$location#search਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀氀漀挀愀琀椀漀渀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * This method is getter / setter.਍   ⨀ഀഀ
   * Return search part (as object) of current url when called without any parameter.਍   ⨀ഀഀ
   * Change search part when called with parameter and return `$location`.਍   ⨀ഀഀ
   * @param {string|Object.<string>|Object.<Array.<string>>} search New search params - string or਍   ⨀ 栀愀猀栀 漀戀樀攀挀琀⸀ 䠀愀猀栀 漀戀樀攀挀琀 洀愀礀 挀漀渀琀愀椀渀 愀渀 愀爀爀愀礀 漀昀 瘀愀氀甀攀猀Ⰰ 眀栀椀挀栀 眀椀氀氀 戀攀 搀攀挀漀搀攀搀 愀猀 搀甀瀀氀椀挀愀琀攀猀 椀渀ഀഀ
   * the url.਍   ⨀ഀഀ
   * @param {(string|Array<string>)=} paramValue If `search` is a string, then `paramValue` will override only a਍   ⨀ 猀椀渀最氀攀 猀攀愀爀挀栀 瀀愀爀愀洀攀琀攀爀⸀ 䤀昀 怀瀀愀爀愀洀嘀愀氀甀攀怀 椀猀 愀渀 愀爀爀愀礀Ⰰ 椀琀 眀椀氀氀 猀攀琀 琀栀攀 瀀愀爀愀洀攀琀攀爀 愀猀 愀ഀഀ
   * comma-separated value. If `paramValue` is `null`, the parameter will be deleted.਍   ⨀ഀഀ
   * @return {string} search਍   ⨀⼀ഀഀ
  search: function(search, paramValue) {਍    猀眀椀琀挀栀 ⠀愀爀最甀洀攀渀琀猀⸀氀攀渀最琀栀⤀ 笀ഀഀ
      case 0:਍        爀攀琀甀爀渀 琀栀椀猀⸀␀␀猀攀愀爀挀栀㬀ഀഀ
      case 1:਍        椀昀 ⠀椀猀匀琀爀椀渀最⠀猀攀愀爀挀栀⤀⤀ 笀ഀഀ
          this.$$search = parseKeyValue(search);਍        紀 攀氀猀攀 椀昀 ⠀椀猀伀戀樀攀挀琀⠀猀攀愀爀挀栀⤀⤀ 笀ഀഀ
          this.$$search = search;਍        紀 攀氀猀攀 笀ഀഀ
          throw $locationMinErr('isrcharg',਍              ✀吀栀攀 昀椀爀猀琀 愀爀最甀洀攀渀琀 漀昀 琀栀攀 怀␀氀漀挀愀琀椀漀渀⌀猀攀愀爀挀栀⠀⤀怀 挀愀氀氀 洀甀猀琀 戀攀 愀 猀琀爀椀渀最 漀爀 愀渀 漀戀樀攀挀琀⸀✀⤀㬀ഀഀ
        }਍        戀爀攀愀欀㬀ഀഀ
      default:਍        椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀瀀愀爀愀洀嘀愀氀甀攀⤀ 簀簀 瀀愀爀愀洀嘀愀氀甀攀 㴀㴀㴀 渀甀氀氀⤀ 笀ഀഀ
          delete this.$$search[search];਍        紀 攀氀猀攀 笀ഀഀ
          this.$$search[search] = paramValue;਍        紀ഀഀ
    }਍ഀഀ
    this.$$compose();਍    爀攀琀甀爀渀 琀栀椀猀㬀ഀഀ
  },਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
   * @name ng.$location#hash਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀氀漀挀愀琀椀漀渀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * This method is getter / setter.਍   ⨀ഀഀ
   * Return hash fragment when called without any parameter.਍   ⨀ഀഀ
   * Change hash fragment when called with parameter and return `$location`.਍   ⨀ഀഀ
   * @param {string=} hash New hash fragment਍   ⨀ 䀀爀攀琀甀爀渀 笀猀琀爀椀渀最紀 栀愀猀栀ഀഀ
   */਍  栀愀猀栀㨀 氀漀挀愀琀椀漀渀䜀攀琀琀攀爀匀攀琀琀攀爀⠀✀␀␀栀愀猀栀✀Ⰰ 椀搀攀渀琀椀琀礀⤀Ⰰഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc method਍   ⨀ 䀀渀愀洀攀 渀最⸀␀氀漀挀愀琀椀漀渀⌀爀攀瀀氀愀挀攀ഀഀ
   * @methodOf ng.$location਍   ⨀ഀഀ
   * @description਍   ⨀ 䤀昀 挀愀氀氀攀搀Ⰰ 愀氀氀 挀栀愀渀最攀猀 琀漀 ␀氀漀挀愀琀椀漀渀 搀甀爀椀渀最 挀甀爀爀攀渀琀 怀␀搀椀最攀猀琀怀 眀椀氀氀 戀攀 爀攀瀀氀愀挀椀渀最 挀甀爀爀攀渀琀 栀椀猀琀漀爀礀ഀഀ
   * record, instead of adding new one.਍   ⨀⼀ഀഀ
  replace: function() {਍    琀栀椀猀⸀␀␀爀攀瀀氀愀挀攀 㴀 琀爀甀攀㬀ഀഀ
    return this;਍  紀ഀഀ
};਍ഀഀ
function locationGetter(property) {਍  爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    return this[property];਍  紀㬀ഀഀ
}਍ഀഀ
਍昀甀渀挀琀椀漀渀 氀漀挀愀琀椀漀渀䜀攀琀琀攀爀匀攀琀琀攀爀⠀瀀爀漀瀀攀爀琀礀Ⰰ 瀀爀攀瀀爀漀挀攀猀猀⤀ 笀ഀഀ
  return function(value) {਍    椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀瘀愀氀甀攀⤀⤀ഀഀ
      return this[property];਍ഀഀ
    this[property] = preprocess(value);਍    琀栀椀猀⸀␀␀挀漀洀瀀漀猀攀⠀⤀㬀ഀഀ
਍    爀攀琀甀爀渀 琀栀椀猀㬀ഀഀ
  };਍紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name ng.$location਍ ⨀ഀഀ
 * @requires $browser਍ ⨀ 䀀爀攀焀甀椀爀攀猀 ␀猀渀椀昀昀攀爀ഀഀ
 * @requires $rootElement਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀攀 ␀氀漀挀愀琀椀漀渀 猀攀爀瘀椀挀攀 瀀愀爀猀攀猀 琀栀攀 唀刀䰀 椀渀 琀栀攀 戀爀漀眀猀攀爀 愀搀搀爀攀猀猀 戀愀爀 ⠀戀愀猀攀搀 漀渀 琀栀攀ഀഀ
 * {@link https://developer.mozilla.org/en/window.location window.location}) and makes the URL਍ ⨀ 愀瘀愀椀氀愀戀氀攀 琀漀 礀漀甀爀 愀瀀瀀氀椀挀愀琀椀漀渀⸀ 䌀栀愀渀最攀猀 琀漀 琀栀攀 唀刀䰀 椀渀 琀栀攀 愀搀搀爀攀猀猀 戀愀爀 愀爀攀 爀攀昀氀攀挀琀攀搀 椀渀琀漀ഀഀ
 * $location service and changes to $location are reflected into the browser address bar.਍ ⨀ഀഀ
 * **The $location service:**਍ ⨀ഀഀ
 * - Exposes the current URL in the browser address bar, so you can਍ ⨀   ⴀ 圀愀琀挀栀 愀渀搀 漀戀猀攀爀瘀攀 琀栀攀 唀刀䰀⸀ഀഀ
 *   - Change the URL.਍ ⨀ ⴀ 匀礀渀挀栀爀漀渀椀稀攀猀 琀栀攀 唀刀䰀 眀椀琀栀 琀栀攀 戀爀漀眀猀攀爀 眀栀攀渀 琀栀攀 甀猀攀爀ഀഀ
 *   - Changes the address bar.਍ ⨀   ⴀ 䌀氀椀挀欀猀 琀栀攀 戀愀挀欀 漀爀 昀漀爀眀愀爀搀 戀甀琀琀漀渀 ⠀漀爀 挀氀椀挀欀猀 愀 䠀椀猀琀漀爀礀 氀椀渀欀⤀⸀ഀഀ
 *   - Clicks on a link.਍ ⨀ ⴀ 刀攀瀀爀攀猀攀渀琀猀 琀栀攀 唀刀䰀 漀戀樀攀挀琀 愀猀 愀 猀攀琀 漀昀 洀攀琀栀漀搀猀 ⠀瀀爀漀琀漀挀漀氀Ⰰ 栀漀猀琀Ⰰ 瀀漀爀琀Ⰰ 瀀愀琀栀Ⰰ 猀攀愀爀挀栀Ⰰ 栀愀猀栀⤀⸀ഀഀ
 *਍ ⨀ 䘀漀爀 洀漀爀攀 椀渀昀漀爀洀愀琀椀漀渀 猀攀攀 笀䀀氀椀渀欀 最甀椀搀攀⼀搀攀瘀开最甀椀搀攀⸀猀攀爀瘀椀挀攀猀⸀␀氀漀挀愀琀椀漀渀 䐀攀瘀攀氀漀瀀攀爀 䜀甀椀搀攀㨀 䄀渀最甀氀愀爀ഀഀ
 * Services: Using $location}਍ ⨀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc object਍ ⨀ 䀀渀愀洀攀 渀最⸀␀氀漀挀愀琀椀漀渀倀爀漀瘀椀搀攀爀ഀഀ
 * @description਍ ⨀ 唀猀攀 琀栀攀 怀␀氀漀挀愀琀椀漀渀倀爀漀瘀椀搀攀爀怀 琀漀 挀漀渀昀椀最甀爀攀 栀漀眀 琀栀攀 愀瀀瀀氀椀挀愀琀椀漀渀 搀攀攀瀀 氀椀渀欀椀渀最 瀀愀琀栀猀 愀爀攀 猀琀漀爀攀搀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 ␀䰀漀挀愀琀椀漀渀倀爀漀瘀椀搀攀爀⠀⤀笀ഀഀ
  var hashPrefix = '',਍      栀琀洀氀㔀䴀漀搀攀 㴀 昀愀氀猀攀㬀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc property਍   ⨀ 䀀渀愀洀攀 渀最⸀␀氀漀挀愀琀椀漀渀倀爀漀瘀椀搀攀爀⌀栀愀猀栀倀爀攀昀椀砀ഀഀ
   * @methodOf ng.$locationProvider਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * @param {string=} prefix Prefix for hash part (containing path and search)਍   ⨀ 䀀爀攀琀甀爀渀猀 笀⨀紀 挀甀爀爀攀渀琀 瘀愀氀甀攀 椀昀 甀猀攀搀 愀猀 最攀琀琀攀爀 漀爀 椀琀猀攀氀昀 ⠀挀栀愀椀渀椀渀最⤀ 椀昀 甀猀攀搀 愀猀 猀攀琀琀攀爀ഀഀ
   */਍  琀栀椀猀⸀栀愀猀栀倀爀攀昀椀砀 㴀 昀甀渀挀琀椀漀渀⠀瀀爀攀昀椀砀⤀ 笀ഀഀ
    if (isDefined(prefix)) {਍      栀愀猀栀倀爀攀昀椀砀 㴀 瀀爀攀昀椀砀㬀ഀഀ
      return this;਍    紀 攀氀猀攀 笀ഀഀ
      return hashPrefix;਍    紀ഀഀ
  };਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 瀀爀漀瀀攀爀琀礀ഀഀ
   * @name ng.$locationProvider#html5Mode਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀氀漀挀愀琀椀漀渀倀爀漀瘀椀搀攀爀ഀഀ
   * @description਍   ⨀ 䀀瀀愀爀愀洀 笀戀漀漀氀攀愀渀㴀紀 洀漀搀攀 唀猀攀 䠀吀䴀䰀㔀 猀琀爀愀琀攀最礀 椀昀 愀瘀愀椀氀愀戀氀攀⸀ഀഀ
   * @returns {*} current value if used as getter or itself (chaining) if used as setter਍   ⨀⼀ഀഀ
  this.html5Mode = function(mode) {਍    椀昀 ⠀椀猀䐀攀昀椀渀攀搀⠀洀漀搀攀⤀⤀ 笀ഀഀ
      html5Mode = mode;਍      爀攀琀甀爀渀 琀栀椀猀㬀ഀഀ
    } else {਍      爀攀琀甀爀渀 栀琀洀氀㔀䴀漀搀攀㬀ഀഀ
    }਍  紀㬀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc event਍   ⨀ 䀀渀愀洀攀 渀最⸀␀氀漀挀愀琀椀漀渀⌀␀氀漀挀愀琀椀漀渀䌀栀愀渀最攀匀琀愀爀琀ഀഀ
   * @eventOf ng.$location਍   ⨀ 䀀攀瘀攀渀琀吀礀瀀攀 戀爀漀愀搀挀愀猀琀 漀渀 爀漀漀琀 猀挀漀瀀攀ഀഀ
   * @description਍   ⨀ 䈀爀漀愀搀挀愀猀琀攀搀 戀攀昀漀爀攀 愀 唀刀䰀 眀椀氀氀 挀栀愀渀最攀⸀ 吀栀椀猀 挀栀愀渀最攀 挀愀渀 戀攀 瀀爀攀瘀攀渀琀攀搀 戀礀 挀愀氀氀椀渀最ഀഀ
   * `preventDefault` method of the event. See {@link ng.$rootScope.Scope#$on} for more਍   ⨀ 搀攀琀愀椀氀猀 愀戀漀甀琀 攀瘀攀渀琀 漀戀樀攀挀琀⸀ 唀瀀漀渀 猀甀挀挀攀猀猀昀甀氀 挀栀愀渀最攀ഀഀ
   * {@link ng.$location#events_$locationChangeSuccess $locationChangeSuccess} is fired.਍   ⨀ഀഀ
   * @param {Object} angularEvent Synthetic event object.਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀攀眀唀爀氀 一攀眀 唀刀䰀ഀഀ
   * @param {string=} oldUrl URL that was before it was changed.਍   ⨀⼀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc event਍   ⨀ 䀀渀愀洀攀 渀最⸀␀氀漀挀愀琀椀漀渀⌀␀氀漀挀愀琀椀漀渀䌀栀愀渀最攀匀甀挀挀攀猀猀ഀഀ
   * @eventOf ng.$location਍   ⨀ 䀀攀瘀攀渀琀吀礀瀀攀 戀爀漀愀搀挀愀猀琀 漀渀 爀漀漀琀 猀挀漀瀀攀ഀഀ
   * @description਍   ⨀ 䈀爀漀愀搀挀愀猀琀攀搀 愀昀琀攀爀 愀 唀刀䰀 眀愀猀 挀栀愀渀最攀搀⸀ഀഀ
   *਍   ⨀ 䀀瀀愀爀愀洀 笀伀戀樀攀挀琀紀 愀渀最甀氀愀爀䔀瘀攀渀琀 匀礀渀琀栀攀琀椀挀 攀瘀攀渀琀 漀戀樀攀挀琀⸀ഀഀ
   * @param {string} newUrl New URL਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 漀氀搀唀爀氀 唀刀䰀 琀栀愀琀 眀愀猀 戀攀昀漀爀攀 椀琀 眀愀猀 挀栀愀渀最攀搀⸀ഀഀ
   */਍ഀഀ
  this.$get = ['$rootScope', '$browser', '$sniffer', '$rootElement',਍      昀甀渀挀琀椀漀渀⠀ ␀爀漀漀琀匀挀漀瀀攀Ⰰ   ␀戀爀漀眀猀攀爀Ⰰ   ␀猀渀椀昀昀攀爀Ⰰ   ␀爀漀漀琀䔀氀攀洀攀渀琀⤀ 笀ഀഀ
    var $location,਍        䰀漀挀愀琀椀漀渀䴀漀搀攀Ⰰഀഀ
        baseHref = $browser.baseHref(), // if base[href] is undefined, it defaults to ''਍        椀渀椀琀椀愀氀唀爀氀 㴀 ␀戀爀漀眀猀攀爀⸀甀爀氀⠀⤀Ⰰഀഀ
        appBase;਍ഀഀ
    if (html5Mode) {਍      愀瀀瀀䈀愀猀攀 㴀 猀攀爀瘀攀爀䈀愀猀攀⠀椀渀椀琀椀愀氀唀爀氀⤀ ⬀ ⠀戀愀猀攀䠀爀攀昀 簀簀 ✀⼀✀⤀㬀ഀഀ
      LocationMode = $sniffer.history ? LocationHtml5Url : LocationHashbangInHtml5Url;਍    紀 攀氀猀攀 笀ഀഀ
      appBase = stripHash(initialUrl);਍      䰀漀挀愀琀椀漀渀䴀漀搀攀 㴀 䰀漀挀愀琀椀漀渀䠀愀猀栀戀愀渀最唀爀氀㬀ഀഀ
    }਍    ␀氀漀挀愀琀椀漀渀 㴀 渀攀眀 䰀漀挀愀琀椀漀渀䴀漀搀攀⠀愀瀀瀀䈀愀猀攀Ⰰ ✀⌀✀ ⬀ 栀愀猀栀倀爀攀昀椀砀⤀㬀ഀഀ
    $location.$$parse($location.$$rewrite(initialUrl));਍ഀഀ
    $rootElement.on('click', function(event) {਍      ⼀⼀ 吀伀䐀伀⠀瘀漀樀琀愀⤀㨀 爀攀眀爀椀琀攀 氀椀渀欀 眀栀攀渀 漀瀀攀渀椀渀最 椀渀 渀攀眀 琀愀戀⼀眀椀渀搀漀眀 ⠀椀渀 氀攀最愀挀礀 戀爀漀眀猀攀爀⤀ഀഀ
      // currently we open nice url link and redirect then਍ഀഀ
      if (event.ctrlKey || event.metaKey || event.which == 2) return;਍ഀഀ
      var elm = jqLite(event.target);਍ഀഀ
      // traverse the DOM up to find first A tag਍      眀栀椀氀攀 ⠀氀漀眀攀爀挀愀猀攀⠀攀氀洀嬀　崀⸀渀漀搀攀一愀洀攀⤀ ℀㴀㴀 ✀愀✀⤀ 笀ഀഀ
        // ignore rewriting if no A tag (reached root element, or no parent - removed from document)਍        椀昀 ⠀攀氀洀嬀　崀 㴀㴀㴀 ␀爀漀漀琀䔀氀攀洀攀渀琀嬀　崀 簀簀 ℀⠀攀氀洀 㴀 攀氀洀⸀瀀愀爀攀渀琀⠀⤀⤀嬀　崀⤀ 爀攀琀甀爀渀㬀ഀഀ
      }਍ഀഀ
      var absHref = elm.prop('href');਍ഀഀ
      if (isObject(absHref) && absHref.toString() === '[object SVGAnimatedString]') {਍        ⼀⼀ 匀嘀䜀䄀渀椀洀愀琀攀搀匀琀爀椀渀最⸀愀渀椀洀嘀愀氀 猀栀漀甀氀搀 戀攀 椀搀攀渀琀椀挀愀氀 琀漀 匀嘀䜀䄀渀椀洀愀琀攀搀匀琀爀椀渀最⸀戀愀猀攀嘀愀氀Ⰰ 甀渀氀攀猀猀 搀甀爀椀渀最ഀഀ
        // an animation.਍        愀戀猀䠀爀攀昀 㴀 甀爀氀刀攀猀漀氀瘀攀⠀愀戀猀䠀爀攀昀⸀愀渀椀洀嘀愀氀⤀⸀栀爀攀昀㬀ഀഀ
      }਍ഀഀ
      var rewrittenUrl = $location.$$rewrite(absHref);਍ഀഀ
      if (absHref && !elm.attr('target') && rewrittenUrl && !event.isDefaultPrevented()) {਍        攀瘀攀渀琀⸀瀀爀攀瘀攀渀琀䐀攀昀愀甀氀琀⠀⤀㬀ഀഀ
        if (rewrittenUrl != $browser.url()) {਍          ⼀⼀ 甀瀀搀愀琀攀 氀漀挀愀琀椀漀渀 洀愀渀甀愀氀氀礀ഀഀ
          $location.$$parse(rewrittenUrl);਍          ␀爀漀漀琀匀挀漀瀀攀⸀␀愀瀀瀀氀礀⠀⤀㬀ഀഀ
          // hack to work around FF6 bug 684208 when scenario runner clicks on links਍          眀椀渀搀漀眀⸀愀渀最甀氀愀爀嬀✀昀昀ⴀ㘀㠀㐀㈀　㠀ⴀ瀀爀攀瘀攀渀琀䐀攀昀愀甀氀琀✀崀 㴀 琀爀甀攀㬀ഀഀ
        }਍      紀ഀഀ
    });਍ഀഀ
਍    ⼀⼀ 爀攀眀爀椀琀攀 栀愀猀栀戀愀渀最 甀爀氀 㰀㸀 栀琀洀氀㔀 甀爀氀ഀഀ
    if ($location.absUrl() != initialUrl) {਍      ␀戀爀漀眀猀攀爀⸀甀爀氀⠀␀氀漀挀愀琀椀漀渀⸀愀戀猀唀爀氀⠀⤀Ⰰ 琀爀甀攀⤀㬀ഀഀ
    }਍ഀഀ
    // update $location when $browser url changes਍    ␀戀爀漀眀猀攀爀⸀漀渀唀爀氀䌀栀愀渀最攀⠀昀甀渀挀琀椀漀渀⠀渀攀眀唀爀氀⤀ 笀ഀഀ
      if ($location.absUrl() != newUrl) {਍        ␀爀漀漀琀匀挀漀瀀攀⸀␀攀瘀愀氀䄀猀礀渀挀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          var oldUrl = $location.absUrl();਍ഀഀ
          $location.$$parse(newUrl);਍          椀昀 ⠀␀爀漀漀琀匀挀漀瀀攀⸀␀戀爀漀愀搀挀愀猀琀⠀✀␀氀漀挀愀琀椀漀渀䌀栀愀渀最攀匀琀愀爀琀✀Ⰰ 渀攀眀唀爀氀Ⰰഀഀ
                                    oldUrl).defaultPrevented) {਍            ␀氀漀挀愀琀椀漀渀⸀␀␀瀀愀爀猀攀⠀漀氀搀唀爀氀⤀㬀ഀഀ
            $browser.url(oldUrl);਍          紀 攀氀猀攀 笀ഀഀ
            afterLocationChange(oldUrl);਍          紀ഀഀ
        });਍        椀昀 ⠀℀␀爀漀漀琀匀挀漀瀀攀⸀␀␀瀀栀愀猀攀⤀ ␀爀漀漀琀匀挀漀瀀攀⸀␀搀椀最攀猀琀⠀⤀㬀ഀഀ
      }਍    紀⤀㬀ഀഀ
਍    ⼀⼀ 甀瀀搀愀琀攀 戀爀漀眀猀攀爀ഀഀ
    var changeCounter = 0;਍    ␀爀漀漀琀匀挀漀瀀攀⸀␀眀愀琀挀栀⠀昀甀渀挀琀椀漀渀 ␀氀漀挀愀琀椀漀渀圀愀琀挀栀⠀⤀ 笀ഀഀ
      var oldUrl = $browser.url();਍      瘀愀爀 挀甀爀爀攀渀琀刀攀瀀氀愀挀攀 㴀 ␀氀漀挀愀琀椀漀渀⸀␀␀爀攀瀀氀愀挀攀㬀ഀഀ
਍      椀昀 ⠀℀挀栀愀渀最攀䌀漀甀渀琀攀爀 簀簀 漀氀搀唀爀氀 ℀㴀 ␀氀漀挀愀琀椀漀渀⸀愀戀猀唀爀氀⠀⤀⤀ 笀ഀഀ
        changeCounter++;਍        ␀爀漀漀琀匀挀漀瀀攀⸀␀攀瘀愀氀䄀猀礀渀挀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          if ($rootScope.$broadcast('$locationChangeStart', $location.absUrl(), oldUrl).਍              搀攀昀愀甀氀琀倀爀攀瘀攀渀琀攀搀⤀ 笀ഀഀ
            $location.$$parse(oldUrl);਍          紀 攀氀猀攀 笀ഀഀ
            $browser.url($location.absUrl(), currentReplace);਍            愀昀琀攀爀䰀漀挀愀琀椀漀渀䌀栀愀渀最攀⠀漀氀搀唀爀氀⤀㬀ഀഀ
          }਍        紀⤀㬀ഀഀ
      }਍      ␀氀漀挀愀琀椀漀渀⸀␀␀爀攀瀀氀愀挀攀 㴀 昀愀氀猀攀㬀ഀഀ
਍      爀攀琀甀爀渀 挀栀愀渀最攀䌀漀甀渀琀攀爀㬀ഀഀ
    });਍ഀഀ
    return $location;਍ഀഀ
    function afterLocationChange(oldUrl) {਍      ␀爀漀漀琀匀挀漀瀀攀⸀␀戀爀漀愀搀挀愀猀琀⠀✀␀氀漀挀愀琀椀漀渀䌀栀愀渀最攀匀甀挀挀攀猀猀✀Ⰰ ␀氀漀挀愀琀椀漀渀⸀愀戀猀唀爀氀⠀⤀Ⰰ 漀氀搀唀爀氀⤀㬀ഀഀ
    }਍紀崀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name ng.$log਍ ⨀ 䀀爀攀焀甀椀爀攀猀 ␀眀椀渀搀漀眀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Simple service for logging. Default implementation safely writes the message਍ ⨀ 椀渀琀漀 琀栀攀 戀爀漀眀猀攀爀✀猀 挀漀渀猀漀氀攀 ⠀椀昀 瀀爀攀猀攀渀琀⤀⸀ഀഀ
 * ਍ ⨀ 吀栀攀 洀愀椀渀 瀀甀爀瀀漀猀攀 漀昀 琀栀椀猀 猀攀爀瘀椀挀攀 椀猀 琀漀 猀椀洀瀀氀椀昀礀 搀攀戀甀最最椀渀最 愀渀搀 琀爀漀甀戀氀攀猀栀漀漀琀椀渀最⸀ഀഀ
 *਍ ⨀ 吀栀攀 搀攀昀愀甀氀琀 椀猀 琀漀 氀漀最 怀搀攀戀甀最怀 洀攀猀猀愀最攀猀⸀ 夀漀甀 挀愀渀 甀猀攀ഀഀ
 * {@link ng.$logProvider ng.$logProvider#debugEnabled} to change this.਍ ⨀ഀഀ
 * @example਍   㰀攀砀愀洀瀀氀攀㸀ഀഀ
     <file name="script.js">਍       昀甀渀挀琀椀漀渀 䰀漀最䌀琀爀氀⠀␀猀挀漀瀀攀Ⰰ ␀氀漀最⤀ 笀ഀഀ
         $scope.$log = $log;਍         ␀猀挀漀瀀攀⸀洀攀猀猀愀最攀 㴀 ✀䠀攀氀氀漀 圀漀爀氀搀℀✀㬀ഀഀ
       }਍     㰀⼀昀椀氀攀㸀ഀഀ
     <file name="index.html">਍       㰀搀椀瘀 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀䰀漀最䌀琀爀氀∀㸀ഀഀ
         <p>Reload this page with open console, enter text and hit the log button...</p>਍         䴀攀猀猀愀最攀㨀ഀഀ
         <input type="text" ng-model="message"/>਍         㰀戀甀琀琀漀渀 渀最ⴀ挀氀椀挀欀㴀∀␀氀漀最⸀氀漀最⠀洀攀猀猀愀最攀⤀∀㸀氀漀最㰀⼀戀甀琀琀漀渀㸀ഀഀ
         <button ng-click="$log.warn(message)">warn</button>਍         㰀戀甀琀琀漀渀 渀最ⴀ挀氀椀挀欀㴀∀␀氀漀最⸀椀渀昀漀⠀洀攀猀猀愀最攀⤀∀㸀椀渀昀漀㰀⼀戀甀琀琀漀渀㸀ഀഀ
         <button ng-click="$log.error(message)">error</button>਍       㰀⼀搀椀瘀㸀ഀഀ
     </file>਍   㰀⼀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name ng.$logProvider਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Use the `$logProvider` to configure how the application logs messages਍ ⨀⼀ഀഀ
function $LogProvider(){਍  瘀愀爀 搀攀戀甀最 㴀 琀爀甀攀Ⰰഀഀ
      self = this;਍  ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 瀀爀漀瀀攀爀琀礀ഀഀ
   * @name ng.$logProvider#debugEnabled਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀氀漀最倀爀漀瘀椀搀攀爀ഀഀ
   * @description਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 昀氀愀最 攀渀愀戀氀攀 漀爀 搀椀猀愀戀氀攀 搀攀戀甀最 氀攀瘀攀氀 洀攀猀猀愀最攀猀ഀഀ
   * @returns {*} current value if used as getter or itself (chaining) if used as setter਍   ⨀⼀ഀഀ
  this.debugEnabled = function(flag) {਍    椀昀 ⠀椀猀䐀攀昀椀渀攀搀⠀昀氀愀最⤀⤀ 笀ഀഀ
      debug = flag;਍    爀攀琀甀爀渀 琀栀椀猀㬀ഀഀ
    } else {਍      爀攀琀甀爀渀 搀攀戀甀最㬀ഀഀ
    }਍  紀㬀ഀഀ
  ਍  琀栀椀猀⸀␀最攀琀 㴀 嬀✀␀眀椀渀搀漀眀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀眀椀渀搀漀眀⤀笀ഀഀ
    return {਍      ⼀⨀⨀ഀഀ
       * @ngdoc method਍       ⨀ 䀀渀愀洀攀 渀最⸀␀氀漀最⌀氀漀最ഀഀ
       * @methodOf ng.$log਍       ⨀ഀഀ
       * @description਍       ⨀ 圀爀椀琀攀 愀 氀漀最 洀攀猀猀愀最攀ഀഀ
       */਍      氀漀最㨀 挀漀渀猀漀氀攀䰀漀最⠀✀氀漀最✀⤀Ⰰഀഀ
਍      ⼀⨀⨀ഀഀ
       * @ngdoc method਍       ⨀ 䀀渀愀洀攀 渀最⸀␀氀漀最⌀椀渀昀漀ഀഀ
       * @methodOf ng.$log਍       ⨀ഀഀ
       * @description਍       ⨀ 圀爀椀琀攀 愀渀 椀渀昀漀爀洀愀琀椀漀渀 洀攀猀猀愀最攀ഀഀ
       */਍      椀渀昀漀㨀 挀漀渀猀漀氀攀䰀漀最⠀✀椀渀昀漀✀⤀Ⰰഀഀ
਍      ⼀⨀⨀ഀഀ
       * @ngdoc method਍       ⨀ 䀀渀愀洀攀 渀最⸀␀氀漀最⌀眀愀爀渀ഀഀ
       * @methodOf ng.$log਍       ⨀ഀഀ
       * @description਍       ⨀ 圀爀椀琀攀 愀 眀愀爀渀椀渀最 洀攀猀猀愀最攀ഀഀ
       */਍      眀愀爀渀㨀 挀漀渀猀漀氀攀䰀漀最⠀✀眀愀爀渀✀⤀Ⰰഀഀ
਍      ⼀⨀⨀ഀഀ
       * @ngdoc method਍       ⨀ 䀀渀愀洀攀 渀最⸀␀氀漀最⌀攀爀爀漀爀ഀഀ
       * @methodOf ng.$log਍       ⨀ഀഀ
       * @description਍       ⨀ 圀爀椀琀攀 愀渀 攀爀爀漀爀 洀攀猀猀愀最攀ഀഀ
       */਍      攀爀爀漀爀㨀 挀漀渀猀漀氀攀䰀漀最⠀✀攀爀爀漀爀✀⤀Ⰰഀഀ
      ਍      ⼀⨀⨀ഀഀ
       * @ngdoc method਍       ⨀ 䀀渀愀洀攀 渀最⸀␀氀漀最⌀搀攀戀甀最ഀഀ
       * @methodOf ng.$log਍       ⨀ ഀഀ
       * @description਍       ⨀ 圀爀椀琀攀 愀 搀攀戀甀最 洀攀猀猀愀最攀ഀഀ
       */਍      搀攀戀甀最㨀 ⠀昀甀渀挀琀椀漀渀 ⠀⤀ 笀ഀഀ
        var fn = consoleLog('debug');਍ഀഀ
        return function() {਍          椀昀 ⠀搀攀戀甀最⤀ 笀ഀഀ
            fn.apply(self, arguments);਍          紀ഀഀ
        };਍      紀⠀⤀⤀ഀഀ
    };਍ഀഀ
    function formatError(arg) {਍      椀昀 ⠀愀爀最 椀渀猀琀愀渀挀攀漀昀 䔀爀爀漀爀⤀ 笀ഀഀ
        if (arg.stack) {਍          愀爀最 㴀 ⠀愀爀最⸀洀攀猀猀愀最攀 ☀☀ 愀爀最⸀猀琀愀挀欀⸀椀渀搀攀砀伀昀⠀愀爀最⸀洀攀猀猀愀最攀⤀ 㴀㴀㴀 ⴀ㄀⤀ഀഀ
              ? 'Error: ' + arg.message + '\n' + arg.stack਍              㨀 愀爀最⸀猀琀愀挀欀㬀ഀഀ
        } else if (arg.sourceURL) {਍          愀爀最 㴀 愀爀最⸀洀攀猀猀愀最攀 ⬀ ✀尀渀✀ ⬀ 愀爀最⸀猀漀甀爀挀攀唀刀䰀 ⬀ ✀㨀✀ ⬀ 愀爀最⸀氀椀渀攀㬀ഀഀ
        }਍      紀ഀഀ
      return arg;਍    紀ഀഀ
਍    昀甀渀挀琀椀漀渀 挀漀渀猀漀氀攀䰀漀最⠀琀礀瀀攀⤀ 笀ഀഀ
      var console = $window.console || {},਍          氀漀最䘀渀 㴀 挀漀渀猀漀氀攀嬀琀礀瀀攀崀 簀簀 挀漀渀猀漀氀攀⸀氀漀最 簀簀 渀漀漀瀀Ⰰഀഀ
          hasApply = false;਍ഀഀ
      // Note: reading logFn.apply throws an error in IE11 in IE8 document mode.਍      ⼀⼀ 吀栀攀 爀攀愀猀漀渀 戀攀栀椀渀搀 琀栀椀猀 椀猀 琀栀愀琀 挀漀渀猀漀氀攀⸀氀漀最 栀愀猀 琀礀瀀攀 ∀漀戀樀攀挀琀∀ 椀渀 䤀䔀㠀⸀⸀⸀ഀഀ
      try {਍        栀愀猀䄀瀀瀀氀礀 㴀 ℀℀ 氀漀最䘀渀⸀愀瀀瀀氀礀㬀ഀഀ
      } catch (e) {}਍ഀഀ
      if (hasApply) {਍        爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          var args = [];਍          昀漀爀䔀愀挀栀⠀愀爀最甀洀攀渀琀猀Ⰰ 昀甀渀挀琀椀漀渀⠀愀爀最⤀ 笀ഀഀ
            args.push(formatError(arg));਍          紀⤀㬀ഀഀ
          return logFn.apply(console, args);਍        紀㬀ഀഀ
      }਍ഀഀ
      // we are IE which either doesn't have window.console => this is noop and we do nothing,਍      ⼀⼀ 漀爀 眀攀 愀爀攀 䤀䔀 眀栀攀爀攀 挀漀渀猀漀氀攀⸀氀漀最 搀漀攀猀渀✀琀 栀愀瘀攀 愀瀀瀀氀礀 猀漀 眀攀 氀漀最 愀琀 氀攀愀猀琀 昀椀爀猀琀 ㈀ 愀爀最猀ഀഀ
      return function(arg1, arg2) {਍        氀漀最䘀渀⠀愀爀最㄀Ⰰ 愀爀最㈀ 㴀㴀 渀甀氀氀 㼀 ✀✀ 㨀 愀爀最㈀⤀㬀ഀഀ
      };਍    紀ഀഀ
  }];਍紀ഀഀ
਍瘀愀爀 ␀瀀愀爀猀攀䴀椀渀䔀爀爀 㴀 洀椀渀䔀爀爀⠀✀␀瀀愀爀猀攀✀⤀㬀ഀഀ
var promiseWarningCache = {};਍瘀愀爀 瀀爀漀洀椀猀攀圀愀爀渀椀渀最㬀ഀഀ
਍⼀⼀ 匀愀渀搀戀漀砀椀渀最 䄀渀最甀氀愀爀 䔀砀瀀爀攀猀猀椀漀渀猀ഀഀ
// ------------------------------਍⼀⼀ 䄀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀猀 愀爀攀 最攀渀攀爀愀氀氀礀 挀漀渀猀椀搀攀爀攀搀 猀愀昀攀 戀攀挀愀甀猀攀 琀栀攀猀攀 攀砀瀀爀攀猀猀椀漀渀猀 漀渀氀礀 栀愀瘀攀 搀椀爀攀挀琀ഀഀ
// access to $scope and locals. However, one can obtain the ability to execute arbitrary JS code by਍⼀⼀ 漀戀琀愀椀渀椀渀最 愀 爀攀昀攀爀攀渀挀攀 琀漀 渀愀琀椀瘀攀 䨀匀 昀甀渀挀琀椀漀渀猀 猀甀挀栀 愀猀 琀栀攀 䘀甀渀挀琀椀漀渀 挀漀渀猀琀爀甀挀琀漀爀⸀ഀഀ
//਍⼀⼀ 䄀猀 愀渀 攀砀愀洀瀀氀攀Ⰰ 挀漀渀猀椀搀攀爀 琀栀攀 昀漀氀氀漀眀椀渀最 䄀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀㨀ഀഀ
//਍⼀⼀   笀紀⸀琀漀匀琀爀椀渀最⸀挀漀渀猀琀爀甀挀琀漀爀⠀愀氀攀爀琀⠀∀攀瘀椀氀 䨀匀 挀漀搀攀∀⤀⤀ഀഀ
//਍⼀⼀ 圀攀 眀愀渀琀 琀漀 瀀爀攀瘀攀渀琀 琀栀椀猀 琀礀瀀攀 漀昀 愀挀挀攀猀猀⸀ 䘀漀爀 琀栀攀 猀愀欀攀 漀昀 瀀攀爀昀漀爀洀愀渀挀攀Ⰰ 搀甀爀椀渀最 琀栀攀 氀攀砀椀渀最 瀀栀愀猀攀 眀攀ഀഀ
// disallow any "dotted" access to any member named "constructor".਍⼀⼀ഀഀ
// For reflective calls (a[b]) we check that the value of the lookup is not the Function constructor਍⼀⼀ 眀栀椀氀攀 攀瘀愀氀甀愀琀椀渀最 琀栀攀 攀砀瀀爀攀猀猀椀漀渀Ⰰ 眀栀椀挀栀 椀猀 愀 猀琀爀漀渀最攀爀 戀甀琀 洀漀爀攀 攀砀瀀攀渀猀椀瘀攀 琀攀猀琀⸀ 匀椀渀挀攀 爀攀昀氀攀挀琀椀瘀攀ഀഀ
// calls are expensive anyway, this is not such a big deal compared to static dereferencing.਍⼀⼀ഀഀ
// This sandboxing technique is not perfect and doesn't aim to be. The goal is to prevent exploits਍⼀⼀ 愀最愀椀渀猀琀 琀栀攀 攀砀瀀爀攀猀猀椀漀渀 氀愀渀最甀愀最攀Ⰰ 戀甀琀 渀漀琀 琀漀 瀀爀攀瘀攀渀琀 攀砀瀀氀漀椀琀猀 琀栀愀琀 眀攀爀攀 攀渀愀戀氀攀搀 戀礀 攀砀瀀漀猀椀渀最ഀഀ
// sensitive JavaScript or browser apis on Scope. Exposing such objects on a Scope is never a good਍⼀⼀ 瀀爀愀挀琀椀挀攀 愀渀搀 琀栀攀爀攀昀漀爀攀 眀攀 愀爀攀 渀漀琀 攀瘀攀渀 琀爀礀椀渀最 琀漀 瀀爀漀琀攀挀琀 愀最愀椀渀猀琀 椀渀琀攀爀愀挀琀椀漀渀 眀椀琀栀 愀渀 漀戀樀攀挀琀ഀഀ
// explicitly exposed in this way.਍⼀⼀ഀഀ
// A developer could foil the name check by aliasing the Function constructor under a different਍⼀⼀ 渀愀洀攀 漀渀 琀栀攀 猀挀漀瀀攀⸀ഀഀ
//਍⼀⼀ 䤀渀 最攀渀攀爀愀氀Ⰰ 椀琀 椀猀 渀漀琀 瀀漀猀猀椀戀氀攀 琀漀 愀挀挀攀猀猀 愀 圀椀渀搀漀眀 漀戀樀攀挀琀 昀爀漀洀 愀渀 愀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 甀渀氀攀猀猀 愀ഀഀ
// window or some DOM object that has a reference to window is published onto a Scope.਍ഀഀ
function ensureSafeMemberName(name, fullExpression) {਍  椀昀 ⠀渀愀洀攀 㴀㴀㴀 ∀挀漀渀猀琀爀甀挀琀漀爀∀⤀ 笀ഀഀ
    throw $parseMinErr('isecfld',਍        ✀刀攀昀攀爀攀渀挀椀渀最 ∀挀漀渀猀琀爀甀挀琀漀爀∀ 昀椀攀氀搀 椀渀 䄀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀猀 椀猀 搀椀猀愀氀氀漀眀攀搀℀ 䔀砀瀀爀攀猀猀椀漀渀㨀 笀　紀✀Ⰰഀഀ
        fullExpression);਍  紀ഀഀ
  return name;਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 攀渀猀甀爀攀匀愀昀攀伀戀樀攀挀琀⠀漀戀樀Ⰰ 昀甀氀氀䔀砀瀀爀攀猀猀椀漀渀⤀ 笀ഀഀ
  // nifty check if obj is Function that is fast and works across iframes and other contexts਍  椀昀 ⠀漀戀樀⤀ 笀ഀഀ
    if (obj.constructor === obj) {਍      琀栀爀漀眀 ␀瀀愀爀猀攀䴀椀渀䔀爀爀⠀✀椀猀攀挀昀渀✀Ⰰഀഀ
          'Referencing Function in Angular expressions is disallowed! Expression: {0}',਍          昀甀氀氀䔀砀瀀爀攀猀猀椀漀渀⤀㬀ഀഀ
    } else if (// isWindow(obj)਍        漀戀樀⸀搀漀挀甀洀攀渀琀 ☀☀ 漀戀樀⸀氀漀挀愀琀椀漀渀 ☀☀ 漀戀樀⸀愀氀攀爀琀 ☀☀ 漀戀樀⸀猀攀琀䤀渀琀攀爀瘀愀氀⤀ 笀ഀഀ
      throw $parseMinErr('isecwindow',਍          ✀刀攀昀攀爀攀渀挀椀渀最 琀栀攀 圀椀渀搀漀眀 椀渀 䄀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀猀 椀猀 搀椀猀愀氀氀漀眀攀搀℀ 䔀砀瀀爀攀猀猀椀漀渀㨀 笀　紀✀Ⰰഀഀ
          fullExpression);਍    紀 攀氀猀攀 椀昀 ⠀⼀⼀ 椀猀䔀氀攀洀攀渀琀⠀漀戀樀⤀ഀഀ
        obj.children && (obj.nodeName || (obj.on && obj.find))) {਍      琀栀爀漀眀 ␀瀀愀爀猀攀䴀椀渀䔀爀爀⠀✀椀猀攀挀搀漀洀✀Ⰰഀഀ
          'Referencing DOM nodes in Angular expressions is disallowed! Expression: {0}',਍          昀甀氀氀䔀砀瀀爀攀猀猀椀漀渀⤀㬀ഀഀ
    }਍  紀ഀഀ
  return obj;਍紀ഀഀ
਍瘀愀爀 伀倀䔀刀䄀吀伀刀匀 㴀 笀ഀഀ
    /* jshint bitwise : false */਍    ✀渀甀氀氀✀㨀昀甀渀挀琀椀漀渀⠀⤀笀爀攀琀甀爀渀 渀甀氀氀㬀紀Ⰰഀഀ
    'true':function(){return true;},਍    ✀昀愀氀猀攀✀㨀昀甀渀挀琀椀漀渀⠀⤀笀爀攀琀甀爀渀 昀愀氀猀攀㬀紀Ⰰഀഀ
    undefined:noop,਍    ✀⬀✀㨀昀甀渀挀琀椀漀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀Ⰰ 愀Ⰰ戀⤀笀ഀഀ
      a=a(self, locals); b=b(self, locals);਍      椀昀 ⠀椀猀䐀攀昀椀渀攀搀⠀愀⤀⤀ 笀ഀഀ
        if (isDefined(b)) {਍          爀攀琀甀爀渀 愀 ⬀ 戀㬀ഀഀ
        }਍        爀攀琀甀爀渀 愀㬀ഀഀ
      }਍      爀攀琀甀爀渀 椀猀䐀攀昀椀渀攀搀⠀戀⤀㼀戀㨀甀渀搀攀昀椀渀攀搀㬀紀Ⰰഀഀ
    '-':function(self, locals, a,b){਍          愀㴀愀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀㬀 戀㴀戀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀㬀ഀഀ
          return (isDefined(a)?a:0)-(isDefined(b)?b:0);਍        紀Ⰰഀഀ
    '*':function(self, locals, a,b){return a(self, locals)*b(self, locals);},਍    ✀⼀✀㨀昀甀渀挀琀椀漀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀Ⰰ 愀Ⰰ戀⤀笀爀攀琀甀爀渀 愀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀⼀戀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀㬀紀Ⰰഀഀ
    '%':function(self, locals, a,b){return a(self, locals)%b(self, locals);},਍    ✀帀✀㨀昀甀渀挀琀椀漀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀Ⰰ 愀Ⰰ戀⤀笀爀攀琀甀爀渀 愀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀帀戀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀㬀紀Ⰰഀഀ
    '=':noop,਍    ✀㴀㴀㴀✀㨀昀甀渀挀琀椀漀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀Ⰰ 愀Ⰰ 戀⤀笀爀攀琀甀爀渀 愀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀㴀㴀㴀戀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀㬀紀Ⰰഀഀ
    '!==':function(self, locals, a, b){return a(self, locals)!==b(self, locals);},਍    ✀㴀㴀✀㨀昀甀渀挀琀椀漀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀Ⰰ 愀Ⰰ戀⤀笀爀攀琀甀爀渀 愀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀㴀㴀戀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀㬀紀Ⰰഀഀ
    '!=':function(self, locals, a,b){return a(self, locals)!=b(self, locals);},਍    ✀㰀✀㨀昀甀渀挀琀椀漀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀Ⰰ 愀Ⰰ戀⤀笀爀攀琀甀爀渀 愀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀㰀戀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀㬀紀Ⰰഀഀ
    '>':function(self, locals, a,b){return a(self, locals)>b(self, locals);},਍    ✀㰀㴀✀㨀昀甀渀挀琀椀漀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀Ⰰ 愀Ⰰ戀⤀笀爀攀琀甀爀渀 愀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀㰀㴀戀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀㬀紀Ⰰഀഀ
    '>=':function(self, locals, a,b){return a(self, locals)>=b(self, locals);},਍    ✀☀☀✀㨀昀甀渀挀琀椀漀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀Ⰰ 愀Ⰰ戀⤀笀爀攀琀甀爀渀 愀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀☀☀戀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀㬀紀Ⰰഀഀ
    '||':function(self, locals, a,b){return a(self, locals)||b(self, locals);},਍    ✀☀✀㨀昀甀渀挀琀椀漀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀Ⰰ 愀Ⰰ戀⤀笀爀攀琀甀爀渀 愀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀☀戀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀㬀紀Ⰰഀഀ
//    '|':function(self, locals, a,b){return a|b;},਍    ✀簀✀㨀昀甀渀挀琀椀漀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀Ⰰ 愀Ⰰ戀⤀笀爀攀琀甀爀渀 戀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀Ⰰ 愀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀⤀㬀紀Ⰰഀഀ
    '!':function(self, locals, a){return !a(self, locals);}਍紀㬀ഀഀ
/* jshint bitwise: true */਍瘀愀爀 䔀匀䌀䄀倀䔀 㴀 笀∀渀∀㨀∀尀渀∀Ⰰ ∀昀∀㨀∀尀昀∀Ⰰ ∀爀∀㨀∀尀爀∀Ⰰ ∀琀∀㨀∀尀琀∀Ⰰ ∀瘀∀㨀∀尀瘀∀Ⰰ ∀✀∀㨀∀✀∀Ⰰ ✀∀✀㨀✀∀✀紀㬀ഀഀ
਍ഀഀ
/////////////////////////////////////////਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @constructor਍ ⨀⼀ഀഀ
var Lexer = function (options) {਍  琀栀椀猀⸀漀瀀琀椀漀渀猀 㴀 漀瀀琀椀漀渀猀㬀ഀഀ
};਍ഀഀ
Lexer.prototype = {਍  挀漀渀猀琀爀甀挀琀漀爀㨀 䰀攀砀攀爀Ⰰഀഀ
਍  氀攀砀㨀 昀甀渀挀琀椀漀渀 ⠀琀攀砀琀⤀ 笀ഀഀ
    this.text = text;਍ഀഀ
    this.index = 0;਍    琀栀椀猀⸀挀栀 㴀 甀渀搀攀昀椀渀攀搀㬀ഀഀ
    this.lastCh = ':'; // can start regexp਍ഀഀ
    this.tokens = [];਍ഀഀ
    var token;਍    瘀愀爀 樀猀漀渀 㴀 嬀崀㬀ഀഀ
਍    眀栀椀氀攀 ⠀琀栀椀猀⸀椀渀搀攀砀 㰀 琀栀椀猀⸀琀攀砀琀⸀氀攀渀最琀栀⤀ 笀ഀഀ
      this.ch = this.text.charAt(this.index);਍      椀昀 ⠀琀栀椀猀⸀椀猀⠀✀∀尀✀✀⤀⤀ 笀ഀഀ
        this.readString(this.ch);਍      紀 攀氀猀攀 椀昀 ⠀琀栀椀猀⸀椀猀一甀洀戀攀爀⠀琀栀椀猀⸀挀栀⤀ 簀簀 琀栀椀猀⸀椀猀⠀✀⸀✀⤀ ☀☀ 琀栀椀猀⸀椀猀一甀洀戀攀爀⠀琀栀椀猀⸀瀀攀攀欀⠀⤀⤀⤀ 笀ഀഀ
        this.readNumber();਍      紀 攀氀猀攀 椀昀 ⠀琀栀椀猀⸀椀猀䤀搀攀渀琀⠀琀栀椀猀⸀挀栀⤀⤀ 笀ഀഀ
        this.readIdent();਍        ⼀⼀ 椀搀攀渀琀椀昀椀攀爀猀 挀愀渀 漀渀氀礀 戀攀 椀昀 琀栀攀 瀀爀攀挀攀搀椀渀最 挀栀愀爀 眀愀猀 愀 笀 漀爀 Ⰰഀഀ
        if (this.was('{,') && json[0] === '{' &&਍            ⠀琀漀欀攀渀 㴀 琀栀椀猀⸀琀漀欀攀渀猀嬀琀栀椀猀⸀琀漀欀攀渀猀⸀氀攀渀最琀栀 ⴀ ㄀崀⤀⤀ 笀ഀഀ
          token.json = token.text.indexOf('.') === -1;਍        紀ഀഀ
      } else if (this.is('(){}[].,;:?')) {਍        琀栀椀猀⸀琀漀欀攀渀猀⸀瀀甀猀栀⠀笀ഀഀ
          index: this.index,਍          琀攀砀琀㨀 琀栀椀猀⸀挀栀Ⰰഀഀ
          json: (this.was(':[,') && this.is('{[')) || this.is('}]:,')਍        紀⤀㬀ഀഀ
        if (this.is('{[')) json.unshift(this.ch);਍        椀昀 ⠀琀栀椀猀⸀椀猀⠀✀紀崀✀⤀⤀ 樀猀漀渀⸀猀栀椀昀琀⠀⤀㬀ഀഀ
        this.index++;਍      紀 攀氀猀攀 椀昀 ⠀琀栀椀猀⸀椀猀圀栀椀琀攀猀瀀愀挀攀⠀琀栀椀猀⸀挀栀⤀⤀ 笀ഀഀ
        this.index++;਍        挀漀渀琀椀渀甀攀㬀ഀഀ
      } else {਍        瘀愀爀 挀栀㈀ 㴀 琀栀椀猀⸀挀栀 ⬀ 琀栀椀猀⸀瀀攀攀欀⠀⤀㬀ഀഀ
        var ch3 = ch2 + this.peek(2);਍        瘀愀爀 昀渀 㴀 伀倀䔀刀䄀吀伀刀匀嬀琀栀椀猀⸀挀栀崀㬀ഀഀ
        var fn2 = OPERATORS[ch2];਍        瘀愀爀 昀渀㌀ 㴀 伀倀䔀刀䄀吀伀刀匀嬀挀栀㌀崀㬀ഀഀ
        if (fn3) {਍          琀栀椀猀⸀琀漀欀攀渀猀⸀瀀甀猀栀⠀笀椀渀搀攀砀㨀 琀栀椀猀⸀椀渀搀攀砀Ⰰ 琀攀砀琀㨀 挀栀㌀Ⰰ 昀渀㨀 昀渀㌀紀⤀㬀ഀഀ
          this.index += 3;਍        紀 攀氀猀攀 椀昀 ⠀昀渀㈀⤀ 笀ഀഀ
          this.tokens.push({index: this.index, text: ch2, fn: fn2});਍          琀栀椀猀⸀椀渀搀攀砀 ⬀㴀 ㈀㬀ഀഀ
        } else if (fn) {਍          琀栀椀猀⸀琀漀欀攀渀猀⸀瀀甀猀栀⠀笀ഀഀ
            index: this.index,਍            琀攀砀琀㨀 琀栀椀猀⸀挀栀Ⰰഀഀ
            fn: fn,਍            樀猀漀渀㨀 ⠀琀栀椀猀⸀眀愀猀⠀✀嬀Ⰰ㨀✀⤀ ☀☀ 琀栀椀猀⸀椀猀⠀✀⬀ⴀ✀⤀⤀ഀഀ
          });਍          琀栀椀猀⸀椀渀搀攀砀 ⬀㴀 ㄀㬀ഀഀ
        } else {਍          琀栀椀猀⸀琀栀爀漀眀䔀爀爀漀爀⠀✀唀渀攀砀瀀攀挀琀攀搀 渀攀砀琀 挀栀愀爀愀挀琀攀爀 ✀Ⰰ 琀栀椀猀⸀椀渀搀攀砀Ⰰ 琀栀椀猀⸀椀渀搀攀砀 ⬀ ㄀⤀㬀ഀഀ
        }਍      紀ഀഀ
      this.lastCh = this.ch;਍    紀ഀഀ
    return this.tokens;਍  紀Ⰰഀഀ
਍  椀猀㨀 昀甀渀挀琀椀漀渀⠀挀栀愀爀猀⤀ 笀ഀഀ
    return chars.indexOf(this.ch) !== -1;਍  紀Ⰰഀഀ
਍  眀愀猀㨀 昀甀渀挀琀椀漀渀⠀挀栀愀爀猀⤀ 笀ഀഀ
    return chars.indexOf(this.lastCh) !== -1;਍  紀Ⰰഀഀ
਍  瀀攀攀欀㨀 昀甀渀挀琀椀漀渀⠀椀⤀ 笀ഀഀ
    var num = i || 1;਍    爀攀琀甀爀渀 ⠀琀栀椀猀⸀椀渀搀攀砀 ⬀ 渀甀洀 㰀 琀栀椀猀⸀琀攀砀琀⸀氀攀渀最琀栀⤀ 㼀 琀栀椀猀⸀琀攀砀琀⸀挀栀愀爀䄀琀⠀琀栀椀猀⸀椀渀搀攀砀 ⬀ 渀甀洀⤀ 㨀 昀愀氀猀攀㬀ഀഀ
  },਍ഀഀ
  isNumber: function(ch) {਍    爀攀琀甀爀渀 ⠀✀　✀ 㰀㴀 挀栀 ☀☀ 挀栀 㰀㴀 ✀㤀✀⤀㬀ഀഀ
  },਍ഀഀ
  isWhitespace: function(ch) {਍    ⼀⼀ 䤀䔀 琀爀攀愀琀猀 渀漀渀ⴀ戀爀攀愀欀椀渀最 猀瀀愀挀攀 愀猀 尀甀　　䄀　ഀഀ
    return (ch === ' ' || ch === '\r' || ch === '\t' ||਍            挀栀 㴀㴀㴀 ✀尀渀✀ 簀簀 挀栀 㴀㴀㴀 ✀尀瘀✀ 簀簀 挀栀 㴀㴀㴀 ✀尀甀　　䄀　✀⤀㬀ഀഀ
  },਍ഀഀ
  isIdent: function(ch) {਍    爀攀琀甀爀渀 ⠀✀愀✀ 㰀㴀 挀栀 ☀☀ 挀栀 㰀㴀 ✀稀✀ 簀簀ഀഀ
            'A' <= ch && ch <= 'Z' ||਍            ✀开✀ 㴀㴀㴀 挀栀 簀簀 挀栀 㴀㴀㴀 ✀␀✀⤀㬀ഀഀ
  },਍ഀഀ
  isExpOperator: function(ch) {਍    爀攀琀甀爀渀 ⠀挀栀 㴀㴀㴀 ✀ⴀ✀ 簀簀 挀栀 㴀㴀㴀 ✀⬀✀ 簀簀 琀栀椀猀⸀椀猀一甀洀戀攀爀⠀挀栀⤀⤀㬀ഀഀ
  },਍ഀഀ
  throwError: function(error, start, end) {਍    攀渀搀 㴀 攀渀搀 簀簀 琀栀椀猀⸀椀渀搀攀砀㬀ഀഀ
    var colStr = (isDefined(start)਍            㼀 ✀猀 ✀ ⬀ 猀琀愀爀琀 ⬀  ✀ⴀ✀ ⬀ 琀栀椀猀⸀椀渀搀攀砀 ⬀ ✀ 嬀✀ ⬀ 琀栀椀猀⸀琀攀砀琀⸀猀甀戀猀琀爀椀渀最⠀猀琀愀爀琀Ⰰ 攀渀搀⤀ ⬀ ✀崀✀ഀഀ
            : ' ' + end);਍    琀栀爀漀眀 ␀瀀愀爀猀攀䴀椀渀䔀爀爀⠀✀氀攀砀攀爀爀✀Ⰰ ✀䰀攀砀攀爀 䔀爀爀漀爀㨀 笀　紀 愀琀 挀漀氀甀洀渀笀㄀紀 椀渀 攀砀瀀爀攀猀猀椀漀渀 嬀笀㈀紀崀⸀✀Ⰰഀഀ
        error, colStr, this.text);਍  紀Ⰰഀഀ
਍  爀攀愀搀一甀洀戀攀爀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var number = '';਍    瘀愀爀 猀琀愀爀琀 㴀 琀栀椀猀⸀椀渀搀攀砀㬀ഀഀ
    while (this.index < this.text.length) {਍      瘀愀爀 挀栀 㴀 氀漀眀攀爀挀愀猀攀⠀琀栀椀猀⸀琀攀砀琀⸀挀栀愀爀䄀琀⠀琀栀椀猀⸀椀渀搀攀砀⤀⤀㬀ഀഀ
      if (ch == '.' || this.isNumber(ch)) {਍        渀甀洀戀攀爀 ⬀㴀 挀栀㬀ഀഀ
      } else {਍        瘀愀爀 瀀攀攀欀䌀栀 㴀 琀栀椀猀⸀瀀攀攀欀⠀⤀㬀ഀഀ
        if (ch == 'e' && this.isExpOperator(peekCh)) {਍          渀甀洀戀攀爀 ⬀㴀 挀栀㬀ഀഀ
        } else if (this.isExpOperator(ch) &&਍            瀀攀攀欀䌀栀 ☀☀ 琀栀椀猀⸀椀猀一甀洀戀攀爀⠀瀀攀攀欀䌀栀⤀ ☀☀ഀഀ
            number.charAt(number.length - 1) == 'e') {਍          渀甀洀戀攀爀 ⬀㴀 挀栀㬀ഀഀ
        } else if (this.isExpOperator(ch) &&਍            ⠀℀瀀攀攀欀䌀栀 簀簀 ℀琀栀椀猀⸀椀猀一甀洀戀攀爀⠀瀀攀攀欀䌀栀⤀⤀ ☀☀ഀഀ
            number.charAt(number.length - 1) == 'e') {਍          琀栀椀猀⸀琀栀爀漀眀䔀爀爀漀爀⠀✀䤀渀瘀愀氀椀搀 攀砀瀀漀渀攀渀琀✀⤀㬀ഀഀ
        } else {਍          戀爀攀愀欀㬀ഀഀ
        }਍      紀ഀഀ
      this.index++;਍    紀ഀഀ
    number = 1 * number;਍    琀栀椀猀⸀琀漀欀攀渀猀⸀瀀甀猀栀⠀笀ഀഀ
      index: start,਍      琀攀砀琀㨀 渀甀洀戀攀爀Ⰰഀഀ
      json: true,਍      昀渀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀 爀攀琀甀爀渀 渀甀洀戀攀爀㬀 紀ഀഀ
    });਍  紀Ⰰഀഀ
਍  爀攀愀搀䤀搀攀渀琀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var parser = this;਍ഀഀ
    var ident = '';਍    瘀愀爀 猀琀愀爀琀 㴀 琀栀椀猀⸀椀渀搀攀砀㬀ഀഀ
਍    瘀愀爀 氀愀猀琀䐀漀琀Ⰰ 瀀攀攀欀䤀渀搀攀砀Ⰰ 洀攀琀栀漀搀一愀洀攀Ⰰ 挀栀㬀ഀഀ
਍    眀栀椀氀攀 ⠀琀栀椀猀⸀椀渀搀攀砀 㰀 琀栀椀猀⸀琀攀砀琀⸀氀攀渀最琀栀⤀ 笀ഀഀ
      ch = this.text.charAt(this.index);਍      椀昀 ⠀挀栀 㴀㴀㴀 ✀⸀✀ 簀簀 琀栀椀猀⸀椀猀䤀搀攀渀琀⠀挀栀⤀ 簀簀 琀栀椀猀⸀椀猀一甀洀戀攀爀⠀挀栀⤀⤀ 笀ഀഀ
        if (ch === '.') lastDot = this.index;਍        椀搀攀渀琀 ⬀㴀 挀栀㬀ഀഀ
      } else {਍        戀爀攀愀欀㬀ഀഀ
      }਍      琀栀椀猀⸀椀渀搀攀砀⬀⬀㬀ഀഀ
    }਍ഀഀ
    //check if this is not a method invocation and if it is back out to last dot਍    椀昀 ⠀氀愀猀琀䐀漀琀⤀ 笀ഀഀ
      peekIndex = this.index;਍      眀栀椀氀攀 ⠀瀀攀攀欀䤀渀搀攀砀 㰀 琀栀椀猀⸀琀攀砀琀⸀氀攀渀最琀栀⤀ 笀ഀഀ
        ch = this.text.charAt(peekIndex);਍        椀昀 ⠀挀栀 㴀㴀㴀 ✀⠀✀⤀ 笀ഀഀ
          methodName = ident.substr(lastDot - start + 1);਍          椀搀攀渀琀 㴀 椀搀攀渀琀⸀猀甀戀猀琀爀⠀　Ⰰ 氀愀猀琀䐀漀琀 ⴀ 猀琀愀爀琀⤀㬀ഀഀ
          this.index = peekIndex;਍          戀爀攀愀欀㬀ഀഀ
        }਍        椀昀 ⠀琀栀椀猀⸀椀猀圀栀椀琀攀猀瀀愀挀攀⠀挀栀⤀⤀ 笀ഀഀ
          peekIndex++;਍        紀 攀氀猀攀 笀ഀഀ
          break;਍        紀ഀഀ
      }਍    紀ഀഀ
਍ഀഀ
    var token = {਍      椀渀搀攀砀㨀 猀琀愀爀琀Ⰰഀഀ
      text: ident਍    紀㬀ഀഀ
਍    ⼀⼀ 伀倀䔀刀䄀吀伀刀匀 椀猀 漀甀爀 漀眀渀 漀戀樀攀挀琀 猀漀 眀攀 搀漀渀✀琀 渀攀攀搀 琀漀 甀猀攀 猀瀀攀挀椀愀氀 栀愀猀伀眀渀倀爀漀瀀攀爀琀礀䘀渀ഀഀ
    if (OPERATORS.hasOwnProperty(ident)) {਍      琀漀欀攀渀⸀昀渀 㴀 伀倀䔀刀䄀吀伀刀匀嬀椀搀攀渀琀崀㬀ഀഀ
      token.json = OPERATORS[ident];਍    紀 攀氀猀攀 笀ഀഀ
      var getter = getterFn(ident, this.options, this.text);਍      琀漀欀攀渀⸀昀渀 㴀 攀砀琀攀渀搀⠀昀甀渀挀琀椀漀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀ 笀ഀഀ
        return (getter(self, locals));਍      紀Ⰰ 笀ഀഀ
        assign: function(self, value) {਍          爀攀琀甀爀渀 猀攀琀琀攀爀⠀猀攀氀昀Ⰰ 椀搀攀渀琀Ⰰ 瘀愀氀甀攀Ⰰ 瀀愀爀猀攀爀⸀琀攀砀琀Ⰰ 瀀愀爀猀攀爀⸀漀瀀琀椀漀渀猀⤀㬀ഀഀ
        }਍      紀⤀㬀ഀഀ
    }਍ഀഀ
    this.tokens.push(token);਍ഀഀ
    if (methodName) {਍      琀栀椀猀⸀琀漀欀攀渀猀⸀瀀甀猀栀⠀笀ഀഀ
        index:lastDot,਍        琀攀砀琀㨀 ✀⸀✀Ⰰഀഀ
        json: false਍      紀⤀㬀ഀഀ
      this.tokens.push({਍        椀渀搀攀砀㨀 氀愀猀琀䐀漀琀 ⬀ ㄀Ⰰഀഀ
        text: methodName,਍        樀猀漀渀㨀 昀愀氀猀攀ഀഀ
      });਍    紀ഀഀ
  },਍ഀഀ
  readString: function(quote) {਍    瘀愀爀 猀琀愀爀琀 㴀 琀栀椀猀⸀椀渀搀攀砀㬀ഀഀ
    this.index++;਍    瘀愀爀 猀琀爀椀渀最 㴀 ✀✀㬀ഀഀ
    var rawString = quote;਍    瘀愀爀 攀猀挀愀瀀攀 㴀 昀愀氀猀攀㬀ഀഀ
    while (this.index < this.text.length) {਍      瘀愀爀 挀栀 㴀 琀栀椀猀⸀琀攀砀琀⸀挀栀愀爀䄀琀⠀琀栀椀猀⸀椀渀搀攀砀⤀㬀ഀഀ
      rawString += ch;਍      椀昀 ⠀攀猀挀愀瀀攀⤀ 笀ഀഀ
        if (ch === 'u') {਍          瘀愀爀 栀攀砀 㴀 琀栀椀猀⸀琀攀砀琀⸀猀甀戀猀琀爀椀渀最⠀琀栀椀猀⸀椀渀搀攀砀 ⬀ ㄀Ⰰ 琀栀椀猀⸀椀渀搀攀砀 ⬀ 㔀⤀㬀ഀഀ
          if (!hex.match(/[\da-f]{4}/i))਍            琀栀椀猀⸀琀栀爀漀眀䔀爀爀漀爀⠀✀䤀渀瘀愀氀椀搀 甀渀椀挀漀搀攀 攀猀挀愀瀀攀 嬀尀尀甀✀ ⬀ 栀攀砀 ⬀ ✀崀✀⤀㬀ഀഀ
          this.index += 4;਍          猀琀爀椀渀最 ⬀㴀 匀琀爀椀渀最⸀昀爀漀洀䌀栀愀爀䌀漀搀攀⠀瀀愀爀猀攀䤀渀琀⠀栀攀砀Ⰰ ㄀㘀⤀⤀㬀ഀഀ
        } else {਍          瘀愀爀 爀攀瀀 㴀 䔀匀䌀䄀倀䔀嬀挀栀崀㬀ഀഀ
          if (rep) {਍            猀琀爀椀渀最 ⬀㴀 爀攀瀀㬀ഀഀ
          } else {਍            猀琀爀椀渀最 ⬀㴀 挀栀㬀ഀഀ
          }਍        紀ഀഀ
        escape = false;਍      紀 攀氀猀攀 椀昀 ⠀挀栀 㴀㴀㴀 ✀尀尀✀⤀ 笀ഀഀ
        escape = true;਍      紀 攀氀猀攀 椀昀 ⠀挀栀 㴀㴀㴀 焀甀漀琀攀⤀ 笀ഀഀ
        this.index++;਍        琀栀椀猀⸀琀漀欀攀渀猀⸀瀀甀猀栀⠀笀ഀഀ
          index: start,਍          琀攀砀琀㨀 爀愀眀匀琀爀椀渀最Ⰰഀഀ
          string: string,਍          樀猀漀渀㨀 琀爀甀攀Ⰰഀഀ
          fn: function() { return string; }਍        紀⤀㬀ഀഀ
        return;਍      紀 攀氀猀攀 笀ഀഀ
        string += ch;਍      紀ഀഀ
      this.index++;਍    紀ഀഀ
    this.throwError('Unterminated quote', start);਍  紀ഀഀ
};਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @constructor਍ ⨀⼀ഀഀ
var Parser = function (lexer, $filter, options) {਍  琀栀椀猀⸀氀攀砀攀爀 㴀 氀攀砀攀爀㬀ഀഀ
  this.$filter = $filter;਍  琀栀椀猀⸀漀瀀琀椀漀渀猀 㴀 漀瀀琀椀漀渀猀㬀ഀഀ
};਍ഀഀ
Parser.ZERO = function () { return 0; };਍ഀഀ
Parser.prototype = {਍  挀漀渀猀琀爀甀挀琀漀爀㨀 倀愀爀猀攀爀Ⰰഀഀ
਍  瀀愀爀猀攀㨀 昀甀渀挀琀椀漀渀 ⠀琀攀砀琀Ⰰ 樀猀漀渀⤀ 笀ഀഀ
    this.text = text;਍ഀഀ
    //TODO(i): strip all the obsolte json stuff from this file਍    琀栀椀猀⸀樀猀漀渀 㴀 樀猀漀渀㬀ഀഀ
਍    琀栀椀猀⸀琀漀欀攀渀猀 㴀 琀栀椀猀⸀氀攀砀攀爀⸀氀攀砀⠀琀攀砀琀⤀㬀ഀഀ
਍    椀昀 ⠀樀猀漀渀⤀ 笀ഀഀ
      // The extra level of aliasing is here, just in case the lexer misses something, so that਍      ⼀⼀ 眀攀 瀀爀攀瘀攀渀琀 愀渀礀 愀挀挀椀搀攀渀琀愀氀 攀砀攀挀甀琀椀漀渀 椀渀 䨀匀伀一⸀ഀഀ
      this.assignment = this.logicalOR;਍ഀഀ
      this.functionCall =਍      琀栀椀猀⸀昀椀攀氀搀䄀挀挀攀猀猀 㴀ഀഀ
      this.objectIndex =਍      琀栀椀猀⸀昀椀氀琀攀爀䌀栀愀椀渀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
        this.throwError('is not valid json', {text: text, index: 0});਍      紀㬀ഀഀ
    }਍ഀഀ
    var value = json ? this.primary() : this.statements();਍ഀഀ
    if (this.tokens.length !== 0) {਍      琀栀椀猀⸀琀栀爀漀眀䔀爀爀漀爀⠀✀椀猀 愀渀 甀渀攀砀瀀攀挀琀攀搀 琀漀欀攀渀✀Ⰰ 琀栀椀猀⸀琀漀欀攀渀猀嬀　崀⤀㬀ഀഀ
    }਍ഀഀ
    value.literal = !!value.literal;਍    瘀愀氀甀攀⸀挀漀渀猀琀愀渀琀 㴀 ℀℀瘀愀氀甀攀⸀挀漀渀猀琀愀渀琀㬀ഀഀ
਍    爀攀琀甀爀渀 瘀愀氀甀攀㬀ഀഀ
  },਍ഀഀ
  primary: function () {਍    瘀愀爀 瀀爀椀洀愀爀礀㬀ഀഀ
    if (this.expect('(')) {਍      瀀爀椀洀愀爀礀 㴀 琀栀椀猀⸀昀椀氀琀攀爀䌀栀愀椀渀⠀⤀㬀ഀഀ
      this.consume(')');਍    紀 攀氀猀攀 椀昀 ⠀琀栀椀猀⸀攀砀瀀攀挀琀⠀✀嬀✀⤀⤀ 笀ഀഀ
      primary = this.arrayDeclaration();਍    紀 攀氀猀攀 椀昀 ⠀琀栀椀猀⸀攀砀瀀攀挀琀⠀✀笀✀⤀⤀ 笀ഀഀ
      primary = this.object();਍    紀 攀氀猀攀 笀ഀഀ
      var token = this.expect();਍      瀀爀椀洀愀爀礀 㴀 琀漀欀攀渀⸀昀渀㬀ഀഀ
      if (!primary) {਍        琀栀椀猀⸀琀栀爀漀眀䔀爀爀漀爀⠀✀渀漀琀 愀 瀀爀椀洀愀爀礀 攀砀瀀爀攀猀猀椀漀渀✀Ⰰ 琀漀欀攀渀⤀㬀ഀഀ
      }਍      椀昀 ⠀琀漀欀攀渀⸀樀猀漀渀⤀ 笀ഀഀ
        primary.constant = true;਍        瀀爀椀洀愀爀礀⸀氀椀琀攀爀愀氀 㴀 琀爀甀攀㬀ഀഀ
      }਍    紀ഀഀ
਍    瘀愀爀 渀攀砀琀Ⰰ 挀漀渀琀攀砀琀㬀ഀഀ
    while ((next = this.expect('(', '[', '.'))) {਍      椀昀 ⠀渀攀砀琀⸀琀攀砀琀 㴀㴀㴀 ✀⠀✀⤀ 笀ഀഀ
        primary = this.functionCall(primary, context);਍        挀漀渀琀攀砀琀 㴀 渀甀氀氀㬀ഀഀ
      } else if (next.text === '[') {਍        挀漀渀琀攀砀琀 㴀 瀀爀椀洀愀爀礀㬀ഀഀ
        primary = this.objectIndex(primary);਍      紀 攀氀猀攀 椀昀 ⠀渀攀砀琀⸀琀攀砀琀 㴀㴀㴀 ✀⸀✀⤀ 笀ഀഀ
        context = primary;਍        瀀爀椀洀愀爀礀 㴀 琀栀椀猀⸀昀椀攀氀搀䄀挀挀攀猀猀⠀瀀爀椀洀愀爀礀⤀㬀ഀഀ
      } else {਍        琀栀椀猀⸀琀栀爀漀眀䔀爀爀漀爀⠀✀䤀䴀倀伀匀匀䤀䈀䰀䔀✀⤀㬀ഀഀ
      }਍    紀ഀഀ
    return primary;਍  紀Ⰰഀഀ
਍  琀栀爀漀眀䔀爀爀漀爀㨀 昀甀渀挀琀椀漀渀⠀洀猀最Ⰰ 琀漀欀攀渀⤀ 笀ഀഀ
    throw $parseMinErr('syntax',਍        ✀匀礀渀琀愀砀 䔀爀爀漀爀㨀 吀漀欀攀渀 尀✀笀　紀尀✀ 笀㄀紀 愀琀 挀漀氀甀洀渀 笀㈀紀 漀昀 琀栀攀 攀砀瀀爀攀猀猀椀漀渀 嬀笀㌀紀崀 猀琀愀爀琀椀渀最 愀琀 嬀笀㐀紀崀⸀✀Ⰰഀഀ
          token.text, msg, (token.index + 1), this.text, this.text.substring(token.index));਍  紀Ⰰഀഀ
਍  瀀攀攀欀吀漀欀攀渀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    if (this.tokens.length === 0)਍      琀栀爀漀眀 ␀瀀愀爀猀攀䴀椀渀䔀爀爀⠀✀甀攀漀攀✀Ⰰ ✀唀渀攀砀瀀攀挀琀攀搀 攀渀搀 漀昀 攀砀瀀爀攀猀猀椀漀渀㨀 笀　紀✀Ⰰ 琀栀椀猀⸀琀攀砀琀⤀㬀ഀഀ
    return this.tokens[0];਍  紀Ⰰഀഀ
਍  瀀攀攀欀㨀 昀甀渀挀琀椀漀渀⠀攀㄀Ⰰ 攀㈀Ⰰ 攀㌀Ⰰ 攀㐀⤀ 笀ഀഀ
    if (this.tokens.length > 0) {਍      瘀愀爀 琀漀欀攀渀 㴀 琀栀椀猀⸀琀漀欀攀渀猀嬀　崀㬀ഀഀ
      var t = token.text;਍      椀昀 ⠀琀 㴀㴀㴀 攀㄀ 簀簀 琀 㴀㴀㴀 攀㈀ 簀簀 琀 㴀㴀㴀 攀㌀ 簀簀 琀 㴀㴀㴀 攀㐀 簀簀ഀഀ
          (!e1 && !e2 && !e3 && !e4)) {਍        爀攀琀甀爀渀 琀漀欀攀渀㬀ഀഀ
      }਍    紀ഀഀ
    return false;਍  紀Ⰰഀഀ
਍  攀砀瀀攀挀琀㨀 昀甀渀挀琀椀漀渀⠀攀㄀Ⰰ 攀㈀Ⰰ 攀㌀Ⰰ 攀㐀⤀笀ഀഀ
    var token = this.peek(e1, e2, e3, e4);਍    椀昀 ⠀琀漀欀攀渀⤀ 笀ഀഀ
      if (this.json && !token.json) {਍        琀栀椀猀⸀琀栀爀漀眀䔀爀爀漀爀⠀✀椀猀 渀漀琀 瘀愀氀椀搀 樀猀漀渀✀Ⰰ 琀漀欀攀渀⤀㬀ഀഀ
      }਍      琀栀椀猀⸀琀漀欀攀渀猀⸀猀栀椀昀琀⠀⤀㬀ഀഀ
      return token;਍    紀ഀഀ
    return false;਍  紀Ⰰഀഀ
਍  挀漀渀猀甀洀攀㨀 昀甀渀挀琀椀漀渀⠀攀㄀⤀笀ഀഀ
    if (!this.expect(e1)) {਍      琀栀椀猀⸀琀栀爀漀眀䔀爀爀漀爀⠀✀椀猀 甀渀攀砀瀀攀挀琀攀搀Ⰰ 攀砀瀀攀挀琀椀渀最 嬀✀ ⬀ 攀㄀ ⬀ ✀崀✀Ⰰ 琀栀椀猀⸀瀀攀攀欀⠀⤀⤀㬀ഀഀ
    }਍  紀Ⰰഀഀ
਍  甀渀愀爀礀䘀渀㨀 昀甀渀挀琀椀漀渀⠀昀渀Ⰰ 爀椀最栀琀⤀ 笀ഀഀ
    return extend(function(self, locals) {਍      爀攀琀甀爀渀 昀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀Ⰰ 爀椀最栀琀⤀㬀ഀഀ
    }, {਍      挀漀渀猀琀愀渀琀㨀爀椀最栀琀⸀挀漀渀猀琀愀渀琀ഀഀ
    });਍  紀Ⰰഀഀ
਍  琀攀爀渀愀爀礀䘀渀㨀 昀甀渀挀琀椀漀渀⠀氀攀昀琀Ⰰ 洀椀搀搀氀攀Ⰰ 爀椀最栀琀⤀笀ഀഀ
    return extend(function(self, locals){਍      爀攀琀甀爀渀 氀攀昀琀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀ 㼀 洀椀搀搀氀攀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀ 㨀 爀椀最栀琀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀㬀ഀഀ
    }, {਍      挀漀渀猀琀愀渀琀㨀 氀攀昀琀⸀挀漀渀猀琀愀渀琀 ☀☀ 洀椀搀搀氀攀⸀挀漀渀猀琀愀渀琀 ☀☀ 爀椀最栀琀⸀挀漀渀猀琀愀渀琀ഀഀ
    });਍  紀Ⰰഀഀ
਍  戀椀渀愀爀礀䘀渀㨀 昀甀渀挀琀椀漀渀⠀氀攀昀琀Ⰰ 昀渀Ⰰ 爀椀最栀琀⤀ 笀ഀഀ
    return extend(function(self, locals) {਍      爀攀琀甀爀渀 昀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀Ⰰ 氀攀昀琀Ⰰ 爀椀最栀琀⤀㬀ഀഀ
    }, {਍      挀漀渀猀琀愀渀琀㨀氀攀昀琀⸀挀漀渀猀琀愀渀琀 ☀☀ 爀椀最栀琀⸀挀漀渀猀琀愀渀琀ഀഀ
    });਍  紀Ⰰഀഀ
਍  猀琀愀琀攀洀攀渀琀猀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var statements = [];਍    眀栀椀氀攀 ⠀琀爀甀攀⤀ 笀ഀഀ
      if (this.tokens.length > 0 && !this.peek('}', ')', ';', ']'))਍        猀琀愀琀攀洀攀渀琀猀⸀瀀甀猀栀⠀琀栀椀猀⸀昀椀氀琀攀爀䌀栀愀椀渀⠀⤀⤀㬀ഀഀ
      if (!this.expect(';')) {਍        ⼀⼀ 漀瀀琀椀洀椀稀攀 昀漀爀 琀栀攀 挀漀洀洀漀渀 挀愀猀攀 眀栀攀爀攀 琀栀攀爀攀 椀猀 漀渀氀礀 漀渀攀 猀琀愀琀攀洀攀渀琀⸀ഀഀ
        // TODO(size): maybe we should not support multiple statements?਍        爀攀琀甀爀渀 ⠀猀琀愀琀攀洀攀渀琀猀⸀氀攀渀最琀栀 㴀㴀㴀 ㄀⤀ഀഀ
            ? statements[0]਍            㨀 昀甀渀挀琀椀漀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀ 笀ഀഀ
                var value;਍                昀漀爀 ⠀瘀愀爀 椀 㴀 　㬀 椀 㰀 猀琀愀琀攀洀攀渀琀猀⸀氀攀渀最琀栀㬀 椀⬀⬀⤀ 笀ഀഀ
                  var statement = statements[i];਍                  椀昀 ⠀猀琀愀琀攀洀攀渀琀⤀ 笀ഀഀ
                    value = statement(self, locals);਍                  紀ഀഀ
                }਍                爀攀琀甀爀渀 瘀愀氀甀攀㬀ഀഀ
              };਍      紀ഀഀ
    }਍  紀Ⰰഀഀ
਍  昀椀氀琀攀爀䌀栀愀椀渀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var left = this.expression();਍    瘀愀爀 琀漀欀攀渀㬀ഀഀ
    while (true) {਍      椀昀 ⠀⠀琀漀欀攀渀 㴀 琀栀椀猀⸀攀砀瀀攀挀琀⠀✀簀✀⤀⤀⤀ 笀ഀഀ
        left = this.binaryFn(left, token.fn, this.filter());਍      紀 攀氀猀攀 笀ഀഀ
        return left;਍      紀ഀഀ
    }਍  紀Ⰰഀഀ
਍  昀椀氀琀攀爀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var token = this.expect();਍    瘀愀爀 昀渀 㴀 琀栀椀猀⸀␀昀椀氀琀攀爀⠀琀漀欀攀渀⸀琀攀砀琀⤀㬀ഀഀ
    var argsFn = [];਍    眀栀椀氀攀 ⠀琀爀甀攀⤀ 笀ഀഀ
      if ((token = this.expect(':'))) {਍        愀爀最猀䘀渀⸀瀀甀猀栀⠀琀栀椀猀⸀攀砀瀀爀攀猀猀椀漀渀⠀⤀⤀㬀ഀഀ
      } else {਍        瘀愀爀 昀渀䤀渀瘀漀欀攀 㴀 昀甀渀挀琀椀漀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀Ⰰ 椀渀瀀甀琀⤀ 笀ഀഀ
          var args = [input];਍          昀漀爀 ⠀瘀愀爀 椀 㴀 　㬀 椀 㰀 愀爀最猀䘀渀⸀氀攀渀最琀栀㬀 椀⬀⬀⤀ 笀ഀഀ
            args.push(argsFn[i](self, locals));਍          紀ഀഀ
          return fn.apply(self, args);਍        紀㬀ഀഀ
        return function() {਍          爀攀琀甀爀渀 昀渀䤀渀瘀漀欀攀㬀ഀഀ
        };਍      紀ഀഀ
    }਍  紀Ⰰഀഀ
਍  攀砀瀀爀攀猀猀椀漀渀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    return this.assignment();਍  紀Ⰰഀഀ
਍  愀猀猀椀最渀洀攀渀琀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var left = this.ternary();਍    瘀愀爀 爀椀最栀琀㬀ഀഀ
    var token;਍    椀昀 ⠀⠀琀漀欀攀渀 㴀 琀栀椀猀⸀攀砀瀀攀挀琀⠀✀㴀✀⤀⤀⤀ 笀ഀഀ
      if (!left.assign) {਍        琀栀椀猀⸀琀栀爀漀眀䔀爀爀漀爀⠀✀椀洀瀀氀椀攀猀 愀猀猀椀最渀洀攀渀琀 戀甀琀 嬀✀ ⬀ഀഀ
            this.text.substring(0, token.index) + '] can not be assigned to', token);਍      紀ഀഀ
      right = this.ternary();਍      爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 氀漀挀愀氀猀⤀ 笀ഀഀ
        return left.assign(scope, right(scope, locals), locals);਍      紀㬀ഀഀ
    }਍    爀攀琀甀爀渀 氀攀昀琀㬀ഀഀ
  },਍ഀഀ
  ternary: function() {਍    瘀愀爀 氀攀昀琀 㴀 琀栀椀猀⸀氀漀最椀挀愀氀伀刀⠀⤀㬀ഀഀ
    var middle;਍    瘀愀爀 琀漀欀攀渀㬀ഀഀ
    if ((token = this.expect('?'))) {਍      洀椀搀搀氀攀 㴀 琀栀椀猀⸀琀攀爀渀愀爀礀⠀⤀㬀ഀഀ
      if ((token = this.expect(':'))) {਍        爀攀琀甀爀渀 琀栀椀猀⸀琀攀爀渀愀爀礀䘀渀⠀氀攀昀琀Ⰰ 洀椀搀搀氀攀Ⰰ 琀栀椀猀⸀琀攀爀渀愀爀礀⠀⤀⤀㬀ഀഀ
      } else {਍        琀栀椀猀⸀琀栀爀漀眀䔀爀爀漀爀⠀✀攀砀瀀攀挀琀攀搀 㨀✀Ⰰ 琀漀欀攀渀⤀㬀ഀഀ
      }਍    紀 攀氀猀攀 笀ഀഀ
      return left;਍    紀ഀഀ
  },਍ഀഀ
  logicalOR: function() {਍    瘀愀爀 氀攀昀琀 㴀 琀栀椀猀⸀氀漀最椀挀愀氀䄀一䐀⠀⤀㬀ഀഀ
    var token;਍    眀栀椀氀攀 ⠀琀爀甀攀⤀ 笀ഀഀ
      if ((token = this.expect('||'))) {਍        氀攀昀琀 㴀 琀栀椀猀⸀戀椀渀愀爀礀䘀渀⠀氀攀昀琀Ⰰ 琀漀欀攀渀⸀昀渀Ⰰ 琀栀椀猀⸀氀漀最椀挀愀氀䄀一䐀⠀⤀⤀㬀ഀഀ
      } else {਍        爀攀琀甀爀渀 氀攀昀琀㬀ഀഀ
      }਍    紀ഀഀ
  },਍ഀഀ
  logicalAND: function() {਍    瘀愀爀 氀攀昀琀 㴀 琀栀椀猀⸀攀焀甀愀氀椀琀礀⠀⤀㬀ഀഀ
    var token;਍    椀昀 ⠀⠀琀漀欀攀渀 㴀 琀栀椀猀⸀攀砀瀀攀挀琀⠀✀☀☀✀⤀⤀⤀ 笀ഀഀ
      left = this.binaryFn(left, token.fn, this.logicalAND());਍    紀ഀഀ
    return left;਍  紀Ⰰഀഀ
਍  攀焀甀愀氀椀琀礀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var left = this.relational();਍    瘀愀爀 琀漀欀攀渀㬀ഀഀ
    if ((token = this.expect('==','!=','===','!=='))) {਍      氀攀昀琀 㴀 琀栀椀猀⸀戀椀渀愀爀礀䘀渀⠀氀攀昀琀Ⰰ 琀漀欀攀渀⸀昀渀Ⰰ 琀栀椀猀⸀攀焀甀愀氀椀琀礀⠀⤀⤀㬀ഀഀ
    }਍    爀攀琀甀爀渀 氀攀昀琀㬀ഀഀ
  },਍ഀഀ
  relational: function() {਍    瘀愀爀 氀攀昀琀 㴀 琀栀椀猀⸀愀搀搀椀琀椀瘀攀⠀⤀㬀ഀഀ
    var token;਍    椀昀 ⠀⠀琀漀欀攀渀 㴀 琀栀椀猀⸀攀砀瀀攀挀琀⠀✀㰀✀Ⰰ ✀㸀✀Ⰰ ✀㰀㴀✀Ⰰ ✀㸀㴀✀⤀⤀⤀ 笀ഀഀ
      left = this.binaryFn(left, token.fn, this.relational());਍    紀ഀഀ
    return left;਍  紀Ⰰഀഀ
਍  愀搀搀椀琀椀瘀攀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var left = this.multiplicative();਍    瘀愀爀 琀漀欀攀渀㬀ഀഀ
    while ((token = this.expect('+','-'))) {਍      氀攀昀琀 㴀 琀栀椀猀⸀戀椀渀愀爀礀䘀渀⠀氀攀昀琀Ⰰ 琀漀欀攀渀⸀昀渀Ⰰ 琀栀椀猀⸀洀甀氀琀椀瀀氀椀挀愀琀椀瘀攀⠀⤀⤀㬀ഀഀ
    }਍    爀攀琀甀爀渀 氀攀昀琀㬀ഀഀ
  },਍ഀഀ
  multiplicative: function() {਍    瘀愀爀 氀攀昀琀 㴀 琀栀椀猀⸀甀渀愀爀礀⠀⤀㬀ഀഀ
    var token;਍    眀栀椀氀攀 ⠀⠀琀漀欀攀渀 㴀 琀栀椀猀⸀攀砀瀀攀挀琀⠀✀⨀✀Ⰰ✀⼀✀Ⰰ✀─✀⤀⤀⤀ 笀ഀഀ
      left = this.binaryFn(left, token.fn, this.unary());਍    紀ഀഀ
    return left;਍  紀Ⰰഀഀ
਍  甀渀愀爀礀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var token;਍    椀昀 ⠀琀栀椀猀⸀攀砀瀀攀挀琀⠀✀⬀✀⤀⤀ 笀ഀഀ
      return this.primary();਍    紀 攀氀猀攀 椀昀 ⠀⠀琀漀欀攀渀 㴀 琀栀椀猀⸀攀砀瀀攀挀琀⠀✀ⴀ✀⤀⤀⤀ 笀ഀഀ
      return this.binaryFn(Parser.ZERO, token.fn, this.unary());਍    紀 攀氀猀攀 椀昀 ⠀⠀琀漀欀攀渀 㴀 琀栀椀猀⸀攀砀瀀攀挀琀⠀✀℀✀⤀⤀⤀ 笀ഀഀ
      return this.unaryFn(token.fn, this.unary());਍    紀 攀氀猀攀 笀ഀഀ
      return this.primary();਍    紀ഀഀ
  },਍ഀഀ
  fieldAccess: function(object) {਍    瘀愀爀 瀀愀爀猀攀爀 㴀 琀栀椀猀㬀ഀഀ
    var field = this.expect().text;਍    瘀愀爀 最攀琀琀攀爀 㴀 最攀琀琀攀爀䘀渀⠀昀椀攀氀搀Ⰰ 琀栀椀猀⸀漀瀀琀椀漀渀猀Ⰰ 琀栀椀猀⸀琀攀砀琀⤀㬀ഀഀ
਍    爀攀琀甀爀渀 攀砀琀攀渀搀⠀昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 氀漀挀愀氀猀Ⰰ 猀攀氀昀⤀ 笀ഀഀ
      return getter(self || object(scope, locals), locals);਍    紀Ⰰ 笀ഀഀ
      assign: function(scope, value, locals) {਍        爀攀琀甀爀渀 猀攀琀琀攀爀⠀漀戀樀攀挀琀⠀猀挀漀瀀攀Ⰰ 氀漀挀愀氀猀⤀Ⰰ 昀椀攀氀搀Ⰰ 瘀愀氀甀攀Ⰰ 瀀愀爀猀攀爀⸀琀攀砀琀Ⰰ 瀀愀爀猀攀爀⸀漀瀀琀椀漀渀猀⤀㬀ഀഀ
      }਍    紀⤀㬀ഀഀ
  },਍ഀഀ
  objectIndex: function(obj) {਍    瘀愀爀 瀀愀爀猀攀爀 㴀 琀栀椀猀㬀ഀഀ
਍    瘀愀爀 椀渀搀攀砀䘀渀 㴀 琀栀椀猀⸀攀砀瀀爀攀猀猀椀漀渀⠀⤀㬀ഀഀ
    this.consume(']');਍ഀഀ
    return extend(function(self, locals) {਍      瘀愀爀 漀 㴀 漀戀樀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀Ⰰഀഀ
          i = indexFn(self, locals),਍          瘀Ⰰ 瀀㬀ഀഀ
਍      椀昀 ⠀℀漀⤀ 爀攀琀甀爀渀 甀渀搀攀昀椀渀攀搀㬀ഀഀ
      v = ensureSafeObject(o[i], parser.text);਍      椀昀 ⠀瘀 ☀☀ 瘀⸀琀栀攀渀 ☀☀ 瀀愀爀猀攀爀⸀漀瀀琀椀漀渀猀⸀甀渀眀爀愀瀀倀爀漀洀椀猀攀猀⤀ 笀ഀഀ
        p = v;਍        椀昀 ⠀℀⠀✀␀␀瘀✀ 椀渀 瘀⤀⤀ 笀ഀഀ
          p.$$v = undefined;਍          瀀⸀琀栀攀渀⠀昀甀渀挀琀椀漀渀⠀瘀愀氀⤀ 笀 瀀⸀␀␀瘀 㴀 瘀愀氀㬀 紀⤀㬀ഀഀ
        }਍        瘀 㴀 瘀⸀␀␀瘀㬀ഀഀ
      }਍      爀攀琀甀爀渀 瘀㬀ഀഀ
    }, {਍      愀猀猀椀最渀㨀 昀甀渀挀琀椀漀渀⠀猀攀氀昀Ⰰ 瘀愀氀甀攀Ⰰ 氀漀挀愀氀猀⤀ 笀ഀഀ
        var key = indexFn(self, locals);਍        ⼀⼀ 瀀爀攀瘀攀渀琀 漀瘀攀爀眀爀椀琀椀渀最 漀昀 䘀甀渀挀琀椀漀渀⸀挀漀渀猀琀爀甀挀琀漀爀 眀栀椀挀栀 眀漀甀氀搀 戀爀攀愀欀 攀渀猀甀爀攀匀愀昀攀伀戀樀攀挀琀 挀栀攀挀欀ഀഀ
        var safe = ensureSafeObject(obj(self, locals), parser.text);਍        爀攀琀甀爀渀 猀愀昀攀嬀欀攀礀崀 㴀 瘀愀氀甀攀㬀ഀഀ
      }਍    紀⤀㬀ഀഀ
  },਍ഀഀ
  functionCall: function(fn, contextGetter) {਍    瘀愀爀 愀爀最猀䘀渀 㴀 嬀崀㬀ഀഀ
    if (this.peekToken().text !== ')') {਍      搀漀 笀ഀഀ
        argsFn.push(this.expression());਍      紀 眀栀椀氀攀 ⠀琀栀椀猀⸀攀砀瀀攀挀琀⠀✀Ⰰ✀⤀⤀㬀ഀഀ
    }਍    琀栀椀猀⸀挀漀渀猀甀洀攀⠀✀⤀✀⤀㬀ഀഀ
਍    瘀愀爀 瀀愀爀猀攀爀 㴀 琀栀椀猀㬀ഀഀ
਍    爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 氀漀挀愀氀猀⤀ 笀ഀഀ
      var args = [];਍      瘀愀爀 挀漀渀琀攀砀琀 㴀 挀漀渀琀攀砀琀䜀攀琀琀攀爀 㼀 挀漀渀琀攀砀琀䜀攀琀琀攀爀⠀猀挀漀瀀攀Ⰰ 氀漀挀愀氀猀⤀ 㨀 猀挀漀瀀攀㬀ഀഀ
਍      昀漀爀 ⠀瘀愀爀 椀 㴀 　㬀 椀 㰀 愀爀最猀䘀渀⸀氀攀渀最琀栀㬀 椀⬀⬀⤀ 笀ഀഀ
        args.push(argsFn[i](scope, locals));਍      紀ഀഀ
      var fnPtr = fn(scope, locals, context) || noop;਍ഀഀ
      ensureSafeObject(context, parser.text);਍      攀渀猀甀爀攀匀愀昀攀伀戀樀攀挀琀⠀昀渀倀琀爀Ⰰ 瀀愀爀猀攀爀⸀琀攀砀琀⤀㬀ഀഀ
਍      ⼀⼀ 䤀䔀 猀琀甀瀀椀搀椀琀礀℀ ⠀䤀䔀 搀漀攀猀渀✀琀 栀愀瘀攀 愀瀀瀀氀礀 昀漀爀 猀漀洀攀 渀愀琀椀瘀攀 昀甀渀挀琀椀漀渀猀⤀ഀഀ
      var v = fnPtr.apply਍            㼀 昀渀倀琀爀⸀愀瀀瀀氀礀⠀挀漀渀琀攀砀琀Ⰰ 愀爀最猀⤀ഀഀ
            : fnPtr(args[0], args[1], args[2], args[3], args[4]);਍ഀഀ
      return ensureSafeObject(v, parser.text);਍    紀㬀ഀഀ
  },਍ഀഀ
  // This is used with json array declaration਍  愀爀爀愀礀䐀攀挀氀愀爀愀琀椀漀渀㨀 昀甀渀挀琀椀漀渀 ⠀⤀ 笀ഀഀ
    var elementFns = [];਍    瘀愀爀 愀氀氀䌀漀渀猀琀愀渀琀 㴀 琀爀甀攀㬀ഀഀ
    if (this.peekToken().text !== ']') {਍      搀漀 笀ഀഀ
        var elementFn = this.expression();਍        攀氀攀洀攀渀琀䘀渀猀⸀瀀甀猀栀⠀攀氀攀洀攀渀琀䘀渀⤀㬀ഀഀ
        if (!elementFn.constant) {਍          愀氀氀䌀漀渀猀琀愀渀琀 㴀 昀愀氀猀攀㬀ഀഀ
        }਍      紀 眀栀椀氀攀 ⠀琀栀椀猀⸀攀砀瀀攀挀琀⠀✀Ⰰ✀⤀⤀㬀ഀഀ
    }਍    琀栀椀猀⸀挀漀渀猀甀洀攀⠀✀崀✀⤀㬀ഀഀ
਍    爀攀琀甀爀渀 攀砀琀攀渀搀⠀昀甀渀挀琀椀漀渀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀ 笀ഀഀ
      var array = [];਍      昀漀爀 ⠀瘀愀爀 椀 㴀 　㬀 椀 㰀 攀氀攀洀攀渀琀䘀渀猀⸀氀攀渀最琀栀㬀 椀⬀⬀⤀ 笀ഀഀ
        array.push(elementFns[i](self, locals));਍      紀ഀഀ
      return array;਍    紀Ⰰ 笀ഀഀ
      literal: true,਍      挀漀渀猀琀愀渀琀㨀 愀氀氀䌀漀渀猀琀愀渀琀ഀഀ
    });਍  紀Ⰰഀഀ
਍  漀戀樀攀挀琀㨀 昀甀渀挀琀椀漀渀 ⠀⤀ 笀ഀഀ
    var keyValues = [];਍    瘀愀爀 愀氀氀䌀漀渀猀琀愀渀琀 㴀 琀爀甀攀㬀ഀഀ
    if (this.peekToken().text !== '}') {਍      搀漀 笀ഀഀ
        var token = this.expect(),਍        欀攀礀 㴀 琀漀欀攀渀⸀猀琀爀椀渀最 簀簀 琀漀欀攀渀⸀琀攀砀琀㬀ഀഀ
        this.consume(':');਍        瘀愀爀 瘀愀氀甀攀 㴀 琀栀椀猀⸀攀砀瀀爀攀猀猀椀漀渀⠀⤀㬀ഀഀ
        keyValues.push({key: key, value: value});਍        椀昀 ⠀℀瘀愀氀甀攀⸀挀漀渀猀琀愀渀琀⤀ 笀ഀഀ
          allConstant = false;਍        紀ഀഀ
      } while (this.expect(','));਍    紀ഀഀ
    this.consume('}');਍ഀഀ
    return extend(function(self, locals) {਍      瘀愀爀 漀戀樀攀挀琀 㴀 笀紀㬀ഀഀ
      for (var i = 0; i < keyValues.length; i++) {਍        瘀愀爀 欀攀礀嘀愀氀甀攀 㴀 欀攀礀嘀愀氀甀攀猀嬀椀崀㬀ഀഀ
        object[keyValue.key] = keyValue.value(self, locals);਍      紀ഀഀ
      return object;਍    紀Ⰰ 笀ഀഀ
      literal: true,਍      挀漀渀猀琀愀渀琀㨀 愀氀氀䌀漀渀猀琀愀渀琀ഀഀ
    });਍  紀ഀഀ
};਍ഀഀ
਍⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
// Parser helper functions਍⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
਍昀甀渀挀琀椀漀渀 猀攀琀琀攀爀⠀漀戀樀Ⰰ 瀀愀琀栀Ⰰ 猀攀琀嘀愀氀甀攀Ⰰ 昀甀氀氀䔀砀瀀Ⰰ 漀瀀琀椀漀渀猀⤀ 笀ഀഀ
  //needed?਍  漀瀀琀椀漀渀猀 㴀 漀瀀琀椀漀渀猀 簀簀 笀紀㬀ഀഀ
਍  瘀愀爀 攀氀攀洀攀渀琀 㴀 瀀愀琀栀⸀猀瀀氀椀琀⠀✀⸀✀⤀Ⰰ 欀攀礀㬀ഀഀ
  for (var i = 0; element.length > 1; i++) {਍    欀攀礀 㴀 攀渀猀甀爀攀匀愀昀攀䴀攀洀戀攀爀一愀洀攀⠀攀氀攀洀攀渀琀⸀猀栀椀昀琀⠀⤀Ⰰ 昀甀氀氀䔀砀瀀⤀㬀ഀഀ
    var propertyObj = obj[key];਍    椀昀 ⠀℀瀀爀漀瀀攀爀琀礀伀戀樀⤀ 笀ഀഀ
      propertyObj = {};਍      漀戀樀嬀欀攀礀崀 㴀 瀀爀漀瀀攀爀琀礀伀戀樀㬀ഀഀ
    }਍    漀戀樀 㴀 瀀爀漀瀀攀爀琀礀伀戀樀㬀ഀഀ
    if (obj.then && options.unwrapPromises) {਍      瀀爀漀洀椀猀攀圀愀爀渀椀渀最⠀昀甀氀氀䔀砀瀀⤀㬀ഀഀ
      if (!("$$v" in obj)) {਍        ⠀昀甀渀挀琀椀漀渀⠀瀀爀漀洀椀猀攀⤀ 笀ഀഀ
          promise.then(function(val) { promise.$$v = val; }); }਍        ⤀⠀漀戀樀⤀㬀ഀഀ
      }਍      椀昀 ⠀漀戀樀⸀␀␀瘀 㴀㴀㴀 甀渀搀攀昀椀渀攀搀⤀ 笀ഀഀ
        obj.$$v = {};਍      紀ഀഀ
      obj = obj.$$v;਍    紀ഀഀ
  }਍  欀攀礀 㴀 攀渀猀甀爀攀匀愀昀攀䴀攀洀戀攀爀一愀洀攀⠀攀氀攀洀攀渀琀⸀猀栀椀昀琀⠀⤀Ⰰ 昀甀氀氀䔀砀瀀⤀㬀ഀഀ
  obj[key] = setValue;਍  爀攀琀甀爀渀 猀攀琀嘀愀氀甀攀㬀ഀഀ
}਍ഀഀ
var getterFnCache = {};਍ഀഀ
/**਍ ⨀ 䤀洀瀀氀攀洀攀渀琀愀琀椀漀渀 漀昀 琀栀攀 ∀䈀氀愀挀欀 䠀漀氀攀∀ 瘀愀爀椀愀渀琀 昀爀漀洀㨀ഀഀ
 * - http://jsperf.com/angularjs-parse-getter/4਍ ⨀ ⴀ 栀琀琀瀀㨀⼀⼀樀猀瀀攀爀昀⸀挀漀洀⼀瀀愀琀栀ⴀ攀瘀愀氀甀愀琀椀漀渀ⴀ猀椀洀瀀氀椀昀椀攀搀⼀㜀ഀഀ
 */਍昀甀渀挀琀椀漀渀 挀猀瀀匀愀昀攀䜀攀琀琀攀爀䘀渀⠀欀攀礀　Ⰰ 欀攀礀㄀Ⰰ 欀攀礀㈀Ⰰ 欀攀礀㌀Ⰰ 欀攀礀㐀Ⰰ 昀甀氀氀䔀砀瀀Ⰰ 漀瀀琀椀漀渀猀⤀ 笀ഀഀ
  ensureSafeMemberName(key0, fullExp);਍  攀渀猀甀爀攀匀愀昀攀䴀攀洀戀攀爀一愀洀攀⠀欀攀礀㄀Ⰰ 昀甀氀氀䔀砀瀀⤀㬀ഀഀ
  ensureSafeMemberName(key2, fullExp);਍  攀渀猀甀爀攀匀愀昀攀䴀攀洀戀攀爀一愀洀攀⠀欀攀礀㌀Ⰰ 昀甀氀氀䔀砀瀀⤀㬀ഀഀ
  ensureSafeMemberName(key4, fullExp);਍ഀഀ
  return !options.unwrapPromises਍      㼀 昀甀渀挀琀椀漀渀 挀猀瀀匀愀昀攀䜀攀琀琀攀爀⠀猀挀漀瀀攀Ⰰ 氀漀挀愀氀猀⤀ 笀ഀഀ
          var pathVal = (locals && locals.hasOwnProperty(key0)) ? locals : scope;਍ഀഀ
          if (pathVal == null) return pathVal;਍          瀀愀琀栀嘀愀氀 㴀 瀀愀琀栀嘀愀氀嬀欀攀礀　崀㬀ഀഀ
਍          椀昀 ⠀℀欀攀礀㄀⤀ 爀攀琀甀爀渀 瀀愀琀栀嘀愀氀㬀ഀഀ
          if (pathVal == null) return undefined;਍          瀀愀琀栀嘀愀氀 㴀 瀀愀琀栀嘀愀氀嬀欀攀礀㄀崀㬀ഀഀ
਍          椀昀 ⠀℀欀攀礀㈀⤀ 爀攀琀甀爀渀 瀀愀琀栀嘀愀氀㬀ഀഀ
          if (pathVal == null) return undefined;਍          瀀愀琀栀嘀愀氀 㴀 瀀愀琀栀嘀愀氀嬀欀攀礀㈀崀㬀ഀഀ
਍          椀昀 ⠀℀欀攀礀㌀⤀ 爀攀琀甀爀渀 瀀愀琀栀嘀愀氀㬀ഀഀ
          if (pathVal == null) return undefined;਍          瀀愀琀栀嘀愀氀 㴀 瀀愀琀栀嘀愀氀嬀欀攀礀㌀崀㬀ഀഀ
਍          椀昀 ⠀℀欀攀礀㐀⤀ 爀攀琀甀爀渀 瀀愀琀栀嘀愀氀㬀ഀഀ
          if (pathVal == null) return undefined;਍          瀀愀琀栀嘀愀氀 㴀 瀀愀琀栀嘀愀氀嬀欀攀礀㐀崀㬀ഀഀ
਍          爀攀琀甀爀渀 瀀愀琀栀嘀愀氀㬀ഀഀ
        }਍      㨀 昀甀渀挀琀椀漀渀 挀猀瀀匀愀昀攀倀爀漀洀椀猀攀䔀渀愀戀氀攀搀䜀攀琀琀攀爀⠀猀挀漀瀀攀Ⰰ 氀漀挀愀氀猀⤀ 笀ഀഀ
          var pathVal = (locals && locals.hasOwnProperty(key0)) ? locals : scope,਍              瀀爀漀洀椀猀攀㬀ഀഀ
਍          椀昀 ⠀瀀愀琀栀嘀愀氀 㴀㴀 渀甀氀氀⤀ 爀攀琀甀爀渀 瀀愀琀栀嘀愀氀㬀ഀഀ
਍          瀀愀琀栀嘀愀氀 㴀 瀀愀琀栀嘀愀氀嬀欀攀礀　崀㬀ഀഀ
          if (pathVal && pathVal.then) {਍            瀀爀漀洀椀猀攀圀愀爀渀椀渀最⠀昀甀氀氀䔀砀瀀⤀㬀ഀഀ
            if (!("$$v" in pathVal)) {਍              瀀爀漀洀椀猀攀 㴀 瀀愀琀栀嘀愀氀㬀ഀഀ
              promise.$$v = undefined;਍              瀀爀漀洀椀猀攀⸀琀栀攀渀⠀昀甀渀挀琀椀漀渀⠀瘀愀氀⤀ 笀 瀀爀漀洀椀猀攀⸀␀␀瘀 㴀 瘀愀氀㬀 紀⤀㬀ഀഀ
            }਍            瀀愀琀栀嘀愀氀 㴀 瀀愀琀栀嘀愀氀⸀␀␀瘀㬀ഀഀ
          }਍ഀഀ
          if (!key1) return pathVal;਍          椀昀 ⠀瀀愀琀栀嘀愀氀 㴀㴀 渀甀氀氀⤀ 爀攀琀甀爀渀 甀渀搀攀昀椀渀攀搀㬀ഀഀ
          pathVal = pathVal[key1];਍          椀昀 ⠀瀀愀琀栀嘀愀氀 ☀☀ 瀀愀琀栀嘀愀氀⸀琀栀攀渀⤀ 笀ഀഀ
            promiseWarning(fullExp);਍            椀昀 ⠀℀⠀∀␀␀瘀∀ 椀渀 瀀愀琀栀嘀愀氀⤀⤀ 笀ഀഀ
              promise = pathVal;਍              瀀爀漀洀椀猀攀⸀␀␀瘀 㴀 甀渀搀攀昀椀渀攀搀㬀ഀഀ
              promise.then(function(val) { promise.$$v = val; });਍            紀ഀഀ
            pathVal = pathVal.$$v;਍          紀ഀഀ
਍          椀昀 ⠀℀欀攀礀㈀⤀ 爀攀琀甀爀渀 瀀愀琀栀嘀愀氀㬀ഀഀ
          if (pathVal == null) return undefined;਍          瀀愀琀栀嘀愀氀 㴀 瀀愀琀栀嘀愀氀嬀欀攀礀㈀崀㬀ഀഀ
          if (pathVal && pathVal.then) {਍            瀀爀漀洀椀猀攀圀愀爀渀椀渀最⠀昀甀氀氀䔀砀瀀⤀㬀ഀഀ
            if (!("$$v" in pathVal)) {਍              瀀爀漀洀椀猀攀 㴀 瀀愀琀栀嘀愀氀㬀ഀഀ
              promise.$$v = undefined;਍              瀀爀漀洀椀猀攀⸀琀栀攀渀⠀昀甀渀挀琀椀漀渀⠀瘀愀氀⤀ 笀 瀀爀漀洀椀猀攀⸀␀␀瘀 㴀 瘀愀氀㬀 紀⤀㬀ഀഀ
            }਍            瀀愀琀栀嘀愀氀 㴀 瀀愀琀栀嘀愀氀⸀␀␀瘀㬀ഀഀ
          }਍ഀഀ
          if (!key3) return pathVal;਍          椀昀 ⠀瀀愀琀栀嘀愀氀 㴀㴀 渀甀氀氀⤀ 爀攀琀甀爀渀 甀渀搀攀昀椀渀攀搀㬀ഀഀ
          pathVal = pathVal[key3];਍          椀昀 ⠀瀀愀琀栀嘀愀氀 ☀☀ 瀀愀琀栀嘀愀氀⸀琀栀攀渀⤀ 笀ഀഀ
            promiseWarning(fullExp);਍            椀昀 ⠀℀⠀∀␀␀瘀∀ 椀渀 瀀愀琀栀嘀愀氀⤀⤀ 笀ഀഀ
              promise = pathVal;਍              瀀爀漀洀椀猀攀⸀␀␀瘀 㴀 甀渀搀攀昀椀渀攀搀㬀ഀഀ
              promise.then(function(val) { promise.$$v = val; });਍            紀ഀഀ
            pathVal = pathVal.$$v;਍          紀ഀഀ
਍          椀昀 ⠀℀欀攀礀㐀⤀ 爀攀琀甀爀渀 瀀愀琀栀嘀愀氀㬀ഀഀ
          if (pathVal == null) return undefined;਍          瀀愀琀栀嘀愀氀 㴀 瀀愀琀栀嘀愀氀嬀欀攀礀㐀崀㬀ഀഀ
          if (pathVal && pathVal.then) {਍            瀀爀漀洀椀猀攀圀愀爀渀椀渀最⠀昀甀氀氀䔀砀瀀⤀㬀ഀഀ
            if (!("$$v" in pathVal)) {਍              瀀爀漀洀椀猀攀 㴀 瀀愀琀栀嘀愀氀㬀ഀഀ
              promise.$$v = undefined;਍              瀀爀漀洀椀猀攀⸀琀栀攀渀⠀昀甀渀挀琀椀漀渀⠀瘀愀氀⤀ 笀 瀀爀漀洀椀猀攀⸀␀␀瘀 㴀 瘀愀氀㬀 紀⤀㬀ഀഀ
            }਍            瀀愀琀栀嘀愀氀 㴀 瀀愀琀栀嘀愀氀⸀␀␀瘀㬀ഀഀ
          }਍          爀攀琀甀爀渀 瀀愀琀栀嘀愀氀㬀ഀഀ
        };਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 猀椀洀瀀氀攀䜀攀琀琀攀爀䘀渀㄀⠀欀攀礀　Ⰰ 昀甀氀氀䔀砀瀀⤀ 笀ഀഀ
  ensureSafeMemberName(key0, fullExp);਍ഀഀ
  return function simpleGetterFn1(scope, locals) {਍    椀昀 ⠀猀挀漀瀀攀 㴀㴀 渀甀氀氀⤀ 爀攀琀甀爀渀 甀渀搀攀昀椀渀攀搀㬀ഀഀ
    return ((locals && locals.hasOwnProperty(key0)) ? locals : scope)[key0];਍  紀㬀ഀഀ
}਍ഀഀ
function simpleGetterFn2(key0, key1, fullExp) {਍  攀渀猀甀爀攀匀愀昀攀䴀攀洀戀攀爀一愀洀攀⠀欀攀礀　Ⰰ 昀甀氀氀䔀砀瀀⤀㬀ഀഀ
  ensureSafeMemberName(key1, fullExp);਍ഀഀ
  return function simpleGetterFn2(scope, locals) {਍    椀昀 ⠀猀挀漀瀀攀 㴀㴀 渀甀氀氀⤀ 爀攀琀甀爀渀 甀渀搀攀昀椀渀攀搀㬀ഀഀ
    scope = ((locals && locals.hasOwnProperty(key0)) ? locals : scope)[key0];਍    爀攀琀甀爀渀 猀挀漀瀀攀 㴀㴀 渀甀氀氀 㼀 甀渀搀攀昀椀渀攀搀 㨀 猀挀漀瀀攀嬀欀攀礀㄀崀㬀ഀഀ
  };਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 最攀琀琀攀爀䘀渀⠀瀀愀琀栀Ⰰ 漀瀀琀椀漀渀猀Ⰰ 昀甀氀氀䔀砀瀀⤀ 笀ഀഀ
  // Check whether the cache has this getter already.਍  ⼀⼀ 圀攀 挀愀渀 甀猀攀 栀愀猀伀眀渀倀爀漀瀀攀爀琀礀 搀椀爀攀挀琀氀礀 漀渀 琀栀攀 挀愀挀栀攀 戀攀挀愀甀猀攀 眀攀 攀渀猀甀爀攀Ⰰഀഀ
  // see below, that the cache never stores a path called 'hasOwnProperty'਍  椀昀 ⠀最攀琀琀攀爀䘀渀䌀愀挀栀攀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀瀀愀琀栀⤀⤀ 笀ഀഀ
    return getterFnCache[path];਍  紀ഀഀ
਍  瘀愀爀 瀀愀琀栀䬀攀礀猀 㴀 瀀愀琀栀⸀猀瀀氀椀琀⠀✀⸀✀⤀Ⰰഀഀ
      pathKeysLength = pathKeys.length,਍      昀渀㬀ഀഀ
਍  ⼀⼀ 圀栀攀渀 眀攀 栀愀瘀攀 漀渀氀礀 ㄀ 漀爀 ㈀ 琀漀欀攀渀猀Ⰰ 甀猀攀 漀瀀琀椀洀椀稀攀搀 猀瀀攀挀椀愀氀 挀愀猀攀 挀氀漀猀甀爀攀猀⸀ഀഀ
  // http://jsperf.com/angularjs-parse-getter/6਍  椀昀 ⠀℀漀瀀琀椀漀渀猀⸀甀渀眀爀愀瀀倀爀漀洀椀猀攀猀 ☀☀ 瀀愀琀栀䬀攀礀猀䰀攀渀最琀栀 㴀㴀㴀 ㄀⤀ 笀ഀഀ
    fn = simpleGetterFn1(pathKeys[0], fullExp);਍  紀 攀氀猀攀 椀昀 ⠀℀漀瀀琀椀漀渀猀⸀甀渀眀爀愀瀀倀爀漀洀椀猀攀猀 ☀☀ 瀀愀琀栀䬀攀礀猀䰀攀渀最琀栀 㴀㴀㴀 ㈀⤀ 笀ഀഀ
    fn = simpleGetterFn2(pathKeys[0], pathKeys[1], fullExp);਍  紀 攀氀猀攀 椀昀 ⠀漀瀀琀椀漀渀猀⸀挀猀瀀⤀ 笀ഀഀ
    if (pathKeysLength < 6) {਍      昀渀 㴀 挀猀瀀匀愀昀攀䜀攀琀琀攀爀䘀渀⠀瀀愀琀栀䬀攀礀猀嬀　崀Ⰰ 瀀愀琀栀䬀攀礀猀嬀㄀崀Ⰰ 瀀愀琀栀䬀攀礀猀嬀㈀崀Ⰰ 瀀愀琀栀䬀攀礀猀嬀㌀崀Ⰰ 瀀愀琀栀䬀攀礀猀嬀㐀崀Ⰰ 昀甀氀氀䔀砀瀀Ⰰഀഀ
                          options);਍    紀 攀氀猀攀 笀ഀഀ
      fn = function(scope, locals) {਍        瘀愀爀 椀 㴀 　Ⰰ 瘀愀氀㬀ഀഀ
        do {਍          瘀愀氀 㴀 挀猀瀀匀愀昀攀䜀攀琀琀攀爀䘀渀⠀瀀愀琀栀䬀攀礀猀嬀椀⬀⬀崀Ⰰ 瀀愀琀栀䬀攀礀猀嬀椀⬀⬀崀Ⰰ 瀀愀琀栀䬀攀礀猀嬀椀⬀⬀崀Ⰰ 瀀愀琀栀䬀攀礀猀嬀椀⬀⬀崀Ⰰഀഀ
                                pathKeys[i++], fullExp, options)(scope, locals);਍ഀഀ
          locals = undefined; // clear after first iteration਍          猀挀漀瀀攀 㴀 瘀愀氀㬀ഀഀ
        } while (i < pathKeysLength);਍        爀攀琀甀爀渀 瘀愀氀㬀ഀഀ
      };਍    紀ഀഀ
  } else {਍    瘀愀爀 挀漀搀攀 㴀 ✀瘀愀爀 瀀㬀尀渀✀㬀ഀഀ
    forEach(pathKeys, function(key, index) {਍      攀渀猀甀爀攀匀愀昀攀䴀攀洀戀攀爀一愀洀攀⠀欀攀礀Ⰰ 昀甀氀氀䔀砀瀀⤀㬀ഀഀ
      code += 'if(s == null) return undefined;\n' +਍              ✀猀㴀✀⬀ ⠀椀渀搀攀砀ഀഀ
                      // we simply dereference 's' on any .dot notation਍                      㼀 ✀猀✀ഀഀ
                      // but if we are first then we check locals first, and if so read it first਍                      㨀 ✀⠀⠀欀☀☀欀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀∀✀ ⬀ 欀攀礀 ⬀ ✀∀⤀⤀㼀欀㨀猀⤀✀⤀ ⬀ ✀嬀∀✀ ⬀ 欀攀礀 ⬀ ✀∀崀✀ ⬀ ✀㬀尀渀✀ ⬀ഀഀ
              (options.unwrapPromises਍                㼀 ✀椀昀 ⠀猀 ☀☀ 猀⸀琀栀攀渀⤀ 笀尀渀✀ ⬀ഀഀ
                  ' pw("' + fullExp.replace(/(["\r\n])/g, '\\$1') + '");\n' +਍                  ✀ 椀昀 ⠀℀⠀∀␀␀瘀∀ 椀渀 猀⤀⤀ 笀尀渀✀ ⬀ഀഀ
                    ' p=s;\n' +਍                    ✀ 瀀⸀␀␀瘀 㴀 甀渀搀攀昀椀渀攀搀㬀尀渀✀ ⬀ഀഀ
                    ' p.then(function(v) {p.$$v=v;});\n' +਍                    ✀紀尀渀✀ ⬀ഀഀ
                  ' s=s.$$v\n' +਍                ✀紀尀渀✀ഀഀ
                : '');਍    紀⤀㬀ഀഀ
    code += 'return s;';਍ഀഀ
    /* jshint -W054 */਍    瘀愀爀 攀瘀愀氀攀搀䘀渀䜀攀琀琀攀爀 㴀 渀攀眀 䘀甀渀挀琀椀漀渀⠀✀猀✀Ⰰ ✀欀✀Ⰰ ✀瀀眀✀Ⰰ 挀漀搀攀⤀㬀 ⼀⼀ 猀㴀猀挀漀瀀攀Ⰰ 欀㴀氀漀挀愀氀猀Ⰰ 瀀眀㴀瀀爀漀洀椀猀攀圀愀爀渀椀渀最ഀഀ
    /* jshint +W054 */਍    攀瘀愀氀攀搀䘀渀䜀攀琀琀攀爀⸀琀漀匀琀爀椀渀最 㴀 瘀愀氀甀攀䘀渀⠀挀漀搀攀⤀㬀ഀഀ
    fn = options.unwrapPromises ? function(scope, locals) {਍      爀攀琀甀爀渀 攀瘀愀氀攀搀䘀渀䜀攀琀琀攀爀⠀猀挀漀瀀攀Ⰰ 氀漀挀愀氀猀Ⰰ 瀀爀漀洀椀猀攀圀愀爀渀椀渀最⤀㬀ഀഀ
    } : evaledFnGetter;਍  紀ഀഀ
਍  ⼀⼀ 伀渀氀礀 挀愀挀栀攀 琀栀攀 瘀愀氀甀攀 椀昀 椀琀✀猀 渀漀琀 最漀椀渀最 琀漀 洀攀猀猀 甀瀀 琀栀攀 挀愀挀栀攀 漀戀樀攀挀琀ഀഀ
  // This is more performant that using Object.prototype.hasOwnProperty.call਍  椀昀 ⠀瀀愀琀栀 ℀㴀㴀 ✀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀✀⤀ 笀ഀഀ
    getterFnCache[path] = fn;਍  紀ഀഀ
  return fn;਍紀ഀഀ
਍⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 渀最⸀␀瀀愀爀猀攀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ഀഀ
 * Converts Angular {@link guide/expression expression} into a function.਍ ⨀ഀഀ
 * <pre>਍ ⨀   瘀愀爀 最攀琀琀攀爀 㴀 ␀瀀愀爀猀攀⠀✀甀猀攀爀⸀渀愀洀攀✀⤀㬀ഀഀ
 *   var setter = getter.assign;਍ ⨀   瘀愀爀 挀漀渀琀攀砀琀 㴀 笀甀猀攀爀㨀笀渀愀洀攀㨀✀愀渀最甀氀愀爀✀紀紀㬀ഀഀ
 *   var locals = {user:{name:'local'}};਍ ⨀ഀഀ
 *   expect(getter(context)).toEqual('angular');਍ ⨀   猀攀琀琀攀爀⠀挀漀渀琀攀砀琀Ⰰ ✀渀攀眀嘀愀氀甀攀✀⤀㬀ഀഀ
 *   expect(context.user.name).toEqual('newValue');਍ ⨀   攀砀瀀攀挀琀⠀最攀琀琀攀爀⠀挀漀渀琀攀砀琀Ⰰ 氀漀挀愀氀猀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀氀漀挀愀氀✀⤀㬀ഀഀ
 * </pre>਍ ⨀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 攀砀瀀爀攀猀猀椀漀渀 匀琀爀椀渀最 攀砀瀀爀攀猀猀椀漀渀 琀漀 挀漀洀瀀椀氀攀⸀ഀഀ
 * @returns {function(context, locals)} a function which represents the compiled expression:਍ ⨀ഀഀ
 *    * `context` �� `{object}` �� an object against which any expressions embedded in the strings਍ ⨀      愀爀攀 攀瘀愀氀甀愀琀攀搀 愀最愀椀渀猀琀 ⠀琀礀瀀椀挀愀氀氀礀 愀 猀挀漀瀀攀 漀戀樀攀挀琀⤀⸀ഀഀ
 *    * `locals` �� `{object=}` �� local variables context object, useful for overriding values in਍ ⨀      怀挀漀渀琀攀砀琀怀⸀ഀഀ
 *਍ ⨀    吀栀攀 爀攀琀甀爀渀攀搀 昀甀渀挀琀椀漀渀 愀氀猀漀 栀愀猀 琀栀攀 昀漀氀氀漀眀椀渀最 瀀爀漀瀀攀爀琀椀攀猀㨀ഀഀ
 *      * `literal` �� `{boolean}` �� whether the expression's top-level node is a JavaScript਍ ⨀        氀椀琀攀爀愀氀⸀ഀഀ
 *      * `constant` �� `{boolean}` �� whether the expression is made entirely of JavaScript਍ ⨀        挀漀渀猀琀愀渀琀 氀椀琀攀爀愀氀猀⸀ഀഀ
 *      * `assign` �� `{?function(context, value)}` �� if the expression is assignable, this will be਍ ⨀        猀攀琀 琀漀 愀 昀甀渀挀琀椀漀渀 琀漀 挀栀愀渀最攀 椀琀猀 瘀愀氀甀攀 漀渀 琀栀攀 最椀瘀攀渀 挀漀渀琀攀砀琀⸀ഀഀ
 *਍ ⨀⼀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name ng.$parseProvider਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * `$parseProvider` can be used for configuring the default behavior of the {@link ng.$parse $parse}਍ ⨀  猀攀爀瘀椀挀攀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 ␀倀愀爀猀攀倀爀漀瘀椀搀攀爀⠀⤀ 笀ഀഀ
  var cache = {};਍ഀഀ
  var $parseOptions = {਍    挀猀瀀㨀 昀愀氀猀攀Ⰰഀഀ
    unwrapPromises: false,਍    氀漀最倀爀漀洀椀猀攀圀愀爀渀椀渀最猀㨀 琀爀甀攀ഀഀ
  };਍ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @deprecated Promise unwrapping via $parse is deprecated and will be removed in the future.਍   ⨀ഀഀ
   * @ngdoc method਍   ⨀ 䀀渀愀洀攀 渀最⸀␀瀀愀爀猀攀倀爀漀瘀椀搀攀爀⌀甀渀眀爀愀瀀倀爀漀洀椀猀攀猀ഀഀ
   * @methodOf ng.$parseProvider਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   *਍   ⨀ ⨀⨀吀栀椀猀 昀攀愀琀甀爀攀 椀猀 搀攀瀀爀攀挀愀琀攀搀Ⰰ 猀攀攀 搀攀瀀爀攀挀愀琀椀漀渀 渀漀琀攀猀 戀攀氀漀眀 昀漀爀 洀漀爀攀 椀渀昀漀⨀⨀ഀഀ
   *਍   ⨀ 䤀昀 猀攀琀 琀漀 琀爀甀攀 ⠀搀攀昀愀甀氀琀 椀猀 昀愀氀猀攀⤀Ⰰ ␀瀀愀爀猀攀 眀椀氀氀 甀渀眀爀愀瀀 瀀爀漀洀椀猀攀猀 愀甀琀漀洀愀琀椀挀愀氀氀礀 眀栀攀渀 愀 瀀爀漀洀椀猀攀 椀猀ഀഀ
   * found at any part of the expression. In other words, if set to true, the expression will always਍   ⨀ 爀攀猀甀氀琀 椀渀 愀 渀漀渀ⴀ瀀爀漀洀椀猀攀 瘀愀氀甀攀⸀ഀഀ
   *਍   ⨀ 圀栀椀氀攀 琀栀攀 瀀爀漀洀椀猀攀 椀猀 甀渀爀攀猀漀氀瘀攀搀Ⰰ 椀琀✀猀 琀爀攀愀琀攀搀 愀猀 甀渀搀攀昀椀渀攀搀Ⰰ 戀甀琀 漀渀挀攀 爀攀猀漀氀瘀攀搀 愀渀搀 昀甀氀昀椀氀氀攀搀Ⰰഀഀ
   * the fulfillment value is used in place of the promise while evaluating the expression.਍   ⨀ഀഀ
   * **Deprecation notice**਍   ⨀ഀഀ
   * This is a feature that didn't prove to be wildly useful or popular, primarily because of the਍   ⨀ 搀椀挀栀漀琀漀洀礀 戀攀琀眀攀攀渀 搀愀琀愀 愀挀挀攀猀猀 椀渀 琀攀洀瀀氀愀琀攀猀 ⠀愀挀挀攀猀猀攀搀 愀猀 爀愀眀 瘀愀氀甀攀猀⤀ 愀渀搀 挀漀渀琀爀漀氀氀攀爀 挀漀搀攀ഀഀ
   * (accessed as promises).਍   ⨀ഀഀ
   * In most code we ended up resolving promises manually in controllers anyway and thus unifying਍   ⨀ 琀栀攀 洀漀搀攀氀 愀挀挀攀猀猀 琀栀攀爀攀⸀ഀഀ
   *਍   ⨀ 伀琀栀攀爀 搀漀眀渀猀椀搀攀猀 漀昀 愀甀琀漀洀愀琀椀挀 瀀爀漀洀椀猀攀 甀渀眀爀愀瀀瀀椀渀最㨀ഀഀ
   *਍   ⨀ ⴀ 眀栀攀渀 戀甀椀氀搀椀渀最 挀漀洀瀀漀渀攀渀琀猀 椀琀✀猀 漀昀琀攀渀 搀攀猀椀爀愀戀氀攀 琀漀 爀攀挀攀椀瘀攀 琀栀攀 爀愀眀 瀀爀漀洀椀猀攀猀ഀഀ
   * - adds complexity and slows down expression evaluation਍   ⨀ ⴀ 洀愀欀攀猀 攀砀瀀爀攀猀猀椀漀渀 挀漀搀攀 瀀爀攀ⴀ最攀渀攀爀愀琀椀漀渀 甀渀愀琀琀爀愀挀琀椀瘀攀 搀甀攀 琀漀 琀栀攀 愀洀漀甀渀琀 漀昀 挀漀搀攀 琀栀愀琀 渀攀攀搀猀 琀漀 戀攀ഀഀ
   *   generated਍   ⨀ ⴀ 洀愀欀攀猀 䤀䐀䔀 愀甀琀漀ⴀ挀漀洀瀀氀攀琀椀漀渀 愀渀搀 琀漀漀氀 猀甀瀀瀀漀爀琀 栀愀爀搀ഀഀ
   *਍   ⨀ ⨀⨀圀愀爀渀椀渀最 䰀漀最猀⨀⨀ഀഀ
   *਍   ⨀ 䤀昀 琀栀攀 甀渀眀爀愀瀀瀀椀渀最 椀猀 攀渀愀戀氀攀搀Ⰰ 䄀渀最甀氀愀爀 眀椀氀氀 氀漀最 愀 眀愀爀渀椀渀最 愀戀漀甀琀 攀愀挀栀 攀砀瀀爀攀猀猀椀漀渀 琀栀愀琀 甀渀眀爀愀瀀猀 愀ഀഀ
   * promise (to reduce the noise, each expression is logged only once). To disable this logging use਍   ⨀ 怀␀瀀愀爀猀攀倀爀漀瘀椀搀攀爀⸀氀漀最倀爀漀洀椀猀攀圀愀爀渀椀渀最猀⠀昀愀氀猀攀⤀怀 愀瀀椀⸀ഀഀ
   *਍   ⨀ഀഀ
   * @param {boolean=} value New value.਍   ⨀ 䀀爀攀琀甀爀渀猀 笀戀漀漀氀攀愀渀簀猀攀氀昀紀 刀攀琀甀爀渀猀 琀栀攀 挀甀爀爀攀渀琀 猀攀琀琀椀渀最 眀栀攀渀 甀猀攀搀 愀猀 最攀琀琀攀爀 愀渀搀 猀攀氀昀 椀昀 甀猀攀搀 愀猀ഀഀ
   *                         setter.਍   ⨀⼀ഀഀ
  this.unwrapPromises = function(value) {਍    椀昀 ⠀椀猀䐀攀昀椀渀攀搀⠀瘀愀氀甀攀⤀⤀ 笀ഀഀ
      $parseOptions.unwrapPromises = !!value;਍      爀攀琀甀爀渀 琀栀椀猀㬀ഀഀ
    } else {਍      爀攀琀甀爀渀 ␀瀀愀爀猀攀伀瀀琀椀漀渀猀⸀甀渀眀爀愀瀀倀爀漀洀椀猀攀猀㬀ഀഀ
    }਍  紀㬀ഀഀ
਍ഀഀ
  /**਍   ⨀ 䀀搀攀瀀爀攀挀愀琀攀搀 倀爀漀洀椀猀攀 甀渀眀爀愀瀀瀀椀渀最 瘀椀愀 ␀瀀愀爀猀攀 椀猀 搀攀瀀爀攀挀愀琀攀搀 愀渀搀 眀椀氀氀 戀攀 爀攀洀漀瘀攀搀 椀渀 琀栀攀 昀甀琀甀爀攀⸀ഀഀ
   *਍   ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
   * @name ng.$parseProvider#logPromiseWarnings਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀瀀愀爀猀攀倀爀漀瘀椀搀攀爀ഀഀ
   * @description਍   ⨀ഀഀ
   * Controls whether Angular should log a warning on any encounter of a promise in an expression.਍   ⨀ഀഀ
   * The default is set to `true`.਍   ⨀ഀഀ
   * This setting applies only if `$parseProvider.unwrapPromises` setting is set to true as well.਍   ⨀ഀഀ
   * @param {boolean=} value New value.਍   ⨀ 䀀爀攀琀甀爀渀猀 笀戀漀漀氀攀愀渀簀猀攀氀昀紀 刀攀琀甀爀渀猀 琀栀攀 挀甀爀爀攀渀琀 猀攀琀琀椀渀最 眀栀攀渀 甀猀攀搀 愀猀 最攀琀琀攀爀 愀渀搀 猀攀氀昀 椀昀 甀猀攀搀 愀猀ഀഀ
   *                         setter.਍   ⨀⼀ഀഀ
 this.logPromiseWarnings = function(value) {਍    椀昀 ⠀椀猀䐀攀昀椀渀攀搀⠀瘀愀氀甀攀⤀⤀ 笀ഀഀ
      $parseOptions.logPromiseWarnings = value;਍      爀攀琀甀爀渀 琀栀椀猀㬀ഀഀ
    } else {਍      爀攀琀甀爀渀 ␀瀀愀爀猀攀伀瀀琀椀漀渀猀⸀氀漀最倀爀漀洀椀猀攀圀愀爀渀椀渀最猀㬀ഀഀ
    }਍  紀㬀ഀഀ
਍ഀഀ
  this.$get = ['$filter', '$sniffer', '$log', function($filter, $sniffer, $log) {਍    ␀瀀愀爀猀攀伀瀀琀椀漀渀猀⸀挀猀瀀 㴀 ␀猀渀椀昀昀攀爀⸀挀猀瀀㬀ഀഀ
਍    瀀爀漀洀椀猀攀圀愀爀渀椀渀最 㴀 昀甀渀挀琀椀漀渀 瀀爀漀洀椀猀攀圀愀爀渀椀渀最䘀渀⠀昀甀氀氀䔀砀瀀⤀ 笀ഀഀ
      if (!$parseOptions.logPromiseWarnings || promiseWarningCache.hasOwnProperty(fullExp)) return;਍      瀀爀漀洀椀猀攀圀愀爀渀椀渀最䌀愀挀栀攀嬀昀甀氀氀䔀砀瀀崀 㴀 琀爀甀攀㬀ഀഀ
      $log.warn('[$parse] Promise found in the expression `' + fullExp + '`. ' +਍          ✀䄀甀琀漀洀愀琀椀挀 甀渀眀爀愀瀀瀀椀渀最 漀昀 瀀爀漀洀椀猀攀猀 椀渀 䄀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀猀 椀猀 搀攀瀀爀攀挀愀琀攀搀⸀✀⤀㬀ഀഀ
    };਍ഀഀ
    return function(exp) {਍      瘀愀爀 瀀愀爀猀攀搀䔀砀瀀爀攀猀猀椀漀渀㬀ഀഀ
਍      猀眀椀琀挀栀 ⠀琀礀瀀攀漀昀 攀砀瀀⤀ 笀ഀഀ
        case 'string':਍ഀഀ
          if (cache.hasOwnProperty(exp)) {਍            爀攀琀甀爀渀 挀愀挀栀攀嬀攀砀瀀崀㬀ഀഀ
          }਍ഀഀ
          var lexer = new Lexer($parseOptions);਍          瘀愀爀 瀀愀爀猀攀爀 㴀 渀攀眀 倀愀爀猀攀爀⠀氀攀砀攀爀Ⰰ ␀昀椀氀琀攀爀Ⰰ ␀瀀愀爀猀攀伀瀀琀椀漀渀猀⤀㬀ഀഀ
          parsedExpression = parser.parse(exp, false);਍ഀഀ
          if (exp !== 'hasOwnProperty') {਍            ⼀⼀ 伀渀氀礀 挀愀挀栀攀 琀栀攀 瘀愀氀甀攀 椀昀 椀琀✀猀 渀漀琀 最漀椀渀最 琀漀 洀攀猀猀 甀瀀 琀栀攀 挀愀挀栀攀 漀戀樀攀挀琀ഀഀ
            // This is more performant that using Object.prototype.hasOwnProperty.call਍            挀愀挀栀攀嬀攀砀瀀崀 㴀 瀀愀爀猀攀搀䔀砀瀀爀攀猀猀椀漀渀㬀ഀഀ
          }਍ഀഀ
          return parsedExpression;਍ഀഀ
        case 'function':਍          爀攀琀甀爀渀 攀砀瀀㬀ഀഀ
਍        搀攀昀愀甀氀琀㨀ഀഀ
          return noop;਍      紀ഀഀ
    };਍  紀崀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 猀攀爀瘀椀挀攀ഀഀ
 * @name ng.$q਍ ⨀ 䀀爀攀焀甀椀爀攀猀 ␀爀漀漀琀匀挀漀瀀攀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * A promise/deferred implementation inspired by [Kris Kowal's Q](https://github.com/kriskowal/q).਍ ⨀ഀഀ
 * [The CommonJS Promise proposal](http://wiki.commonjs.org/wiki/Promises) describes a promise as an਍ ⨀ 椀渀琀攀爀昀愀挀攀 昀漀爀 椀渀琀攀爀愀挀琀椀渀最 眀椀琀栀 愀渀 漀戀樀攀挀琀 琀栀愀琀 爀攀瀀爀攀猀攀渀琀猀 琀栀攀 爀攀猀甀氀琀 漀昀 愀渀 愀挀琀椀漀渀 琀栀愀琀 椀猀ഀഀ
 * performed asynchronously, and may or may not be finished at any given point in time.਍ ⨀ഀഀ
 * From the perspective of dealing with error handling, deferred and promise APIs are to਍ ⨀ 愀猀礀渀挀栀爀漀渀漀甀猀 瀀爀漀最爀愀洀洀椀渀最 眀栀愀琀 怀琀爀礀怀Ⰰ 怀挀愀琀挀栀怀 愀渀搀 怀琀栀爀漀眀怀 欀攀礀眀漀爀搀猀 愀爀攀 琀漀 猀礀渀挀栀爀漀渀漀甀猀 瀀爀漀最爀愀洀洀椀渀最⸀ഀഀ
 *਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   // for the purpose of this example let's assume that variables `$q` and `scope` are਍ ⨀   ⼀⼀ 愀瘀愀椀氀愀戀氀攀 椀渀 琀栀攀 挀甀爀爀攀渀琀 氀攀砀椀挀愀氀 猀挀漀瀀攀 ⠀琀栀攀礀 挀漀甀氀搀 栀愀瘀攀 戀攀攀渀 椀渀樀攀挀琀攀搀 漀爀 瀀愀猀猀攀搀 椀渀⤀⸀ഀഀ
 *਍ ⨀   昀甀渀挀琀椀漀渀 愀猀礀渀挀䜀爀攀攀琀⠀渀愀洀攀⤀ 笀ഀഀ
 *     var deferred = $q.defer();਍ ⨀ഀഀ
 *     setTimeout(function() {਍ ⨀       ⼀⼀ 猀椀渀挀攀 琀栀椀猀 昀渀 攀砀攀挀甀琀攀猀 愀猀礀渀挀 椀渀 愀 昀甀琀甀爀攀 琀甀爀渀 漀昀 琀栀攀 攀瘀攀渀琀 氀漀漀瀀Ⰰ 眀攀 渀攀攀搀 琀漀 眀爀愀瀀ഀഀ
 *       // our code into an $apply call so that the model changes are properly observed.਍ ⨀       猀挀漀瀀攀⸀␀愀瀀瀀氀礀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
 *         deferred.notify('About to greet ' + name + '.');਍ ⨀ഀഀ
 *         if (okToGreet(name)) {਍ ⨀           搀攀昀攀爀爀攀搀⸀爀攀猀漀氀瘀攀⠀✀䠀攀氀氀漀Ⰰ ✀ ⬀ 渀愀洀攀 ⬀ ✀℀✀⤀㬀ഀഀ
 *         } else {਍ ⨀           搀攀昀攀爀爀攀搀⸀爀攀樀攀挀琀⠀✀䜀爀攀攀琀椀渀最 ✀ ⬀ 渀愀洀攀 ⬀ ✀ 椀猀 渀漀琀 愀氀氀漀眀攀搀⸀✀⤀㬀ഀഀ
 *         }਍ ⨀       紀⤀㬀ഀഀ
 *     }, 1000);਍ ⨀ഀഀ
 *     return deferred.promise;਍ ⨀   紀ഀഀ
 *਍ ⨀   瘀愀爀 瀀爀漀洀椀猀攀 㴀 愀猀礀渀挀䜀爀攀攀琀⠀✀刀漀戀椀渀 䠀漀漀搀✀⤀㬀ഀഀ
 *   promise.then(function(greeting) {਍ ⨀     愀氀攀爀琀⠀✀匀甀挀挀攀猀猀㨀 ✀ ⬀ 最爀攀攀琀椀渀最⤀㬀ഀഀ
 *   }, function(reason) {਍ ⨀     愀氀攀爀琀⠀✀䘀愀椀氀攀搀㨀 ✀ ⬀ 爀攀愀猀漀渀⤀㬀ഀഀ
 *   }, function(update) {਍ ⨀     愀氀攀爀琀⠀✀䜀漀琀 渀漀琀椀昀椀挀愀琀椀漀渀㨀 ✀ ⬀ 甀瀀搀愀琀攀⤀㬀ഀഀ
 *   });਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 䄀琀 昀椀爀猀琀 椀琀 洀椀最栀琀 渀漀琀 戀攀 漀戀瘀椀漀甀猀 眀栀礀 琀栀椀猀 攀砀琀爀愀 挀漀洀瀀氀攀砀椀琀礀 椀猀 眀漀爀琀栀 琀栀攀 琀爀漀甀戀氀攀⸀ 吀栀攀 瀀愀礀漀昀昀ഀഀ
 * comes in the way of guarantees that promise and deferred APIs make, see਍ ⨀ 栀琀琀瀀猀㨀⼀⼀最椀琀栀甀戀⸀挀漀洀⼀欀爀椀猀欀漀眀愀氀⼀甀渀挀漀洀洀漀渀樀猀⼀戀氀漀戀⼀洀愀猀琀攀爀⼀瀀爀漀洀椀猀攀猀⼀猀瀀攀挀椀昀椀挀愀琀椀漀渀⸀洀搀⸀ഀഀ
 *਍ ⨀ 䄀搀搀椀琀椀漀渀愀氀氀礀 琀栀攀 瀀爀漀洀椀猀攀 愀瀀椀 愀氀氀漀眀猀 昀漀爀 挀漀洀瀀漀猀椀琀椀漀渀 琀栀愀琀 椀猀 瘀攀爀礀 栀愀爀搀 琀漀 搀漀 眀椀琀栀 琀栀攀ഀഀ
 * traditional callback ([CPS](http://en.wikipedia.org/wiki/Continuation-passing_style)) approach.਍ ⨀ 䘀漀爀 洀漀爀攀 漀渀 琀栀椀猀 瀀氀攀愀猀攀 猀攀攀 琀栀攀 嬀儀 搀漀挀甀洀攀渀琀愀琀椀漀渀崀⠀栀琀琀瀀猀㨀⼀⼀最椀琀栀甀戀⸀挀漀洀⼀欀爀椀猀欀漀眀愀氀⼀焀⤀ 攀猀瀀攀挀椀愀氀氀礀 琀栀攀ഀഀ
 * section on serial or parallel joining of promises.਍ ⨀ഀഀ
 *਍ ⨀ ⌀ 吀栀攀 䐀攀昀攀爀爀攀搀 䄀倀䤀ഀഀ
 *਍ ⨀ 䄀 渀攀眀 椀渀猀琀愀渀挀攀 漀昀 搀攀昀攀爀爀攀搀 椀猀 挀漀渀猀琀爀甀挀琀攀搀 戀礀 挀愀氀氀椀渀最 怀␀焀⸀搀攀昀攀爀⠀⤀怀⸀ഀഀ
 *਍ ⨀ 吀栀攀 瀀甀爀瀀漀猀攀 漀昀 琀栀攀 搀攀昀攀爀爀攀搀 漀戀樀攀挀琀 椀猀 琀漀 攀砀瀀漀猀攀 琀栀攀 愀猀猀漀挀椀愀琀攀搀 倀爀漀洀椀猀攀 椀渀猀琀愀渀挀攀 愀猀 眀攀氀氀 愀猀 䄀倀䤀猀ഀഀ
 * that can be used for signaling the successful or unsuccessful completion, as well as the status਍ ⨀ 漀昀 琀栀攀 琀愀猀欀⸀ഀഀ
 *਍ ⨀ ⨀⨀䴀攀琀栀漀搀猀⨀⨀ഀഀ
 *਍ ⨀ ⴀ 怀爀攀猀漀氀瘀攀⠀瘀愀氀甀攀⤀怀 ﴀ﷿⃿爀攀猀漀氀瘀攀猀 琀栀攀 搀攀爀椀瘀攀搀 瀀爀漀洀椀猀攀 眀椀琀栀 琀栀攀 怀瘀愀氀甀攀怀⸀ 䤀昀 琀栀攀 瘀愀氀甀攀 椀猀 愀 爀攀樀攀挀琀椀漀渀ഀഀ
 *   constructed via `$q.reject`, the promise will be rejected instead.਍ ⨀ ⴀ 怀爀攀樀攀挀琀⠀爀攀愀猀漀渀⤀怀 ﴀ﷿⃿爀攀樀攀挀琀猀 琀栀攀 搀攀爀椀瘀攀搀 瀀爀漀洀椀猀攀 眀椀琀栀 琀栀攀 怀爀攀愀猀漀渀怀⸀ 吀栀椀猀 椀猀 攀焀甀椀瘀愀氀攀渀琀 琀漀ഀഀ
 *   resolving it with a rejection constructed via `$q.reject`.਍ ⨀ ⴀ 怀渀漀琀椀昀礀⠀瘀愀氀甀攀⤀怀 ⴀ 瀀爀漀瘀椀搀攀猀 甀瀀搀愀琀攀猀 漀渀 琀栀攀 猀琀愀琀甀猀 漀昀 琀栀攀 瀀爀漀洀椀猀攀猀 攀砀攀挀甀琀椀漀渀⸀ 吀栀椀猀 洀愀礀 戀攀 挀愀氀氀攀搀ഀഀ
 *   multiple times before the promise is either resolved or rejected.਍ ⨀ഀഀ
 * **Properties**਍ ⨀ഀഀ
 * - promise �� `{Promise}` �� promise object associated with this deferred.਍ ⨀ഀഀ
 *਍ ⨀ ⌀ 吀栀攀 倀爀漀洀椀猀攀 䄀倀䤀ഀഀ
 *਍ ⨀ 䄀 渀攀眀 瀀爀漀洀椀猀攀 椀渀猀琀愀渀挀攀 椀猀 挀爀攀愀琀攀搀 眀栀攀渀 愀 搀攀昀攀爀爀攀搀 椀渀猀琀愀渀挀攀 椀猀 挀爀攀愀琀攀搀 愀渀搀 挀愀渀 戀攀 爀攀琀爀椀攀瘀攀搀 戀礀ഀഀ
 * calling `deferred.promise`.਍ ⨀ഀഀ
 * The purpose of the promise object is to allow for interested parties to get access to the result਍ ⨀ 漀昀 琀栀攀 搀攀昀攀爀爀攀搀 琀愀猀欀 眀栀攀渀 椀琀 挀漀洀瀀氀攀琀攀猀⸀ഀഀ
 *਍ ⨀ ⨀⨀䴀攀琀栀漀搀猀⨀⨀ഀഀ
 *਍ ⨀ ⴀ 怀琀栀攀渀⠀猀甀挀挀攀猀猀䌀愀氀氀戀愀挀欀Ⰰ 攀爀爀漀爀䌀愀氀氀戀愀挀欀Ⰰ 渀漀琀椀昀礀䌀愀氀氀戀愀挀欀⤀怀 ﴀ﷿⃿爀攀最愀爀搀氀攀猀猀 漀昀 眀栀攀渀 琀栀攀 瀀爀漀洀椀猀攀 眀愀猀 漀爀ഀഀ
 *   will be resolved or rejected, `then` calls one of the success or error callbacks asynchronously਍ ⨀   愀猀 猀漀漀渀 愀猀 琀栀攀 爀攀猀甀氀琀 椀猀 愀瘀愀椀氀愀戀氀攀⸀ 吀栀攀 挀愀氀氀戀愀挀欀猀 愀爀攀 挀愀氀氀攀搀 眀椀琀栀 愀 猀椀渀最氀攀 愀爀最甀洀攀渀琀㨀 琀栀攀 爀攀猀甀氀琀ഀഀ
 *   or rejection reason. Additionally, the notify callback may be called zero or more times to਍ ⨀   瀀爀漀瘀椀搀攀 愀 瀀爀漀最爀攀猀猀 椀渀搀椀挀愀琀椀漀渀Ⰰ 戀攀昀漀爀攀 琀栀攀 瀀爀漀洀椀猀攀 椀猀 爀攀猀漀氀瘀攀搀 漀爀 爀攀樀攀挀琀攀搀⸀ഀഀ
 *਍ ⨀   吀栀椀猀 洀攀琀栀漀搀 ⨀爀攀琀甀爀渀猀 愀 渀攀眀 瀀爀漀洀椀猀攀⨀ 眀栀椀挀栀 椀猀 爀攀猀漀氀瘀攀搀 漀爀 爀攀樀攀挀琀攀搀 瘀椀愀 琀栀攀 爀攀琀甀爀渀 瘀愀氀甀攀 漀昀 琀栀攀ഀഀ
 *   `successCallback`, `errorCallback`. It also notifies via the return value of the਍ ⨀   怀渀漀琀椀昀礀䌀愀氀氀戀愀挀欀怀 洀攀琀栀漀搀⸀ 吀栀攀 瀀爀漀洀椀猀攀 挀愀渀 渀漀琀 戀攀 爀攀猀漀氀瘀攀搀 漀爀 爀攀樀攀挀琀攀搀 昀爀漀洀 琀栀攀 渀漀琀椀昀礀䌀愀氀氀戀愀挀欀ഀഀ
 *   method.਍ ⨀ഀഀ
 * - `catch(errorCallback)` �� shorthand for `promise.then(null, errorCallback)`਍ ⨀ഀഀ
 * - `finally(callback)` �� allows you to observe either the fulfillment or rejection of a promise,਍ ⨀   戀甀琀 琀漀 搀漀 猀漀 眀椀琀栀漀甀琀 洀漀搀椀昀礀椀渀最 琀栀攀 昀椀渀愀氀 瘀愀氀甀攀⸀ 吀栀椀猀 椀猀 甀猀攀昀甀氀 琀漀 爀攀氀攀愀猀攀 爀攀猀漀甀爀挀攀猀 漀爀 搀漀 猀漀洀攀ഀഀ
 *   clean-up that needs to be done whether the promise was rejected or resolved. See the [full਍ ⨀   猀瀀攀挀椀昀椀挀愀琀椀漀渀崀⠀栀琀琀瀀猀㨀⼀⼀最椀琀栀甀戀⸀挀漀洀⼀欀爀椀猀欀漀眀愀氀⼀焀⼀眀椀欀椀⼀䄀倀䤀ⴀ刀攀昀攀爀攀渀挀攀⌀瀀爀漀洀椀猀攀昀椀渀愀氀氀礀挀愀氀氀戀愀挀欀⤀ 昀漀爀ഀഀ
 *   more information.਍ ⨀ഀഀ
 *   Because `finally` is a reserved word in JavaScript and reserved keywords are not supported as਍ ⨀   瀀爀漀瀀攀爀琀礀 渀愀洀攀猀 戀礀 䔀匀㌀Ⰰ 礀漀甀✀氀氀 渀攀攀搀 琀漀 椀渀瘀漀欀攀 琀栀攀 洀攀琀栀漀搀 氀椀欀攀 怀瀀爀漀洀椀猀攀嬀✀昀椀渀愀氀氀礀✀崀⠀挀愀氀氀戀愀挀欀⤀怀 琀漀ഀഀ
 *   make your code IE8 compatible.਍ ⨀ഀഀ
 * # Chaining promises਍ ⨀ഀഀ
 * Because calling the `then` method of a promise returns a new derived promise, it is easily਍ ⨀ 瀀漀猀猀椀戀氀攀 琀漀 挀爀攀愀琀攀 愀 挀栀愀椀渀 漀昀 瀀爀漀洀椀猀攀猀㨀ഀഀ
 *਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   promiseB = promiseA.then(function(result) {਍ ⨀     爀攀琀甀爀渀 爀攀猀甀氀琀 ⬀ ㄀㬀ഀഀ
 *   });਍ ⨀ഀഀ
 *   // promiseB will be resolved immediately after promiseA is resolved and its value਍ ⨀   ⼀⼀ 眀椀氀氀 戀攀 琀栀攀 爀攀猀甀氀琀 漀昀 瀀爀漀洀椀猀攀䄀 椀渀挀爀攀洀攀渀琀攀搀 戀礀 ㄀ഀഀ
 * </pre>਍ ⨀ഀഀ
 * It is possible to create chains of any length and since a promise can be resolved with another਍ ⨀ 瀀爀漀洀椀猀攀 ⠀眀栀椀挀栀 眀椀氀氀 搀攀昀攀爀 椀琀猀 爀攀猀漀氀甀琀椀漀渀 昀甀爀琀栀攀爀⤀Ⰰ 椀琀 椀猀 瀀漀猀猀椀戀氀攀 琀漀 瀀愀甀猀攀⼀搀攀昀攀爀 爀攀猀漀氀甀琀椀漀渀 漀昀ഀഀ
 * the promises at any point in the chain. This makes it possible to implement powerful APIs like਍ ⨀ ␀栀琀琀瀀✀猀 爀攀猀瀀漀渀猀攀 椀渀琀攀爀挀攀瀀琀漀爀猀⸀ഀഀ
 *਍ ⨀ഀഀ
 * # Differences between Kris Kowal's Q and $q਍ ⨀ഀഀ
 *  There are two main differences:਍ ⨀ഀഀ
 * - $q is integrated with the {@link ng.$rootScope.Scope} Scope model observation਍ ⨀   洀攀挀栀愀渀椀猀洀 椀渀 愀渀最甀氀愀爀Ⰰ 眀栀椀挀栀 洀攀愀渀猀 昀愀猀琀攀爀 瀀爀漀瀀愀最愀琀椀漀渀 漀昀 爀攀猀漀氀甀琀椀漀渀 漀爀 爀攀樀攀挀琀椀漀渀 椀渀琀漀 礀漀甀爀ഀഀ
 *   models and avoiding unnecessary browser repaints, which would result in flickering UI.਍ ⨀ ⴀ 儀 栀愀猀 洀愀渀礀 洀漀爀攀 昀攀愀琀甀爀攀猀 琀栀愀渀 ␀焀Ⰰ 戀甀琀 琀栀愀琀 挀漀洀攀猀 愀琀 愀 挀漀猀琀 漀昀 戀礀琀攀猀⸀ ␀焀 椀猀 琀椀渀礀Ⰰ 戀甀琀 挀漀渀琀愀椀渀猀ഀഀ
 *   all the important functionality needed for common async tasks.਍ ⨀ഀഀ
 *  # Testing਍ ⨀ഀഀ
 *  <pre>਍ ⨀    椀琀⠀✀猀栀漀甀氀搀 猀椀洀甀氀愀琀攀 瀀爀漀洀椀猀攀✀Ⰰ 椀渀樀攀挀琀⠀昀甀渀挀琀椀漀渀⠀␀焀Ⰰ ␀爀漀漀琀匀挀漀瀀攀⤀ 笀ഀഀ
 *      var deferred = $q.defer();਍ ⨀      瘀愀爀 瀀爀漀洀椀猀攀 㴀 搀攀昀攀爀爀攀搀⸀瀀爀漀洀椀猀攀㬀ഀഀ
 *      var resolvedValue;਍ ⨀ഀഀ
 *      promise.then(function(value) { resolvedValue = value; });਍ ⨀      攀砀瀀攀挀琀⠀爀攀猀漀氀瘀攀搀嘀愀氀甀攀⤀⸀琀漀䈀攀唀渀搀攀昀椀渀攀搀⠀⤀㬀ഀഀ
 *਍ ⨀      ⼀⼀ 匀椀洀甀氀愀琀攀 爀攀猀漀氀瘀椀渀最 漀昀 瀀爀漀洀椀猀攀ഀഀ
 *      deferred.resolve(123);਍ ⨀      ⼀⼀ 一漀琀攀 琀栀愀琀 琀栀攀 ✀琀栀攀渀✀ 昀甀渀挀琀椀漀渀 搀漀攀猀 渀漀琀 最攀琀 挀愀氀氀攀搀 猀礀渀挀栀爀漀渀漀甀猀氀礀⸀ഀഀ
 *      // This is because we want the promise API to always be async, whether or not਍ ⨀      ⼀⼀ 椀琀 最漀琀 挀愀氀氀攀搀 猀礀渀挀栀爀漀渀漀甀猀氀礀 漀爀 愀猀礀渀挀栀爀漀渀漀甀猀氀礀⸀ഀഀ
 *      expect(resolvedValue).toBeUndefined();਍ ⨀ഀഀ
 *      // Propagate promise resolution to 'then' functions using $apply().਍ ⨀      ␀爀漀漀琀匀挀漀瀀攀⸀␀愀瀀瀀氀礀⠀⤀㬀ഀഀ
 *      expect(resolvedValue).toEqual(123);਍ ⨀    紀⤀⤀㬀ഀഀ
 *  </pre>਍ ⨀⼀ഀഀ
function $QProvider() {਍ഀഀ
  this.$get = ['$rootScope', '$exceptionHandler', function($rootScope, $exceptionHandler) {਍    爀攀琀甀爀渀 焀䘀愀挀琀漀爀礀⠀昀甀渀挀琀椀漀渀⠀挀愀氀氀戀愀挀欀⤀ 笀ഀഀ
      $rootScope.$evalAsync(callback);਍    紀Ⰰ ␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀⤀㬀ഀഀ
  }];਍紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䌀漀渀猀琀爀甀挀琀猀 愀 瀀爀漀洀椀猀攀 洀愀渀愀最攀爀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀昀甀渀挀琀椀漀渀⠀昀甀渀挀琀椀漀渀⤀紀 渀攀砀琀吀椀挀欀 䘀甀渀挀琀椀漀渀 昀漀爀 攀砀攀挀甀琀椀渀最 昀甀渀挀琀椀漀渀猀 椀渀 琀栀攀 渀攀砀琀 琀甀爀渀⸀ഀഀ
 * @param {function(...*)} exceptionHandler Function into which unexpected exceptions are passed for਍ ⨀     搀攀戀甀最最椀渀最 瀀甀爀瀀漀猀攀猀⸀ഀഀ
 * @returns {object} Promise manager.਍ ⨀⼀ഀഀ
function qFactory(nextTick, exceptionHandler) {਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀ഀഀ
   * @name ng.$q#defer਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀焀ഀഀ
   * @description਍   ⨀ 䌀爀攀愀琀攀猀 愀 怀䐀攀昀攀爀爀攀搀怀 漀戀樀攀挀琀 眀栀椀挀栀 爀攀瀀爀攀猀攀渀琀猀 愀 琀愀猀欀 眀栀椀挀栀 眀椀氀氀 昀椀渀椀猀栀 椀渀 琀栀攀 昀甀琀甀爀攀⸀ഀഀ
   *਍   ⨀ 䀀爀攀琀甀爀渀猀 笀䐀攀昀攀爀爀攀搀紀 刀攀琀甀爀渀猀 愀 渀攀眀 椀渀猀琀愀渀挀攀 漀昀 搀攀昀攀爀爀攀搀⸀ഀഀ
   */਍  瘀愀爀 搀攀昀攀爀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var pending = [],਍        瘀愀氀甀攀Ⰰ 搀攀昀攀爀爀攀搀㬀ഀഀ
਍    搀攀昀攀爀爀攀搀 㴀 笀ഀഀ
਍      爀攀猀漀氀瘀攀㨀 昀甀渀挀琀椀漀渀⠀瘀愀氀⤀ 笀ഀഀ
        if (pending) {਍          瘀愀爀 挀愀氀氀戀愀挀欀猀 㴀 瀀攀渀搀椀渀最㬀ഀഀ
          pending = undefined;਍          瘀愀氀甀攀 㴀 爀攀昀⠀瘀愀氀⤀㬀ഀഀ
਍          椀昀 ⠀挀愀氀氀戀愀挀欀猀⸀氀攀渀最琀栀⤀ 笀ഀഀ
            nextTick(function() {਍              瘀愀爀 挀愀氀氀戀愀挀欀㬀ഀഀ
              for (var i = 0, ii = callbacks.length; i < ii; i++) {਍                挀愀氀氀戀愀挀欀 㴀 挀愀氀氀戀愀挀欀猀嬀椀崀㬀ഀഀ
                value.then(callback[0], callback[1], callback[2]);਍              紀ഀഀ
            });਍          紀ഀഀ
        }਍      紀Ⰰഀഀ
਍ഀഀ
      reject: function(reason) {਍        搀攀昀攀爀爀攀搀⸀爀攀猀漀氀瘀攀⠀爀攀樀攀挀琀⠀爀攀愀猀漀渀⤀⤀㬀ഀഀ
      },਍ഀഀ
਍      渀漀琀椀昀礀㨀 昀甀渀挀琀椀漀渀⠀瀀爀漀最爀攀猀猀⤀ 笀ഀഀ
        if (pending) {਍          瘀愀爀 挀愀氀氀戀愀挀欀猀 㴀 瀀攀渀搀椀渀最㬀ഀഀ
਍          椀昀 ⠀瀀攀渀搀椀渀最⸀氀攀渀最琀栀⤀ 笀ഀഀ
            nextTick(function() {਍              瘀愀爀 挀愀氀氀戀愀挀欀㬀ഀഀ
              for (var i = 0, ii = callbacks.length; i < ii; i++) {਍                挀愀氀氀戀愀挀欀 㴀 挀愀氀氀戀愀挀欀猀嬀椀崀㬀ഀഀ
                callback[2](progress);਍              紀ഀഀ
            });਍          紀ഀഀ
        }਍      紀Ⰰഀഀ
਍ഀഀ
      promise: {਍        琀栀攀渀㨀 昀甀渀挀琀椀漀渀⠀挀愀氀氀戀愀挀欀Ⰰ 攀爀爀戀愀挀欀Ⰰ 瀀爀漀最爀攀猀猀戀愀挀欀⤀ 笀ഀഀ
          var result = defer();਍ഀഀ
          var wrappedCallback = function(value) {਍            琀爀礀 笀ഀഀ
              result.resolve((isFunction(callback) ? callback : defaultCallback)(value));਍            紀 挀愀琀挀栀⠀攀⤀ 笀ഀഀ
              result.reject(e);਍              攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀⠀攀⤀㬀ഀഀ
            }਍          紀㬀ഀഀ
਍          瘀愀爀 眀爀愀瀀瀀攀搀䔀爀爀戀愀挀欀 㴀 昀甀渀挀琀椀漀渀⠀爀攀愀猀漀渀⤀ 笀ഀഀ
            try {਍              爀攀猀甀氀琀⸀爀攀猀漀氀瘀攀⠀⠀椀猀䘀甀渀挀琀椀漀渀⠀攀爀爀戀愀挀欀⤀ 㼀 攀爀爀戀愀挀欀 㨀 搀攀昀愀甀氀琀䔀爀爀戀愀挀欀⤀⠀爀攀愀猀漀渀⤀⤀㬀ഀഀ
            } catch(e) {਍              爀攀猀甀氀琀⸀爀攀樀攀挀琀⠀攀⤀㬀ഀഀ
              exceptionHandler(e);਍            紀ഀഀ
          };਍ഀഀ
          var wrappedProgressback = function(progress) {਍            琀爀礀 笀ഀഀ
              result.notify((isFunction(progressback) ? progressback : defaultCallback)(progress));਍            紀 挀愀琀挀栀⠀攀⤀ 笀ഀഀ
              exceptionHandler(e);਍            紀ഀഀ
          };਍ഀഀ
          if (pending) {਍            瀀攀渀搀椀渀最⸀瀀甀猀栀⠀嬀眀爀愀瀀瀀攀搀䌀愀氀氀戀愀挀欀Ⰰ 眀爀愀瀀瀀攀搀䔀爀爀戀愀挀欀Ⰰ 眀爀愀瀀瀀攀搀倀爀漀最爀攀猀猀戀愀挀欀崀⤀㬀ഀഀ
          } else {਍            瘀愀氀甀攀⸀琀栀攀渀⠀眀爀愀瀀瀀攀搀䌀愀氀氀戀愀挀欀Ⰰ 眀爀愀瀀瀀攀搀䔀爀爀戀愀挀欀Ⰰ 眀爀愀瀀瀀攀搀倀爀漀最爀攀猀猀戀愀挀欀⤀㬀ഀഀ
          }਍ഀഀ
          return result.promise;਍        紀Ⰰഀഀ
਍        ∀挀愀琀挀栀∀㨀 昀甀渀挀琀椀漀渀⠀挀愀氀氀戀愀挀欀⤀ 笀ഀഀ
          return this.then(null, callback);਍        紀Ⰰഀഀ
਍        ∀昀椀渀愀氀氀礀∀㨀 昀甀渀挀琀椀漀渀⠀挀愀氀氀戀愀挀欀⤀ 笀ഀഀ
਍          昀甀渀挀琀椀漀渀 洀愀欀攀倀爀漀洀椀猀攀⠀瘀愀氀甀攀Ⰰ 爀攀猀漀氀瘀攀搀⤀ 笀ഀഀ
            var result = defer();਍            椀昀 ⠀爀攀猀漀氀瘀攀搀⤀ 笀ഀഀ
              result.resolve(value);਍            紀 攀氀猀攀 笀ഀഀ
              result.reject(value);਍            紀ഀഀ
            return result.promise;਍          紀ഀഀ
਍          昀甀渀挀琀椀漀渀 栀愀渀搀氀攀䌀愀氀氀戀愀挀欀⠀瘀愀氀甀攀Ⰰ 椀猀刀攀猀漀氀瘀攀搀⤀ 笀ഀഀ
            var callbackOutput = null;਍            琀爀礀 笀ഀഀ
              callbackOutput = (callback ||defaultCallback)();਍            紀 挀愀琀挀栀⠀攀⤀ 笀ഀഀ
              return makePromise(e, false);਍            紀ഀഀ
            if (callbackOutput && isFunction(callbackOutput.then)) {਍              爀攀琀甀爀渀 挀愀氀氀戀愀挀欀伀甀琀瀀甀琀⸀琀栀攀渀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
                return makePromise(value, isResolved);਍              紀Ⰰ 昀甀渀挀琀椀漀渀⠀攀爀爀漀爀⤀ 笀ഀഀ
                return makePromise(error, false);਍              紀⤀㬀ഀഀ
            } else {਍              爀攀琀甀爀渀 洀愀欀攀倀爀漀洀椀猀攀⠀瘀愀氀甀攀Ⰰ 椀猀刀攀猀漀氀瘀攀搀⤀㬀ഀഀ
            }਍          紀ഀഀ
਍          爀攀琀甀爀渀 琀栀椀猀⸀琀栀攀渀⠀昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
            return handleCallback(value, true);਍          紀Ⰰ 昀甀渀挀琀椀漀渀⠀攀爀爀漀爀⤀ 笀ഀഀ
            return handleCallback(error, false);਍          紀⤀㬀ഀഀ
        }਍      紀ഀഀ
    };਍ഀഀ
    return deferred;਍  紀㬀ഀഀ
਍ഀഀ
  var ref = function(value) {਍    椀昀 ⠀瘀愀氀甀攀 ☀☀ 椀猀䘀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⸀琀栀攀渀⤀⤀ 爀攀琀甀爀渀 瘀愀氀甀攀㬀ഀഀ
    return {਍      琀栀攀渀㨀 昀甀渀挀琀椀漀渀⠀挀愀氀氀戀愀挀欀⤀ 笀ഀഀ
        var result = defer();਍        渀攀砀琀吀椀挀欀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          result.resolve(callback(value));਍        紀⤀㬀ഀഀ
        return result.promise;਍      紀ഀഀ
    };਍  紀㬀ഀഀ
਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀ഀഀ
   * @name ng.$q#reject਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀焀ഀഀ
   * @description਍   ⨀ 䌀爀攀愀琀攀猀 愀 瀀爀漀洀椀猀攀 琀栀愀琀 椀猀 爀攀猀漀氀瘀攀搀 愀猀 爀攀樀攀挀琀攀搀 眀椀琀栀 琀栀攀 猀瀀攀挀椀昀椀攀搀 怀爀攀愀猀漀渀怀⸀ 吀栀椀猀 愀瀀椀 猀栀漀甀氀搀 戀攀ഀഀ
   * used to forward rejection in a chain of promises. If you are dealing with the last promise in਍   ⨀ 愀 瀀爀漀洀椀猀攀 挀栀愀椀渀Ⰰ 礀漀甀 搀漀渀✀琀 渀攀攀搀 琀漀 眀漀爀爀礀 愀戀漀甀琀 椀琀⸀ഀഀ
   *਍   ⨀ 圀栀攀渀 挀漀洀瀀愀爀椀渀最 搀攀昀攀爀爀攀搀猀⼀瀀爀漀洀椀猀攀猀 琀漀 琀栀攀 昀愀洀椀氀椀愀爀 戀攀栀愀瘀椀漀爀 漀昀 琀爀礀⼀挀愀琀挀栀⼀琀栀爀漀眀Ⰰ 琀栀椀渀欀 漀昀ഀഀ
   * `reject` as the `throw` keyword in JavaScript. This also means that if you "catch" an error via਍   ⨀ 愀 瀀爀漀洀椀猀攀 攀爀爀漀爀 挀愀氀氀戀愀挀欀 愀渀搀 礀漀甀 眀愀渀琀 琀漀 昀漀爀眀愀爀搀 琀栀攀 攀爀爀漀爀 琀漀 琀栀攀 瀀爀漀洀椀猀攀 搀攀爀椀瘀攀搀 昀爀漀洀 琀栀攀ഀഀ
   * current promise, you have to "rethrow" the error by returning a rejection constructed via਍   ⨀ 怀爀攀樀攀挀琀怀⸀ഀഀ
   *਍   ⨀ 㰀瀀爀攀㸀ഀഀ
   *   promiseB = promiseA.then(function(result) {਍   ⨀     ⼀⼀ 猀甀挀挀攀猀猀㨀 搀漀 猀漀洀攀琀栀椀渀最 愀渀搀 爀攀猀漀氀瘀攀 瀀爀漀洀椀猀攀䈀ഀഀ
   *     //          with the old or a new result਍   ⨀     爀攀琀甀爀渀 爀攀猀甀氀琀㬀ഀഀ
   *   }, function(reason) {਍   ⨀     ⼀⼀ 攀爀爀漀爀㨀 栀愀渀搀氀攀 琀栀攀 攀爀爀漀爀 椀昀 瀀漀猀猀椀戀氀攀 愀渀搀ഀഀ
   *     //        resolve promiseB with newPromiseOrValue,਍   ⨀     ⼀⼀        漀琀栀攀爀眀椀猀攀 昀漀爀眀愀爀搀 琀栀攀 爀攀樀攀挀琀椀漀渀 琀漀 瀀爀漀洀椀猀攀䈀ഀഀ
   *     if (canHandle(reason)) {਍   ⨀      ⼀⼀ 栀愀渀搀氀攀 琀栀攀 攀爀爀漀爀 愀渀搀 爀攀挀漀瘀攀爀ഀഀ
   *      return newPromiseOrValue;਍   ⨀     紀ഀഀ
   *     return $q.reject(reason);਍   ⨀   紀⤀㬀ഀഀ
   * </pre>਍   ⨀ഀഀ
   * @param {*} reason Constant, message, exception or an object representing the rejection reason.਍   ⨀ 䀀爀攀琀甀爀渀猀 笀倀爀漀洀椀猀攀紀 刀攀琀甀爀渀猀 愀 瀀爀漀洀椀猀攀 琀栀愀琀 眀愀猀 愀氀爀攀愀搀礀 爀攀猀漀氀瘀攀搀 愀猀 爀攀樀攀挀琀攀搀 眀椀琀栀 琀栀攀 怀爀攀愀猀漀渀怀⸀ഀഀ
   */਍  瘀愀爀 爀攀樀攀挀琀 㴀 昀甀渀挀琀椀漀渀⠀爀攀愀猀漀渀⤀ 笀ഀഀ
    return {਍      琀栀攀渀㨀 昀甀渀挀琀椀漀渀⠀挀愀氀氀戀愀挀欀Ⰰ 攀爀爀戀愀挀欀⤀ 笀ഀഀ
        var result = defer();਍        渀攀砀琀吀椀挀欀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          try {਍            爀攀猀甀氀琀⸀爀攀猀漀氀瘀攀⠀⠀椀猀䘀甀渀挀琀椀漀渀⠀攀爀爀戀愀挀欀⤀ 㼀 攀爀爀戀愀挀欀 㨀 搀攀昀愀甀氀琀䔀爀爀戀愀挀欀⤀⠀爀攀愀猀漀渀⤀⤀㬀ഀഀ
          } catch(e) {਍            爀攀猀甀氀琀⸀爀攀樀攀挀琀⠀攀⤀㬀ഀഀ
            exceptionHandler(e);਍          紀ഀഀ
        });਍        爀攀琀甀爀渀 爀攀猀甀氀琀⸀瀀爀漀洀椀猀攀㬀ഀഀ
      }਍    紀㬀ഀഀ
  };਍ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc਍   ⨀ 䀀渀愀洀攀 渀最⸀␀焀⌀眀栀攀渀ഀഀ
   * @methodOf ng.$q਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Wraps an object that might be a value or a (3rd party) then-able promise into a $q promise.਍   ⨀ 吀栀椀猀 椀猀 甀猀攀昀甀氀 眀栀攀渀 礀漀甀 愀爀攀 搀攀愀氀椀渀最 眀椀琀栀 愀渀 漀戀樀攀挀琀 琀栀愀琀 洀椀最栀琀 漀爀 洀椀最栀琀 渀漀琀 戀攀 愀 瀀爀漀洀椀猀攀Ⰰ 漀爀 椀昀ഀഀ
   * the promise comes from a source that can't be trusted.਍   ⨀ഀഀ
   * @param {*} value Value or a promise਍   ⨀ 䀀爀攀琀甀爀渀猀 笀倀爀漀洀椀猀攀紀 刀攀琀甀爀渀猀 愀 瀀爀漀洀椀猀攀 漀昀 琀栀攀 瀀愀猀猀攀搀 瘀愀氀甀攀 漀爀 瀀爀漀洀椀猀攀ഀഀ
   */਍  瘀愀爀 眀栀攀渀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀Ⰰ 挀愀氀氀戀愀挀欀Ⰰ 攀爀爀戀愀挀欀Ⰰ 瀀爀漀最爀攀猀猀戀愀挀欀⤀ 笀ഀഀ
    var result = defer(),਍        搀漀渀攀㬀ഀഀ
਍    瘀愀爀 眀爀愀瀀瀀攀搀䌀愀氀氀戀愀挀欀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
      try {਍        爀攀琀甀爀渀 ⠀椀猀䘀甀渀挀琀椀漀渀⠀挀愀氀氀戀愀挀欀⤀ 㼀 挀愀氀氀戀愀挀欀 㨀 搀攀昀愀甀氀琀䌀愀氀氀戀愀挀欀⤀⠀瘀愀氀甀攀⤀㬀ഀഀ
      } catch (e) {਍        攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀⠀攀⤀㬀ഀഀ
        return reject(e);਍      紀ഀഀ
    };਍ഀഀ
    var wrappedErrback = function(reason) {਍      琀爀礀 笀ഀഀ
        return (isFunction(errback) ? errback : defaultErrback)(reason);਍      紀 挀愀琀挀栀 ⠀攀⤀ 笀ഀഀ
        exceptionHandler(e);਍        爀攀琀甀爀渀 爀攀樀攀挀琀⠀攀⤀㬀ഀഀ
      }਍    紀㬀ഀഀ
਍    瘀愀爀 眀爀愀瀀瀀攀搀倀爀漀最爀攀猀猀戀愀挀欀 㴀 昀甀渀挀琀椀漀渀⠀瀀爀漀最爀攀猀猀⤀ 笀ഀഀ
      try {਍        爀攀琀甀爀渀 ⠀椀猀䘀甀渀挀琀椀漀渀⠀瀀爀漀最爀攀猀猀戀愀挀欀⤀ 㼀 瀀爀漀最爀攀猀猀戀愀挀欀 㨀 搀攀昀愀甀氀琀䌀愀氀氀戀愀挀欀⤀⠀瀀爀漀最爀攀猀猀⤀㬀ഀഀ
      } catch (e) {਍        攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀⠀攀⤀㬀ഀഀ
      }਍    紀㬀ഀഀ
਍    渀攀砀琀吀椀挀欀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
      ref(value).then(function(value) {਍        椀昀 ⠀搀漀渀攀⤀ 爀攀琀甀爀渀㬀ഀഀ
        done = true;਍        爀攀猀甀氀琀⸀爀攀猀漀氀瘀攀⠀爀攀昀⠀瘀愀氀甀攀⤀⸀琀栀攀渀⠀眀爀愀瀀瀀攀搀䌀愀氀氀戀愀挀欀Ⰰ 眀爀愀瀀瀀攀搀䔀爀爀戀愀挀欀Ⰰ 眀爀愀瀀瀀攀搀倀爀漀最爀攀猀猀戀愀挀欀⤀⤀㬀ഀഀ
      }, function(reason) {਍        椀昀 ⠀搀漀渀攀⤀ 爀攀琀甀爀渀㬀ഀഀ
        done = true;਍        爀攀猀甀氀琀⸀爀攀猀漀氀瘀攀⠀眀爀愀瀀瀀攀搀䔀爀爀戀愀挀欀⠀爀攀愀猀漀渀⤀⤀㬀ഀഀ
      }, function(progress) {਍        椀昀 ⠀搀漀渀攀⤀ 爀攀琀甀爀渀㬀ഀഀ
        result.notify(wrappedProgressback(progress));਍      紀⤀㬀ഀഀ
    });਍ഀഀ
    return result.promise;਍  紀㬀ഀഀ
਍ഀഀ
  function defaultCallback(value) {਍    爀攀琀甀爀渀 瘀愀氀甀攀㬀ഀഀ
  }਍ഀഀ
਍  昀甀渀挀琀椀漀渀 搀攀昀愀甀氀琀䔀爀爀戀愀挀欀⠀爀攀愀猀漀渀⤀ 笀ഀഀ
    return reject(reason);਍  紀ഀഀ
਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀ഀഀ
   * @name ng.$q#all਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀焀ഀഀ
   * @description਍   ⨀ 䌀漀洀戀椀渀攀猀 洀甀氀琀椀瀀氀攀 瀀爀漀洀椀猀攀猀 椀渀琀漀 愀 猀椀渀最氀攀 瀀爀漀洀椀猀攀 琀栀愀琀 椀猀 爀攀猀漀氀瘀攀搀 眀栀攀渀 愀氀氀 漀昀 琀栀攀 椀渀瀀甀琀ഀഀ
   * promises are resolved.਍   ⨀ഀഀ
   * @param {Array.<Promise>|Object.<Promise>} promises An array or hash of promises.਍   ⨀ 䀀爀攀琀甀爀渀猀 笀倀爀漀洀椀猀攀紀 刀攀琀甀爀渀猀 愀 猀椀渀最氀攀 瀀爀漀洀椀猀攀 琀栀愀琀 眀椀氀氀 戀攀 爀攀猀漀氀瘀攀搀 眀椀琀栀 愀渀 愀爀爀愀礀⼀栀愀猀栀 漀昀 瘀愀氀甀攀猀Ⰰഀഀ
   *   each value corresponding to the promise at the same index/key in the `promises` array/hash.਍   ⨀   䤀昀 愀渀礀 漀昀 琀栀攀 瀀爀漀洀椀猀攀猀 椀猀 爀攀猀漀氀瘀攀搀 眀椀琀栀 愀 爀攀樀攀挀琀椀漀渀Ⰰ 琀栀椀猀 爀攀猀甀氀琀椀渀最 瀀爀漀洀椀猀攀 眀椀氀氀 戀攀 爀攀樀攀挀琀攀搀ഀഀ
   *   with the same rejection value.਍   ⨀⼀ഀഀ
  function all(promises) {਍    瘀愀爀 搀攀昀攀爀爀攀搀 㴀 搀攀昀攀爀⠀⤀Ⰰഀഀ
        counter = 0,਍        爀攀猀甀氀琀猀 㴀 椀猀䄀爀爀愀礀⠀瀀爀漀洀椀猀攀猀⤀ 㼀 嬀崀 㨀 笀紀㬀ഀഀ
਍    昀漀爀䔀愀挀栀⠀瀀爀漀洀椀猀攀猀Ⰰ 昀甀渀挀琀椀漀渀⠀瀀爀漀洀椀猀攀Ⰰ 欀攀礀⤀ 笀ഀഀ
      counter++;਍      爀攀昀⠀瀀爀漀洀椀猀攀⤀⸀琀栀攀渀⠀昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
        if (results.hasOwnProperty(key)) return;਍        爀攀猀甀氀琀猀嬀欀攀礀崀 㴀 瘀愀氀甀攀㬀ഀഀ
        if (!(--counter)) deferred.resolve(results);਍      紀Ⰰ 昀甀渀挀琀椀漀渀⠀爀攀愀猀漀渀⤀ 笀ഀഀ
        if (results.hasOwnProperty(key)) return;਍        搀攀昀攀爀爀攀搀⸀爀攀樀攀挀琀⠀爀攀愀猀漀渀⤀㬀ഀഀ
      });਍    紀⤀㬀ഀഀ
਍    椀昀 ⠀挀漀甀渀琀攀爀 㴀㴀㴀 　⤀ 笀ഀഀ
      deferred.resolve(results);਍    紀ഀഀ
਍    爀攀琀甀爀渀 搀攀昀攀爀爀攀搀⸀瀀爀漀洀椀猀攀㬀ഀഀ
  }਍ഀഀ
  return {਍    搀攀昀攀爀㨀 搀攀昀攀爀Ⰰഀഀ
    reject: reject,਍    眀栀攀渀㨀 眀栀攀渀Ⰰഀഀ
    all: all਍  紀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䐀䔀匀䤀䜀一 一伀吀䔀匀ഀഀ
 *਍ ⨀ 吀栀攀 搀攀猀椀最渀 搀攀挀椀猀椀漀渀猀 戀攀栀椀渀搀 琀栀攀 猀挀漀瀀攀 愀爀攀 栀攀愀瘀椀氀礀 昀愀瘀漀爀攀搀 昀漀爀 猀瀀攀攀搀 愀渀搀 洀攀洀漀爀礀 挀漀渀猀甀洀瀀琀椀漀渀⸀ഀഀ
 *਍ ⨀ 吀栀攀 琀礀瀀椀挀愀氀 甀猀攀 漀昀 猀挀漀瀀攀 椀猀 琀漀 眀愀琀挀栀 琀栀攀 攀砀瀀爀攀猀猀椀漀渀猀Ⰰ 眀栀椀挀栀 洀漀猀琀 漀昀 琀栀攀 琀椀洀攀 爀攀琀甀爀渀 琀栀攀 猀愀洀攀ഀഀ
 * value as last time so we optimize the operation.਍ ⨀ഀഀ
 * Closures construction is expensive in terms of speed as well as memory:਍ ⨀   ⴀ 一漀 挀氀漀猀甀爀攀猀Ⰰ 椀渀猀琀攀愀搀 甀猀攀 瀀爀漀琀漀琀礀瀀椀挀愀氀 椀渀栀攀爀椀琀愀渀挀攀 昀漀爀 䄀倀䤀ഀഀ
 *   - Internal state needs to be stored on scope directly, which means that private state is਍ ⨀     攀砀瀀漀猀攀搀 愀猀 ␀␀开开开开 瀀爀漀瀀攀爀琀椀攀猀ഀഀ
 *਍ ⨀ 䰀漀漀瀀 漀瀀攀爀愀琀椀漀渀猀 愀爀攀 漀瀀琀椀洀椀稀攀搀 戀礀 甀猀椀渀最 眀栀椀氀攀⠀挀漀甀渀琀ⴀⴀ⤀ 笀 ⸀⸀⸀ 紀ഀഀ
 *   - this means that in order to keep the same order of execution as addition we have to add਍ ⨀     椀琀攀洀猀 琀漀 琀栀攀 愀爀爀愀礀 愀琀 琀栀攀 戀攀最椀渀渀椀渀最 ⠀猀栀椀昀琀⤀ 椀渀猀琀攀愀搀 漀昀 愀琀 琀栀攀 攀渀搀 ⠀瀀甀猀栀⤀ഀഀ
 *਍ ⨀ 䌀栀椀氀搀 猀挀漀瀀攀猀 愀爀攀 挀爀攀愀琀攀搀 愀渀搀 爀攀洀漀瘀攀搀 漀昀琀攀渀ഀഀ
 *   - Using an array would be slow since inserts in middle are expensive so we use linked list਍ ⨀ഀഀ
 * There are few watches then a lot of observers. This is why you don't want the observer to be਍ ⨀ 椀洀瀀氀攀洀攀渀琀攀搀 椀渀 琀栀攀 猀愀洀攀 眀愀礀 愀猀 眀愀琀挀栀⸀ 圀愀琀挀栀 爀攀焀甀椀爀攀猀 爀攀琀甀爀渀 漀昀 椀渀椀琀椀愀氀椀稀愀琀椀漀渀 昀甀渀挀琀椀漀渀 眀栀椀挀栀ഀഀ
 * are expensive to construct.਍ ⨀⼀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name ng.$rootScopeProvider਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 *਍ ⨀ 倀爀漀瘀椀搀攀爀 昀漀爀 琀栀攀 ␀爀漀漀琀匀挀漀瀀攀 猀攀爀瘀椀挀攀⸀ഀഀ
 */਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
 * @name ng.$rootScopeProvider#digestTtl਍ ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀爀漀漀琀匀挀漀瀀攀倀爀漀瘀椀搀攀爀ഀഀ
 * @description਍ ⨀ഀഀ
 * Sets the number of `$digest` iterations the scope should attempt to execute before giving up and਍ ⨀ 愀猀猀甀洀椀渀最 琀栀愀琀 琀栀攀 洀漀搀攀氀 椀猀 甀渀猀琀愀戀氀攀⸀ഀഀ
 *਍ ⨀ 吀栀攀 挀甀爀爀攀渀琀 搀攀昀愀甀氀琀 椀猀 ㄀　 椀琀攀爀愀琀椀漀渀猀⸀ഀഀ
 *਍ ⨀ 䤀渀 挀漀洀瀀氀攀砀 愀瀀瀀氀椀挀愀琀椀漀渀猀 椀琀✀猀 瀀漀猀猀椀戀氀攀 琀栀愀琀 琀栀攀 搀攀瀀攀渀搀攀渀挀椀攀猀 戀攀琀眀攀攀渀 怀␀眀愀琀挀栀怀猀 眀椀氀氀 爀攀猀甀氀琀 椀渀ഀഀ
 * several digest iterations. However if an application needs more than the default 10 digest਍ ⨀ 椀琀攀爀愀琀椀漀渀猀 昀漀爀 椀琀猀 洀漀搀攀氀 琀漀 猀琀愀戀椀氀椀稀攀 琀栀攀渀 礀漀甀 猀栀漀甀氀搀 椀渀瘀攀猀琀椀最愀琀攀 眀栀愀琀 椀猀 挀愀甀猀椀渀最 琀栀攀 洀漀搀攀氀 琀漀ഀഀ
 * continuously change during the digest.਍ ⨀ഀഀ
 * Increasing the TTL could have performance implications, so you should not change it without਍ ⨀ 瀀爀漀瀀攀爀 樀甀猀琀椀昀椀挀愀琀椀漀渀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀紀 氀椀洀椀琀 吀栀攀 渀甀洀戀攀爀 漀昀 搀椀最攀猀琀 椀琀攀爀愀琀椀漀渀猀⸀ഀഀ
 */਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc object਍ ⨀ 䀀渀愀洀攀 渀最⸀␀爀漀漀琀匀挀漀瀀攀ഀഀ
 * @description਍ ⨀ഀഀ
 * Every application has a single root {@link ng.$rootScope.Scope scope}.਍ ⨀ 䄀氀氀 漀琀栀攀爀 猀挀漀瀀攀猀 愀爀攀 搀攀猀挀攀渀搀愀渀琀 猀挀漀瀀攀猀 漀昀 琀栀攀 爀漀漀琀 猀挀漀瀀攀⸀ 匀挀漀瀀攀猀 瀀爀漀瘀椀搀攀 猀攀瀀愀爀愀琀椀漀渀ഀഀ
 * between the model and the view, via a mechanism for watching the model for changes.਍ ⨀ 吀栀攀礀 愀氀猀漀 瀀爀漀瘀椀搀攀 愀渀 攀瘀攀渀琀 攀洀椀猀猀椀漀渀⼀戀爀漀愀搀挀愀猀琀 愀渀搀 猀甀戀猀挀爀椀瀀琀椀漀渀 昀愀挀椀氀椀琀礀⸀ 匀攀攀 琀栀攀ഀഀ
 * {@link guide/scope developer guide on scopes}.਍ ⨀⼀ഀഀ
function $RootScopeProvider(){਍  瘀愀爀 吀吀䰀 㴀 ㄀　㬀ഀഀ
  var $rootScopeMinErr = minErr('$rootScope');਍  瘀愀爀 氀愀猀琀䐀椀爀琀礀圀愀琀挀栀 㴀 渀甀氀氀㬀ഀഀ
਍  琀栀椀猀⸀搀椀最攀猀琀吀琀氀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
    if (arguments.length) {਍      吀吀䰀 㴀 瘀愀氀甀攀㬀ഀഀ
    }਍    爀攀琀甀爀渀 吀吀䰀㬀ഀഀ
  };਍ഀഀ
  this.$get = ['$injector', '$exceptionHandler', '$parse', '$browser',਍      昀甀渀挀琀椀漀渀⠀ ␀椀渀樀攀挀琀漀爀Ⰰ   ␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀Ⰰ   ␀瀀愀爀猀攀Ⰰ   ␀戀爀漀眀猀攀爀⤀ 笀ഀഀ
਍    ⼀⨀⨀ഀഀ
     * @ngdoc function਍     ⨀ 䀀渀愀洀攀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * A root scope can be retrieved using the {@link ng.$rootScope $rootScope} key from the਍     ⨀ 笀䀀氀椀渀欀 䄀唀吀伀⸀␀椀渀樀攀挀琀漀爀 ␀椀渀樀攀挀琀漀爀紀⸀ 䌀栀椀氀搀 猀挀漀瀀攀猀 愀爀攀 挀爀攀愀琀攀搀 甀猀椀渀最 琀栀攀ഀഀ
     * {@link ng.$rootScope.Scope#methods_$new $new()} method. (Most scopes are created automatically when਍     ⨀ 挀漀洀瀀椀氀攀搀 䠀吀䴀䰀 琀攀洀瀀氀愀琀攀 椀猀 攀砀攀挀甀琀攀搀⸀⤀ഀഀ
     *਍     ⨀ 䠀攀爀攀 椀猀 愀 猀椀洀瀀氀攀 猀挀漀瀀攀 猀渀椀瀀瀀攀琀 琀漀 猀栀漀眀 栀漀眀 礀漀甀 挀愀渀 椀渀琀攀爀愀挀琀 眀椀琀栀 琀栀攀 猀挀漀瀀攀⸀ഀഀ
     * <pre>਍     ⨀ 㰀昀椀氀攀 猀爀挀㴀∀⸀⼀琀攀猀琀⼀渀最⼀爀漀漀琀匀挀漀瀀攀匀瀀攀挀⸀樀猀∀ 琀愀最㴀∀搀漀挀猀㄀∀ ⼀㸀ഀഀ
     * </pre>਍     ⨀ഀഀ
     * # Inheritance਍     ⨀ 䄀 猀挀漀瀀攀 挀愀渀 椀渀栀攀爀椀琀 昀爀漀洀 愀 瀀愀爀攀渀琀 猀挀漀瀀攀Ⰰ 愀猀 椀渀 琀栀椀猀 攀砀愀洀瀀氀攀㨀ഀഀ
     * <pre>਍         瘀愀爀 瀀愀爀攀渀琀 㴀 ␀爀漀漀琀匀挀漀瀀攀㬀ഀഀ
         var child = parent.$new();਍ഀഀ
         parent.salutation = "Hello";਍         挀栀椀氀搀⸀渀愀洀攀 㴀 ∀圀漀爀氀搀∀㬀ഀഀ
         expect(child.salutation).toEqual('Hello');਍ഀഀ
         child.salutation = "Welcome";਍         攀砀瀀攀挀琀⠀挀栀椀氀搀⸀猀愀氀甀琀愀琀椀漀渀⤀⸀琀漀䔀焀甀愀氀⠀✀圀攀氀挀漀洀攀✀⤀㬀ഀഀ
         expect(parent.salutation).toEqual('Hello');਍     ⨀ 㰀⼀瀀爀攀㸀ഀഀ
     *਍     ⨀ഀഀ
     * @param {Object.<string, function()>=} providers Map of service factory which need to be਍     ⨀                                       瀀爀漀瘀椀搀攀搀 昀漀爀 琀栀攀 挀甀爀爀攀渀琀 猀挀漀瀀攀⸀ 䐀攀昀愀甀氀琀猀 琀漀 笀䀀氀椀渀欀 渀最紀⸀ഀഀ
     * @param {Object.<string, *>=} instanceCache Provides pre-instantiated services which should਍     ⨀                              愀瀀瀀攀渀搀⼀漀瘀攀爀爀椀搀攀 猀攀爀瘀椀挀攀猀 瀀爀漀瘀椀搀攀搀 戀礀 怀瀀爀漀瘀椀搀攀爀猀怀⸀ 吀栀椀猀 椀猀 栀愀渀搀礀ഀഀ
     *                              when unit-testing and having the need to override a default਍     ⨀                              猀攀爀瘀椀挀攀⸀ഀഀ
     * @returns {Object} Newly created scope.਍     ⨀ഀഀ
     */਍    昀甀渀挀琀椀漀渀 匀挀漀瀀攀⠀⤀ 笀ഀഀ
      this.$id = nextUid();਍      琀栀椀猀⸀␀␀瀀栀愀猀攀 㴀 琀栀椀猀⸀␀瀀愀爀攀渀琀 㴀 琀栀椀猀⸀␀␀眀愀琀挀栀攀爀猀 㴀ഀഀ
                     this.$$nextSibling = this.$$prevSibling =਍                     琀栀椀猀⸀␀␀挀栀椀氀搀䠀攀愀搀 㴀 琀栀椀猀⸀␀␀挀栀椀氀搀吀愀椀氀 㴀 渀甀氀氀㬀ഀഀ
      this['this'] = this.$root =  this;਍      琀栀椀猀⸀␀␀搀攀猀琀爀漀礀攀搀 㴀 昀愀氀猀攀㬀ഀഀ
      this.$$asyncQueue = [];਍      琀栀椀猀⸀␀␀瀀漀猀琀䐀椀最攀猀琀儀甀攀甀攀 㴀 嬀崀㬀ഀഀ
      this.$$listeners = {};਍      琀栀椀猀⸀␀␀氀椀猀琀攀渀攀爀䌀漀甀渀琀 㴀 笀紀㬀ഀഀ
      this.$$isolateBindings = {};਍    紀ഀഀ
਍    ⼀⨀⨀ഀഀ
     * @ngdoc property਍     ⨀ 䀀渀愀洀攀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀␀椀搀ഀഀ
     * @propertyOf ng.$rootScope.Scope਍     ⨀ 䀀爀攀琀甀爀渀猀 笀渀甀洀戀攀爀紀 唀渀椀焀甀攀 猀挀漀瀀攀 䤀䐀 ⠀洀漀渀漀琀漀渀椀挀愀氀氀礀 椀渀挀爀攀愀猀椀渀最 愀氀瀀栀愀渀甀洀攀爀椀挀 猀攀焀甀攀渀挀攀⤀ 甀猀攀昀甀氀 昀漀爀ഀഀ
     *   debugging.਍     ⨀⼀ഀഀ
਍ഀഀ
    Scope.prototype = {਍      挀漀渀猀琀爀甀挀琀漀爀㨀 匀挀漀瀀攀Ⰰഀഀ
      /**਍       ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
       * @name ng.$rootScope.Scope#$new਍       ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀ഀഀ
       * @function਍       ⨀ഀഀ
       * @description਍       ⨀ 䌀爀攀愀琀攀猀 愀 渀攀眀 挀栀椀氀搀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀 猀挀漀瀀攀紀⸀ഀഀ
       *਍       ⨀ 吀栀攀 瀀愀爀攀渀琀 猀挀漀瀀攀 眀椀氀氀 瀀爀漀瀀愀最愀琀攀 琀栀攀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀搀椀最攀猀琀 ␀搀椀最攀猀琀⠀⤀紀 愀渀搀ഀഀ
       * {@link ng.$rootScope.Scope#methods_$digest $digest()} events. The scope can be removed from the਍       ⨀ 猀挀漀瀀攀 栀椀攀爀愀爀挀栀礀 甀猀椀渀最 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀搀攀猀琀爀漀礀 ␀搀攀猀琀爀漀礀⠀⤀紀⸀ഀഀ
       *਍       ⨀ 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀搀攀猀琀爀漀礀 ␀搀攀猀琀爀漀礀⠀⤀紀 洀甀猀琀 戀攀 挀愀氀氀攀搀 漀渀 愀 猀挀漀瀀攀 眀栀攀渀 椀琀 椀猀ഀഀ
       * desired for the scope and its child scopes to be permanently detached from the parent and਍       ⨀ 琀栀甀猀 猀琀漀瀀 瀀愀爀琀椀挀椀瀀愀琀椀渀最 椀渀 洀漀搀攀氀 挀栀愀渀最攀 搀攀琀攀挀琀椀漀渀 愀渀搀 氀椀猀琀攀渀攀爀 渀漀琀椀昀椀挀愀琀椀漀渀 戀礀 椀渀瘀漀欀椀渀最⸀ഀഀ
       *਍       ⨀ 䀀瀀愀爀愀洀 笀戀漀漀氀攀愀渀紀 椀猀漀氀愀琀攀 䤀昀 琀爀甀攀Ⰰ 琀栀攀渀 琀栀攀 猀挀漀瀀攀 搀漀攀猀 渀漀琀 瀀爀漀琀漀琀礀瀀椀挀愀氀氀礀 椀渀栀攀爀椀琀 昀爀漀洀 琀栀攀ഀഀ
       *         parent scope. The scope is isolated, as it can not see parent scope properties.਍       ⨀         圀栀攀渀 挀爀攀愀琀椀渀最 眀椀搀最攀琀猀Ⰰ 椀琀 椀猀 甀猀攀昀甀氀 昀漀爀 琀栀攀 眀椀搀最攀琀 琀漀 渀漀琀 愀挀挀椀搀攀渀琀愀氀氀礀 爀攀愀搀 瀀愀爀攀渀琀ഀഀ
       *         state.਍       ⨀ഀഀ
       * @returns {Object} The newly created child scope.਍       ⨀ഀഀ
       */਍      ␀渀攀眀㨀 昀甀渀挀琀椀漀渀⠀椀猀漀氀愀琀攀⤀ 笀ഀഀ
        var ChildScope,਍            挀栀椀氀搀㬀ഀഀ
਍        椀昀 ⠀椀猀漀氀愀琀攀⤀ 笀ഀഀ
          child = new Scope();਍          挀栀椀氀搀⸀␀爀漀漀琀 㴀 琀栀椀猀⸀␀爀漀漀琀㬀ഀഀ
          // ensure that there is just one async queue per $rootScope and its children਍          挀栀椀氀搀⸀␀␀愀猀礀渀挀儀甀攀甀攀 㴀 琀栀椀猀⸀␀␀愀猀礀渀挀儀甀攀甀攀㬀ഀഀ
          child.$$postDigestQueue = this.$$postDigestQueue;਍        紀 攀氀猀攀 笀ഀഀ
          ChildScope = function() {}; // should be anonymous; This is so that when the minifier munges਍            ⼀⼀ 琀栀攀 渀愀洀攀 椀琀 搀漀攀猀 渀漀琀 戀攀挀漀洀攀 爀愀渀搀漀洀 猀攀琀 漀昀 挀栀愀爀猀⸀ 吀栀椀猀 眀椀氀氀 琀栀攀渀 猀栀漀眀 甀瀀 愀猀 挀氀愀猀猀ഀഀ
            // name in the web inspector.਍          䌀栀椀氀搀匀挀漀瀀攀⸀瀀爀漀琀漀琀礀瀀攀 㴀 琀栀椀猀㬀ഀഀ
          child = new ChildScope();਍          挀栀椀氀搀⸀␀椀搀 㴀 渀攀砀琀唀椀搀⠀⤀㬀ഀഀ
        }਍        挀栀椀氀搀嬀✀琀栀椀猀✀崀 㴀 挀栀椀氀搀㬀ഀഀ
        child.$$listeners = {};਍        挀栀椀氀搀⸀␀␀氀椀猀琀攀渀攀爀䌀漀甀渀琀 㴀 笀紀㬀ഀഀ
        child.$parent = this;਍        挀栀椀氀搀⸀␀␀眀愀琀挀栀攀爀猀 㴀 挀栀椀氀搀⸀␀␀渀攀砀琀匀椀戀氀椀渀最 㴀 挀栀椀氀搀⸀␀␀挀栀椀氀搀䠀攀愀搀 㴀 挀栀椀氀搀⸀␀␀挀栀椀氀搀吀愀椀氀 㴀 渀甀氀氀㬀ഀഀ
        child.$$prevSibling = this.$$childTail;਍        椀昀 ⠀琀栀椀猀⸀␀␀挀栀椀氀搀䠀攀愀搀⤀ 笀ഀഀ
          this.$$childTail.$$nextSibling = child;਍          琀栀椀猀⸀␀␀挀栀椀氀搀吀愀椀氀 㴀 挀栀椀氀搀㬀ഀഀ
        } else {਍          琀栀椀猀⸀␀␀挀栀椀氀搀䠀攀愀搀 㴀 琀栀椀猀⸀␀␀挀栀椀氀搀吀愀椀氀 㴀 挀栀椀氀搀㬀ഀഀ
        }਍        爀攀琀甀爀渀 挀栀椀氀搀㬀ഀഀ
      },਍ഀഀ
      /**਍       ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
       * @name ng.$rootScope.Scope#$watch਍       ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀ഀഀ
       * @function਍       ⨀ഀഀ
       * @description਍       ⨀ 刀攀最椀猀琀攀爀猀 愀 怀氀椀猀琀攀渀攀爀怀 挀愀氀氀戀愀挀欀 琀漀 戀攀 攀砀攀挀甀琀攀搀 眀栀攀渀攀瘀攀爀 琀栀攀 怀眀愀琀挀栀䔀砀瀀爀攀猀猀椀漀渀怀 挀栀愀渀最攀猀⸀ഀഀ
       *਍       ⨀ ⴀ 吀栀攀 怀眀愀琀挀栀䔀砀瀀爀攀猀猀椀漀渀怀 椀猀 挀愀氀氀攀搀 漀渀 攀瘀攀爀礀 挀愀氀氀 琀漀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀搀椀最攀猀琀ഀഀ
       *   $digest()} and should return the value that will be watched. (Since਍       ⨀   笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀搀椀最攀猀琀 ␀搀椀最攀猀琀⠀⤀紀 爀攀爀甀渀猀 眀栀攀渀 椀琀 搀攀琀攀挀琀猀 挀栀愀渀最攀猀 琀栀攀ഀഀ
       *   `watchExpression` can execute multiple times per਍       ⨀   笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀搀椀最攀猀琀 ␀搀椀最攀猀琀⠀⤀紀 愀渀搀 猀栀漀甀氀搀 戀攀 椀搀攀洀瀀漀琀攀渀琀⸀⤀ഀഀ
       * - The `listener` is called only when the value from the current `watchExpression` and the਍       ⨀   瀀爀攀瘀椀漀甀猀 挀愀氀氀 琀漀 怀眀愀琀挀栀䔀砀瀀爀攀猀猀椀漀渀怀 愀爀攀 渀漀琀 攀焀甀愀氀 ⠀眀椀琀栀 琀栀攀 攀砀挀攀瀀琀椀漀渀 漀昀 琀栀攀 椀渀椀琀椀愀氀 爀甀渀Ⰰഀഀ
       *   see below). The inequality is determined according to਍       ⨀   笀䀀氀椀渀欀 愀渀最甀氀愀爀⸀攀焀甀愀氀猀紀 昀甀渀挀琀椀漀渀⸀ 吀漀 猀愀瘀攀 琀栀攀 瘀愀氀甀攀 漀昀 琀栀攀 漀戀樀攀挀琀 昀漀爀 氀愀琀攀爀 挀漀洀瀀愀爀椀猀漀渀Ⰰഀഀ
       *   the {@link angular.copy} function is used. It also means that watching complex options਍       ⨀   眀椀氀氀 栀愀瘀攀 愀搀瘀攀爀猀攀 洀攀洀漀爀礀 愀渀搀 瀀攀爀昀漀爀洀愀渀挀攀 椀洀瀀氀椀挀愀琀椀漀渀猀⸀ഀഀ
       * - The watch `listener` may change the model, which may trigger other `listener`s to fire.਍       ⨀   吀栀椀猀 椀猀 愀挀栀椀攀瘀攀搀 戀礀 爀攀爀甀渀渀椀渀最 琀栀攀 眀愀琀挀栀攀爀猀 甀渀琀椀氀 渀漀 挀栀愀渀最攀猀 愀爀攀 搀攀琀攀挀琀攀搀⸀ 吀栀攀 爀攀爀甀渀ഀഀ
       *   iteration limit is 10 to prevent an infinite loop deadlock.਍       ⨀ഀഀ
       *਍       ⨀ 䤀昀 礀漀甀 眀愀渀琀 琀漀 戀攀 渀漀琀椀昀椀攀搀 眀栀攀渀攀瘀攀爀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀搀椀最攀猀琀 ␀搀椀最攀猀琀紀 椀猀 挀愀氀氀攀搀Ⰰഀഀ
       * you can register a `watchExpression` function with no `listener`. (Since `watchExpression`਍       ⨀ 挀愀渀 攀砀攀挀甀琀攀 洀甀氀琀椀瀀氀攀 琀椀洀攀猀 瀀攀爀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀搀椀最攀猀琀 ␀搀椀最攀猀琀紀 挀礀挀氀攀 眀栀攀渀 愀ഀഀ
       * change is detected, be prepared for multiple calls to your listener.)਍       ⨀ഀഀ
       * After a watcher is registered with the scope, the `listener` fn is called asynchronously਍       ⨀ ⠀瘀椀愀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀攀瘀愀氀䄀猀礀渀挀 ␀攀瘀愀氀䄀猀礀渀挀紀⤀ 琀漀 椀渀椀琀椀愀氀椀稀攀 琀栀攀ഀഀ
       * watcher. In rare cases, this is undesirable because the listener is called when the result਍       ⨀ 漀昀 怀眀愀琀挀栀䔀砀瀀爀攀猀猀椀漀渀怀 搀椀搀渀✀琀 挀栀愀渀最攀⸀ 吀漀 搀攀琀攀挀琀 琀栀椀猀 猀挀攀渀愀爀椀漀 眀椀琀栀椀渀 琀栀攀 怀氀椀猀琀攀渀攀爀怀 昀渀Ⰰ 礀漀甀ഀഀ
       * can compare the `newVal` and `oldVal`. If these two values are identical (`===`) then the਍       ⨀ 氀椀猀琀攀渀攀爀 眀愀猀 挀愀氀氀攀搀 搀甀攀 琀漀 椀渀椀琀椀愀氀椀稀愀琀椀漀渀⸀ഀഀ
       *਍       ⨀ 吀栀攀 攀砀愀洀瀀氀攀 戀攀氀漀眀 挀漀渀琀愀椀渀猀 愀渀 椀氀氀甀猀琀爀愀琀椀漀渀 漀昀 甀猀椀渀最 愀 昀甀渀挀琀椀漀渀 愀猀 礀漀甀爀 ␀眀愀琀挀栀 氀椀猀琀攀渀攀爀ഀഀ
       *਍       ⨀ഀഀ
       * # Example਍       ⨀ 㰀瀀爀攀㸀ഀഀ
           // let's assume that scope was dependency injected as the $rootScope਍           瘀愀爀 猀挀漀瀀攀 㴀 ␀爀漀漀琀匀挀漀瀀攀㬀ഀഀ
           scope.name = 'misko';਍           猀挀漀瀀攀⸀挀漀甀渀琀攀爀 㴀 　㬀ഀഀ
਍           攀砀瀀攀挀琀⠀猀挀漀瀀攀⸀挀漀甀渀琀攀爀⤀⸀琀漀䔀焀甀愀氀⠀　⤀㬀ഀഀ
           scope.$watch('name', function(newValue, oldValue) {਍             猀挀漀瀀攀⸀挀漀甀渀琀攀爀 㴀 猀挀漀瀀攀⸀挀漀甀渀琀攀爀 ⬀ ㄀㬀ഀഀ
           });਍           攀砀瀀攀挀琀⠀猀挀漀瀀攀⸀挀漀甀渀琀攀爀⤀⸀琀漀䔀焀甀愀氀⠀　⤀㬀ഀഀ
਍           猀挀漀瀀攀⸀␀搀椀最攀猀琀⠀⤀㬀ഀഀ
           // no variable change਍           攀砀瀀攀挀琀⠀猀挀漀瀀攀⸀挀漀甀渀琀攀爀⤀⸀琀漀䔀焀甀愀氀⠀　⤀㬀ഀഀ
਍           猀挀漀瀀攀⸀渀愀洀攀 㴀 ✀愀搀愀洀✀㬀ഀഀ
           scope.$digest();਍           攀砀瀀攀挀琀⠀猀挀漀瀀攀⸀挀漀甀渀琀攀爀⤀⸀琀漀䔀焀甀愀氀⠀㄀⤀㬀ഀഀ
਍ഀഀ
਍           ⼀⼀ 唀猀椀渀最 愀 氀椀猀琀攀渀攀爀 昀甀渀挀琀椀漀渀ഀഀ
           var food;਍           猀挀漀瀀攀⸀昀漀漀搀䌀漀甀渀琀攀爀 㴀 　㬀ഀഀ
           expect(scope.foodCounter).toEqual(0);਍           猀挀漀瀀攀⸀␀眀愀琀挀栀⠀ഀഀ
             // This is the listener function਍             昀甀渀挀琀椀漀渀⠀⤀ 笀 爀攀琀甀爀渀 昀漀漀搀㬀 紀Ⰰഀഀ
             // This is the change handler਍             昀甀渀挀琀椀漀渀⠀渀攀眀嘀愀氀甀攀Ⰰ 漀氀搀嘀愀氀甀攀⤀ 笀ഀഀ
               if ( newValue !== oldValue ) {਍                 ⼀⼀ 伀渀氀礀 椀渀挀爀攀洀攀渀琀 琀栀攀 挀漀甀渀琀攀爀 椀昀 琀栀攀 瘀愀氀甀攀 挀栀愀渀最攀搀ഀഀ
                 scope.foodCounter = scope.foodCounter + 1;਍               紀ഀഀ
             }਍           ⤀㬀ഀഀ
           // No digest has been run so the counter will be zero਍           攀砀瀀攀挀琀⠀猀挀漀瀀攀⸀昀漀漀搀䌀漀甀渀琀攀爀⤀⸀琀漀䔀焀甀愀氀⠀　⤀㬀ഀഀ
਍           ⼀⼀ 刀甀渀 琀栀攀 搀椀最攀猀琀 戀甀琀 猀椀渀挀攀 昀漀漀搀 栀愀猀 渀漀琀 挀栀愀渀最攀搀 挀漀甀渀琀 眀椀氀氀 猀琀椀氀氀 戀攀 稀攀爀漀ഀഀ
           scope.$digest();਍           攀砀瀀攀挀琀⠀猀挀漀瀀攀⸀昀漀漀搀䌀漀甀渀琀攀爀⤀⸀琀漀䔀焀甀愀氀⠀　⤀㬀ഀഀ
਍           ⼀⼀ 唀瀀搀愀琀攀 昀漀漀搀 愀渀搀 爀甀渀 搀椀最攀猀琀⸀  一漀眀 琀栀攀 挀漀甀渀琀攀爀 眀椀氀氀 椀渀挀爀攀洀攀渀琀ഀഀ
           food = 'cheeseburger';਍           猀挀漀瀀攀⸀␀搀椀最攀猀琀⠀⤀㬀ഀഀ
           expect(scope.foodCounter).toEqual(1);਍ഀഀ
       * </pre>਍       ⨀ഀഀ
       *਍       ⨀ഀഀ
       * @param {(function()|string)} watchExpression Expression that is evaluated on each਍       ⨀    笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀搀椀最攀猀琀 ␀搀椀最攀猀琀紀 挀礀挀氀攀⸀ 䄀 挀栀愀渀最攀 椀渀 琀栀攀 爀攀琀甀爀渀 瘀愀氀甀攀 琀爀椀最最攀爀猀ഀഀ
       *    a call to the `listener`.਍       ⨀ഀഀ
       *    - `string`: Evaluated as {@link guide/expression expression}਍       ⨀    ⴀ 怀昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀⤀怀㨀 挀愀氀氀攀搀 眀椀琀栀 挀甀爀爀攀渀琀 怀猀挀漀瀀攀怀 愀猀 愀 瀀愀爀愀洀攀琀攀爀⸀ഀഀ
       * @param {(function()|string)=} listener Callback called whenever the return value of਍       ⨀   琀栀攀 怀眀愀琀挀栀䔀砀瀀爀攀猀猀椀漀渀怀 挀栀愀渀最攀猀⸀ഀഀ
       *਍       ⨀    ⴀ 怀猀琀爀椀渀最怀㨀 䔀瘀愀氀甀愀琀攀搀 愀猀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 攀砀瀀爀攀猀猀椀漀渀紀ഀഀ
       *    - `function(newValue, oldValue, scope)`: called with current and previous values as਍       ⨀      瀀愀爀愀洀攀琀攀爀猀⸀ഀഀ
       *਍       ⨀ 䀀瀀愀爀愀洀 笀戀漀漀氀攀愀渀㴀紀 漀戀樀攀挀琀䔀焀甀愀氀椀琀礀 䌀漀洀瀀愀爀攀 漀戀樀攀挀琀 昀漀爀 攀焀甀愀氀椀琀礀 爀愀琀栀攀爀 琀栀愀渀 昀漀爀 爀攀昀攀爀攀渀挀攀⸀ഀഀ
       * @returns {function()} Returns a deregistration function for this listener.਍       ⨀⼀ഀഀ
      $watch: function(watchExp, listener, objectEquality) {਍        瘀愀爀 猀挀漀瀀攀 㴀 琀栀椀猀Ⰰഀഀ
            get = compileToFn(watchExp, 'watch'),਍            愀爀爀愀礀 㴀 猀挀漀瀀攀⸀␀␀眀愀琀挀栀攀爀猀Ⰰഀഀ
            watcher = {਍              昀渀㨀 氀椀猀琀攀渀攀爀Ⰰഀഀ
              last: initWatchVal,਍              最攀琀㨀 最攀琀Ⰰഀഀ
              exp: watchExp,਍              攀焀㨀 ℀℀漀戀樀攀挀琀䔀焀甀愀氀椀琀礀ഀഀ
            };਍ഀഀ
        lastDirtyWatch = null;਍ഀഀ
        // in the case user pass string, we need to compile it, do we really need this ?਍        椀昀 ⠀℀椀猀䘀甀渀挀琀椀漀渀⠀氀椀猀琀攀渀攀爀⤀⤀ 笀ഀഀ
          var listenFn = compileToFn(listener || noop, 'listener');਍          眀愀琀挀栀攀爀⸀昀渀 㴀 昀甀渀挀琀椀漀渀⠀渀攀眀嘀愀氀Ⰰ 漀氀搀嘀愀氀Ⰰ 猀挀漀瀀攀⤀ 笀氀椀猀琀攀渀䘀渀⠀猀挀漀瀀攀⤀㬀紀㬀ഀഀ
        }਍ഀഀ
        if (typeof watchExp == 'string' && get.constant) {਍          瘀愀爀 漀爀椀最椀渀愀氀䘀渀 㴀 眀愀琀挀栀攀爀⸀昀渀㬀ഀഀ
          watcher.fn = function(newVal, oldVal, scope) {਍            漀爀椀最椀渀愀氀䘀渀⸀挀愀氀氀⠀琀栀椀猀Ⰰ 渀攀眀嘀愀氀Ⰰ 漀氀搀嘀愀氀Ⰰ 猀挀漀瀀攀⤀㬀ഀഀ
            arrayRemove(array, watcher);਍          紀㬀ഀഀ
        }਍ഀഀ
        if (!array) {਍          愀爀爀愀礀 㴀 猀挀漀瀀攀⸀␀␀眀愀琀挀栀攀爀猀 㴀 嬀崀㬀ഀഀ
        }਍        ⼀⼀ 眀攀 甀猀攀 甀渀猀栀椀昀琀 猀椀渀挀攀 眀攀 甀猀攀 愀 眀栀椀氀攀 氀漀漀瀀 椀渀 ␀搀椀最攀猀琀 昀漀爀 猀瀀攀攀搀⸀ഀഀ
        // the while loop reads in reverse order.਍        愀爀爀愀礀⸀甀渀猀栀椀昀琀⠀眀愀琀挀栀攀爀⤀㬀ഀഀ
਍        爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          arrayRemove(array, watcher);਍          氀愀猀琀䐀椀爀琀礀圀愀琀挀栀 㴀 渀甀氀氀㬀ഀഀ
        };਍      紀Ⰰഀഀ
਍ഀഀ
      /**਍       ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
       * @name ng.$rootScope.Scope#$watchCollection਍       ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀ഀഀ
       * @function਍       ⨀ഀഀ
       * @description਍       ⨀ 匀栀愀氀氀漀眀 眀愀琀挀栀攀猀 琀栀攀 瀀爀漀瀀攀爀琀椀攀猀 漀昀 愀渀 漀戀樀攀挀琀 愀渀搀 昀椀爀攀猀 眀栀攀渀攀瘀攀爀 愀渀礀 漀昀 琀栀攀 瀀爀漀瀀攀爀琀椀攀猀 挀栀愀渀最攀ഀഀ
       * (for arrays, this implies watching the array items; for object maps, this implies watching਍       ⨀ 琀栀攀 瀀爀漀瀀攀爀琀椀攀猀⤀⸀ 䤀昀 愀 挀栀愀渀最攀 椀猀 搀攀琀攀挀琀攀搀Ⰰ 琀栀攀 怀氀椀猀琀攀渀攀爀怀 挀愀氀氀戀愀挀欀 椀猀 昀椀爀攀搀⸀ഀഀ
       *਍       ⨀ ⴀ 吀栀攀 怀漀戀樀怀 挀漀氀氀攀挀琀椀漀渀 椀猀 漀戀猀攀爀瘀攀搀 瘀椀愀 猀琀愀渀搀愀爀搀 ␀眀愀琀挀栀 漀瀀攀爀愀琀椀漀渀 愀渀搀 椀猀 攀砀愀洀椀渀攀搀 漀渀 攀瘀攀爀礀ഀഀ
       *   call to $digest() to see if any items have been added, removed, or moved.਍       ⨀ ⴀ 吀栀攀 怀氀椀猀琀攀渀攀爀怀 椀猀 挀愀氀氀攀搀 眀栀攀渀攀瘀攀爀 愀渀礀琀栀椀渀最 眀椀琀栀椀渀 琀栀攀 怀漀戀樀怀 栀愀猀 挀栀愀渀最攀搀⸀ 䔀砀愀洀瀀氀攀猀 椀渀挀氀甀搀攀ഀഀ
       *   adding, removing, and moving items belonging to an object or array.਍       ⨀ഀഀ
       *਍       ⨀ ⌀ 䔀砀愀洀瀀氀攀ഀഀ
       * <pre>਍          ␀猀挀漀瀀攀⸀渀愀洀攀猀 㴀 嬀✀椀最漀爀✀Ⰰ ✀洀愀琀椀愀猀✀Ⰰ ✀洀椀猀欀漀✀Ⰰ ✀樀愀洀攀猀✀崀㬀ഀഀ
          $scope.dataCount = 4;਍ഀഀ
          $scope.$watchCollection('names', function(newNames, oldNames) {਍            ␀猀挀漀瀀攀⸀搀愀琀愀䌀漀甀渀琀 㴀 渀攀眀一愀洀攀猀⸀氀攀渀最琀栀㬀ഀഀ
          });਍ഀഀ
          expect($scope.dataCount).toEqual(4);਍          ␀猀挀漀瀀攀⸀␀搀椀最攀猀琀⠀⤀㬀ഀഀ
਍          ⼀⼀猀琀椀氀氀 愀琀 㐀 ⸀⸀⸀ 渀漀 挀栀愀渀最攀猀ഀഀ
          expect($scope.dataCount).toEqual(4);਍ഀഀ
          $scope.names.pop();਍          ␀猀挀漀瀀攀⸀␀搀椀最攀猀琀⠀⤀㬀ഀഀ
਍          ⼀⼀渀漀眀 琀栀攀爀攀✀猀 戀攀攀渀 愀 挀栀愀渀最攀ഀഀ
          expect($scope.dataCount).toEqual(3);਍       ⨀ 㰀⼀瀀爀攀㸀ഀഀ
       *਍       ⨀ഀഀ
       * @param {string|Function(scope)} obj Evaluated as {@link guide/expression expression}. The਍       ⨀    攀砀瀀爀攀猀猀椀漀渀 瘀愀氀甀攀 猀栀漀甀氀搀 攀瘀愀氀甀愀琀攀 琀漀 愀渀 漀戀樀攀挀琀 漀爀 愀渀 愀爀爀愀礀 眀栀椀挀栀 椀猀 漀戀猀攀爀瘀攀搀 漀渀 攀愀挀栀ഀഀ
       *    {@link ng.$rootScope.Scope#methods_$digest $digest} cycle. Any shallow change within the਍       ⨀    挀漀氀氀攀挀琀椀漀渀 眀椀氀氀 琀爀椀最最攀爀 愀 挀愀氀氀 琀漀 琀栀攀 怀氀椀猀琀攀渀攀爀怀⸀ഀഀ
       *਍       ⨀ 䀀瀀愀爀愀洀 笀昀甀渀挀琀椀漀渀⠀渀攀眀䌀漀氀氀攀挀琀椀漀渀Ⰰ 漀氀搀䌀漀氀氀攀挀琀椀漀渀Ⰰ 猀挀漀瀀攀⤀紀 氀椀猀琀攀渀攀爀 愀 挀愀氀氀戀愀挀欀 昀甀渀挀琀椀漀渀 琀栀愀琀 椀猀ഀഀ
       *    fired with both the `newCollection` and `oldCollection` as parameters.਍       ⨀    吀栀攀 怀渀攀眀䌀漀氀氀攀挀琀椀漀渀怀 漀戀樀攀挀琀 椀猀 琀栀攀 渀攀眀氀礀 洀漀搀椀昀椀攀搀 搀愀琀愀 漀戀琀愀椀渀攀搀 昀爀漀洀 琀栀攀 怀漀戀樀怀 攀砀瀀爀攀猀猀椀漀渀ഀഀ
       *    and the `oldCollection` object is a copy of the former collection data.਍       ⨀    吀栀攀 怀猀挀漀瀀攀怀 爀攀昀攀爀猀 琀漀 琀栀攀 挀甀爀爀攀渀琀 猀挀漀瀀攀⸀ഀഀ
       *਍       ⨀ 䀀爀攀琀甀爀渀猀 笀昀甀渀挀琀椀漀渀⠀⤀紀 刀攀琀甀爀渀猀 愀 搀攀ⴀ爀攀最椀猀琀爀愀琀椀漀渀 昀甀渀挀琀椀漀渀 昀漀爀 琀栀椀猀 氀椀猀琀攀渀攀爀⸀ 圀栀攀渀 琀栀攀ഀഀ
       *    de-registration function is executed, the internal watch operation is terminated.਍       ⨀⼀ഀഀ
      $watchCollection: function(obj, listener) {਍        瘀愀爀 猀攀氀昀 㴀 琀栀椀猀㬀ഀഀ
        var oldValue;਍        瘀愀爀 渀攀眀嘀愀氀甀攀㬀ഀഀ
        var changeDetected = 0;਍        瘀愀爀 漀戀樀䜀攀琀琀攀爀 㴀 ␀瀀愀爀猀攀⠀漀戀樀⤀㬀ഀഀ
        var internalArray = [];਍        瘀愀爀 椀渀琀攀爀渀愀氀伀戀樀攀挀琀 㴀 笀紀㬀ഀഀ
        var oldLength = 0;਍ഀഀ
        function $watchCollectionWatch() {਍          渀攀眀嘀愀氀甀攀 㴀 漀戀樀䜀攀琀琀攀爀⠀猀攀氀昀⤀㬀ഀഀ
          var newLength, key;਍ഀഀ
          if (!isObject(newValue)) {਍            椀昀 ⠀漀氀搀嘀愀氀甀攀 ℀㴀㴀 渀攀眀嘀愀氀甀攀⤀ 笀ഀഀ
              oldValue = newValue;਍              挀栀愀渀最攀䐀攀琀攀挀琀攀搀⬀⬀㬀ഀഀ
            }਍          紀 攀氀猀攀 椀昀 ⠀椀猀䄀爀爀愀礀䰀椀欀攀⠀渀攀眀嘀愀氀甀攀⤀⤀ 笀ഀഀ
            if (oldValue !== internalArray) {਍              ⼀⼀ 眀攀 愀爀攀 琀爀愀渀猀椀琀椀漀渀椀渀最 昀爀漀洀 猀漀洀攀琀栀椀渀最 眀栀椀挀栀 眀愀猀 渀漀琀 愀渀 愀爀爀愀礀 椀渀琀漀 愀爀爀愀礀⸀ഀഀ
              oldValue = internalArray;਍              漀氀搀䰀攀渀最琀栀 㴀 漀氀搀嘀愀氀甀攀⸀氀攀渀最琀栀 㴀 　㬀ഀഀ
              changeDetected++;਍            紀ഀഀ
਍            渀攀眀䰀攀渀最琀栀 㴀 渀攀眀嘀愀氀甀攀⸀氀攀渀最琀栀㬀ഀഀ
਍            椀昀 ⠀漀氀搀䰀攀渀最琀栀 ℀㴀㴀 渀攀眀䰀攀渀最琀栀⤀ 笀ഀഀ
              // if lengths do not match we need to trigger change notification਍              挀栀愀渀最攀䐀攀琀攀挀琀攀搀⬀⬀㬀ഀഀ
              oldValue.length = oldLength = newLength;਍            紀ഀഀ
            // copy the items to oldValue and look for changes.਍            昀漀爀 ⠀瘀愀爀 椀 㴀 　㬀 椀 㰀 渀攀眀䰀攀渀最琀栀㬀 椀⬀⬀⤀ 笀ഀഀ
              if (oldValue[i] !== newValue[i]) {਍                挀栀愀渀最攀䐀攀琀攀挀琀攀搀⬀⬀㬀ഀഀ
                oldValue[i] = newValue[i];਍              紀ഀഀ
            }਍          紀 攀氀猀攀 笀ഀഀ
            if (oldValue !== internalObject) {਍              ⼀⼀ 眀攀 愀爀攀 琀爀愀渀猀椀琀椀漀渀椀渀最 昀爀漀洀 猀漀洀攀琀栀椀渀最 眀栀椀挀栀 眀愀猀 渀漀琀 愀渀 漀戀樀攀挀琀 椀渀琀漀 漀戀樀攀挀琀⸀ഀഀ
              oldValue = internalObject = {};਍              漀氀搀䰀攀渀最琀栀 㴀 　㬀ഀഀ
              changeDetected++;਍            紀ഀഀ
            // copy the items to oldValue and look for changes.਍            渀攀眀䰀攀渀最琀栀 㴀 　㬀ഀഀ
            for (key in newValue) {਍              椀昀 ⠀渀攀眀嘀愀氀甀攀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀欀攀礀⤀⤀ 笀ഀഀ
                newLength++;਍                椀昀 ⠀漀氀搀嘀愀氀甀攀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀欀攀礀⤀⤀ 笀ഀഀ
                  if (oldValue[key] !== newValue[key]) {਍                    挀栀愀渀最攀䐀攀琀攀挀琀攀搀⬀⬀㬀ഀഀ
                    oldValue[key] = newValue[key];਍                  紀ഀഀ
                } else {਍                  漀氀搀䰀攀渀最琀栀⬀⬀㬀ഀഀ
                  oldValue[key] = newValue[key];਍                  挀栀愀渀最攀䐀攀琀攀挀琀攀搀⬀⬀㬀ഀഀ
                }਍              紀ഀഀ
            }਍            椀昀 ⠀漀氀搀䰀攀渀最琀栀 㸀 渀攀眀䰀攀渀最琀栀⤀ 笀ഀഀ
              // we used to have more keys, need to find them and destroy them.਍              挀栀愀渀最攀䐀攀琀攀挀琀攀搀⬀⬀㬀ഀഀ
              for(key in oldValue) {਍                椀昀 ⠀漀氀搀嘀愀氀甀攀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀欀攀礀⤀ ☀☀ ℀渀攀眀嘀愀氀甀攀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀欀攀礀⤀⤀ 笀ഀഀ
                  oldLength--;਍                  搀攀氀攀琀攀 漀氀搀嘀愀氀甀攀嬀欀攀礀崀㬀ഀഀ
                }਍              紀ഀഀ
            }਍          紀ഀഀ
          return changeDetected;਍        紀ഀഀ
਍        昀甀渀挀琀椀漀渀 ␀眀愀琀挀栀䌀漀氀氀攀挀琀椀漀渀䄀挀琀椀漀渀⠀⤀ 笀ഀഀ
          listener(newValue, oldValue, self);਍        紀ഀഀ
਍        爀攀琀甀爀渀 琀栀椀猀⸀␀眀愀琀挀栀⠀␀眀愀琀挀栀䌀漀氀氀攀挀琀椀漀渀圀愀琀挀栀Ⰰ ␀眀愀琀挀栀䌀漀氀氀攀挀琀椀漀渀䄀挀琀椀漀渀⤀㬀ഀഀ
      },਍ഀഀ
      /**਍       ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
       * @name ng.$rootScope.Scope#$digest਍       ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀ഀഀ
       * @function਍       ⨀ഀഀ
       * @description਍       ⨀ 倀爀漀挀攀猀猀攀猀 愀氀氀 漀昀 琀栀攀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀眀愀琀挀栀 眀愀琀挀栀攀爀猀紀 漀昀 琀栀攀 挀甀爀爀攀渀琀 猀挀漀瀀攀 愀渀搀ഀഀ
       * its children. Because a {@link ng.$rootScope.Scope#methods_$watch watcher}'s listener can change਍       ⨀ 琀栀攀 洀漀搀攀氀Ⰰ 琀栀攀 怀␀搀椀最攀猀琀⠀⤀怀 欀攀攀瀀猀 挀愀氀氀椀渀最 琀栀攀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀眀愀琀挀栀 眀愀琀挀栀攀爀猀紀ഀഀ
       * until no more listeners are firing. This means that it is possible to get into an infinite਍       ⨀ 氀漀漀瀀⸀ 吀栀椀猀 昀甀渀挀琀椀漀渀 眀椀氀氀 琀栀爀漀眀 怀✀䴀愀砀椀洀甀洀 椀琀攀爀愀琀椀漀渀 氀椀洀椀琀 攀砀挀攀攀搀攀搀⸀✀怀 椀昀 琀栀攀 渀甀洀戀攀爀 漀昀ഀഀ
       * iterations exceeds 10.਍       ⨀ഀഀ
       * Usually, you don't call `$digest()` directly in਍       ⨀ 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䌀漀渀琀爀漀氀氀攀爀 挀漀渀琀爀漀氀氀攀爀猀紀 漀爀 椀渀ഀഀ
       * {@link ng.$compileProvider#methods_directive directives}.਍       ⨀ 䤀渀猀琀攀愀搀Ⰰ 礀漀甀 猀栀漀甀氀搀 挀愀氀氀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀愀瀀瀀氀礀 ␀愀瀀瀀氀礀⠀⤀紀 ⠀琀礀瀀椀挀愀氀氀礀 昀爀漀洀 眀椀琀栀椀渀ഀഀ
       * a {@link ng.$compileProvider#methods_directive directives}), which will force a `$digest()`.਍       ⨀ഀഀ
       * If you want to be notified whenever `$digest()` is called,਍       ⨀ 礀漀甀 挀愀渀 爀攀最椀猀琀攀爀 愀 怀眀愀琀挀栀䔀砀瀀爀攀猀猀椀漀渀怀 昀甀渀挀琀椀漀渀 眀椀琀栀ഀഀ
       * {@link ng.$rootScope.Scope#methods_$watch $watch()} with no `listener`.਍       ⨀ഀഀ
       * In unit tests, you may need to call `$digest()` to simulate the scope life cycle.਍       ⨀ഀഀ
       * # Example਍       ⨀ 㰀瀀爀攀㸀ഀഀ
           var scope = ...;਍           猀挀漀瀀攀⸀渀愀洀攀 㴀 ✀洀椀猀欀漀✀㬀ഀഀ
           scope.counter = 0;਍ഀഀ
           expect(scope.counter).toEqual(0);਍           猀挀漀瀀攀⸀␀眀愀琀挀栀⠀✀渀愀洀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀渀攀眀嘀愀氀甀攀Ⰰ 漀氀搀嘀愀氀甀攀⤀ 笀ഀഀ
             scope.counter = scope.counter + 1;਍           紀⤀㬀ഀഀ
           expect(scope.counter).toEqual(0);਍ഀഀ
           scope.$digest();਍           ⼀⼀ 渀漀 瘀愀爀椀愀戀氀攀 挀栀愀渀最攀ഀഀ
           expect(scope.counter).toEqual(0);਍ഀഀ
           scope.name = 'adam';਍           猀挀漀瀀攀⸀␀搀椀最攀猀琀⠀⤀㬀ഀഀ
           expect(scope.counter).toEqual(1);਍       ⨀ 㰀⼀瀀爀攀㸀ഀഀ
       *਍       ⨀⼀ഀഀ
      $digest: function() {਍        瘀愀爀 眀愀琀挀栀Ⰰ 瘀愀氀甀攀Ⰰ 氀愀猀琀Ⰰഀഀ
            watchers,਍            愀猀礀渀挀儀甀攀甀攀 㴀 琀栀椀猀⸀␀␀愀猀礀渀挀儀甀攀甀攀Ⰰഀഀ
            postDigestQueue = this.$$postDigestQueue,਍            氀攀渀最琀栀Ⰰഀഀ
            dirty, ttl = TTL,਍            渀攀砀琀Ⰰ 挀甀爀爀攀渀琀Ⰰ 琀愀爀最攀琀 㴀 琀栀椀猀Ⰰഀഀ
            watchLog = [],਍            氀漀最䤀搀砀Ⰰ 氀漀最䴀猀最Ⰰ 愀猀礀渀挀吀愀猀欀㬀ഀഀ
਍        戀攀最椀渀倀栀愀猀攀⠀✀␀搀椀最攀猀琀✀⤀㬀ഀഀ
਍        氀愀猀琀䐀椀爀琀礀圀愀琀挀栀 㴀 渀甀氀氀㬀ഀഀ
਍        搀漀 笀 ⼀⼀ ∀眀栀椀氀攀 搀椀爀琀礀∀ 氀漀漀瀀ഀഀ
          dirty = false;਍          挀甀爀爀攀渀琀 㴀 琀愀爀最攀琀㬀ഀഀ
਍          眀栀椀氀攀⠀愀猀礀渀挀儀甀攀甀攀⸀氀攀渀最琀栀⤀ 笀ഀഀ
            try {਍              愀猀礀渀挀吀愀猀欀 㴀 愀猀礀渀挀儀甀攀甀攀⸀猀栀椀昀琀⠀⤀㬀ഀഀ
              asyncTask.scope.$eval(asyncTask.expression);਍            紀 挀愀琀挀栀 ⠀攀⤀ 笀ഀഀ
              clearPhase();਍              ␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀⠀攀⤀㬀ഀഀ
            }਍            氀愀猀琀䐀椀爀琀礀圀愀琀挀栀 㴀 渀甀氀氀㬀ഀഀ
          }਍ഀഀ
          traverseScopesLoop:਍          搀漀 笀 ⼀⼀ ∀琀爀愀瘀攀爀猀攀 琀栀攀 猀挀漀瀀攀猀∀ 氀漀漀瀀ഀഀ
            if ((watchers = current.$$watchers)) {਍              ⼀⼀ 瀀爀漀挀攀猀猀 漀甀爀 眀愀琀挀栀攀猀ഀഀ
              length = watchers.length;਍              眀栀椀氀攀 ⠀氀攀渀最琀栀ⴀⴀ⤀ 笀ഀഀ
                try {਍                  眀愀琀挀栀 㴀 眀愀琀挀栀攀爀猀嬀氀攀渀最琀栀崀㬀ഀഀ
                  // Most common watches are on primitives, in which case we can short਍                  ⼀⼀ 挀椀爀挀甀椀琀 椀琀 眀椀琀栀 㴀㴀㴀 漀瀀攀爀愀琀漀爀Ⰰ 漀渀氀礀 眀栀攀渀 㴀㴀㴀 昀愀椀氀猀 搀漀 眀攀 甀猀攀 ⸀攀焀甀愀氀猀ഀഀ
                  if (watch) {਍                    椀昀 ⠀⠀瘀愀氀甀攀 㴀 眀愀琀挀栀⸀最攀琀⠀挀甀爀爀攀渀琀⤀⤀ ℀㴀㴀 ⠀氀愀猀琀 㴀 眀愀琀挀栀⸀氀愀猀琀⤀ ☀☀ഀഀ
                        !(watch.eq਍                            㼀 攀焀甀愀氀猀⠀瘀愀氀甀攀Ⰰ 氀愀猀琀⤀ഀഀ
                            : (typeof value == 'number' && typeof last == 'number'਍                               ☀☀ 椀猀一愀一⠀瘀愀氀甀攀⤀ ☀☀ 椀猀一愀一⠀氀愀猀琀⤀⤀⤀⤀ 笀ഀഀ
                      dirty = true;਍                      氀愀猀琀䐀椀爀琀礀圀愀琀挀栀 㴀 眀愀琀挀栀㬀ഀഀ
                      watch.last = watch.eq ? copy(value) : value;਍                      眀愀琀挀栀⸀昀渀⠀瘀愀氀甀攀Ⰰ ⠀⠀氀愀猀琀 㴀㴀㴀 椀渀椀琀圀愀琀挀栀嘀愀氀⤀ 㼀 瘀愀氀甀攀 㨀 氀愀猀琀⤀Ⰰ 挀甀爀爀攀渀琀⤀㬀ഀഀ
                      if (ttl < 5) {਍                        氀漀最䤀搀砀 㴀 㐀 ⴀ 琀琀氀㬀ഀഀ
                        if (!watchLog[logIdx]) watchLog[logIdx] = [];਍                        氀漀最䴀猀最 㴀 ⠀椀猀䘀甀渀挀琀椀漀渀⠀眀愀琀挀栀⸀攀砀瀀⤀⤀ഀഀ
                            ? 'fn: ' + (watch.exp.name || watch.exp.toString())਍                            㨀 眀愀琀挀栀⸀攀砀瀀㬀ഀഀ
                        logMsg += '; newVal: ' + toJson(value) + '; oldVal: ' + toJson(last);਍                        眀愀琀挀栀䰀漀最嬀氀漀最䤀搀砀崀⸀瀀甀猀栀⠀氀漀最䴀猀最⤀㬀ഀഀ
                      }਍                    紀 攀氀猀攀 椀昀 ⠀眀愀琀挀栀 㴀㴀㴀 氀愀猀琀䐀椀爀琀礀圀愀琀挀栀⤀ 笀ഀഀ
                      // If the most recently dirty watcher is now clean, short circuit since the remaining watchers਍                      ⼀⼀ 栀愀瘀攀 愀氀爀攀愀搀礀 戀攀攀渀 琀攀猀琀攀搀⸀ഀഀ
                      dirty = false;਍                      戀爀攀愀欀 琀爀愀瘀攀爀猀攀匀挀漀瀀攀猀䰀漀漀瀀㬀ഀഀ
                    }਍                  紀ഀഀ
                } catch (e) {਍                  挀氀攀愀爀倀栀愀猀攀⠀⤀㬀ഀഀ
                  $exceptionHandler(e);਍                紀ഀഀ
              }਍            紀ഀഀ
਍            ⼀⼀ 䤀渀猀愀渀椀琀礀 圀愀爀渀椀渀最㨀 猀挀漀瀀攀 搀攀瀀琀栀ⴀ昀椀爀猀琀 琀爀愀瘀攀爀猀愀氀ഀഀ
            // yes, this code is a bit crazy, but it works and we have tests to prove it!਍            ⼀⼀ 琀栀椀猀 瀀椀攀挀攀 猀栀漀甀氀搀 戀攀 欀攀瀀琀 椀渀 猀礀渀挀 眀椀琀栀 琀栀攀 琀爀愀瘀攀爀猀愀氀 椀渀 ␀戀爀漀愀搀挀愀猀琀ഀഀ
            if (!(next = (current.$$childHead ||਍                ⠀挀甀爀爀攀渀琀 ℀㴀㴀 琀愀爀最攀琀 ☀☀ 挀甀爀爀攀渀琀⸀␀␀渀攀砀琀匀椀戀氀椀渀最⤀⤀⤀⤀ 笀ഀഀ
              while(current !== target && !(next = current.$$nextSibling)) {਍                挀甀爀爀攀渀琀 㴀 挀甀爀爀攀渀琀⸀␀瀀愀爀攀渀琀㬀ഀഀ
              }਍            紀ഀഀ
          } while ((current = next));਍ഀഀ
          // `break traverseScopesLoop;` takes us to here਍ഀഀ
          if(dirty && !(ttl--)) {਍            挀氀攀愀爀倀栀愀猀攀⠀⤀㬀ഀഀ
            throw $rootScopeMinErr('infdig',਍                ✀笀　紀 ␀搀椀最攀猀琀⠀⤀ 椀琀攀爀愀琀椀漀渀猀 爀攀愀挀栀攀搀⸀ 䄀戀漀爀琀椀渀最℀尀渀✀ ⬀ഀഀ
                'Watchers fired in the last 5 iterations: {1}',਍                吀吀䰀Ⰰ 琀漀䨀猀漀渀⠀眀愀琀挀栀䰀漀最⤀⤀㬀ഀഀ
          }਍ഀഀ
        } while (dirty || asyncQueue.length);਍ഀഀ
        clearPhase();਍ഀഀ
        while(postDigestQueue.length) {਍          琀爀礀 笀ഀഀ
            postDigestQueue.shift()();਍          紀 挀愀琀挀栀 ⠀攀⤀ 笀ഀഀ
            $exceptionHandler(e);਍          紀ഀഀ
        }਍      紀Ⰰഀഀ
਍ഀഀ
      /**਍       ⨀ 䀀渀最搀漀挀 攀瘀攀渀琀ഀഀ
       * @name ng.$rootScope.Scope#$destroy਍       ⨀ 䀀攀瘀攀渀琀伀昀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀ഀഀ
       * @eventType broadcast on scope being destroyed਍       ⨀ഀഀ
       * @description਍       ⨀ 䈀爀漀愀搀挀愀猀琀攀搀 眀栀攀渀 愀 猀挀漀瀀攀 愀渀搀 椀琀猀 挀栀椀氀搀爀攀渀 愀爀攀 戀攀椀渀最 搀攀猀琀爀漀礀攀搀⸀ഀഀ
       *਍       ⨀ 一漀琀攀 琀栀愀琀Ⰰ 椀渀 䄀渀最甀氀愀爀䨀匀Ⰰ 琀栀攀爀攀 椀猀 愀氀猀漀 愀 怀␀搀攀猀琀爀漀礀怀 樀儀甀攀爀礀 攀瘀攀渀琀Ⰰ 眀栀椀挀栀 挀愀渀 戀攀 甀猀攀搀 琀漀ഀഀ
       * clean up DOM bindings before an element is removed from the DOM.਍       ⨀⼀ഀഀ
਍      ⼀⨀⨀ഀഀ
       * @ngdoc function਍       ⨀ 䀀渀愀洀攀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀␀搀攀猀琀爀漀礀ഀഀ
       * @methodOf ng.$rootScope.Scope਍       ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
       *਍       ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
       * Removes the current scope (and all of its children) from the parent scope. Removal implies਍       ⨀ 琀栀愀琀 挀愀氀氀猀 琀漀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀搀椀最攀猀琀 ␀搀椀最攀猀琀⠀⤀紀 眀椀氀氀 渀漀 氀漀渀最攀爀ഀഀ
       * propagate to the current scope and its children. Removal also implies that the current਍       ⨀ 猀挀漀瀀攀 椀猀 攀氀椀最椀戀氀攀 昀漀爀 最愀爀戀愀最攀 挀漀氀氀攀挀琀椀漀渀⸀ഀഀ
       *਍       ⨀ 吀栀攀 怀␀搀攀猀琀爀漀礀⠀⤀怀 椀猀 甀猀甀愀氀氀礀 甀猀攀搀 戀礀 搀椀爀攀挀琀椀瘀攀猀 猀甀挀栀 愀猀ഀഀ
       * {@link ng.directive:ngRepeat ngRepeat} for managing the਍       ⨀ 甀渀爀漀氀氀椀渀最 漀昀 琀栀攀 氀漀漀瀀⸀ഀഀ
       *਍       ⨀ 䨀甀猀琀 戀攀昀漀爀攀 愀 猀挀漀瀀攀 椀猀 搀攀猀琀爀漀礀攀搀Ⰰ 愀 怀␀搀攀猀琀爀漀礀怀 攀瘀攀渀琀 椀猀 戀爀漀愀搀挀愀猀琀攀搀 漀渀 琀栀椀猀 猀挀漀瀀攀⸀ഀഀ
       * Application code can register a `$destroy` event handler that will give it a chance to਍       ⨀ 瀀攀爀昀漀爀洀 愀渀礀 渀攀挀攀猀猀愀爀礀 挀氀攀愀渀甀瀀⸀ഀഀ
       *਍       ⨀ 一漀琀攀 琀栀愀琀Ⰰ 椀渀 䄀渀最甀氀愀爀䨀匀Ⰰ 琀栀攀爀攀 椀猀 愀氀猀漀 愀 怀␀搀攀猀琀爀漀礀怀 樀儀甀攀爀礀 攀瘀攀渀琀Ⰰ 眀栀椀挀栀 挀愀渀 戀攀 甀猀攀搀 琀漀ഀഀ
       * clean up DOM bindings before an element is removed from the DOM.਍       ⨀⼀ഀഀ
      $destroy: function() {਍        ⼀⼀ 眀攀 挀愀渀✀琀 搀攀猀琀爀漀礀 琀栀攀 爀漀漀琀 猀挀漀瀀攀 漀爀 愀 猀挀漀瀀攀 琀栀愀琀 栀愀猀 戀攀攀渀 愀氀爀攀愀搀礀 搀攀猀琀爀漀礀攀搀ഀഀ
        if (this.$$destroyed) return;਍        瘀愀爀 瀀愀爀攀渀琀 㴀 琀栀椀猀⸀␀瀀愀爀攀渀琀㬀ഀഀ
਍        琀栀椀猀⸀␀戀爀漀愀搀挀愀猀琀⠀✀␀搀攀猀琀爀漀礀✀⤀㬀ഀഀ
        this.$$destroyed = true;਍        椀昀 ⠀琀栀椀猀 㴀㴀㴀 ␀爀漀漀琀匀挀漀瀀攀⤀ 爀攀琀甀爀渀㬀ഀഀ
਍        昀漀爀䔀愀挀栀⠀琀栀椀猀⸀␀␀氀椀猀琀攀渀攀爀䌀漀甀渀琀Ⰰ 戀椀渀搀⠀渀甀氀氀Ⰰ 搀攀挀爀攀洀攀渀琀䰀椀猀琀攀渀攀爀䌀漀甀渀琀Ⰰ 琀栀椀猀⤀⤀㬀ഀഀ
਍        椀昀 ⠀瀀愀爀攀渀琀⸀␀␀挀栀椀氀搀䠀攀愀搀 㴀㴀 琀栀椀猀⤀ 瀀愀爀攀渀琀⸀␀␀挀栀椀氀搀䠀攀愀搀 㴀 琀栀椀猀⸀␀␀渀攀砀琀匀椀戀氀椀渀最㬀ഀഀ
        if (parent.$$childTail == this) parent.$$childTail = this.$$prevSibling;਍        椀昀 ⠀琀栀椀猀⸀␀␀瀀爀攀瘀匀椀戀氀椀渀最⤀ 琀栀椀猀⸀␀␀瀀爀攀瘀匀椀戀氀椀渀最⸀␀␀渀攀砀琀匀椀戀氀椀渀最 㴀 琀栀椀猀⸀␀␀渀攀砀琀匀椀戀氀椀渀最㬀ഀഀ
        if (this.$$nextSibling) this.$$nextSibling.$$prevSibling = this.$$prevSibling;਍ഀഀ
        // This is bogus code that works around Chrome's GC leak਍        ⼀⼀ 猀攀攀㨀 栀琀琀瀀猀㨀⼀⼀最椀琀栀甀戀⸀挀漀洀⼀愀渀最甀氀愀爀⼀愀渀最甀氀愀爀⸀樀猀⼀椀猀猀甀攀猀⼀㄀㌀㄀㌀⌀椀猀猀甀攀挀漀洀洀攀渀琀ⴀ㄀　㌀㜀㠀㐀㔀㄀ഀഀ
        this.$parent = this.$$nextSibling = this.$$prevSibling = this.$$childHead =਍            琀栀椀猀⸀␀␀挀栀椀氀搀吀愀椀氀 㴀 渀甀氀氀㬀ഀഀ
      },਍ഀഀ
      /**਍       ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
       * @name ng.$rootScope.Scope#$eval਍       ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀ഀഀ
       * @function਍       ⨀ഀഀ
       * @description਍       ⨀ 䔀砀攀挀甀琀攀猀 琀栀攀 怀攀砀瀀爀攀猀猀椀漀渀怀 漀渀 琀栀攀 挀甀爀爀攀渀琀 猀挀漀瀀攀 愀渀搀 爀攀琀甀爀渀猀 琀栀攀 爀攀猀甀氀琀⸀ 䄀渀礀 攀砀挀攀瀀琀椀漀渀猀 椀渀ഀഀ
       * the expression are propagated (uncaught). This is useful when evaluating Angular਍       ⨀ 攀砀瀀爀攀猀猀椀漀渀猀⸀ഀഀ
       *਍       ⨀ ⌀ 䔀砀愀洀瀀氀攀ഀഀ
       * <pre>਍           瘀愀爀 猀挀漀瀀攀 㴀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⠀⤀㬀ഀഀ
           scope.a = 1;਍           猀挀漀瀀攀⸀戀 㴀 ㈀㬀ഀഀ
਍           攀砀瀀攀挀琀⠀猀挀漀瀀攀⸀␀攀瘀愀氀⠀✀愀⬀戀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀㌀⤀㬀ഀഀ
           expect(scope.$eval(function(scope){ return scope.a + scope.b; })).toEqual(3);਍       ⨀ 㰀⼀瀀爀攀㸀ഀഀ
       *਍       ⨀ 䀀瀀愀爀愀洀 笀⠀猀琀爀椀渀最簀昀甀渀挀琀椀漀渀⠀⤀⤀㴀紀 攀砀瀀爀攀猀猀椀漀渀 䄀渀 愀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 琀漀 戀攀 攀砀攀挀甀琀攀搀⸀ഀഀ
       *਍       ⨀    ⴀ 怀猀琀爀椀渀最怀㨀 攀砀攀挀甀琀攀 甀猀椀渀最 琀栀攀 爀甀氀攀猀 愀猀 搀攀昀椀渀攀搀 椀渀  笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 攀砀瀀爀攀猀猀椀漀渀紀⸀ഀഀ
       *    - `function(scope)`: execute the function with the current `scope` parameter.਍       ⨀ഀഀ
       * @param {(object)=} locals Local variables object, useful for overriding values in scope.਍       ⨀ 䀀爀攀琀甀爀渀猀 笀⨀紀 吀栀攀 爀攀猀甀氀琀 漀昀 攀瘀愀氀甀愀琀椀渀最 琀栀攀 攀砀瀀爀攀猀猀椀漀渀⸀ഀഀ
       */਍      ␀攀瘀愀氀㨀 昀甀渀挀琀椀漀渀⠀攀砀瀀爀Ⰰ 氀漀挀愀氀猀⤀ 笀ഀഀ
        return $parse(expr)(this, locals);਍      紀Ⰰഀഀ
਍      ⼀⨀⨀ഀഀ
       * @ngdoc function਍       ⨀ 䀀渀愀洀攀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀␀攀瘀愀氀䄀猀礀渀挀ഀഀ
       * @methodOf ng.$rootScope.Scope਍       ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
       *਍       ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
       * Executes the expression on the current scope at a later point in time.਍       ⨀ഀഀ
       * The `$evalAsync` makes no guarantees as to when the `expression` will be executed, only਍       ⨀ 琀栀愀琀㨀ഀഀ
       *਍       ⨀   ⴀ 椀琀 眀椀氀氀 攀砀攀挀甀琀攀 愀昀琀攀爀 琀栀攀 昀甀渀挀琀椀漀渀 琀栀愀琀 猀挀栀攀搀甀氀攀搀 琀栀攀 攀瘀愀氀甀愀琀椀漀渀 ⠀瀀爀攀昀攀爀愀戀氀礀 戀攀昀漀爀攀 䐀伀䴀ഀഀ
       *     rendering).਍       ⨀   ⴀ 愀琀 氀攀愀猀琀 漀渀攀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀搀椀最攀猀琀 ␀搀椀最攀猀琀 挀礀挀氀攀紀 眀椀氀氀 戀攀 瀀攀爀昀漀爀洀攀搀 愀昀琀攀爀ഀഀ
       *     `expression` execution.਍       ⨀ഀഀ
       * Any exceptions from the execution of the expression are forwarded to the਍       ⨀ 笀䀀氀椀渀欀 渀最⸀␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀 ␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀紀 猀攀爀瘀椀挀攀⸀ഀഀ
       *਍       ⨀ 开开一漀琀攀㨀开开 椀昀 琀栀椀猀 昀甀渀挀琀椀漀渀 椀猀 挀愀氀氀攀搀 漀甀琀猀椀搀攀 漀昀 愀 怀␀搀椀最攀猀琀怀 挀礀挀氀攀Ⰰ 愀 渀攀眀 怀␀搀椀最攀猀琀怀 挀礀挀氀攀ഀഀ
       * will be scheduled. However, it is encouraged to always call code that changes the model਍       ⨀ 昀爀漀洀 眀椀琀栀椀渀 愀渀 怀␀愀瀀瀀氀礀怀 挀愀氀氀⸀ 吀栀愀琀 椀渀挀氀甀搀攀猀 挀漀搀攀 攀瘀愀氀甀愀琀攀搀 瘀椀愀 怀␀攀瘀愀氀䄀猀礀渀挀怀⸀ഀഀ
       *਍       ⨀ 䀀瀀愀爀愀洀 笀⠀猀琀爀椀渀最簀昀甀渀挀琀椀漀渀⠀⤀⤀㴀紀 攀砀瀀爀攀猀猀椀漀渀 䄀渀 愀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 琀漀 戀攀 攀砀攀挀甀琀攀搀⸀ഀഀ
       *਍       ⨀    ⴀ 怀猀琀爀椀渀最怀㨀 攀砀攀挀甀琀攀 甀猀椀渀最 琀栀攀 爀甀氀攀猀 愀猀 搀攀昀椀渀攀搀 椀渀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 攀砀瀀爀攀猀猀椀漀渀紀⸀ഀഀ
       *    - `function(scope)`: execute the function with the current `scope` parameter.਍       ⨀ഀഀ
       */਍      ␀攀瘀愀氀䄀猀礀渀挀㨀 昀甀渀挀琀椀漀渀⠀攀砀瀀爀⤀ 笀ഀഀ
        // if we are outside of an $digest loop and this is the first time we are scheduling async਍        ⼀⼀ 琀愀猀欀 愀氀猀漀 猀挀栀攀搀甀氀攀 愀猀礀渀挀 愀甀琀漀ⴀ昀氀甀猀栀ഀഀ
        if (!$rootScope.$$phase && !$rootScope.$$asyncQueue.length) {਍          ␀戀爀漀眀猀攀爀⸀搀攀昀攀爀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
            if ($rootScope.$$asyncQueue.length) {਍              ␀爀漀漀琀匀挀漀瀀攀⸀␀搀椀最攀猀琀⠀⤀㬀ഀഀ
            }਍          紀⤀㬀ഀഀ
        }਍ഀഀ
        this.$$asyncQueue.push({scope: this, expression: expr});਍      紀Ⰰഀഀ
਍      ␀␀瀀漀猀琀䐀椀最攀猀琀 㨀 昀甀渀挀琀椀漀渀⠀昀渀⤀ 笀ഀഀ
        this.$$postDigestQueue.push(fn);਍      紀Ⰰഀഀ
਍      ⼀⨀⨀ഀഀ
       * @ngdoc function਍       ⨀ 䀀渀愀洀攀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀␀愀瀀瀀氀礀ഀഀ
       * @methodOf ng.$rootScope.Scope਍       ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
       *਍       ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
       * `$apply()` is used to execute an expression in angular from outside of the angular਍       ⨀ 昀爀愀洀攀眀漀爀欀⸀ ⠀䘀漀爀 攀砀愀洀瀀氀攀 昀爀漀洀 戀爀漀眀猀攀爀 䐀伀䴀 攀瘀攀渀琀猀Ⰰ 猀攀琀吀椀洀攀漀甀琀Ⰰ 堀䠀刀 漀爀 琀栀椀爀搀 瀀愀爀琀礀 氀椀戀爀愀爀椀攀猀⤀⸀ഀഀ
       * Because we are calling into the angular framework we need to perform proper scope life਍       ⨀ 挀礀挀氀攀 漀昀 笀䀀氀椀渀欀 渀最⸀␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀 攀砀挀攀瀀琀椀漀渀 栀愀渀搀氀椀渀最紀Ⰰഀഀ
       * {@link ng.$rootScope.Scope#methods_$digest executing watches}.਍       ⨀ഀഀ
       * ## Life cycle਍       ⨀ഀഀ
       * # Pseudo-Code of `$apply()`਍       ⨀ 㰀瀀爀攀㸀ഀഀ
           function $apply(expr) {਍             琀爀礀 笀ഀഀ
               return $eval(expr);਍             紀 挀愀琀挀栀 ⠀攀⤀ 笀ഀഀ
               $exceptionHandler(e);਍             紀 昀椀渀愀氀氀礀 笀ഀഀ
               $root.$digest();਍             紀ഀഀ
           }਍       ⨀ 㰀⼀瀀爀攀㸀ഀഀ
       *਍       ⨀ഀഀ
       * Scope's `$apply()` method transitions through the following stages:਍       ⨀ഀഀ
       * 1. The {@link guide/expression expression} is executed using the਍       ⨀    笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀攀瘀愀氀 ␀攀瘀愀氀⠀⤀紀 洀攀琀栀漀搀⸀ഀഀ
       * 2. Any exceptions from the execution of the expression are forwarded to the਍       ⨀    笀䀀氀椀渀欀 渀最⸀␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀 ␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀紀 猀攀爀瘀椀挀攀⸀ഀഀ
       * 3. The {@link ng.$rootScope.Scope#methods_$watch watch} listeners are fired immediately after the਍       ⨀    攀砀瀀爀攀猀猀椀漀渀 眀愀猀 攀砀攀挀甀琀攀搀 甀猀椀渀最 琀栀攀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀搀椀最攀猀琀 ␀搀椀最攀猀琀⠀⤀紀 洀攀琀栀漀搀⸀ഀഀ
       *਍       ⨀ഀഀ
       * @param {(string|function())=} exp An angular expression to be executed.਍       ⨀ഀഀ
       *    - `string`: execute using the rules as defined in {@link guide/expression expression}.਍       ⨀    ⴀ 怀昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀⤀怀㨀 攀砀攀挀甀琀攀 琀栀攀 昀甀渀挀琀椀漀渀 眀椀琀栀 挀甀爀爀攀渀琀 怀猀挀漀瀀攀怀 瀀愀爀愀洀攀琀攀爀⸀ഀഀ
       *਍       ⨀ 䀀爀攀琀甀爀渀猀 笀⨀紀 吀栀攀 爀攀猀甀氀琀 漀昀 攀瘀愀氀甀愀琀椀渀最 琀栀攀 攀砀瀀爀攀猀猀椀漀渀⸀ഀഀ
       */਍      ␀愀瀀瀀氀礀㨀 昀甀渀挀琀椀漀渀⠀攀砀瀀爀⤀ 笀ഀഀ
        try {਍          戀攀最椀渀倀栀愀猀攀⠀✀␀愀瀀瀀氀礀✀⤀㬀ഀഀ
          return this.$eval(expr);਍        紀 挀愀琀挀栀 ⠀攀⤀ 笀ഀഀ
          $exceptionHandler(e);਍        紀 昀椀渀愀氀氀礀 笀ഀഀ
          clearPhase();਍          琀爀礀 笀ഀഀ
            $rootScope.$digest();਍          紀 挀愀琀挀栀 ⠀攀⤀ 笀ഀഀ
            $exceptionHandler(e);਍            琀栀爀漀眀 攀㬀ഀഀ
          }਍        紀ഀഀ
      },਍ഀഀ
      /**਍       ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
       * @name ng.$rootScope.Scope#$on਍       ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀ഀഀ
       * @function਍       ⨀ഀഀ
       * @description਍       ⨀ 䰀椀猀琀攀渀猀 漀渀 攀瘀攀渀琀猀 漀昀 愀 最椀瘀攀渀 琀礀瀀攀⸀ 匀攀攀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀攀洀椀琀 ␀攀洀椀琀紀 昀漀爀ഀഀ
       * discussion of event life cycle.਍       ⨀ഀഀ
       * The event listener function format is: `function(event, args...)`. The `event` object਍       ⨀ 瀀愀猀猀攀搀 椀渀琀漀 琀栀攀 氀椀猀琀攀渀攀爀 栀愀猀 琀栀攀 昀漀氀氀漀眀椀渀最 愀琀琀爀椀戀甀琀攀猀㨀ഀഀ
       *਍       ⨀   ⴀ 怀琀愀爀最攀琀匀挀漀瀀攀怀 ⴀ 怀笀匀挀漀瀀攀紀怀㨀 琀栀攀 猀挀漀瀀攀 漀渀 眀栀椀挀栀 琀栀攀 攀瘀攀渀琀 眀愀猀 怀␀攀洀椀琀怀ⴀ攀搀 漀爀ഀഀ
       *     `$broadcast`-ed.਍       ⨀   ⴀ 怀挀甀爀爀攀渀琀匀挀漀瀀攀怀 ⴀ 怀笀匀挀漀瀀攀紀怀㨀 琀栀攀 挀甀爀爀攀渀琀 猀挀漀瀀攀 眀栀椀挀栀 椀猀 栀愀渀搀氀椀渀最 琀栀攀 攀瘀攀渀琀⸀ഀഀ
       *   - `name` - `{string}`: name of the event.਍       ⨀   ⴀ 怀猀琀漀瀀倀爀漀瀀愀最愀琀椀漀渀怀 ⴀ 怀笀昀甀渀挀琀椀漀渀㴀紀怀㨀 挀愀氀氀椀渀最 怀猀琀漀瀀倀爀漀瀀愀最愀琀椀漀渀怀 昀甀渀挀琀椀漀渀 眀椀氀氀 挀愀渀挀攀氀ഀഀ
       *     further event propagation (available only for events that were `$emit`-ed).਍       ⨀   ⴀ 怀瀀爀攀瘀攀渀琀䐀攀昀愀甀氀琀怀 ⴀ 怀笀昀甀渀挀琀椀漀渀紀怀㨀 挀愀氀氀椀渀最 怀瀀爀攀瘀攀渀琀䐀攀昀愀甀氀琀怀 猀攀琀猀 怀搀攀昀愀甀氀琀倀爀攀瘀攀渀琀攀搀怀 昀氀愀最ഀഀ
       *     to true.਍       ⨀   ⴀ 怀搀攀昀愀甀氀琀倀爀攀瘀攀渀琀攀搀怀 ⴀ 怀笀戀漀漀氀攀愀渀紀怀㨀 琀爀甀攀 椀昀 怀瀀爀攀瘀攀渀琀䐀攀昀愀甀氀琀怀 眀愀猀 挀愀氀氀攀搀⸀ഀഀ
       *਍       ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀愀洀攀 䔀瘀攀渀琀 渀愀洀攀 琀漀 氀椀猀琀攀渀 漀渀⸀ഀഀ
       * @param {function(event, args...)} listener Function to call when the event is emitted.਍       ⨀ 䀀爀攀琀甀爀渀猀 笀昀甀渀挀琀椀漀渀⠀⤀紀 刀攀琀甀爀渀猀 愀 搀攀爀攀最椀猀琀爀愀琀椀漀渀 昀甀渀挀琀椀漀渀 昀漀爀 琀栀椀猀 氀椀猀琀攀渀攀爀⸀ഀഀ
       */਍      ␀漀渀㨀 昀甀渀挀琀椀漀渀⠀渀愀洀攀Ⰰ 氀椀猀琀攀渀攀爀⤀ 笀ഀഀ
        var namedListeners = this.$$listeners[name];਍        椀昀 ⠀℀渀愀洀攀搀䰀椀猀琀攀渀攀爀猀⤀ 笀ഀഀ
          this.$$listeners[name] = namedListeners = [];਍        紀ഀഀ
        namedListeners.push(listener);਍ഀഀ
        var current = this;਍        搀漀 笀ഀഀ
          if (!current.$$listenerCount[name]) {਍            挀甀爀爀攀渀琀⸀␀␀氀椀猀琀攀渀攀爀䌀漀甀渀琀嬀渀愀洀攀崀 㴀 　㬀ഀഀ
          }਍          挀甀爀爀攀渀琀⸀␀␀氀椀猀琀攀渀攀爀䌀漀甀渀琀嬀渀愀洀攀崀⬀⬀㬀ഀഀ
        } while ((current = current.$parent));਍ഀഀ
        var self = this;਍        爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          namedListeners[indexOf(namedListeners, listener)] = null;਍          搀攀挀爀攀洀攀渀琀䰀椀猀琀攀渀攀爀䌀漀甀渀琀⠀猀攀氀昀Ⰰ ㄀Ⰰ 渀愀洀攀⤀㬀ഀഀ
        };਍      紀Ⰰഀഀ
਍ഀഀ
      /**਍       ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
       * @name ng.$rootScope.Scope#$emit਍       ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀ഀഀ
       * @function਍       ⨀ഀഀ
       * @description਍       ⨀ 䐀椀猀瀀愀琀挀栀攀猀 愀渀 攀瘀攀渀琀 怀渀愀洀攀怀 甀瀀眀愀爀搀猀 琀栀爀漀甀最栀 琀栀攀 猀挀漀瀀攀 栀椀攀爀愀爀挀栀礀 渀漀琀椀昀礀椀渀最 琀栀攀ഀഀ
       * registered {@link ng.$rootScope.Scope#methods_$on} listeners.਍       ⨀ഀഀ
       * The event life cycle starts at the scope on which `$emit` was called. All਍       ⨀ 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀漀渀 氀椀猀琀攀渀攀爀猀紀 氀椀猀琀攀渀椀渀最 昀漀爀 怀渀愀洀攀怀 攀瘀攀渀琀 漀渀 琀栀椀猀 猀挀漀瀀攀 最攀琀ഀഀ
       * notified. Afterwards, the event traverses upwards toward the root scope and calls all਍       ⨀ 爀攀最椀猀琀攀爀攀搀 氀椀猀琀攀渀攀爀猀 愀氀漀渀最 琀栀攀 眀愀礀⸀ 吀栀攀 攀瘀攀渀琀 眀椀氀氀 猀琀漀瀀 瀀爀漀瀀愀最愀琀椀渀最 椀昀 漀渀攀 漀昀 琀栀攀 氀椀猀琀攀渀攀爀猀ഀഀ
       * cancels it.਍       ⨀ഀഀ
       * Any exception emitted from the {@link ng.$rootScope.Scope#methods_$on listeners} will be passed਍       ⨀ 漀渀琀漀 琀栀攀 笀䀀氀椀渀欀 渀最⸀␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀 ␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀紀 猀攀爀瘀椀挀攀⸀ഀഀ
       *਍       ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀愀洀攀 䔀瘀攀渀琀 渀愀洀攀 琀漀 攀洀椀琀⸀ഀഀ
       * @param {...*} args Optional set of arguments which will be passed onto the event listeners.਍       ⨀ 䀀爀攀琀甀爀渀 笀伀戀樀攀挀琀紀 䔀瘀攀渀琀 漀戀樀攀挀琀 ⠀猀攀攀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀漀渀紀⤀⸀ഀഀ
       */਍      ␀攀洀椀琀㨀 昀甀渀挀琀椀漀渀⠀渀愀洀攀Ⰰ 愀爀最猀⤀ 笀ഀഀ
        var empty = [],਍            渀愀洀攀搀䰀椀猀琀攀渀攀爀猀Ⰰഀഀ
            scope = this,਍            猀琀漀瀀倀爀漀瀀愀最愀琀椀漀渀 㴀 昀愀氀猀攀Ⰰഀഀ
            event = {਍              渀愀洀攀㨀 渀愀洀攀Ⰰഀഀ
              targetScope: scope,਍              猀琀漀瀀倀爀漀瀀愀最愀琀椀漀渀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀猀琀漀瀀倀爀漀瀀愀最愀琀椀漀渀 㴀 琀爀甀攀㬀紀Ⰰഀഀ
              preventDefault: function() {਍                攀瘀攀渀琀⸀搀攀昀愀甀氀琀倀爀攀瘀攀渀琀攀搀 㴀 琀爀甀攀㬀ഀഀ
              },਍              搀攀昀愀甀氀琀倀爀攀瘀攀渀琀攀搀㨀 昀愀氀猀攀ഀഀ
            },਍            氀椀猀琀攀渀攀爀䄀爀最猀 㴀 挀漀渀挀愀琀⠀嬀攀瘀攀渀琀崀Ⰰ 愀爀最甀洀攀渀琀猀Ⰰ ㄀⤀Ⰰഀഀ
            i, length;਍ഀഀ
        do {਍          渀愀洀攀搀䰀椀猀琀攀渀攀爀猀 㴀 猀挀漀瀀攀⸀␀␀氀椀猀琀攀渀攀爀猀嬀渀愀洀攀崀 簀簀 攀洀瀀琀礀㬀ഀഀ
          event.currentScope = scope;਍          昀漀爀 ⠀椀㴀　Ⰰ 氀攀渀最琀栀㴀渀愀洀攀搀䰀椀猀琀攀渀攀爀猀⸀氀攀渀最琀栀㬀 椀㰀氀攀渀最琀栀㬀 椀⬀⬀⤀ 笀ഀഀ
਍            ⼀⼀ 椀昀 氀椀猀琀攀渀攀爀猀 眀攀爀攀 搀攀爀攀最椀猀琀攀爀攀搀Ⰰ 搀攀昀爀愀最洀攀渀琀 琀栀攀 愀爀爀愀礀ഀഀ
            if (!namedListeners[i]) {਍              渀愀洀攀搀䰀椀猀琀攀渀攀爀猀⸀猀瀀氀椀挀攀⠀椀Ⰰ ㄀⤀㬀ഀഀ
              i--;਍              氀攀渀最琀栀ⴀⴀ㬀ഀഀ
              continue;਍            紀ഀഀ
            try {਍              ⼀⼀愀氀氀漀眀 愀氀氀 氀椀猀琀攀渀攀爀猀 愀琀琀愀挀栀攀搀 琀漀 琀栀攀 挀甀爀爀攀渀琀 猀挀漀瀀攀 琀漀 爀甀渀ഀഀ
              namedListeners[i].apply(null, listenerArgs);਍            紀 挀愀琀挀栀 ⠀攀⤀ 笀ഀഀ
              $exceptionHandler(e);਍            紀ഀഀ
          }਍          ⼀⼀椀昀 愀渀礀 氀椀猀琀攀渀攀爀 漀渀 琀栀攀 挀甀爀爀攀渀琀 猀挀漀瀀攀 猀琀漀瀀猀 瀀爀漀瀀愀最愀琀椀漀渀Ⰰ 瀀爀攀瘀攀渀琀 戀甀戀戀氀椀渀最ഀഀ
          if (stopPropagation) return event;਍          ⼀⼀琀爀愀瘀攀爀猀攀 甀瀀眀愀爀搀猀ഀഀ
          scope = scope.$parent;਍        紀 眀栀椀氀攀 ⠀猀挀漀瀀攀⤀㬀ഀഀ
਍        爀攀琀甀爀渀 攀瘀攀渀琀㬀ഀഀ
      },਍ഀഀ
਍      ⼀⨀⨀ഀഀ
       * @ngdoc function਍       ⨀ 䀀渀愀洀攀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀␀戀爀漀愀搀挀愀猀琀ഀഀ
       * @methodOf ng.$rootScope.Scope਍       ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
       *਍       ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
       * Dispatches an event `name` downwards to all child scopes (and their children) notifying the਍       ⨀ 爀攀最椀猀琀攀爀攀搀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀漀渀紀 氀椀猀琀攀渀攀爀猀⸀ഀഀ
       *਍       ⨀ 吀栀攀 攀瘀攀渀琀 氀椀昀攀 挀礀挀氀攀 猀琀愀爀琀猀 愀琀 琀栀攀 猀挀漀瀀攀 漀渀 眀栀椀挀栀 怀␀戀爀漀愀搀挀愀猀琀怀 眀愀猀 挀愀氀氀攀搀⸀ 䄀氀氀ഀഀ
       * {@link ng.$rootScope.Scope#methods_$on listeners} listening for `name` event on this scope get਍       ⨀ 渀漀琀椀昀椀攀搀⸀ 䄀昀琀攀爀眀愀爀搀猀Ⰰ 琀栀攀 攀瘀攀渀琀 瀀爀漀瀀愀最愀琀攀猀 琀漀 愀氀氀 搀椀爀攀挀琀 愀渀搀 椀渀搀椀爀攀挀琀 猀挀漀瀀攀猀 漀昀 琀栀攀 挀甀爀爀攀渀琀ഀഀ
       * scope and calls all registered listeners along the way. The event cannot be canceled.਍       ⨀ഀഀ
       * Any exception emitted from the {@link ng.$rootScope.Scope#methods_$on listeners} will be passed਍       ⨀ 漀渀琀漀 琀栀攀 笀䀀氀椀渀欀 渀最⸀␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀 ␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀紀 猀攀爀瘀椀挀攀⸀ഀഀ
       *਍       ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀愀洀攀 䔀瘀攀渀琀 渀愀洀攀 琀漀 戀爀漀愀搀挀愀猀琀⸀ഀഀ
       * @param {...*} args Optional set of arguments which will be passed onto the event listeners.਍       ⨀ 䀀爀攀琀甀爀渀 笀伀戀樀攀挀琀紀 䔀瘀攀渀琀 漀戀樀攀挀琀Ⰰ 猀攀攀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀漀渀紀ഀഀ
       */਍      ␀戀爀漀愀搀挀愀猀琀㨀 昀甀渀挀琀椀漀渀⠀渀愀洀攀Ⰰ 愀爀最猀⤀ 笀ഀഀ
        var target = this,਍            挀甀爀爀攀渀琀 㴀 琀愀爀最攀琀Ⰰഀഀ
            next = target,਍            攀瘀攀渀琀 㴀 笀ഀഀ
              name: name,਍              琀愀爀最攀琀匀挀漀瀀攀㨀 琀愀爀最攀琀Ⰰഀഀ
              preventDefault: function() {਍                攀瘀攀渀琀⸀搀攀昀愀甀氀琀倀爀攀瘀攀渀琀攀搀 㴀 琀爀甀攀㬀ഀഀ
              },਍              搀攀昀愀甀氀琀倀爀攀瘀攀渀琀攀搀㨀 昀愀氀猀攀ഀഀ
            },਍            氀椀猀琀攀渀攀爀䄀爀最猀 㴀 挀漀渀挀愀琀⠀嬀攀瘀攀渀琀崀Ⰰ 愀爀最甀洀攀渀琀猀Ⰰ ㄀⤀Ⰰഀഀ
            listeners, i, length;਍ഀഀ
        //down while you can, then up and next sibling or up and next sibling until back at root਍        眀栀椀氀攀 ⠀⠀挀甀爀爀攀渀琀 㴀 渀攀砀琀⤀⤀ 笀ഀഀ
          event.currentScope = current;਍          氀椀猀琀攀渀攀爀猀 㴀 挀甀爀爀攀渀琀⸀␀␀氀椀猀琀攀渀攀爀猀嬀渀愀洀攀崀 簀簀 嬀崀㬀ഀഀ
          for (i=0, length = listeners.length; i<length; i++) {਍            ⼀⼀ 椀昀 氀椀猀琀攀渀攀爀猀 眀攀爀攀 搀攀爀攀最椀猀琀攀爀攀搀Ⰰ 搀攀昀爀愀最洀攀渀琀 琀栀攀 愀爀爀愀礀ഀഀ
            if (!listeners[i]) {਍              氀椀猀琀攀渀攀爀猀⸀猀瀀氀椀挀攀⠀椀Ⰰ ㄀⤀㬀ഀഀ
              i--;਍              氀攀渀最琀栀ⴀⴀ㬀ഀഀ
              continue;਍            紀ഀഀ
਍            琀爀礀 笀ഀഀ
              listeners[i].apply(null, listenerArgs);਍            紀 挀愀琀挀栀⠀攀⤀ 笀ഀഀ
              $exceptionHandler(e);਍            紀ഀഀ
          }਍ഀഀ
          // Insanity Warning: scope depth-first traversal਍          ⼀⼀ 礀攀猀Ⰰ 琀栀椀猀 挀漀搀攀 椀猀 愀 戀椀琀 挀爀愀稀礀Ⰰ 戀甀琀 椀琀 眀漀爀欀猀 愀渀搀 眀攀 栀愀瘀攀 琀攀猀琀猀 琀漀 瀀爀漀瘀攀 椀琀℀ഀഀ
          // this piece should be kept in sync with the traversal in $digest਍          ⼀⼀ ⠀琀栀漀甀最栀 椀琀 搀椀昀昀攀爀猀 搀甀攀 琀漀 栀愀瘀椀渀最 琀栀攀 攀砀琀爀愀 挀栀攀挀欀 昀漀爀 ␀␀氀椀猀琀攀渀攀爀䌀漀甀渀琀⤀ഀഀ
          if (!(next = ((current.$$listenerCount[name] && current.$$childHead) ||਍              ⠀挀甀爀爀攀渀琀 ℀㴀㴀 琀愀爀最攀琀 ☀☀ 挀甀爀爀攀渀琀⸀␀␀渀攀砀琀匀椀戀氀椀渀最⤀⤀⤀⤀ 笀ഀഀ
            while(current !== target && !(next = current.$$nextSibling)) {਍              挀甀爀爀攀渀琀 㴀 挀甀爀爀攀渀琀⸀␀瀀愀爀攀渀琀㬀ഀഀ
            }਍          紀ഀഀ
        }਍ഀഀ
        return event;਍      紀ഀഀ
    };਍ഀഀ
    var $rootScope = new Scope();਍ഀഀ
    return $rootScope;਍ഀഀ
਍    昀甀渀挀琀椀漀渀 戀攀最椀渀倀栀愀猀攀⠀瀀栀愀猀攀⤀ 笀ഀഀ
      if ($rootScope.$$phase) {਍        琀栀爀漀眀 ␀爀漀漀琀匀挀漀瀀攀䴀椀渀䔀爀爀⠀✀椀渀瀀爀漀最✀Ⰰ ✀笀　紀 愀氀爀攀愀搀礀 椀渀 瀀爀漀最爀攀猀猀✀Ⰰ ␀爀漀漀琀匀挀漀瀀攀⸀␀␀瀀栀愀猀攀⤀㬀ഀഀ
      }਍ഀഀ
      $rootScope.$$phase = phase;਍    紀ഀഀ
਍    昀甀渀挀琀椀漀渀 挀氀攀愀爀倀栀愀猀攀⠀⤀ 笀ഀഀ
      $rootScope.$$phase = null;਍    紀ഀഀ
਍    昀甀渀挀琀椀漀渀 挀漀洀瀀椀氀攀吀漀䘀渀⠀攀砀瀀Ⰰ 渀愀洀攀⤀ 笀ഀഀ
      var fn = $parse(exp);਍      愀猀猀攀爀琀䄀爀最䘀渀⠀昀渀Ⰰ 渀愀洀攀⤀㬀ഀഀ
      return fn;਍    紀ഀഀ
਍    昀甀渀挀琀椀漀渀 搀攀挀爀攀洀攀渀琀䰀椀猀琀攀渀攀爀䌀漀甀渀琀⠀挀甀爀爀攀渀琀Ⰰ 挀漀甀渀琀Ⰰ 渀愀洀攀⤀ 笀ഀഀ
      do {਍        挀甀爀爀攀渀琀⸀␀␀氀椀猀琀攀渀攀爀䌀漀甀渀琀嬀渀愀洀攀崀 ⴀ㴀 挀漀甀渀琀㬀ഀഀ
਍        椀昀 ⠀挀甀爀爀攀渀琀⸀␀␀氀椀猀琀攀渀攀爀䌀漀甀渀琀嬀渀愀洀攀崀 㴀㴀㴀 　⤀ 笀ഀഀ
          delete current.$$listenerCount[name];਍        紀ഀഀ
      } while ((current = current.$parent));਍    紀ഀഀ
਍    ⼀⨀⨀ഀഀ
     * function used as an initial value for watchers.਍     ⨀ 戀攀挀愀甀猀攀 椀琀✀猀 甀渀椀焀甀攀 眀攀 挀愀渀 攀愀猀椀氀礀 琀攀氀氀 椀琀 愀瀀愀爀琀 昀爀漀洀 漀琀栀攀爀 瘀愀氀甀攀猀ഀഀ
     */਍    昀甀渀挀琀椀漀渀 椀渀椀琀圀愀琀挀栀嘀愀氀⠀⤀ 笀紀ഀഀ
  }];਍紀ഀഀ
਍⼀⨀⨀ഀഀ
 * @description਍ ⨀ 倀爀椀瘀愀琀攀 猀攀爀瘀椀挀攀 琀漀 猀愀渀椀琀椀稀攀 甀爀椀猀 昀漀爀 氀椀渀欀猀 愀渀搀 椀洀愀最攀猀⸀ 唀猀攀搀 戀礀 ␀挀漀洀瀀椀氀攀 愀渀搀 ␀猀愀渀椀琀椀稀攀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 ␀␀匀愀渀椀琀椀稀攀唀爀椀倀爀漀瘀椀搀攀爀⠀⤀ 笀ഀഀ
  var aHrefSanitizationWhitelist = /^\s*(https?|ftp|mailto|tel|file):/,਍    椀洀最匀爀挀匀愀渀椀琀椀稀愀琀椀漀渀圀栀椀琀攀氀椀猀琀 㴀 ⼀帀尀猀⨀⠀栀琀琀瀀猀㼀簀昀琀瀀簀昀椀氀攀⤀㨀簀搀愀琀愀㨀椀洀愀最攀尀⼀⼀㬀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @description਍   ⨀ 刀攀琀爀椀攀瘀攀猀 漀爀 漀瘀攀爀爀椀搀攀猀 琀栀攀 搀攀昀愀甀氀琀 爀攀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 琀栀愀琀 椀猀 甀猀攀搀 昀漀爀 眀栀椀琀攀氀椀猀琀椀渀最 漀昀 猀愀昀攀ഀഀ
   * urls during a[href] sanitization.਍   ⨀ഀഀ
   * The sanitization is a security measure aimed at prevent XSS attacks via html links.਍   ⨀ഀഀ
   * Any url about to be assigned to a[href] via data-binding is first normalized and turned into਍   ⨀ 愀渀 愀戀猀漀氀甀琀攀 甀爀氀⸀ 䄀昀琀攀爀眀愀爀搀猀Ⰰ 琀栀攀 甀爀氀 椀猀 洀愀琀挀栀攀搀 愀最愀椀渀猀琀 琀栀攀 怀愀䠀爀攀昀匀愀渀椀琀椀稀愀琀椀漀渀圀栀椀琀攀氀椀猀琀怀ഀഀ
   * regular expression. If a match is found, the original url is written into the dom. Otherwise,਍   ⨀ 琀栀攀 愀戀猀漀氀甀琀攀 甀爀氀 椀猀 瀀爀攀昀椀砀攀搀 眀椀琀栀 怀✀甀渀猀愀昀攀㨀✀怀 猀琀爀椀渀最 愀渀搀 漀渀氀礀 琀栀攀渀 椀猀 椀琀 眀爀椀琀琀攀渀 椀渀琀漀 琀栀攀 䐀伀䴀⸀ഀഀ
   *਍   ⨀ 䀀瀀愀爀愀洀 笀刀攀最䔀砀瀀㴀紀 爀攀最攀砀瀀 一攀眀 爀攀最攀砀瀀 琀漀 眀栀椀琀攀氀椀猀琀 甀爀氀猀 眀椀琀栀⸀ഀഀ
   * @returns {RegExp|ng.$compileProvider} Current RegExp if called without value or self for਍   ⨀    挀栀愀椀渀椀渀最 漀琀栀攀爀眀椀猀攀⸀ഀഀ
   */਍  琀栀椀猀⸀愀䠀爀攀昀匀愀渀椀琀椀稀愀琀椀漀渀圀栀椀琀攀氀椀猀琀 㴀 昀甀渀挀琀椀漀渀⠀爀攀最攀砀瀀⤀ 笀ഀഀ
    if (isDefined(regexp)) {਍      愀䠀爀攀昀匀愀渀椀琀椀稀愀琀椀漀渀圀栀椀琀攀氀椀猀琀 㴀 爀攀最攀砀瀀㬀ഀഀ
      return this;਍    紀ഀഀ
    return aHrefSanitizationWhitelist;਍  紀㬀ഀഀ
਍ഀഀ
  /**਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Retrieves or overrides the default regular expression that is used for whitelisting of safe਍   ⨀ 甀爀氀猀 搀甀爀椀渀最 椀洀最嬀猀爀挀崀 猀愀渀椀琀椀稀愀琀椀漀渀⸀ഀഀ
   *਍   ⨀ 吀栀攀 猀愀渀椀琀椀稀愀琀椀漀渀 椀猀 愀 猀攀挀甀爀椀琀礀 洀攀愀猀甀爀攀 愀椀洀攀搀 愀琀 瀀爀攀瘀攀渀琀 堀匀匀 愀琀琀愀挀欀猀 瘀椀愀 栀琀洀氀 氀椀渀欀猀⸀ഀഀ
   *਍   ⨀ 䄀渀礀 甀爀氀 愀戀漀甀琀 琀漀 戀攀 愀猀猀椀最渀攀搀 琀漀 椀洀最嬀猀爀挀崀 瘀椀愀 搀愀琀愀ⴀ戀椀渀搀椀渀最 椀猀 昀椀爀猀琀 渀漀爀洀愀氀椀稀攀搀 愀渀搀 琀甀爀渀攀搀 椀渀琀漀ഀഀ
   * an absolute url. Afterwards, the url is matched against the `imgSrcSanitizationWhitelist`਍   ⨀ 爀攀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀⸀ 䤀昀 愀 洀愀琀挀栀 椀猀 昀漀甀渀搀Ⰰ 琀栀攀 漀爀椀最椀渀愀氀 甀爀氀 椀猀 眀爀椀琀琀攀渀 椀渀琀漀 琀栀攀 搀漀洀⸀ 伀琀栀攀爀眀椀猀攀Ⰰഀഀ
   * the absolute url is prefixed with `'unsafe:'` string and only then is it written into the DOM.਍   ⨀ഀഀ
   * @param {RegExp=} regexp New regexp to whitelist urls with.਍   ⨀ 䀀爀攀琀甀爀渀猀 笀刀攀最䔀砀瀀簀渀最⸀␀挀漀洀瀀椀氀攀倀爀漀瘀椀搀攀爀紀 䌀甀爀爀攀渀琀 刀攀最䔀砀瀀 椀昀 挀愀氀氀攀搀 眀椀琀栀漀甀琀 瘀愀氀甀攀 漀爀 猀攀氀昀 昀漀爀ഀഀ
   *    chaining otherwise.਍   ⨀⼀ഀഀ
  this.imgSrcSanitizationWhitelist = function(regexp) {਍    椀昀 ⠀椀猀䐀攀昀椀渀攀搀⠀爀攀最攀砀瀀⤀⤀ 笀ഀഀ
      imgSrcSanitizationWhitelist = regexp;਍      爀攀琀甀爀渀 琀栀椀猀㬀ഀഀ
    }਍    爀攀琀甀爀渀 椀洀最匀爀挀匀愀渀椀琀椀稀愀琀椀漀渀圀栀椀琀攀氀椀猀琀㬀ഀഀ
  };਍ഀഀ
  this.$get = function() {਍    爀攀琀甀爀渀 昀甀渀挀琀椀漀渀 猀愀渀椀琀椀稀攀唀爀椀⠀甀爀椀Ⰰ 椀猀䤀洀愀最攀⤀ 笀ഀഀ
      var regex = isImage ? imgSrcSanitizationWhitelist : aHrefSanitizationWhitelist;਍      瘀愀爀 渀漀爀洀愀氀椀稀攀搀嘀愀氀㬀ഀഀ
      // NOTE: urlResolve() doesn't support IE < 8 so we don't sanitize for that case.਍      椀昀 ⠀℀洀猀椀攀 簀簀 洀猀椀攀 㸀㴀 㠀 ⤀ 笀ഀഀ
        normalizedVal = urlResolve(uri).href;਍        椀昀 ⠀渀漀爀洀愀氀椀稀攀搀嘀愀氀 ℀㴀㴀 ✀✀ ☀☀ ℀渀漀爀洀愀氀椀稀攀搀嘀愀氀⸀洀愀琀挀栀⠀爀攀最攀砀⤀⤀ 笀ഀഀ
          return 'unsafe:'+normalizedVal;਍        紀ഀഀ
      }਍      爀攀琀甀爀渀 甀爀椀㬀ഀഀ
    };਍  紀㬀ഀഀ
}਍ഀഀ
var $sceMinErr = minErr('$sce');਍ഀഀ
var SCE_CONTEXTS = {਍  䠀吀䴀䰀㨀 ✀栀琀洀氀✀Ⰰഀഀ
  CSS: 'css',਍  唀刀䰀㨀 ✀甀爀氀✀Ⰰഀഀ
  // RESOURCE_URL is a subtype of URL used in contexts where a privileged resource is sourced from a਍  ⼀⼀ 甀爀氀⸀  ⠀攀⸀最⸀ 渀最ⴀ椀渀挀氀甀搀攀Ⰰ 猀挀爀椀瀀琀 猀爀挀Ⰰ 琀攀洀瀀氀愀琀攀唀爀氀⤀ഀഀ
  RESOURCE_URL: 'resourceUrl',਍  䨀匀㨀 ✀樀猀✀ഀഀ
};਍ഀഀ
// Helper functions follow.਍ഀഀ
// Copied from:਍⼀⼀ 栀琀琀瀀㨀⼀⼀搀漀挀猀⸀挀氀漀猀甀爀攀ⴀ氀椀戀爀愀爀礀⸀最漀漀最氀攀挀漀搀攀⸀挀漀洀⼀最椀琀⼀挀氀漀猀甀爀攀开最漀漀最开猀琀爀椀渀最开猀琀爀椀渀最⸀樀猀⸀猀漀甀爀挀攀⸀栀琀洀氀⌀氀椀渀攀㤀㘀㈀ഀഀ
// Prereq: s is a string.਍昀甀渀挀琀椀漀渀 攀猀挀愀瀀攀䘀漀爀刀攀最攀砀瀀⠀猀⤀ 笀ഀഀ
  return s.replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g, '\\$1').਍           爀攀瀀氀愀挀攀⠀⼀尀砀　㠀⼀最Ⰰ ✀尀尀砀　㠀✀⤀㬀ഀഀ
}਍ഀഀ
਍昀甀渀挀琀椀漀渀 愀搀樀甀猀琀䴀愀琀挀栀攀爀⠀洀愀琀挀栀攀爀⤀ 笀ഀഀ
  if (matcher === 'self') {਍    爀攀琀甀爀渀 洀愀琀挀栀攀爀㬀ഀഀ
  } else if (isString(matcher)) {਍    ⼀⼀ 匀琀爀椀渀最猀 洀愀琀挀栀 攀砀愀挀琀氀礀 攀砀挀攀瀀琀 昀漀爀 ㈀ 眀椀氀搀挀愀爀搀猀 ⴀ ✀⨀✀ 愀渀搀 ✀⨀⨀✀⸀ഀഀ
    // '*' matches any character except those from the set ':/.?&'.਍    ⼀⼀ ✀⨀⨀✀ 洀愀琀挀栀攀猀 愀渀礀 挀栀愀爀愀挀琀攀爀 ⠀氀椀欀攀 ⸀⨀ 椀渀 愀 刀攀最䔀砀瀀⤀⸀ഀഀ
    // More than 2 *'s raises an error as it's ill defined.਍    椀昀 ⠀洀愀琀挀栀攀爀⸀椀渀搀攀砀伀昀⠀✀⨀⨀⨀✀⤀ 㸀 ⴀ㄀⤀ 笀ഀഀ
      throw $sceMinErr('iwcard',਍          ✀䤀氀氀攀最愀氀 猀攀焀甀攀渀挀攀 ⨀⨀⨀ 椀渀 猀琀爀椀渀最 洀愀琀挀栀攀爀⸀  匀琀爀椀渀最㨀 笀　紀✀Ⰰ 洀愀琀挀栀攀爀⤀㬀ഀഀ
    }਍    洀愀琀挀栀攀爀 㴀 攀猀挀愀瀀攀䘀漀爀刀攀最攀砀瀀⠀洀愀琀挀栀攀爀⤀⸀ഀഀ
                  replace('\\*\\*', '.*').਍                  爀攀瀀氀愀挀攀⠀✀尀尀⨀✀Ⰰ ✀嬀帀㨀⼀⸀㼀☀㬀崀⨀✀⤀㬀ഀഀ
    return new RegExp('^' + matcher + '$');਍  紀 攀氀猀攀 椀昀 ⠀椀猀刀攀最䔀砀瀀⠀洀愀琀挀栀攀爀⤀⤀ 笀ഀഀ
    // The only other type of matcher allowed is a Regexp.਍    ⼀⼀ 䴀愀琀挀栀 攀渀琀椀爀攀 唀刀䰀 ⼀ 搀椀猀愀氀氀漀眀 瀀愀爀琀椀愀氀 洀愀琀挀栀攀猀⸀ഀഀ
    // Flags are reset (i.e. no global, ignoreCase or multiline)਍    爀攀琀甀爀渀 渀攀眀 刀攀最䔀砀瀀⠀✀帀✀ ⬀ 洀愀琀挀栀攀爀⸀猀漀甀爀挀攀 ⬀ ✀␀✀⤀㬀ഀഀ
  } else {਍    琀栀爀漀眀 ␀猀挀攀䴀椀渀䔀爀爀⠀✀椀洀愀琀挀栀攀爀✀Ⰰഀഀ
        'Matchers may only be "self", string patterns or RegExp objects');਍  紀ഀഀ
}਍ഀഀ
਍昀甀渀挀琀椀漀渀 愀搀樀甀猀琀䴀愀琀挀栀攀爀猀⠀洀愀琀挀栀攀爀猀⤀ 笀ഀഀ
  var adjustedMatchers = [];਍  椀昀 ⠀椀猀䐀攀昀椀渀攀搀⠀洀愀琀挀栀攀爀猀⤀⤀ 笀ഀഀ
    forEach(matchers, function(matcher) {਍      愀搀樀甀猀琀攀搀䴀愀琀挀栀攀爀猀⸀瀀甀猀栀⠀愀搀樀甀猀琀䴀愀琀挀栀攀爀⠀洀愀琀挀栀攀爀⤀⤀㬀ഀഀ
    });਍  紀ഀഀ
  return adjustedMatchers;਍紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 猀攀爀瘀椀挀攀ഀഀ
 * @name ng.$sceDelegate਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 *਍ ⨀ 怀␀猀挀攀䐀攀氀攀最愀琀攀怀 椀猀 愀 猀攀爀瘀椀挀攀 琀栀愀琀 椀猀 甀猀攀搀 戀礀 琀栀攀 怀␀猀挀攀怀 猀攀爀瘀椀挀攀 琀漀 瀀爀漀瘀椀搀攀 笀䀀氀椀渀欀 渀最⸀␀猀挀攀 匀琀爀椀挀琀ഀഀ
 * Contextual Escaping (SCE)} services to AngularJS.਍ ⨀ഀഀ
 * Typically, you would configure or override the {@link ng.$sceDelegate $sceDelegate} instead of਍ ⨀ 琀栀攀 怀␀猀挀攀怀 猀攀爀瘀椀挀攀 琀漀 挀甀猀琀漀洀椀稀攀 琀栀攀 眀愀礀 匀琀爀椀挀琀 䌀漀渀琀攀砀琀甀愀氀 䔀猀挀愀瀀椀渀最 眀漀爀欀猀 椀渀 䄀渀最甀氀愀爀䨀匀⸀  吀栀椀猀 椀猀ഀഀ
 * because, while the `$sce` provides numerous shorthand methods, etc., you really only need to਍ ⨀ 漀瘀攀爀爀椀搀攀 ㌀ 挀漀爀攀 昀甀渀挀琀椀漀渀猀 ⠀怀琀爀甀猀琀䄀猀怀Ⰰ 怀最攀琀吀爀甀猀琀攀搀怀 愀渀搀 怀瘀愀氀甀攀伀昀怀⤀ 琀漀 爀攀瀀氀愀挀攀 琀栀攀 眀愀礀 琀栀椀渀最猀ഀഀ
 * work because `$sce` delegates to `$sceDelegate` for these operations.਍ ⨀ഀഀ
 * Refer {@link ng.$sceDelegateProvider $sceDelegateProvider} to configure this service.਍ ⨀ഀഀ
 * The default instance of `$sceDelegate` should work out of the box with little pain.  While you਍ ⨀ 挀愀渀 漀瘀攀爀爀椀搀攀 椀琀 挀漀洀瀀氀攀琀攀氀礀 琀漀 挀栀愀渀最攀 琀栀攀 戀攀栀愀瘀椀漀爀 漀昀 怀␀猀挀攀怀Ⰰ 琀栀攀 挀漀洀洀漀渀 挀愀猀攀 眀漀甀氀搀ഀഀ
 * involve configuring the {@link ng.$sceDelegateProvider $sceDelegateProvider} instead by setting਍ ⨀ 礀漀甀爀 漀眀渀 眀栀椀琀攀氀椀猀琀猀 愀渀搀 戀氀愀挀欀氀椀猀琀猀 昀漀爀 琀爀甀猀琀椀渀最 唀刀䰀猀 甀猀攀搀 昀漀爀 氀漀愀搀椀渀最 䄀渀最甀氀愀爀䨀匀 爀攀猀漀甀爀挀攀猀 猀甀挀栀 愀猀ഀഀ
 * templates.  Refer {@link ng.$sceDelegateProvider#methods_resourceUrlWhitelist਍ ⨀ ␀猀挀攀䐀攀氀攀最愀琀攀倀爀漀瘀椀搀攀爀⸀爀攀猀漀甀爀挀攀唀爀氀圀栀椀琀攀氀椀猀琀紀 愀渀搀 笀䀀氀椀渀欀ഀഀ
 * ng.$sceDelegateProvider#methods_resourceUrlBlacklist $sceDelegateProvider.resourceUrlBlacklist}਍ ⨀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc object਍ ⨀ 䀀渀愀洀攀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀倀爀漀瘀椀搀攀爀ഀഀ
 * @description਍ ⨀ഀഀ
 * The `$sceDelegateProvider` provider allows developers to configure the {@link ng.$sceDelegate਍ ⨀ ␀猀挀攀䐀攀氀攀最愀琀攀紀 猀攀爀瘀椀挀攀⸀  吀栀椀猀 愀氀氀漀眀猀 漀渀攀 琀漀 最攀琀⼀猀攀琀 琀栀攀 眀栀椀琀攀氀椀猀琀猀 愀渀搀 戀氀愀挀欀氀椀猀琀猀 甀猀攀搀 琀漀 攀渀猀甀爀攀ഀഀ
 * that the URLs used for sourcing Angular templates are safe.  Refer {@link਍ ⨀ 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀倀爀漀瘀椀搀攀爀⌀洀攀琀栀漀搀猀开爀攀猀漀甀爀挀攀唀爀氀圀栀椀琀攀氀椀猀琀 ␀猀挀攀䐀攀氀攀最愀琀攀倀爀漀瘀椀搀攀爀⸀爀攀猀漀甀爀挀攀唀爀氀圀栀椀琀攀氀椀猀琀紀 愀渀搀ഀഀ
 * {@link ng.$sceDelegateProvider#methods_resourceUrlBlacklist $sceDelegateProvider.resourceUrlBlacklist}਍ ⨀ഀഀ
 * For the general details about this service in Angular, read the main page for {@link ng.$sce਍ ⨀ 匀琀爀椀挀琀 䌀漀渀琀攀砀琀甀愀氀 䔀猀挀愀瀀椀渀最 ⠀匀䌀䔀⤀紀⸀ഀഀ
 *਍ ⨀ ⨀⨀䔀砀愀洀瀀氀攀⨀⨀㨀  䌀漀渀猀椀搀攀爀 琀栀攀 昀漀氀氀漀眀椀渀最 挀愀猀攀⸀ 㰀愀 渀愀洀攀㴀∀攀砀愀洀瀀氀攀∀㸀㰀⼀愀㸀ഀഀ
 *਍ ⨀ ⴀ 礀漀甀爀 愀瀀瀀 椀猀 栀漀猀琀攀搀 愀琀 甀爀氀 怀栀琀琀瀀㨀⼀⼀洀礀愀瀀瀀⸀攀砀愀洀瀀氀攀⸀挀漀洀⼀怀ഀഀ
 * - but some of your templates are hosted on other domains you control such as਍ ⨀   怀栀琀琀瀀㨀⼀⼀猀爀瘀　㄀⸀愀猀猀攀琀猀⸀攀砀愀洀瀀氀攀⸀挀漀洀⼀怀Ⰰﴀ﷿⃿怀栀琀琀瀀㨀⼀⼀猀爀瘀　㈀⸀愀猀猀攀琀猀⸀攀砀愀洀瀀氀攀⸀挀漀洀⼀怀Ⰰ 攀琀挀⸀ഀഀ
 * - and you have an open redirect at `http://myapp.example.com/clickThru?...`.਍ ⨀ഀഀ
 * Here is what a secure configuration for this scenario might look like:਍ ⨀ഀഀ
 * <pre class="prettyprint">਍ ⨀    愀渀最甀氀愀爀⸀洀漀搀甀氀攀⠀✀洀礀䄀瀀瀀✀Ⰰ 嬀崀⤀⸀挀漀渀昀椀最⠀昀甀渀挀琀椀漀渀⠀␀猀挀攀䐀攀氀攀最愀琀攀倀爀漀瘀椀搀攀爀⤀ 笀ഀഀ
 *      $sceDelegateProvider.resourceUrlWhitelist([਍ ⨀        ⼀⼀ 䄀氀氀漀眀 猀愀洀攀 漀爀椀最椀渀 爀攀猀漀甀爀挀攀 氀漀愀搀猀⸀ഀഀ
 *        'self',਍ ⨀        ⼀⼀ 䄀氀氀漀眀 氀漀愀搀椀渀最 昀爀漀洀 漀甀爀 愀猀猀攀琀猀 搀漀洀愀椀渀⸀  一漀琀椀挀攀 琀栀攀 搀椀昀昀攀爀攀渀挀攀 戀攀琀眀攀攀渀 ⨀ 愀渀搀 ⨀⨀⸀ഀഀ
 *        'http://srv*.assets.example.com/**']);਍ ⨀ഀഀ
 *      // The blacklist overrides the whitelist so the open redirect here is blocked.਍ ⨀      ␀猀挀攀䐀攀氀攀最愀琀攀倀爀漀瘀椀搀攀爀⸀爀攀猀漀甀爀挀攀唀爀氀䈀氀愀挀欀氀椀猀琀⠀嬀ഀഀ
 *        'http://myapp.example.com/clickThru**']);਍ ⨀      紀⤀㬀ഀഀ
 * </pre>਍ ⨀⼀ഀഀ
਍昀甀渀挀琀椀漀渀 ␀匀挀攀䐀攀氀攀最愀琀攀倀爀漀瘀椀搀攀爀⠀⤀ 笀ഀഀ
  this.SCE_CONTEXTS = SCE_CONTEXTS;਍ഀഀ
  // Resource URLs can also be trusted by policy.਍  瘀愀爀 爀攀猀漀甀爀挀攀唀爀氀圀栀椀琀攀氀椀猀琀 㴀 嬀✀猀攀氀昀✀崀Ⰰഀഀ
      resourceUrlBlacklist = [];਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
   * @name ng.sceDelegateProvider#resourceUrlWhitelist਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀倀爀漀瘀椀搀攀爀ഀഀ
   * @function਍   ⨀ഀഀ
   * @param {Array=} whitelist When provided, replaces the resourceUrlWhitelist with the value਍   ⨀     瀀爀漀瘀椀搀攀搀⸀  吀栀椀猀 洀甀猀琀 戀攀 愀渀 愀爀爀愀礀 漀爀 渀甀氀氀⸀  䄀 猀渀愀瀀猀栀漀琀 漀昀 琀栀椀猀 愀爀爀愀礀 椀猀 甀猀攀搀 猀漀 昀甀爀琀栀攀爀ഀഀ
   *     changes to the array are ignored.਍   ⨀ഀഀ
   *     Follow {@link ng.$sce#resourceUrlPatternItem this link} for a description of the items਍   ⨀     愀氀氀漀眀攀搀 椀渀 琀栀椀猀 愀爀爀愀礀⸀ഀഀ
   *਍   ⨀     一漀琀攀㨀 ⨀⨀愀渀 攀洀瀀琀礀 眀栀椀琀攀氀椀猀琀 愀爀爀愀礀 眀椀氀氀 戀氀漀挀欀 愀氀氀 唀刀䰀猀⨀⨀℀ഀഀ
   *਍   ⨀ 䀀爀攀琀甀爀渀 笀䄀爀爀愀礀紀 琀栀攀 挀甀爀爀攀渀琀氀礀 猀攀琀 眀栀椀琀攀氀椀猀琀 愀爀爀愀礀⸀ഀഀ
   *਍   ⨀ 吀栀攀 ⨀⨀搀攀昀愀甀氀琀 瘀愀氀甀攀⨀⨀ 眀栀攀渀 渀漀 眀栀椀琀攀氀椀猀琀 栀愀猀 戀攀攀渀 攀砀瀀氀椀挀椀琀氀礀 猀攀琀 椀猀 怀嬀✀猀攀氀昀✀崀怀 愀氀氀漀眀椀渀最 漀渀氀礀ഀഀ
   * same origin resource requests.਍   ⨀ഀഀ
   * @description਍   ⨀ 匀攀琀猀⼀䜀攀琀猀 琀栀攀 眀栀椀琀攀氀椀猀琀 漀昀 琀爀甀猀琀攀搀 爀攀猀漀甀爀挀攀 唀刀䰀猀⸀ഀഀ
   */਍  琀栀椀猀⸀爀攀猀漀甀爀挀攀唀爀氀圀栀椀琀攀氀椀猀琀 㴀 昀甀渀挀琀椀漀渀 ⠀瘀愀氀甀攀⤀ 笀ഀഀ
    if (arguments.length) {਍      爀攀猀漀甀爀挀攀唀爀氀圀栀椀琀攀氀椀猀琀 㴀 愀搀樀甀猀琀䴀愀琀挀栀攀爀猀⠀瘀愀氀甀攀⤀㬀ഀഀ
    }਍    爀攀琀甀爀渀 爀攀猀漀甀爀挀攀唀爀氀圀栀椀琀攀氀椀猀琀㬀ഀഀ
  };਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
   * @name ng.sceDelegateProvider#resourceUrlBlacklist਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀倀爀漀瘀椀搀攀爀ഀഀ
   * @function਍   ⨀ഀഀ
   * @param {Array=} blacklist When provided, replaces the resourceUrlBlacklist with the value਍   ⨀     瀀爀漀瘀椀搀攀搀⸀  吀栀椀猀 洀甀猀琀 戀攀 愀渀 愀爀爀愀礀 漀爀 渀甀氀氀⸀  䄀 猀渀愀瀀猀栀漀琀 漀昀 琀栀椀猀 愀爀爀愀礀 椀猀 甀猀攀搀 猀漀 昀甀爀琀栀攀爀ഀഀ
   *     changes to the array are ignored.਍   ⨀ഀഀ
   *     Follow {@link ng.$sce#resourceUrlPatternItem this link} for a description of the items਍   ⨀     愀氀氀漀眀攀搀 椀渀 琀栀椀猀 愀爀爀愀礀⸀ഀഀ
   *਍   ⨀     吀栀攀 琀礀瀀椀挀愀氀 甀猀愀最攀 昀漀爀 琀栀攀 戀氀愀挀欀氀椀猀琀 椀猀 琀漀 ⨀⨀戀氀漀挀欀ഀഀ
   *     [open redirects](http://cwe.mitre.org/data/definitions/601.html)** served by your domain as਍   ⨀     琀栀攀猀攀 眀漀甀氀搀 漀琀栀攀爀眀椀猀攀 戀攀 琀爀甀猀琀攀搀 戀甀琀 愀挀琀甀愀氀氀礀 爀攀琀甀爀渀 挀漀渀琀攀渀琀 昀爀漀洀 琀栀攀 爀攀搀椀爀攀挀琀攀搀 搀漀洀愀椀渀⸀ഀഀ
   *਍   ⨀     䘀椀渀愀氀氀礀Ⰰ ⨀⨀琀栀攀 戀氀愀挀欀氀椀猀琀 漀瘀攀爀爀椀搀攀猀 琀栀攀 眀栀椀琀攀氀椀猀琀⨀⨀ 愀渀搀 栀愀猀 琀栀攀 昀椀渀愀氀 猀愀礀⸀ഀഀ
   *਍   ⨀ 䀀爀攀琀甀爀渀 笀䄀爀爀愀礀紀 琀栀攀 挀甀爀爀攀渀琀氀礀 猀攀琀 戀氀愀挀欀氀椀猀琀 愀爀爀愀礀⸀ഀഀ
   *਍   ⨀ 吀栀攀 ⨀⨀搀攀昀愀甀氀琀 瘀愀氀甀攀⨀⨀ 眀栀攀渀 渀漀 眀栀椀琀攀氀椀猀琀 栀愀猀 戀攀攀渀 攀砀瀀氀椀挀椀琀氀礀 猀攀琀 椀猀 琀栀攀 攀洀瀀琀礀 愀爀爀愀礀 ⠀椀⸀攀⸀ 琀栀攀爀攀ഀഀ
   * is no blacklist.)਍   ⨀ഀഀ
   * @description਍   ⨀ 匀攀琀猀⼀䜀攀琀猀 琀栀攀 戀氀愀挀欀氀椀猀琀 漀昀 琀爀甀猀琀攀搀 爀攀猀漀甀爀挀攀 唀刀䰀猀⸀ഀഀ
   */਍ഀഀ
  this.resourceUrlBlacklist = function (value) {਍    椀昀 ⠀愀爀最甀洀攀渀琀猀⸀氀攀渀最琀栀⤀ 笀ഀഀ
      resourceUrlBlacklist = adjustMatchers(value);਍    紀ഀഀ
    return resourceUrlBlacklist;਍  紀㬀ഀഀ
਍  琀栀椀猀⸀␀最攀琀 㴀 嬀✀␀椀渀樀攀挀琀漀爀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀椀渀樀攀挀琀漀爀⤀ 笀ഀഀ
਍    瘀愀爀 栀琀洀氀匀愀渀椀琀椀稀攀爀 㴀 昀甀渀挀琀椀漀渀 栀琀洀氀匀愀渀椀琀椀稀攀爀⠀栀琀洀氀⤀ 笀ഀഀ
      throw $sceMinErr('unsafe', 'Attempting to use an unsafe value in a safe context.');਍    紀㬀ഀഀ
਍    椀昀 ⠀␀椀渀樀攀挀琀漀爀⸀栀愀猀⠀✀␀猀愀渀椀琀椀稀攀✀⤀⤀ 笀ഀഀ
      htmlSanitizer = $injector.get('$sanitize');਍    紀ഀഀ
਍ഀഀ
    function matchUrl(matcher, parsedUrl) {਍      椀昀 ⠀洀愀琀挀栀攀爀 㴀㴀㴀 ✀猀攀氀昀✀⤀ 笀ഀഀ
        return urlIsSameOrigin(parsedUrl);਍      紀 攀氀猀攀 笀ഀഀ
        // definitely a regex.  See adjustMatchers()਍        爀攀琀甀爀渀 ℀℀洀愀琀挀栀攀爀⸀攀砀攀挀⠀瀀愀爀猀攀搀唀爀氀⸀栀爀攀昀⤀㬀ഀഀ
      }਍    紀ഀഀ
਍    昀甀渀挀琀椀漀渀 椀猀刀攀猀漀甀爀挀攀唀爀氀䄀氀氀漀眀攀搀䈀礀倀漀氀椀挀礀⠀甀爀氀⤀ 笀ഀഀ
      var parsedUrl = urlResolve(url.toString());਍      瘀愀爀 椀Ⰰ 渀Ⰰ 愀氀氀漀眀攀搀 㴀 昀愀氀猀攀㬀ഀഀ
      // Ensure that at least one item from the whitelist allows this url.਍      昀漀爀 ⠀椀 㴀 　Ⰰ 渀 㴀 爀攀猀漀甀爀挀攀唀爀氀圀栀椀琀攀氀椀猀琀⸀氀攀渀最琀栀㬀 椀 㰀 渀㬀 椀⬀⬀⤀ 笀ഀഀ
        if (matchUrl(resourceUrlWhitelist[i], parsedUrl)) {਍          愀氀氀漀眀攀搀 㴀 琀爀甀攀㬀ഀഀ
          break;਍        紀ഀഀ
      }਍      椀昀 ⠀愀氀氀漀眀攀搀⤀ 笀ഀഀ
        // Ensure that no item from the blacklist blocked this url.਍        昀漀爀 ⠀椀 㴀 　Ⰰ 渀 㴀 爀攀猀漀甀爀挀攀唀爀氀䈀氀愀挀欀氀椀猀琀⸀氀攀渀最琀栀㬀 椀 㰀 渀㬀 椀⬀⬀⤀ 笀ഀഀ
          if (matchUrl(resourceUrlBlacklist[i], parsedUrl)) {਍            愀氀氀漀眀攀搀 㴀 昀愀氀猀攀㬀ഀഀ
            break;਍          紀ഀഀ
        }਍      紀ഀഀ
      return allowed;਍    紀ഀഀ
਍    昀甀渀挀琀椀漀渀 最攀渀攀爀愀琀攀䠀漀氀搀攀爀吀礀瀀攀⠀䈀愀猀攀⤀ 笀ഀഀ
      var holderType = function TrustedValueHolderType(trustedValue) {਍        琀栀椀猀⸀␀␀甀渀眀爀愀瀀吀爀甀猀琀攀搀嘀愀氀甀攀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          return trustedValue;਍        紀㬀ഀഀ
      };਍      椀昀 ⠀䈀愀猀攀⤀ 笀ഀഀ
        holderType.prototype = new Base();਍      紀ഀഀ
      holderType.prototype.valueOf = function sceValueOf() {਍        爀攀琀甀爀渀 琀栀椀猀⸀␀␀甀渀眀爀愀瀀吀爀甀猀琀攀搀嘀愀氀甀攀⠀⤀㬀ഀഀ
      };਍      栀漀氀搀攀爀吀礀瀀攀⸀瀀爀漀琀漀琀礀瀀攀⸀琀漀匀琀爀椀渀最 㴀 昀甀渀挀琀椀漀渀 猀挀攀吀漀匀琀爀椀渀最⠀⤀ 笀ഀഀ
        return this.$$unwrapTrustedValue().toString();਍      紀㬀ഀഀ
      return holderType;਍    紀ഀഀ
਍    瘀愀爀 琀爀甀猀琀攀搀嘀愀氀甀攀䠀漀氀搀攀爀䈀愀猀攀 㴀 最攀渀攀爀愀琀攀䠀漀氀搀攀爀吀礀瀀攀⠀⤀Ⰰഀഀ
        byType = {};਍ഀഀ
    byType[SCE_CONTEXTS.HTML] = generateHolderType(trustedValueHolderBase);਍    戀礀吀礀瀀攀嬀匀䌀䔀开䌀伀一吀䔀堀吀匀⸀䌀匀匀崀 㴀 最攀渀攀爀愀琀攀䠀漀氀搀攀爀吀礀瀀攀⠀琀爀甀猀琀攀搀嘀愀氀甀攀䠀漀氀搀攀爀䈀愀猀攀⤀㬀ഀഀ
    byType[SCE_CONTEXTS.URL] = generateHolderType(trustedValueHolderBase);਍    戀礀吀礀瀀攀嬀匀䌀䔀开䌀伀一吀䔀堀吀匀⸀䨀匀崀 㴀 最攀渀攀爀愀琀攀䠀漀氀搀攀爀吀礀瀀攀⠀琀爀甀猀琀攀搀嘀愀氀甀攀䠀漀氀搀攀爀䈀愀猀攀⤀㬀ഀഀ
    byType[SCE_CONTEXTS.RESOURCE_URL] = generateHolderType(byType[SCE_CONTEXTS.URL]);਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$sceDelegate#trustAs਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Returns an object that is trusted by angular for use in specified strict਍     ⨀ 挀漀渀琀攀砀琀甀愀氀 攀猀挀愀瀀椀渀最 挀漀渀琀攀砀琀猀 ⠀猀甀挀栀 愀猀 渀最ⴀ栀琀洀氀ⴀ戀椀渀搀ⴀ甀渀猀愀昀攀Ⰰ 渀最ⴀ椀渀挀氀甀搀攀Ⰰ 愀渀礀 猀爀挀ഀഀ
     * attribute interpolation, any dom event binding attribute interpolation਍     ⨀ 猀甀挀栀 愀猀 昀漀爀 漀渀挀氀椀挀欀Ⰰ  攀琀挀⸀⤀ 琀栀愀琀 甀猀攀猀 琀栀攀 瀀爀漀瘀椀搀攀搀 瘀愀氀甀攀⸀ഀഀ
     * See {@link ng.$sce $sce} for enabling strict contextual escaping.਍     ⨀ഀഀ
     * @param {string} type The kind of context in which this value is safe for use.  e.g. url,਍     ⨀   爀攀猀漀甀爀挀攀唀爀氀Ⰰ 栀琀洀氀Ⰰ 樀猀 愀渀搀 挀猀猀⸀ഀഀ
     * @param {*} value The value that that should be considered trusted/safe.਍     ⨀ 䀀爀攀琀甀爀渀猀 笀⨀紀 䄀 瘀愀氀甀攀 琀栀愀琀 挀愀渀 戀攀 甀猀攀搀 琀漀 猀琀愀渀搀 椀渀 昀漀爀 琀栀攀 瀀爀漀瘀椀搀攀搀 怀瘀愀氀甀攀怀 椀渀 瀀氀愀挀攀猀ഀഀ
     * where Angular expects a $sce.trustAs() return value.਍     ⨀⼀ഀഀ
    function trustAs(type, trustedValue) {਍      瘀愀爀 䌀漀渀猀琀爀甀挀琀漀爀 㴀 ⠀戀礀吀礀瀀攀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀琀礀瀀攀⤀ 㼀 戀礀吀礀瀀攀嬀琀礀瀀攀崀 㨀 渀甀氀氀⤀㬀ഀഀ
      if (!Constructor) {਍        琀栀爀漀眀 ␀猀挀攀䴀椀渀䔀爀爀⠀✀椀挀漀渀琀攀砀琀✀Ⰰഀഀ
            'Attempted to trust a value in invalid context. Context: {0}; Value: {1}',਍            琀礀瀀攀Ⰰ 琀爀甀猀琀攀搀嘀愀氀甀攀⤀㬀ഀഀ
      }਍      椀昀 ⠀琀爀甀猀琀攀搀嘀愀氀甀攀 㴀㴀㴀 渀甀氀氀 簀簀 琀爀甀猀琀攀搀嘀愀氀甀攀 㴀㴀㴀 甀渀搀攀昀椀渀攀搀 簀簀 琀爀甀猀琀攀搀嘀愀氀甀攀 㴀㴀㴀 ✀✀⤀ 笀ഀഀ
        return trustedValue;਍      紀ഀഀ
      // All the current contexts in SCE_CONTEXTS happen to be strings.  In order to avoid trusting਍      ⼀⼀ 洀甀琀愀戀氀攀 漀戀樀攀挀琀猀Ⰰ 眀攀 攀渀猀甀爀攀 栀攀爀攀 琀栀愀琀 琀栀攀 瘀愀氀甀攀 瀀愀猀猀攀搀 椀渀 椀猀 愀挀琀甀愀氀氀礀 愀 猀琀爀椀渀最⸀ഀഀ
      if (typeof trustedValue !== 'string') {਍        琀栀爀漀眀 ␀猀挀攀䴀椀渀䔀爀爀⠀✀椀琀礀瀀攀✀Ⰰഀഀ
            'Attempted to trust a non-string value in a content requiring a string: Context: {0}',਍            琀礀瀀攀⤀㬀ഀഀ
      }਍      爀攀琀甀爀渀 渀攀眀 䌀漀渀猀琀爀甀挀琀漀爀⠀琀爀甀猀琀攀搀嘀愀氀甀攀⤀㬀ഀഀ
    }਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$sceDelegate#valueOf਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * If the passed parameter had been returned by a prior call to {@link ng.$sceDelegate#methods_trustAs਍     ⨀ 怀␀猀挀攀䐀攀氀攀最愀琀攀⸀琀爀甀猀琀䄀猀怀紀Ⰰ 爀攀琀甀爀渀猀 琀栀攀 瘀愀氀甀攀 琀栀愀琀 栀愀搀 戀攀攀渀 瀀愀猀猀攀搀 琀漀 笀䀀氀椀渀欀ഀഀ
     * ng.$sceDelegate#methods_trustAs `$sceDelegate.trustAs`}.਍     ⨀ഀഀ
     * If the passed parameter is not a value that had been returned by {@link਍     ⨀ 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀 怀␀猀挀攀䐀攀氀攀最愀琀攀⸀琀爀甀猀琀䄀猀怀紀Ⰰ 爀攀琀甀爀渀猀 椀琀 愀猀ⴀ椀猀⸀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀⨀紀 瘀愀氀甀攀 吀栀攀 爀攀猀甀氀琀 漀昀 愀 瀀爀椀漀爀 笀䀀氀椀渀欀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀 怀␀猀挀攀䐀攀氀攀最愀琀攀⸀琀爀甀猀琀䄀猀怀紀ഀഀ
     *      call or anything else.਍     ⨀ 䀀爀攀琀甀爀渀猀 笀⨀紀 吀栀攀 瘀愀氀甀攀 琀栀攀 眀愀猀 漀爀椀最椀渀愀氀氀礀 瀀爀漀瘀椀搀攀搀 琀漀 笀䀀氀椀渀欀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀ഀഀ
     *     `$sceDelegate.trustAs`} if `value` is the result of such a call.  Otherwise, returns਍     ⨀     怀瘀愀氀甀攀怀 甀渀挀栀愀渀最攀搀⸀ഀഀ
     */਍    昀甀渀挀琀椀漀渀 瘀愀氀甀攀伀昀⠀洀愀礀戀攀吀爀甀猀琀攀搀⤀ 笀ഀഀ
      if (maybeTrusted instanceof trustedValueHolderBase) {਍        爀攀琀甀爀渀 洀愀礀戀攀吀爀甀猀琀攀搀⸀␀␀甀渀眀爀愀瀀吀爀甀猀琀攀搀嘀愀氀甀攀⠀⤀㬀ഀഀ
      } else {਍        爀攀琀甀爀渀 洀愀礀戀攀吀爀甀猀琀攀搀㬀ഀഀ
      }਍    紀ഀഀ
਍    ⼀⨀⨀ഀഀ
     * @ngdoc method਍     ⨀ 䀀渀愀洀攀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀⌀最攀琀吀爀甀猀琀攀搀ഀഀ
     * @methodOf ng.$sceDelegate਍     ⨀ഀഀ
     * @description਍     ⨀ 吀愀欀攀猀 琀栀攀 爀攀猀甀氀琀 漀昀 愀 笀䀀氀椀渀欀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀 怀␀猀挀攀䐀攀氀攀最愀琀攀⸀琀爀甀猀琀䄀猀怀紀 挀愀氀氀 愀渀搀ഀഀ
     * returns the originally supplied value if the queried context type is a supertype of the਍     ⨀ 挀爀攀愀琀攀搀 琀礀瀀攀⸀  䤀昀 琀栀椀猀 挀漀渀搀椀琀椀漀渀 椀猀渀✀琀 猀愀琀椀猀昀椀攀搀Ⰰ 琀栀爀漀眀猀 愀渀 攀砀挀攀瀀琀椀漀渀⸀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 琀礀瀀攀 吀栀攀 欀椀渀搀 漀昀 挀漀渀琀攀砀琀 椀渀 眀栀椀挀栀 琀栀椀猀 瘀愀氀甀攀 椀猀 琀漀 戀攀 甀猀攀搀⸀ഀഀ
     * @param {*} maybeTrusted The result of a prior {@link ng.$sceDelegate#methods_trustAs਍     ⨀     怀␀猀挀攀䐀攀氀攀最愀琀攀⸀琀爀甀猀琀䄀猀怀紀 挀愀氀氀⸀ഀഀ
     * @returns {*} The value the was originally provided to {@link ng.$sceDelegate#methods_trustAs਍     ⨀     怀␀猀挀攀䐀攀氀攀最愀琀攀⸀琀爀甀猀琀䄀猀怀紀 椀昀 瘀愀氀椀搀 椀渀 琀栀椀猀 挀漀渀琀攀砀琀⸀  伀琀栀攀爀眀椀猀攀Ⰰ 琀栀爀漀眀猀 愀渀 攀砀挀攀瀀琀椀漀渀⸀ഀഀ
     */਍    昀甀渀挀琀椀漀渀 最攀琀吀爀甀猀琀攀搀⠀琀礀瀀攀Ⰰ 洀愀礀戀攀吀爀甀猀琀攀搀⤀ 笀ഀഀ
      if (maybeTrusted === null || maybeTrusted === undefined || maybeTrusted === '') {਍        爀攀琀甀爀渀 洀愀礀戀攀吀爀甀猀琀攀搀㬀ഀഀ
      }਍      瘀愀爀 挀漀渀猀琀爀甀挀琀漀爀 㴀 ⠀戀礀吀礀瀀攀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀琀礀瀀攀⤀ 㼀 戀礀吀礀瀀攀嬀琀礀瀀攀崀 㨀 渀甀氀氀⤀㬀ഀഀ
      if (constructor && maybeTrusted instanceof constructor) {਍        爀攀琀甀爀渀 洀愀礀戀攀吀爀甀猀琀攀搀⸀␀␀甀渀眀爀愀瀀吀爀甀猀琀攀搀嘀愀氀甀攀⠀⤀㬀ഀഀ
      }਍      ⼀⼀ 䤀昀 眀攀 最攀琀 栀攀爀攀Ⰰ 琀栀攀渀 眀攀 洀愀礀 漀渀氀礀 琀愀欀攀 漀渀攀 漀昀 琀眀漀 愀挀琀椀漀渀猀⸀ഀഀ
      // 1. sanitize the value for the requested type, or਍      ⼀⼀ ㈀⸀ 琀栀爀漀眀 愀渀 攀砀挀攀瀀琀椀漀渀⸀ഀഀ
      if (type === SCE_CONTEXTS.RESOURCE_URL) {਍        椀昀 ⠀椀猀刀攀猀漀甀爀挀攀唀爀氀䄀氀氀漀眀攀搀䈀礀倀漀氀椀挀礀⠀洀愀礀戀攀吀爀甀猀琀攀搀⤀⤀ 笀ഀഀ
          return maybeTrusted;਍        紀 攀氀猀攀 笀ഀഀ
          throw $sceMinErr('insecurl',਍              ✀䈀氀漀挀欀攀搀 氀漀愀搀椀渀最 爀攀猀漀甀爀挀攀 昀爀漀洀 甀爀氀 渀漀琀 愀氀氀漀眀攀搀 戀礀 ␀猀挀攀䐀攀氀攀最愀琀攀 瀀漀氀椀挀礀⸀  唀刀䰀㨀 笀　紀✀Ⰰഀഀ
              maybeTrusted.toString());਍        紀ഀഀ
      } else if (type === SCE_CONTEXTS.HTML) {਍        爀攀琀甀爀渀 栀琀洀氀匀愀渀椀琀椀稀攀爀⠀洀愀礀戀攀吀爀甀猀琀攀搀⤀㬀ഀഀ
      }਍      琀栀爀漀眀 ␀猀挀攀䴀椀渀䔀爀爀⠀✀甀渀猀愀昀攀✀Ⰰ ✀䄀琀琀攀洀瀀琀椀渀最 琀漀 甀猀攀 愀渀 甀渀猀愀昀攀 瘀愀氀甀攀 椀渀 愀 猀愀昀攀 挀漀渀琀攀砀琀⸀✀⤀㬀ഀഀ
    }਍ഀഀ
    return { trustAs: trustAs,਍             最攀琀吀爀甀猀琀攀搀㨀 最攀琀吀爀甀猀琀攀搀Ⰰഀഀ
             valueOf: valueOf };਍  紀崀㬀ഀഀ
}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc object਍ ⨀ 䀀渀愀洀攀 渀最⸀␀猀挀攀倀爀漀瘀椀搀攀爀ഀഀ
 * @description਍ ⨀ഀഀ
 * The $sceProvider provider allows developers to configure the {@link ng.$sce $sce} service.਍ ⨀ ⴀ   攀渀愀戀氀攀⼀搀椀猀愀戀氀攀 匀琀爀椀挀琀 䌀漀渀琀攀砀琀甀愀氀 䔀猀挀愀瀀椀渀最 ⠀匀䌀䔀⤀ 椀渀 愀 洀漀搀甀氀攀ഀഀ
 * -   override the default implementation with a custom delegate਍ ⨀ഀഀ
 * Read more about {@link ng.$sce Strict Contextual Escaping (SCE)}.਍ ⨀⼀ഀഀ
਍⼀⨀ 樀猀栀椀渀琀 洀愀砀氀攀渀㨀 昀愀氀猀攀⨀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc service਍ ⨀ 䀀渀愀洀攀 渀最⸀␀猀挀攀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ഀഀ
 * `$sce` is a service that provides Strict Contextual Escaping services to AngularJS.਍ ⨀ഀഀ
 * # Strict Contextual Escaping਍ ⨀ഀഀ
 * Strict Contextual Escaping (SCE) is a mode in which AngularJS requires bindings in certain਍ ⨀ 挀漀渀琀攀砀琀猀 琀漀 爀攀猀甀氀琀 椀渀 愀 瘀愀氀甀攀 琀栀愀琀 椀猀 洀愀爀欀攀搀 愀猀 猀愀昀攀 琀漀 甀猀攀 昀漀爀 琀栀愀琀 挀漀渀琀攀砀琀⸀  伀渀攀 攀砀愀洀瀀氀攀 漀昀ഀഀ
 * such a context is binding arbitrary html controlled by the user via `ng-bind-html`.  We refer਍ ⨀ 琀漀 琀栀攀猀攀 挀漀渀琀攀砀琀猀 愀猀 瀀爀椀瘀椀氀攀最攀搀 漀爀 匀䌀䔀 挀漀渀琀攀砀琀猀⸀ഀഀ
 *਍ ⨀ 䄀猀 漀昀 瘀攀爀猀椀漀渀 ㄀⸀㈀Ⰰ 䄀渀最甀氀愀爀 猀栀椀瀀猀 眀椀琀栀 匀䌀䔀 攀渀愀戀氀攀搀 戀礀 搀攀昀愀甀氀琀⸀ഀഀ
 *਍ ⨀ 一漀琀攀㨀  圀栀攀渀 攀渀愀戀氀攀搀 ⠀琀栀攀 搀攀昀愀甀氀琀⤀Ⰰ 䤀䔀㠀 椀渀 焀甀椀爀欀猀 洀漀搀攀 椀猀 渀漀琀 猀甀瀀瀀漀爀琀攀搀⸀  䤀渀 琀栀椀猀 洀漀搀攀Ⰰ 䤀䔀㠀 愀氀氀漀眀猀ഀഀ
 * one to execute arbitrary javascript by the use of the expression() syntax.  Refer਍ ⨀ 㰀栀琀琀瀀㨀⼀⼀戀氀漀最猀⸀洀猀搀渀⸀挀漀洀⼀戀⼀椀攀⼀愀爀挀栀椀瘀攀⼀㈀　　㠀⼀㄀　⼀㄀㘀⼀攀渀搀椀渀最ⴀ攀砀瀀爀攀猀猀椀漀渀猀⸀愀猀瀀砀㸀 琀漀 氀攀愀爀渀 洀漀爀攀 愀戀漀甀琀 琀栀攀洀⸀ഀഀ
 * You can ensure your document is in standards mode and not quirks mode by adding `<!doctype html>`਍ ⨀ 琀漀 琀栀攀 琀漀瀀 漀昀 礀漀甀爀 䠀吀䴀䰀 搀漀挀甀洀攀渀琀⸀ഀഀ
 *਍ ⨀ 匀䌀䔀 愀猀猀椀猀琀猀 椀渀 眀爀椀琀椀渀最 挀漀搀攀 椀渀 眀愀礀 琀栀愀琀 ⠀愀⤀ 椀猀 猀攀挀甀爀攀 戀礀 搀攀昀愀甀氀琀 愀渀搀 ⠀戀⤀ 洀愀欀攀猀 愀甀搀椀琀椀渀最 昀漀爀ഀഀ
 * security vulnerabilities such as XSS, clickjacking, etc. a lot easier.਍ ⨀ഀഀ
 * Here's an example of a binding in a privileged context:਍ ⨀ഀഀ
 * <pre class="prettyprint">਍ ⨀     㰀椀渀瀀甀琀 渀最ⴀ洀漀搀攀氀㴀∀甀猀攀爀䠀琀洀氀∀㸀ഀഀ
 *     <div ng-bind-html="userHtml">਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 一漀琀椀挀攀 琀栀愀琀 怀渀最ⴀ戀椀渀搀ⴀ栀琀洀氀怀 椀猀 戀漀甀渀搀 琀漀 怀甀猀攀爀䠀琀洀氀怀 挀漀渀琀爀漀氀氀攀搀 戀礀 琀栀攀 甀猀攀爀⸀  圀椀琀栀 匀䌀䔀ഀഀ
 * disabled, this application allows the user to render arbitrary HTML into the DIV.਍ ⨀ 䤀渀 愀 洀漀爀攀 爀攀愀氀椀猀琀椀挀 攀砀愀洀瀀氀攀Ⰰ 漀渀攀 洀愀礀 戀攀 爀攀渀搀攀爀椀渀最 甀猀攀爀 挀漀洀洀攀渀琀猀Ⰰ 戀氀漀最 愀爀琀椀挀氀攀猀Ⰰ 攀琀挀⸀ 瘀椀愀ഀഀ
 * bindings.  (HTML is just one example of a context where rendering user controlled input creates਍ ⨀ 猀攀挀甀爀椀琀礀 瘀甀氀渀攀爀愀戀椀氀椀琀椀攀猀⸀⤀ഀഀ
 *਍ ⨀ 䘀漀爀 琀栀攀 挀愀猀攀 漀昀 䠀吀䴀䰀Ⰰ 礀漀甀 洀椀最栀琀 甀猀攀 愀 氀椀戀爀愀爀礀Ⰰ 攀椀琀栀攀爀 漀渀 琀栀攀 挀氀椀攀渀琀 猀椀搀攀Ⰰ 漀爀 漀渀 琀栀攀 猀攀爀瘀攀爀 猀椀搀攀Ⰰഀഀ
 * to sanitize unsafe HTML before binding to the value and rendering it in the document.਍ ⨀ഀഀ
 * How would you ensure that every place that used these types of bindings was bound to a value that਍ ⨀ 眀愀猀 猀愀渀椀琀椀稀攀搀 戀礀 礀漀甀爀 氀椀戀爀愀爀礀 ⠀漀爀 爀攀琀甀爀渀攀搀 愀猀 猀愀昀攀 昀漀爀 爀攀渀搀攀爀椀渀最 戀礀 礀漀甀爀 猀攀爀瘀攀爀㼀⤀  䠀漀眀 挀愀渀 礀漀甀ഀഀ
 * ensure that you didn't accidentally delete the line that sanitized the value, or renamed some਍ ⨀ 瀀爀漀瀀攀爀琀椀攀猀⼀昀椀攀氀搀猀 愀渀搀 昀漀爀最漀琀 琀漀 甀瀀搀愀琀攀 琀栀攀 戀椀渀搀椀渀最 琀漀 琀栀攀 猀愀渀椀琀椀稀攀搀 瘀愀氀甀攀㼀ഀഀ
 *਍ ⨀ 吀漀 戀攀 猀攀挀甀爀攀 戀礀 搀攀昀愀甀氀琀Ⰰ 礀漀甀 眀愀渀琀 琀漀 攀渀猀甀爀攀 琀栀愀琀 愀渀礀 猀甀挀栀 戀椀渀搀椀渀最猀 愀爀攀 搀椀猀愀氀氀漀眀攀搀 甀渀氀攀猀猀 礀漀甀 挀愀渀ഀഀ
 * determine that something explicitly says it's safe to use a value for binding in that਍ ⨀ 挀漀渀琀攀砀琀⸀  夀漀甀 挀愀渀 琀栀攀渀 愀甀搀椀琀 礀漀甀爀 挀漀搀攀 ⠀愀 猀椀洀瀀氀攀 最爀攀瀀 眀漀甀氀搀 搀漀⤀ 琀漀 攀渀猀甀爀攀 琀栀愀琀 琀栀椀猀 椀猀 漀渀氀礀 搀漀渀攀ഀഀ
 * for those values that you can easily tell are safe - because they were received from your server,਍ ⨀ 猀愀渀椀琀椀稀攀搀 戀礀 礀漀甀爀 氀椀戀爀愀爀礀Ⰰ 攀琀挀⸀  夀漀甀 挀愀渀 漀爀最愀渀椀稀攀 礀漀甀爀 挀漀搀攀戀愀猀攀 琀漀 栀攀氀瀀 眀椀琀栀 琀栀椀猀 ⴀ 瀀攀爀栀愀瀀猀ഀഀ
 * allowing only the files in a specific directory to do this.  Ensuring that the internal API਍ ⨀ 攀砀瀀漀猀攀搀 戀礀 琀栀愀琀 挀漀搀攀 搀漀攀猀渀✀琀 洀愀爀欀甀瀀 愀爀戀椀琀爀愀爀礀 瘀愀氀甀攀猀 愀猀 猀愀昀攀 琀栀攀渀 戀攀挀漀洀攀猀 愀 洀漀爀攀 洀愀渀愀最攀愀戀氀攀 琀愀猀欀⸀ഀഀ
 *਍ ⨀ 䤀渀 琀栀攀 挀愀猀攀 漀昀 䄀渀最甀氀愀爀䨀匀✀ 匀䌀䔀 猀攀爀瘀椀挀攀Ⰰ 漀渀攀 甀猀攀猀 笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀 ␀猀挀攀⸀琀爀甀猀琀䄀猀紀 ഀഀ
 * (and shorthand methods such as {@link ng.$sce#methods_trustAsHtml $sce.trustAsHtml}, etc.) to਍ ⨀ 漀戀琀愀椀渀 瘀愀氀甀攀猀 琀栀愀琀 眀椀氀氀 戀攀 愀挀挀攀瀀琀攀搀 戀礀 匀䌀䔀 ⼀ 瀀爀椀瘀椀氀攀最攀搀 挀漀渀琀攀砀琀猀⸀ഀഀ
 *਍ ⨀ഀഀ
 * ## How does it work?਍ ⨀ഀഀ
 * In privileged contexts, directives and code will bind to the result of {@link ng.$sce#methods_getTrusted਍ ⨀ ␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀⠀挀漀渀琀攀砀琀Ⰰ 瘀愀氀甀攀⤀紀 爀愀琀栀攀爀 琀栀愀渀 琀漀 琀栀攀 瘀愀氀甀攀 搀椀爀攀挀琀氀礀⸀  䐀椀爀攀挀琀椀瘀攀猀 甀猀攀 笀䀀氀椀渀欀ഀഀ
 * ng.$sce#methods_parse $sce.parseAs} rather than `$parse` to watch attribute bindings, which performs the਍ ⨀ 笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开最攀琀吀爀甀猀琀攀搀 ␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀紀 戀攀栀椀渀搀 琀栀攀 猀挀攀渀攀猀 漀渀 渀漀渀ⴀ挀漀渀猀琀愀渀琀 氀椀琀攀爀愀氀猀⸀ഀഀ
 *਍ ⨀ 䄀猀 愀渀 攀砀愀洀瀀氀攀Ⰰ 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䈀椀渀搀䠀琀洀氀 渀最䈀椀渀搀䠀琀洀氀紀 甀猀攀猀 笀䀀氀椀渀欀ഀഀ
 * ng.$sce#methods_parseAsHtml $sce.parseAsHtml(binding expression)}.  Here's the actual code (slightly਍ ⨀ 猀椀洀瀀氀椀昀椀攀搀⤀㨀ഀഀ
 *਍ ⨀ 㰀瀀爀攀 挀氀愀猀猀㴀∀瀀爀攀琀琀礀瀀爀椀渀琀∀㸀ഀഀ
 *   var ngBindHtmlDirective = ['$sce', function($sce) {਍ ⨀     爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀⤀ 笀ഀഀ
 *       scope.$watch($sce.parseAsHtml(attr.ngBindHtml), function(value) {਍ ⨀         攀氀攀洀攀渀琀⸀栀琀洀氀⠀瘀愀氀甀攀 簀簀 ✀✀⤀㬀ഀഀ
 *       });਍ ⨀     紀㬀ഀഀ
 *   }];਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ ⌀⌀ 䤀洀瀀愀挀琀 漀渀 氀漀愀搀椀渀最 琀攀洀瀀氀愀琀攀猀ഀഀ
 *਍ ⨀ 吀栀椀猀 愀瀀瀀氀椀攀猀 戀漀琀栀 琀漀 琀栀攀 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䤀渀挀氀甀搀攀 怀渀最ⴀ椀渀挀氀甀搀攀怀紀 搀椀爀攀挀琀椀瘀攀 愀猀 眀攀氀氀 愀猀ഀഀ
 * `templateUrl`'s specified by {@link guide/directive directives}.਍ ⨀ഀഀ
 * By default, Angular only loads templates from the same domain and protocol as the application਍ ⨀ 搀漀挀甀洀攀渀琀⸀  吀栀椀猀 椀猀 搀漀渀攀 戀礀 挀愀氀氀椀渀最 笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开最攀琀吀爀甀猀琀攀搀刀攀猀漀甀爀挀攀唀爀氀ഀഀ
 * $sce.getTrustedResourceUrl} on the template URL.  To load templates from other domains and/or਍ ⨀ 瀀爀漀琀漀挀漀氀猀Ⰰ 礀漀甀 洀愀礀 攀椀琀栀攀爀 攀椀琀栀攀爀 笀䀀氀椀渀欀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀倀爀漀瘀椀搀攀爀⌀洀攀琀栀漀搀猀开爀攀猀漀甀爀挀攀唀爀氀圀栀椀琀攀氀椀猀琀 眀栀椀琀攀氀椀猀琀ഀഀ
 * them} or {@link ng.$sce#methods_trustAsResourceUrl wrap it} into a trusted value.਍ ⨀ഀഀ
 * *Please note*:਍ ⨀ 吀栀攀 戀爀漀眀猀攀爀✀猀ഀഀ
 * {@link https://code.google.com/p/browsersec/wiki/Part2#Same-origin_policy_for_XMLHttpRequest਍ ⨀ 匀愀洀攀 伀爀椀最椀渀 倀漀氀椀挀礀紀 愀渀搀 笀䀀氀椀渀欀 栀琀琀瀀㨀⼀⼀眀眀眀⸀眀㌀⸀漀爀最⼀吀刀⼀挀漀爀猀⼀ 䌀爀漀猀猀ⴀ伀爀椀最椀渀 刀攀猀漀甀爀挀攀 匀栀愀爀椀渀最 ⠀䌀伀刀匀⤀紀ഀഀ
 * policy apply in addition to this and may further restrict whether the template is successfully਍ ⨀ 氀漀愀搀攀搀⸀  吀栀椀猀 洀攀愀渀猀 琀栀愀琀 眀椀琀栀漀甀琀 琀栀攀 爀椀最栀琀 䌀伀刀匀 瀀漀氀椀挀礀Ⰰ 氀漀愀搀椀渀最 琀攀洀瀀氀愀琀攀猀 昀爀漀洀 愀 搀椀昀昀攀爀攀渀琀 搀漀洀愀椀渀ഀഀ
 * won't work on all browsers.  Also, loading templates from `file://` URL does not work on some਍ ⨀ 戀爀漀眀猀攀爀猀⸀ഀഀ
 *਍ ⨀ ⌀⌀ 吀栀椀猀 昀攀攀氀猀 氀椀欀攀 琀漀漀 洀甀挀栀 漀瘀攀爀栀攀愀搀 昀漀爀 琀栀攀 搀攀瘀攀氀漀瀀攀爀㼀ഀഀ
 *਍ ⨀ 䤀琀✀猀 椀洀瀀漀爀琀愀渀琀 琀漀 爀攀洀攀洀戀攀爀 琀栀愀琀 匀䌀䔀 漀渀氀礀 愀瀀瀀氀椀攀猀 琀漀 椀渀琀攀爀瀀漀氀愀琀椀漀渀 攀砀瀀爀攀猀猀椀漀渀猀⸀ഀഀ
 *਍ ⨀ 䤀昀 礀漀甀爀 攀砀瀀爀攀猀猀椀漀渀猀 愀爀攀 挀漀渀猀琀愀渀琀 氀椀琀攀爀愀氀猀Ⰰ 琀栀攀礀✀爀攀 愀甀琀漀洀愀琀椀挀愀氀氀礀 琀爀甀猀琀攀搀 愀渀搀 礀漀甀 搀漀渀✀琀 渀攀攀搀 琀漀ഀഀ
 * call `$sce.trustAs` on them.  (e.g.਍ ⨀ 怀㰀搀椀瘀 渀最ⴀ栀琀洀氀ⴀ戀椀渀搀ⴀ甀渀猀愀昀攀㴀∀✀㰀戀㸀椀洀瀀氀椀挀椀琀氀礀 琀爀甀猀琀攀搀㰀⼀戀㸀✀∀㸀㰀⼀搀椀瘀㸀怀⤀ 樀甀猀琀 眀漀爀欀猀⸀ഀഀ
 *਍ ⨀ 䄀搀搀椀琀椀漀渀愀氀氀礀Ⰰ 怀愀嬀栀爀攀昀崀怀 愀渀搀 怀椀洀最嬀猀爀挀崀怀 愀甀琀漀洀愀琀椀挀愀氀氀礀 猀愀渀椀琀椀稀攀 琀栀攀椀爀 唀刀䰀猀 愀渀搀 搀漀 渀漀琀 瀀愀猀猀 琀栀攀洀ഀഀ
 * through {@link ng.$sce#methods_getTrusted $sce.getTrusted}.  SCE doesn't play a role here.਍ ⨀ഀഀ
 * The included {@link ng.$sceDelegate $sceDelegate} comes with sane defaults to allow you to load਍ ⨀ 琀攀洀瀀氀愀琀攀猀 椀渀 怀渀最ⴀ椀渀挀氀甀搀攀怀 昀爀漀洀 礀漀甀爀 愀瀀瀀氀椀挀愀琀椀漀渀✀猀 搀漀洀愀椀渀 眀椀琀栀漀甀琀 栀愀瘀椀渀最 琀漀 攀瘀攀渀 欀渀漀眀 愀戀漀甀琀 匀䌀䔀⸀ഀഀ
 * It blocks loading templates from other domains or loading templates over http from an https਍ ⨀ 猀攀爀瘀攀搀 搀漀挀甀洀攀渀琀⸀  夀漀甀 挀愀渀 挀栀愀渀最攀 琀栀攀猀攀 戀礀 猀攀琀琀椀渀最 礀漀甀爀 漀眀渀 挀甀猀琀漀洀 笀䀀氀椀渀欀ഀഀ
 * ng.$sceDelegateProvider#methods_resourceUrlWhitelist whitelists} and {@link਍ ⨀ 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀倀爀漀瘀椀搀攀爀⌀洀攀琀栀漀搀猀开爀攀猀漀甀爀挀攀唀爀氀䈀氀愀挀欀氀椀猀琀 戀氀愀挀欀氀椀猀琀猀紀 昀漀爀 洀愀琀挀栀椀渀最 猀甀挀栀 唀刀䰀猀⸀ഀഀ
 *਍ ⨀ 吀栀椀猀 猀椀最渀椀昀椀挀愀渀琀氀礀 爀攀搀甀挀攀猀 琀栀攀 漀瘀攀爀栀攀愀搀⸀  䤀琀 椀猀 昀愀爀 攀愀猀椀攀爀 琀漀 瀀愀礀 琀栀攀 猀洀愀氀氀 漀瘀攀爀栀攀愀搀 愀渀搀 栀愀瘀攀 愀渀ഀഀ
 * application that's secure and can be audited to verify that with much more ease than bolting਍ ⨀ 猀攀挀甀爀椀琀礀 漀渀琀漀 愀渀 愀瀀瀀氀椀挀愀琀椀漀渀 氀愀琀攀爀⸀ഀഀ
 *਍ ⨀ 㰀愀 渀愀洀攀㴀∀挀漀渀琀攀砀琀猀∀㸀㰀⼀愀㸀ഀഀ
 * ## What trusted context types are supported?਍ ⨀ഀഀ
 * | Context             | Notes          |਍ ⨀ 簀ⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀ簀ⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀ簀ഀഀ
 * | `$sce.HTML`         | For HTML that's safe to source into the application.  The {@link ng.directive:ngBindHtml ngBindHtml} directive uses this context for bindings. |਍ ⨀ 簀 怀␀猀挀攀⸀䌀匀匀怀          簀 䘀漀爀 䌀匀匀 琀栀愀琀✀猀 猀愀昀攀 琀漀 猀漀甀爀挀攀 椀渀琀漀 琀栀攀 愀瀀瀀氀椀挀愀琀椀漀渀⸀  䌀甀爀爀攀渀琀氀礀 甀渀甀猀攀搀⸀  䘀攀攀氀 昀爀攀攀 琀漀 甀猀攀 椀琀 椀渀 礀漀甀爀 漀眀渀 搀椀爀攀挀琀椀瘀攀猀⸀ 簀ഀഀ
 * | `$sce.URL`          | For URLs that are safe to follow as links.  Currently unused (`<a href=` and `<img src=` sanitize their urls and don't consititute an SCE context. |਍ ⨀ 簀 怀␀猀挀攀⸀刀䔀匀伀唀刀䌀䔀开唀刀䰀怀 簀 䘀漀爀 唀刀䰀猀 琀栀愀琀 愀爀攀 渀漀琀 漀渀氀礀 猀愀昀攀 琀漀 昀漀氀氀漀眀 愀猀 氀椀渀欀猀Ⰰ 戀甀琀 眀栀漀猀攀 挀漀渀琀攀渀猀 愀爀攀 愀氀猀漀 猀愀昀攀 琀漀 椀渀挀氀甀搀攀 椀渀 礀漀甀爀 愀瀀瀀氀椀挀愀琀椀漀渀⸀  䔀砀愀洀瀀氀攀猀 椀渀挀氀甀搀攀 怀渀最ⴀ椀渀挀氀甀搀攀怀Ⰰ 怀猀爀挀怀 ⼀ 怀渀最匀爀挀怀 戀椀渀搀椀渀最猀 昀漀爀 琀愀最猀 漀琀栀攀爀 琀栀愀渀 怀䤀䴀䜀怀 ⠀攀⸀最⸀ 怀䤀䘀刀䄀䴀䔀怀Ⰰ 怀伀䈀䨀䔀䌀吀怀Ⰰ 攀琀挀⸀⤀  㰀戀爀㸀㰀戀爀㸀一漀琀攀 琀栀愀琀 怀␀猀挀攀⸀刀䔀匀伀唀刀䌀䔀开唀刀䰀怀 洀愀欀攀猀 愀 猀琀爀漀渀最攀爀 猀琀愀琀攀洀攀渀琀 愀戀漀甀琀 琀栀攀 唀刀䰀 琀栀愀渀 怀␀猀挀攀⸀唀刀䰀怀 搀漀攀猀 愀渀搀 琀栀攀爀攀昀漀爀攀 挀漀渀琀攀砀琀猀 爀攀焀甀椀爀椀渀最 瘀愀氀甀攀猀 琀爀甀猀琀攀搀 昀漀爀 怀␀猀挀攀⸀刀䔀匀伀唀刀䌀䔀开唀刀䰀怀 挀愀渀 戀攀 甀猀攀搀 愀渀礀眀栀攀爀攀 琀栀愀琀 瘀愀氀甀攀猀 琀爀甀猀琀攀搀 昀漀爀 怀␀猀挀攀⸀唀刀䰀怀 愀爀攀 爀攀焀甀椀爀攀搀⸀ 簀ഀഀ
 * | `$sce.JS`           | For JavaScript that is safe to execute in your application's context.  Currently unused.  Feel free to use it in your own directives. |਍ ⨀ഀഀ
 * ## Format of items in {@link ng.$sceDelegateProvider#methods_resourceUrlWhitelist resourceUrlWhitelist}/{@link ng.$sceDelegateProvider#methods_resourceUrlBlacklist Blacklist} <a name="resourceUrlPatternItem"></a>਍ ⨀ഀഀ
 *  Each element in these arrays must be one of the following:਍ ⨀ഀഀ
 *  - **'self'**਍ ⨀    ⴀ 吀栀攀 猀瀀攀挀椀愀氀 ⨀⨀猀琀爀椀渀最⨀⨀Ⰰ 怀✀猀攀氀昀✀怀Ⰰ 挀愀渀 戀攀 甀猀攀搀 琀漀 洀愀琀挀栀 愀最愀椀渀猀琀 愀氀氀 唀刀䰀猀 漀昀 琀栀攀 ⨀⨀猀愀洀攀ഀഀ
 *      domain** as the application document using the **same protocol**.਍ ⨀  ⴀ ⨀⨀匀琀爀椀渀最⨀⨀ ⠀攀砀挀攀瀀琀 琀栀攀 猀瀀攀挀椀愀氀 瘀愀氀甀攀 怀✀猀攀氀昀✀怀⤀ഀഀ
 *    - The string is matched against the full *normalized / absolute URL* of the resource਍ ⨀      戀攀椀渀最 琀攀猀琀攀搀 ⠀猀甀戀猀琀爀椀渀最 洀愀琀挀栀攀猀 愀爀攀 渀漀琀 最漀漀搀 攀渀漀甀最栀⸀⤀ഀഀ
 *    - There are exactly **two wildcard sequences** - `*` and `**`.  All other characters਍ ⨀      洀愀琀挀栀 琀栀攀洀猀攀氀瘀攀猀⸀ഀഀ
 *    - `*`: matches zero or more occurances of any character other than one of the following 6਍ ⨀      挀栀愀爀愀挀琀攀爀猀㨀 ✀怀㨀怀✀Ⰰ ✀怀⼀怀✀Ⰰ ✀怀⸀怀✀Ⰰ ✀怀㼀怀✀Ⰰ ✀怀☀怀✀ 愀渀搀 ✀㬀✀⸀  䤀琀✀猀 愀 甀猀攀昀甀氀 眀椀氀搀挀愀爀搀 昀漀爀 甀猀攀ഀഀ
 *      in a whitelist.਍ ⨀    ⴀ 怀⨀⨀怀㨀 洀愀琀挀栀攀猀 稀攀爀漀 漀爀 洀漀爀攀 漀挀挀甀爀愀渀挀攀猀 漀昀 ⨀愀渀礀⨀ 挀栀愀爀愀挀琀攀爀⸀  䄀猀 猀甀挀栀Ⰰ 椀琀✀猀 渀漀琀ഀഀ
 *      not appropriate to use in for a scheme, domain, etc. as it would match too much.  (e.g.਍ ⨀      栀琀琀瀀㨀⼀⼀⨀⨀⸀攀砀愀洀瀀氀攀⸀挀漀洀⼀ 眀漀甀氀搀 洀愀琀挀栀 栀琀琀瀀㨀⼀⼀攀瘀椀氀⸀挀漀洀⼀㼀椀最渀漀爀攀㴀⸀攀砀愀洀瀀氀攀⸀挀漀洀⼀ 愀渀搀 琀栀愀琀 洀椀最栀琀ഀഀ
 *      not have been the intention.)  It's usage at the very end of the path is ok.  (e.g.਍ ⨀      栀琀琀瀀㨀⼀⼀昀漀漀⸀攀砀愀洀瀀氀攀⸀挀漀洀⼀琀攀洀瀀氀愀琀攀猀⼀⨀⨀⤀⸀ഀഀ
 *  - **RegExp** (*see caveat below*)਍ ⨀    ⴀ ⨀䌀愀瘀攀愀琀⨀㨀  圀栀椀氀攀 爀攀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀猀 愀爀攀 瀀漀眀攀爀昀甀氀 愀渀搀 漀昀昀攀爀 最爀攀愀琀 昀氀攀砀椀戀椀氀椀琀礀Ⰰ  琀栀攀椀爀 猀礀渀琀愀砀ഀഀ
 *      (and all the inevitable escaping) makes them *harder to maintain*.  It's easy to਍ ⨀      愀挀挀椀搀攀渀琀愀氀氀礀 椀渀琀爀漀搀甀挀攀 愀 戀甀最 眀栀攀渀 漀渀攀 甀瀀搀愀琀攀猀 愀 挀漀洀瀀氀攀砀 攀砀瀀爀攀猀猀椀漀渀 ⠀椀洀栀漀Ⰰ 愀氀氀 爀攀最攀砀攀猀 猀栀漀甀氀搀ഀഀ
 *      have good test coverage.).  For instance, the use of `.` in the regex is correct only in a਍ ⨀      猀洀愀氀氀 渀甀洀戀攀爀 漀昀 挀愀猀攀猀⸀  䄀 怀⸀怀 挀栀愀爀愀挀琀攀爀 椀渀 琀栀攀 爀攀最攀砀 甀猀攀搀 眀栀攀渀 洀愀琀挀栀椀渀最 琀栀攀 猀挀栀攀洀攀 漀爀 愀ഀഀ
 *      subdomain could be matched against a `:` or literal `.` that was likely not intended.   It਍ ⨀      椀猀 栀椀最栀氀礀 爀攀挀漀洀洀攀渀搀攀搀 琀漀 甀猀攀 琀栀攀 猀琀爀椀渀最 瀀愀琀琀攀爀渀猀 愀渀搀 漀渀氀礀 昀愀氀氀 戀愀挀欀 琀漀 爀攀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀猀ഀഀ
 *      if they as a last resort.਍ ⨀    ⴀ 吀栀攀 爀攀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 洀甀猀琀 戀攀 愀渀 椀渀猀琀愀渀挀攀 漀昀 刀攀最䔀砀瀀 ⠀椀⸀攀⸀ 渀漀琀 愀 猀琀爀椀渀最⸀⤀  䤀琀 椀猀ഀഀ
 *      matched against the **entire** *normalized / absolute URL* of the resource being tested਍ ⨀      ⠀攀瘀攀渀 眀栀攀渀 琀栀攀 刀攀最䔀砀瀀 搀椀搀 渀漀琀 栀愀瘀攀 琀栀攀 怀帀怀 愀渀搀 怀␀怀 挀漀搀攀猀⸀⤀  䤀渀 愀搀搀椀琀椀漀渀Ⰰ 愀渀礀 昀氀愀最猀ഀഀ
 *      present on the RegExp (such as multiline, global, ignoreCase) are ignored.਍ ⨀    ⴀ 䤀昀 礀漀甀 愀爀攀 最攀渀攀爀愀琀椀渀最 礀漀甀爀 䨀愀瘀愀猀挀爀椀瀀琀 昀爀漀洀 猀漀洀攀 漀琀栀攀爀 琀攀洀瀀氀愀琀椀渀最 攀渀最椀渀攀 ⠀渀漀琀ഀഀ
 *      recommended, e.g. in issue [#4006](https://github.com/angular/angular.js/issues/4006)),਍ ⨀      爀攀洀攀洀戀攀爀 琀漀 攀猀挀愀瀀攀 礀漀甀爀 爀攀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 ⠀愀渀搀 戀攀 愀眀愀爀攀 琀栀愀琀 礀漀甀 洀椀最栀琀 渀攀攀搀 洀漀爀攀 琀栀愀渀ഀഀ
 *      one level of escaping depending on your templating engine and the way you interpolated਍ ⨀      琀栀攀 瘀愀氀甀攀⸀⤀  䐀漀 洀愀欀攀 甀猀攀 漀昀 礀漀甀爀 瀀氀愀琀昀漀爀洀✀猀 攀猀挀愀瀀椀渀最 洀攀挀栀愀渀椀猀洀 愀猀 椀琀 洀椀最栀琀 戀攀 最漀漀搀ഀഀ
 *      enough before coding your own.  e.g. Ruby has਍ ⨀      嬀刀攀最攀砀瀀⸀攀猀挀愀瀀攀⠀猀琀爀⤀崀⠀栀琀琀瀀㨀⼀⼀眀眀眀⸀爀甀戀礀ⴀ搀漀挀⸀漀爀最⼀挀漀爀攀ⴀ㈀⸀　⸀　⼀刀攀最攀砀瀀⸀栀琀洀氀⌀洀攀琀栀漀搀ⴀ挀ⴀ攀猀挀愀瀀攀⤀ഀഀ
 *      and Python has [re.escape](http://docs.python.org/library/re.html#re.escape).਍ ⨀      䨀愀瘀愀猀挀爀椀瀀琀 氀愀挀欀猀 愀 猀椀洀椀氀愀爀 戀甀椀氀琀 椀渀 昀甀渀挀琀椀漀渀 昀漀爀 攀猀挀愀瀀椀渀最⸀  吀愀欀攀 愀 氀漀漀欀 愀琀 䜀漀漀最氀攀ഀഀ
 *      Closure library's [goog.string.regExpEscape(s)](਍ ⨀      栀琀琀瀀㨀⼀⼀搀漀挀猀⸀挀氀漀猀甀爀攀ⴀ氀椀戀爀愀爀礀⸀最漀漀最氀攀挀漀搀攀⸀挀漀洀⼀最椀琀⼀挀氀漀猀甀爀攀开最漀漀最开猀琀爀椀渀最开猀琀爀椀渀最⸀樀猀⸀猀漀甀爀挀攀⸀栀琀洀氀⌀氀椀渀攀㤀㘀㈀⤀⸀ഀഀ
 *਍ ⨀ 刀攀昀攀爀 笀䀀氀椀渀欀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀倀爀漀瘀椀搀攀爀 ␀猀挀攀䐀攀氀攀最愀琀攀倀爀漀瘀椀搀攀爀紀 昀漀爀 愀渀 攀砀愀洀瀀氀攀⸀ഀഀ
 *਍ ⨀ ⌀⌀ 匀栀漀眀 洀攀 愀渀 攀砀愀洀瀀氀攀 甀猀椀渀最 匀䌀䔀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
<example module="mySceApp">਍㰀昀椀氀攀 渀愀洀攀㴀∀椀渀搀攀砀⸀栀琀洀氀∀㸀ഀഀ
  <div ng-controller="myAppController as myCtrl">਍    㰀椀 渀最ⴀ戀椀渀搀ⴀ栀琀洀氀㴀∀洀礀䌀琀爀氀⸀攀砀瀀氀椀挀椀琀氀礀吀爀甀猀琀攀搀䠀琀洀氀∀ 椀搀㴀∀攀砀瀀氀椀挀椀琀氀礀吀爀甀猀琀攀搀䠀琀洀氀∀㸀㰀⼀椀㸀㰀戀爀㸀㰀戀爀㸀ഀഀ
    <b>User comments</b><br>਍    䈀礀 搀攀昀愀甀氀琀Ⰰ 䠀吀䴀䰀 琀栀愀琀 椀猀渀✀琀 攀砀瀀氀椀挀椀琀氀礀 琀爀甀猀琀攀搀 ⠀攀⸀最⸀ 䄀氀椀挀攀✀猀 挀漀洀洀攀渀琀⤀ 椀猀 猀愀渀椀琀椀稀攀搀 眀栀攀渀ഀഀ
    $sanitize is available.  If $sanitize isn't available, this results in an error instead of an਍    攀砀瀀氀漀椀琀⸀ഀഀ
    <div class="well">਍      㰀搀椀瘀 渀最ⴀ爀攀瀀攀愀琀㴀∀甀猀攀爀䌀漀洀洀攀渀琀 椀渀 洀礀䌀琀爀氀⸀甀猀攀爀䌀漀洀洀攀渀琀猀∀㸀ഀഀ
        <b>{{userComment.name}}</b>:਍        㰀猀瀀愀渀 渀最ⴀ戀椀渀搀ⴀ栀琀洀氀㴀∀甀猀攀爀䌀漀洀洀攀渀琀⸀栀琀洀氀䌀漀洀洀攀渀琀∀ 挀氀愀猀猀㴀∀栀琀洀氀䌀漀洀洀攀渀琀∀㸀㰀⼀猀瀀愀渀㸀ഀഀ
        <br>਍      㰀⼀搀椀瘀㸀ഀഀ
    </div>਍  㰀⼀搀椀瘀㸀ഀഀ
</file>਍ഀഀ
<file name="script.js">਍  瘀愀爀 洀礀匀挀攀䄀瀀瀀 㴀 愀渀最甀氀愀爀⸀洀漀搀甀氀攀⠀✀洀礀匀挀攀䄀瀀瀀✀Ⰰ 嬀✀渀最匀愀渀椀琀椀稀攀✀崀⤀㬀ഀഀ
਍  洀礀匀挀攀䄀瀀瀀⸀挀漀渀琀爀漀氀氀攀爀⠀∀洀礀䄀瀀瀀䌀漀渀琀爀漀氀氀攀爀∀Ⰰ 昀甀渀挀琀椀漀渀 洀礀䄀瀀瀀䌀漀渀琀爀漀氀氀攀爀⠀␀栀琀琀瀀Ⰰ ␀琀攀洀瀀氀愀琀攀䌀愀挀栀攀Ⰰ ␀猀挀攀⤀ 笀ഀഀ
    var self = this;਍    ␀栀琀琀瀀⸀最攀琀⠀∀琀攀猀琀开搀愀琀愀⸀樀猀漀渀∀Ⰰ 笀挀愀挀栀攀㨀 ␀琀攀洀瀀氀愀琀攀䌀愀挀栀攀紀⤀⸀猀甀挀挀攀猀猀⠀昀甀渀挀琀椀漀渀⠀甀猀攀爀䌀漀洀洀攀渀琀猀⤀ 笀ഀഀ
      self.userComments = userComments;਍    紀⤀㬀ഀഀ
    self.explicitlyTrustedHtml = $sce.trustAsHtml(਍        ✀㰀猀瀀愀渀 漀渀洀漀甀猀攀漀瘀攀爀㴀∀琀栀椀猀⸀琀攀砀琀䌀漀渀琀攀渀琀㴀☀焀甀漀琀㬀䔀砀瀀氀椀挀椀琀氀礀 琀爀甀猀琀攀搀 䠀吀䴀䰀 戀礀瀀愀猀猀攀猀 ✀ ⬀ഀഀ
        'sanitization.&quot;">Hover over this text.</span>');਍  紀⤀㬀ഀഀ
</file>਍ഀഀ
<file name="test_data.json">਍嬀ഀഀ
  { "name": "Alice",਍    ∀栀琀洀氀䌀漀洀洀攀渀琀∀㨀ഀഀ
        "<span onmouseover='this.textContent=\"PWN3D!\"'>Is <i>anyone</i> reading this?</span>"਍  紀Ⰰഀഀ
  { "name": "Bob",਍    ∀栀琀洀氀䌀漀洀洀攀渀琀∀㨀 ∀㰀椀㸀夀攀猀℀㰀⼀椀㸀  䄀洀 䤀 琀栀攀 漀渀氀礀 漀琀栀攀爀 漀渀攀㼀∀ഀഀ
  }਍崀ഀഀ
</file>਍ഀഀ
<file name="scenario.js">਍  搀攀猀挀爀椀戀攀⠀✀匀䌀䔀 搀漀挀 搀攀洀漀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    it('should sanitize untrusted values', function() {਍      攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀栀琀洀氀䌀漀洀洀攀渀琀✀⤀⸀栀琀洀氀⠀⤀⤀⸀琀漀䈀攀⠀✀㰀猀瀀愀渀㸀䤀猀 㰀椀㸀愀渀礀漀渀攀㰀⼀椀㸀 爀攀愀搀椀渀最 琀栀椀猀㼀㰀⼀猀瀀愀渀㸀✀⤀㬀ഀഀ
    });਍    椀琀⠀✀猀栀漀甀氀搀 一伀吀 猀愀渀椀琀椀稀攀 攀砀瀀氀椀挀椀琀氀礀 琀爀甀猀琀攀搀 瘀愀氀甀攀猀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
      expect(element('#explicitlyTrustedHtml').html()).toBe(਍          ✀㰀猀瀀愀渀 漀渀洀漀甀猀攀漀瘀攀爀㴀∀琀栀椀猀⸀琀攀砀琀䌀漀渀琀攀渀琀㴀☀焀甀漀琀㬀䔀砀瀀氀椀挀椀琀氀礀 琀爀甀猀琀攀搀 䠀吀䴀䰀 戀礀瀀愀猀猀攀猀 ✀ ⬀ഀഀ
          'sanitization.&quot;">Hover over this text.</span>');਍    紀⤀㬀ഀഀ
  });਍㰀⼀昀椀氀攀㸀ഀഀ
</example>਍ ⨀ഀഀ
 *਍ ⨀ഀഀ
 * ## Can I disable SCE completely?਍ ⨀ഀഀ
 * Yes, you can.  However, this is strongly discouraged.  SCE gives you a lot of security benefits਍ ⨀ 昀漀爀 氀椀琀琀氀攀 挀漀搀椀渀最 漀瘀攀爀栀攀愀搀⸀  䤀琀 眀椀氀氀 戀攀 洀甀挀栀 栀愀爀搀攀爀 琀漀 琀愀欀攀 愀渀 匀䌀䔀 搀椀猀愀戀氀攀搀 愀瀀瀀氀椀挀愀琀椀漀渀 愀渀搀ഀഀ
 * either secure it on your own or enable SCE at a later stage.  It might make sense to disable SCE਍ ⨀ 昀漀爀 挀愀猀攀猀 眀栀攀爀攀 礀漀甀 栀愀瘀攀 愀 氀漀琀 漀昀 攀砀椀猀琀椀渀最 挀漀搀攀 琀栀愀琀 眀愀猀 眀爀椀琀琀攀渀 戀攀昀漀爀攀 匀䌀䔀 眀愀猀 椀渀琀爀漀搀甀挀攀搀 愀渀搀ഀഀ
 * you're migrating them a module at a time.਍ ⨀ഀഀ
 * That said, here's how you can completely disable SCE:਍ ⨀ഀഀ
 * <pre class="prettyprint">਍ ⨀   愀渀最甀氀愀爀⸀洀漀搀甀氀攀⠀✀洀礀䄀瀀瀀圀椀琀栀匀挀攀䐀椀猀愀戀氀攀搀洀礀䄀瀀瀀✀Ⰰ 嬀崀⤀⸀挀漀渀昀椀最⠀昀甀渀挀琀椀漀渀⠀␀猀挀攀倀爀漀瘀椀搀攀爀⤀ 笀ഀഀ
 *     // Completely disable SCE.  For demonstration purposes only!਍ ⨀     ⼀⼀ 䐀漀 渀漀琀 甀猀攀 椀渀 渀攀眀 瀀爀漀樀攀挀琀猀⸀ഀഀ
 *     $sceProvider.enabled(false);਍ ⨀   紀⤀㬀ഀഀ
 * </pre>਍ ⨀ഀഀ
 */਍⼀⨀ 樀猀栀椀渀琀 洀愀砀氀攀渀㨀 ㄀　　 ⨀⼀ഀഀ
਍昀甀渀挀琀椀漀渀 ␀匀挀攀倀爀漀瘀椀搀攀爀⠀⤀ 笀ഀഀ
  var enabled = true;਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
   * @name ng.sceProvider#enabled਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀倀爀漀瘀椀搀攀爀ഀഀ
   * @function਍   ⨀ഀഀ
   * @param {boolean=} value If provided, then enables/disables SCE.਍   ⨀ 䀀爀攀琀甀爀渀 笀戀漀漀氀攀愀渀紀 琀爀甀攀 椀昀 匀䌀䔀 椀猀 攀渀愀戀氀攀搀Ⰰ 昀愀氀猀攀 漀琀栀攀爀眀椀猀攀⸀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Enables/disables SCE and returns the current value.਍   ⨀⼀ഀഀ
  this.enabled = function (value) {਍    椀昀 ⠀愀爀最甀洀攀渀琀猀⸀氀攀渀最琀栀⤀ 笀ഀഀ
      enabled = !!value;਍    紀ഀഀ
    return enabled;਍  紀㬀ഀഀ
਍ഀഀ
  /* Design notes on the default implementation for SCE.਍   ⨀ഀഀ
   * The API contract for the SCE delegate਍   ⨀ ⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀഀഀ
   * The SCE delegate object must provide the following 3 methods:਍   ⨀ഀഀ
   * - trustAs(contextEnum, value)਍   ⨀     吀栀椀猀 洀攀琀栀漀搀 椀猀 甀猀攀搀 琀漀 琀攀氀氀 琀栀攀 匀䌀䔀 猀攀爀瘀椀挀攀 琀栀愀琀 琀栀攀 瀀爀漀瘀椀搀攀搀 瘀愀氀甀攀 椀猀 伀䬀 琀漀 甀猀攀 椀渀 琀栀攀ഀഀ
   *     contexts specified by contextEnum.  It must return an object that will be accepted by਍   ⨀     最攀琀吀爀甀猀琀攀搀⠀⤀ 昀漀爀 愀 挀漀洀瀀愀琀椀戀氀攀 挀漀渀琀攀砀琀䔀渀甀洀 愀渀搀 爀攀琀甀爀渀 琀栀椀猀 瘀愀氀甀攀⸀ഀഀ
   *਍   ⨀ ⴀ 瘀愀氀甀攀伀昀⠀瘀愀氀甀攀⤀ഀഀ
   *     For values that were not produced by trustAs(), return them as is.  For values that were਍   ⨀     瀀爀漀搀甀挀攀搀 戀礀 琀爀甀猀琀䄀猀⠀⤀Ⰰ 爀攀琀甀爀渀 琀栀攀 挀漀爀爀攀猀瀀漀渀搀椀渀最 椀渀瀀甀琀 瘀愀氀甀攀 琀漀 琀爀甀猀琀䄀猀⸀  䈀愀猀椀挀愀氀氀礀Ⰰ 椀昀ഀഀ
   *     trustAs is wrapping the given values into some type, this operation unwraps it when given਍   ⨀     猀甀挀栀 愀 瘀愀氀甀攀⸀ഀഀ
   *਍   ⨀ ⴀ 最攀琀吀爀甀猀琀攀搀⠀挀漀渀琀攀砀琀䔀渀甀洀Ⰰ 瘀愀氀甀攀⤀ഀഀ
   *     This function should return the a value that is safe to use in the context specified by਍   ⨀     挀漀渀琀攀砀琀䔀渀甀洀 漀爀 琀栀爀漀眀 愀渀搀 攀砀挀攀瀀琀椀漀渀 漀琀栀攀爀眀椀猀攀⸀ഀഀ
   *਍   ⨀ 一伀吀䔀㨀 吀栀椀猀 挀漀渀琀爀愀挀琀 搀攀氀椀戀攀爀愀琀攀氀礀 搀漀攀猀 一伀吀 猀琀愀琀攀 琀栀愀琀 瘀愀氀甀攀猀 爀攀琀甀爀渀攀搀 戀礀 琀爀甀猀琀䄀猀⠀⤀ 洀甀猀琀 戀攀ഀഀ
   * opaque or wrapped in some holder object.  That happens to be an implementation detail.  For਍   ⨀ 椀渀猀琀愀渀挀攀Ⰰ 愀渀 椀洀瀀氀攀洀攀渀琀愀琀椀漀渀 挀漀甀氀搀 洀愀椀渀琀愀椀渀 愀 爀攀最椀猀琀爀礀 漀昀 愀氀氀 琀爀甀猀琀攀搀 漀戀樀攀挀琀猀 戀礀 挀漀渀琀攀砀琀⸀  䤀渀ഀഀ
   * such a case, trustAs() would return the same object that was passed in.  getTrusted() would਍   ⨀ 爀攀琀甀爀渀 琀栀攀 猀愀洀攀 漀戀樀攀挀琀 瀀愀猀猀攀搀 椀渀 椀昀 椀琀 眀愀猀 昀漀甀渀搀 椀渀 琀栀攀 爀攀最椀猀琀爀礀 甀渀搀攀爀 愀 挀漀洀瀀愀琀椀戀氀攀 挀漀渀琀攀砀琀 漀爀ഀഀ
   * throw an exception otherwise.  An implementation might only wrap values some of the time based਍   ⨀ 漀渀 猀漀洀攀 挀爀椀琀攀爀椀愀⸀  最攀琀吀爀甀猀琀攀搀⠀⤀ 洀椀最栀琀 爀攀琀甀爀渀 愀 瘀愀氀甀攀 愀渀搀 渀漀琀 琀栀爀漀眀 愀渀 攀砀挀攀瀀琀椀漀渀 昀漀爀 猀瀀攀挀椀愀氀ഀഀ
   * constants or objects even if not wrapped.  All such implementations fulfill this contract.਍   ⨀ഀഀ
   *਍   ⨀ 䄀 渀漀琀攀 漀渀 琀栀攀 椀渀栀攀爀椀琀愀渀挀攀 洀漀搀攀氀 昀漀爀 匀䌀䔀 挀漀渀琀攀砀琀猀ഀഀ
   * ------------------------------------------------਍   ⨀ 䤀✀瘀攀 甀猀攀搀 椀渀栀攀爀椀琀愀渀挀攀 愀渀搀 洀愀搀攀 刀䔀匀伀唀刀䌀䔀开唀刀䰀 眀爀愀瀀瀀攀搀 琀礀瀀攀猀 愀 猀甀戀琀礀瀀攀 漀昀 唀刀䰀 眀爀愀瀀瀀攀搀 琀礀瀀攀猀⸀  吀栀椀猀ഀഀ
   * is purely an implementation details.਍   ⨀ഀഀ
   * The contract is simply this:਍   ⨀ഀഀ
   *     getTrusted($sce.RESOURCE_URL, value) succeeding implies that getTrusted($sce.URL, value)਍   ⨀     眀椀氀氀 愀氀猀漀 猀甀挀挀攀攀搀⸀ഀഀ
   *਍   ⨀ 䤀渀栀攀爀椀琀愀渀挀攀 栀愀瀀瀀攀渀猀 琀漀 挀愀瀀琀甀爀攀 琀栀椀猀 椀渀 愀 渀愀琀甀爀愀氀 眀愀礀⸀  䤀渀 猀漀洀攀 昀甀琀甀爀攀Ⰰ 眀攀ഀഀ
   * may not use inheritance anymore.  That is OK because no code outside of਍   ⨀ 猀挀攀⸀樀猀 愀渀搀 猀挀攀匀瀀攀挀猀⸀樀猀 眀漀甀氀搀 渀攀攀搀 琀漀 戀攀 愀眀愀爀攀 漀昀 琀栀椀猀 搀攀琀愀椀氀⸀ഀഀ
   */਍ഀഀ
  this.$get = ['$parse', '$sniffer', '$sceDelegate', function(਍                ␀瀀愀爀猀攀Ⰰ   ␀猀渀椀昀昀攀爀Ⰰ   ␀猀挀攀䐀攀氀攀最愀琀攀⤀ 笀ഀഀ
    // Prereq: Ensure that we're not running in IE8 quirks mode.  In that mode, IE allows਍    ⼀⼀ 琀栀攀 ∀攀砀瀀爀攀猀猀椀漀渀⠀樀愀瘀愀猀挀爀椀瀀琀 攀砀瀀爀攀猀猀椀漀渀⤀∀ 猀礀渀琀愀砀 眀栀椀挀栀 椀猀 椀渀猀攀挀甀爀攀⸀ഀഀ
    if (enabled && $sniffer.msie && $sniffer.msieDocumentMode < 8) {਍      琀栀爀漀眀 ␀猀挀攀䴀椀渀䔀爀爀⠀✀椀攀焀甀椀爀欀猀✀Ⰰഀഀ
        'Strict Contextual Escaping does not support Internet Explorer version < 9 in quirks ' +਍        ✀洀漀搀攀⸀  夀漀甀 挀愀渀 昀椀砀 琀栀椀猀 戀礀 愀搀搀椀渀最 琀栀攀 琀攀砀琀 㰀℀搀漀挀琀礀瀀攀 栀琀洀氀㸀 琀漀 琀栀攀 琀漀瀀 漀昀 礀漀甀爀 䠀吀䴀䰀 ✀ ⬀ഀഀ
        'document.  See http://docs.angularjs.org/api/ng.$sce for more information.');਍    紀ഀഀ
਍    瘀愀爀 猀挀攀 㴀 挀漀瀀礀⠀匀䌀䔀开䌀伀一吀䔀堀吀匀⤀㬀ഀഀ
਍    ⼀⨀⨀ഀഀ
     * @ngdoc function਍     ⨀ 䀀渀愀洀攀 渀最⸀猀挀攀⌀椀猀䔀渀愀戀氀攀搀ഀഀ
     * @methodOf ng.$sce਍     ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
     *਍     ⨀ 䀀爀攀琀甀爀渀 笀䈀漀漀氀攀愀渀紀 琀爀甀攀 椀昀 匀䌀䔀 椀猀 攀渀愀戀氀攀搀Ⰰ 昀愀氀猀攀 漀琀栀攀爀眀椀猀攀⸀  䤀昀 礀漀甀 眀愀渀琀 琀漀 猀攀琀 琀栀攀 瘀愀氀甀攀Ⰰ 礀漀甀ഀഀ
     * have to do it at module config time on {@link ng.$sceProvider $sceProvider}.਍     ⨀ഀഀ
     * @description਍     ⨀ 刀攀琀甀爀渀猀 愀 戀漀漀氀攀愀渀 椀渀搀椀挀愀琀椀渀最 椀昀 匀䌀䔀 椀猀 攀渀愀戀氀攀搀⸀ഀഀ
     */਍    猀挀攀⸀椀猀䔀渀愀戀氀攀搀 㴀 昀甀渀挀琀椀漀渀 ⠀⤀ 笀ഀഀ
      return enabled;਍    紀㬀ഀഀ
    sce.trustAs = $sceDelegate.trustAs;਍    猀挀攀⸀最攀琀吀爀甀猀琀攀搀 㴀 ␀猀挀攀䐀攀氀攀最愀琀攀⸀最攀琀吀爀甀猀琀攀搀㬀ഀഀ
    sce.valueOf = $sceDelegate.valueOf;਍ഀഀ
    if (!enabled) {਍      猀挀攀⸀琀爀甀猀琀䄀猀 㴀 猀挀攀⸀最攀琀吀爀甀猀琀攀搀 㴀 昀甀渀挀琀椀漀渀⠀琀礀瀀攀Ⰰ 瘀愀氀甀攀⤀ 笀 爀攀琀甀爀渀 瘀愀氀甀攀㬀 紀㬀ഀഀ
      sce.valueOf = identity;਍    紀ഀഀ
਍    ⼀⨀⨀ഀഀ
     * @ngdoc method਍     ⨀ 䀀渀愀洀攀 渀最⸀␀猀挀攀⌀瀀愀爀猀攀ഀഀ
     * @methodOf ng.$sce਍     ⨀ഀഀ
     * @description਍     ⨀ 䌀漀渀瘀攀爀琀猀 䄀渀最甀氀愀爀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 攀砀瀀爀攀猀猀椀漀渀紀 椀渀琀漀 愀 昀甀渀挀琀椀漀渀⸀  吀栀椀猀 椀猀 氀椀欀攀 笀䀀氀椀渀欀ഀഀ
     * ng.$parse $parse} and is identical when the expression is a literal constant.  Otherwise, it਍     ⨀ 眀爀愀瀀猀 琀栀攀 攀砀瀀爀攀猀猀椀漀渀 椀渀 愀 挀愀氀氀 琀漀 笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开最攀琀吀爀甀猀琀攀搀 ␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀⠀⨀琀礀瀀攀⨀Ⰰഀഀ
     * *result*)}਍     ⨀ഀഀ
     * @param {string} type The kind of SCE context in which this result will be used.਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 攀砀瀀爀攀猀猀椀漀渀 匀琀爀椀渀最 攀砀瀀爀攀猀猀椀漀渀 琀漀 挀漀洀瀀椀氀攀⸀ഀഀ
     * @returns {function(context, locals)} a function which represents the compiled expression:਍     ⨀ഀഀ
     *    * `context` �� `{object}` �� an object against which any expressions embedded in the strings਍     ⨀      愀爀攀 攀瘀愀氀甀愀琀攀搀 愀最愀椀渀猀琀 ⠀琀礀瀀椀挀愀氀氀礀 愀 猀挀漀瀀攀 漀戀樀攀挀琀⤀⸀ഀഀ
     *    * `locals` �� `{object=}` �� local variables context object, useful for overriding values in਍     ⨀      怀挀漀渀琀攀砀琀怀⸀ഀഀ
     */਍    猀挀攀⸀瀀愀爀猀攀䄀猀 㴀 昀甀渀挀琀椀漀渀 猀挀攀倀愀爀猀攀䄀猀⠀琀礀瀀攀Ⰰ 攀砀瀀爀⤀ 笀ഀഀ
      var parsed = $parse(expr);਍      椀昀 ⠀瀀愀爀猀攀搀⸀氀椀琀攀爀愀氀 ☀☀ 瀀愀爀猀攀搀⸀挀漀渀猀琀愀渀琀⤀ 笀ഀഀ
        return parsed;਍      紀 攀氀猀攀 笀ഀഀ
        return function sceParseAsTrusted(self, locals) {਍          爀攀琀甀爀渀 猀挀攀⸀最攀琀吀爀甀猀琀攀搀⠀琀礀瀀攀Ⰰ 瀀愀爀猀攀搀⠀猀攀氀昀Ⰰ 氀漀挀愀氀猀⤀⤀㬀ഀഀ
        };਍      紀ഀഀ
    };਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$sce#trustAs਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Delegates to {@link ng.$sceDelegate#methods_trustAs `$sceDelegate.trustAs`}.  As such,਍     ⨀ 爀攀琀甀爀渀猀 愀渀 漀戀樀攀挀琀琀栀愀琀 椀猀 琀爀甀猀琀攀搀 戀礀 愀渀最甀氀愀爀 昀漀爀 甀猀攀 椀渀 猀瀀攀挀椀昀椀攀搀 猀琀爀椀挀琀 挀漀渀琀攀砀琀甀愀氀ഀഀ
     * escaping contexts (such as ng-html-bind-unsafe, ng-include, any src attribute਍     ⨀ 椀渀琀攀爀瀀漀氀愀琀椀漀渀Ⰰ 愀渀礀 搀漀洀 攀瘀攀渀琀 戀椀渀搀椀渀最 愀琀琀爀椀戀甀琀攀 椀渀琀攀爀瀀漀氀愀琀椀漀渀 猀甀挀栀 愀猀 昀漀爀 漀渀挀氀椀挀欀Ⰰ  攀琀挀⸀⤀ഀഀ
     * that uses the provided value.  See * {@link ng.$sce $sce} for enabling strict contextual਍     ⨀ 攀猀挀愀瀀椀渀最⸀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 琀礀瀀攀 吀栀攀 欀椀渀搀 漀昀 挀漀渀琀攀砀琀 椀渀 眀栀椀挀栀 琀栀椀猀 瘀愀氀甀攀 椀猀 猀愀昀攀 昀漀爀 甀猀攀⸀  攀⸀最⸀ 甀爀氀Ⰰഀഀ
     *   resource_url, html, js and css.਍     ⨀ 䀀瀀愀爀愀洀 笀⨀紀 瘀愀氀甀攀 吀栀攀 瘀愀氀甀攀 琀栀愀琀 琀栀愀琀 猀栀漀甀氀搀 戀攀 挀漀渀猀椀搀攀爀攀搀 琀爀甀猀琀攀搀⼀猀愀昀攀⸀ഀഀ
     * @returns {*} A value that can be used to stand in for the provided `value` in places਍     ⨀ 眀栀攀爀攀 䄀渀最甀氀愀爀 攀砀瀀攀挀琀猀 愀 ␀猀挀攀⸀琀爀甀猀琀䄀猀⠀⤀ 爀攀琀甀爀渀 瘀愀氀甀攀⸀ഀഀ
     */਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$sce#trustAsHtml਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Shorthand method.  `$sce.trustAsHtml(value)` ��਍     ⨀     笀䀀氀椀渀欀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀 怀␀猀挀攀䐀攀氀攀最愀琀攀⸀琀爀甀猀琀䄀猀⠀␀猀挀攀⸀䠀吀䴀䰀Ⰰ 瘀愀氀甀攀⤀怀紀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀⨀紀 瘀愀氀甀攀 吀栀攀 瘀愀氀甀攀 琀漀 琀爀甀猀琀䄀猀⸀ഀഀ
     * @returns {*} An object that can be passed to {@link ng.$sce#methods_getTrustedHtml਍     ⨀     ␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀䠀琀洀氀⠀瘀愀氀甀攀⤀紀 琀漀 漀戀琀愀椀渀 琀栀攀 漀爀椀最椀渀愀氀 瘀愀氀甀攀⸀  ⠀瀀爀椀瘀椀氀攀最攀搀 搀椀爀攀挀琀椀瘀攀猀ഀഀ
     *     only accept expressions that are either literal constants or are the਍     ⨀     爀攀琀甀爀渀 瘀愀氀甀攀 漀昀 笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀 ␀猀挀攀⸀琀爀甀猀琀䄀猀紀⸀⤀ഀഀ
     */਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$sce#trustAsUrl਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Shorthand method.  `$sce.trustAsUrl(value)` ��਍     ⨀     笀䀀氀椀渀欀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀 怀␀猀挀攀䐀攀氀攀最愀琀攀⸀琀爀甀猀琀䄀猀⠀␀猀挀攀⸀唀刀䰀Ⰰ 瘀愀氀甀攀⤀怀紀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀⨀紀 瘀愀氀甀攀 吀栀攀 瘀愀氀甀攀 琀漀 琀爀甀猀琀䄀猀⸀ഀഀ
     * @returns {*} An object that can be passed to {@link ng.$sce#methods_getTrustedUrl਍     ⨀     ␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀唀爀氀⠀瘀愀氀甀攀⤀紀 琀漀 漀戀琀愀椀渀 琀栀攀 漀爀椀最椀渀愀氀 瘀愀氀甀攀⸀  ⠀瀀爀椀瘀椀氀攀最攀搀 搀椀爀攀挀琀椀瘀攀猀ഀഀ
     *     only accept expressions that are either literal constants or are the਍     ⨀     爀攀琀甀爀渀 瘀愀氀甀攀 漀昀 笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀 ␀猀挀攀⸀琀爀甀猀琀䄀猀紀⸀⤀ഀഀ
     */਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$sce#trustAsResourceUrl਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Shorthand method.  `$sce.trustAsResourceUrl(value)` ��਍     ⨀     笀䀀氀椀渀欀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀 怀␀猀挀攀䐀攀氀攀最愀琀攀⸀琀爀甀猀琀䄀猀⠀␀猀挀攀⸀刀䔀匀伀唀刀䌀䔀开唀刀䰀Ⰰ 瘀愀氀甀攀⤀怀紀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀⨀紀 瘀愀氀甀攀 吀栀攀 瘀愀氀甀攀 琀漀 琀爀甀猀琀䄀猀⸀ഀഀ
     * @returns {*} An object that can be passed to {@link ng.$sce#methods_getTrustedResourceUrl਍     ⨀     ␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀刀攀猀漀甀爀挀攀唀爀氀⠀瘀愀氀甀攀⤀紀 琀漀 漀戀琀愀椀渀 琀栀攀 漀爀椀最椀渀愀氀 瘀愀氀甀攀⸀  ⠀瀀爀椀瘀椀氀攀最攀搀 搀椀爀攀挀琀椀瘀攀猀ഀഀ
     *     only accept expressions that are either literal constants or are the return਍     ⨀     瘀愀氀甀攀 漀昀 笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀 ␀猀挀攀⸀琀爀甀猀琀䄀猀紀⸀⤀ഀഀ
     */਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$sce#trustAsJs਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Shorthand method.  `$sce.trustAsJs(value)` ��਍     ⨀     笀䀀氀椀渀欀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀 怀␀猀挀攀䐀攀氀攀最愀琀攀⸀琀爀甀猀琀䄀猀⠀␀猀挀攀⸀䨀匀Ⰰ 瘀愀氀甀攀⤀怀紀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀⨀紀 瘀愀氀甀攀 吀栀攀 瘀愀氀甀攀 琀漀 琀爀甀猀琀䄀猀⸀ഀഀ
     * @returns {*} An object that can be passed to {@link ng.$sce#methods_getTrustedJs਍     ⨀     ␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀䨀猀⠀瘀愀氀甀攀⤀紀 琀漀 漀戀琀愀椀渀 琀栀攀 漀爀椀最椀渀愀氀 瘀愀氀甀攀⸀  ⠀瀀爀椀瘀椀氀攀最攀搀 搀椀爀攀挀琀椀瘀攀猀ഀഀ
     *     only accept expressions that are either literal constants or are the਍     ⨀     爀攀琀甀爀渀 瘀愀氀甀攀 漀昀 笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀 ␀猀挀攀⸀琀爀甀猀琀䄀猀紀⸀⤀ഀഀ
     */਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$sce#getTrusted਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Delegates to {@link ng.$sceDelegate#methods_getTrusted `$sceDelegate.getTrusted`}.  As such,਍     ⨀ 琀愀欀攀猀 琀栀攀 爀攀猀甀氀琀 漀昀 愀 笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀 怀␀猀挀攀⸀琀爀甀猀琀䄀猀怀紀⠀⤀ 挀愀氀氀 愀渀搀 爀攀琀甀爀渀猀 琀栀攀ഀഀ
     * originally supplied value if the queried context type is a supertype of the created type.਍     ⨀ 䤀昀 琀栀椀猀 挀漀渀搀椀琀椀漀渀 椀猀渀✀琀 猀愀琀椀猀昀椀攀搀Ⰰ 琀栀爀漀眀猀 愀渀 攀砀挀攀瀀琀椀漀渀⸀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 琀礀瀀攀 吀栀攀 欀椀渀搀 漀昀 挀漀渀琀攀砀琀 椀渀 眀栀椀挀栀 琀栀椀猀 瘀愀氀甀攀 椀猀 琀漀 戀攀 甀猀攀搀⸀ഀഀ
     * @param {*} maybeTrusted The result of a prior {@link ng.$sce#methods_trustAs `$sce.trustAs`}਍     ⨀                         挀愀氀氀⸀ഀഀ
     * @returns {*} The value the was originally provided to਍     ⨀              笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀 怀␀猀挀攀⸀琀爀甀猀琀䄀猀怀紀 椀昀 瘀愀氀椀搀 椀渀 琀栀椀猀 挀漀渀琀攀砀琀⸀ഀഀ
     *              Otherwise, throws an exception.਍     ⨀⼀ഀഀ
਍    ⼀⨀⨀ഀഀ
     * @ngdoc method਍     ⨀ 䀀渀愀洀攀 渀最⸀␀猀挀攀⌀最攀琀吀爀甀猀琀攀搀䠀琀洀氀ഀഀ
     * @methodOf ng.$sce਍     ⨀ഀഀ
     * @description਍     ⨀ 匀栀漀爀琀栀愀渀搀 洀攀琀栀漀搀⸀  怀␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀䠀琀洀氀⠀瘀愀氀甀攀⤀怀 ﴀ﷿෿ഀ
     *     {@link ng.$sceDelegate#methods_getTrusted `$sceDelegate.getTrusted($sce.HTML, value)`}਍     ⨀ഀഀ
     * @param {*} value The value to pass to `$sce.getTrusted`.਍     ⨀ 䀀爀攀琀甀爀渀猀 笀⨀紀 吀栀攀 爀攀琀甀爀渀 瘀愀氀甀攀 漀昀 怀␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀⠀␀猀挀攀⸀䠀吀䴀䰀Ⰰ 瘀愀氀甀攀⤀怀ഀഀ
     */਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$sce#getTrustedCss਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Shorthand method.  `$sce.getTrustedCss(value)` ��਍     ⨀     笀䀀氀椀渀欀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀⌀洀攀琀栀漀搀猀开最攀琀吀爀甀猀琀攀搀 怀␀猀挀攀䐀攀氀攀最愀琀攀⸀最攀琀吀爀甀猀琀攀搀⠀␀猀挀攀⸀䌀匀匀Ⰰ 瘀愀氀甀攀⤀怀紀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀⨀紀 瘀愀氀甀攀 吀栀攀 瘀愀氀甀攀 琀漀 瀀愀猀猀 琀漀 怀␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀怀⸀ഀഀ
     * @returns {*} The return value of `$sce.getTrusted($sce.CSS, value)`਍     ⨀⼀ഀഀ
਍    ⼀⨀⨀ഀഀ
     * @ngdoc method਍     ⨀ 䀀渀愀洀攀 渀最⸀␀猀挀攀⌀最攀琀吀爀甀猀琀攀搀唀爀氀ഀഀ
     * @methodOf ng.$sce਍     ⨀ഀഀ
     * @description਍     ⨀ 匀栀漀爀琀栀愀渀搀 洀攀琀栀漀搀⸀  怀␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀唀爀氀⠀瘀愀氀甀攀⤀怀 ﴀ﷿෿ഀ
     *     {@link ng.$sceDelegate#methods_getTrusted `$sceDelegate.getTrusted($sce.URL, value)`}਍     ⨀ഀഀ
     * @param {*} value The value to pass to `$sce.getTrusted`.਍     ⨀ 䀀爀攀琀甀爀渀猀 笀⨀紀 吀栀攀 爀攀琀甀爀渀 瘀愀氀甀攀 漀昀 怀␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀⠀␀猀挀攀⸀唀刀䰀Ⰰ 瘀愀氀甀攀⤀怀ഀഀ
     */਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$sce#getTrustedResourceUrl਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Shorthand method.  `$sce.getTrustedResourceUrl(value)` ��਍     ⨀     笀䀀氀椀渀欀 渀最⸀␀猀挀攀䐀攀氀攀最愀琀攀⌀洀攀琀栀漀搀猀开最攀琀吀爀甀猀琀攀搀 怀␀猀挀攀䐀攀氀攀最愀琀攀⸀最攀琀吀爀甀猀琀攀搀⠀␀猀挀攀⸀刀䔀匀伀唀刀䌀䔀开唀刀䰀Ⰰ 瘀愀氀甀攀⤀怀紀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀⨀紀 瘀愀氀甀攀 吀栀攀 瘀愀氀甀攀 琀漀 瀀愀猀猀 琀漀 怀␀猀挀攀䐀攀氀攀最愀琀攀⸀最攀琀吀爀甀猀琀攀搀怀⸀ഀഀ
     * @returns {*} The return value of `$sce.getTrusted($sce.RESOURCE_URL, value)`਍     ⨀⼀ഀഀ
਍    ⼀⨀⨀ഀഀ
     * @ngdoc method਍     ⨀ 䀀渀愀洀攀 渀最⸀␀猀挀攀⌀最攀琀吀爀甀猀琀攀搀䨀猀ഀഀ
     * @methodOf ng.$sce਍     ⨀ഀഀ
     * @description਍     ⨀ 匀栀漀爀琀栀愀渀搀 洀攀琀栀漀搀⸀  怀␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀䨀猀⠀瘀愀氀甀攀⤀怀 ﴀ﷿෿ഀ
     *     {@link ng.$sceDelegate#methods_getTrusted `$sceDelegate.getTrusted($sce.JS, value)`}਍     ⨀ഀഀ
     * @param {*} value The value to pass to `$sce.getTrusted`.਍     ⨀ 䀀爀攀琀甀爀渀猀 笀⨀紀 吀栀攀 爀攀琀甀爀渀 瘀愀氀甀攀 漀昀 怀␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀⠀␀猀挀攀⸀䨀匀Ⰰ 瘀愀氀甀攀⤀怀ഀഀ
     */਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$sce#parseAsHtml਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Shorthand method.  `$sce.parseAsHtml(expression string)` ��਍     ⨀     笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开瀀愀爀猀攀 怀␀猀挀攀⸀瀀愀爀猀攀䄀猀⠀␀猀挀攀⸀䠀吀䴀䰀Ⰰ 瘀愀氀甀攀⤀怀紀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 攀砀瀀爀攀猀猀椀漀渀 匀琀爀椀渀最 攀砀瀀爀攀猀猀椀漀渀 琀漀 挀漀洀瀀椀氀攀⸀ഀഀ
     * @returns {function(context, locals)} a function which represents the compiled expression:਍     ⨀ഀഀ
     *    * `context` �� `{object}` �� an object against which any expressions embedded in the strings਍     ⨀      愀爀攀 攀瘀愀氀甀愀琀攀搀 愀最愀椀渀猀琀 ⠀琀礀瀀椀挀愀氀氀礀 愀 猀挀漀瀀攀 漀戀樀攀挀琀⤀⸀ഀഀ
     *    * `locals` �� `{object=}` �� local variables context object, useful for overriding values in਍     ⨀      怀挀漀渀琀攀砀琀怀⸀ഀഀ
     */਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$sce#parseAsCss਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Shorthand method.  `$sce.parseAsCss(value)` ��਍     ⨀     笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开瀀愀爀猀攀 怀␀猀挀攀⸀瀀愀爀猀攀䄀猀⠀␀猀挀攀⸀䌀匀匀Ⰰ 瘀愀氀甀攀⤀怀紀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 攀砀瀀爀攀猀猀椀漀渀 匀琀爀椀渀最 攀砀瀀爀攀猀猀椀漀渀 琀漀 挀漀洀瀀椀氀攀⸀ഀഀ
     * @returns {function(context, locals)} a function which represents the compiled expression:਍     ⨀ഀഀ
     *    * `context` �� `{object}` �� an object against which any expressions embedded in the strings਍     ⨀      愀爀攀 攀瘀愀氀甀愀琀攀搀 愀最愀椀渀猀琀 ⠀琀礀瀀椀挀愀氀氀礀 愀 猀挀漀瀀攀 漀戀樀攀挀琀⤀⸀ഀഀ
     *    * `locals` �� `{object=}` �� local variables context object, useful for overriding values in਍     ⨀      怀挀漀渀琀攀砀琀怀⸀ഀഀ
     */਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$sce#parseAsUrl਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Shorthand method.  `$sce.parseAsUrl(value)` ��਍     ⨀     笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开瀀愀爀猀攀 怀␀猀挀攀⸀瀀愀爀猀攀䄀猀⠀␀猀挀攀⸀唀刀䰀Ⰰ 瘀愀氀甀攀⤀怀紀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 攀砀瀀爀攀猀猀椀漀渀 匀琀爀椀渀最 攀砀瀀爀攀猀猀椀漀渀 琀漀 挀漀洀瀀椀氀攀⸀ഀഀ
     * @returns {function(context, locals)} a function which represents the compiled expression:਍     ⨀ഀഀ
     *    * `context` �� `{object}` �� an object against which any expressions embedded in the strings਍     ⨀      愀爀攀 攀瘀愀氀甀愀琀攀搀 愀最愀椀渀猀琀 ⠀琀礀瀀椀挀愀氀氀礀 愀 猀挀漀瀀攀 漀戀樀攀挀琀⤀⸀ഀഀ
     *    * `locals` �� `{object=}` �� local variables context object, useful for overriding values in਍     ⨀      怀挀漀渀琀攀砀琀怀⸀ഀഀ
     */਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$sce#parseAsResourceUrl਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Shorthand method.  `$sce.parseAsResourceUrl(value)` ��਍     ⨀     笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开瀀愀爀猀攀 怀␀猀挀攀⸀瀀愀爀猀攀䄀猀⠀␀猀挀攀⸀刀䔀匀伀唀刀䌀䔀开唀刀䰀Ⰰ 瘀愀氀甀攀⤀怀紀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 攀砀瀀爀攀猀猀椀漀渀 匀琀爀椀渀最 攀砀瀀爀攀猀猀椀漀渀 琀漀 挀漀洀瀀椀氀攀⸀ഀഀ
     * @returns {function(context, locals)} a function which represents the compiled expression:਍     ⨀ഀഀ
     *    * `context` �� `{object}` �� an object against which any expressions embedded in the strings਍     ⨀      愀爀攀 攀瘀愀氀甀愀琀攀搀 愀最愀椀渀猀琀 ⠀琀礀瀀椀挀愀氀氀礀 愀 猀挀漀瀀攀 漀戀樀攀挀琀⤀⸀ഀഀ
     *    * `locals` �� `{object=}` �� local variables context object, useful for overriding values in਍     ⨀      怀挀漀渀琀攀砀琀怀⸀ഀഀ
     */਍ഀഀ
    /**਍     ⨀ 䀀渀最搀漀挀 洀攀琀栀漀搀ഀഀ
     * @name ng.$sce#parseAsJs਍     ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀␀猀挀攀ഀഀ
     *਍     ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
     * Shorthand method.  `$sce.parseAsJs(value)` ��਍     ⨀     笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开瀀愀爀猀攀 怀␀猀挀攀⸀瀀愀爀猀攀䄀猀⠀␀猀挀攀⸀䨀匀Ⰰ 瘀愀氀甀攀⤀怀紀ഀഀ
     *਍     ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 攀砀瀀爀攀猀猀椀漀渀 匀琀爀椀渀最 攀砀瀀爀攀猀猀椀漀渀 琀漀 挀漀洀瀀椀氀攀⸀ഀഀ
     * @returns {function(context, locals)} a function which represents the compiled expression:਍     ⨀ഀഀ
     *    * `context` �� `{object}` �� an object against which any expressions embedded in the strings਍     ⨀      愀爀攀 攀瘀愀氀甀愀琀攀搀 愀最愀椀渀猀琀 ⠀琀礀瀀椀挀愀氀氀礀 愀 猀挀漀瀀攀 漀戀樀攀挀琀⤀⸀ഀഀ
     *    * `locals` �� `{object=}` �� local variables context object, useful for overriding values in਍     ⨀      怀挀漀渀琀攀砀琀怀⸀ഀഀ
     */਍ഀഀ
    // Shorthand delegations.਍    瘀愀爀 瀀愀爀猀攀 㴀 猀挀攀⸀瀀愀爀猀攀䄀猀Ⰰഀഀ
        getTrusted = sce.getTrusted,਍        琀爀甀猀琀䄀猀 㴀 猀挀攀⸀琀爀甀猀琀䄀猀㬀ഀഀ
਍    昀漀爀䔀愀挀栀⠀匀䌀䔀开䌀伀一吀䔀堀吀匀Ⰰ 昀甀渀挀琀椀漀渀 ⠀攀渀甀洀嘀愀氀甀攀Ⰰ 渀愀洀攀⤀ 笀ഀഀ
      var lName = lowercase(name);਍      猀挀攀嬀挀愀洀攀氀䌀愀猀攀⠀∀瀀愀爀猀攀开愀猀开∀ ⬀ 氀一愀洀攀⤀崀 㴀 昀甀渀挀琀椀漀渀 ⠀攀砀瀀爀⤀ 笀ഀഀ
        return parse(enumValue, expr);਍      紀㬀ഀഀ
      sce[camelCase("get_trusted_" + lName)] = function (value) {਍        爀攀琀甀爀渀 最攀琀吀爀甀猀琀攀搀⠀攀渀甀洀嘀愀氀甀攀Ⰰ 瘀愀氀甀攀⤀㬀ഀഀ
      };਍      猀挀攀嬀挀愀洀攀氀䌀愀猀攀⠀∀琀爀甀猀琀开愀猀开∀ ⬀ 氀一愀洀攀⤀崀 㴀 昀甀渀挀琀椀漀渀 ⠀瘀愀氀甀攀⤀ 笀ഀഀ
        return trustAs(enumValue, value);਍      紀㬀ഀഀ
    });਍ഀഀ
    return sce;਍  紀崀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ ℀℀℀ 吀栀椀猀 椀猀 愀渀 甀渀搀漀挀甀洀攀渀琀攀搀 ∀瀀爀椀瘀愀琀攀∀ 猀攀爀瘀椀挀攀 ℀℀℀ഀഀ
 *਍ ⨀ 䀀渀愀洀攀 渀最⸀␀猀渀椀昀昀攀爀ഀഀ
 * @requires $window਍ ⨀ 䀀爀攀焀甀椀爀攀猀 ␀搀漀挀甀洀攀渀琀ഀഀ
 *਍ ⨀ 䀀瀀爀漀瀀攀爀琀礀 笀戀漀漀氀攀愀渀紀 栀椀猀琀漀爀礀 䐀漀攀猀 琀栀攀 戀爀漀眀猀攀爀 猀甀瀀瀀漀爀琀 栀琀洀氀㔀 栀椀猀琀漀爀礀 愀瀀椀 㼀ഀഀ
 * @property {boolean} hashchange Does the browser support hashchange event ?਍ ⨀ 䀀瀀爀漀瀀攀爀琀礀 笀戀漀漀氀攀愀渀紀 琀爀愀渀猀椀琀椀漀渀猀 䐀漀攀猀 琀栀攀 戀爀漀眀猀攀爀 猀甀瀀瀀漀爀琀 䌀匀匀 琀爀愀渀猀椀琀椀漀渀 攀瘀攀渀琀猀 㼀ഀഀ
 * @property {boolean} animations Does the browser support CSS animation events ?਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀椀猀 椀猀 瘀攀爀礀 猀椀洀瀀氀攀 椀洀瀀氀攀洀攀渀琀愀琀椀漀渀 漀昀 琀攀猀琀椀渀最 戀爀漀眀猀攀爀✀猀 昀攀愀琀甀爀攀猀⸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 ␀匀渀椀昀昀攀爀倀爀漀瘀椀搀攀爀⠀⤀ 笀ഀഀ
  this.$get = ['$window', '$document', function($window, $document) {਍    瘀愀爀 攀瘀攀渀琀匀甀瀀瀀漀爀琀 㴀 笀紀Ⰰഀഀ
        android =਍          椀渀琀⠀⠀⼀愀渀搀爀漀椀搀 ⠀尀搀⬀⤀⼀⸀攀砀攀挀⠀氀漀眀攀爀挀愀猀攀⠀⠀␀眀椀渀搀漀眀⸀渀愀瘀椀最愀琀漀爀 簀簀 笀紀⤀⸀甀猀攀爀䄀最攀渀琀⤀⤀ 簀簀 嬀崀⤀嬀㄀崀⤀Ⰰഀഀ
        boxee = /Boxee/i.test(($window.navigator || {}).userAgent),਍        搀漀挀甀洀攀渀琀 㴀 ␀搀漀挀甀洀攀渀琀嬀　崀 簀簀 笀紀Ⰰഀഀ
        documentMode = document.documentMode,਍        瘀攀渀搀漀爀倀爀攀昀椀砀Ⰰഀഀ
        vendorRegex = /^(Moz|webkit|O|ms)(?=[A-Z])/,਍        戀漀搀礀匀琀礀氀攀 㴀 搀漀挀甀洀攀渀琀⸀戀漀搀礀 ☀☀ 搀漀挀甀洀攀渀琀⸀戀漀搀礀⸀猀琀礀氀攀Ⰰഀഀ
        transitions = false,਍        愀渀椀洀愀琀椀漀渀猀 㴀 昀愀氀猀攀Ⰰഀഀ
        match;਍ഀഀ
    if (bodyStyle) {਍      昀漀爀⠀瘀愀爀 瀀爀漀瀀 椀渀 戀漀搀礀匀琀礀氀攀⤀ 笀ഀഀ
        if(match = vendorRegex.exec(prop)) {਍          瘀攀渀搀漀爀倀爀攀昀椀砀 㴀 洀愀琀挀栀嬀　崀㬀ഀഀ
          vendorPrefix = vendorPrefix.substr(0, 1).toUpperCase() + vendorPrefix.substr(1);਍          戀爀攀愀欀㬀ഀഀ
        }਍      紀ഀഀ
਍      椀昀⠀℀瘀攀渀搀漀爀倀爀攀昀椀砀⤀ 笀ഀഀ
        vendorPrefix = ('WebkitOpacity' in bodyStyle) && 'webkit';਍      紀ഀഀ
਍      琀爀愀渀猀椀琀椀漀渀猀 㴀 ℀℀⠀⠀✀琀爀愀渀猀椀琀椀漀渀✀ 椀渀 戀漀搀礀匀琀礀氀攀⤀ 簀簀 ⠀瘀攀渀搀漀爀倀爀攀昀椀砀 ⬀ ✀吀爀愀渀猀椀琀椀漀渀✀ 椀渀 戀漀搀礀匀琀礀氀攀⤀⤀㬀ഀഀ
      animations  = !!(('animation' in bodyStyle) || (vendorPrefix + 'Animation' in bodyStyle));਍ഀഀ
      if (android && (!transitions||!animations)) {਍        琀爀愀渀猀椀琀椀漀渀猀 㴀 椀猀匀琀爀椀渀最⠀搀漀挀甀洀攀渀琀⸀戀漀搀礀⸀猀琀礀氀攀⸀眀攀戀欀椀琀吀爀愀渀猀椀琀椀漀渀⤀㬀ഀഀ
        animations = isString(document.body.style.webkitAnimation);਍      紀ഀഀ
    }਍ഀഀ
਍    爀攀琀甀爀渀 笀ഀഀ
      // Android has history.pushState, but it does not update location correctly਍      ⼀⼀ 猀漀 氀攀琀✀猀 渀漀琀 甀猀攀 琀栀攀 栀椀猀琀漀爀礀 䄀倀䤀 愀琀 愀氀氀⸀ഀഀ
      // http://code.google.com/p/android/issues/detail?id=17471਍      ⼀⼀ 栀琀琀瀀猀㨀⼀⼀最椀琀栀甀戀⸀挀漀洀⼀愀渀最甀氀愀爀⼀愀渀最甀氀愀爀⸀樀猀⼀椀猀猀甀攀猀⼀㤀　㐀ഀഀ
਍      ⼀⼀ 漀氀搀攀爀 眀攀戀欀椀琀 戀爀漀眀猀攀爀 ⠀㔀㌀㌀⸀㤀⤀ 漀渀 䈀漀砀攀攀 戀漀砀 栀愀猀 攀砀愀挀琀氀礀 琀栀攀 猀愀洀攀 瀀爀漀戀氀攀洀 愀猀 䄀渀搀爀漀椀搀 栀愀猀ഀഀ
      // so let's not use the history API also਍      ⼀⼀ 圀攀 愀爀攀 瀀甀爀瀀漀猀攀昀甀氀氀礀 甀猀椀渀最 怀℀⠀愀渀搀爀漀椀搀 㰀 㐀⤀怀 琀漀 挀漀瘀攀爀 琀栀攀 挀愀猀攀 眀栀攀渀 怀愀渀搀爀漀椀搀怀 椀猀 甀渀搀攀昀椀渀攀搀ഀഀ
      // jshint -W018਍      栀椀猀琀漀爀礀㨀 ℀℀⠀␀眀椀渀搀漀眀⸀栀椀猀琀漀爀礀 ☀☀ ␀眀椀渀搀漀眀⸀栀椀猀琀漀爀礀⸀瀀甀猀栀匀琀愀琀攀 ☀☀ ℀⠀愀渀搀爀漀椀搀 㰀 㐀⤀ ☀☀ ℀戀漀砀攀攀⤀Ⰰഀഀ
      // jshint +W018਍      栀愀猀栀挀栀愀渀最攀㨀 ✀漀渀栀愀猀栀挀栀愀渀最攀✀ 椀渀 ␀眀椀渀搀漀眀 ☀☀ഀഀ
                  // IE8 compatible mode lies਍                  ⠀℀搀漀挀甀洀攀渀琀䴀漀搀攀 簀簀 搀漀挀甀洀攀渀琀䴀漀搀攀 㸀 㜀⤀Ⰰഀഀ
      hasEvent: function(event) {਍        ⼀⼀ 䤀䔀㤀 椀洀瀀氀攀洀攀渀琀猀 ✀椀渀瀀甀琀✀ 攀瘀攀渀琀 椀琀✀猀 猀漀 昀甀戀愀爀攀搀 琀栀愀琀 眀攀 爀愀琀栀攀爀 瀀爀攀琀攀渀搀 琀栀愀琀 椀琀 搀漀攀猀渀✀琀 栀愀瘀攀ഀഀ
        // it. In particular the event is not fired when backspace or delete key are pressed or਍        ⼀⼀ 眀栀攀渀 挀甀琀 漀瀀攀爀愀琀椀漀渀 椀猀 瀀攀爀昀漀爀洀攀搀⸀ഀഀ
        if (event == 'input' && msie == 9) return false;਍ഀഀ
        if (isUndefined(eventSupport[event])) {਍          瘀愀爀 搀椀瘀䔀氀洀 㴀 搀漀挀甀洀攀渀琀⸀挀爀攀愀琀攀䔀氀攀洀攀渀琀⠀✀搀椀瘀✀⤀㬀ഀഀ
          eventSupport[event] = 'on' + event in divElm;਍        紀ഀഀ
਍        爀攀琀甀爀渀 攀瘀攀渀琀匀甀瀀瀀漀爀琀嬀攀瘀攀渀琀崀㬀ഀഀ
      },਍      挀猀瀀㨀 挀猀瀀⠀⤀Ⰰഀഀ
      vendorPrefix: vendorPrefix,਍      琀爀愀渀猀椀琀椀漀渀猀 㨀 琀爀愀渀猀椀琀椀漀渀猀Ⰰഀഀ
      animations : animations,਍      愀渀搀爀漀椀搀㨀 愀渀搀爀漀椀搀Ⰰഀഀ
      msie : msie,਍      洀猀椀攀䐀漀挀甀洀攀渀琀䴀漀搀攀㨀 搀漀挀甀洀攀渀琀䴀漀搀攀ഀഀ
    };਍  紀崀㬀ഀഀ
}਍ഀഀ
function $TimeoutProvider() {਍  琀栀椀猀⸀␀最攀琀 㴀 嬀✀␀爀漀漀琀匀挀漀瀀攀✀Ⰰ ✀␀戀爀漀眀猀攀爀✀Ⰰ ✀␀焀✀Ⰰ ✀␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀✀Ⰰഀഀ
       function($rootScope,   $browser,   $q,   $exceptionHandler) {਍    瘀愀爀 搀攀昀攀爀爀攀搀猀 㴀 笀紀㬀ഀഀ
਍ഀഀ
     /**਍      ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
      * @name ng.$timeout਍      ⨀ 䀀爀攀焀甀椀爀攀猀 ␀戀爀漀眀猀攀爀ഀഀ
      *਍      ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
      * Angular's wrapper for `window.setTimeout`. The `fn` function is wrapped into a try/catch਍      ⨀ 戀氀漀挀欀 愀渀搀 搀攀氀攀最愀琀攀猀 愀渀礀 攀砀挀攀瀀琀椀漀渀猀 琀漀ഀഀ
      * {@link ng.$exceptionHandler $exceptionHandler} service.਍      ⨀ഀഀ
      * The return value of registering a timeout function is a promise, which will be resolved when਍      ⨀ 琀栀攀 琀椀洀攀漀甀琀 椀猀 爀攀愀挀栀攀搀 愀渀搀 琀栀攀 琀椀洀攀漀甀琀 昀甀渀挀琀椀漀渀 椀猀 攀砀攀挀甀琀攀搀⸀ഀഀ
      *਍      ⨀ 吀漀 挀愀渀挀攀氀 愀 琀椀洀攀漀甀琀 爀攀焀甀攀猀琀Ⰰ 挀愀氀氀 怀␀琀椀洀攀漀甀琀⸀挀愀渀挀攀氀⠀瀀爀漀洀椀猀攀⤀怀⸀ഀഀ
      *਍      ⨀ 䤀渀 琀攀猀琀猀 礀漀甀 挀愀渀 甀猀攀 笀䀀氀椀渀欀 渀最䴀漀挀欀⸀␀琀椀洀攀漀甀琀 怀␀琀椀洀攀漀甀琀⸀昀氀甀猀栀⠀⤀怀紀 琀漀ഀഀ
      * synchronously flush the queue of deferred functions.਍      ⨀ഀഀ
      * @param {function()} fn A function, whose execution should be delayed.਍      ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀㴀紀 嬀搀攀氀愀礀㴀　崀 䐀攀氀愀礀 椀渀 洀椀氀氀椀猀攀挀漀渀搀猀⸀ഀഀ
      * @param {boolean=} [invokeApply=true] If set to `false` skips model dirty checking, otherwise਍      ⨀   眀椀氀氀 椀渀瘀漀欀攀 怀昀渀怀 眀椀琀栀椀渀 琀栀攀 笀䀀氀椀渀欀 渀最⸀␀爀漀漀琀匀挀漀瀀攀⸀匀挀漀瀀攀⌀洀攀琀栀漀搀猀开␀愀瀀瀀氀礀 ␀愀瀀瀀氀礀紀 戀氀漀挀欀⸀ഀഀ
      * @returns {Promise} Promise that will be resolved when the timeout is reached. The value this਍      ⨀   瀀爀漀洀椀猀攀 眀椀氀氀 戀攀 爀攀猀漀氀瘀攀搀 眀椀琀栀 椀猀 琀栀攀 爀攀琀甀爀渀 瘀愀氀甀攀 漀昀 琀栀攀 怀昀渀怀 昀甀渀挀琀椀漀渀⸀ഀഀ
      * ਍      ⨀⼀ഀഀ
    function timeout(fn, delay, invokeApply) {਍      瘀愀爀 搀攀昀攀爀爀攀搀 㴀 ␀焀⸀搀攀昀攀爀⠀⤀Ⰰഀഀ
          promise = deferred.promise,਍          猀欀椀瀀䄀瀀瀀氀礀 㴀 ⠀椀猀䐀攀昀椀渀攀搀⠀椀渀瘀漀欀攀䄀瀀瀀氀礀⤀ ☀☀ ℀椀渀瘀漀欀攀䄀瀀瀀氀礀⤀Ⰰഀഀ
          timeoutId;਍ഀഀ
      timeoutId = $browser.defer(function() {਍        琀爀礀 笀ഀഀ
          deferred.resolve(fn());਍        紀 挀愀琀挀栀⠀攀⤀ 笀ഀഀ
          deferred.reject(e);਍          ␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀⠀攀⤀㬀ഀഀ
        }਍        昀椀渀愀氀氀礀 笀ഀഀ
          delete deferreds[promise.$$timeoutId];਍        紀ഀഀ
਍        椀昀 ⠀℀猀欀椀瀀䄀瀀瀀氀礀⤀ ␀爀漀漀琀匀挀漀瀀攀⸀␀愀瀀瀀氀礀⠀⤀㬀ഀഀ
      }, delay);਍ഀഀ
      promise.$$timeoutId = timeoutId;਍      搀攀昀攀爀爀攀搀猀嬀琀椀洀攀漀甀琀䤀搀崀 㴀 搀攀昀攀爀爀攀搀㬀ഀഀ
਍      爀攀琀甀爀渀 瀀爀漀洀椀猀攀㬀ഀഀ
    }਍ഀഀ
਍     ⼀⨀⨀ഀഀ
      * @ngdoc function਍      ⨀ 䀀渀愀洀攀 渀最⸀␀琀椀洀攀漀甀琀⌀挀愀渀挀攀氀ഀഀ
      * @methodOf ng.$timeout਍      ⨀ഀഀ
      * @description਍      ⨀ 䌀愀渀挀攀氀猀 愀 琀愀猀欀 愀猀猀漀挀椀愀琀攀搀 眀椀琀栀 琀栀攀 怀瀀爀漀洀椀猀攀怀⸀ 䄀猀 愀 爀攀猀甀氀琀 漀昀 琀栀椀猀Ⰰ 琀栀攀 瀀爀漀洀椀猀攀 眀椀氀氀 戀攀ഀഀ
      * resolved with a rejection.਍      ⨀ഀഀ
      * @param {Promise=} promise Promise returned by the `$timeout` function.਍      ⨀ 䀀爀攀琀甀爀渀猀 笀戀漀漀氀攀愀渀紀 刀攀琀甀爀渀猀 怀琀爀甀攀怀 椀昀 琀栀攀 琀愀猀欀 栀愀猀渀✀琀 攀砀攀挀甀琀攀搀 礀攀琀 愀渀搀 眀愀猀 猀甀挀挀攀猀猀昀甀氀氀礀ഀഀ
      *   canceled.਍      ⨀⼀ഀഀ
    timeout.cancel = function(promise) {਍      椀昀 ⠀瀀爀漀洀椀猀攀 ☀☀ 瀀爀漀洀椀猀攀⸀␀␀琀椀洀攀漀甀琀䤀搀 椀渀 搀攀昀攀爀爀攀搀猀⤀ 笀ഀഀ
        deferreds[promise.$$timeoutId].reject('canceled');਍        搀攀氀攀琀攀 搀攀昀攀爀爀攀搀猀嬀瀀爀漀洀椀猀攀⸀␀␀琀椀洀攀漀甀琀䤀搀崀㬀ഀഀ
        return $browser.defer.cancel(promise.$$timeoutId);਍      紀ഀഀ
      return false;਍    紀㬀ഀഀ
਍    爀攀琀甀爀渀 琀椀洀攀漀甀琀㬀ഀഀ
  }];਍紀ഀഀ
਍⼀⼀ 一伀吀䔀㨀  吀栀攀 甀猀愀最攀 漀昀 眀椀渀搀漀眀 愀渀搀 搀漀挀甀洀攀渀琀 椀渀猀琀攀愀搀 漀昀 ␀眀椀渀搀漀眀 愀渀搀 ␀搀漀挀甀洀攀渀琀 栀攀爀攀 椀猀ഀഀ
// deliberate.  This service depends on the specific behavior of anchor nodes created by the਍⼀⼀ 戀爀漀眀猀攀爀 ⠀爀攀猀漀氀瘀椀渀最 愀渀搀 瀀愀爀猀椀渀最 唀刀䰀猀⤀ 琀栀愀琀 椀猀 甀渀氀椀欀攀氀礀 琀漀 戀攀 瀀爀漀瘀椀搀攀搀 戀礀 洀漀挀欀 漀戀樀攀挀琀猀 愀渀搀ഀഀ
// cause us to break tests.  In addition, when the browser resolves a URL for XHR, it਍⼀⼀ 搀漀攀猀渀✀琀 欀渀漀眀 愀戀漀甀琀 洀漀挀欀攀搀 氀漀挀愀琀椀漀渀猀 愀渀搀 爀攀猀漀氀瘀攀猀 唀刀䰀猀 琀漀 琀栀攀 爀攀愀氀 搀漀挀甀洀攀渀琀 ⴀ 眀栀椀挀栀 椀猀ഀഀ
// exactly the behavior needed here.  There is little value is mocking these out for this਍⼀⼀ 猀攀爀瘀椀挀攀⸀ഀഀ
var urlParsingNode = document.createElement("a");਍瘀愀爀 漀爀椀最椀渀唀爀氀 㴀 甀爀氀刀攀猀漀氀瘀攀⠀眀椀渀搀漀眀⸀氀漀挀愀琀椀漀渀⸀栀爀攀昀Ⰰ 琀爀甀攀⤀㬀ഀഀ
਍ഀഀ
/**਍ ⨀ഀഀ
 * Implementation Notes for non-IE browsers਍ ⨀ ⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀഀഀ
 * Assigning a URL to the href property of an anchor DOM node, even one attached to the DOM,਍ ⨀ 爀攀猀甀氀琀猀 戀漀琀栀 椀渀 琀栀攀 渀漀爀洀愀氀椀稀椀渀最 愀渀搀 瀀愀爀猀椀渀最 漀昀 琀栀攀 唀刀䰀⸀  一漀爀洀愀氀椀稀椀渀最 洀攀愀渀猀 琀栀愀琀 愀 爀攀氀愀琀椀瘀攀ഀഀ
 * URL will be resolved into an absolute URL in the context of the application document.਍ ⨀ 倀愀爀猀椀渀最 洀攀愀渀猀 琀栀愀琀 琀栀攀 愀渀挀栀漀爀 渀漀搀攀✀猀 栀漀猀琀Ⰰ 栀漀猀琀渀愀洀攀Ⰰ 瀀爀漀琀漀挀漀氀Ⰰ 瀀漀爀琀Ⰰ 瀀愀琀栀渀愀洀攀 愀渀搀 爀攀氀愀琀攀搀ഀഀ
 * properties are all populated to reflect the normalized URL.  This approach has wide਍ ⨀ 挀漀洀瀀愀琀椀戀椀氀椀琀礀 ⴀ 匀愀昀愀爀椀 ㄀⬀Ⰰ 䴀漀稀椀氀氀愀 ㄀⬀Ⰰ 伀瀀攀爀愀 㜀⬀Ⰰ攀 攀琀挀⸀  匀攀攀ഀഀ
 * http://www.aptana.com/reference/html/api/HTMLAnchorElement.html਍ ⨀ഀഀ
 * Implementation Notes for IE਍ ⨀ ⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀഀഀ
 * IE >= 8 and <= 10 normalizes the URL when assigned to the anchor node similar to the other਍ ⨀ 戀爀漀眀猀攀爀猀⸀  䠀漀眀攀瘀攀爀Ⰰ 琀栀攀 瀀愀爀猀攀搀 挀漀洀瀀漀渀攀渀琀猀 眀椀氀氀 渀漀琀 戀攀 猀攀琀 椀昀 琀栀攀 唀刀䰀 愀猀猀椀最渀攀搀 搀椀搀 渀漀琀 猀瀀攀挀椀昀礀ഀഀ
 * them.  (e.g. if you assign a.href = "foo", then a.protocol, a.host, etc. will be empty.)  We਍ ⨀ 眀漀爀欀 愀爀漀甀渀搀 琀栀愀琀 戀礀 瀀攀爀昀漀爀洀椀渀最 琀栀攀 瀀愀爀猀椀渀最 椀渀 愀 ㈀渀搀 猀琀攀瀀 戀礀 琀愀欀椀渀最 愀 瀀爀攀瘀椀漀甀猀氀礀 渀漀爀洀愀氀椀稀攀搀ഀഀ
 * URL (e.g. by assigning to a.href) and assigning it a.href again.  This correctly populates the਍ ⨀ 瀀爀漀瀀攀爀琀椀攀猀 猀甀挀栀 愀猀 瀀爀漀琀漀挀漀氀Ⰰ 栀漀猀琀渀愀洀攀Ⰰ 瀀漀爀琀Ⰰ 攀琀挀⸀ഀഀ
 *਍ ⨀ 䤀䔀㜀 搀漀攀猀 渀漀琀 渀漀爀洀愀氀椀稀攀 琀栀攀 唀刀䰀 眀栀攀渀 愀猀猀椀最渀攀搀 琀漀 愀渀 愀渀挀栀漀爀 渀漀搀攀⸀  ⠀䄀瀀瀀愀爀攀渀琀氀礀Ⰰ 椀琀 搀漀攀猀Ⰰ 椀昀 漀渀攀ഀഀ
 * uses the inner HTML approach to assign the URL as part of an HTML snippet -਍ ⨀ 栀琀琀瀀㨀⼀⼀猀琀愀挀欀漀瘀攀爀昀氀漀眀⸀挀漀洀⼀愀⼀㐀㜀㈀㜀㈀㤀⤀  䠀漀眀攀瘀攀爀Ⰰ 猀攀琀琀椀渀最 椀洀最嬀猀爀挀崀 搀漀攀猀 渀漀爀洀愀氀椀稀攀 琀栀攀 唀刀䰀⸀ഀഀ
 * Unfortunately, setting img[src] to something like "javascript:foo" on IE throws an exception.਍ ⨀ 匀椀渀挀攀 琀栀攀 瀀爀椀洀愀爀礀 甀猀愀最攀 昀漀爀 渀漀爀洀愀氀椀稀椀渀最 唀刀䰀猀 椀猀 琀漀 猀愀渀椀琀椀稀攀 猀甀挀栀 唀刀䰀猀Ⰰ 眀攀 挀愀渀✀琀 甀猀攀 琀栀愀琀ഀഀ
 * method and IE < 8 is unsupported.਍ ⨀ഀഀ
 * References:਍ ⨀   栀琀琀瀀㨀⼀⼀搀攀瘀攀氀漀瀀攀爀⸀洀漀稀椀氀氀愀⸀漀爀最⼀攀渀ⴀ唀匀⼀搀漀挀猀⼀圀攀戀⼀䄀倀䤀⼀䠀吀䴀䰀䄀渀挀栀漀爀䔀氀攀洀攀渀琀ഀഀ
 *   http://www.aptana.com/reference/html/api/HTMLAnchorElement.html਍ ⨀   栀琀琀瀀㨀⼀⼀甀爀氀⸀猀瀀攀挀⸀眀栀愀琀眀最⸀漀爀最⼀⌀甀爀氀甀琀椀氀猀ഀഀ
 *   https://github.com/angular/angular.js/pull/2902਍ ⨀   栀琀琀瀀㨀⼀⼀樀愀洀攀猀⸀瀀愀搀漀氀猀攀礀⸀挀漀洀⼀樀愀瘀愀猀挀爀椀瀀琀⼀瀀愀爀猀椀渀最ⴀ甀爀氀猀ⴀ眀椀琀栀ⴀ琀栀攀ⴀ搀漀洀⼀ഀഀ
 *਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 * @param {string} url The URL to be parsed.਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀 一漀爀洀愀氀椀稀攀猀 愀渀搀 瀀愀爀猀攀猀 愀 唀刀䰀⸀ഀഀ
 * @returns {object} Returns the normalized URL as a dictionary.਍ ⨀ഀഀ
 *   | member name   | Description    |਍ ⨀   簀ⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀ簀ⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀ簀ഀഀ
 *   | href          | A normalized version of the provided URL if it was not an absolute URL |਍ ⨀   簀 瀀爀漀琀漀挀漀氀      簀 吀栀攀 瀀爀漀琀漀挀漀氀 椀渀挀氀甀搀椀渀最 琀栀攀 琀爀愀椀氀椀渀最 挀漀氀漀渀                              簀ഀഀ
 *   | host          | The host and port (if the port is non-default) of the normalizedUrl    |਍ ⨀   簀 猀攀愀爀挀栀        簀 吀栀攀 猀攀愀爀挀栀 瀀愀爀愀洀猀Ⰰ 洀椀渀甀猀 琀栀攀 焀甀攀猀琀椀漀渀 洀愀爀欀                             簀ഀഀ
 *   | hash          | The hash string, minus the hash symbol਍ ⨀   簀 栀漀猀琀渀愀洀攀      簀 吀栀攀 栀漀猀琀渀愀洀攀ഀഀ
 *   | port          | The port, without ":"਍ ⨀   簀 瀀愀琀栀渀愀洀攀      簀 吀栀攀 瀀愀琀栀渀愀洀攀Ⰰ 戀攀最椀渀渀椀渀最 眀椀琀栀 ∀⼀∀ഀഀ
 *਍ ⨀⼀ഀഀ
function urlResolve(url, base) {਍  瘀愀爀 栀爀攀昀 㴀 甀爀氀㬀ഀഀ
਍  椀昀 ⠀洀猀椀攀⤀ 笀ഀഀ
    // Normalize before parse.  Refer Implementation Notes on why this is਍    ⼀⼀ 搀漀渀攀 椀渀 琀眀漀 猀琀攀瀀猀 漀渀 䤀䔀⸀ഀഀ
    urlParsingNode.setAttribute("href", href);਍    栀爀攀昀 㴀 甀爀氀倀愀爀猀椀渀最一漀搀攀⸀栀爀攀昀㬀ഀഀ
  }਍ഀഀ
  urlParsingNode.setAttribute('href', href);਍ഀഀ
  // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils਍  爀攀琀甀爀渀 笀ഀഀ
    href: urlParsingNode.href,਍    瀀爀漀琀漀挀漀氀㨀 甀爀氀倀愀爀猀椀渀最一漀搀攀⸀瀀爀漀琀漀挀漀氀 㼀 甀爀氀倀愀爀猀椀渀最一漀搀攀⸀瀀爀漀琀漀挀漀氀⸀爀攀瀀氀愀挀攀⠀⼀㨀␀⼀Ⰰ ✀✀⤀ 㨀 ✀✀Ⰰഀഀ
    host: urlParsingNode.host,਍    猀攀愀爀挀栀㨀 甀爀氀倀愀爀猀椀渀最一漀搀攀⸀猀攀愀爀挀栀 㼀 甀爀氀倀愀爀猀椀渀最一漀搀攀⸀猀攀愀爀挀栀⸀爀攀瀀氀愀挀攀⠀⼀帀尀㼀⼀Ⰰ ✀✀⤀ 㨀 ✀✀Ⰰഀഀ
    hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',਍    栀漀猀琀渀愀洀攀㨀 甀爀氀倀愀爀猀椀渀最一漀搀攀⸀栀漀猀琀渀愀洀攀Ⰰഀഀ
    port: urlParsingNode.port,਍    瀀愀琀栀渀愀洀攀㨀 ⠀甀爀氀倀愀爀猀椀渀最一漀搀攀⸀瀀愀琀栀渀愀洀攀⸀挀栀愀爀䄀琀⠀　⤀ 㴀㴀㴀 ✀⼀✀⤀ഀഀ
      ? urlParsingNode.pathname਍      㨀 ✀⼀✀ ⬀ 甀爀氀倀愀爀猀椀渀最一漀搀攀⸀瀀愀琀栀渀愀洀攀ഀഀ
  };਍紀ഀഀ
਍⼀⨀⨀ഀഀ
 * Parse a request URL and determine whether this is a same-origin request as the application document.਍ ⨀ഀഀ
 * @param {string|object} requestUrl The url of the request as a string that will be resolved਍ ⨀ 漀爀 愀 瀀愀爀猀攀搀 唀刀䰀 漀戀樀攀挀琀⸀ഀഀ
 * @returns {boolean} Whether the request is for the same origin as the application document.਍ ⨀⼀ഀഀ
function urlIsSameOrigin(requestUrl) {਍  瘀愀爀 瀀愀爀猀攀搀 㴀 ⠀椀猀匀琀爀椀渀最⠀爀攀焀甀攀猀琀唀爀氀⤀⤀ 㼀 甀爀氀刀攀猀漀氀瘀攀⠀爀攀焀甀攀猀琀唀爀氀⤀ 㨀 爀攀焀甀攀猀琀唀爀氀㬀ഀഀ
  return (parsed.protocol === originUrl.protocol &&਍          瀀愀爀猀攀搀⸀栀漀猀琀 㴀㴀㴀 漀爀椀最椀渀唀爀氀⸀栀漀猀琀⤀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name ng.$window਍ ⨀ഀഀ
 * @description਍ ⨀ 䄀 爀攀昀攀爀攀渀挀攀 琀漀 琀栀攀 戀爀漀眀猀攀爀✀猀 怀眀椀渀搀漀眀怀 漀戀樀攀挀琀⸀ 圀栀椀氀攀 怀眀椀渀搀漀眀怀ഀഀ
 * is globally available in JavaScript, it causes testability problems, because਍ ⨀ 椀琀 椀猀 愀 最氀漀戀愀氀 瘀愀爀椀愀戀氀攀⸀ 䤀渀 愀渀最甀氀愀爀 眀攀 愀氀眀愀礀猀 爀攀昀攀爀 琀漀 椀琀 琀栀爀漀甀最栀 琀栀攀ഀഀ
 * `$window` service, so it may be overridden, removed or mocked for testing.਍ ⨀ഀഀ
 * Expressions, like the one defined for the `ngClick` directive in the example਍ ⨀ 戀攀氀漀眀Ⰰ 愀爀攀 攀瘀愀氀甀愀琀攀搀 眀椀琀栀 爀攀猀瀀攀挀琀 琀漀 琀栀攀 挀甀爀爀攀渀琀 猀挀漀瀀攀⸀  吀栀攀爀攀昀漀爀攀Ⰰ 琀栀攀爀攀 椀猀ഀഀ
 * no risk of inadvertently coding in a dependency on a global value in such an਍ ⨀ 攀砀瀀爀攀猀猀椀漀渀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
   <doc:example>਍     㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
       <script>਍         昀甀渀挀琀椀漀渀 䌀琀爀氀⠀␀猀挀漀瀀攀Ⰰ ␀眀椀渀搀漀眀⤀ 笀ഀഀ
           $scope.greeting = 'Hello, World!';਍           ␀猀挀漀瀀攀⸀搀漀䜀爀攀攀琀椀渀最 㴀 昀甀渀挀琀椀漀渀⠀最爀攀攀琀椀渀最⤀ 笀ഀഀ
               $window.alert(greeting);਍           紀㬀ഀഀ
         }਍       㰀⼀猀挀爀椀瀀琀㸀ഀഀ
       <div ng-controller="Ctrl">਍         㰀椀渀瀀甀琀 琀礀瀀攀㴀∀琀攀砀琀∀ 渀最ⴀ洀漀搀攀氀㴀∀最爀攀攀琀椀渀最∀ ⼀㸀ഀഀ
         <button ng-click="doGreeting(greeting)">ALERT</button>਍       㰀⼀搀椀瘀㸀ഀഀ
     </doc:source>਍     㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
      it('should display the greeting in the input box', function() {਍       椀渀瀀甀琀⠀✀最爀攀攀琀椀渀最✀⤀⸀攀渀琀攀爀⠀✀䠀攀氀氀漀Ⰰ 䔀㈀䔀 吀攀猀琀猀✀⤀㬀ഀഀ
       // If we click the button it will block the test runner਍       ⼀⼀ 攀氀攀洀攀渀琀⠀✀㨀戀甀琀琀漀渀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
      });਍     㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
   </doc:example>਍ ⨀⼀ഀഀ
function $WindowProvider(){਍  琀栀椀猀⸀␀最攀琀 㴀 瘀愀氀甀攀䘀渀⠀眀椀渀搀漀眀⤀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name ng.$filterProvider਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 *਍ ⨀ 䘀椀氀琀攀爀猀 愀爀攀 樀甀猀琀 昀甀渀挀琀椀漀渀猀 眀栀椀挀栀 琀爀愀渀猀昀漀爀洀 椀渀瀀甀琀 琀漀 愀渀 漀甀琀瀀甀琀⸀ 䠀漀眀攀瘀攀爀 昀椀氀琀攀爀猀 渀攀攀搀 琀漀 戀攀ഀഀ
 * Dependency Injected. To achieve this a filter definition consists of a factory function which is਍ ⨀ 愀渀渀漀琀愀琀攀搀 眀椀琀栀 搀攀瀀攀渀搀攀渀挀椀攀猀 愀渀搀 椀猀 爀攀猀瀀漀渀猀椀戀氀攀 昀漀爀 挀爀攀愀琀椀渀最 愀 昀椀氀琀攀爀 昀甀渀挀琀椀漀渀⸀ഀഀ
 *਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   // Filter registration਍ ⨀   昀甀渀挀琀椀漀渀 䴀礀䴀漀搀甀氀攀⠀␀瀀爀漀瘀椀搀攀Ⰰ ␀昀椀氀琀攀爀倀爀漀瘀椀搀攀爀⤀ 笀ഀഀ
 *     // create a service to demonstrate injection (not always needed)਍ ⨀     ␀瀀爀漀瘀椀搀攀⸀瘀愀氀甀攀⠀✀最爀攀攀琀✀Ⰰ 昀甀渀挀琀椀漀渀⠀渀愀洀攀⤀笀ഀഀ
 *       return 'Hello ' + name + '!';਍ ⨀     紀⤀㬀ഀഀ
 *਍ ⨀     ⼀⼀ 爀攀最椀猀琀攀爀 愀 昀椀氀琀攀爀 昀愀挀琀漀爀礀 眀栀椀挀栀 甀猀攀猀 琀栀攀ഀഀ
 *     // greet service to demonstrate DI.਍ ⨀     ␀昀椀氀琀攀爀倀爀漀瘀椀搀攀爀⸀爀攀最椀猀琀攀爀⠀✀最爀攀攀琀✀Ⰰ 昀甀渀挀琀椀漀渀⠀最爀攀攀琀⤀笀ഀഀ
 *       // return the filter function which uses the greet service਍ ⨀       ⼀⼀ 琀漀 最攀渀攀爀愀琀攀 猀愀氀甀琀愀琀椀漀渀ഀഀ
 *       return function(text) {਍ ⨀         ⼀⼀ 昀椀氀琀攀爀猀 渀攀攀搀 琀漀 戀攀 昀漀爀最椀瘀椀渀最 猀漀 挀栀攀挀欀 椀渀瀀甀琀 瘀愀氀椀搀椀琀礀ഀഀ
 *         return text && greet(text) || text;਍ ⨀       紀㬀ഀഀ
 *     });਍ ⨀   紀ഀഀ
 * </pre>਍ ⨀ഀഀ
 * The filter function is registered with the `$injector` under the filter name suffix with਍ ⨀ 怀䘀椀氀琀攀爀怀⸀ഀഀ
 * ਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   it('should be the same instance', inject(਍ ⨀     昀甀渀挀琀椀漀渀⠀␀昀椀氀琀攀爀倀爀漀瘀椀搀攀爀⤀ 笀ഀഀ
 *       $filterProvider.register('reverse', function(){਍ ⨀         爀攀琀甀爀渀 ⸀⸀⸀㬀ഀഀ
 *       });਍ ⨀     紀Ⰰഀഀ
 *     function($filter, reverseFilter) {਍ ⨀       攀砀瀀攀挀琀⠀␀昀椀氀琀攀爀⠀✀爀攀瘀攀爀猀攀✀⤀⤀⸀琀漀䈀攀⠀爀攀瘀攀爀猀攀䘀椀氀琀攀爀⤀㬀ഀഀ
 *     });਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ഀഀ
 * For more information about how angular filters work, and how to create your own filters, see਍ ⨀ 笀䀀氀椀渀欀 最甀椀搀攀⼀昀椀氀琀攀爀 䘀椀氀琀攀爀猀紀 椀渀 琀栀攀 䄀渀最甀氀愀爀 䐀攀瘀攀氀漀瀀攀爀 䜀甀椀搀攀⸀ഀഀ
 */਍⼀⨀⨀ഀഀ
 * @ngdoc method਍ ⨀ 䀀渀愀洀攀 渀最⸀␀昀椀氀琀攀爀倀爀漀瘀椀搀攀爀⌀爀攀最椀猀琀攀爀ഀഀ
 * @methodOf ng.$filterProvider਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Register filter factory function.਍ ⨀ഀഀ
 * @param {String} name Name of the filter.਍ ⨀ 䀀瀀愀爀愀洀 笀昀甀渀挀琀椀漀渀紀 昀渀 吀栀攀 昀椀氀琀攀爀 昀愀挀琀漀爀礀 昀甀渀挀琀椀漀渀 眀栀椀挀栀 椀猀 椀渀樀攀挀琀愀戀氀攀⸀ഀഀ
 */਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 渀最⸀␀昀椀氀琀攀爀ഀഀ
 * @function਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Filters are used for formatting data displayed to the user.਍ ⨀ഀഀ
 * The general syntax in templates is as follows:਍ ⨀ഀഀ
 *         {{ expression [| filter_name[:parameter_value] ... ] }}਍ ⨀ഀഀ
 * @param {String} name Name of the filter function to retrieve਍ ⨀ 䀀爀攀琀甀爀渀 笀䘀甀渀挀琀椀漀渀紀 琀栀攀 昀椀氀琀攀爀 昀甀渀挀琀椀漀渀ഀഀ
 */਍␀䘀椀氀琀攀爀倀爀漀瘀椀搀攀爀⸀␀椀渀樀攀挀琀 㴀 嬀✀␀瀀爀漀瘀椀搀攀✀崀㬀ഀഀ
function $FilterProvider($provide) {਍  瘀愀爀 猀甀昀昀椀砀 㴀 ✀䘀椀氀琀攀爀✀㬀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc function਍   ⨀ 䀀渀愀洀攀 渀最⸀␀挀漀渀琀爀漀氀氀攀爀倀爀漀瘀椀搀攀爀⌀爀攀最椀猀琀攀爀ഀഀ
   * @methodOf ng.$controllerProvider਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最簀伀戀樀攀挀琀紀 渀愀洀攀 一愀洀攀 漀昀 琀栀攀 昀椀氀琀攀爀 昀甀渀挀琀椀漀渀Ⰰ 漀爀 愀渀 漀戀樀攀挀琀 洀愀瀀 漀昀 昀椀氀琀攀爀猀 眀栀攀爀攀ഀഀ
   *    the keys are the filter names and the values are the filter factories.਍   ⨀ 䀀爀攀琀甀爀渀猀 笀伀戀樀攀挀琀紀 刀攀最椀猀琀攀爀攀搀 昀椀氀琀攀爀 椀渀猀琀愀渀挀攀Ⰰ 漀爀 椀昀 愀 洀愀瀀 漀昀 昀椀氀琀攀爀猀 眀愀猀 瀀爀漀瘀椀搀攀搀 琀栀攀渀 愀 洀愀瀀ഀഀ
   *    of the registered filter instances.਍   ⨀⼀ഀഀ
  function register(name, factory) {਍    椀昀⠀椀猀伀戀樀攀挀琀⠀渀愀洀攀⤀⤀ 笀ഀഀ
      var filters = {};਍      昀漀爀䔀愀挀栀⠀渀愀洀攀Ⰰ 昀甀渀挀琀椀漀渀⠀昀椀氀琀攀爀Ⰰ 欀攀礀⤀ 笀ഀഀ
        filters[key] = register(key, filter);਍      紀⤀㬀ഀഀ
      return filters;਍    紀 攀氀猀攀 笀ഀഀ
      return $provide.factory(name + suffix, factory);਍    紀ഀഀ
  }਍  琀栀椀猀⸀爀攀最椀猀琀攀爀 㴀 爀攀最椀猀琀攀爀㬀ഀഀ
਍  琀栀椀猀⸀␀最攀琀 㴀 嬀✀␀椀渀樀攀挀琀漀爀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀椀渀樀攀挀琀漀爀⤀ 笀ഀഀ
    return function(name) {਍      爀攀琀甀爀渀 ␀椀渀樀攀挀琀漀爀⸀最攀琀⠀渀愀洀攀 ⬀ 猀甀昀昀椀砀⤀㬀ഀഀ
    };਍  紀崀㬀ഀഀ
਍  ⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
  ਍  ⼀⨀ 最氀漀戀愀氀ഀഀ
    currencyFilter: false,਍    搀愀琀攀䘀椀氀琀攀爀㨀 昀愀氀猀攀Ⰰഀഀ
    filterFilter: false,਍    樀猀漀渀䘀椀氀琀攀爀㨀 昀愀氀猀攀Ⰰഀഀ
    limitToFilter: false,਍    氀漀眀攀爀挀愀猀攀䘀椀氀琀攀爀㨀 昀愀氀猀攀Ⰰഀഀ
    numberFilter: false,਍    漀爀搀攀爀䈀礀䘀椀氀琀攀爀㨀 昀愀氀猀攀Ⰰഀഀ
    uppercaseFilter: false,਍  ⨀⼀ഀഀ
਍  爀攀最椀猀琀攀爀⠀✀挀甀爀爀攀渀挀礀✀Ⰰ 挀甀爀爀攀渀挀礀䘀椀氀琀攀爀⤀㬀ഀഀ
  register('date', dateFilter);਍  爀攀最椀猀琀攀爀⠀✀昀椀氀琀攀爀✀Ⰰ 昀椀氀琀攀爀䘀椀氀琀攀爀⤀㬀ഀഀ
  register('json', jsonFilter);਍  爀攀最椀猀琀攀爀⠀✀氀椀洀椀琀吀漀✀Ⰰ 氀椀洀椀琀吀漀䘀椀氀琀攀爀⤀㬀ഀഀ
  register('lowercase', lowercaseFilter);਍  爀攀最椀猀琀攀爀⠀✀渀甀洀戀攀爀✀Ⰰ 渀甀洀戀攀爀䘀椀氀琀攀爀⤀㬀ഀഀ
  register('orderBy', orderByFilter);਍  爀攀最椀猀琀攀爀⠀✀甀瀀瀀攀爀挀愀猀攀✀Ⰰ 甀瀀瀀攀爀挀愀猀攀䘀椀氀琀攀爀⤀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀椀氀琀攀爀ഀഀ
 * @name ng.filter:filter਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Selects a subset of items from `array` and returns it as a new array.਍ ⨀ഀഀ
 * @param {Array} array The source array.਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最簀伀戀樀攀挀琀簀昀甀渀挀琀椀漀渀⠀⤀紀 攀砀瀀爀攀猀猀椀漀渀 吀栀攀 瀀爀攀搀椀挀愀琀攀 琀漀 戀攀 甀猀攀搀 昀漀爀 猀攀氀攀挀琀椀渀最 椀琀攀洀猀 昀爀漀洀ഀഀ
 *   `array`.਍ ⨀ഀഀ
 *   Can be one of:਍ ⨀ഀഀ
 *   - `string`: The string is evaluated as an expression and the resulting value is used for substring match against਍ ⨀     琀栀攀 挀漀渀琀攀渀琀猀 漀昀 琀栀攀 怀愀爀爀愀礀怀⸀ 䄀氀氀 猀琀爀椀渀最猀 漀爀 漀戀樀攀挀琀猀 眀椀琀栀 猀琀爀椀渀最 瀀爀漀瀀攀爀琀椀攀猀 椀渀 怀愀爀爀愀礀怀 琀栀愀琀 挀漀渀琀愀椀渀 琀栀椀猀 猀琀爀椀渀最ഀഀ
 *     will be returned. The predicate can be negated by prefixing the string with `!`.਍ ⨀ഀഀ
 *   - `Object`: A pattern object can be used to filter specific properties on objects contained਍ ⨀     戀礀 怀愀爀爀愀礀怀⸀ 䘀漀爀 攀砀愀洀瀀氀攀 怀笀渀愀洀攀㨀∀䴀∀Ⰰ 瀀栀漀渀攀㨀∀㄀∀紀怀 瀀爀攀搀椀挀愀琀攀 眀椀氀氀 爀攀琀甀爀渀 愀渀 愀爀爀愀礀 漀昀 椀琀攀洀猀ഀഀ
 *     which have property `name` containing "M" and property `phone` containing "1". A special਍ ⨀     瀀爀漀瀀攀爀琀礀 渀愀洀攀 怀␀怀 挀愀渀 戀攀 甀猀攀搀 ⠀愀猀 椀渀 怀笀␀㨀∀琀攀砀琀∀紀怀⤀ 琀漀 愀挀挀攀瀀琀 愀 洀愀琀挀栀 愀最愀椀渀猀琀 愀渀礀ഀഀ
 *     property of the object. That's equivalent to the simple substring match with a `string`਍ ⨀     愀猀 搀攀猀挀爀椀戀攀搀 愀戀漀瘀攀⸀ഀഀ
 *਍ ⨀   ⴀ 怀昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀怀㨀 䄀 瀀爀攀搀椀挀愀琀攀 昀甀渀挀琀椀漀渀 挀愀渀 戀攀 甀猀攀搀 琀漀 眀爀椀琀攀 愀爀戀椀琀爀愀爀礀 昀椀氀琀攀爀猀⸀ 吀栀攀 昀甀渀挀琀椀漀渀 椀猀ഀഀ
 *     called for each element of `array`. The final result is an array of those elements that਍ ⨀     琀栀攀 瀀爀攀搀椀挀愀琀攀 爀攀琀甀爀渀攀搀 琀爀甀攀 昀漀爀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀昀甀渀挀琀椀漀渀⠀愀挀琀甀愀氀Ⰰ 攀砀瀀攀挀琀攀搀⤀簀琀爀甀攀簀甀渀搀攀昀椀渀攀搀紀 挀漀洀瀀愀爀愀琀漀爀 䌀漀洀瀀愀爀愀琀漀爀 眀栀椀挀栀 椀猀 甀猀攀搀 椀渀ഀഀ
 *     determining if the expected value (from the filter expression) and actual value (from਍ ⨀     琀栀攀 漀戀樀攀挀琀 椀渀 琀栀攀 愀爀爀愀礀⤀ 猀栀漀甀氀搀 戀攀 挀漀渀猀椀搀攀爀攀搀 愀 洀愀琀挀栀⸀ഀഀ
 *਍ ⨀   䌀愀渀 戀攀 漀渀攀 漀昀㨀ഀഀ
 *਍ ⨀     ⴀ 怀昀甀渀挀琀椀漀渀⠀愀挀琀甀愀氀Ⰰ 攀砀瀀攀挀琀攀搀⤀怀㨀ഀഀ
 *       The function will be given the object value and the predicate value to compare and਍ ⨀       猀栀漀甀氀搀 爀攀琀甀爀渀 琀爀甀攀 椀昀 琀栀攀 椀琀攀洀 猀栀漀甀氀搀 戀攀 椀渀挀氀甀搀攀搀 椀渀 昀椀氀琀攀爀攀搀 爀攀猀甀氀琀⸀ഀഀ
 *਍ ⨀     ⴀ 怀琀爀甀攀怀㨀 䄀 猀栀漀爀琀栀愀渀搀 昀漀爀 怀昀甀渀挀琀椀漀渀⠀愀挀琀甀愀氀Ⰰ 攀砀瀀攀挀琀攀搀⤀ 笀 爀攀琀甀爀渀 愀渀最甀氀愀爀⸀攀焀甀愀氀猀⠀攀砀瀀攀挀琀攀搀Ⰰ 愀挀琀甀愀氀⤀紀怀⸀ഀഀ
 *       this is essentially strict comparison of expected and actual.਍ ⨀ഀഀ
 *     - `false|undefined`: A short hand for a function which will look for a substring match in case਍ ⨀       椀渀猀攀渀猀椀琀椀瘀攀 眀愀礀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
   <doc:example>਍     㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
       <div ng-init="friends = [{name:'John', phone:'555-1276'},਍                                笀渀愀洀攀㨀✀䴀愀爀礀✀Ⰰ 瀀栀漀渀攀㨀✀㠀　　ⴀ䈀䤀䜀ⴀ䴀䄀刀夀✀紀Ⰰഀഀ
                                {name:'Mike', phone:'555-4321'},਍                                笀渀愀洀攀㨀✀䄀搀愀洀✀Ⰰ 瀀栀漀渀攀㨀✀㔀㔀㔀ⴀ㔀㘀㜀㠀✀紀Ⰰഀഀ
                                {name:'Julie', phone:'555-8765'},਍                                笀渀愀洀攀㨀✀䨀甀氀椀攀琀琀攀✀Ⰰ 瀀栀漀渀攀㨀✀㔀㔀㔀ⴀ㔀㘀㜀㠀✀紀崀∀㸀㰀⼀搀椀瘀㸀ഀഀ
਍       匀攀愀爀挀栀㨀 㰀椀渀瀀甀琀 渀最ⴀ洀漀搀攀氀㴀∀猀攀愀爀挀栀吀攀砀琀∀㸀ഀഀ
       <table id="searchTextResults">਍         㰀琀爀㸀㰀琀栀㸀一愀洀攀㰀⼀琀栀㸀㰀琀栀㸀倀栀漀渀攀㰀⼀琀栀㸀㰀⼀琀爀㸀ഀഀ
         <tr ng-repeat="friend in friends | filter:searchText">਍           㰀琀搀㸀笀笀昀爀椀攀渀搀⸀渀愀洀攀紀紀㰀⼀琀搀㸀ഀഀ
           <td>{{friend.phone}}</td>਍         㰀⼀琀爀㸀ഀഀ
       </table>਍       㰀栀爀㸀ഀഀ
       Any: <input ng-model="search.$"> <br>਍       一愀洀攀 漀渀氀礀 㰀椀渀瀀甀琀 渀最ⴀ洀漀搀攀氀㴀∀猀攀愀爀挀栀⸀渀愀洀攀∀㸀㰀戀爀㸀ഀഀ
       Phone only <input ng-model="search.phone"><br>਍       䔀焀甀愀氀椀琀礀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀挀栀攀挀欀戀漀砀∀ 渀最ⴀ洀漀搀攀氀㴀∀猀琀爀椀挀琀∀㸀㰀戀爀㸀ഀഀ
       <table id="searchObjResults">਍         㰀琀爀㸀㰀琀栀㸀一愀洀攀㰀⼀琀栀㸀㰀琀栀㸀倀栀漀渀攀㰀⼀琀栀㸀㰀⼀琀爀㸀ഀഀ
         <tr ng-repeat="friend in friends | filter:search:strict">਍           㰀琀搀㸀笀笀昀爀椀攀渀搀⸀渀愀洀攀紀紀㰀⼀琀搀㸀ഀഀ
           <td>{{friend.phone}}</td>਍         㰀⼀琀爀㸀ഀഀ
       </table>਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
     <doc:scenario>਍       椀琀⠀✀猀栀漀甀氀搀 猀攀愀爀挀栀 愀挀爀漀猀猀 愀氀氀 昀椀攀氀搀猀 眀栀攀渀 昀椀氀琀攀爀椀渀最 眀椀琀栀 愀 猀琀爀椀渀最✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         input('searchText').enter('m');਍         攀砀瀀攀挀琀⠀爀攀瀀攀愀琀攀爀⠀✀⌀猀攀愀爀挀栀吀攀砀琀刀攀猀甀氀琀猀 琀爀✀Ⰰ ✀昀爀椀攀渀搀 椀渀 昀爀椀攀渀搀猀✀⤀⸀挀漀氀甀洀渀⠀✀昀爀椀攀渀搀⸀渀愀洀攀✀⤀⤀⸀ഀഀ
           toEqual(['Mary', 'Mike', 'Adam']);਍ഀഀ
         input('searchText').enter('76');਍         攀砀瀀攀挀琀⠀爀攀瀀攀愀琀攀爀⠀✀⌀猀攀愀爀挀栀吀攀砀琀刀攀猀甀氀琀猀 琀爀✀Ⰰ ✀昀爀椀攀渀搀 椀渀 昀爀椀攀渀搀猀✀⤀⸀挀漀氀甀洀渀⠀✀昀爀椀攀渀搀⸀渀愀洀攀✀⤀⤀⸀ഀഀ
           toEqual(['John', 'Julie']);਍       紀⤀㬀ഀഀ
਍       椀琀⠀✀猀栀漀甀氀搀 猀攀愀爀挀栀 椀渀 猀瀀攀挀椀昀椀挀 昀椀攀氀搀猀 眀栀攀渀 昀椀氀琀攀爀椀渀最 眀椀琀栀 愀 瀀爀攀搀椀挀愀琀攀 漀戀樀攀挀琀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         input('search.$').enter('i');਍         攀砀瀀攀挀琀⠀爀攀瀀攀愀琀攀爀⠀✀⌀猀攀愀爀挀栀伀戀樀刀攀猀甀氀琀猀 琀爀✀Ⰰ ✀昀爀椀攀渀搀 椀渀 昀爀椀攀渀搀猀✀⤀⸀挀漀氀甀洀渀⠀✀昀爀椀攀渀搀⸀渀愀洀攀✀⤀⤀⸀ഀഀ
           toEqual(['Mary', 'Mike', 'Julie', 'Juliette']);਍       紀⤀㬀ഀഀ
       it('should use a equal comparison when comparator is true', function() {਍         椀渀瀀甀琀⠀✀猀攀愀爀挀栀⸀渀愀洀攀✀⤀⸀攀渀琀攀爀⠀✀䨀甀氀椀攀✀⤀㬀ഀഀ
         input('strict').check();਍         攀砀瀀攀挀琀⠀爀攀瀀攀愀琀攀爀⠀✀⌀猀攀愀爀挀栀伀戀樀刀攀猀甀氀琀猀 琀爀✀Ⰰ ✀昀爀椀攀渀搀 椀渀 昀爀椀攀渀搀猀✀⤀⸀挀漀氀甀洀渀⠀✀昀爀椀攀渀搀⸀渀愀洀攀✀⤀⤀⸀ഀഀ
           toEqual(['Julie']);਍       紀⤀㬀ഀഀ
     </doc:scenario>਍   㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 昀椀氀琀攀爀䘀椀氀琀攀爀⠀⤀ 笀ഀഀ
  return function(array, expression, comparator) {਍    椀昀 ⠀℀椀猀䄀爀爀愀礀⠀愀爀爀愀礀⤀⤀ 爀攀琀甀爀渀 愀爀爀愀礀㬀ഀഀ
਍    瘀愀爀 挀漀洀瀀愀爀愀琀漀爀吀礀瀀攀 㴀 琀礀瀀攀漀昀⠀挀漀洀瀀愀爀愀琀漀爀⤀Ⰰഀഀ
        predicates = [];਍ഀഀ
    predicates.check = function(value) {਍      昀漀爀 ⠀瘀愀爀 樀 㴀 　㬀 樀 㰀 瀀爀攀搀椀挀愀琀攀猀⸀氀攀渀最琀栀㬀 樀⬀⬀⤀ 笀ഀഀ
        if(!predicates[j](value)) {਍          爀攀琀甀爀渀 昀愀氀猀攀㬀ഀഀ
        }਍      紀ഀഀ
      return true;਍    紀㬀ഀഀ
਍    椀昀 ⠀挀漀洀瀀愀爀愀琀漀爀吀礀瀀攀 ℀㴀㴀 ✀昀甀渀挀琀椀漀渀✀⤀ 笀ഀഀ
      if (comparatorType === 'boolean' && comparator) {਍        挀漀洀瀀愀爀愀琀漀爀 㴀 昀甀渀挀琀椀漀渀⠀漀戀樀Ⰰ 琀攀砀琀⤀ 笀ഀഀ
          return angular.equals(obj, text);਍        紀㬀ഀഀ
      } else {਍        挀漀洀瀀愀爀愀琀漀爀 㴀 昀甀渀挀琀椀漀渀⠀漀戀樀Ⰰ 琀攀砀琀⤀ 笀ഀഀ
          text = (''+text).toLowerCase();਍          爀攀琀甀爀渀 ⠀✀✀⬀漀戀樀⤀⸀琀漀䰀漀眀攀爀䌀愀猀攀⠀⤀⸀椀渀搀攀砀伀昀⠀琀攀砀琀⤀ 㸀 ⴀ㄀㬀ഀഀ
        };਍      紀ഀഀ
    }਍ഀഀ
    var search = function(obj, text){਍      椀昀 ⠀琀礀瀀攀漀昀 琀攀砀琀 㴀㴀 ✀猀琀爀椀渀最✀ ☀☀ 琀攀砀琀⸀挀栀愀爀䄀琀⠀　⤀ 㴀㴀㴀 ✀℀✀⤀ 笀ഀഀ
        return !search(obj, text.substr(1));਍      紀ഀഀ
      switch (typeof obj) {਍        挀愀猀攀 ∀戀漀漀氀攀愀渀∀㨀ഀഀ
        case "number":਍        挀愀猀攀 ∀猀琀爀椀渀最∀㨀ഀഀ
          return comparator(obj, text);਍        挀愀猀攀 ∀漀戀樀攀挀琀∀㨀ഀഀ
          switch (typeof text) {਍            挀愀猀攀 ∀漀戀樀攀挀琀∀㨀ഀഀ
              return comparator(obj, text);਍            搀攀昀愀甀氀琀㨀ഀഀ
              for ( var objKey in obj) {਍                椀昀 ⠀漀戀樀䬀攀礀⸀挀栀愀爀䄀琀⠀　⤀ ℀㴀㴀 ✀␀✀ ☀☀ 猀攀愀爀挀栀⠀漀戀樀嬀漀戀樀䬀攀礀崀Ⰰ 琀攀砀琀⤀⤀ 笀ഀഀ
                  return true;਍                紀ഀഀ
              }਍              戀爀攀愀欀㬀ഀഀ
          }਍          爀攀琀甀爀渀 昀愀氀猀攀㬀ഀഀ
        case "array":਍          昀漀爀 ⠀ 瘀愀爀 椀 㴀 　㬀 椀 㰀 漀戀樀⸀氀攀渀最琀栀㬀 椀⬀⬀⤀ 笀ഀഀ
            if (search(obj[i], text)) {਍              爀攀琀甀爀渀 琀爀甀攀㬀ഀഀ
            }਍          紀ഀഀ
          return false;਍        搀攀昀愀甀氀琀㨀ഀഀ
          return false;਍      紀ഀഀ
    };਍    猀眀椀琀挀栀 ⠀琀礀瀀攀漀昀 攀砀瀀爀攀猀猀椀漀渀⤀ 笀ഀഀ
      case "boolean":਍      挀愀猀攀 ∀渀甀洀戀攀爀∀㨀ഀഀ
      case "string":਍        ⼀⼀ 匀攀琀 甀瀀 攀砀瀀爀攀猀猀椀漀渀 漀戀樀攀挀琀 愀渀搀 昀愀氀氀 琀栀爀漀甀最栀ഀഀ
        expression = {$:expression};਍        ⼀⼀ 樀猀栀椀渀琀 ⴀ圀　㠀㘀ഀഀ
      case "object":਍        ⼀⼀ 樀猀栀椀渀琀 ⬀圀　㠀㘀ഀഀ
        for (var key in expression) {਍          ⠀昀甀渀挀琀椀漀渀⠀瀀愀琀栀⤀ 笀ഀഀ
            if (typeof expression[path] == 'undefined') return;਍            瀀爀攀搀椀挀愀琀攀猀⸀瀀甀猀栀⠀昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
              return search(path == '$' ? value : getter(value, path), expression[path]);਍            紀⤀㬀ഀഀ
          })(key);਍        紀ഀഀ
        break;਍      挀愀猀攀 ✀昀甀渀挀琀椀漀渀✀㨀ഀഀ
        predicates.push(expression);਍        戀爀攀愀欀㬀ഀഀ
      default:਍        爀攀琀甀爀渀 愀爀爀愀礀㬀ഀഀ
    }਍    瘀愀爀 昀椀氀琀攀爀攀搀 㴀 嬀崀㬀ഀഀ
    for ( var j = 0; j < array.length; j++) {਍      瘀愀爀 瘀愀氀甀攀 㴀 愀爀爀愀礀嬀樀崀㬀ഀഀ
      if (predicates.check(value)) {਍        昀椀氀琀攀爀攀搀⸀瀀甀猀栀⠀瘀愀氀甀攀⤀㬀ഀഀ
      }਍    紀ഀഀ
    return filtered;਍  紀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀椀氀琀攀爀ഀഀ
 * @name ng.filter:currency਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Formats a number as a currency (ie $1,234.56). When no currency symbol is provided, default਍ ⨀ 猀礀洀戀漀氀 昀漀爀 挀甀爀爀攀渀琀 氀漀挀愀氀攀 椀猀 甀猀攀搀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀紀 愀洀漀甀渀琀 䤀渀瀀甀琀 琀漀 昀椀氀琀攀爀⸀ഀഀ
 * @param {string=} symbol Currency symbol or identifier to be displayed.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀猀琀爀椀渀最紀 䘀漀爀洀愀琀琀攀搀 渀甀洀戀攀爀⸀ഀഀ
 *਍ ⨀ഀഀ
 * @example਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍       㰀猀挀爀椀瀀琀㸀ഀഀ
         function Ctrl($scope) {਍           ␀猀挀漀瀀攀⸀愀洀漀甀渀琀 㴀 ㄀㈀㌀㐀⸀㔀㘀㬀ഀഀ
         }਍       㰀⼀猀挀爀椀瀀琀㸀ഀഀ
       <div ng-controller="Ctrl">਍         㰀椀渀瀀甀琀 琀礀瀀攀㴀∀渀甀洀戀攀爀∀ 渀最ⴀ洀漀搀攀氀㴀∀愀洀漀甀渀琀∀㸀 㰀戀爀㸀ഀഀ
         default currency symbol ($): {{amount | currency}}<br>਍         挀甀猀琀漀洀 挀甀爀爀攀渀挀礀 椀搀攀渀琀椀昀椀攀爀 ⠀唀匀䐀␀⤀㨀 笀笀愀洀漀甀渀琀 簀 挀甀爀爀攀渀挀礀㨀∀唀匀䐀␀∀紀紀ഀഀ
       </div>਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
     <doc:scenario>਍       椀琀⠀✀猀栀漀甀氀搀 椀渀椀琀 眀椀琀栀 ㄀㈀㌀㐀⸀㔀㘀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         expect(binding('amount | currency')).toBe('$1,234.56');਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀愀洀漀甀渀琀 簀 挀甀爀爀攀渀挀礀㨀∀唀匀䐀␀∀✀⤀⤀⸀琀漀䈀攀⠀✀唀匀䐀␀㄀Ⰰ㈀㌀㐀⸀㔀㘀✀⤀㬀ഀഀ
       });਍       椀琀⠀✀猀栀漀甀氀搀 甀瀀搀愀琀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         input('amount').enter('-1234');਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀愀洀漀甀渀琀 簀 挀甀爀爀攀渀挀礀✀⤀⤀⸀琀漀䈀攀⠀✀⠀␀㄀Ⰰ㈀㌀㐀⸀　　⤀✀⤀㬀ഀഀ
         expect(binding('amount | currency:"USD$"')).toBe('(USD$1,234.00)');਍       紀⤀㬀ഀഀ
     </doc:scenario>਍   㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍挀甀爀爀攀渀挀礀䘀椀氀琀攀爀⸀␀椀渀樀攀挀琀 㴀 嬀✀␀氀漀挀愀氀攀✀崀㬀ഀഀ
function currencyFilter($locale) {਍  瘀愀爀 昀漀爀洀愀琀猀 㴀 ␀氀漀挀愀氀攀⸀一唀䴀䈀䔀刀开䘀伀刀䴀䄀吀匀㬀ഀഀ
  return function(amount, currencySymbol){਍    椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀挀甀爀爀攀渀挀礀匀礀洀戀漀氀⤀⤀ 挀甀爀爀攀渀挀礀匀礀洀戀漀氀 㴀 昀漀爀洀愀琀猀⸀䌀唀刀刀䔀一䌀夀开匀夀䴀㬀ഀഀ
    return formatNumber(amount, formats.PATTERNS[1], formats.GROUP_SEP, formats.DECIMAL_SEP, 2).਍                爀攀瀀氀愀挀攀⠀⼀尀甀　　䄀㐀⼀最Ⰰ 挀甀爀爀攀渀挀礀匀礀洀戀漀氀⤀㬀ഀഀ
  };਍紀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc filter਍ ⨀ 䀀渀愀洀攀 渀最⸀昀椀氀琀攀爀㨀渀甀洀戀攀爀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ 䘀漀爀洀愀琀猀 愀 渀甀洀戀攀爀 愀猀 琀攀砀琀⸀ഀഀ
 *਍ ⨀ 䤀昀 琀栀攀 椀渀瀀甀琀 椀猀 渀漀琀 愀 渀甀洀戀攀爀 愀渀 攀洀瀀琀礀 猀琀爀椀渀最 椀猀 爀攀琀甀爀渀攀搀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀簀猀琀爀椀渀最紀 渀甀洀戀攀爀 一甀洀戀攀爀 琀漀 昀漀爀洀愀琀⸀ഀഀ
 * @param {(number|string)=} fractionSize Number of decimal places to round the number to.਍ ⨀ 䤀昀 琀栀椀猀 椀猀 渀漀琀 瀀爀漀瘀椀搀攀搀 琀栀攀渀 琀栀攀 昀爀愀挀琀椀漀渀 猀椀稀攀 椀猀 挀漀洀瀀甀琀攀搀 昀爀漀洀 琀栀攀 挀甀爀爀攀渀琀 氀漀挀愀氀攀✀猀 渀甀洀戀攀爀ഀഀ
 * formatting pattern. In the case of the default locale, it will be 3.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀猀琀爀椀渀最紀 一甀洀戀攀爀 爀漀甀渀搀攀搀 琀漀 搀攀挀椀洀愀氀倀氀愀挀攀猀 愀渀搀 瀀氀愀挀攀猀 愀 ﴀ﷿⳿ﴀ﷿⃿愀昀琀攀爀 攀愀挀栀 琀栀椀爀搀 搀椀最椀琀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
   <doc:example>਍     㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
       <script>਍         昀甀渀挀琀椀漀渀 䌀琀爀氀⠀␀猀挀漀瀀攀⤀ 笀ഀഀ
           $scope.val = 1234.56789;਍         紀ഀഀ
       </script>਍       㰀搀椀瘀 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀䌀琀爀氀∀㸀ഀഀ
         Enter number: <input ng-model='val'><br>਍         䐀攀昀愀甀氀琀 昀漀爀洀愀琀琀椀渀最㨀 笀笀瘀愀氀 簀 渀甀洀戀攀爀紀紀㰀戀爀㸀ഀഀ
         No fractions: {{val | number:0}}<br>਍         一攀最愀琀椀瘀攀 渀甀洀戀攀爀㨀 笀笀ⴀ瘀愀氀 簀 渀甀洀戀攀爀㨀㐀紀紀ഀഀ
       </div>਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
     <doc:scenario>਍       椀琀⠀✀猀栀漀甀氀搀 昀漀爀洀愀琀 渀甀洀戀攀爀猀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         expect(binding('val | number')).toBe('1,234.568');਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀瘀愀氀 簀 渀甀洀戀攀爀㨀　✀⤀⤀⸀琀漀䈀攀⠀✀㄀Ⰰ㈀㌀㔀✀⤀㬀ഀഀ
         expect(binding('-val | number:4')).toBe('-1,234.5679');਍       紀⤀㬀ഀഀ
਍       椀琀⠀✀猀栀漀甀氀搀 甀瀀搀愀琀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         input('val').enter('3374.333');਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀瘀愀氀 簀 渀甀洀戀攀爀✀⤀⤀⸀琀漀䈀攀⠀✀㌀Ⰰ㌀㜀㐀⸀㌀㌀㌀✀⤀㬀ഀഀ
         expect(binding('val | number:0')).toBe('3,374');਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀ⴀ瘀愀氀 簀 渀甀洀戀攀爀㨀㐀✀⤀⤀⸀琀漀䈀攀⠀✀ⴀ㌀Ⰰ㌀㜀㐀⸀㌀㌀㌀　✀⤀㬀ഀഀ
       });਍     㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
   </doc:example>਍ ⨀⼀ഀഀ
਍ഀഀ
numberFilter.$inject = ['$locale'];਍昀甀渀挀琀椀漀渀 渀甀洀戀攀爀䘀椀氀琀攀爀⠀␀氀漀挀愀氀攀⤀ 笀ഀഀ
  var formats = $locale.NUMBER_FORMATS;਍  爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀渀甀洀戀攀爀Ⰰ 昀爀愀挀琀椀漀渀匀椀稀攀⤀ 笀ഀഀ
    return formatNumber(number, formats.PATTERNS[0], formats.GROUP_SEP, formats.DECIMAL_SEP,਍      昀爀愀挀琀椀漀渀匀椀稀攀⤀㬀ഀഀ
  };਍紀ഀഀ
਍瘀愀爀 䐀䔀䌀䤀䴀䄀䰀开匀䔀倀 㴀 ✀⸀✀㬀ഀഀ
function formatNumber(number, pattern, groupSep, decimalSep, fractionSize) {਍  椀昀 ⠀椀猀一愀一⠀渀甀洀戀攀爀⤀ 簀簀 ℀椀猀䘀椀渀椀琀攀⠀渀甀洀戀攀爀⤀⤀ 爀攀琀甀爀渀 ✀✀㬀ഀഀ
਍  瘀愀爀 椀猀一攀最愀琀椀瘀攀 㴀 渀甀洀戀攀爀 㰀 　㬀ഀഀ
  number = Math.abs(number);਍  瘀愀爀 渀甀洀匀琀爀 㴀 渀甀洀戀攀爀 ⬀ ✀✀Ⰰഀഀ
      formatedText = '',਍      瀀愀爀琀猀 㴀 嬀崀㬀ഀഀ
਍  瘀愀爀 栀愀猀䔀砀瀀漀渀攀渀琀 㴀 昀愀氀猀攀㬀ഀഀ
  if (numStr.indexOf('e') !== -1) {਍    瘀愀爀 洀愀琀挀栀 㴀 渀甀洀匀琀爀⸀洀愀琀挀栀⠀⼀⠀嬀尀搀尀⸀崀⬀⤀攀⠀ⴀ㼀⤀⠀尀搀⬀⤀⼀⤀㬀ഀഀ
    if (match && match[2] == '-' && match[3] > fractionSize + 1) {਍      渀甀洀匀琀爀 㴀 ✀　✀㬀ഀഀ
    } else {਍      昀漀爀洀愀琀攀搀吀攀砀琀 㴀 渀甀洀匀琀爀㬀ഀഀ
      hasExponent = true;਍    紀ഀഀ
  }਍ഀഀ
  if (!hasExponent) {਍    瘀愀爀 昀爀愀挀琀椀漀渀䰀攀渀 㴀 ⠀渀甀洀匀琀爀⸀猀瀀氀椀琀⠀䐀䔀䌀䤀䴀䄀䰀开匀䔀倀⤀嬀㄀崀 簀簀 ✀✀⤀⸀氀攀渀最琀栀㬀ഀഀ
਍    ⼀⼀ 搀攀琀攀爀洀椀渀攀 昀爀愀挀琀椀漀渀匀椀稀攀 椀昀 椀琀 椀猀 渀漀琀 猀瀀攀挀椀昀椀攀搀ഀഀ
    if (isUndefined(fractionSize)) {਍      昀爀愀挀琀椀漀渀匀椀稀攀 㴀 䴀愀琀栀⸀洀椀渀⠀䴀愀琀栀⸀洀愀砀⠀瀀愀琀琀攀爀渀⸀洀椀渀䘀爀愀挀Ⰰ 昀爀愀挀琀椀漀渀䰀攀渀⤀Ⰰ 瀀愀琀琀攀爀渀⸀洀愀砀䘀爀愀挀⤀㬀ഀഀ
    }਍ഀഀ
    var pow = Math.pow(10, fractionSize);਍    渀甀洀戀攀爀 㴀 䴀愀琀栀⸀爀漀甀渀搀⠀渀甀洀戀攀爀 ⨀ 瀀漀眀⤀ ⼀ 瀀漀眀㬀ഀഀ
    var fraction = ('' + number).split(DECIMAL_SEP);਍    瘀愀爀 眀栀漀氀攀 㴀 昀爀愀挀琀椀漀渀嬀　崀㬀ഀഀ
    fraction = fraction[1] || '';਍ഀഀ
    var i, pos = 0,਍        氀最爀漀甀瀀 㴀 瀀愀琀琀攀爀渀⸀氀最匀椀稀攀Ⰰഀഀ
        group = pattern.gSize;਍ഀഀ
    if (whole.length >= (lgroup + group)) {਍      瀀漀猀 㴀 眀栀漀氀攀⸀氀攀渀最琀栀 ⴀ 氀最爀漀甀瀀㬀ഀഀ
      for (i = 0; i < pos; i++) {਍        椀昀 ⠀⠀瀀漀猀 ⴀ 椀⤀─最爀漀甀瀀 㴀㴀㴀 　 ☀☀ 椀 ℀㴀㴀 　⤀ 笀ഀഀ
          formatedText += groupSep;਍        紀ഀഀ
        formatedText += whole.charAt(i);਍      紀ഀഀ
    }਍ഀഀ
    for (i = pos; i < whole.length; i++) {਍      椀昀 ⠀⠀眀栀漀氀攀⸀氀攀渀最琀栀 ⴀ 椀⤀─氀最爀漀甀瀀 㴀㴀㴀 　 ☀☀ 椀 ℀㴀㴀 　⤀ 笀ഀഀ
        formatedText += groupSep;਍      紀ഀഀ
      formatedText += whole.charAt(i);਍    紀ഀഀ
਍    ⼀⼀ 昀漀爀洀愀琀 昀爀愀挀琀椀漀渀 瀀愀爀琀⸀ഀഀ
    while(fraction.length < fractionSize) {਍      昀爀愀挀琀椀漀渀 ⬀㴀 ✀　✀㬀ഀഀ
    }਍ഀഀ
    if (fractionSize && fractionSize !== "0") formatedText += decimalSep + fraction.substr(0, fractionSize);਍  紀 攀氀猀攀 笀ഀഀ
਍    椀昀 ⠀昀爀愀挀琀椀漀渀匀椀稀攀 㸀 　 ☀☀ 渀甀洀戀攀爀 㸀 ⴀ㄀ ☀☀ 渀甀洀戀攀爀 㰀 ㄀⤀ 笀ഀഀ
      formatedText = number.toFixed(fractionSize);਍    紀ഀഀ
  }਍ഀഀ
  parts.push(isNegative ? pattern.negPre : pattern.posPre);਍  瀀愀爀琀猀⸀瀀甀猀栀⠀昀漀爀洀愀琀攀搀吀攀砀琀⤀㬀ഀഀ
  parts.push(isNegative ? pattern.negSuf : pattern.posSuf);਍  爀攀琀甀爀渀 瀀愀爀琀猀⸀樀漀椀渀⠀✀✀⤀㬀ഀഀ
}਍ഀഀ
function padNumber(num, digits, trim) {਍  瘀愀爀 渀攀最 㴀 ✀✀㬀ഀഀ
  if (num < 0) {਍    渀攀最 㴀  ✀ⴀ✀㬀ഀഀ
    num = -num;਍  紀ഀഀ
  num = '' + num;਍  眀栀椀氀攀⠀渀甀洀⸀氀攀渀最琀栀 㰀 搀椀最椀琀猀⤀ 渀甀洀 㴀 ✀　✀ ⬀ 渀甀洀㬀ഀഀ
  if (trim)਍    渀甀洀 㴀 渀甀洀⸀猀甀戀猀琀爀⠀渀甀洀⸀氀攀渀最琀栀 ⴀ 搀椀最椀琀猀⤀㬀ഀഀ
  return neg + num;਍紀ഀഀ
਍ഀഀ
function dateGetter(name, size, offset, trim) {਍  漀昀昀猀攀琀 㴀 漀昀昀猀攀琀 簀簀 　㬀ഀഀ
  return function(date) {਍    瘀愀爀 瘀愀氀甀攀 㴀 搀愀琀攀嬀✀最攀琀✀ ⬀ 渀愀洀攀崀⠀⤀㬀ഀഀ
    if (offset > 0 || value > -offset)਍      瘀愀氀甀攀 ⬀㴀 漀昀昀猀攀琀㬀ഀഀ
    if (value === 0 && offset == -12 ) value = 12;਍    爀攀琀甀爀渀 瀀愀搀一甀洀戀攀爀⠀瘀愀氀甀攀Ⰰ 猀椀稀攀Ⰰ 琀爀椀洀⤀㬀ഀഀ
  };਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 搀愀琀攀匀琀爀䜀攀琀琀攀爀⠀渀愀洀攀Ⰰ 猀栀漀爀琀䘀漀爀洀⤀ 笀ഀഀ
  return function(date, formats) {਍    瘀愀爀 瘀愀氀甀攀 㴀 搀愀琀攀嬀✀最攀琀✀ ⬀ 渀愀洀攀崀⠀⤀㬀ഀഀ
    var get = uppercase(shortForm ? ('SHORT' + name) : name);਍ഀഀ
    return formats[get][value];਍  紀㬀ഀഀ
}਍ഀഀ
function timeZoneGetter(date) {਍  瘀愀爀 稀漀渀攀 㴀 ⴀ㄀ ⨀ 搀愀琀攀⸀最攀琀吀椀洀攀稀漀渀攀伀昀昀猀攀琀⠀⤀㬀ഀഀ
  var paddedZone = (zone >= 0) ? "+" : "";਍ഀഀ
  paddedZone += padNumber(Math[zone > 0 ? 'floor' : 'ceil'](zone / 60), 2) +਍                瀀愀搀一甀洀戀攀爀⠀䴀愀琀栀⸀愀戀猀⠀稀漀渀攀 ─ 㘀　⤀Ⰰ ㈀⤀㬀ഀഀ
਍  爀攀琀甀爀渀 瀀愀搀搀攀搀娀漀渀攀㬀ഀഀ
}਍ഀഀ
function ampmGetter(date, formats) {਍  爀攀琀甀爀渀 搀愀琀攀⸀最攀琀䠀漀甀爀猀⠀⤀ 㰀 ㄀㈀ 㼀 昀漀爀洀愀琀猀⸀䄀䴀倀䴀匀嬀　崀 㨀 昀漀爀洀愀琀猀⸀䄀䴀倀䴀匀嬀㄀崀㬀ഀഀ
}਍ഀഀ
var DATE_FORMATS = {਍  礀礀礀礀㨀 搀愀琀攀䜀攀琀琀攀爀⠀✀䘀甀氀氀夀攀愀爀✀Ⰰ 㐀⤀Ⰰഀഀ
    yy: dateGetter('FullYear', 2, 0, true),਍     礀㨀 搀愀琀攀䜀攀琀琀攀爀⠀✀䘀甀氀氀夀攀愀爀✀Ⰰ ㄀⤀Ⰰഀഀ
  MMMM: dateStrGetter('Month'),਍   䴀䴀䴀㨀 搀愀琀攀匀琀爀䜀攀琀琀攀爀⠀✀䴀漀渀琀栀✀Ⰰ 琀爀甀攀⤀Ⰰഀഀ
    MM: dateGetter('Month', 2, 1),਍     䴀㨀 搀愀琀攀䜀攀琀琀攀爀⠀✀䴀漀渀琀栀✀Ⰰ ㄀Ⰰ ㄀⤀Ⰰഀഀ
    dd: dateGetter('Date', 2),਍     搀㨀 搀愀琀攀䜀攀琀琀攀爀⠀✀䐀愀琀攀✀Ⰰ ㄀⤀Ⰰഀഀ
    HH: dateGetter('Hours', 2),਍     䠀㨀 搀愀琀攀䜀攀琀琀攀爀⠀✀䠀漀甀爀猀✀Ⰰ ㄀⤀Ⰰഀഀ
    hh: dateGetter('Hours', 2, -12),਍     栀㨀 搀愀琀攀䜀攀琀琀攀爀⠀✀䠀漀甀爀猀✀Ⰰ ㄀Ⰰ ⴀ㄀㈀⤀Ⰰഀഀ
    mm: dateGetter('Minutes', 2),਍     洀㨀 搀愀琀攀䜀攀琀琀攀爀⠀✀䴀椀渀甀琀攀猀✀Ⰰ ㄀⤀Ⰰഀഀ
    ss: dateGetter('Seconds', 2),਍     猀㨀 搀愀琀攀䜀攀琀琀攀爀⠀✀匀攀挀漀渀搀猀✀Ⰰ ㄀⤀Ⰰഀഀ
     // while ISO 8601 requires fractions to be prefixed with `.` or `,`਍     ⼀⼀ 眀攀 挀愀渀 戀攀 樀甀猀琀 猀愀昀攀氀礀 爀攀氀礀 漀渀 甀猀椀渀最 怀猀猀猀怀 猀椀渀挀攀 眀攀 挀甀爀爀攀渀琀氀礀 搀漀渀✀琀 猀甀瀀瀀漀爀琀 猀椀渀最氀攀 漀爀 琀眀漀 搀椀最椀琀 昀爀愀挀琀椀漀渀猀ഀഀ
   sss: dateGetter('Milliseconds', 3),਍  䔀䔀䔀䔀㨀 搀愀琀攀匀琀爀䜀攀琀琀攀爀⠀✀䐀愀礀✀⤀Ⰰഀഀ
   EEE: dateStrGetter('Day', true),਍     愀㨀 愀洀瀀洀䜀攀琀琀攀爀Ⰰഀഀ
     Z: timeZoneGetter਍紀㬀ഀഀ
਍瘀愀爀 䐀䄀吀䔀开䘀伀刀䴀䄀吀匀开匀倀䰀䤀吀 㴀 ⼀⠀⠀㼀㨀嬀帀礀䴀搀䠀栀洀猀愀娀䔀✀崀⬀⤀簀⠀㼀㨀✀⠀㼀㨀嬀帀✀崀簀✀✀⤀⨀✀⤀簀⠀㼀㨀䔀⬀簀礀⬀簀䴀⬀簀搀⬀簀䠀⬀簀栀⬀簀洀⬀簀猀⬀簀愀簀娀⤀⤀⠀⸀⨀⤀⼀Ⰰഀഀ
    NUMBER_STRING = /^\-?\d+$/;਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀椀氀琀攀爀ഀഀ
 * @name ng.filter:date਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 *   Formats `date` to a string based on the requested `format`.਍ ⨀ഀഀ
 *   `format` string can be composed of the following elements:਍ ⨀ഀഀ
 *   * `'yyyy'`: 4 digit representation of year (e.g. AD 1 => 0001, AD 2010 => 2010)਍ ⨀   ⨀ 怀✀礀礀✀怀㨀 ㈀ 搀椀最椀琀 爀攀瀀爀攀猀攀渀琀愀琀椀漀渀 漀昀 礀攀愀爀Ⰰ 瀀愀搀搀攀搀 ⠀　　ⴀ㤀㤀⤀⸀ ⠀攀⸀最⸀ 䄀䐀 ㈀　　㄀ 㴀㸀 　㄀Ⰰ 䄀䐀 ㈀　㄀　 㴀㸀 ㄀　⤀ഀഀ
 *   * `'y'`: 1 digit representation of year, e.g. (AD 1 => 1, AD 199 => 199)਍ ⨀   ⨀ 怀✀䴀䴀䴀䴀✀怀㨀 䴀漀渀琀栀 椀渀 礀攀愀爀 ⠀䨀愀渀甀愀爀礀ⴀ䐀攀挀攀洀戀攀爀⤀ഀഀ
 *   * `'MMM'`: Month in year (Jan-Dec)਍ ⨀   ⨀ 怀✀䴀䴀✀怀㨀 䴀漀渀琀栀 椀渀 礀攀愀爀Ⰰ 瀀愀搀搀攀搀 ⠀　㄀ⴀ㄀㈀⤀ഀഀ
 *   * `'M'`: Month in year (1-12)਍ ⨀   ⨀ 怀✀搀搀✀怀㨀 䐀愀礀 椀渀 洀漀渀琀栀Ⰰ 瀀愀搀搀攀搀 ⠀　㄀ⴀ㌀㄀⤀ഀഀ
 *   * `'d'`: Day in month (1-31)਍ ⨀   ⨀ 怀✀䔀䔀䔀䔀✀怀㨀 䐀愀礀 椀渀 圀攀攀欀Ⰰ⠀匀甀渀搀愀礀ⴀ匀愀琀甀爀搀愀礀⤀ഀഀ
 *   * `'EEE'`: Day in Week, (Sun-Sat)਍ ⨀   ⨀ 怀✀䠀䠀✀怀㨀 䠀漀甀爀 椀渀 搀愀礀Ⰰ 瀀愀搀搀攀搀 ⠀　　ⴀ㈀㌀⤀ഀഀ
 *   * `'H'`: Hour in day (0-23)਍ ⨀   ⨀ 怀✀栀栀✀怀㨀 䠀漀甀爀 椀渀 愀洀⼀瀀洀Ⰰ 瀀愀搀搀攀搀 ⠀　㄀ⴀ㄀㈀⤀ഀഀ
 *   * `'h'`: Hour in am/pm, (1-12)਍ ⨀   ⨀ 怀✀洀洀✀怀㨀 䴀椀渀甀琀攀 椀渀 栀漀甀爀Ⰰ 瀀愀搀搀攀搀 ⠀　　ⴀ㔀㤀⤀ഀഀ
 *   * `'m'`: Minute in hour (0-59)਍ ⨀   ⨀ 怀✀猀猀✀怀㨀 匀攀挀漀渀搀 椀渀 洀椀渀甀琀攀Ⰰ 瀀愀搀搀攀搀 ⠀　　ⴀ㔀㤀⤀ഀഀ
 *   * `'s'`: Second in minute (0-59)਍ ⨀   ⨀ 怀✀⸀猀猀猀✀ 漀爀 ✀Ⰰ猀猀猀✀怀㨀 䴀椀氀氀椀猀攀挀漀渀搀 椀渀 猀攀挀漀渀搀Ⰰ 瀀愀搀搀攀搀 ⠀　　　ⴀ㤀㤀㤀⤀ഀഀ
 *   * `'a'`: am/pm marker਍ ⨀   ⨀ 怀✀娀✀怀㨀 㐀 搀椀最椀琀 ⠀⬀猀椀最渀⤀ 爀攀瀀爀攀猀攀渀琀愀琀椀漀渀 漀昀 琀栀攀 琀椀洀攀稀漀渀攀 漀昀昀猀攀琀 ⠀ⴀ㄀㈀　　ⴀ⬀㄀㈀　　⤀ഀഀ
 *਍ ⨀   怀昀漀爀洀愀琀怀 猀琀爀椀渀最 挀愀渀 愀氀猀漀 戀攀 漀渀攀 漀昀 琀栀攀 昀漀氀氀漀眀椀渀最 瀀爀攀搀攀昀椀渀攀搀ഀഀ
 *   {@link guide/i18n localizable formats}:਍ ⨀ഀഀ
 *   * `'medium'`: equivalent to `'MMM d, y h:mm:ss a'` for en_US locale਍ ⨀     ⠀攀⸀最⸀ 匀攀瀀 ㌀Ⰰ ㈀　㄀　 ㄀㈀㨀　㔀㨀　㠀 瀀洀⤀ഀഀ
 *   * `'short'`: equivalent to `'M/d/yy h:mm a'` for en_US  locale (e.g. 9/3/10 12:05 pm)਍ ⨀   ⨀ 怀✀昀甀氀氀䐀愀琀攀✀怀㨀 攀焀甀椀瘀愀氀攀渀琀 琀漀 怀✀䔀䔀䔀䔀Ⰰ 䴀䴀䴀䴀 搀Ⰰ礀✀怀 昀漀爀 攀渀开唀匀  氀漀挀愀氀攀ഀഀ
 *     (e.g. Friday, September 3, 2010)਍ ⨀   ⨀ 怀✀氀漀渀最䐀愀琀攀✀怀㨀 攀焀甀椀瘀愀氀攀渀琀 琀漀 怀✀䴀䴀䴀䴀 搀Ⰰ 礀✀怀 昀漀爀 攀渀开唀匀  氀漀挀愀氀攀 ⠀攀⸀最⸀ 匀攀瀀琀攀洀戀攀爀 ㌀Ⰰ ㈀　㄀　⤀ഀഀ
 *   * `'mediumDate'`: equivalent to `'MMM d, y'` for en_US  locale (e.g. Sep 3, 2010)਍ ⨀   ⨀ 怀✀猀栀漀爀琀䐀愀琀攀✀怀㨀 攀焀甀椀瘀愀氀攀渀琀 琀漀 怀✀䴀⼀搀⼀礀礀✀怀 昀漀爀 攀渀开唀匀 氀漀挀愀氀攀 ⠀攀⸀最⸀ 㤀⼀㌀⼀㄀　⤀ഀഀ
 *   * `'mediumTime'`: equivalent to `'h:mm:ss a'` for en_US locale (e.g. 12:05:08 pm)਍ ⨀   ⨀ 怀✀猀栀漀爀琀吀椀洀攀✀怀㨀 攀焀甀椀瘀愀氀攀渀琀 琀漀 怀✀栀㨀洀洀 愀✀怀 昀漀爀 攀渀开唀匀 氀漀挀愀氀攀 ⠀攀⸀最⸀ ㄀㈀㨀　㔀 瀀洀⤀ഀഀ
 *਍ ⨀   怀昀漀爀洀愀琀怀 猀琀爀椀渀最 挀愀渀 挀漀渀琀愀椀渀 氀椀琀攀爀愀氀 瘀愀氀甀攀猀⸀ 吀栀攀猀攀 渀攀攀搀 琀漀 戀攀 焀甀漀琀攀搀 眀椀琀栀 猀椀渀最氀攀 焀甀漀琀攀猀 ⠀攀⸀最⸀ഀഀ
 *   `"h 'in the morning'"`). In order to output single quote, use two single quotes in a sequence਍ ⨀   ⠀攀⸀最⸀ 怀∀栀 ✀漀✀✀挀氀漀挀欀✀∀怀⤀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀⠀䐀愀琀攀簀渀甀洀戀攀爀簀猀琀爀椀渀最⤀紀 搀愀琀攀 䐀愀琀攀 琀漀 昀漀爀洀愀琀 攀椀琀栀攀爀 愀猀 䐀愀琀攀 漀戀樀攀挀琀Ⰰ 洀椀氀氀椀猀攀挀漀渀搀猀 ⠀猀琀爀椀渀最 漀爀ഀഀ
 *    number) or various ISO 8601 datetime string formats (e.g. yyyy-MM-ddTHH:mm:ss.SSSZ and its਍ ⨀    猀栀漀爀琀攀爀 瘀攀爀猀椀漀渀猀 氀椀欀攀 礀礀礀礀ⴀ䴀䴀ⴀ搀搀吀䠀䠀㨀洀洀娀Ⰰ 礀礀礀礀ⴀ䴀䴀ⴀ搀搀 漀爀 礀礀礀礀䴀䴀搀搀吀䠀䠀洀洀猀猀娀⤀⸀ 䤀昀 渀漀 琀椀洀攀稀漀渀攀 椀猀ഀഀ
 *    specified in the string input, the time is considered to be in the local timezone.਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 昀漀爀洀愀琀 䘀漀爀洀愀琀琀椀渀最 爀甀氀攀猀 ⠀猀攀攀 䐀攀猀挀爀椀瀀琀椀漀渀⤀⸀ 䤀昀 渀漀琀 猀瀀攀挀椀昀椀攀搀Ⰰഀഀ
 *    `mediumDate` is used.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀猀琀爀椀渀最紀 䘀漀爀洀愀琀琀攀搀 猀琀爀椀渀最 漀爀 琀栀攀 椀渀瀀甀琀 椀昀 椀渀瀀甀琀 椀猀 渀漀琀 爀攀挀漀最渀椀稀攀搀 愀猀 搀愀琀攀⼀洀椀氀氀椀猀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
   <doc:example>਍     㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
       <span ng-non-bindable>{{1288323623006 | date:'medium'}}</span>:਍           笀笀㄀㈀㠀㠀㌀㈀㌀㘀㈀㌀　　㘀 簀 搀愀琀攀㨀✀洀攀搀椀甀洀✀紀紀㰀戀爀㸀ഀഀ
       <span ng-non-bindable>{{1288323623006 | date:'yyyy-MM-dd HH:mm:ss Z'}}</span>:਍          笀笀㄀㈀㠀㠀㌀㈀㌀㘀㈀㌀　　㘀 簀 搀愀琀攀㨀✀礀礀礀礀ⴀ䴀䴀ⴀ搀搀 䠀䠀㨀洀洀㨀猀猀 娀✀紀紀㰀戀爀㸀ഀഀ
       <span ng-non-bindable>{{1288323623006 | date:'MM/dd/yyyy @ h:mma'}}</span>:਍          笀笀✀㄀㈀㠀㠀㌀㈀㌀㘀㈀㌀　　㘀✀ 簀 搀愀琀攀㨀✀䴀䴀⼀搀搀⼀礀礀礀礀 䀀 栀㨀洀洀愀✀紀紀㰀戀爀㸀ഀഀ
     </doc:source>਍     㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
       it('should format date', function() {਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀∀㄀㈀㠀㠀㌀㈀㌀㘀㈀㌀　　㘀 簀 搀愀琀攀㨀✀洀攀搀椀甀洀✀∀⤀⤀⸀ഀഀ
            toMatch(/Oct 2\d, 2010 \d{1,2}:\d{2}:\d{2} (AM|PM)/);਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀∀㄀㈀㠀㠀㌀㈀㌀㘀㈀㌀　　㘀 簀 搀愀琀攀㨀✀礀礀礀礀ⴀ䴀䴀ⴀ搀搀 䠀䠀㨀洀洀㨀猀猀 娀✀∀⤀⤀⸀ഀഀ
            toMatch(/2010\-10\-2\d \d{2}:\d{2}:\d{2} (\-|\+)?\d{4}/);਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀∀✀㄀㈀㠀㠀㌀㈀㌀㘀㈀㌀　　㘀✀ 簀 搀愀琀攀㨀✀䴀䴀⼀搀搀⼀礀礀礀礀 䀀 栀㨀洀洀愀✀∀⤀⤀⸀ഀഀ
            toMatch(/10\/2\d\/2010 @ \d{1,2}:\d{2}(AM|PM)/);਍       紀⤀㬀ഀഀ
     </doc:scenario>਍   㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍搀愀琀攀䘀椀氀琀攀爀⸀␀椀渀樀攀挀琀 㴀 嬀✀␀氀漀挀愀氀攀✀崀㬀ഀഀ
function dateFilter($locale) {਍ഀഀ
਍  瘀愀爀 刀开䤀匀伀㠀㘀　㄀开匀吀刀 㴀 ⼀帀⠀尀搀笀㐀紀⤀ⴀ㼀⠀尀搀尀搀⤀ⴀ㼀⠀尀搀尀搀⤀⠀㼀㨀吀⠀尀搀尀搀⤀⠀㼀㨀㨀㼀⠀尀搀尀搀⤀⠀㼀㨀㨀㼀⠀尀搀尀搀⤀⠀㼀㨀尀⸀⠀尀搀⬀⤀⤀㼀⤀㼀⤀㼀⠀娀簀⠀嬀⬀ⴀ崀⤀⠀尀搀尀搀⤀㨀㼀⠀尀搀尀搀⤀⤀㼀⤀㼀␀⼀㬀ഀഀ
                     // 1        2       3         4          5          6          7          8  9     10      11਍  昀甀渀挀琀椀漀渀 樀猀漀渀匀琀爀椀渀最吀漀䐀愀琀攀⠀猀琀爀椀渀最⤀ 笀ഀഀ
    var match;਍    椀昀 ⠀洀愀琀挀栀 㴀 猀琀爀椀渀最⸀洀愀琀挀栀⠀刀开䤀匀伀㠀㘀　㄀开匀吀刀⤀⤀ 笀ഀഀ
      var date = new Date(0),਍          琀稀䠀漀甀爀 㴀 　Ⰰഀഀ
          tzMin  = 0,਍          搀愀琀攀匀攀琀琀攀爀 㴀 洀愀琀挀栀嬀㠀崀 㼀 搀愀琀攀⸀猀攀琀唀吀䌀䘀甀氀氀夀攀愀爀 㨀 搀愀琀攀⸀猀攀琀䘀甀氀氀夀攀愀爀Ⰰഀഀ
          timeSetter = match[8] ? date.setUTCHours : date.setHours;਍ഀഀ
      if (match[9]) {਍        琀稀䠀漀甀爀 㴀 椀渀琀⠀洀愀琀挀栀嬀㤀崀 ⬀ 洀愀琀挀栀嬀㄀　崀⤀㬀ഀഀ
        tzMin = int(match[9] + match[11]);਍      紀ഀഀ
      dateSetter.call(date, int(match[1]), int(match[2]) - 1, int(match[3]));਍      瘀愀爀 栀 㴀 椀渀琀⠀洀愀琀挀栀嬀㐀崀簀簀　⤀ ⴀ 琀稀䠀漀甀爀㬀ഀഀ
      var m = int(match[5]||0) - tzMin;਍      瘀愀爀 猀 㴀 椀渀琀⠀洀愀琀挀栀嬀㘀崀簀簀　⤀㬀ഀഀ
      var ms = Math.round(parseFloat('0.' + (match[7]||0)) * 1000);਍      琀椀洀攀匀攀琀琀攀爀⸀挀愀氀氀⠀搀愀琀攀Ⰰ 栀Ⰰ 洀Ⰰ 猀Ⰰ 洀猀⤀㬀ഀഀ
      return date;਍    紀ഀഀ
    return string;਍  紀ഀഀ
਍ഀഀ
  return function(date, format) {਍    瘀愀爀 琀攀砀琀 㴀 ✀✀Ⰰഀഀ
        parts = [],਍        昀渀Ⰰ 洀愀琀挀栀㬀ഀഀ
਍    昀漀爀洀愀琀 㴀 昀漀爀洀愀琀 簀簀 ✀洀攀搀椀甀洀䐀愀琀攀✀㬀ഀഀ
    format = $locale.DATETIME_FORMATS[format] || format;਍    椀昀 ⠀椀猀匀琀爀椀渀最⠀搀愀琀攀⤀⤀ 笀ഀഀ
      if (NUMBER_STRING.test(date)) {਍        搀愀琀攀 㴀 椀渀琀⠀搀愀琀攀⤀㬀ഀഀ
      } else {਍        搀愀琀攀 㴀 樀猀漀渀匀琀爀椀渀最吀漀䐀愀琀攀⠀搀愀琀攀⤀㬀ഀഀ
      }਍    紀ഀഀ
਍    椀昀 ⠀椀猀一甀洀戀攀爀⠀搀愀琀攀⤀⤀ 笀ഀഀ
      date = new Date(date);਍    紀ഀഀ
਍    椀昀 ⠀℀椀猀䐀愀琀攀⠀搀愀琀攀⤀⤀ 笀ഀഀ
      return date;਍    紀ഀഀ
਍    眀栀椀氀攀⠀昀漀爀洀愀琀⤀ 笀ഀഀ
      match = DATE_FORMATS_SPLIT.exec(format);਍      椀昀 ⠀洀愀琀挀栀⤀ 笀ഀഀ
        parts = concat(parts, match, 1);਍        昀漀爀洀愀琀 㴀 瀀愀爀琀猀⸀瀀漀瀀⠀⤀㬀ഀഀ
      } else {਍        瀀愀爀琀猀⸀瀀甀猀栀⠀昀漀爀洀愀琀⤀㬀ഀഀ
        format = null;਍      紀ഀഀ
    }਍ഀഀ
    forEach(parts, function(value){਍      昀渀 㴀 䐀䄀吀䔀开䘀伀刀䴀䄀吀匀嬀瘀愀氀甀攀崀㬀ഀഀ
      text += fn ? fn(date, $locale.DATETIME_FORMATS)਍                 㨀 瘀愀氀甀攀⸀爀攀瀀氀愀挀攀⠀⼀⠀帀✀簀✀␀⤀⼀最Ⰰ ✀✀⤀⸀爀攀瀀氀愀挀攀⠀⼀✀✀⼀最Ⰰ ∀✀∀⤀㬀ഀഀ
    });਍ഀഀ
    return text;਍  紀㬀ഀഀ
}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc filter਍ ⨀ 䀀渀愀洀攀 渀最⸀昀椀氀琀攀爀㨀樀猀漀渀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀   䄀氀氀漀眀猀 礀漀甀 琀漀 挀漀渀瘀攀爀琀 愀 䨀愀瘀愀匀挀爀椀瀀琀 漀戀樀攀挀琀 椀渀琀漀 䨀匀伀一 猀琀爀椀渀最⸀ഀഀ
 *਍ ⨀   吀栀椀猀 昀椀氀琀攀爀 椀猀 洀漀猀琀氀礀 甀猀攀昀甀氀 昀漀爀 搀攀戀甀最最椀渀最⸀ 圀栀攀渀 甀猀椀渀最 琀栀攀 搀漀甀戀氀攀 挀甀爀氀礀 笀笀瘀愀氀甀攀紀紀 渀漀琀愀琀椀漀渀ഀഀ
 *   the binding is automatically converted to JSON.਍ ⨀ഀഀ
 * @param {*} object Any JavaScript object (including arrays and primitive types) to filter.਍ ⨀ 䀀爀攀琀甀爀渀猀 笀猀琀爀椀渀最紀 䨀匀伀一 猀琀爀椀渀最⸀ഀഀ
 *਍ ⨀ഀഀ
 * @example:਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍       㰀瀀爀攀㸀笀笀 笀✀渀愀洀攀✀㨀✀瘀愀氀甀攀✀紀 簀 樀猀漀渀 紀紀㰀⼀瀀爀攀㸀ഀഀ
     </doc:source>਍     㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
       it('should jsonify filtered objects', function() {਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀∀笀✀渀愀洀攀✀㨀✀瘀愀氀甀攀✀紀∀⤀⤀⸀琀漀䴀愀琀挀栀⠀⼀尀笀尀渀  ∀渀愀洀攀∀㨀 㼀∀瘀愀氀甀攀∀尀渀紀⼀⤀㬀ഀഀ
       });਍     㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
   </doc:example>਍ ⨀ഀഀ
 */਍昀甀渀挀琀椀漀渀 樀猀漀渀䘀椀氀琀攀爀⠀⤀ 笀ഀഀ
  return function(object) {਍    爀攀琀甀爀渀 琀漀䨀猀漀渀⠀漀戀樀攀挀琀Ⰰ 琀爀甀攀⤀㬀ഀഀ
  };਍紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 昀椀氀琀攀爀ഀഀ
 * @name ng.filter:lowercase਍ ⨀ 䀀昀甀渀挀琀椀漀渀ഀഀ
 * @description਍ ⨀ 䌀漀渀瘀攀爀琀猀 猀琀爀椀渀最 琀漀 氀漀眀攀爀挀愀猀攀⸀ഀഀ
 * @see angular.lowercase਍ ⨀⼀ഀഀ
var lowercaseFilter = valueFn(lowercase);਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc filter਍ ⨀ 䀀渀愀洀攀 渀最⸀昀椀氀琀攀爀㨀甀瀀瀀攀爀挀愀猀攀ഀഀ
 * @function਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Converts string to uppercase.਍ ⨀ 䀀猀攀攀 愀渀最甀氀愀爀⸀甀瀀瀀攀爀挀愀猀攀ഀഀ
 */਍瘀愀爀 甀瀀瀀攀爀挀愀猀攀䘀椀氀琀攀爀 㴀 瘀愀氀甀攀䘀渀⠀甀瀀瀀攀爀挀愀猀攀⤀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 渀最⸀昀椀氀琀攀爀㨀氀椀洀椀琀吀漀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ 䌀爀攀愀琀攀猀 愀 渀攀眀 愀爀爀愀礀 漀爀 猀琀爀椀渀最 挀漀渀琀愀椀渀椀渀最 漀渀氀礀 愀 猀瀀攀挀椀昀椀攀搀 渀甀洀戀攀爀 漀昀 攀氀攀洀攀渀琀猀⸀ 吀栀攀 攀氀攀洀攀渀琀猀ഀഀ
 * are taken from either the beginning or the end of the source array or string, as specified by਍ ⨀ 琀栀攀 瘀愀氀甀攀 愀渀搀 猀椀最渀 ⠀瀀漀猀椀琀椀瘀攀 漀爀 渀攀最愀琀椀瘀攀⤀ 漀昀 怀氀椀洀椀琀怀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀䄀爀爀愀礀簀猀琀爀椀渀最紀 椀渀瀀甀琀 匀漀甀爀挀攀 愀爀爀愀礀 漀爀 猀琀爀椀渀最 琀漀 戀攀 氀椀洀椀琀攀搀⸀ഀഀ
 * @param {string|number} limit The length of the returned array or string. If the `limit` number ਍ ⨀     椀猀 瀀漀猀椀琀椀瘀攀Ⰰ 怀氀椀洀椀琀怀 渀甀洀戀攀爀 漀昀 椀琀攀洀猀 昀爀漀洀 琀栀攀 戀攀最椀渀渀椀渀最 漀昀 琀栀攀 猀漀甀爀挀攀 愀爀爀愀礀⼀猀琀爀椀渀最 愀爀攀 挀漀瀀椀攀搀⸀ഀഀ
 *     If the number is negative, `limit` number  of items from the end of the source array/string ਍ ⨀     愀爀攀 挀漀瀀椀攀搀⸀ 吀栀攀 怀氀椀洀椀琀怀 眀椀氀氀 戀攀 琀爀椀洀洀攀搀 椀昀 椀琀 攀砀挀攀攀搀猀 怀愀爀爀愀礀⸀氀攀渀最琀栀怀ഀഀ
 * @returns {Array|string} A new sub-array or substring of length `limit` or less if input array਍ ⨀     栀愀搀 氀攀猀猀 琀栀愀渀 怀氀椀洀椀琀怀 攀氀攀洀攀渀琀猀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
   <doc:example>਍     㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
       <script>਍         昀甀渀挀琀椀漀渀 䌀琀爀氀⠀␀猀挀漀瀀攀⤀ 笀ഀഀ
           $scope.numbers = [1,2,3,4,5,6,7,8,9];਍           ␀猀挀漀瀀攀⸀氀攀琀琀攀爀猀 㴀 ∀愀戀挀搀攀昀最栀椀∀㬀ഀഀ
           $scope.numLimit = 3;਍           ␀猀挀漀瀀攀⸀氀攀琀琀攀爀䰀椀洀椀琀 㴀 ㌀㬀ഀഀ
         }਍       㰀⼀猀挀爀椀瀀琀㸀ഀഀ
       <div ng-controller="Ctrl">਍         䰀椀洀椀琀 笀笀渀甀洀戀攀爀猀紀紀 琀漀㨀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀椀渀琀攀最攀爀∀ 渀最ⴀ洀漀搀攀氀㴀∀渀甀洀䰀椀洀椀琀∀㸀ഀഀ
         <p>Output numbers: {{ numbers | limitTo:numLimit }}</p>਍         䰀椀洀椀琀 笀笀氀攀琀琀攀爀猀紀紀 琀漀㨀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀椀渀琀攀最攀爀∀ 渀最ⴀ洀漀搀攀氀㴀∀氀攀琀琀攀爀䰀椀洀椀琀∀㸀ഀഀ
         <p>Output letters: {{ letters | limitTo:letterLimit }}</p>਍       㰀⼀搀椀瘀㸀ഀഀ
     </doc:source>਍     㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
       it('should limit the number array to first three items', function() {਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 椀渀瀀甀琀嬀渀最ⴀ洀漀搀攀氀㴀渀甀洀䰀椀洀椀琀崀✀⤀⸀瘀愀氀⠀⤀⤀⸀琀漀䈀攀⠀✀㌀✀⤀㬀ഀഀ
         expect(element('.doc-example-live input[ng-model=letterLimit]').val()).toBe('3');਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀渀甀洀戀攀爀猀 簀 氀椀洀椀琀吀漀㨀渀甀洀䰀椀洀椀琀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀嬀㄀Ⰰ㈀Ⰰ㌀崀✀⤀㬀ഀഀ
         expect(binding('letters | limitTo:letterLimit')).toEqual('abc');਍       紀⤀㬀ഀഀ
਍       椀琀⠀✀猀栀漀甀氀搀 甀瀀搀愀琀攀 琀栀攀 漀甀琀瀀甀琀 眀栀攀渀 ⴀ㌀ 椀猀 攀渀琀攀爀攀搀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         input('numLimit').enter(-3);਍         椀渀瀀甀琀⠀✀氀攀琀琀攀爀䰀椀洀椀琀✀⤀⸀攀渀琀攀爀⠀ⴀ㌀⤀㬀ഀഀ
         expect(binding('numbers | limitTo:numLimit')).toEqual('[7,8,9]');਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀氀攀琀琀攀爀猀 簀 氀椀洀椀琀吀漀㨀氀攀琀琀攀爀䰀椀洀椀琀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀最栀椀✀⤀㬀ഀഀ
       });਍ഀഀ
       it('should not exceed the maximum size of input array', function() {਍         椀渀瀀甀琀⠀✀渀甀洀䰀椀洀椀琀✀⤀⸀攀渀琀攀爀⠀㄀　　⤀㬀ഀഀ
         input('letterLimit').enter(100);਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀渀甀洀戀攀爀猀 簀 氀椀洀椀琀吀漀㨀渀甀洀䰀椀洀椀琀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀嬀㄀Ⰰ㈀Ⰰ㌀Ⰰ㐀Ⰰ㔀Ⰰ㘀Ⰰ㜀Ⰰ㠀Ⰰ㤀崀✀⤀㬀ഀഀ
         expect(binding('letters | limitTo:letterLimit')).toEqual('abcdefghi');਍       紀⤀㬀ഀഀ
     </doc:scenario>਍   㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍昀甀渀挀琀椀漀渀 氀椀洀椀琀吀漀䘀椀氀琀攀爀⠀⤀笀ഀഀ
  return function(input, limit) {਍    椀昀 ⠀℀椀猀䄀爀爀愀礀⠀椀渀瀀甀琀⤀ ☀☀ ℀椀猀匀琀爀椀渀最⠀椀渀瀀甀琀⤀⤀ 爀攀琀甀爀渀 椀渀瀀甀琀㬀ഀഀ
    ਍    氀椀洀椀琀 㴀 椀渀琀⠀氀椀洀椀琀⤀㬀ഀഀ
਍    椀昀 ⠀椀猀匀琀爀椀渀最⠀椀渀瀀甀琀⤀⤀ 笀ഀഀ
      //NaN check on limit਍      椀昀 ⠀氀椀洀椀琀⤀ 笀ഀഀ
        return limit >= 0 ? input.slice(0, limit) : input.slice(limit, input.length);਍      紀 攀氀猀攀 笀ഀഀ
        return "";਍      紀ഀഀ
    }਍ഀഀ
    var out = [],਍      椀Ⰰ 渀㬀ഀഀ
਍    ⼀⼀ 椀昀 愀戀猀⠀氀椀洀椀琀⤀ 攀砀挀攀攀搀猀 洀愀砀椀洀甀洀 氀攀渀最琀栀Ⰰ 琀爀椀洀 椀琀ഀഀ
    if (limit > input.length)਍      氀椀洀椀琀 㴀 椀渀瀀甀琀⸀氀攀渀最琀栀㬀ഀഀ
    else if (limit < -input.length)਍      氀椀洀椀琀 㴀 ⴀ椀渀瀀甀琀⸀氀攀渀最琀栀㬀ഀഀ
਍    椀昀 ⠀氀椀洀椀琀 㸀 　⤀ 笀ഀഀ
      i = 0;਍      渀 㴀 氀椀洀椀琀㬀ഀഀ
    } else {਍      椀 㴀 椀渀瀀甀琀⸀氀攀渀最琀栀 ⬀ 氀椀洀椀琀㬀ഀഀ
      n = input.length;਍    紀ഀഀ
਍    昀漀爀 ⠀㬀 椀㰀渀㬀 椀⬀⬀⤀ 笀ഀഀ
      out.push(input[i]);਍    紀ഀഀ
਍    爀攀琀甀爀渀 漀甀琀㬀ഀഀ
  };਍紀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc function਍ ⨀ 䀀渀愀洀攀 渀最⸀昀椀氀琀攀爀㨀漀爀搀攀爀䈀礀ഀഀ
 * @function਍ ⨀ഀഀ
 * @description਍ ⨀ 伀爀搀攀爀猀 愀 猀瀀攀挀椀昀椀攀搀 怀愀爀爀愀礀怀 戀礀 琀栀攀 怀攀砀瀀爀攀猀猀椀漀渀怀 瀀爀攀搀椀挀愀琀攀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀䄀爀爀愀礀紀 愀爀爀愀礀 吀栀攀 愀爀爀愀礀 琀漀 猀漀爀琀⸀ഀഀ
 * @param {function(*)|string|Array.<(function(*)|string)>} expression A predicate to be਍ ⨀    甀猀攀搀 戀礀 琀栀攀 挀漀洀瀀愀爀愀琀漀爀 琀漀 搀攀琀攀爀洀椀渀攀 琀栀攀 漀爀搀攀爀 漀昀 攀氀攀洀攀渀琀猀⸀ഀഀ
 *਍ ⨀    䌀愀渀 戀攀 漀渀攀 漀昀㨀ഀഀ
 *਍ ⨀    ⴀ 怀昀甀渀挀琀椀漀渀怀㨀 䜀攀琀琀攀爀 昀甀渀挀琀椀漀渀⸀ 吀栀攀 爀攀猀甀氀琀 漀昀 琀栀椀猀 昀甀渀挀琀椀漀渀 眀椀氀氀 戀攀 猀漀爀琀攀搀 甀猀椀渀最 琀栀攀ഀഀ
 *      `<`, `=`, `>` operator.਍ ⨀    ⴀ 怀猀琀爀椀渀最怀㨀 䄀渀 䄀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 眀栀椀挀栀 攀瘀愀氀甀愀琀攀猀 琀漀 愀渀 漀戀樀攀挀琀 琀漀 漀爀搀攀爀 戀礀Ⰰ 猀甀挀栀 愀猀 ✀渀愀洀攀✀ഀഀ
 *      to sort by a property called 'name'. Optionally prefixed with `+` or `-` to control਍ ⨀      愀猀挀攀渀搀椀渀最 漀爀 搀攀猀挀攀渀搀椀渀最 猀漀爀琀 漀爀搀攀爀 ⠀昀漀爀 攀砀愀洀瀀氀攀Ⰰ ⬀渀愀洀攀 漀爀 ⴀ渀愀洀攀⤀⸀ഀഀ
 *    - `Array`: An array of function or string predicates. The first predicate in the array਍ ⨀      椀猀 甀猀攀搀 昀漀爀 猀漀爀琀椀渀最Ⰰ 戀甀琀 眀栀攀渀 琀眀漀 椀琀攀洀猀 愀爀攀 攀焀甀椀瘀愀氀攀渀琀Ⰰ 琀栀攀 渀攀砀琀 瀀爀攀搀椀挀愀琀攀 椀猀 甀猀攀搀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀戀漀漀氀攀愀渀㴀紀 爀攀瘀攀爀猀攀 刀攀瘀攀爀猀攀 琀栀攀 漀爀搀攀爀 琀栀攀 愀爀爀愀礀⸀ഀഀ
 * @returns {Array} Sorted copy of the source array.਍ ⨀ഀഀ
 * @example਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍       㰀猀挀爀椀瀀琀㸀ഀഀ
         function Ctrl($scope) {਍           ␀猀挀漀瀀攀⸀昀爀椀攀渀搀猀 㴀ഀഀ
               [{name:'John', phone:'555-1212', age:10},਍                笀渀愀洀攀㨀✀䴀愀爀礀✀Ⰰ 瀀栀漀渀攀㨀✀㔀㔀㔀ⴀ㤀㠀㜀㘀✀Ⰰ 愀最攀㨀㄀㤀紀Ⰰഀഀ
                {name:'Mike', phone:'555-4321', age:21},਍                笀渀愀洀攀㨀✀䄀搀愀洀✀Ⰰ 瀀栀漀渀攀㨀✀㔀㔀㔀ⴀ㔀㘀㜀㠀✀Ⰰ 愀最攀㨀㌀㔀紀Ⰰഀഀ
                {name:'Julie', phone:'555-8765', age:29}]਍           ␀猀挀漀瀀攀⸀瀀爀攀搀椀挀愀琀攀 㴀 ✀ⴀ愀最攀✀㬀ഀഀ
         }਍       㰀⼀猀挀爀椀瀀琀㸀ഀഀ
       <div ng-controller="Ctrl">਍         㰀瀀爀攀㸀匀漀爀琀椀渀最 瀀爀攀搀椀挀愀琀攀 㴀 笀笀瀀爀攀搀椀挀愀琀攀紀紀㬀 爀攀瘀攀爀猀攀 㴀 笀笀爀攀瘀攀爀猀攀紀紀㰀⼀瀀爀攀㸀ഀഀ
         <hr/>਍         嬀 㰀愀 栀爀攀昀㴀∀∀ 渀最ⴀ挀氀椀挀欀㴀∀瀀爀攀搀椀挀愀琀攀㴀✀✀∀㸀甀渀猀漀爀琀攀搀㰀⼀愀㸀 崀ഀഀ
         <table class="friend">਍           㰀琀爀㸀ഀഀ
             <th><a href="" ng-click="predicate = 'name'; reverse=false">Name</a>਍                 ⠀㰀愀 栀爀攀昀㴀∀∀ 渀最ⴀ挀氀椀挀欀㴀∀瀀爀攀搀椀挀愀琀攀 㴀 ✀ⴀ渀愀洀攀✀㬀 爀攀瘀攀爀猀攀㴀昀愀氀猀攀∀㸀帀㰀⼀愀㸀⤀㰀⼀琀栀㸀ഀഀ
             <th><a href="" ng-click="predicate = 'phone'; reverse=!reverse">Phone Number</a></th>਍             㰀琀栀㸀㰀愀 栀爀攀昀㴀∀∀ 渀最ⴀ挀氀椀挀欀㴀∀瀀爀攀搀椀挀愀琀攀 㴀 ✀愀最攀✀㬀 爀攀瘀攀爀猀攀㴀℀爀攀瘀攀爀猀攀∀㸀䄀最攀㰀⼀愀㸀㰀⼀琀栀㸀ഀഀ
           </tr>਍           㰀琀爀 渀最ⴀ爀攀瀀攀愀琀㴀∀昀爀椀攀渀搀 椀渀 昀爀椀攀渀搀猀 簀 漀爀搀攀爀䈀礀㨀瀀爀攀搀椀挀愀琀攀㨀爀攀瘀攀爀猀攀∀㸀ഀഀ
             <td>{{friend.name}}</td>਍             㰀琀搀㸀笀笀昀爀椀攀渀搀⸀瀀栀漀渀攀紀紀㰀⼀琀搀㸀ഀഀ
             <td>{{friend.age}}</td>਍           㰀⼀琀爀㸀ഀഀ
         </table>਍       㰀⼀搀椀瘀㸀ഀഀ
     </doc:source>਍     㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
       it('should be reverse ordered by aged', function() {਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀瀀爀攀搀椀挀愀琀攀✀⤀⤀⸀琀漀䈀攀⠀✀ⴀ愀最攀✀⤀㬀ഀഀ
         expect(repeater('table.friend', 'friend in friends').column('friend.age')).਍           琀漀䔀焀甀愀氀⠀嬀✀㌀㔀✀Ⰰ ✀㈀㤀✀Ⰰ ✀㈀㄀✀Ⰰ ✀㄀㤀✀Ⰰ ✀㄀　✀崀⤀㬀ഀഀ
         expect(repeater('table.friend', 'friend in friends').column('friend.name')).਍           琀漀䔀焀甀愀氀⠀嬀✀䄀搀愀洀✀Ⰰ ✀䨀甀氀椀攀✀Ⰰ ✀䴀椀欀攀✀Ⰰ ✀䴀愀爀礀✀Ⰰ ✀䨀漀栀渀✀崀⤀㬀ഀഀ
       });਍ഀഀ
       it('should reorder the table when user selects different predicate', function() {਍         攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 愀㨀挀漀渀琀愀椀渀猀⠀∀一愀洀攀∀⤀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
         expect(repeater('table.friend', 'friend in friends').column('friend.name')).਍           琀漀䔀焀甀愀氀⠀嬀✀䄀搀愀洀✀Ⰰ ✀䨀漀栀渀✀Ⰰ ✀䨀甀氀椀攀✀Ⰰ ✀䴀愀爀礀✀Ⰰ ✀䴀椀欀攀✀崀⤀㬀ഀഀ
         expect(repeater('table.friend', 'friend in friends').column('friend.age')).਍           琀漀䔀焀甀愀氀⠀嬀✀㌀㔀✀Ⰰ ✀㄀　✀Ⰰ ✀㈀㤀✀Ⰰ ✀㄀㤀✀Ⰰ ✀㈀㄀✀崀⤀㬀ഀഀ
਍         攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 愀㨀挀漀渀琀愀椀渀猀⠀∀倀栀漀渀攀∀⤀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
         expect(repeater('table.friend', 'friend in friends').column('friend.phone')).਍           琀漀䔀焀甀愀氀⠀嬀✀㔀㔀㔀ⴀ㤀㠀㜀㘀✀Ⰰ ✀㔀㔀㔀ⴀ㠀㜀㘀㔀✀Ⰰ ✀㔀㔀㔀ⴀ㔀㘀㜀㠀✀Ⰰ ✀㔀㔀㔀ⴀ㐀㌀㈀㄀✀Ⰰ ✀㔀㔀㔀ⴀ㄀㈀㄀㈀✀崀⤀㬀ഀഀ
         expect(repeater('table.friend', 'friend in friends').column('friend.name')).਍           琀漀䔀焀甀愀氀⠀嬀✀䴀愀爀礀✀Ⰰ ✀䨀甀氀椀攀✀Ⰰ ✀䄀搀愀洀✀Ⰰ ✀䴀椀欀攀✀Ⰰ ✀䨀漀栀渀✀崀⤀㬀ഀഀ
       });਍     㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
   </doc:example>਍ ⨀⼀ഀഀ
orderByFilter.$inject = ['$parse'];਍昀甀渀挀琀椀漀渀 漀爀搀攀爀䈀礀䘀椀氀琀攀爀⠀␀瀀愀爀猀攀⤀笀ഀഀ
  return function(array, sortPredicate, reverseOrder) {਍    椀昀 ⠀℀椀猀䄀爀爀愀礀⠀愀爀爀愀礀⤀⤀ 爀攀琀甀爀渀 愀爀爀愀礀㬀ഀഀ
    if (!sortPredicate) return array;਍    猀漀爀琀倀爀攀搀椀挀愀琀攀 㴀 椀猀䄀爀爀愀礀⠀猀漀爀琀倀爀攀搀椀挀愀琀攀⤀ 㼀 猀漀爀琀倀爀攀搀椀挀愀琀攀㨀 嬀猀漀爀琀倀爀攀搀椀挀愀琀攀崀㬀ഀഀ
    sortPredicate = map(sortPredicate, function(predicate){਍      瘀愀爀 搀攀猀挀攀渀搀椀渀最 㴀 昀愀氀猀攀Ⰰ 最攀琀 㴀 瀀爀攀搀椀挀愀琀攀 簀簀 椀搀攀渀琀椀琀礀㬀ഀഀ
      if (isString(predicate)) {਍        椀昀 ⠀⠀瀀爀攀搀椀挀愀琀攀⸀挀栀愀爀䄀琀⠀　⤀ 㴀㴀 ✀⬀✀ 簀簀 瀀爀攀搀椀挀愀琀攀⸀挀栀愀爀䄀琀⠀　⤀ 㴀㴀 ✀ⴀ✀⤀⤀ 笀ഀഀ
          descending = predicate.charAt(0) == '-';਍          瀀爀攀搀椀挀愀琀攀 㴀 瀀爀攀搀椀挀愀琀攀⸀猀甀戀猀琀爀椀渀最⠀㄀⤀㬀ഀഀ
        }਍        最攀琀 㴀 ␀瀀愀爀猀攀⠀瀀爀攀搀椀挀愀琀攀⤀㬀ഀഀ
      }਍      爀攀琀甀爀渀 爀攀瘀攀爀猀攀䌀漀洀瀀愀爀愀琀漀爀⠀昀甀渀挀琀椀漀渀⠀愀Ⰰ戀⤀笀ഀഀ
        return compare(get(a),get(b));਍      紀Ⰰ 搀攀猀挀攀渀搀椀渀最⤀㬀ഀഀ
    });਍    瘀愀爀 愀爀爀愀礀䌀漀瀀礀 㴀 嬀崀㬀ഀഀ
    for ( var i = 0; i < array.length; i++) { arrayCopy.push(array[i]); }਍    爀攀琀甀爀渀 愀爀爀愀礀䌀漀瀀礀⸀猀漀爀琀⠀爀攀瘀攀爀猀攀䌀漀洀瀀愀爀愀琀漀爀⠀挀漀洀瀀愀爀愀琀漀爀Ⰰ 爀攀瘀攀爀猀攀伀爀搀攀爀⤀⤀㬀ഀഀ
਍    昀甀渀挀琀椀漀渀 挀漀洀瀀愀爀愀琀漀爀⠀漀㄀Ⰰ 漀㈀⤀笀ഀഀ
      for ( var i = 0; i < sortPredicate.length; i++) {਍        瘀愀爀 挀漀洀瀀 㴀 猀漀爀琀倀爀攀搀椀挀愀琀攀嬀椀崀⠀漀㄀Ⰰ 漀㈀⤀㬀ഀഀ
        if (comp !== 0) return comp;਍      紀ഀഀ
      return 0;਍    紀ഀഀ
    function reverseComparator(comp, descending) {਍      爀攀琀甀爀渀 琀漀䈀漀漀氀攀愀渀⠀搀攀猀挀攀渀搀椀渀最⤀ഀഀ
          ? function(a,b){return comp(b,a);}਍          㨀 挀漀洀瀀㬀ഀഀ
    }਍    昀甀渀挀琀椀漀渀 挀漀洀瀀愀爀攀⠀瘀㄀Ⰰ 瘀㈀⤀笀ഀഀ
      var t1 = typeof v1;਍      瘀愀爀 琀㈀ 㴀 琀礀瀀攀漀昀 瘀㈀㬀ഀഀ
      if (t1 == t2) {਍        椀昀 ⠀琀㄀ 㴀㴀 ∀猀琀爀椀渀最∀⤀ 笀ഀഀ
           v1 = v1.toLowerCase();਍           瘀㈀ 㴀 瘀㈀⸀琀漀䰀漀眀攀爀䌀愀猀攀⠀⤀㬀ഀഀ
        }਍        椀昀 ⠀瘀㄀ 㴀㴀㴀 瘀㈀⤀ 爀攀琀甀爀渀 　㬀ഀഀ
        return v1 < v2 ? -1 : 1;਍      紀 攀氀猀攀 笀ഀഀ
        return t1 < t2 ? -1 : 1;਍      紀ഀഀ
    }਍  紀㬀ഀഀ
}਍ഀഀ
function ngDirective(directive) {਍  椀昀 ⠀椀猀䘀甀渀挀琀椀漀渀⠀搀椀爀攀挀琀椀瘀攀⤀⤀ 笀ഀഀ
    directive = {਍      氀椀渀欀㨀 搀椀爀攀挀琀椀瘀攀ഀഀ
    };਍  紀ഀഀ
  directive.restrict = directive.restrict || 'AC';਍  爀攀琀甀爀渀 瘀愀氀甀攀䘀渀⠀搀椀爀攀挀琀椀瘀攀⤀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:a਍ ⨀ 䀀爀攀猀琀爀椀挀琀 䔀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Modifies the default behavior of the html A tag so that the default action is prevented when਍ ⨀ 琀栀攀 栀爀攀昀 愀琀琀爀椀戀甀琀攀 椀猀 攀洀瀀琀礀⸀ഀഀ
 *਍ ⨀ 吀栀椀猀 挀栀愀渀最攀 瀀攀爀洀椀琀猀 琀栀攀 攀愀猀礀 挀爀攀愀琀椀漀渀 漀昀 愀挀琀椀漀渀 氀椀渀欀猀 眀椀琀栀 琀栀攀 怀渀最䌀氀椀挀欀怀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * without changing the location or causing page reloads, e.g.:਍ ⨀ 怀㰀愀 栀爀攀昀㴀∀∀ 渀最ⴀ挀氀椀挀欀㴀∀氀椀猀琀⸀愀搀搀䤀琀攀洀⠀⤀∀㸀䄀搀搀 䤀琀攀洀㰀⼀愀㸀怀ഀഀ
 */਍瘀愀爀 栀琀洀氀䄀渀挀栀漀爀䐀椀爀攀挀琀椀瘀攀 㴀 瘀愀氀甀攀䘀渀⠀笀ഀഀ
  restrict: 'E',਍  挀漀洀瀀椀氀攀㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 愀琀琀爀⤀ 笀ഀഀ
਍    椀昀 ⠀洀猀椀攀 㰀㴀 㠀⤀ 笀ഀഀ
਍      ⼀⼀ 琀甀爀渀 㰀愀 栀爀攀昀 渀最ⴀ挀氀椀挀欀㴀∀⸀⸀∀㸀氀椀渀欀㰀⼀愀㸀 椀渀琀漀 愀 猀琀礀氀愀戀氀攀 氀椀渀欀 椀渀 䤀䔀ഀഀ
      // but only if it doesn't have name attribute, in which case it's an anchor਍      椀昀 ⠀℀愀琀琀爀⸀栀爀攀昀 ☀☀ ℀愀琀琀爀⸀渀愀洀攀⤀ 笀ഀഀ
        attr.$set('href', '');਍      紀ഀഀ
਍      ⼀⼀ 愀搀搀 愀 挀漀洀洀攀渀琀 渀漀搀攀 琀漀 愀渀挀栀漀爀猀 琀漀 眀漀爀欀愀爀漀甀渀搀 䤀䔀 戀甀最 琀栀愀琀 挀愀甀猀攀猀 攀氀攀洀攀渀琀 挀漀渀琀攀渀琀 琀漀 戀攀 爀攀猀攀琀ഀഀ
      // to new attribute content if attribute is updated with value containing @ and element also਍      ⼀⼀ 挀漀渀琀愀椀渀猀 瘀愀氀甀攀 眀椀琀栀 䀀ഀഀ
      // see issue #1949਍      攀氀攀洀攀渀琀⸀愀瀀瀀攀渀搀⠀搀漀挀甀洀攀渀琀⸀挀爀攀愀琀攀䌀漀洀洀攀渀琀⠀✀䤀䔀 昀椀砀✀⤀⤀㬀ഀഀ
    }਍ഀഀ
    if (!attr.href && !attr.name) {਍      爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀⤀ 笀ഀഀ
        element.on('click', function(event){਍          ⼀⼀ 椀昀 眀攀 栀愀瘀攀 渀漀 栀爀攀昀 甀爀氀Ⰰ 琀栀攀渀 搀漀渀✀琀 渀愀瘀椀最愀琀攀 愀渀礀眀栀攀爀攀⸀ഀഀ
          if (!element.attr('href')) {਍            攀瘀攀渀琀⸀瀀爀攀瘀攀渀琀䐀攀昀愀甀氀琀⠀⤀㬀ഀഀ
          }਍        紀⤀㬀ഀഀ
      };਍    紀ഀഀ
  }਍紀⤀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䠀爀攀昀ഀഀ
 * @restrict A਍ ⨀ 䀀瀀爀椀漀爀椀琀礀 㤀㤀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Using Angular markup like `{{hash}}` in an href attribute will਍ ⨀ 洀愀欀攀 琀栀攀 氀椀渀欀 最漀 琀漀 琀栀攀 眀爀漀渀最 唀刀䰀 椀昀 琀栀攀 甀猀攀爀 挀氀椀挀欀猀 椀琀 戀攀昀漀爀攀ഀഀ
 * Angular has a chance to replace the `{{hash}}` markup with its਍ ⨀ 瘀愀氀甀攀⸀ 唀渀琀椀氀 䄀渀最甀氀愀爀 爀攀瀀氀愀挀攀猀 琀栀攀 洀愀爀欀甀瀀 琀栀攀 氀椀渀欀 眀椀氀氀 戀攀 戀爀漀欀攀渀ഀഀ
 * and will most likely return a 404 error.਍ ⨀ഀഀ
 * The `ngHref` directive solves this problem.਍ ⨀ഀഀ
 * The wrong way to write it:਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 * <a href="http://www.gravatar.com/avatar/{{hash}}"/>਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 吀栀攀 挀漀爀爀攀挀琀 眀愀礀 琀漀 眀爀椀琀攀 椀琀㨀ഀഀ
 * <pre>਍ ⨀ 㰀愀 渀最ⴀ栀爀攀昀㴀∀栀琀琀瀀㨀⼀⼀眀眀眀⸀最爀愀瘀愀琀愀爀⸀挀漀洀⼀愀瘀愀琀愀爀⼀笀笀栀愀猀栀紀紀∀⼀㸀ഀഀ
 * </pre>਍ ⨀ഀഀ
 * @element A਍ ⨀ 䀀瀀愀爀愀洀 笀琀攀洀瀀氀愀琀攀紀 渀最䠀爀攀昀 愀渀礀 猀琀爀椀渀最 眀栀椀挀栀 挀愀渀 挀漀渀琀愀椀渀 怀笀笀紀紀怀 洀愀爀欀甀瀀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
 * This example shows various combinations of `href`, `ng-href` and `ng-click` attributes਍ ⨀ 椀渀 氀椀渀欀猀 愀渀搀 琀栀攀椀爀 搀椀昀昀攀爀攀渀琀 戀攀栀愀瘀椀漀爀猀㨀ഀഀ
    <doc:example>਍      㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
        <input ng-model="value" /><br />਍        㰀愀 椀搀㴀∀氀椀渀欀ⴀ㄀∀ 栀爀攀昀 渀最ⴀ挀氀椀挀欀㴀∀瘀愀氀甀攀 㴀 ㄀∀㸀氀椀渀欀 ㄀㰀⼀愀㸀 ⠀氀椀渀欀Ⰰ 搀漀渀✀琀 爀攀氀漀愀搀⤀㰀戀爀 ⼀㸀ഀഀ
        <a id="link-2" href="" ng-click="value = 2">link 2</a> (link, don't reload)<br />਍        㰀愀 椀搀㴀∀氀椀渀欀ⴀ㌀∀ 渀最ⴀ栀爀攀昀㴀∀⼀笀笀✀㄀㈀㌀✀紀紀∀㸀氀椀渀欀 ㌀㰀⼀愀㸀 ⠀氀椀渀欀Ⰰ 爀攀氀漀愀搀℀⤀㰀戀爀 ⼀㸀ഀഀ
        <a id="link-4" href="" name="xx" ng-click="value = 4">anchor</a> (link, don't reload)<br />਍        㰀愀 椀搀㴀∀氀椀渀欀ⴀ㔀∀ 渀愀洀攀㴀∀砀砀砀∀ 渀最ⴀ挀氀椀挀欀㴀∀瘀愀氀甀攀 㴀 㔀∀㸀愀渀挀栀漀爀㰀⼀愀㸀 ⠀渀漀 氀椀渀欀⤀㰀戀爀 ⼀㸀ഀഀ
        <a id="link-6" ng-href="{{value}}">link</a> (link, change location)਍      㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      <doc:scenario>਍        椀琀⠀✀猀栀漀甀氀搀 攀砀攀挀甀琀攀 渀最ⴀ挀氀椀挀欀 戀甀琀 渀漀琀 爀攀氀漀愀搀 眀栀攀渀 栀爀攀昀 眀椀琀栀漀甀琀 瘀愀氀甀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          element('#link-1').click();਍          攀砀瀀攀挀琀⠀椀渀瀀甀琀⠀✀瘀愀氀甀攀✀⤀⸀瘀愀氀⠀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀㄀✀⤀㬀ഀഀ
          expect(element('#link-1').attr('href')).toBe("");਍        紀⤀㬀ഀഀ
਍        椀琀⠀✀猀栀漀甀氀搀 攀砀攀挀甀琀攀 渀最ⴀ挀氀椀挀欀 戀甀琀 渀漀琀 爀攀氀漀愀搀 眀栀攀渀 栀爀攀昀 攀洀瀀琀礀 猀琀爀椀渀最✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          element('#link-2').click();਍          攀砀瀀攀挀琀⠀椀渀瀀甀琀⠀✀瘀愀氀甀攀✀⤀⸀瘀愀氀⠀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀㈀✀⤀㬀ഀഀ
          expect(element('#link-2').attr('href')).toBe("");਍        紀⤀㬀ഀഀ
਍        椀琀⠀✀猀栀漀甀氀搀 攀砀攀挀甀琀攀 渀最ⴀ挀氀椀挀欀 愀渀搀 挀栀愀渀最攀 甀爀氀 眀栀攀渀 渀最ⴀ栀爀攀昀 猀瀀攀挀椀昀椀攀搀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          expect(element('#link-3').attr('href')).toBe("/123");਍ഀഀ
          element('#link-3').click();਍          攀砀瀀攀挀琀⠀戀爀漀眀猀攀爀⠀⤀⸀眀椀渀搀漀眀⠀⤀⸀瀀愀琀栀⠀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀⼀㄀㈀㌀✀⤀㬀ഀഀ
        });਍ഀഀ
        it('should execute ng-click but not reload when href empty string and name specified', function() {਍          攀氀攀洀攀渀琀⠀✀⌀氀椀渀欀ⴀ㐀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
          expect(input('value').val()).toEqual('4');਍          攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⌀氀椀渀欀ⴀ㐀✀⤀⸀愀琀琀爀⠀✀栀爀攀昀✀⤀⤀⸀琀漀䈀攀⠀✀✀⤀㬀ഀഀ
        });਍ഀഀ
        it('should execute ng-click but not reload when no href but name specified', function() {਍          攀氀攀洀攀渀琀⠀✀⌀氀椀渀欀ⴀ㔀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
          expect(input('value').val()).toEqual('5');਍          攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⌀氀椀渀欀ⴀ㔀✀⤀⸀愀琀琀爀⠀✀栀爀攀昀✀⤀⤀⸀琀漀䈀攀⠀甀渀搀攀昀椀渀攀搀⤀㬀ഀഀ
        });਍ഀഀ
        it('should only change url when only ng-href', function() {਍          椀渀瀀甀琀⠀✀瘀愀氀甀攀✀⤀⸀攀渀琀攀爀⠀✀㘀✀⤀㬀ഀഀ
          expect(element('#link-6').attr('href')).toBe('6');਍ഀഀ
          element('#link-6').click();਍          攀砀瀀攀挀琀⠀戀爀漀眀猀攀爀⠀⤀⸀氀漀挀愀琀椀漀渀⠀⤀⸀甀爀氀⠀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀⼀㘀✀⤀㬀ഀഀ
        });਍      㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
    </doc:example>਍ ⨀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最匀爀挀ഀഀ
 * @restrict A਍ ⨀ 䀀瀀爀椀漀爀椀琀礀 㤀㤀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Using Angular markup like `{{hash}}` in a `src` attribute doesn't਍ ⨀ 眀漀爀欀 爀椀最栀琀㨀 吀栀攀 戀爀漀眀猀攀爀 眀椀氀氀 昀攀琀挀栀 昀爀漀洀 琀栀攀 唀刀䰀 眀椀琀栀 琀栀攀 氀椀琀攀爀愀氀ഀഀ
 * text `{{hash}}` until Angular replaces the expression inside਍ ⨀ 怀笀笀栀愀猀栀紀紀怀⸀ 吀栀攀 怀渀最匀爀挀怀 搀椀爀攀挀琀椀瘀攀 猀漀氀瘀攀猀 琀栀椀猀 瀀爀漀戀氀攀洀⸀ഀഀ
 *਍ ⨀ 吀栀攀 戀甀最最礀 眀愀礀 琀漀 眀爀椀琀攀 椀琀㨀ഀഀ
 * <pre>਍ ⨀ 㰀椀洀最 猀爀挀㴀∀栀琀琀瀀㨀⼀⼀眀眀眀⸀最爀愀瘀愀琀愀爀⸀挀漀洀⼀愀瘀愀琀愀爀⼀笀笀栀愀猀栀紀紀∀⼀㸀ഀഀ
 * </pre>਍ ⨀ഀഀ
 * The correct way to write it:਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 * <img ng-src="http://www.gravatar.com/avatar/{{hash}}"/>਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䤀䴀䜀ഀഀ
 * @param {template} ngSrc any string which can contain `{{}}` markup.਍ ⨀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最匀爀挀猀攀琀ഀഀ
 * @restrict A਍ ⨀ 䀀瀀爀椀漀爀椀琀礀 㤀㤀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Using Angular markup like `{{hash}}` in a `srcset` attribute doesn't਍ ⨀ 眀漀爀欀 爀椀最栀琀㨀 吀栀攀 戀爀漀眀猀攀爀 眀椀氀氀 昀攀琀挀栀 昀爀漀洀 琀栀攀 唀刀䰀 眀椀琀栀 琀栀攀 氀椀琀攀爀愀氀ഀഀ
 * text `{{hash}}` until Angular replaces the expression inside਍ ⨀ 怀笀笀栀愀猀栀紀紀怀⸀ 吀栀攀 怀渀最匀爀挀猀攀琀怀 搀椀爀攀挀琀椀瘀攀 猀漀氀瘀攀猀 琀栀椀猀 瀀爀漀戀氀攀洀⸀ഀഀ
 *਍ ⨀ 吀栀攀 戀甀最最礀 眀愀礀 琀漀 眀爀椀琀攀 椀琀㨀ഀഀ
 * <pre>਍ ⨀ 㰀椀洀最 猀爀挀猀攀琀㴀∀栀琀琀瀀㨀⼀⼀眀眀眀⸀最爀愀瘀愀琀愀爀⸀挀漀洀⼀愀瘀愀琀愀爀⼀笀笀栀愀猀栀紀紀 ㈀砀∀⼀㸀ഀഀ
 * </pre>਍ ⨀ഀഀ
 * The correct way to write it:਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 * <img ng-srcset="http://www.gravatar.com/avatar/{{hash}} 2x"/>਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䤀䴀䜀ഀഀ
 * @param {template} ngSrcset any string which can contain `{{}}` markup.਍ ⨀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䐀椀猀愀戀氀攀搀ഀഀ
 * @restrict A਍ ⨀ 䀀瀀爀椀漀爀椀琀礀 ㄀　　ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 *਍ ⨀ 吀栀攀 昀漀氀氀漀眀椀渀最 洀愀爀欀甀瀀 眀椀氀氀 洀愀欀攀 琀栀攀 戀甀琀琀漀渀 攀渀愀戀氀攀搀 漀渀 䌀栀爀漀洀攀⼀䘀椀爀攀昀漀砀 戀甀琀 渀漀琀 漀渀 䤀䔀㠀 愀渀搀 漀氀搀攀爀 䤀䔀猀㨀ഀഀ
 * <pre>਍ ⨀ 㰀搀椀瘀 渀最ⴀ椀渀椀琀㴀∀猀挀漀瀀攀 㴀 笀 椀猀䐀椀猀愀戀氀攀搀㨀 昀愀氀猀攀 紀∀㸀ഀഀ
 *  <button disabled="{{scope.isDisabled}}">Disabled</button>਍ ⨀ 㰀⼀搀椀瘀㸀ഀഀ
 * </pre>਍ ⨀ഀഀ
 * The HTML specification does not require browsers to preserve the values of boolean attributes਍ ⨀ 猀甀挀栀 愀猀 搀椀猀愀戀氀攀搀⸀ ⠀吀栀攀椀爀 瀀爀攀猀攀渀挀攀 洀攀愀渀猀 琀爀甀攀 愀渀搀 琀栀攀椀爀 愀戀猀攀渀挀攀 洀攀愀渀猀 昀愀氀猀攀⸀⤀ഀഀ
 * If we put an Angular interpolation expression into such an attribute then the਍ ⨀ 戀椀渀搀椀渀最 椀渀昀漀爀洀愀琀椀漀渀 眀漀甀氀搀 戀攀 氀漀猀琀 眀栀攀渀 琀栀攀 戀爀漀眀猀攀爀 爀攀洀漀瘀攀猀 琀栀攀 愀琀琀爀椀戀甀琀攀⸀ഀഀ
 * The `ngDisabled` directive solves this problem for the `disabled` attribute.਍ ⨀ 吀栀椀猀 挀漀洀瀀氀攀洀攀渀琀愀爀礀 搀椀爀攀挀琀椀瘀攀 椀猀 渀漀琀 爀攀洀漀瘀攀搀 戀礀 琀栀攀 戀爀漀眀猀攀爀 愀渀搀 猀漀 瀀爀漀瘀椀搀攀猀ഀഀ
 * a permanent reliable place to store the binding information.਍ ⨀ഀഀ
 * @example਍    㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
      <doc:source>਍        䌀氀椀挀欀 洀攀 琀漀 琀漀最最氀攀㨀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀挀栀攀挀欀戀漀砀∀ 渀最ⴀ洀漀搀攀氀㴀∀挀栀攀挀欀攀搀∀㸀㰀戀爀⼀㸀ഀഀ
        <button ng-model="button" ng-disabled="checked">Button</button>਍      㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      <doc:scenario>਍        椀琀⠀✀猀栀漀甀氀搀 琀漀最最氀攀 戀甀琀琀漀渀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          expect(element('.doc-example-live :button').prop('disabled')).toBeFalsy();਍          椀渀瀀甀琀⠀✀挀栀攀挀欀攀搀✀⤀⸀挀栀攀挀欀⠀⤀㬀ഀഀ
          expect(element('.doc-example-live :button').prop('disabled')).toBeTruthy();਍        紀⤀㬀ഀഀ
      </doc:scenario>਍    㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䤀一倀唀吀ഀഀ
 * @param {expression} ngDisabled If the {@link guide/expression expression} is truthy, ਍ ⨀     琀栀攀渀 猀瀀攀挀椀愀氀 愀琀琀爀椀戀甀琀攀 ∀搀椀猀愀戀氀攀搀∀ 眀椀氀氀 戀攀 猀攀琀 漀渀 琀栀攀 攀氀攀洀攀渀琀ഀഀ
 */਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䌀栀攀挀欀攀搀ഀഀ
 * @restrict A਍ ⨀ 䀀瀀爀椀漀爀椀琀礀 ㄀　　ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * The HTML specification does not require browsers to preserve the values of boolean attributes਍ ⨀ 猀甀挀栀 愀猀 挀栀攀挀欀攀搀⸀ ⠀吀栀攀椀爀 瀀爀攀猀攀渀挀攀 洀攀愀渀猀 琀爀甀攀 愀渀搀 琀栀攀椀爀 愀戀猀攀渀挀攀 洀攀愀渀猀 昀愀氀猀攀⸀⤀ഀഀ
 * If we put an Angular interpolation expression into such an attribute then the਍ ⨀ 戀椀渀搀椀渀最 椀渀昀漀爀洀愀琀椀漀渀 眀漀甀氀搀 戀攀 氀漀猀琀 眀栀攀渀 琀栀攀 戀爀漀眀猀攀爀 爀攀洀漀瘀攀猀 琀栀攀 愀琀琀爀椀戀甀琀攀⸀ഀഀ
 * The `ngChecked` directive solves this problem for the `checked` attribute.਍ ⨀ 吀栀椀猀 挀漀洀瀀氀攀洀攀渀琀愀爀礀 搀椀爀攀挀琀椀瘀攀 椀猀 渀漀琀 爀攀洀漀瘀攀搀 戀礀 琀栀攀 戀爀漀眀猀攀爀 愀渀搀 猀漀 瀀爀漀瘀椀搀攀猀ഀഀ
 * a permanent reliable place to store the binding information.਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
    <doc:example>਍      㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
        Check me to check both: <input type="checkbox" ng-model="master"><br/>਍        㰀椀渀瀀甀琀 椀搀㴀∀挀栀攀挀欀匀氀愀瘀攀∀ 琀礀瀀攀㴀∀挀栀攀挀欀戀漀砀∀ 渀最ⴀ挀栀攀挀欀攀搀㴀∀洀愀猀琀攀爀∀㸀ഀഀ
      </doc:source>਍      㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
        it('should check both checkBoxes', function() {਍          攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 ⌀挀栀攀挀欀匀氀愀瘀攀✀⤀⸀瀀爀漀瀀⠀✀挀栀攀挀欀攀搀✀⤀⤀⸀琀漀䈀攀䘀愀氀猀礀⠀⤀㬀ഀഀ
          input('master').check();਍          攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 ⌀挀栀攀挀欀匀氀愀瘀攀✀⤀⸀瀀爀漀瀀⠀✀挀栀攀挀欀攀搀✀⤀⤀⸀琀漀䈀攀吀爀甀琀栀礀⠀⤀㬀ഀഀ
        });਍      㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
    </doc:example>਍ ⨀ഀഀ
 * @element INPUT਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䌀栀攀挀欀攀搀 䤀昀 琀栀攀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 攀砀瀀爀攀猀猀椀漀渀紀 椀猀 琀爀甀琀栀礀Ⰰ ഀഀ
 *     then special attribute "checked" will be set on the element਍ ⨀⼀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngReadonly਍ ⨀ 䀀爀攀猀琀爀椀挀琀 䄀ഀഀ
 * @priority 100਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀攀 䠀吀䴀䰀 猀瀀攀挀椀昀椀挀愀琀椀漀渀 搀漀攀猀 渀漀琀 爀攀焀甀椀爀攀 戀爀漀眀猀攀爀猀 琀漀 瀀爀攀猀攀爀瘀攀 琀栀攀 瘀愀氀甀攀猀 漀昀 戀漀漀氀攀愀渀 愀琀琀爀椀戀甀琀攀猀ഀഀ
 * such as readonly. (Their presence means true and their absence means false.)਍ ⨀ 䤀昀 眀攀 瀀甀琀 愀渀 䄀渀最甀氀愀爀 椀渀琀攀爀瀀漀氀愀琀椀漀渀 攀砀瀀爀攀猀猀椀漀渀 椀渀琀漀 猀甀挀栀 愀渀 愀琀琀爀椀戀甀琀攀 琀栀攀渀 琀栀攀ഀഀ
 * binding information would be lost when the browser removes the attribute.਍ ⨀ 吀栀攀 怀渀最刀攀愀搀漀渀氀礀怀 搀椀爀攀挀琀椀瘀攀 猀漀氀瘀攀猀 琀栀椀猀 瀀爀漀戀氀攀洀 昀漀爀 琀栀攀 怀爀攀愀搀漀渀氀礀怀 愀琀琀爀椀戀甀琀攀⸀ഀഀ
 * This complementary directive is not removed by the browser and so provides਍ ⨀ 愀 瀀攀爀洀愀渀攀渀琀 爀攀氀椀愀戀氀攀 瀀氀愀挀攀 琀漀 猀琀漀爀攀 琀栀攀 戀椀渀搀椀渀最 椀渀昀漀爀洀愀琀椀漀渀⸀ഀഀ
 * @example਍    㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
      <doc:source>਍        䌀栀攀挀欀 洀攀 琀漀 洀愀欀攀 琀攀砀琀 爀攀愀搀漀渀氀礀㨀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀挀栀攀挀欀戀漀砀∀ 渀最ⴀ洀漀搀攀氀㴀∀挀栀攀挀欀攀搀∀㸀㰀戀爀⼀㸀ഀഀ
        <input type="text" ng-readonly="checked" value="I'm Angular"/>਍      㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      <doc:scenario>਍        椀琀⠀✀猀栀漀甀氀搀 琀漀最最氀攀 爀攀愀搀漀渀氀礀 愀琀琀爀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          expect(element('.doc-example-live :text').prop('readonly')).toBeFalsy();਍          椀渀瀀甀琀⠀✀挀栀攀挀欀攀搀✀⤀⸀挀栀攀挀欀⠀⤀㬀ഀഀ
          expect(element('.doc-example-live :text').prop('readonly')).toBeTruthy();਍        紀⤀㬀ഀഀ
      </doc:scenario>਍    㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䤀一倀唀吀ഀഀ
 * @param {expression} ngReadonly If the {@link guide/expression expression} is truthy, ਍ ⨀     琀栀攀渀 猀瀀攀挀椀愀氀 愀琀琀爀椀戀甀琀攀 ∀爀攀愀搀漀渀氀礀∀ 眀椀氀氀 戀攀 猀攀琀 漀渀 琀栀攀 攀氀攀洀攀渀琀ഀഀ
 */਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最匀攀氀攀挀琀攀搀ഀഀ
 * @restrict A਍ ⨀ 䀀瀀爀椀漀爀椀琀礀 ㄀　　ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * The HTML specification does not require browsers to preserve the values of boolean attributes਍ ⨀ 猀甀挀栀 愀猀 猀攀氀攀挀琀攀搀⸀ ⠀吀栀攀椀爀 瀀爀攀猀攀渀挀攀 洀攀愀渀猀 琀爀甀攀 愀渀搀 琀栀攀椀爀 愀戀猀攀渀挀攀 洀攀愀渀猀 昀愀氀猀攀⸀⤀ഀഀ
 * If we put an Angular interpolation expression into such an attribute then the਍ ⨀ 戀椀渀搀椀渀最 椀渀昀漀爀洀愀琀椀漀渀 眀漀甀氀搀 戀攀 氀漀猀琀 眀栀攀渀 琀栀攀 戀爀漀眀猀攀爀 爀攀洀漀瘀攀猀 琀栀攀 愀琀琀爀椀戀甀琀攀⸀ഀഀ
 * The `ngSelected` directive solves this problem for the `selected` atttribute.਍ ⨀ 吀栀椀猀 挀漀洀瀀氀攀洀攀渀琀愀爀礀 搀椀爀攀挀琀椀瘀攀 椀猀 渀漀琀 爀攀洀漀瘀攀搀 戀礀 琀栀攀 戀爀漀眀猀攀爀 愀渀搀 猀漀 瀀爀漀瘀椀搀攀猀ഀഀ
 * a permanent reliable place to store the binding information.਍ ⨀ ഀഀ
 * @example਍    㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
      <doc:source>਍        䌀栀攀挀欀 洀攀 琀漀 猀攀氀攀挀琀㨀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀挀栀攀挀欀戀漀砀∀ 渀最ⴀ洀漀搀攀氀㴀∀猀攀氀攀挀琀攀搀∀㸀㰀戀爀⼀㸀ഀഀ
        <select>਍          㰀漀瀀琀椀漀渀㸀䠀攀氀氀漀℀㰀⼀漀瀀琀椀漀渀㸀ഀഀ
          <option id="greet" ng-selected="selected">Greetings!</option>਍        㰀⼀猀攀氀攀挀琀㸀ഀഀ
      </doc:source>਍      㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
        it('should select Greetings!', function() {਍          攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 ⌀最爀攀攀琀✀⤀⸀瀀爀漀瀀⠀✀猀攀氀攀挀琀攀搀✀⤀⤀⸀琀漀䈀攀䘀愀氀猀礀⠀⤀㬀ഀഀ
          input('selected').check();਍          攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 ⌀最爀攀攀琀✀⤀⸀瀀爀漀瀀⠀✀猀攀氀攀挀琀攀搀✀⤀⤀⸀琀漀䈀攀吀爀甀琀栀礀⠀⤀㬀ഀഀ
        });਍      㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
    </doc:example>਍ ⨀ഀഀ
 * @element OPTION਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最匀攀氀攀挀琀攀搀 䤀昀 琀栀攀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 攀砀瀀爀攀猀猀椀漀渀紀 椀猀 琀爀甀琀栀礀Ⰰ ഀഀ
 *     then special attribute "selected" will be set on the element਍ ⨀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最伀瀀攀渀ഀഀ
 * @restrict A਍ ⨀ 䀀瀀爀椀漀爀椀琀礀 ㄀　　ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * The HTML specification does not require browsers to preserve the values of boolean attributes਍ ⨀ 猀甀挀栀 愀猀 漀瀀攀渀⸀ ⠀吀栀攀椀爀 瀀爀攀猀攀渀挀攀 洀攀愀渀猀 琀爀甀攀 愀渀搀 琀栀攀椀爀 愀戀猀攀渀挀攀 洀攀愀渀猀 昀愀氀猀攀⸀⤀ഀഀ
 * If we put an Angular interpolation expression into such an attribute then the਍ ⨀ 戀椀渀搀椀渀最 椀渀昀漀爀洀愀琀椀漀渀 眀漀甀氀搀 戀攀 氀漀猀琀 眀栀攀渀 琀栀攀 戀爀漀眀猀攀爀 爀攀洀漀瘀攀猀 琀栀攀 愀琀琀爀椀戀甀琀攀⸀ഀഀ
 * The `ngOpen` directive solves this problem for the `open` attribute.਍ ⨀ 吀栀椀猀 挀漀洀瀀氀攀洀攀渀琀愀爀礀 搀椀爀攀挀琀椀瘀攀 椀猀 渀漀琀 爀攀洀漀瘀攀搀 戀礀 琀栀攀 戀爀漀眀猀攀爀 愀渀搀 猀漀 瀀爀漀瘀椀搀攀猀ഀഀ
 * a permanent reliable place to store the binding information.਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
     <doc:example>਍       㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
         Check me check multiple: <input type="checkbox" ng-model="open"><br/>਍         㰀搀攀琀愀椀氀猀 椀搀㴀∀搀攀琀愀椀氀猀∀ 渀最ⴀ漀瀀攀渀㴀∀漀瀀攀渀∀㸀ഀഀ
            <summary>Show/Hide me</summary>਍         㰀⼀搀攀琀愀椀氀猀㸀ഀഀ
       </doc:source>਍       㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
         it('should toggle open', function() {਍           攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⌀搀攀琀愀椀氀猀✀⤀⸀瀀爀漀瀀⠀✀漀瀀攀渀✀⤀⤀⸀琀漀䈀攀䘀愀氀猀礀⠀⤀㬀ഀഀ
           input('open').check();਍           攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⌀搀攀琀愀椀氀猀✀⤀⸀瀀爀漀瀀⠀✀漀瀀攀渀✀⤀⤀⸀琀漀䈀攀吀爀甀琀栀礀⠀⤀㬀ഀഀ
         });਍       㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
     </doc:example>਍ ⨀ഀഀ
 * @element DETAILS਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最伀瀀攀渀 䤀昀 琀栀攀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 攀砀瀀爀攀猀猀椀漀渀紀 椀猀 琀爀甀琀栀礀Ⰰ ഀഀ
 *     then special attribute "open" will be set on the element਍ ⨀⼀ഀഀ
਍瘀愀爀 渀最䄀琀琀爀椀戀甀琀攀䄀氀椀愀猀䐀椀爀攀挀琀椀瘀攀猀 㴀 笀紀㬀ഀഀ
਍ഀഀ
// boolean attrs are evaluated਍昀漀爀䔀愀挀栀⠀䈀伀伀䰀䔀䄀一开䄀吀吀刀Ⰰ 昀甀渀挀琀椀漀渀⠀瀀爀漀瀀一愀洀攀Ⰰ 愀琀琀爀一愀洀攀⤀ 笀ഀഀ
  // binding to multiple is not supported਍  椀昀 ⠀瀀爀漀瀀一愀洀攀 㴀㴀 ∀洀甀氀琀椀瀀氀攀∀⤀ 爀攀琀甀爀渀㬀ഀഀ
਍  瘀愀爀 渀漀爀洀愀氀椀稀攀搀 㴀 搀椀爀攀挀琀椀瘀攀一漀爀洀愀氀椀稀攀⠀✀渀最ⴀ✀ ⬀ 愀琀琀爀一愀洀攀⤀㬀ഀഀ
  ngAttributeAliasDirectives[normalized] = function() {਍    爀攀琀甀爀渀 笀ഀഀ
      priority: 100,਍      氀椀渀欀㨀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀⤀ 笀ഀഀ
        scope.$watch(attr[normalized], function ngBooleanAttrWatchAction(value) {਍          愀琀琀爀⸀␀猀攀琀⠀愀琀琀爀一愀洀攀Ⰰ ℀℀瘀愀氀甀攀⤀㬀ഀഀ
        });਍      紀ഀഀ
    };਍  紀㬀ഀഀ
});਍ഀഀ
਍⼀⼀ 渀最ⴀ猀爀挀Ⰰ 渀最ⴀ猀爀挀猀攀琀Ⰰ 渀最ⴀ栀爀攀昀 愀爀攀 椀渀琀攀爀瀀漀氀愀琀攀搀ഀഀ
forEach(['src', 'srcset', 'href'], function(attrName) {਍  瘀愀爀 渀漀爀洀愀氀椀稀攀搀 㴀 搀椀爀攀挀琀椀瘀攀一漀爀洀愀氀椀稀攀⠀✀渀最ⴀ✀ ⬀ 愀琀琀爀一愀洀攀⤀㬀ഀഀ
  ngAttributeAliasDirectives[normalized] = function() {਍    爀攀琀甀爀渀 笀ഀഀ
      priority: 99, // it needs to run after the attributes are interpolated਍      氀椀渀欀㨀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀⤀ 笀ഀഀ
        attr.$observe(normalized, function(value) {਍          椀昀 ⠀℀瘀愀氀甀攀⤀ഀഀ
             return;਍ഀഀ
          attr.$set(attrName, value);਍ഀഀ
          // on IE, if "ng:src" directive declaration is used and "src" attribute doesn't exist਍          ⼀⼀ 琀栀攀渀 挀愀氀氀椀渀最 攀氀攀洀攀渀琀⸀猀攀琀䄀琀琀爀椀戀甀琀攀⠀✀猀爀挀✀Ⰰ ✀昀漀漀✀⤀ 搀漀攀猀渀✀琀 搀漀 愀渀礀琀栀椀渀最Ⰰ 猀漀 眀攀 渀攀攀搀ഀഀ
          // to set the property as well to achieve the desired effect.਍          ⼀⼀ 眀攀 甀猀攀 愀琀琀爀嬀愀琀琀爀一愀洀攀崀 瘀愀氀甀攀 猀椀渀挀攀 ␀猀攀琀 挀愀渀 猀愀渀椀琀椀稀攀 琀栀攀 甀爀氀⸀ഀഀ
          if (msie) element.prop(attrName, attr[attrName]);਍        紀⤀㬀ഀഀ
      }਍    紀㬀ഀഀ
  };਍紀⤀㬀ഀഀ
਍⼀⨀ 最氀漀戀愀氀 ⴀ渀甀氀氀䘀漀爀洀䌀琀爀氀 ⨀⼀ഀഀ
var nullFormCtrl = {਍  ␀愀搀搀䌀漀渀琀爀漀氀㨀 渀漀漀瀀Ⰰഀഀ
  $removeControl: noop,਍  ␀猀攀琀嘀愀氀椀搀椀琀礀㨀 渀漀漀瀀Ⰰഀഀ
  $setDirty: noop,਍  ␀猀攀琀倀爀椀猀琀椀渀攀㨀 渀漀漀瀀ഀഀ
};਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name ng.directive:form.FormController਍ ⨀ഀഀ
 * @property {boolean} $pristine True if user has not interacted with the form yet.਍ ⨀ 䀀瀀爀漀瀀攀爀琀礀 笀戀漀漀氀攀愀渀紀 ␀搀椀爀琀礀 吀爀甀攀 椀昀 甀猀攀爀 栀愀猀 愀氀爀攀愀搀礀 椀渀琀攀爀愀挀琀攀搀 眀椀琀栀 琀栀攀 昀漀爀洀⸀ഀഀ
 * @property {boolean} $valid True if all of the containing forms and controls are valid.਍ ⨀ 䀀瀀爀漀瀀攀爀琀礀 笀戀漀漀氀攀愀渀紀 ␀椀渀瘀愀氀椀搀 吀爀甀攀 椀昀 愀琀 氀攀愀猀琀 漀渀攀 挀漀渀琀愀椀渀椀渀最 挀漀渀琀爀漀氀 漀爀 昀漀爀洀 椀猀 椀渀瘀愀氀椀搀⸀ഀഀ
 *਍ ⨀ 䀀瀀爀漀瀀攀爀琀礀 笀伀戀樀攀挀琀紀 ␀攀爀爀漀爀 䤀猀 愀渀 漀戀樀攀挀琀 栀愀猀栀Ⰰ 挀漀渀琀愀椀渀椀渀最 爀攀昀攀爀攀渀挀攀猀 琀漀 愀氀氀 椀渀瘀愀氀椀搀 挀漀渀琀爀漀氀猀 漀爀ഀഀ
 *  forms, where:਍ ⨀ഀഀ
 *  - keys are validation tokens (error names),਍ ⨀  ⴀ 瘀愀氀甀攀猀 愀爀攀 愀爀爀愀礀猀 漀昀 挀漀渀琀爀漀氀猀 漀爀 昀漀爀洀猀 琀栀愀琀 愀爀攀 椀渀瘀愀氀椀搀 昀漀爀 最椀瘀攀渀 攀爀爀漀爀 渀愀洀攀⸀ഀഀ
 *਍ ⨀ഀഀ
 *  Built-in validation tokens:਍ ⨀ഀഀ
 *  - `email`਍ ⨀  ⴀ 怀洀愀砀怀ഀഀ
 *  - `maxlength`਍ ⨀  ⴀ 怀洀椀渀怀ഀഀ
 *  - `minlength`਍ ⨀  ⴀ 怀渀甀洀戀攀爀怀ഀഀ
 *  - `pattern`਍ ⨀  ⴀ 怀爀攀焀甀椀爀攀搀怀ഀഀ
 *  - `url`਍ ⨀ ഀഀ
 * @description਍ ⨀ 怀䘀漀爀洀䌀漀渀琀爀漀氀氀攀爀怀 欀攀攀瀀猀 琀爀愀挀欀 漀昀 愀氀氀 椀琀猀 挀漀渀琀爀漀氀猀 愀渀搀 渀攀猀琀攀搀 昀漀爀洀猀 愀猀 眀攀氀氀 愀猀 猀琀愀琀攀 漀昀 琀栀攀洀Ⰰഀഀ
 * such as being valid/invalid or dirty/pristine.਍ ⨀ഀഀ
 * Each {@link ng.directive:form form} directive creates an instance਍ ⨀ 漀昀 怀䘀漀爀洀䌀漀渀琀爀漀氀氀攀爀怀⸀ഀഀ
 *਍ ⨀⼀ഀഀ
//asks for $scope to fool the BC controller module਍䘀漀爀洀䌀漀渀琀爀漀氀氀攀爀⸀␀椀渀樀攀挀琀 㴀 嬀✀␀攀氀攀洀攀渀琀✀Ⰰ ✀␀愀琀琀爀猀✀Ⰰ ✀␀猀挀漀瀀攀✀崀㬀ഀഀ
function FormController(element, attrs) {਍  瘀愀爀 昀漀爀洀 㴀 琀栀椀猀Ⰰഀഀ
      parentForm = element.parent().controller('form') || nullFormCtrl,਍      椀渀瘀愀氀椀搀䌀漀甀渀琀 㴀 　Ⰰ ⼀⼀ 甀猀攀搀 琀漀 攀愀猀椀氀礀 搀攀琀攀爀洀椀渀攀 椀昀 眀攀 愀爀攀 瘀愀氀椀搀ഀഀ
      errors = form.$error = {},਍      挀漀渀琀爀漀氀猀 㴀 嬀崀㬀ഀഀ
਍  ⼀⼀ 椀渀椀琀 猀琀愀琀攀ഀഀ
  form.$name = attrs.name || attrs.ngForm;਍  昀漀爀洀⸀␀搀椀爀琀礀 㴀 昀愀氀猀攀㬀ഀഀ
  form.$pristine = true;਍  昀漀爀洀⸀␀瘀愀氀椀搀 㴀 琀爀甀攀㬀ഀഀ
  form.$invalid = false;਍ഀഀ
  parentForm.$addControl(form);਍ഀഀ
  // Setup initial state of the control਍  攀氀攀洀攀渀琀⸀愀搀搀䌀氀愀猀猀⠀倀刀䤀匀吀䤀一䔀开䌀䰀䄀匀匀⤀㬀ഀഀ
  toggleValidCss(true);਍ഀഀ
  // convenience method for easy toggling of classes਍  昀甀渀挀琀椀漀渀 琀漀最最氀攀嘀愀氀椀搀䌀猀猀⠀椀猀嘀愀氀椀搀Ⰰ 瘀愀氀椀搀愀琀椀漀渀䔀爀爀漀爀䬀攀礀⤀ 笀ഀഀ
    validationErrorKey = validationErrorKey ? '-' + snake_case(validationErrorKey, '-') : '';਍    攀氀攀洀攀渀琀⸀ഀഀ
      removeClass((isValid ? INVALID_CLASS : VALID_CLASS) + validationErrorKey).਍      愀搀搀䌀氀愀猀猀⠀⠀椀猀嘀愀氀椀搀 㼀 嘀䄀䰀䤀䐀开䌀䰀䄀匀匀 㨀 䤀一嘀䄀䰀䤀䐀开䌀䰀䄀匀匀⤀ ⬀ 瘀愀氀椀搀愀琀椀漀渀䔀爀爀漀爀䬀攀礀⤀㬀ഀഀ
  }਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
   * @name ng.directive:form.FormController#$addControl਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀昀漀爀洀⸀䘀漀爀洀䌀漀渀琀爀漀氀氀攀爀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Register a control with the form.਍   ⨀ഀഀ
   * Input elements using ngModelController do this automatically when they are linked.਍   ⨀⼀ഀഀ
  form.$addControl = function(control) {਍    ⼀⼀ 䈀爀攀愀欀椀渀最 挀栀愀渀最攀 ⴀ 戀攀昀漀爀攀Ⰰ 椀渀瀀甀琀猀 眀栀漀猀攀 渀愀洀攀 眀愀猀 ∀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀∀ 眀攀爀攀 焀甀椀攀琀氀礀 椀最渀漀爀攀搀ഀഀ
    // and not added to the scope.  Now we throw an error.਍    愀猀猀攀爀琀一漀琀䠀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀挀漀渀琀爀漀氀⸀␀渀愀洀攀Ⰰ ✀椀渀瀀甀琀✀⤀㬀ഀഀ
    controls.push(control);਍ഀഀ
    if (control.$name) {਍      昀漀爀洀嬀挀漀渀琀爀漀氀⸀␀渀愀洀攀崀 㴀 挀漀渀琀爀漀氀㬀ഀഀ
    }਍  紀㬀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc function਍   ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀昀漀爀洀⸀䘀漀爀洀䌀漀渀琀爀漀氀氀攀爀⌀␀爀攀洀漀瘀攀䌀漀渀琀爀漀氀ഀഀ
   * @methodOf ng.directive:form.FormController਍   ⨀ഀഀ
   * @description਍   ⨀ 䐀攀爀攀最椀猀琀攀爀 愀 挀漀渀琀爀漀氀 昀爀漀洀 琀栀攀 昀漀爀洀⸀ഀഀ
   *਍   ⨀ 䤀渀瀀甀琀 攀氀攀洀攀渀琀猀 甀猀椀渀最 渀最䴀漀搀攀氀䌀漀渀琀爀漀氀氀攀爀 搀漀 琀栀椀猀 愀甀琀漀洀愀琀椀挀愀氀氀礀 眀栀攀渀 琀栀攀礀 愀爀攀 搀攀猀琀爀漀礀攀搀⸀ഀഀ
   */਍  昀漀爀洀⸀␀爀攀洀漀瘀攀䌀漀渀琀爀漀氀 㴀 昀甀渀挀琀椀漀渀⠀挀漀渀琀爀漀氀⤀ 笀ഀഀ
    if (control.$name && form[control.$name] === control) {਍      搀攀氀攀琀攀 昀漀爀洀嬀挀漀渀琀爀漀氀⸀␀渀愀洀攀崀㬀ഀഀ
    }਍    昀漀爀䔀愀挀栀⠀攀爀爀漀爀猀Ⰰ 昀甀渀挀琀椀漀渀⠀焀甀攀甀攀Ⰰ 瘀愀氀椀搀愀琀椀漀渀吀漀欀攀渀⤀ 笀ഀഀ
      form.$setValidity(validationToken, true, control);਍    紀⤀㬀ഀഀ
਍    愀爀爀愀礀刀攀洀漀瘀攀⠀挀漀渀琀爀漀氀猀Ⰰ 挀漀渀琀爀漀氀⤀㬀ഀഀ
  };਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
   * @name ng.directive:form.FormController#$setValidity਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀昀漀爀洀⸀䘀漀爀洀䌀漀渀琀爀漀氀氀攀爀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Sets the validity of a form control.਍   ⨀ഀഀ
   * This method will also propagate to parent forms.਍   ⨀⼀ഀഀ
  form.$setValidity = function(validationToken, isValid, control) {਍    瘀愀爀 焀甀攀甀攀 㴀 攀爀爀漀爀猀嬀瘀愀氀椀搀愀琀椀漀渀吀漀欀攀渀崀㬀ഀഀ
਍    椀昀 ⠀椀猀嘀愀氀椀搀⤀ 笀ഀഀ
      if (queue) {਍        愀爀爀愀礀刀攀洀漀瘀攀⠀焀甀攀甀攀Ⰰ 挀漀渀琀爀漀氀⤀㬀ഀഀ
        if (!queue.length) {਍          椀渀瘀愀氀椀搀䌀漀甀渀琀ⴀⴀ㬀ഀഀ
          if (!invalidCount) {਍            琀漀最最氀攀嘀愀氀椀搀䌀猀猀⠀椀猀嘀愀氀椀搀⤀㬀ഀഀ
            form.$valid = true;਍            昀漀爀洀⸀␀椀渀瘀愀氀椀搀 㴀 昀愀氀猀攀㬀ഀഀ
          }਍          攀爀爀漀爀猀嬀瘀愀氀椀搀愀琀椀漀渀吀漀欀攀渀崀 㴀 昀愀氀猀攀㬀ഀഀ
          toggleValidCss(true, validationToken);਍          瀀愀爀攀渀琀䘀漀爀洀⸀␀猀攀琀嘀愀氀椀搀椀琀礀⠀瘀愀氀椀搀愀琀椀漀渀吀漀欀攀渀Ⰰ 琀爀甀攀Ⰰ 昀漀爀洀⤀㬀ഀഀ
        }਍      紀ഀഀ
਍    紀 攀氀猀攀 笀ഀഀ
      if (!invalidCount) {਍        琀漀最最氀攀嘀愀氀椀搀䌀猀猀⠀椀猀嘀愀氀椀搀⤀㬀ഀഀ
      }਍      椀昀 ⠀焀甀攀甀攀⤀ 笀ഀഀ
        if (includes(queue, control)) return;਍      紀 攀氀猀攀 笀ഀഀ
        errors[validationToken] = queue = [];਍        椀渀瘀愀氀椀搀䌀漀甀渀琀⬀⬀㬀ഀഀ
        toggleValidCss(false, validationToken);਍        瀀愀爀攀渀琀䘀漀爀洀⸀␀猀攀琀嘀愀氀椀搀椀琀礀⠀瘀愀氀椀搀愀琀椀漀渀吀漀欀攀渀Ⰰ 昀愀氀猀攀Ⰰ 昀漀爀洀⤀㬀ഀഀ
      }਍      焀甀攀甀攀⸀瀀甀猀栀⠀挀漀渀琀爀漀氀⤀㬀ഀഀ
਍      昀漀爀洀⸀␀瘀愀氀椀搀 㴀 昀愀氀猀攀㬀ഀഀ
      form.$invalid = true;਍    紀ഀഀ
  };਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
   * @name ng.directive:form.FormController#$setDirty਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀昀漀爀洀⸀䘀漀爀洀䌀漀渀琀爀漀氀氀攀爀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Sets the form to a dirty state.਍   ⨀ഀഀ
   * This method can be called to add the 'ng-dirty' class and set the form to a dirty਍   ⨀ 猀琀愀琀攀 ⠀渀最ⴀ搀椀爀琀礀 挀氀愀猀猀⤀⸀ 吀栀椀猀 洀攀琀栀漀搀 眀椀氀氀 愀氀猀漀 瀀爀漀瀀愀最愀琀攀 琀漀 瀀愀爀攀渀琀 昀漀爀洀猀⸀ഀഀ
   */਍  昀漀爀洀⸀␀猀攀琀䐀椀爀琀礀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    element.removeClass(PRISTINE_CLASS).addClass(DIRTY_CLASS);਍    昀漀爀洀⸀␀搀椀爀琀礀 㴀 琀爀甀攀㬀ഀഀ
    form.$pristine = false;਍    瀀愀爀攀渀琀䘀漀爀洀⸀␀猀攀琀䐀椀爀琀礀⠀⤀㬀ഀഀ
  };਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
   * @name ng.directive:form.FormController#$setPristine਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀昀漀爀洀⸀䘀漀爀洀䌀漀渀琀爀漀氀氀攀爀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Sets the form to its pristine state.਍   ⨀ഀഀ
   * This method can be called to remove the 'ng-dirty' class and set the form to its pristine਍   ⨀ 猀琀愀琀攀 ⠀渀最ⴀ瀀爀椀猀琀椀渀攀 挀氀愀猀猀⤀⸀ 吀栀椀猀 洀攀琀栀漀搀 眀椀氀氀 愀氀猀漀 瀀爀漀瀀愀最愀琀攀 琀漀 愀氀氀 琀栀攀 挀漀渀琀爀漀氀猀 挀漀渀琀愀椀渀攀搀ഀഀ
   * in this form.਍   ⨀ഀഀ
   * Setting a form back to a pristine state is often useful when we want to 'reuse' a form after਍   ⨀ 猀愀瘀椀渀最 漀爀 爀攀猀攀琀琀椀渀最 椀琀⸀ഀഀ
   */਍  昀漀爀洀⸀␀猀攀琀倀爀椀猀琀椀渀攀 㴀 昀甀渀挀琀椀漀渀 ⠀⤀ 笀ഀഀ
    element.removeClass(DIRTY_CLASS).addClass(PRISTINE_CLASS);਍    昀漀爀洀⸀␀搀椀爀琀礀 㴀 昀愀氀猀攀㬀ഀഀ
    form.$pristine = true;਍    昀漀爀䔀愀挀栀⠀挀漀渀琀爀漀氀猀Ⰰ 昀甀渀挀琀椀漀渀⠀挀漀渀琀爀漀氀⤀ 笀ഀഀ
      control.$setPristine();਍    紀⤀㬀ഀഀ
  };਍紀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngForm਍ ⨀ 䀀爀攀猀琀爀椀挀琀 䔀䄀䌀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Nestable alias of {@link ng.directive:form `form`} directive. HTML਍ ⨀ 搀漀攀猀 渀漀琀 愀氀氀漀眀 渀攀猀琀椀渀最 漀昀 昀漀爀洀 攀氀攀洀攀渀琀猀⸀ 䤀琀 椀猀 甀猀攀昀甀氀 琀漀 渀攀猀琀 昀漀爀洀猀Ⰰ 昀漀爀 攀砀愀洀瀀氀攀 椀昀 琀栀攀 瘀愀氀椀搀椀琀礀 漀昀 愀ഀഀ
 * sub-group of controls needs to be determined.਍ ⨀ഀഀ
 * @param {string=} ngForm|name Name of the form. If specified, the form controller will be published into਍ ⨀                       爀攀氀愀琀攀搀 猀挀漀瀀攀Ⰰ 甀渀搀攀爀 琀栀椀猀 渀愀洀攀⸀ഀഀ
 *਍ ⨀⼀ഀഀ
਍ ⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀昀漀爀洀ഀഀ
 * @restrict E਍ ⨀ഀഀ
 * @description਍ ⨀ 䐀椀爀攀挀琀椀瘀攀 琀栀愀琀 椀渀猀琀愀渀琀椀愀琀攀猀ഀഀ
 * {@link ng.directive:form.FormController FormController}.਍ ⨀ഀഀ
 * If the `name` attribute is specified, the form controller is published onto the current scope under਍ ⨀ 琀栀椀猀 渀愀洀攀⸀ഀഀ
 *਍ ⨀ ⌀ 䄀氀椀愀猀㨀 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䘀漀爀洀 怀渀最䘀漀爀洀怀紀ഀഀ
 *਍ ⨀ 䤀渀 䄀渀最甀氀愀爀 昀漀爀洀猀 挀愀渀 戀攀 渀攀猀琀攀搀⸀ 吀栀椀猀 洀攀愀渀猀 琀栀愀琀 琀栀攀 漀甀琀攀爀 昀漀爀洀 椀猀 瘀愀氀椀搀 眀栀攀渀 愀氀氀 漀昀 琀栀攀 挀栀椀氀搀ഀഀ
 * forms are valid as well. However, browsers do not allow nesting of `<form>` elements, so਍ ⨀ 䄀渀最甀氀愀爀 瀀爀漀瘀椀搀攀猀 琀栀攀 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䘀漀爀洀 怀渀最䘀漀爀洀怀紀 搀椀爀攀挀琀椀瘀攀 眀栀椀挀栀 戀攀栀愀瘀攀猀 椀搀攀渀琀椀挀愀氀氀礀 琀漀ഀഀ
 * `<form>` but can be nested.  This allows you to have nested forms, which is very useful when਍ ⨀ 甀猀椀渀最 䄀渀最甀氀愀爀 瘀愀氀椀搀愀琀椀漀渀 搀椀爀攀挀琀椀瘀攀猀 椀渀 昀漀爀洀猀 琀栀愀琀 愀爀攀 搀礀渀愀洀椀挀愀氀氀礀 最攀渀攀爀愀琀攀搀 甀猀椀渀最 琀栀攀ഀഀ
 * {@link ng.directive:ngRepeat `ngRepeat`} directive. Since you cannot dynamically generate the `name`਍ ⨀ 愀琀琀爀椀戀甀琀攀 漀昀 椀渀瀀甀琀 攀氀攀洀攀渀琀猀 甀猀椀渀最 椀渀琀攀爀瀀漀氀愀琀椀漀渀Ⰰ 礀漀甀 栀愀瘀攀 琀漀 眀爀愀瀀 攀愀挀栀 猀攀琀 漀昀 爀攀瀀攀愀琀攀搀 椀渀瀀甀琀猀 椀渀 愀渀ഀഀ
 * `ngForm` directive and nest these in an outer `form` element.਍ ⨀ഀഀ
 *਍ ⨀ ⌀ 䌀匀匀 挀氀愀猀猀攀猀ഀഀ
 *  - `ng-valid` is set if the form is valid.਍ ⨀  ⴀ 怀渀最ⴀ椀渀瘀愀氀椀搀怀 椀猀 猀攀琀 椀昀 琀栀攀 昀漀爀洀 椀猀 椀渀瘀愀氀椀搀⸀ഀഀ
 *  - `ng-pristine` is set if the form is pristine.਍ ⨀  ⴀ 怀渀最ⴀ搀椀爀琀礀怀 椀猀 猀攀琀 椀昀 琀栀攀 昀漀爀洀 椀猀 搀椀爀琀礀⸀ഀഀ
 *਍ ⨀ഀഀ
 * # Submitting a form and preventing the default action਍ ⨀ഀഀ
 * Since the role of forms in client-side Angular applications is different than in classical਍ ⨀ 爀漀甀渀搀琀爀椀瀀 愀瀀瀀猀Ⰰ 椀琀 椀猀 搀攀猀椀爀愀戀氀攀 昀漀爀 琀栀攀 戀爀漀眀猀攀爀 渀漀琀 琀漀 琀爀愀渀猀氀愀琀攀 琀栀攀 昀漀爀洀 猀甀戀洀椀猀猀椀漀渀 椀渀琀漀 愀 昀甀氀氀ഀഀ
 * page reload that sends the data to the server. Instead some javascript logic should be triggered਍ ⨀ 琀漀 栀愀渀搀氀攀 琀栀攀 昀漀爀洀 猀甀戀洀椀猀猀椀漀渀 椀渀 愀渀 愀瀀瀀氀椀挀愀琀椀漀渀ⴀ猀瀀攀挀椀昀椀挀 眀愀礀⸀ഀഀ
 *਍ ⨀ 䘀漀爀 琀栀椀猀 爀攀愀猀漀渀Ⰰ 䄀渀最甀氀愀爀 瀀爀攀瘀攀渀琀猀 琀栀攀 搀攀昀愀甀氀琀 愀挀琀椀漀渀 ⠀昀漀爀洀 猀甀戀洀椀猀猀椀漀渀 琀漀 琀栀攀 猀攀爀瘀攀爀⤀ 甀渀氀攀猀猀 琀栀攀ഀഀ
 * `<form>` element has an `action` attribute specified.਍ ⨀ഀഀ
 * You can use one of the following two ways to specify what javascript method should be called when਍ ⨀ 愀 昀漀爀洀 椀猀 猀甀戀洀椀琀琀攀搀㨀ഀഀ
 *਍ ⨀ ⴀ 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最匀甀戀洀椀琀 渀最匀甀戀洀椀琀紀 搀椀爀攀挀琀椀瘀攀 漀渀 琀栀攀 昀漀爀洀 攀氀攀洀攀渀琀ഀഀ
 * - {@link ng.directive:ngClick ngClick} directive on the first਍  ⨀  戀甀琀琀漀渀 漀爀 椀渀瀀甀琀 昀椀攀氀搀 漀昀 琀礀瀀攀 猀甀戀洀椀琀 ⠀椀渀瀀甀琀嬀琀礀瀀攀㴀猀甀戀洀椀琀崀⤀ഀഀ
 *਍ ⨀ 吀漀 瀀爀攀瘀攀渀琀 搀漀甀戀氀攀 攀砀攀挀甀琀椀漀渀 漀昀 琀栀攀 栀愀渀搀氀攀爀Ⰰ 甀猀攀 漀渀氀礀 漀渀攀 漀昀 琀栀攀 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最匀甀戀洀椀琀 渀最匀甀戀洀椀琀紀ഀഀ
 * or {@link ng.directive:ngClick ngClick} directives.਍ ⨀ 吀栀椀猀 椀猀 戀攀挀愀甀猀攀 漀昀 琀栀攀 昀漀氀氀漀眀椀渀最 昀漀爀洀 猀甀戀洀椀猀猀椀漀渀 爀甀氀攀猀 椀渀 琀栀攀 䠀吀䴀䰀 猀瀀攀挀椀昀椀挀愀琀椀漀渀㨀ഀഀ
 *਍ ⨀ ⴀ 䤀昀 愀 昀漀爀洀 栀愀猀 漀渀氀礀 漀渀攀 椀渀瀀甀琀 昀椀攀氀搀 琀栀攀渀 栀椀琀琀椀渀最 攀渀琀攀爀 椀渀 琀栀椀猀 昀椀攀氀搀 琀爀椀最最攀爀猀 昀漀爀洀 猀甀戀洀椀琀ഀഀ
 * (`ngSubmit`)਍ ⨀ ⴀ 椀昀 愀 昀漀爀洀 栀愀猀 ㈀⬀ 椀渀瀀甀琀 昀椀攀氀搀猀 愀渀搀 渀漀 戀甀琀琀漀渀猀 漀爀 椀渀瀀甀琀嬀琀礀瀀攀㴀猀甀戀洀椀琀崀 琀栀攀渀 栀椀琀琀椀渀最 攀渀琀攀爀ഀഀ
 * doesn't trigger submit਍ ⨀ ⴀ 椀昀 愀 昀漀爀洀 栀愀猀 漀渀攀 漀爀 洀漀爀攀 椀渀瀀甀琀 昀椀攀氀搀猀 愀渀搀 漀渀攀 漀爀 洀漀爀攀 戀甀琀琀漀渀猀 漀爀 椀渀瀀甀琀嬀琀礀瀀攀㴀猀甀戀洀椀琀崀 琀栀攀渀ഀഀ
 * hitting enter in any of the input fields will trigger the click handler on the *first* button or਍ ⨀ 椀渀瀀甀琀嬀琀礀瀀攀㴀猀甀戀洀椀琀崀 ⠀怀渀最䌀氀椀挀欀怀⤀ ⨀愀渀搀⨀ 愀 猀甀戀洀椀琀 栀愀渀搀氀攀爀 漀渀 琀栀攀 攀渀挀氀漀猀椀渀最 昀漀爀洀 ⠀怀渀最匀甀戀洀椀琀怀⤀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 渀愀洀攀 一愀洀攀 漀昀 琀栀攀 昀漀爀洀⸀ 䤀昀 猀瀀攀挀椀昀椀攀搀Ⰰ 琀栀攀 昀漀爀洀 挀漀渀琀爀漀氀氀攀爀 眀椀氀氀 戀攀 瀀甀戀氀椀猀栀攀搀 椀渀琀漀ഀഀ
 *                       related scope, under this name.਍ ⨀ഀഀ
 * @example਍    㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
      <doc:source>਍       㰀猀挀爀椀瀀琀㸀ഀഀ
         function Ctrl($scope) {਍           ␀猀挀漀瀀攀⸀甀猀攀爀吀礀瀀攀 㴀 ✀最甀攀猀琀✀㬀ഀഀ
         }਍       㰀⼀猀挀爀椀瀀琀㸀ഀഀ
       <form name="myForm" ng-controller="Ctrl">਍         甀猀攀爀吀礀瀀攀㨀 㰀椀渀瀀甀琀 渀愀洀攀㴀∀椀渀瀀甀琀∀ 渀最ⴀ洀漀搀攀氀㴀∀甀猀攀爀吀礀瀀攀∀ 爀攀焀甀椀爀攀搀㸀ഀഀ
         <span class="error" ng-show="myForm.input.$error.required">Required!</span><br>਍         㰀琀琀㸀甀猀攀爀吀礀瀀攀 㴀 笀笀甀猀攀爀吀礀瀀攀紀紀㰀⼀琀琀㸀㰀戀爀㸀ഀഀ
         <tt>myForm.input.$valid = {{myForm.input.$valid}}</tt><br>਍         㰀琀琀㸀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀 㴀 笀笀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀紀紀㰀⼀琀琀㸀㰀戀爀㸀ഀഀ
         <tt>myForm.$valid = {{myForm.$valid}}</tt><br>਍         㰀琀琀㸀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀 㴀 笀笀℀℀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀紀紀㰀⼀琀琀㸀㰀戀爀㸀ഀഀ
        </form>਍      㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      <doc:scenario>਍        椀琀⠀✀猀栀漀甀氀搀 椀渀椀琀椀愀氀椀稀攀 琀漀 洀漀搀攀氀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         expect(binding('userType')).toEqual('guest');਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀瘀愀氀椀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀琀爀甀攀✀⤀㬀ഀഀ
        });਍ഀഀ
        it('should be invalid if empty', function() {਍         椀渀瀀甀琀⠀✀甀猀攀爀吀礀瀀攀✀⤀⸀攀渀琀攀爀⠀✀✀⤀㬀ഀഀ
         expect(binding('userType')).toEqual('');਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀瘀愀氀椀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀昀愀氀猀攀✀⤀㬀ഀഀ
        });਍      㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
    </doc:example>਍ ⨀⼀ഀഀ
var formDirectiveFactory = function(isNgForm) {਍  爀攀琀甀爀渀 嬀✀␀琀椀洀攀漀甀琀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀琀椀洀攀漀甀琀⤀ 笀ഀഀ
    var formDirective = {਍      渀愀洀攀㨀 ✀昀漀爀洀✀Ⰰഀഀ
      restrict: isNgForm ? 'EAC' : 'E',਍      挀漀渀琀爀漀氀氀攀爀㨀 䘀漀爀洀䌀漀渀琀爀漀氀氀攀爀Ⰰഀഀ
      compile: function() {਍        爀攀琀甀爀渀 笀ഀഀ
          pre: function(scope, formElement, attr, controller) {਍            椀昀 ⠀℀愀琀琀爀⸀愀挀琀椀漀渀⤀ 笀ഀഀ
              // we can't use jq events because if a form is destroyed during submission the default਍              ⼀⼀ 愀挀琀椀漀渀 椀猀 渀漀琀 瀀爀攀瘀攀渀琀攀搀⸀ 猀攀攀 ⌀㄀㈀㌀㠀ഀഀ
              //਍              ⼀⼀ 䤀䔀 㤀 椀猀 渀漀琀 愀昀昀攀挀琀攀搀 戀攀挀愀甀猀攀 椀琀 搀漀攀猀渀✀琀 昀椀爀攀 愀 猀甀戀洀椀琀 攀瘀攀渀琀 愀渀搀 琀爀礀 琀漀 搀漀 愀 昀甀氀氀ഀഀ
              // page reload if the form was destroyed by submission of the form via a click handler਍              ⼀⼀ 漀渀 愀 戀甀琀琀漀渀 椀渀 琀栀攀 昀漀爀洀⸀ 䰀漀漀欀猀 氀椀欀攀 愀渀 䤀䔀㤀 猀瀀攀挀椀昀椀挀 戀甀最⸀ഀഀ
              var preventDefaultListener = function(event) {਍                攀瘀攀渀琀⸀瀀爀攀瘀攀渀琀䐀攀昀愀甀氀琀ഀഀ
                  ? event.preventDefault()਍                  㨀 攀瘀攀渀琀⸀爀攀琀甀爀渀嘀愀氀甀攀 㴀 昀愀氀猀攀㬀 ⼀⼀ 䤀䔀ഀഀ
              };਍ഀഀ
              addEventListenerFn(formElement[0], 'submit', preventDefaultListener);਍ഀഀ
              // unregister the preventDefault listener so that we don't not leak memory but in a਍              ⼀⼀ 眀愀礀 琀栀愀琀 眀椀氀氀 愀挀栀椀攀瘀攀 琀栀攀 瀀爀攀瘀攀渀琀椀漀渀 漀昀 琀栀攀 搀攀昀愀甀氀琀 愀挀琀椀漀渀⸀ഀഀ
              formElement.on('$destroy', function() {਍                ␀琀椀洀攀漀甀琀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
                  removeEventListenerFn(formElement[0], 'submit', preventDefaultListener);਍                紀Ⰰ 　Ⰰ 昀愀氀猀攀⤀㬀ഀഀ
              });਍            紀ഀഀ
਍            瘀愀爀 瀀愀爀攀渀琀䘀漀爀洀䌀琀爀氀 㴀 昀漀爀洀䔀氀攀洀攀渀琀⸀瀀愀爀攀渀琀⠀⤀⸀挀漀渀琀爀漀氀氀攀爀⠀✀昀漀爀洀✀⤀Ⰰഀഀ
                alias = attr.name || attr.ngForm;਍ഀഀ
            if (alias) {਍              猀攀琀琀攀爀⠀猀挀漀瀀攀Ⰰ 愀氀椀愀猀Ⰰ 挀漀渀琀爀漀氀氀攀爀Ⰰ 愀氀椀愀猀⤀㬀ഀഀ
            }਍            椀昀 ⠀瀀愀爀攀渀琀䘀漀爀洀䌀琀爀氀⤀ 笀ഀഀ
              formElement.on('$destroy', function() {਍                瀀愀爀攀渀琀䘀漀爀洀䌀琀爀氀⸀␀爀攀洀漀瘀攀䌀漀渀琀爀漀氀⠀挀漀渀琀爀漀氀氀攀爀⤀㬀ഀഀ
                if (alias) {਍                  猀攀琀琀攀爀⠀猀挀漀瀀攀Ⰰ 愀氀椀愀猀Ⰰ 甀渀搀攀昀椀渀攀搀Ⰰ 愀氀椀愀猀⤀㬀ഀഀ
                }਍                攀砀琀攀渀搀⠀挀漀渀琀爀漀氀氀攀爀Ⰰ 渀甀氀氀䘀漀爀洀䌀琀爀氀⤀㬀 ⼀⼀猀琀漀瀀 瀀爀漀瀀愀最愀琀椀渀最 挀栀椀氀搀 搀攀猀琀爀甀挀琀椀漀渀 栀愀渀搀氀攀爀猀 甀瀀眀愀爀搀猀ഀഀ
              });਍            紀ഀഀ
          }਍        紀㬀ഀഀ
      }਍    紀㬀ഀഀ
਍    爀攀琀甀爀渀 昀漀爀洀䐀椀爀攀挀琀椀瘀攀㬀ഀഀ
  }];਍紀㬀ഀഀ
਍瘀愀爀 昀漀爀洀䐀椀爀攀挀琀椀瘀攀 㴀 昀漀爀洀䐀椀爀攀挀琀椀瘀攀䘀愀挀琀漀爀礀⠀⤀㬀ഀഀ
var ngFormDirective = formDirectiveFactory(true);਍ഀഀ
/* global਍ഀഀ
    -VALID_CLASS,਍    ⴀ䤀一嘀䄀䰀䤀䐀开䌀䰀䄀匀匀Ⰰഀഀ
    -PRISTINE_CLASS,਍    ⴀ䐀䤀刀吀夀开䌀䰀䄀匀匀ഀഀ
*/਍ഀഀ
var URL_REGEXP = /^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/;਍瘀愀爀 䔀䴀䄀䤀䰀开刀䔀䜀䔀堀倀 㴀 ⼀帀嬀䄀ⴀ娀愀ⴀ稀　ⴀ㤀⸀开─⬀ⴀ崀⬀䀀嬀䄀ⴀ娀愀ⴀ稀　ⴀ㤀⸀ⴀ崀⬀尀⸀嬀䄀ⴀ娀愀ⴀ稀崀笀㈀Ⰰ㘀紀␀⼀㬀ഀഀ
var NUMBER_REGEXP = /^\s*(\-|\+)?(\d+|(\d*(\.\d*)))\s*$/;਍ഀഀ
var inputType = {਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 椀渀瀀甀琀吀礀瀀攀ഀഀ
   * @name ng.directive:input.text਍   ⨀ഀഀ
   * @description਍   ⨀ 匀琀愀渀搀愀爀搀 䠀吀䴀䰀 琀攀砀琀 椀渀瀀甀琀 眀椀琀栀 愀渀最甀氀愀爀 搀愀琀愀 戀椀渀搀椀渀最⸀ഀഀ
   *਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀最䴀漀搀攀氀 䄀猀猀椀最渀愀戀氀攀 愀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 琀漀 搀愀琀愀ⴀ戀椀渀搀 琀漀⸀ഀഀ
   * @param {string=} name Property name of the form under which the control is published.਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 爀攀焀甀椀爀攀搀 䄀搀搀猀 怀爀攀焀甀椀爀攀搀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 椀猀 渀漀琀 攀渀琀攀爀攀搀⸀ഀഀ
   * @param {string=} ngRequired Adds `required` attribute and `required` validation constraint to਍   ⨀    琀栀攀 攀氀攀洀攀渀琀 眀栀攀渀 琀栀攀 渀最刀攀焀甀椀爀攀搀 攀砀瀀爀攀猀猀椀漀渀 攀瘀愀氀甀愀琀攀猀 琀漀 琀爀甀攀⸀ 唀猀攀 怀渀最刀攀焀甀椀爀攀搀怀 椀渀猀琀攀愀搀 漀昀ഀഀ
   *    `required` when you want to data-bind to the `required` attribute.਍   ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀㴀紀 渀最䴀椀渀氀攀渀最琀栀 匀攀琀猀 怀洀椀渀氀攀渀最琀栀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 椀猀 猀栀漀爀琀攀爀 琀栀愀渀ഀഀ
   *    minlength.਍   ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀㴀紀 渀最䴀愀砀氀攀渀最琀栀 匀攀琀猀 怀洀愀砀氀攀渀最琀栀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 椀猀 氀漀渀最攀爀 琀栀愀渀ഀഀ
   *    maxlength.਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 渀最倀愀琀琀攀爀渀 匀攀琀猀 怀瀀愀琀琀攀爀渀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 搀漀攀猀 渀漀琀 洀愀琀挀栀 琀栀攀ഀഀ
   *    RegExp pattern expression. Expected value is `/regexp/` for inline patterns or `regexp` for਍   ⨀    瀀愀琀琀攀爀渀猀 搀攀昀椀渀攀搀 愀猀 猀挀漀瀀攀 攀砀瀀爀攀猀猀椀漀渀猀⸀ഀഀ
   * @param {string=} ngChange Angular expression to be executed when input changes due to user਍   ⨀    椀渀琀攀爀愀挀琀椀漀渀 眀椀琀栀 琀栀攀 椀渀瀀甀琀 攀氀攀洀攀渀琀⸀ഀഀ
   * @param {boolean=} [ngTrim=true] If set to false Angular will not automatically trim the input.਍   ⨀ഀഀ
   * @example਍      㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
        <doc:source>਍         㰀猀挀爀椀瀀琀㸀ഀഀ
           function Ctrl($scope) {਍             ␀猀挀漀瀀攀⸀琀攀砀琀 㴀 ✀最甀攀猀琀✀㬀ഀഀ
             $scope.word = /^\s*\w*\s*$/;਍           紀ഀഀ
         </script>਍         㰀昀漀爀洀 渀愀洀攀㴀∀洀礀䘀漀爀洀∀ 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀䌀琀爀氀∀㸀ഀഀ
           Single word: <input type="text" name="input" ng-model="text"਍                               渀最ⴀ瀀愀琀琀攀爀渀㴀∀眀漀爀搀∀ 爀攀焀甀椀爀攀搀 渀最ⴀ琀爀椀洀㴀∀昀愀氀猀攀∀㸀ഀഀ
           <span class="error" ng-show="myForm.input.$error.required">਍             刀攀焀甀椀爀攀搀℀㰀⼀猀瀀愀渀㸀ഀഀ
           <span class="error" ng-show="myForm.input.$error.pattern">਍             匀椀渀最氀攀 眀漀爀搀 漀渀氀礀℀㰀⼀猀瀀愀渀㸀ഀഀ
਍           㰀琀琀㸀琀攀砀琀 㴀 笀笀琀攀砀琀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
           <tt>myForm.input.$valid = {{myForm.input.$valid}}</tt><br/>਍           㰀琀琀㸀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀 㴀 笀笀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
           <tt>myForm.$valid = {{myForm.$valid}}</tt><br/>਍           㰀琀琀㸀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀 㴀 笀笀℀℀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
          </form>਍        㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
        <doc:scenario>਍          椀琀⠀✀猀栀漀甀氀搀 椀渀椀琀椀愀氀椀稀攀 琀漀 洀漀搀攀氀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
            expect(binding('text')).toEqual('guest');਍            攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀瘀愀氀椀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀琀爀甀攀✀⤀㬀ഀഀ
          });਍ഀഀ
          it('should be invalid if empty', function() {਍            椀渀瀀甀琀⠀✀琀攀砀琀✀⤀⸀攀渀琀攀爀⠀✀✀⤀㬀ഀഀ
            expect(binding('text')).toEqual('');਍            攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀瘀愀氀椀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀昀愀氀猀攀✀⤀㬀ഀഀ
          });਍ഀഀ
          it('should be invalid if multi word', function() {਍            椀渀瀀甀琀⠀✀琀攀砀琀✀⤀⸀攀渀琀攀爀⠀✀栀攀氀氀漀 眀漀爀氀搀✀⤀㬀ഀഀ
            expect(binding('myForm.input.$valid')).toEqual('false');਍          紀⤀㬀ഀഀ
਍          椀琀⠀✀猀栀漀甀氀搀 渀漀琀 戀攀 琀爀椀洀洀攀搀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
            input('text').enter('untrimmed ');਍            攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀琀攀砀琀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀甀渀琀爀椀洀洀攀搀 ✀⤀㬀ഀഀ
            expect(binding('myForm.input.$valid')).toEqual('true');਍          紀⤀㬀ഀഀ
        </doc:scenario>਍      㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
   */਍  ✀琀攀砀琀✀㨀 琀攀砀琀䤀渀瀀甀琀吀礀瀀攀Ⰰഀഀ
਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 椀渀瀀甀琀吀礀瀀攀ഀഀ
   * @name ng.directive:input.number਍   ⨀ഀഀ
   * @description਍   ⨀ 吀攀砀琀 椀渀瀀甀琀 眀椀琀栀 渀甀洀戀攀爀 瘀愀氀椀搀愀琀椀漀渀 愀渀搀 琀爀愀渀猀昀漀爀洀愀琀椀漀渀⸀ 匀攀琀猀 琀栀攀 怀渀甀洀戀攀爀怀 瘀愀氀椀搀愀琀椀漀渀ഀഀ
   * error if not a valid number.਍   ⨀ഀഀ
   * @param {string} ngModel Assignable angular expression to data-bind to.਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 渀愀洀攀 倀爀漀瀀攀爀琀礀 渀愀洀攀 漀昀 琀栀攀 昀漀爀洀 甀渀搀攀爀 眀栀椀挀栀 琀栀攀 挀漀渀琀爀漀氀 椀猀 瀀甀戀氀椀猀栀攀搀⸀ഀഀ
   * @param {string=} min Sets the `min` validation error key if the value entered is less than `min`.਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 洀愀砀 匀攀琀猀 琀栀攀 怀洀愀砀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 攀渀琀攀爀攀搀 椀猀 最爀攀愀琀攀爀 琀栀愀渀 怀洀愀砀怀⸀ഀഀ
   * @param {string=} required Sets `required` validation error key if the value is not entered.਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 渀最刀攀焀甀椀爀攀搀 䄀搀搀猀 怀爀攀焀甀椀爀攀搀怀 愀琀琀爀椀戀甀琀攀 愀渀搀 怀爀攀焀甀椀爀攀搀怀 瘀愀氀椀搀愀琀椀漀渀 挀漀渀猀琀爀愀椀渀琀 琀漀ഀഀ
   *    the element when the ngRequired expression evaluates to true. Use `ngRequired` instead of਍   ⨀    怀爀攀焀甀椀爀攀搀怀 眀栀攀渀 礀漀甀 眀愀渀琀 琀漀 搀愀琀愀ⴀ戀椀渀搀 琀漀 琀栀攀 怀爀攀焀甀椀爀攀搀怀 愀琀琀爀椀戀甀琀攀⸀ഀഀ
   * @param {number=} ngMinlength Sets `minlength` validation error key if the value is shorter than਍   ⨀    洀椀渀氀攀渀最琀栀⸀ഀഀ
   * @param {number=} ngMaxlength Sets `maxlength` validation error key if the value is longer than਍   ⨀    洀愀砀氀攀渀最琀栀⸀ഀഀ
   * @param {string=} ngPattern Sets `pattern` validation error key if the value does not match the਍   ⨀    刀攀最䔀砀瀀 瀀愀琀琀攀爀渀 攀砀瀀爀攀猀猀椀漀渀⸀ 䔀砀瀀攀挀琀攀搀 瘀愀氀甀攀 椀猀 怀⼀爀攀最攀砀瀀⼀怀 昀漀爀 椀渀氀椀渀攀 瀀愀琀琀攀爀渀猀 漀爀 怀爀攀最攀砀瀀怀 昀漀爀ഀഀ
   *    patterns defined as scope expressions.਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 渀最䌀栀愀渀最攀 䄀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 琀漀 戀攀 攀砀攀挀甀琀攀搀 眀栀攀渀 椀渀瀀甀琀 挀栀愀渀最攀猀 搀甀攀 琀漀 甀猀攀爀ഀഀ
   *    interaction with the input element.਍   ⨀ഀഀ
   * @example਍      㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
        <doc:source>਍         㰀猀挀爀椀瀀琀㸀ഀഀ
           function Ctrl($scope) {਍             ␀猀挀漀瀀攀⸀瘀愀氀甀攀 㴀 ㄀㈀㬀ഀഀ
           }਍         㰀⼀猀挀爀椀瀀琀㸀ഀഀ
         <form name="myForm" ng-controller="Ctrl">਍           一甀洀戀攀爀㨀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀渀甀洀戀攀爀∀ 渀愀洀攀㴀∀椀渀瀀甀琀∀ 渀最ⴀ洀漀搀攀氀㴀∀瘀愀氀甀攀∀ഀഀ
                          min="0" max="99" required>਍           㰀猀瀀愀渀 挀氀愀猀猀㴀∀攀爀爀漀爀∀ 渀最ⴀ猀栀漀眀㴀∀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀∀㸀ഀഀ
             Required!</span>਍           㰀猀瀀愀渀 挀氀愀猀猀㴀∀攀爀爀漀爀∀ 渀最ⴀ猀栀漀眀㴀∀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀⸀渀甀洀戀攀爀∀㸀ഀഀ
             Not valid number!</span>਍           㰀琀琀㸀瘀愀氀甀攀 㴀 笀笀瘀愀氀甀攀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
           <tt>myForm.input.$valid = {{myForm.input.$valid}}</tt><br/>਍           㰀琀琀㸀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀 㴀 笀笀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
           <tt>myForm.$valid = {{myForm.$valid}}</tt><br/>਍           㰀琀琀㸀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀 㴀 笀笀℀℀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
          </form>਍        㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
        <doc:scenario>਍          椀琀⠀✀猀栀漀甀氀搀 椀渀椀琀椀愀氀椀稀攀 琀漀 洀漀搀攀氀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
           expect(binding('value')).toEqual('12');਍           攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀瘀愀氀椀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀琀爀甀攀✀⤀㬀ഀഀ
          });਍ഀഀ
          it('should be invalid if empty', function() {਍           椀渀瀀甀琀⠀✀瘀愀氀甀攀✀⤀⸀攀渀琀攀爀⠀✀✀⤀㬀ഀഀ
           expect(binding('value')).toEqual('');਍           攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀瘀愀氀椀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀昀愀氀猀攀✀⤀㬀ഀഀ
          });਍ഀഀ
          it('should be invalid if over max', function() {਍           椀渀瀀甀琀⠀✀瘀愀氀甀攀✀⤀⸀攀渀琀攀爀⠀✀㄀㈀㌀✀⤀㬀ഀഀ
           expect(binding('value')).toEqual('');਍           攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀瘀愀氀椀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀昀愀氀猀攀✀⤀㬀ഀഀ
          });਍        㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
      </doc:example>਍   ⨀⼀ഀഀ
  'number': numberInputType,਍ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc inputType਍   ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀椀渀瀀甀琀⸀甀爀氀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Text input with URL validation. Sets the `url` validation error key if the content is not a਍   ⨀ 瘀愀氀椀搀 唀刀䰀⸀ഀഀ
   *਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀最䴀漀搀攀氀 䄀猀猀椀最渀愀戀氀攀 愀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 琀漀 搀愀琀愀ⴀ戀椀渀搀 琀漀⸀ഀഀ
   * @param {string=} name Property name of the form under which the control is published.਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 爀攀焀甀椀爀攀搀 匀攀琀猀 怀爀攀焀甀椀爀攀搀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 椀猀 渀漀琀 攀渀琀攀爀攀搀⸀ഀഀ
   * @param {string=} ngRequired Adds `required` attribute and `required` validation constraint to਍   ⨀    琀栀攀 攀氀攀洀攀渀琀 眀栀攀渀 琀栀攀 渀最刀攀焀甀椀爀攀搀 攀砀瀀爀攀猀猀椀漀渀 攀瘀愀氀甀愀琀攀猀 琀漀 琀爀甀攀⸀ 唀猀攀 怀渀最刀攀焀甀椀爀攀搀怀 椀渀猀琀攀愀搀 漀昀ഀഀ
   *    `required` when you want to data-bind to the `required` attribute.਍   ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀㴀紀 渀最䴀椀渀氀攀渀最琀栀 匀攀琀猀 怀洀椀渀氀攀渀最琀栀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 椀猀 猀栀漀爀琀攀爀 琀栀愀渀ഀഀ
   *    minlength.਍   ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀㴀紀 渀最䴀愀砀氀攀渀最琀栀 匀攀琀猀 怀洀愀砀氀攀渀最琀栀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 椀猀 氀漀渀最攀爀 琀栀愀渀ഀഀ
   *    maxlength.਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 渀最倀愀琀琀攀爀渀 匀攀琀猀 怀瀀愀琀琀攀爀渀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 搀漀攀猀 渀漀琀 洀愀琀挀栀 琀栀攀ഀഀ
   *    RegExp pattern expression. Expected value is `/regexp/` for inline patterns or `regexp` for਍   ⨀    瀀愀琀琀攀爀渀猀 搀攀昀椀渀攀搀 愀猀 猀挀漀瀀攀 攀砀瀀爀攀猀猀椀漀渀猀⸀ഀഀ
   * @param {string=} ngChange Angular expression to be executed when input changes due to user਍   ⨀    椀渀琀攀爀愀挀琀椀漀渀 眀椀琀栀 琀栀攀 椀渀瀀甀琀 攀氀攀洀攀渀琀⸀ഀഀ
   *਍   ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
      <doc:example>਍        㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
         <script>਍           昀甀渀挀琀椀漀渀 䌀琀爀氀⠀␀猀挀漀瀀攀⤀ 笀ഀഀ
             $scope.text = 'http://google.com';਍           紀ഀഀ
         </script>਍         㰀昀漀爀洀 渀愀洀攀㴀∀洀礀䘀漀爀洀∀ 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀䌀琀爀氀∀㸀ഀഀ
           URL: <input type="url" name="input" ng-model="text" required>਍           㰀猀瀀愀渀 挀氀愀猀猀㴀∀攀爀爀漀爀∀ 渀最ⴀ猀栀漀眀㴀∀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀∀㸀ഀഀ
             Required!</span>਍           㰀猀瀀愀渀 挀氀愀猀猀㴀∀攀爀爀漀爀∀ 渀最ⴀ猀栀漀眀㴀∀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀⸀甀爀氀∀㸀ഀഀ
             Not valid url!</span>਍           㰀琀琀㸀琀攀砀琀 㴀 笀笀琀攀砀琀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
           <tt>myForm.input.$valid = {{myForm.input.$valid}}</tt><br/>਍           㰀琀琀㸀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀 㴀 笀笀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
           <tt>myForm.$valid = {{myForm.$valid}}</tt><br/>਍           㰀琀琀㸀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀 㴀 笀笀℀℀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
           <tt>myForm.$error.url = {{!!myForm.$error.url}}</tt><br/>਍          㰀⼀昀漀爀洀㸀ഀഀ
        </doc:source>਍        㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
          it('should initialize to model', function() {਍            攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀琀攀砀琀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀栀琀琀瀀㨀⼀⼀最漀漀最氀攀⸀挀漀洀✀⤀㬀ഀഀ
            expect(binding('myForm.input.$valid')).toEqual('true');਍          紀⤀㬀ഀഀ
਍          椀琀⠀✀猀栀漀甀氀搀 戀攀 椀渀瘀愀氀椀搀 椀昀 攀洀瀀琀礀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
            input('text').enter('');਍            攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀琀攀砀琀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀✀⤀㬀ഀഀ
            expect(binding('myForm.input.$valid')).toEqual('false');਍          紀⤀㬀ഀഀ
਍          椀琀⠀✀猀栀漀甀氀搀 戀攀 椀渀瘀愀氀椀搀 椀昀 渀漀琀 甀爀氀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
            input('text').enter('xxx');਍            攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀瘀愀氀椀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀昀愀氀猀攀✀⤀㬀ഀഀ
          });਍        㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
      </doc:example>਍   ⨀⼀ഀഀ
  'url': urlInputType,਍ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc inputType਍   ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀椀渀瀀甀琀⸀攀洀愀椀氀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Text input with email validation. Sets the `email` validation error key if not a valid email਍   ⨀ 愀搀搀爀攀猀猀⸀ഀഀ
   *਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀最䴀漀搀攀氀 䄀猀猀椀最渀愀戀氀攀 愀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 琀漀 搀愀琀愀ⴀ戀椀渀搀 琀漀⸀ഀഀ
   * @param {string=} name Property name of the form under which the control is published.਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 爀攀焀甀椀爀攀搀 匀攀琀猀 怀爀攀焀甀椀爀攀搀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 椀猀 渀漀琀 攀渀琀攀爀攀搀⸀ഀഀ
   * @param {string=} ngRequired Adds `required` attribute and `required` validation constraint to਍   ⨀    琀栀攀 攀氀攀洀攀渀琀 眀栀攀渀 琀栀攀 渀最刀攀焀甀椀爀攀搀 攀砀瀀爀攀猀猀椀漀渀 攀瘀愀氀甀愀琀攀猀 琀漀 琀爀甀攀⸀ 唀猀攀 怀渀最刀攀焀甀椀爀攀搀怀 椀渀猀琀攀愀搀 漀昀ഀഀ
   *    `required` when you want to data-bind to the `required` attribute.਍   ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀㴀紀 渀最䴀椀渀氀攀渀最琀栀 匀攀琀猀 怀洀椀渀氀攀渀最琀栀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 椀猀 猀栀漀爀琀攀爀 琀栀愀渀ഀഀ
   *    minlength.਍   ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀㴀紀 渀最䴀愀砀氀攀渀最琀栀 匀攀琀猀 怀洀愀砀氀攀渀最琀栀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 椀猀 氀漀渀最攀爀 琀栀愀渀ഀഀ
   *    maxlength.਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 渀最倀愀琀琀攀爀渀 匀攀琀猀 怀瀀愀琀琀攀爀渀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 搀漀攀猀 渀漀琀 洀愀琀挀栀 琀栀攀ഀഀ
   *    RegExp pattern expression. Expected value is `/regexp/` for inline patterns or `regexp` for਍   ⨀    瀀愀琀琀攀爀渀猀 搀攀昀椀渀攀搀 愀猀 猀挀漀瀀攀 攀砀瀀爀攀猀猀椀漀渀猀⸀ഀഀ
   * @param {string=} ngChange Angular expression to be executed when input changes due to user਍   ⨀    椀渀琀攀爀愀挀琀椀漀渀 眀椀琀栀 琀栀攀 椀渀瀀甀琀 攀氀攀洀攀渀琀⸀ഀഀ
   *਍   ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
      <doc:example>਍        㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
         <script>਍           昀甀渀挀琀椀漀渀 䌀琀爀氀⠀␀猀挀漀瀀攀⤀ 笀ഀഀ
             $scope.text = 'me@example.com';਍           紀ഀഀ
         </script>਍           㰀昀漀爀洀 渀愀洀攀㴀∀洀礀䘀漀爀洀∀ 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀䌀琀爀氀∀㸀ഀഀ
             Email: <input type="email" name="input" ng-model="text" required>਍             㰀猀瀀愀渀 挀氀愀猀猀㴀∀攀爀爀漀爀∀ 渀最ⴀ猀栀漀眀㴀∀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀∀㸀ഀഀ
               Required!</span>਍             㰀猀瀀愀渀 挀氀愀猀猀㴀∀攀爀爀漀爀∀ 渀最ⴀ猀栀漀眀㴀∀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀⸀攀洀愀椀氀∀㸀ഀഀ
               Not valid email!</span>਍             㰀琀琀㸀琀攀砀琀 㴀 笀笀琀攀砀琀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
             <tt>myForm.input.$valid = {{myForm.input.$valid}}</tt><br/>਍             㰀琀琀㸀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀 㴀 笀笀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀攀爀爀漀爀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
             <tt>myForm.$valid = {{myForm.$valid}}</tt><br/>਍             㰀琀琀㸀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀 㴀 笀笀℀℀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
             <tt>myForm.$error.email = {{!!myForm.$error.email}}</tt><br/>਍           㰀⼀昀漀爀洀㸀ഀഀ
        </doc:source>਍        㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
          it('should initialize to model', function() {਍            攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀琀攀砀琀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀洀攀䀀攀砀愀洀瀀氀攀⸀挀漀洀✀⤀㬀ഀഀ
            expect(binding('myForm.input.$valid')).toEqual('true');਍          紀⤀㬀ഀഀ
਍          椀琀⠀✀猀栀漀甀氀搀 戀攀 椀渀瘀愀氀椀搀 椀昀 攀洀瀀琀礀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
            input('text').enter('');਍            攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀琀攀砀琀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀✀⤀㬀ഀഀ
            expect(binding('myForm.input.$valid')).toEqual('false');਍          紀⤀㬀ഀഀ
਍          椀琀⠀✀猀栀漀甀氀搀 戀攀 椀渀瘀愀氀椀搀 椀昀 渀漀琀 攀洀愀椀氀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
            input('text').enter('xxx');਍            攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀椀渀瀀甀琀⸀␀瘀愀氀椀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀昀愀氀猀攀✀⤀㬀ഀഀ
          });਍        㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
      </doc:example>਍   ⨀⼀ഀഀ
  'email': emailInputType,਍ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc inputType਍   ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀椀渀瀀甀琀⸀爀愀搀椀漀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * HTML radio button.਍   ⨀ഀഀ
   * @param {string} ngModel Assignable angular expression to data-bind to.਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 瘀愀氀甀攀 吀栀攀 瘀愀氀甀攀 琀漀 眀栀椀挀栀 琀栀攀 攀砀瀀爀攀猀猀椀漀渀 猀栀漀甀氀搀 戀攀 猀攀琀 眀栀攀渀 猀攀氀攀挀琀攀搀⸀ഀഀ
   * @param {string=} name Property name of the form under which the control is published.਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 渀最䌀栀愀渀最攀 䄀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 琀漀 戀攀 攀砀攀挀甀琀攀搀 眀栀攀渀 椀渀瀀甀琀 挀栀愀渀最攀猀 搀甀攀 琀漀 甀猀攀爀ഀഀ
   *    interaction with the input element.਍   ⨀ഀഀ
   * @example਍      㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
        <doc:source>਍         㰀猀挀爀椀瀀琀㸀ഀഀ
           function Ctrl($scope) {਍             ␀猀挀漀瀀攀⸀挀漀氀漀爀 㴀 ✀戀氀甀攀✀㬀ഀഀ
           }਍         㰀⼀猀挀爀椀瀀琀㸀ഀഀ
         <form name="myForm" ng-controller="Ctrl">਍           㰀椀渀瀀甀琀 琀礀瀀攀㴀∀爀愀搀椀漀∀ 渀最ⴀ洀漀搀攀氀㴀∀挀漀氀漀爀∀ 瘀愀氀甀攀㴀∀爀攀搀∀㸀  刀攀搀 㰀戀爀⼀㸀ഀഀ
           <input type="radio" ng-model="color" value="green"> Green <br/>਍           㰀椀渀瀀甀琀 琀礀瀀攀㴀∀爀愀搀椀漀∀ 渀最ⴀ洀漀搀攀氀㴀∀挀漀氀漀爀∀ 瘀愀氀甀攀㴀∀戀氀甀攀∀㸀 䈀氀甀攀 㰀戀爀⼀㸀ഀഀ
           <tt>color = {{color}}</tt><br/>਍          㰀⼀昀漀爀洀㸀ഀഀ
        </doc:source>਍        㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
          it('should change state', function() {਍            攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀挀漀氀漀爀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀戀氀甀攀✀⤀㬀ഀഀ
਍            椀渀瀀甀琀⠀✀挀漀氀漀爀✀⤀⸀猀攀氀攀挀琀⠀✀爀攀搀✀⤀㬀ഀഀ
            expect(binding('color')).toEqual('red');਍          紀⤀㬀ഀഀ
        </doc:scenario>਍      㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
   */਍  ✀爀愀搀椀漀✀㨀 爀愀搀椀漀䤀渀瀀甀琀吀礀瀀攀Ⰰഀഀ
਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 椀渀瀀甀琀吀礀瀀攀ഀഀ
   * @name ng.directive:input.checkbox਍   ⨀ഀഀ
   * @description਍   ⨀ 䠀吀䴀䰀 挀栀攀挀欀戀漀砀⸀ഀഀ
   *਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀最䴀漀搀攀氀 䄀猀猀椀最渀愀戀氀攀 愀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 琀漀 搀愀琀愀ⴀ戀椀渀搀 琀漀⸀ഀഀ
   * @param {string=} name Property name of the form under which the control is published.਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 渀最吀爀甀攀嘀愀氀甀攀 吀栀攀 瘀愀氀甀攀 琀漀 眀栀椀挀栀 琀栀攀 攀砀瀀爀攀猀猀椀漀渀 猀栀漀甀氀搀 戀攀 猀攀琀 眀栀攀渀 猀攀氀攀挀琀攀搀⸀ഀഀ
   * @param {string=} ngFalseValue The value to which the expression should be set when not selected.਍   ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 渀最䌀栀愀渀最攀 䄀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 琀漀 戀攀 攀砀攀挀甀琀攀搀 眀栀攀渀 椀渀瀀甀琀 挀栀愀渀最攀猀 搀甀攀 琀漀 甀猀攀爀ഀഀ
   *    interaction with the input element.਍   ⨀ഀഀ
   * @example਍      㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
        <doc:source>਍         㰀猀挀爀椀瀀琀㸀ഀഀ
           function Ctrl($scope) {਍             ␀猀挀漀瀀攀⸀瘀愀氀甀攀㄀ 㴀 琀爀甀攀㬀ഀഀ
             $scope.value2 = 'YES'਍           紀ഀഀ
         </script>਍         㰀昀漀爀洀 渀愀洀攀㴀∀洀礀䘀漀爀洀∀ 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀䌀琀爀氀∀㸀ഀഀ
           Value1: <input type="checkbox" ng-model="value1"> <br/>਍           嘀愀氀甀攀㈀㨀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀挀栀攀挀欀戀漀砀∀ 渀最ⴀ洀漀搀攀氀㴀∀瘀愀氀甀攀㈀∀ഀഀ
                          ng-true-value="YES" ng-false-value="NO"> <br/>਍           㰀琀琀㸀瘀愀氀甀攀㄀ 㴀 笀笀瘀愀氀甀攀㄀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
           <tt>value2 = {{value2}}</tt><br/>਍          㰀⼀昀漀爀洀㸀ഀഀ
        </doc:source>਍        㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
          it('should change state', function() {਍            攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀瘀愀氀甀攀㄀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀琀爀甀攀✀⤀㬀ഀഀ
            expect(binding('value2')).toEqual('YES');਍ഀഀ
            input('value1').check();਍            椀渀瀀甀琀⠀✀瘀愀氀甀攀㈀✀⤀⸀挀栀攀挀欀⠀⤀㬀ഀഀ
            expect(binding('value1')).toEqual('false');਍            攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀瘀愀氀甀攀㈀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀一伀✀⤀㬀ഀഀ
          });਍        㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
      </doc:example>਍   ⨀⼀ഀഀ
  'checkbox': checkboxInputType,਍ഀഀ
  'hidden': noop,਍  ✀戀甀琀琀漀渀✀㨀 渀漀漀瀀Ⰰഀഀ
  'submit': noop,਍  ✀爀攀猀攀琀✀㨀 渀漀漀瀀ഀഀ
};਍ഀഀ
// A helper function to call $setValidity and return the value / undefined,਍⼀⼀ 愀 瀀愀琀琀攀爀渀 琀栀愀琀 椀猀 爀攀瀀攀愀琀攀搀 愀 氀漀琀 椀渀 琀栀攀 椀渀瀀甀琀 瘀愀氀椀搀愀琀椀漀渀 氀漀最椀挀⸀ഀഀ
function validate(ctrl, validatorName, validity, value){਍  挀琀爀氀⸀␀猀攀琀嘀愀氀椀搀椀琀礀⠀瘀愀氀椀搀愀琀漀爀一愀洀攀Ⰰ 瘀愀氀椀搀椀琀礀⤀㬀ഀഀ
  return validity ? value : undefined;਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 琀攀砀琀䤀渀瀀甀琀吀礀瀀攀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀Ⰰ 挀琀爀氀Ⰰ ␀猀渀椀昀昀攀爀Ⰰ ␀戀爀漀眀猀攀爀⤀ 笀ഀഀ
  // In composition mode, users are still inputing intermediate text buffer,਍  ⼀⼀ 栀漀氀搀 琀栀攀 氀椀猀琀攀渀攀爀 甀渀琀椀氀 挀漀洀瀀漀猀椀琀椀漀渀 椀猀 搀漀渀攀⸀ഀഀ
  // More about composition events: https://developer.mozilla.org/en-US/docs/Web/API/CompositionEvent਍  椀昀 ⠀℀␀猀渀椀昀昀攀爀⸀愀渀搀爀漀椀搀⤀ 笀ഀഀ
    var composing = false;਍ഀഀ
    element.on('compositionstart', function(data) {਍      挀漀洀瀀漀猀椀渀最 㴀 琀爀甀攀㬀ഀഀ
    });਍ഀഀ
    element.on('compositionend', function() {਍      挀漀洀瀀漀猀椀渀最 㴀 昀愀氀猀攀㬀ഀഀ
    });਍  紀ഀഀ
਍  瘀愀爀 氀椀猀琀攀渀攀爀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    if (composing) return;਍    瘀愀爀 瘀愀氀甀攀 㴀 攀氀攀洀攀渀琀⸀瘀愀氀⠀⤀㬀ഀഀ
਍    ⼀⼀ 䈀礀 搀攀昀愀甀氀琀 眀攀 眀椀氀氀 琀爀椀洀 琀栀攀 瘀愀氀甀攀ഀഀ
    // If the attribute ng-trim exists we will avoid trimming਍    ⼀⼀ 攀⸀最⸀ 㰀椀渀瀀甀琀 渀最ⴀ洀漀搀攀氀㴀∀昀漀漀∀ 渀最ⴀ琀爀椀洀㴀∀昀愀氀猀攀∀㸀ഀഀ
    if (toBoolean(attr.ngTrim || 'T')) {਍      瘀愀氀甀攀 㴀 琀爀椀洀⠀瘀愀氀甀攀⤀㬀ഀഀ
    }਍ഀഀ
    if (ctrl.$viewValue !== value) {਍      椀昀 ⠀猀挀漀瀀攀⸀␀␀瀀栀愀猀攀⤀ 笀ഀഀ
        ctrl.$setViewValue(value);਍      紀 攀氀猀攀 笀ഀഀ
        scope.$apply(function() {਍          挀琀爀氀⸀␀猀攀琀嘀椀攀眀嘀愀氀甀攀⠀瘀愀氀甀攀⤀㬀ഀഀ
        });਍      紀ഀഀ
    }਍  紀㬀ഀഀ
਍  ⼀⼀ 椀昀 琀栀攀 戀爀漀眀猀攀爀 搀漀攀猀 猀甀瀀瀀漀爀琀 ∀椀渀瀀甀琀∀ 攀瘀攀渀琀Ⰰ 眀攀 愀爀攀 昀椀渀攀 ⴀ 攀砀挀攀瀀琀 漀渀 䤀䔀㤀 眀栀椀挀栀 搀漀攀猀渀✀琀 昀椀爀攀 琀栀攀ഀഀ
  // input event on backspace, delete or cut਍  椀昀 ⠀␀猀渀椀昀昀攀爀⸀栀愀猀䔀瘀攀渀琀⠀✀椀渀瀀甀琀✀⤀⤀ 笀ഀഀ
    element.on('input', listener);਍  紀 攀氀猀攀 笀ഀഀ
    var timeout;਍ഀഀ
    var deferListener = function() {਍      椀昀 ⠀℀琀椀洀攀漀甀琀⤀ 笀ഀഀ
        timeout = $browser.defer(function() {਍          氀椀猀琀攀渀攀爀⠀⤀㬀ഀഀ
          timeout = null;਍        紀⤀㬀ഀഀ
      }਍    紀㬀ഀഀ
਍    攀氀攀洀攀渀琀⸀漀渀⠀✀欀攀礀搀漀眀渀✀Ⰰ 昀甀渀挀琀椀漀渀⠀攀瘀攀渀琀⤀ 笀ഀഀ
      var key = event.keyCode;਍ഀഀ
      // ignore਍      ⼀⼀    挀漀洀洀愀渀搀            洀漀搀椀昀椀攀爀猀                   愀爀爀漀眀猀ഀഀ
      if (key === 91 || (15 < key && key < 19) || (37 <= key && key <= 40)) return;਍ഀഀ
      deferListener();਍    紀⤀㬀ഀഀ
਍    ⼀⼀ 椀昀 甀猀攀爀 洀漀搀椀昀椀攀猀 椀渀瀀甀琀 瘀愀氀甀攀 甀猀椀渀最 挀漀渀琀攀砀琀 洀攀渀甀 椀渀 䤀䔀Ⰰ 眀攀 渀攀攀搀 ∀瀀愀猀琀攀∀ 愀渀搀 ∀挀甀琀∀ 攀瘀攀渀琀猀 琀漀 挀愀琀挀栀 椀琀ഀഀ
    if ($sniffer.hasEvent('paste')) {਍      攀氀攀洀攀渀琀⸀漀渀⠀✀瀀愀猀琀攀 挀甀琀✀Ⰰ 搀攀昀攀爀䰀椀猀琀攀渀攀爀⤀㬀ഀഀ
    }਍  紀ഀഀ
਍  ⼀⼀ 椀昀 甀猀攀爀 瀀愀猀琀攀 椀渀琀漀 椀渀瀀甀琀 甀猀椀渀最 洀漀甀猀攀 漀渀 漀氀搀攀爀 戀爀漀眀猀攀爀ഀഀ
  // or form autocomplete on newer browser, we need "change" event to catch it਍  攀氀攀洀攀渀琀⸀漀渀⠀✀挀栀愀渀最攀✀Ⰰ 氀椀猀琀攀渀攀爀⤀㬀ഀഀ
਍  挀琀爀氀⸀␀爀攀渀搀攀爀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    element.val(ctrl.$isEmpty(ctrl.$viewValue) ? '' : ctrl.$viewValue);਍  紀㬀ഀഀ
਍  ⼀⼀ 瀀愀琀琀攀爀渀 瘀愀氀椀搀愀琀漀爀ഀഀ
  var pattern = attr.ngPattern,਍      瀀愀琀琀攀爀渀嘀愀氀椀搀愀琀漀爀Ⰰഀഀ
      match;਍ഀഀ
  if (pattern) {਍    瘀愀爀 瘀愀氀椀搀愀琀攀刀攀最攀砀 㴀 昀甀渀挀琀椀漀渀⠀爀攀最攀砀瀀Ⰰ 瘀愀氀甀攀⤀ 笀ഀഀ
      return validate(ctrl, 'pattern', ctrl.$isEmpty(value) || regexp.test(value), value);਍    紀㬀ഀഀ
    match = pattern.match(/^\/(.*)\/([gim]*)$/);਍    椀昀 ⠀洀愀琀挀栀⤀ 笀ഀഀ
      pattern = new RegExp(match[1], match[2]);਍      瀀愀琀琀攀爀渀嘀愀氀椀搀愀琀漀爀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
        return validateRegex(pattern, value);਍      紀㬀ഀഀ
    } else {਍      瀀愀琀琀攀爀渀嘀愀氀椀搀愀琀漀爀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
        var patternObj = scope.$eval(pattern);਍ഀഀ
        if (!patternObj || !patternObj.test) {਍          琀栀爀漀眀 洀椀渀䔀爀爀⠀✀渀最倀愀琀琀攀爀渀✀⤀⠀✀渀漀爀攀最攀砀瀀✀Ⰰഀഀ
            'Expected {0} to be a RegExp but was {1}. Element: {2}', pattern,਍            瀀愀琀琀攀爀渀伀戀樀Ⰰ 猀琀愀爀琀椀渀最吀愀最⠀攀氀攀洀攀渀琀⤀⤀㬀ഀഀ
        }਍        爀攀琀甀爀渀 瘀愀氀椀搀愀琀攀刀攀最攀砀⠀瀀愀琀琀攀爀渀伀戀樀Ⰰ 瘀愀氀甀攀⤀㬀ഀഀ
      };਍    紀ഀഀ
਍    挀琀爀氀⸀␀昀漀爀洀愀琀琀攀爀猀⸀瀀甀猀栀⠀瀀愀琀琀攀爀渀嘀愀氀椀搀愀琀漀爀⤀㬀ഀഀ
    ctrl.$parsers.push(patternValidator);਍  紀ഀഀ
਍  ⼀⼀ 洀椀渀 氀攀渀最琀栀 瘀愀氀椀搀愀琀漀爀ഀഀ
  if (attr.ngMinlength) {਍    瘀愀爀 洀椀渀氀攀渀最琀栀 㴀 椀渀琀⠀愀琀琀爀⸀渀最䴀椀渀氀攀渀最琀栀⤀㬀ഀഀ
    var minLengthValidator = function(value) {਍      爀攀琀甀爀渀 瘀愀氀椀搀愀琀攀⠀挀琀爀氀Ⰰ ✀洀椀渀氀攀渀最琀栀✀Ⰰ 挀琀爀氀⸀␀椀猀䔀洀瀀琀礀⠀瘀愀氀甀攀⤀ 簀簀 瘀愀氀甀攀⸀氀攀渀最琀栀 㸀㴀 洀椀渀氀攀渀最琀栀Ⰰ 瘀愀氀甀攀⤀㬀ഀഀ
    };਍ഀഀ
    ctrl.$parsers.push(minLengthValidator);਍    挀琀爀氀⸀␀昀漀爀洀愀琀琀攀爀猀⸀瀀甀猀栀⠀洀椀渀䰀攀渀最琀栀嘀愀氀椀搀愀琀漀爀⤀㬀ഀഀ
  }਍ഀഀ
  // max length validator਍  椀昀 ⠀愀琀琀爀⸀渀最䴀愀砀氀攀渀最琀栀⤀ 笀ഀഀ
    var maxlength = int(attr.ngMaxlength);਍    瘀愀爀 洀愀砀䰀攀渀最琀栀嘀愀氀椀搀愀琀漀爀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
      return validate(ctrl, 'maxlength', ctrl.$isEmpty(value) || value.length <= maxlength, value);਍    紀㬀ഀഀ
਍    挀琀爀氀⸀␀瀀愀爀猀攀爀猀⸀瀀甀猀栀⠀洀愀砀䰀攀渀最琀栀嘀愀氀椀搀愀琀漀爀⤀㬀ഀഀ
    ctrl.$formatters.push(maxLengthValidator);਍  紀ഀഀ
}਍ഀഀ
function numberInputType(scope, element, attr, ctrl, $sniffer, $browser) {਍  琀攀砀琀䤀渀瀀甀琀吀礀瀀攀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀Ⰰ 挀琀爀氀Ⰰ ␀猀渀椀昀昀攀爀Ⰰ ␀戀爀漀眀猀攀爀⤀㬀ഀഀ
਍  挀琀爀氀⸀␀瀀愀爀猀攀爀猀⸀瀀甀猀栀⠀昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
    var empty = ctrl.$isEmpty(value);਍    椀昀 ⠀攀洀瀀琀礀 簀簀 一唀䴀䈀䔀刀开刀䔀䜀䔀堀倀⸀琀攀猀琀⠀瘀愀氀甀攀⤀⤀ 笀ഀഀ
      ctrl.$setValidity('number', true);਍      爀攀琀甀爀渀 瘀愀氀甀攀 㴀㴀㴀 ✀✀ 㼀 渀甀氀氀 㨀 ⠀攀洀瀀琀礀 㼀 瘀愀氀甀攀 㨀 瀀愀爀猀攀䘀氀漀愀琀⠀瘀愀氀甀攀⤀⤀㬀ഀഀ
    } else {਍      挀琀爀氀⸀␀猀攀琀嘀愀氀椀搀椀琀礀⠀✀渀甀洀戀攀爀✀Ⰰ 昀愀氀猀攀⤀㬀ഀഀ
      return undefined;਍    紀ഀഀ
  });਍ഀഀ
  ctrl.$formatters.push(function(value) {਍    爀攀琀甀爀渀 挀琀爀氀⸀␀椀猀䔀洀瀀琀礀⠀瘀愀氀甀攀⤀ 㼀 ✀✀ 㨀 ✀✀ ⬀ 瘀愀氀甀攀㬀ഀഀ
  });਍ഀഀ
  if (attr.min) {਍    瘀愀爀 洀椀渀嘀愀氀椀搀愀琀漀爀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
      var min = parseFloat(attr.min);਍      爀攀琀甀爀渀 瘀愀氀椀搀愀琀攀⠀挀琀爀氀Ⰰ ✀洀椀渀✀Ⰰ 挀琀爀氀⸀␀椀猀䔀洀瀀琀礀⠀瘀愀氀甀攀⤀ 簀簀 瘀愀氀甀攀 㸀㴀 洀椀渀Ⰰ 瘀愀氀甀攀⤀㬀ഀഀ
    };਍ഀഀ
    ctrl.$parsers.push(minValidator);਍    挀琀爀氀⸀␀昀漀爀洀愀琀琀攀爀猀⸀瀀甀猀栀⠀洀椀渀嘀愀氀椀搀愀琀漀爀⤀㬀ഀഀ
  }਍ഀഀ
  if (attr.max) {਍    瘀愀爀 洀愀砀嘀愀氀椀搀愀琀漀爀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
      var max = parseFloat(attr.max);਍      爀攀琀甀爀渀 瘀愀氀椀搀愀琀攀⠀挀琀爀氀Ⰰ ✀洀愀砀✀Ⰰ 挀琀爀氀⸀␀椀猀䔀洀瀀琀礀⠀瘀愀氀甀攀⤀ 簀簀 瘀愀氀甀攀 㰀㴀 洀愀砀Ⰰ 瘀愀氀甀攀⤀㬀ഀഀ
    };਍ഀഀ
    ctrl.$parsers.push(maxValidator);਍    挀琀爀氀⸀␀昀漀爀洀愀琀琀攀爀猀⸀瀀甀猀栀⠀洀愀砀嘀愀氀椀搀愀琀漀爀⤀㬀ഀഀ
  }਍ഀഀ
  ctrl.$formatters.push(function(value) {਍    爀攀琀甀爀渀 瘀愀氀椀搀愀琀攀⠀挀琀爀氀Ⰰ ✀渀甀洀戀攀爀✀Ⰰ 挀琀爀氀⸀␀椀猀䔀洀瀀琀礀⠀瘀愀氀甀攀⤀ 簀簀 椀猀一甀洀戀攀爀⠀瘀愀氀甀攀⤀Ⰰ 瘀愀氀甀攀⤀㬀ഀഀ
  });਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 甀爀氀䤀渀瀀甀琀吀礀瀀攀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀Ⰰ 挀琀爀氀Ⰰ ␀猀渀椀昀昀攀爀Ⰰ ␀戀爀漀眀猀攀爀⤀ 笀ഀഀ
  textInputType(scope, element, attr, ctrl, $sniffer, $browser);਍ഀഀ
  var urlValidator = function(value) {਍    爀攀琀甀爀渀 瘀愀氀椀搀愀琀攀⠀挀琀爀氀Ⰰ ✀甀爀氀✀Ⰰ 挀琀爀氀⸀␀椀猀䔀洀瀀琀礀⠀瘀愀氀甀攀⤀ 簀簀 唀刀䰀开刀䔀䜀䔀堀倀⸀琀攀猀琀⠀瘀愀氀甀攀⤀Ⰰ 瘀愀氀甀攀⤀㬀ഀഀ
  };਍ഀഀ
  ctrl.$formatters.push(urlValidator);਍  挀琀爀氀⸀␀瀀愀爀猀攀爀猀⸀瀀甀猀栀⠀甀爀氀嘀愀氀椀搀愀琀漀爀⤀㬀ഀഀ
}਍ഀഀ
function emailInputType(scope, element, attr, ctrl, $sniffer, $browser) {਍  琀攀砀琀䤀渀瀀甀琀吀礀瀀攀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀Ⰰ 挀琀爀氀Ⰰ ␀猀渀椀昀昀攀爀Ⰰ ␀戀爀漀眀猀攀爀⤀㬀ഀഀ
਍  瘀愀爀 攀洀愀椀氀嘀愀氀椀搀愀琀漀爀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
    return validate(ctrl, 'email', ctrl.$isEmpty(value) || EMAIL_REGEXP.test(value), value);਍  紀㬀ഀഀ
਍  挀琀爀氀⸀␀昀漀爀洀愀琀琀攀爀猀⸀瀀甀猀栀⠀攀洀愀椀氀嘀愀氀椀搀愀琀漀爀⤀㬀ഀഀ
  ctrl.$parsers.push(emailValidator);਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 爀愀搀椀漀䤀渀瀀甀琀吀礀瀀攀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀Ⰰ 挀琀爀氀⤀ 笀ഀഀ
  // make the name unique, if not defined਍  椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀愀琀琀爀⸀渀愀洀攀⤀⤀ 笀ഀഀ
    element.attr('name', nextUid());਍  紀ഀഀ
਍  攀氀攀洀攀渀琀⸀漀渀⠀✀挀氀椀挀欀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    if (element[0].checked) {਍      猀挀漀瀀攀⸀␀愀瀀瀀氀礀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
        ctrl.$setViewValue(attr.value);਍      紀⤀㬀ഀഀ
    }਍  紀⤀㬀ഀഀ
਍  挀琀爀氀⸀␀爀攀渀搀攀爀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var value = attr.value;਍    攀氀攀洀攀渀琀嬀　崀⸀挀栀攀挀欀攀搀 㴀 ⠀瘀愀氀甀攀 㴀㴀 挀琀爀氀⸀␀瘀椀攀眀嘀愀氀甀攀⤀㬀ഀഀ
  };਍ഀഀ
  attr.$observe('value', ctrl.$render);਍紀ഀഀ
਍昀甀渀挀琀椀漀渀 挀栀攀挀欀戀漀砀䤀渀瀀甀琀吀礀瀀攀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀Ⰰ 挀琀爀氀⤀ 笀ഀഀ
  var trueValue = attr.ngTrueValue,਍      昀愀氀猀攀嘀愀氀甀攀 㴀 愀琀琀爀⸀渀最䘀愀氀猀攀嘀愀氀甀攀㬀ഀഀ
਍  椀昀 ⠀℀椀猀匀琀爀椀渀最⠀琀爀甀攀嘀愀氀甀攀⤀⤀ 琀爀甀攀嘀愀氀甀攀 㴀 琀爀甀攀㬀ഀഀ
  if (!isString(falseValue)) falseValue = false;਍ഀഀ
  element.on('click', function() {਍    猀挀漀瀀攀⸀␀愀瀀瀀氀礀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
      ctrl.$setViewValue(element[0].checked);਍    紀⤀㬀ഀഀ
  });਍ഀഀ
  ctrl.$render = function() {਍    攀氀攀洀攀渀琀嬀　崀⸀挀栀攀挀欀攀搀 㴀 挀琀爀氀⸀␀瘀椀攀眀嘀愀氀甀攀㬀ഀഀ
  };਍ഀഀ
  // Override the standard `$isEmpty` because a value of `false` means empty in a checkbox.਍  挀琀爀氀⸀␀椀猀䔀洀瀀琀礀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
    return value !== trueValue;਍  紀㬀ഀഀ
਍  挀琀爀氀⸀␀昀漀爀洀愀琀琀攀爀猀⸀瀀甀猀栀⠀昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
    return value === trueValue;਍  紀⤀㬀ഀഀ
਍  挀琀爀氀⸀␀瀀愀爀猀攀爀猀⸀瀀甀猀栀⠀昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
    return value ? trueValue : falseValue;਍  紀⤀㬀ഀഀ
}਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀琀攀砀琀愀爀攀愀ഀഀ
 * @restrict E਍ ⨀ഀഀ
 * @description਍ ⨀ 䠀吀䴀䰀 琀攀砀琀愀爀攀愀 攀氀攀洀攀渀琀 挀漀渀琀爀漀氀 眀椀琀栀 愀渀最甀氀愀爀 搀愀琀愀ⴀ戀椀渀搀椀渀最⸀ 吀栀攀 搀愀琀愀ⴀ戀椀渀搀椀渀最 愀渀搀 瘀愀氀椀搀愀琀椀漀渀ഀഀ
 * properties of this element are exactly the same as those of the਍ ⨀ 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀椀渀瀀甀琀 椀渀瀀甀琀 攀氀攀洀攀渀琀紀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀最䴀漀搀攀氀 䄀猀猀椀最渀愀戀氀攀 愀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 琀漀 搀愀琀愀ⴀ戀椀渀搀 琀漀⸀ഀഀ
 * @param {string=} name Property name of the form under which the control is published.਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 爀攀焀甀椀爀攀搀 匀攀琀猀 怀爀攀焀甀椀爀攀搀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 椀猀 渀漀琀 攀渀琀攀爀攀搀⸀ഀഀ
 * @param {string=} ngRequired Adds `required` attribute and `required` validation constraint to਍ ⨀    琀栀攀 攀氀攀洀攀渀琀 眀栀攀渀 琀栀攀 渀最刀攀焀甀椀爀攀搀 攀砀瀀爀攀猀猀椀漀渀 攀瘀愀氀甀愀琀攀猀 琀漀 琀爀甀攀⸀ 唀猀攀 怀渀最刀攀焀甀椀爀攀搀怀 椀渀猀琀攀愀搀 漀昀ഀഀ
 *    `required` when you want to data-bind to the `required` attribute.਍ ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀㴀紀 渀最䴀椀渀氀攀渀最琀栀 匀攀琀猀 怀洀椀渀氀攀渀最琀栀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 椀猀 猀栀漀爀琀攀爀 琀栀愀渀ഀഀ
 *    minlength.਍ ⨀ 䀀瀀愀爀愀洀 笀渀甀洀戀攀爀㴀紀 渀最䴀愀砀氀攀渀最琀栀 匀攀琀猀 怀洀愀砀氀攀渀最琀栀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 椀猀 氀漀渀最攀爀 琀栀愀渀ഀഀ
 *    maxlength.਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 渀最倀愀琀琀攀爀渀 匀攀琀猀 怀瀀愀琀琀攀爀渀怀 瘀愀氀椀搀愀琀椀漀渀 攀爀爀漀爀 欀攀礀 椀昀 琀栀攀 瘀愀氀甀攀 搀漀攀猀 渀漀琀 洀愀琀挀栀 琀栀攀ഀഀ
 *    RegExp pattern expression. Expected value is `/regexp/` for inline patterns or `regexp` for਍ ⨀    瀀愀琀琀攀爀渀猀 搀攀昀椀渀攀搀 愀猀 猀挀漀瀀攀 攀砀瀀爀攀猀猀椀漀渀猀⸀ഀഀ
 * @param {string=} ngChange Angular expression to be executed when input changes due to user਍ ⨀    椀渀琀攀爀愀挀琀椀漀渀 眀椀琀栀 琀栀攀 椀渀瀀甀琀 攀氀攀洀攀渀琀⸀ഀഀ
 */਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀椀渀瀀甀琀ഀഀ
 * @restrict E਍ ⨀ഀഀ
 * @description਍ ⨀ 䠀吀䴀䰀 椀渀瀀甀琀 攀氀攀洀攀渀琀 挀漀渀琀爀漀氀 眀椀琀栀 愀渀最甀氀愀爀 搀愀琀愀ⴀ戀椀渀搀椀渀最⸀ 䤀渀瀀甀琀 挀漀渀琀爀漀氀 昀漀氀氀漀眀猀 䠀吀䴀䰀㔀 椀渀瀀甀琀 琀礀瀀攀猀ഀഀ
 * and polyfills the HTML5 validation behavior for older browsers.਍ ⨀ഀഀ
 * @param {string} ngModel Assignable angular expression to data-bind to.਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 渀愀洀攀 倀爀漀瀀攀爀琀礀 渀愀洀攀 漀昀 琀栀攀 昀漀爀洀 甀渀搀攀爀 眀栀椀挀栀 琀栀攀 挀漀渀琀爀漀氀 椀猀 瀀甀戀氀椀猀栀攀搀⸀ഀഀ
 * @param {string=} required Sets `required` validation error key if the value is not entered.਍ ⨀ 䀀瀀愀爀愀洀 笀戀漀漀氀攀愀渀㴀紀 渀最刀攀焀甀椀爀攀搀 匀攀琀猀 怀爀攀焀甀椀爀攀搀怀 愀琀琀爀椀戀甀琀攀 椀昀 猀攀琀 琀漀 琀爀甀攀ഀഀ
 * @param {number=} ngMinlength Sets `minlength` validation error key if the value is shorter than਍ ⨀    洀椀渀氀攀渀最琀栀⸀ഀഀ
 * @param {number=} ngMaxlength Sets `maxlength` validation error key if the value is longer than਍ ⨀    洀愀砀氀攀渀最琀栀⸀ഀഀ
 * @param {string=} ngPattern Sets `pattern` validation error key if the value does not match the਍ ⨀    刀攀最䔀砀瀀 瀀愀琀琀攀爀渀 攀砀瀀爀攀猀猀椀漀渀⸀ 䔀砀瀀攀挀琀攀搀 瘀愀氀甀攀 椀猀 怀⼀爀攀最攀砀瀀⼀怀 昀漀爀 椀渀氀椀渀攀 瀀愀琀琀攀爀渀猀 漀爀 怀爀攀最攀砀瀀怀 昀漀爀ഀഀ
 *    patterns defined as scope expressions.਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 渀最䌀栀愀渀最攀 䄀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 琀漀 戀攀 攀砀攀挀甀琀攀搀 眀栀攀渀 椀渀瀀甀琀 挀栀愀渀最攀猀 搀甀攀 琀漀 甀猀攀爀ഀഀ
 *    interaction with the input element.਍ ⨀ഀഀ
 * @example਍    㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
      <doc:source>਍       㰀猀挀爀椀瀀琀㸀ഀഀ
         function Ctrl($scope) {਍           ␀猀挀漀瀀攀⸀甀猀攀爀 㴀 笀渀愀洀攀㨀 ✀最甀攀猀琀✀Ⰰ 氀愀猀琀㨀 ✀瘀椀猀椀琀漀爀✀紀㬀ഀഀ
         }਍       㰀⼀猀挀爀椀瀀琀㸀ഀഀ
       <div ng-controller="Ctrl">਍         㰀昀漀爀洀 渀愀洀攀㴀∀洀礀䘀漀爀洀∀㸀ഀഀ
           User name: <input type="text" name="userName" ng-model="user.name" required>਍           㰀猀瀀愀渀 挀氀愀猀猀㴀∀攀爀爀漀爀∀ 渀最ⴀ猀栀漀眀㴀∀洀礀䘀漀爀洀⸀甀猀攀爀一愀洀攀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀∀㸀ഀഀ
             Required!</span><br>਍           䰀愀猀琀 渀愀洀攀㨀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀琀攀砀琀∀ 渀愀洀攀㴀∀氀愀猀琀一愀洀攀∀ 渀最ⴀ洀漀搀攀氀㴀∀甀猀攀爀⸀氀愀猀琀∀ഀഀ
             ng-minlength="3" ng-maxlength="10">਍           㰀猀瀀愀渀 挀氀愀猀猀㴀∀攀爀爀漀爀∀ 渀最ⴀ猀栀漀眀㴀∀洀礀䘀漀爀洀⸀氀愀猀琀一愀洀攀⸀␀攀爀爀漀爀⸀洀椀渀氀攀渀最琀栀∀㸀ഀഀ
             Too short!</span>਍           㰀猀瀀愀渀 挀氀愀猀猀㴀∀攀爀爀漀爀∀ 渀最ⴀ猀栀漀眀㴀∀洀礀䘀漀爀洀⸀氀愀猀琀一愀洀攀⸀␀攀爀爀漀爀⸀洀愀砀氀攀渀最琀栀∀㸀ഀഀ
             Too long!</span><br>਍         㰀⼀昀漀爀洀㸀ഀഀ
         <hr>਍         㰀琀琀㸀甀猀攀爀 㴀 笀笀甀猀攀爀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
         <tt>myForm.userName.$valid = {{myForm.userName.$valid}}</tt><br>਍         㰀琀琀㸀洀礀䘀漀爀洀⸀甀猀攀爀一愀洀攀⸀␀攀爀爀漀爀 㴀 笀笀洀礀䘀漀爀洀⸀甀猀攀爀一愀洀攀⸀␀攀爀爀漀爀紀紀㰀⼀琀琀㸀㰀戀爀㸀ഀഀ
         <tt>myForm.lastName.$valid = {{myForm.lastName.$valid}}</tt><br>਍         㰀琀琀㸀洀礀䘀漀爀洀⸀氀愀猀琀一愀洀攀⸀␀攀爀爀漀爀 㴀 笀笀洀礀䘀漀爀洀⸀氀愀猀琀一愀洀攀⸀␀攀爀爀漀爀紀紀㰀⼀琀琀㸀㰀戀爀㸀ഀഀ
         <tt>myForm.$valid = {{myForm.$valid}}</tt><br>਍         㰀琀琀㸀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀 㴀 笀笀℀℀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀紀紀㰀⼀琀琀㸀㰀戀爀㸀ഀഀ
         <tt>myForm.$error.minlength = {{!!myForm.$error.minlength}}</tt><br>਍         㰀琀琀㸀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀洀愀砀氀攀渀最琀栀 㴀 笀笀℀℀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀洀愀砀氀攀渀最琀栀紀紀㰀⼀琀琀㸀㰀戀爀㸀ഀഀ
       </div>਍      㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      <doc:scenario>਍        椀琀⠀✀猀栀漀甀氀搀 椀渀椀琀椀愀氀椀稀攀 琀漀 洀漀搀攀氀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          expect(binding('user')).toEqual('{"name":"guest","last":"visitor"}');਍          攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀甀猀攀爀一愀洀攀⸀␀瘀愀氀椀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀琀爀甀攀✀⤀㬀ഀഀ
          expect(binding('myForm.$valid')).toEqual('true');਍        紀⤀㬀ഀഀ
਍        椀琀⠀✀猀栀漀甀氀搀 戀攀 椀渀瘀愀氀椀搀 椀昀 攀洀瀀琀礀 眀栀攀渀 爀攀焀甀椀爀攀搀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          input('user.name').enter('');਍          攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀甀猀攀爀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀笀∀氀愀猀琀∀㨀∀瘀椀猀椀琀漀爀∀紀✀⤀㬀ഀഀ
          expect(binding('myForm.userName.$valid')).toEqual('false');਍          攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀␀瘀愀氀椀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀昀愀氀猀攀✀⤀㬀ഀഀ
        });਍ഀഀ
        it('should be valid if empty when min length is set', function() {਍          椀渀瀀甀琀⠀✀甀猀攀爀⸀氀愀猀琀✀⤀⸀攀渀琀攀爀⠀✀✀⤀㬀ഀഀ
          expect(binding('user')).toEqual('{"name":"guest","last":""}');਍          攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀氀愀猀琀一愀洀攀⸀␀瘀愀氀椀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀琀爀甀攀✀⤀㬀ഀഀ
          expect(binding('myForm.$valid')).toEqual('true');਍        紀⤀㬀ഀഀ
਍        椀琀⠀✀猀栀漀甀氀搀 戀攀 椀渀瘀愀氀椀搀 椀昀 氀攀猀猀 琀栀愀渀 爀攀焀甀椀爀攀搀 洀椀渀 氀攀渀最琀栀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          input('user.last').enter('xx');਍          攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀甀猀攀爀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀笀∀渀愀洀攀∀㨀∀最甀攀猀琀∀紀✀⤀㬀ഀഀ
          expect(binding('myForm.lastName.$valid')).toEqual('false');਍          攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀氀愀猀琀一愀洀攀⸀␀攀爀爀漀爀✀⤀⤀⸀琀漀䴀愀琀挀栀⠀⼀洀椀渀氀攀渀最琀栀⼀⤀㬀ഀഀ
          expect(binding('myForm.$valid')).toEqual('false');਍        紀⤀㬀ഀഀ
਍        椀琀⠀✀猀栀漀甀氀搀 戀攀 椀渀瘀愀氀椀搀 椀昀 氀漀渀最攀爀 琀栀愀渀 洀愀砀 氀攀渀最琀栀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          input('user.last').enter('some ridiculously long name');਍          攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀甀猀攀爀✀⤀⤀ഀഀ
            .toEqual('{"name":"guest"}');਍          攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀氀愀猀琀一愀洀攀⸀␀瘀愀氀椀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀昀愀氀猀攀✀⤀㬀ഀഀ
          expect(binding('myForm.lastName.$error')).toMatch(/maxlength/);਍          攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀␀瘀愀氀椀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀昀愀氀猀攀✀⤀㬀ഀഀ
        });਍      㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
    </doc:example>਍ ⨀⼀ഀഀ
var inputDirective = ['$browser', '$sniffer', function($browser, $sniffer) {਍  爀攀琀甀爀渀 笀ഀഀ
    restrict: 'E',਍    爀攀焀甀椀爀攀㨀 ✀㼀渀最䴀漀搀攀氀✀Ⰰഀഀ
    link: function(scope, element, attr, ctrl) {਍      椀昀 ⠀挀琀爀氀⤀ 笀ഀഀ
        (inputType[lowercase(attr.type)] || inputType.text)(scope, element, attr, ctrl, $sniffer,਍                                                            ␀戀爀漀眀猀攀爀⤀㬀ഀഀ
      }਍    紀ഀഀ
  };਍紀崀㬀ഀഀ
਍瘀愀爀 嘀䄀䰀䤀䐀开䌀䰀䄀匀匀 㴀 ✀渀最ⴀ瘀愀氀椀搀✀Ⰰഀഀ
    INVALID_CLASS = 'ng-invalid',਍    倀刀䤀匀吀䤀一䔀开䌀䰀䄀匀匀 㴀 ✀渀最ⴀ瀀爀椀猀琀椀渀攀✀Ⰰഀഀ
    DIRTY_CLASS = 'ng-dirty';਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 漀戀樀攀挀琀ഀഀ
 * @name ng.directive:ngModel.NgModelController਍ ⨀ഀഀ
 * @property {string} $viewValue Actual string value in the view.਍ ⨀ 䀀瀀爀漀瀀攀爀琀礀 笀⨀紀 ␀洀漀搀攀氀嘀愀氀甀攀 吀栀攀 瘀愀氀甀攀 椀渀 琀栀攀 洀漀搀攀氀Ⰰ 琀栀愀琀 琀栀攀 挀漀渀琀爀漀氀 椀猀 戀漀甀渀搀 琀漀⸀ഀഀ
 * @property {Array.<Function>} $parsers Array of functions to execute, as a pipeline, whenever਍       琀栀攀 挀漀渀琀爀漀氀 爀攀愀搀猀 瘀愀氀甀攀 昀爀漀洀 琀栀攀 䐀伀䴀⸀  䔀愀挀栀 昀甀渀挀琀椀漀渀 椀猀 挀愀氀氀攀搀Ⰰ 椀渀 琀甀爀渀Ⰰ 瀀愀猀猀椀渀最 琀栀攀 瘀愀氀甀攀ഀഀ
       through to the next. Used to sanitize / convert the value as well as validation.਍       䘀漀爀 瘀愀氀椀搀愀琀椀漀渀Ⰰ 琀栀攀 瀀愀爀猀攀爀猀 猀栀漀甀氀搀 甀瀀搀愀琀攀 琀栀攀 瘀愀氀椀搀椀琀礀 猀琀愀琀攀 甀猀椀渀最ഀഀ
       {@link ng.directive:ngModel.NgModelController#methods_$setValidity $setValidity()},਍       愀渀搀 爀攀琀甀爀渀 怀甀渀搀攀昀椀渀攀搀怀 昀漀爀 椀渀瘀愀氀椀搀 瘀愀氀甀攀猀⸀ഀഀ
਍ ⨀ഀഀ
 * @property {Array.<Function>} $formatters Array of functions to execute, as a pipeline, whenever਍       琀栀攀 洀漀搀攀氀 瘀愀氀甀攀 挀栀愀渀最攀猀⸀ 䔀愀挀栀 昀甀渀挀琀椀漀渀 椀猀 挀愀氀氀攀搀Ⰰ 椀渀 琀甀爀渀Ⰰ 瀀愀猀猀椀渀最 琀栀攀 瘀愀氀甀攀 琀栀爀漀甀最栀 琀漀 琀栀攀ഀഀ
       next. Used to format / convert values for display in the control and validation.਍ ⨀      㰀瀀爀攀㸀ഀഀ
 *      function formatter(value) {਍ ⨀        椀昀 ⠀瘀愀氀甀攀⤀ 笀ഀഀ
 *          return value.toUpperCase();਍ ⨀        紀ഀഀ
 *      }਍ ⨀      渀最䴀漀搀攀氀⸀␀昀漀爀洀愀琀琀攀爀猀⸀瀀甀猀栀⠀昀漀爀洀愀琀琀攀爀⤀㬀ഀഀ
 *      </pre>਍ ⨀ഀഀ
 * @property {Array.<Function>} $viewChangeListeners Array of functions to execute whenever the਍ ⨀     瘀椀攀眀 瘀愀氀甀攀 栀愀猀 挀栀愀渀最攀搀⸀ 䤀琀 椀猀 挀愀氀氀攀搀 眀椀琀栀 渀漀 愀爀最甀洀攀渀琀猀Ⰰ 愀渀搀 椀琀猀 爀攀琀甀爀渀 瘀愀氀甀攀 椀猀 椀最渀漀爀攀搀⸀ഀഀ
 *     This can be used in place of additional $watches against the model value.਍ ⨀ഀഀ
 * @property {Object} $error An object hash with all errors as keys.਍ ⨀ഀഀ
 * @property {boolean} $pristine True if user has not interacted with the control yet.਍ ⨀ 䀀瀀爀漀瀀攀爀琀礀 笀戀漀漀氀攀愀渀紀 ␀搀椀爀琀礀 吀爀甀攀 椀昀 甀猀攀爀 栀愀猀 愀氀爀攀愀搀礀 椀渀琀攀爀愀挀琀攀搀 眀椀琀栀 琀栀攀 挀漀渀琀爀漀氀⸀ഀഀ
 * @property {boolean} $valid True if there is no error.਍ ⨀ 䀀瀀爀漀瀀攀爀琀礀 笀戀漀漀氀攀愀渀紀 ␀椀渀瘀愀氀椀搀 吀爀甀攀 椀昀 愀琀 氀攀愀猀琀 漀渀攀 攀爀爀漀爀 漀渀 琀栀攀 挀漀渀琀爀漀氀⸀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 *਍ ⨀ 怀一最䴀漀搀攀氀䌀漀渀琀爀漀氀氀攀爀怀 瀀爀漀瘀椀搀攀猀 䄀倀䤀 昀漀爀 琀栀攀 怀渀最ⴀ洀漀搀攀氀怀 搀椀爀攀挀琀椀瘀攀⸀ 吀栀攀 挀漀渀琀爀漀氀氀攀爀 挀漀渀琀愀椀渀猀ഀഀ
 * services for data-binding, validation, CSS updates, and value formatting and parsing. It਍ ⨀ 瀀甀爀瀀漀猀攀昀甀氀氀礀 搀漀攀猀 渀漀琀 挀漀渀琀愀椀渀 愀渀礀 氀漀最椀挀 眀栀椀挀栀 搀攀愀氀猀 眀椀琀栀 䐀伀䴀 爀攀渀搀攀爀椀渀最 漀爀 氀椀猀琀攀渀椀渀最 琀漀ഀഀ
 * DOM events. Such DOM related logic should be provided by other directives which make use of਍ ⨀ 怀一最䴀漀搀攀氀䌀漀渀琀爀漀氀氀攀爀怀 昀漀爀 搀愀琀愀ⴀ戀椀渀搀椀渀最⸀ഀഀ
 *਍ ⨀ ⌀⌀ 䌀甀猀琀漀洀 䌀漀渀琀爀漀氀 䔀砀愀洀瀀氀攀ഀഀ
 * This example shows how to use `NgModelController` with a custom control to achieve਍ ⨀ 搀愀琀愀ⴀ戀椀渀搀椀渀最⸀ 一漀琀椀挀攀 栀漀眀 搀椀昀昀攀爀攀渀琀 搀椀爀攀挀琀椀瘀攀猀 ⠀怀挀漀渀琀攀渀琀攀搀椀琀愀戀氀攀怀Ⰰ 怀渀最ⴀ洀漀搀攀氀怀Ⰰ 愀渀搀 怀爀攀焀甀椀爀攀搀怀⤀ഀഀ
 * collaborate together to achieve the desired result.਍ ⨀ഀഀ
 * Note that `contenteditable` is an HTML5 attribute, which tells the browser to let the element਍ ⨀ 挀漀渀琀攀渀琀猀 戀攀 攀搀椀琀攀搀 椀渀 瀀氀愀挀攀 戀礀 琀栀攀 甀猀攀爀⸀  吀栀椀猀 眀椀氀氀 渀漀琀 眀漀爀欀 漀渀 漀氀搀攀爀 戀爀漀眀猀攀爀猀⸀ഀഀ
 *਍ ⨀ 㰀攀砀愀洀瀀氀攀 洀漀搀甀氀攀㴀∀挀甀猀琀漀洀䌀漀渀琀爀漀氀∀㸀ഀഀ
    <file name="style.css">਍      嬀挀漀渀琀攀渀琀攀搀椀琀愀戀氀攀崀 笀ഀഀ
        border: 1px solid black;਍        戀愀挀欀最爀漀甀渀搀ⴀ挀漀氀漀爀㨀 眀栀椀琀攀㬀ഀഀ
        min-height: 20px;਍      紀ഀഀ
਍      ⸀渀最ⴀ椀渀瘀愀氀椀搀 笀ഀഀ
        border: 1px solid red;਍      紀ഀഀ
਍    㰀⼀昀椀氀攀㸀ഀഀ
    <file name="script.js">਍      愀渀最甀氀愀爀⸀洀漀搀甀氀攀⠀✀挀甀猀琀漀洀䌀漀渀琀爀漀氀✀Ⰰ 嬀崀⤀⸀ഀഀ
        directive('contenteditable', function() {਍          爀攀琀甀爀渀 笀ഀഀ
            restrict: 'A', // only activate on element attribute਍            爀攀焀甀椀爀攀㨀 ✀㼀渀最䴀漀搀攀氀✀Ⰰ ⼀⼀ 最攀琀 愀 栀漀氀搀 漀昀 一最䴀漀搀攀氀䌀漀渀琀爀漀氀氀攀爀ഀഀ
            link: function(scope, element, attrs, ngModel) {਍              椀昀⠀℀渀最䴀漀搀攀氀⤀ 爀攀琀甀爀渀㬀 ⼀⼀ 搀漀 渀漀琀栀椀渀最 椀昀 渀漀 渀最ⴀ洀漀搀攀氀ഀഀ
਍              ⼀⼀ 匀瀀攀挀椀昀礀 栀漀眀 唀䤀 猀栀漀甀氀搀 戀攀 甀瀀搀愀琀攀搀ഀഀ
              ngModel.$render = function() {਍                攀氀攀洀攀渀琀⸀栀琀洀氀⠀渀最䴀漀搀攀氀⸀␀瘀椀攀眀嘀愀氀甀攀 簀簀 ✀✀⤀㬀ഀഀ
              };਍ഀഀ
              // Listen for change events to enable binding਍              攀氀攀洀攀渀琀⸀漀渀⠀✀戀氀甀爀 欀攀礀甀瀀 挀栀愀渀最攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
                scope.$apply(read);਍              紀⤀㬀ഀഀ
              read(); // initialize਍ഀഀ
              // Write data to the model਍              昀甀渀挀琀椀漀渀 爀攀愀搀⠀⤀ 笀ഀഀ
                var html = element.html();਍                ⼀⼀ 圀栀攀渀 眀攀 挀氀攀愀爀 琀栀攀 挀漀渀琀攀渀琀 攀搀椀琀愀戀氀攀 琀栀攀 戀爀漀眀猀攀爀 氀攀愀瘀攀猀 愀 㰀戀爀㸀 戀攀栀椀渀搀ഀഀ
                // If strip-br attribute is provided then we strip this out਍                椀昀⠀ 愀琀琀爀猀⸀猀琀爀椀瀀䈀爀 ☀☀ 栀琀洀氀 㴀㴀 ✀㰀戀爀㸀✀ ⤀ 笀ഀഀ
                  html = '';਍                紀ഀഀ
                ngModel.$setViewValue(html);਍              紀ഀഀ
            }਍          紀㬀ഀഀ
        });਍    㰀⼀昀椀氀攀㸀ഀഀ
    <file name="index.html">਍      㰀昀漀爀洀 渀愀洀攀㴀∀洀礀䘀漀爀洀∀㸀ഀഀ
       <div contenteditable਍            渀愀洀攀㴀∀洀礀圀椀搀最攀琀∀ 渀最ⴀ洀漀搀攀氀㴀∀甀猀攀爀䌀漀渀琀攀渀琀∀ഀഀ
            strip-br="true"਍            爀攀焀甀椀爀攀搀㸀䌀栀愀渀最攀 洀攀℀㰀⼀搀椀瘀㸀ഀഀ
        <span ng-show="myForm.myWidget.$error.required">Required!</span>਍       㰀栀爀㸀ഀഀ
       <textarea ng-model="userContent"></textarea>਍      㰀⼀昀漀爀洀㸀ഀഀ
    </file>਍    㰀昀椀氀攀 渀愀洀攀㴀∀猀挀攀渀愀爀椀漀⸀樀猀∀㸀ഀഀ
      it('should data-bind and become invalid', function() {਍        瘀愀爀 挀漀渀琀攀渀琀䔀搀椀琀愀戀氀攀 㴀 攀氀攀洀攀渀琀⠀✀嬀挀漀渀琀攀渀琀攀搀椀琀愀戀氀攀崀✀⤀㬀ഀഀ
਍        攀砀瀀攀挀琀⠀挀漀渀琀攀渀琀䔀搀椀琀愀戀氀攀⸀琀攀砀琀⠀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀䌀栀愀渀最攀 洀攀℀✀⤀㬀ഀഀ
        input('userContent').enter('');਍        攀砀瀀攀挀琀⠀挀漀渀琀攀渀琀䔀搀椀琀愀戀氀攀⸀琀攀砀琀⠀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀✀⤀㬀ഀഀ
        expect(contentEditable.prop('className')).toMatch(/ng-invalid-required/);਍      紀⤀㬀ഀഀ
    </file>਍ ⨀ 㰀⼀攀砀愀洀瀀氀攀㸀ഀഀ
 *਍ ⨀ഀഀ
 */਍瘀愀爀 一最䴀漀搀攀氀䌀漀渀琀爀漀氀氀攀爀 㴀 嬀✀␀猀挀漀瀀攀✀Ⰰ ✀␀攀砀挀攀瀀琀椀漀渀䠀愀渀搀氀攀爀✀Ⰰ ✀␀愀琀琀爀猀✀Ⰰ ✀␀攀氀攀洀攀渀琀✀Ⰰ ✀␀瀀愀爀猀攀✀Ⰰഀഀ
    function($scope, $exceptionHandler, $attr, $element, $parse) {਍  琀栀椀猀⸀␀瘀椀攀眀嘀愀氀甀攀 㴀 一甀洀戀攀爀⸀一愀一㬀ഀഀ
  this.$modelValue = Number.NaN;਍  琀栀椀猀⸀␀瀀愀爀猀攀爀猀 㴀 嬀崀㬀ഀഀ
  this.$formatters = [];਍  琀栀椀猀⸀␀瘀椀攀眀䌀栀愀渀最攀䰀椀猀琀攀渀攀爀猀 㴀 嬀崀㬀ഀഀ
  this.$pristine = true;਍  琀栀椀猀⸀␀搀椀爀琀礀 㴀 昀愀氀猀攀㬀ഀഀ
  this.$valid = true;਍  琀栀椀猀⸀␀椀渀瘀愀氀椀搀 㴀 昀愀氀猀攀㬀ഀഀ
  this.$name = $attr.name;਍ഀഀ
  var ngModelGet = $parse($attr.ngModel),਍      渀最䴀漀搀攀氀匀攀琀 㴀 渀最䴀漀搀攀氀䜀攀琀⸀愀猀猀椀最渀㬀ഀഀ
਍  椀昀 ⠀℀渀最䴀漀搀攀氀匀攀琀⤀ 笀ഀഀ
    throw minErr('ngModel')('nonassign', "Expression '{0}' is non-assignable. Element: {1}",਍        ␀愀琀琀爀⸀渀最䴀漀搀攀氀Ⰰ 猀琀愀爀琀椀渀最吀愀最⠀␀攀氀攀洀攀渀琀⤀⤀㬀ഀഀ
  }਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
   * @name ng.directive:ngModel.NgModelController#$render਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䴀漀搀攀氀⸀一最䴀漀搀攀氀䌀漀渀琀爀漀氀氀攀爀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Called when the view needs to be updated. It is expected that the user of the ng-model਍   ⨀ 搀椀爀攀挀琀椀瘀攀 眀椀氀氀 椀洀瀀氀攀洀攀渀琀 琀栀椀猀 洀攀琀栀漀搀⸀ഀഀ
   */਍  琀栀椀猀⸀␀爀攀渀搀攀爀 㴀 渀漀漀瀀㬀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc function਍   ⨀ 䀀渀愀洀攀 笀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䴀漀搀攀氀⸀一最䴀漀搀攀氀䌀漀渀琀爀漀氀氀攀爀⌀␀椀猀䔀洀瀀琀礀ഀഀ
   * @methodOf ng.directive:ngModel.NgModelController਍   ⨀ഀഀ
   * @description਍   ⨀ 吀栀椀猀 椀猀 挀愀氀氀攀搀 眀栀攀渀 眀攀 渀攀攀搀 琀漀 搀攀琀攀爀洀椀渀攀 椀昀 琀栀攀 瘀愀氀甀攀 漀昀 琀栀攀 椀渀瀀甀琀 椀猀 攀洀瀀琀礀⸀ഀഀ
   *਍   ⨀ 䘀漀爀 椀渀猀琀愀渀挀攀Ⰰ 琀栀攀 爀攀焀甀椀爀攀搀 搀椀爀攀挀琀椀瘀攀 搀漀攀猀 琀栀椀猀 琀漀 眀漀爀欀 漀甀琀 椀昀 琀栀攀 椀渀瀀甀琀 栀愀猀 搀愀琀愀 漀爀 渀漀琀⸀ഀഀ
   * The default `$isEmpty` function checks whether the value is `undefined`, `''`, `null` or `NaN`.਍   ⨀ഀഀ
   * You can override this for input directives whose concept of being empty is different to the਍   ⨀ 搀攀昀愀甀氀琀⸀ 吀栀攀 怀挀栀攀挀欀戀漀砀䤀渀瀀甀琀吀礀瀀攀怀 搀椀爀攀挀琀椀瘀攀 搀漀攀猀 琀栀椀猀 戀攀挀愀甀猀攀 椀渀 椀琀猀 挀愀猀攀 愀 瘀愀氀甀攀 漀昀 怀昀愀氀猀攀怀ഀഀ
   * implies empty.਍   ⨀⼀ഀഀ
  this.$isEmpty = function(value) {਍    爀攀琀甀爀渀 椀猀唀渀搀攀昀椀渀攀搀⠀瘀愀氀甀攀⤀ 簀簀 瘀愀氀甀攀 㴀㴀㴀 ✀✀ 簀簀 瘀愀氀甀攀 㴀㴀㴀 渀甀氀氀 簀簀 瘀愀氀甀攀 ℀㴀㴀 瘀愀氀甀攀㬀ഀഀ
  };਍ഀഀ
  var parentForm = $element.inheritedData('$formController') || nullFormCtrl,਍      椀渀瘀愀氀椀搀䌀漀甀渀琀 㴀 　Ⰰ ⼀⼀ 甀猀攀搀 琀漀 攀愀猀椀氀礀 搀攀琀攀爀洀椀渀攀 椀昀 眀攀 愀爀攀 瘀愀氀椀搀ഀഀ
      $error = this.$error = {}; // keep invalid keys here਍ഀഀ
਍  ⼀⼀ 匀攀琀甀瀀 椀渀椀琀椀愀氀 猀琀愀琀攀 漀昀 琀栀攀 挀漀渀琀爀漀氀ഀഀ
  $element.addClass(PRISTINE_CLASS);਍  琀漀最最氀攀嘀愀氀椀搀䌀猀猀⠀琀爀甀攀⤀㬀ഀഀ
਍  ⼀⼀ 挀漀渀瘀攀渀椀攀渀挀攀 洀攀琀栀漀搀 昀漀爀 攀愀猀礀 琀漀最最氀椀渀最 漀昀 挀氀愀猀猀攀猀ഀഀ
  function toggleValidCss(isValid, validationErrorKey) {਍    瘀愀氀椀搀愀琀椀漀渀䔀爀爀漀爀䬀攀礀 㴀 瘀愀氀椀搀愀琀椀漀渀䔀爀爀漀爀䬀攀礀 㼀 ✀ⴀ✀ ⬀ 猀渀愀欀攀开挀愀猀攀⠀瘀愀氀椀搀愀琀椀漀渀䔀爀爀漀爀䬀攀礀Ⰰ ✀ⴀ✀⤀ 㨀 ✀✀㬀ഀഀ
    $element.਍      爀攀洀漀瘀攀䌀氀愀猀猀⠀⠀椀猀嘀愀氀椀搀 㼀 䤀一嘀䄀䰀䤀䐀开䌀䰀䄀匀匀 㨀 嘀䄀䰀䤀䐀开䌀䰀䄀匀匀⤀ ⬀ 瘀愀氀椀搀愀琀椀漀渀䔀爀爀漀爀䬀攀礀⤀⸀ഀഀ
      addClass((isValid ? VALID_CLASS : INVALID_CLASS) + validationErrorKey);਍  紀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc function਍   ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䴀漀搀攀氀⸀一最䴀漀搀攀氀䌀漀渀琀爀漀氀氀攀爀⌀␀猀攀琀嘀愀氀椀搀椀琀礀ഀഀ
   * @methodOf ng.directive:ngModel.NgModelController਍   ⨀ഀഀ
   * @description਍   ⨀ 䌀栀愀渀最攀 琀栀攀 瘀愀氀椀搀椀琀礀 猀琀愀琀攀Ⰰ 愀渀搀 渀漀琀椀昀椀攀猀 琀栀攀 昀漀爀洀 眀栀攀渀 琀栀攀 挀漀渀琀爀漀氀 挀栀愀渀最攀猀 瘀愀氀椀搀椀琀礀⸀ ⠀椀⸀攀⸀ 椀琀ഀഀ
   * does not notify form if given validator is already marked as invalid).਍   ⨀ഀഀ
   * This method should be called by validators - i.e. the parser or formatter functions.਍   ⨀ഀഀ
   * @param {string} validationErrorKey Name of the validator. the `validationErrorKey` will assign਍   ⨀        琀漀 怀␀攀爀爀漀爀嬀瘀愀氀椀搀愀琀椀漀渀䔀爀爀漀爀䬀攀礀崀㴀椀猀嘀愀氀椀搀怀 猀漀 琀栀愀琀 椀琀 椀猀 愀瘀愀椀氀愀戀氀攀 昀漀爀 搀愀琀愀ⴀ戀椀渀搀椀渀最⸀ഀഀ
   *        The `validationErrorKey` should be in camelCase and will get converted into dash-case਍   ⨀        昀漀爀 挀氀愀猀猀 渀愀洀攀⸀ 䔀砀愀洀瀀氀攀㨀 怀洀礀䔀爀爀漀爀怀 眀椀氀氀 爀攀猀甀氀琀 椀渀 怀渀最ⴀ瘀愀氀椀搀ⴀ洀礀ⴀ攀爀爀漀爀怀 愀渀搀 怀渀最ⴀ椀渀瘀愀氀椀搀ⴀ洀礀ⴀ攀爀爀漀爀怀ഀഀ
   *        class and can be bound to as  `{{someForm.someControl.$error.myError}}` .਍   ⨀ 䀀瀀愀爀愀洀 笀戀漀漀氀攀愀渀紀 椀猀嘀愀氀椀搀 圀栀攀琀栀攀爀 琀栀攀 挀甀爀爀攀渀琀 猀琀愀琀攀 椀猀 瘀愀氀椀搀 ⠀琀爀甀攀⤀ 漀爀 椀渀瘀愀氀椀搀 ⠀昀愀氀猀攀⤀⸀ഀഀ
   */਍  琀栀椀猀⸀␀猀攀琀嘀愀氀椀搀椀琀礀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀椀搀愀琀椀漀渀䔀爀爀漀爀䬀攀礀Ⰰ 椀猀嘀愀氀椀搀⤀ 笀ഀഀ
    // Purposeful use of ! here to cast isValid to boolean in case it is undefined਍    ⼀⼀ 樀猀栀椀渀琀 ⴀ圀　㄀㠀ഀഀ
    if ($error[validationErrorKey] === !isValid) return;਍    ⼀⼀ 樀猀栀椀渀琀 ⬀圀　㄀㠀ഀഀ
਍    椀昀 ⠀椀猀嘀愀氀椀搀⤀ 笀ഀഀ
      if ($error[validationErrorKey]) invalidCount--;਍      椀昀 ⠀℀椀渀瘀愀氀椀搀䌀漀甀渀琀⤀ 笀ഀഀ
        toggleValidCss(true);਍        琀栀椀猀⸀␀瘀愀氀椀搀 㴀 琀爀甀攀㬀ഀഀ
        this.$invalid = false;਍      紀ഀഀ
    } else {਍      琀漀最最氀攀嘀愀氀椀搀䌀猀猀⠀昀愀氀猀攀⤀㬀ഀഀ
      this.$invalid = true;਍      琀栀椀猀⸀␀瘀愀氀椀搀 㴀 昀愀氀猀攀㬀ഀഀ
      invalidCount++;਍    紀ഀഀ
਍    ␀攀爀爀漀爀嬀瘀愀氀椀搀愀琀椀漀渀䔀爀爀漀爀䬀攀礀崀 㴀 ℀椀猀嘀愀氀椀搀㬀ഀഀ
    toggleValidCss(isValid, validationErrorKey);਍ഀഀ
    parentForm.$setValidity(validationErrorKey, isValid, this);਍  紀㬀ഀഀ
਍  ⼀⨀⨀ഀഀ
   * @ngdoc function਍   ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䴀漀搀攀氀⸀一最䴀漀搀攀氀䌀漀渀琀爀漀氀氀攀爀⌀␀猀攀琀倀爀椀猀琀椀渀攀ഀഀ
   * @methodOf ng.directive:ngModel.NgModelController਍   ⨀ഀഀ
   * @description਍   ⨀ 匀攀琀猀 琀栀攀 挀漀渀琀爀漀氀 琀漀 椀琀猀 瀀爀椀猀琀椀渀攀 猀琀愀琀攀⸀ഀഀ
   *਍   ⨀ 吀栀椀猀 洀攀琀栀漀搀 挀愀渀 戀攀 挀愀氀氀攀搀 琀漀 爀攀洀漀瘀攀 琀栀攀 ✀渀最ⴀ搀椀爀琀礀✀ 挀氀愀猀猀 愀渀搀 猀攀琀 琀栀攀 挀漀渀琀爀漀氀 琀漀 椀琀猀 瀀爀椀猀琀椀渀攀ഀഀ
   * state (ng-pristine class).਍   ⨀⼀ഀഀ
  this.$setPristine = function () {਍    琀栀椀猀⸀␀搀椀爀琀礀 㴀 昀愀氀猀攀㬀ഀഀ
    this.$pristine = true;਍    ␀攀氀攀洀攀渀琀⸀爀攀洀漀瘀攀䌀氀愀猀猀⠀䐀䤀刀吀夀开䌀䰀䄀匀匀⤀⸀愀搀搀䌀氀愀猀猀⠀倀刀䤀匀吀䤀一䔀开䌀䰀䄀匀匀⤀㬀ഀഀ
  };਍ഀഀ
  /**਍   ⨀ 䀀渀最搀漀挀 昀甀渀挀琀椀漀渀ഀഀ
   * @name ng.directive:ngModel.NgModelController#$setViewValue਍   ⨀ 䀀洀攀琀栀漀搀伀昀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䴀漀搀攀氀⸀一最䴀漀搀攀氀䌀漀渀琀爀漀氀氀攀爀ഀഀ
   *਍   ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
   * Update the view value.਍   ⨀ഀഀ
   * This method should be called when the view value changes, typically from within a DOM event handler.਍   ⨀ 䘀漀爀 攀砀愀洀瀀氀攀 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀椀渀瀀甀琀 椀渀瀀甀琀紀 愀渀搀ഀഀ
   * {@link ng.directive:select select} directives call it.਍   ⨀ഀഀ
   * It will update the $viewValue, then pass this value through each of the functions in `$parsers`,਍   ⨀ 眀栀椀挀栀 椀渀挀氀甀搀攀猀 愀渀礀 瘀愀氀椀搀愀琀漀爀猀⸀ 吀栀攀 瘀愀氀甀攀 琀栀愀琀 挀漀洀攀猀 漀甀琀 漀昀 琀栀椀猀 怀␀瀀愀爀猀攀爀猀怀 瀀椀瀀攀氀椀渀攀Ⰰ 戀攀 愀瀀瀀氀椀攀搀 琀漀ഀഀ
   * `$modelValue` and the **expression** specified in the `ng-model` attribute.਍   ⨀ഀഀ
   * Lastly, all the registered change listeners, in the `$viewChangeListeners` list, are called.਍   ⨀ഀഀ
   * Note that calling this function does not trigger a `$digest`.਍   ⨀ഀഀ
   * @param {string} value Value from the view.਍   ⨀⼀ഀഀ
  this.$setViewValue = function(value) {਍    琀栀椀猀⸀␀瘀椀攀眀嘀愀氀甀攀 㴀 瘀愀氀甀攀㬀ഀഀ
਍    ⼀⼀ 挀栀愀渀最攀 琀漀 搀椀爀琀礀ഀഀ
    if (this.$pristine) {਍      琀栀椀猀⸀␀搀椀爀琀礀 㴀 琀爀甀攀㬀ഀഀ
      this.$pristine = false;਍      ␀攀氀攀洀攀渀琀⸀爀攀洀漀瘀攀䌀氀愀猀猀⠀倀刀䤀匀吀䤀一䔀开䌀䰀䄀匀匀⤀⸀愀搀搀䌀氀愀猀猀⠀䐀䤀刀吀夀开䌀䰀䄀匀匀⤀㬀ഀഀ
      parentForm.$setDirty();਍    紀ഀഀ
਍    昀漀爀䔀愀挀栀⠀琀栀椀猀⸀␀瀀愀爀猀攀爀猀Ⰰ 昀甀渀挀琀椀漀渀⠀昀渀⤀ 笀ഀഀ
      value = fn(value);਍    紀⤀㬀ഀഀ
਍    椀昀 ⠀琀栀椀猀⸀␀洀漀搀攀氀嘀愀氀甀攀 ℀㴀㴀 瘀愀氀甀攀⤀ 笀ഀഀ
      this.$modelValue = value;਍      渀最䴀漀搀攀氀匀攀琀⠀␀猀挀漀瀀攀Ⰰ 瘀愀氀甀攀⤀㬀ഀഀ
      forEach(this.$viewChangeListeners, function(listener) {਍        琀爀礀 笀ഀഀ
          listener();਍        紀 挀愀琀挀栀⠀攀⤀ 笀ഀഀ
          $exceptionHandler(e);਍        紀ഀഀ
      });਍    紀ഀഀ
  };਍ഀഀ
  // model -> value਍  瘀愀爀 挀琀爀氀 㴀 琀栀椀猀㬀ഀഀ
਍  ␀猀挀漀瀀攀⸀␀眀愀琀挀栀⠀昀甀渀挀琀椀漀渀 渀最䴀漀搀攀氀圀愀琀挀栀⠀⤀ 笀ഀഀ
    var value = ngModelGet($scope);਍ഀഀ
    // if scope model value and ngModel value are out of sync਍    椀昀 ⠀挀琀爀氀⸀␀洀漀搀攀氀嘀愀氀甀攀 ℀㴀㴀 瘀愀氀甀攀⤀ 笀ഀഀ
਍      瘀愀爀 昀漀爀洀愀琀琀攀爀猀 㴀 挀琀爀氀⸀␀昀漀爀洀愀琀琀攀爀猀Ⰰഀഀ
          idx = formatters.length;਍ഀഀ
      ctrl.$modelValue = value;਍      眀栀椀氀攀⠀椀搀砀ⴀⴀ⤀ 笀ഀഀ
        value = formatters[idx](value);਍      紀ഀഀ
਍      椀昀 ⠀挀琀爀氀⸀␀瘀椀攀眀嘀愀氀甀攀 ℀㴀㴀 瘀愀氀甀攀⤀ 笀ഀഀ
        ctrl.$viewValue = value;਍        挀琀爀氀⸀␀爀攀渀搀攀爀⠀⤀㬀ഀഀ
      }਍    紀ഀഀ
਍    爀攀琀甀爀渀 瘀愀氀甀攀㬀ഀഀ
  });਍紀崀㬀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngModel਍ ⨀ഀഀ
 * @element input਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀攀 怀渀最䴀漀搀攀氀怀 搀椀爀攀挀琀椀瘀攀 戀椀渀搀猀 愀渀 怀椀渀瀀甀琀怀Ⰰ怀猀攀氀攀挀琀怀Ⰰ 怀琀攀砀琀愀爀攀愀怀 ⠀漀爀 挀甀猀琀漀洀 昀漀爀洀 挀漀渀琀爀漀氀⤀ 琀漀 愀ഀഀ
 * property on the scope using {@link ng.directive:ngModel.NgModelController NgModelController},਍ ⨀ 眀栀椀挀栀 椀猀 挀爀攀愀琀攀搀 愀渀搀 攀砀瀀漀猀攀搀 戀礀 琀栀椀猀 搀椀爀攀挀琀椀瘀攀⸀ഀഀ
 *਍ ⨀ 怀渀最䴀漀搀攀氀怀 椀猀 爀攀猀瀀漀渀猀椀戀氀攀 昀漀爀㨀ഀഀ
 *਍ ⨀ ⴀ 䈀椀渀搀椀渀最 琀栀攀 瘀椀攀眀 椀渀琀漀 琀栀攀 洀漀搀攀氀Ⰰ 眀栀椀挀栀 漀琀栀攀爀 搀椀爀攀挀琀椀瘀攀猀 猀甀挀栀 愀猀 怀椀渀瀀甀琀怀Ⰰ 怀琀攀砀琀愀爀攀愀怀 漀爀 怀猀攀氀攀挀琀怀ഀഀ
 *   require.਍ ⨀ ⴀ 倀爀漀瘀椀搀椀渀最 瘀愀氀椀搀愀琀椀漀渀 戀攀栀愀瘀椀漀爀 ⠀椀⸀攀⸀ 爀攀焀甀椀爀攀搀Ⰰ 渀甀洀戀攀爀Ⰰ 攀洀愀椀氀Ⰰ 甀爀氀⤀⸀ഀഀ
 * - Keeping the state of the control (valid/invalid, dirty/pristine, validation errors).਍ ⨀ ⴀ 匀攀琀琀椀渀最 爀攀氀愀琀攀搀 挀猀猀 挀氀愀猀猀攀猀 漀渀 琀栀攀 攀氀攀洀攀渀琀 ⠀怀渀最ⴀ瘀愀氀椀搀怀Ⰰ 怀渀最ⴀ椀渀瘀愀氀椀搀怀Ⰰ 怀渀最ⴀ搀椀爀琀礀怀Ⰰ 怀渀最ⴀ瀀爀椀猀琀椀渀攀怀⤀⸀ഀഀ
 * - Registering the control with its parent {@link ng.directive:form form}.਍ ⨀ഀഀ
 * Note: `ngModel` will try to bind to the property given by evaluating the expression on the਍ ⨀ 挀甀爀爀攀渀琀 猀挀漀瀀攀⸀ 䤀昀 琀栀攀 瀀爀漀瀀攀爀琀礀 搀漀攀猀渀✀琀 愀氀爀攀愀搀礀 攀砀椀猀琀 漀渀 琀栀椀猀 猀挀漀瀀攀Ⰰ 椀琀 眀椀氀氀 戀攀 挀爀攀愀琀攀搀ഀഀ
 * implicitly and added to the scope.਍ ⨀ഀഀ
 * For best practices on using `ngModel`, see:਍ ⨀ഀഀ
 *  - {@link https://github.com/angular/angular.js/wiki/Understanding-Scopes}਍ ⨀ഀഀ
 * For basic examples, how to use `ngModel`, see:਍ ⨀ഀഀ
 *  - {@link ng.directive:input input}਍ ⨀    ⴀ 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀椀渀瀀甀琀⸀琀攀砀琀 琀攀砀琀紀ഀഀ
 *    - {@link ng.directive:input.checkbox checkbox}਍ ⨀    ⴀ 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀椀渀瀀甀琀⸀爀愀搀椀漀 爀愀搀椀漀紀ഀഀ
 *    - {@link ng.directive:input.number number}਍ ⨀    ⴀ 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀椀渀瀀甀琀⸀攀洀愀椀氀 攀洀愀椀氀紀ഀഀ
 *    - {@link ng.directive:input.url url}਍ ⨀  ⴀ 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀猀攀氀攀挀琀 猀攀氀攀挀琀紀ഀഀ
 *  - {@link ng.directive:textarea textarea}਍ ⨀ഀഀ
 */਍瘀愀爀 渀最䴀漀搀攀氀䐀椀爀攀挀琀椀瘀攀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
  return {਍    爀攀焀甀椀爀攀㨀 嬀✀渀最䴀漀搀攀氀✀Ⰰ ✀帀㼀昀漀爀洀✀崀Ⰰഀഀ
    controller: NgModelController,਍    氀椀渀欀㨀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀Ⰰ 挀琀爀氀猀⤀ 笀ഀഀ
      // notify others, especially parent forms਍ഀഀ
      var modelCtrl = ctrls[0],਍          昀漀爀洀䌀琀爀氀 㴀 挀琀爀氀猀嬀㄀崀 簀簀 渀甀氀氀䘀漀爀洀䌀琀爀氀㬀ഀഀ
਍      昀漀爀洀䌀琀爀氀⸀␀愀搀搀䌀漀渀琀爀漀氀⠀洀漀搀攀氀䌀琀爀氀⤀㬀ഀഀ
਍      猀挀漀瀀攀⸀␀漀渀⠀✀␀搀攀猀琀爀漀礀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
        formCtrl.$removeControl(modelCtrl);਍      紀⤀㬀ഀഀ
    }਍  紀㬀ഀഀ
};਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䌀栀愀渀最攀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Evaluate given expression when user changes the input.਍ ⨀ 吀栀攀 攀砀瀀爀攀猀猀椀漀渀 椀猀 渀漀琀 攀瘀愀氀甀愀琀攀搀 眀栀攀渀 琀栀攀 瘀愀氀甀攀 挀栀愀渀最攀 椀猀 挀漀洀椀渀最 昀爀漀洀 琀栀攀 洀漀搀攀氀⸀ഀഀ
 *਍ ⨀ 一漀琀攀Ⰰ 琀栀椀猀 搀椀爀攀挀琀椀瘀攀 爀攀焀甀椀爀攀猀 怀渀最䴀漀搀攀氀怀 琀漀 戀攀 瀀爀攀猀攀渀琀⸀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 椀渀瀀甀琀ഀഀ
 * @param {expression} ngChange {@link guide/expression Expression} to evaluate upon change਍ ⨀ 椀渀 椀渀瀀甀琀 瘀愀氀甀攀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
 * <doc:example>਍ ⨀   㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
 *     <script>਍ ⨀       昀甀渀挀琀椀漀渀 䌀漀渀琀爀漀氀氀攀爀⠀␀猀挀漀瀀攀⤀ 笀ഀഀ
 *         $scope.counter = 0;਍ ⨀         ␀猀挀漀瀀攀⸀挀栀愀渀最攀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
 *           $scope.counter++;਍ ⨀         紀㬀ഀഀ
 *       }਍ ⨀     㰀⼀猀挀爀椀瀀琀㸀ഀഀ
 *     <div ng-controller="Controller">਍ ⨀       㰀椀渀瀀甀琀 琀礀瀀攀㴀∀挀栀攀挀欀戀漀砀∀ 渀最ⴀ洀漀搀攀氀㴀∀挀漀渀昀椀爀洀攀搀∀ 渀最ⴀ挀栀愀渀最攀㴀∀挀栀愀渀最攀⠀⤀∀ 椀搀㴀∀渀最ⴀ挀栀愀渀最攀ⴀ攀砀愀洀瀀氀攀㄀∀ ⼀㸀ഀഀ
 *       <input type="checkbox" ng-model="confirmed" id="ng-change-example2" />਍ ⨀       㰀氀愀戀攀氀 昀漀爀㴀∀渀最ⴀ挀栀愀渀最攀ⴀ攀砀愀洀瀀氀攀㈀∀㸀䌀漀渀昀椀爀洀攀搀㰀⼀氀愀戀攀氀㸀㰀戀爀 ⼀㸀ഀഀ
 *       debug = {{confirmed}}<br />਍ ⨀       挀漀甀渀琀攀爀 㴀 笀笀挀漀甀渀琀攀爀紀紀ഀഀ
 *     </div>਍ ⨀   㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
 *   <doc:scenario>਍ ⨀     椀琀⠀✀猀栀漀甀氀搀 攀瘀愀氀甀愀琀攀 琀栀攀 攀砀瀀爀攀猀猀椀漀渀 椀昀 挀栀愀渀最椀渀最 昀爀漀洀 瘀椀攀眀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
 *       expect(binding('counter')).toEqual('0');਍ ⨀       攀氀攀洀攀渀琀⠀✀⌀渀最ⴀ挀栀愀渀最攀ⴀ攀砀愀洀瀀氀攀㄀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
 *       expect(binding('counter')).toEqual('1');਍ ⨀       攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀挀漀渀昀椀爀洀攀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀琀爀甀攀✀⤀㬀ഀഀ
 *     });਍ ⨀ഀഀ
 *     it('should not evaluate the expression if changing from model', function() {਍ ⨀       攀氀攀洀攀渀琀⠀✀⌀渀最ⴀ挀栀愀渀最攀ⴀ攀砀愀洀瀀氀攀㈀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
 *       expect(binding('counter')).toEqual('0');਍ ⨀       攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀挀漀渀昀椀爀洀攀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀琀爀甀攀✀⤀㬀ഀഀ
 *     });਍ ⨀   㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
 * </doc:example>਍ ⨀⼀ഀഀ
var ngChangeDirective = valueFn({਍  爀攀焀甀椀爀攀㨀 ✀渀最䴀漀搀攀氀✀Ⰰഀഀ
  link: function(scope, element, attr, ctrl) {਍    挀琀爀氀⸀␀瘀椀攀眀䌀栀愀渀最攀䰀椀猀琀攀渀攀爀猀⸀瀀甀猀栀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
      scope.$eval(attr.ngChange);਍    紀⤀㬀ഀഀ
  }਍紀⤀㬀ഀഀ
਍ഀഀ
var requiredDirective = function() {਍  爀攀琀甀爀渀 笀ഀഀ
    require: '?ngModel',਍    氀椀渀欀㨀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 攀氀洀Ⰰ 愀琀琀爀Ⰰ 挀琀爀氀⤀ 笀ഀഀ
      if (!ctrl) return;਍      愀琀琀爀⸀爀攀焀甀椀爀攀搀 㴀 琀爀甀攀㬀 ⼀⼀ 昀漀爀挀攀 琀爀甀琀栀礀 椀渀 挀愀猀攀 眀攀 愀爀攀 漀渀 渀漀渀 椀渀瀀甀琀 攀氀攀洀攀渀琀ഀഀ
਍      瘀愀爀 瘀愀氀椀搀愀琀漀爀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
        if (attr.required && ctrl.$isEmpty(value)) {਍          挀琀爀氀⸀␀猀攀琀嘀愀氀椀搀椀琀礀⠀✀爀攀焀甀椀爀攀搀✀Ⰰ 昀愀氀猀攀⤀㬀ഀഀ
          return;਍        紀 攀氀猀攀 笀ഀഀ
          ctrl.$setValidity('required', true);਍          爀攀琀甀爀渀 瘀愀氀甀攀㬀ഀഀ
        }਍      紀㬀ഀഀ
਍      挀琀爀氀⸀␀昀漀爀洀愀琀琀攀爀猀⸀瀀甀猀栀⠀瘀愀氀椀搀愀琀漀爀⤀㬀ഀഀ
      ctrl.$parsers.unshift(validator);਍ഀഀ
      attr.$observe('required', function() {਍        瘀愀氀椀搀愀琀漀爀⠀挀琀爀氀⸀␀瘀椀攀眀嘀愀氀甀攀⤀㬀ഀഀ
      });਍    紀ഀഀ
  };਍紀㬀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngList਍ ⨀ഀഀ
 * @description਍ ⨀ 吀攀砀琀 椀渀瀀甀琀 琀栀愀琀 挀漀渀瘀攀爀琀猀 戀攀琀眀攀攀渀 愀 搀攀氀椀洀椀琀攀搀 猀琀爀椀渀最 愀渀搀 愀渀 愀爀爀愀礀 漀昀 猀琀爀椀渀最猀⸀ 吀栀攀 搀攀氀椀洀椀琀攀爀ഀഀ
 * can be a fixed string (by default a comma) or a regular expression.਍ ⨀ഀഀ
 * @element input਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 渀最䰀椀猀琀 漀瀀琀椀漀渀愀氀 搀攀氀椀洀椀琀攀爀 琀栀愀琀 猀栀漀甀氀搀 戀攀 甀猀攀搀 琀漀 猀瀀氀椀琀 琀栀攀 瘀愀氀甀攀⸀ 䤀昀ഀഀ
 *   specified in form `/something/` then the value will be converted into a regular expression.਍ ⨀ഀഀ
 * @example਍    㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
      <doc:source>਍       㰀猀挀爀椀瀀琀㸀ഀഀ
         function Ctrl($scope) {਍           ␀猀挀漀瀀攀⸀渀愀洀攀猀 㴀 嬀✀椀最漀爀✀Ⰰ ✀洀椀猀欀漀✀Ⰰ ✀瘀漀樀琀愀✀崀㬀ഀഀ
         }਍       㰀⼀猀挀爀椀瀀琀㸀ഀഀ
       <form name="myForm" ng-controller="Ctrl">਍         䰀椀猀琀㨀 㰀椀渀瀀甀琀 渀愀洀攀㴀∀渀愀洀攀猀䤀渀瀀甀琀∀ 渀最ⴀ洀漀搀攀氀㴀∀渀愀洀攀猀∀ 渀最ⴀ氀椀猀琀 爀攀焀甀椀爀攀搀㸀ഀഀ
         <span class="error" ng-show="myForm.namesInput.$error.required">਍           刀攀焀甀椀爀攀搀℀㰀⼀猀瀀愀渀㸀ഀഀ
         <br>਍         㰀琀琀㸀渀愀洀攀猀 㴀 笀笀渀愀洀攀猀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
         <tt>myForm.namesInput.$valid = {{myForm.namesInput.$valid}}</tt><br/>਍         㰀琀琀㸀洀礀䘀漀爀洀⸀渀愀洀攀猀䤀渀瀀甀琀⸀␀攀爀爀漀爀 㴀 笀笀洀礀䘀漀爀洀⸀渀愀洀攀猀䤀渀瀀甀琀⸀␀攀爀爀漀爀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
         <tt>myForm.$valid = {{myForm.$valid}}</tt><br/>਍         㰀琀琀㸀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀 㴀 笀笀℀℀洀礀䘀漀爀洀⸀␀攀爀爀漀爀⸀爀攀焀甀椀爀攀搀紀紀㰀⼀琀琀㸀㰀戀爀⼀㸀ഀഀ
        </form>਍      㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      <doc:scenario>਍        椀琀⠀✀猀栀漀甀氀搀 椀渀椀琀椀愀氀椀稀攀 琀漀 洀漀搀攀氀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          expect(binding('names')).toEqual('["igor","misko","vojta"]');਍          攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀䘀漀爀洀⸀渀愀洀攀猀䤀渀瀀甀琀⸀␀瘀愀氀椀搀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀琀爀甀攀✀⤀㬀ഀഀ
          expect(element('span.error').css('display')).toBe('none');਍        紀⤀㬀ഀഀ
਍        椀琀⠀✀猀栀漀甀氀搀 戀攀 椀渀瘀愀氀椀搀 椀昀 攀洀瀀琀礀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          input('names').enter('');਍          攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀渀愀洀攀猀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀✀⤀㬀ഀഀ
          expect(binding('myForm.namesInput.$valid')).toEqual('false');਍          攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀猀瀀愀渀⸀攀爀爀漀爀✀⤀⸀挀猀猀⠀✀搀椀猀瀀氀愀礀✀⤀⤀⸀渀漀琀⠀⤀⸀琀漀䈀攀⠀✀渀漀渀攀✀⤀㬀ഀഀ
        });਍      㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
    </doc:example>਍ ⨀⼀ഀഀ
var ngListDirective = function() {਍  爀攀琀甀爀渀 笀ഀഀ
    require: 'ngModel',਍    氀椀渀欀㨀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀Ⰰ 挀琀爀氀⤀ 笀ഀഀ
      var match = /\/(.*)\//.exec(attr.ngList),਍          猀攀瀀愀爀愀琀漀爀 㴀 洀愀琀挀栀 ☀☀ 渀攀眀 刀攀最䔀砀瀀⠀洀愀琀挀栀嬀㄀崀⤀ 簀簀 愀琀琀爀⸀渀最䰀椀猀琀 簀簀 ✀Ⰰ✀㬀ഀഀ
਍      瘀愀爀 瀀愀爀猀攀 㴀 昀甀渀挀琀椀漀渀⠀瘀椀攀眀嘀愀氀甀攀⤀ 笀ഀഀ
        // If the viewValue is invalid (say required but empty) it will be `undefined`਍        椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀瘀椀攀眀嘀愀氀甀攀⤀⤀ 爀攀琀甀爀渀㬀ഀഀ
਍        瘀愀爀 氀椀猀琀 㴀 嬀崀㬀ഀഀ
਍        椀昀 ⠀瘀椀攀眀嘀愀氀甀攀⤀ 笀ഀഀ
          forEach(viewValue.split(separator), function(value) {਍            椀昀 ⠀瘀愀氀甀攀⤀ 氀椀猀琀⸀瀀甀猀栀⠀琀爀椀洀⠀瘀愀氀甀攀⤀⤀㬀ഀഀ
          });਍        紀ഀഀ
਍        爀攀琀甀爀渀 氀椀猀琀㬀ഀഀ
      };਍ഀഀ
      ctrl.$parsers.push(parse);਍      挀琀爀氀⸀␀昀漀爀洀愀琀琀攀爀猀⸀瀀甀猀栀⠀昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
        if (isArray(value)) {਍          爀攀琀甀爀渀 瘀愀氀甀攀⸀樀漀椀渀⠀✀Ⰰ ✀⤀㬀ഀഀ
        }਍ഀഀ
        return undefined;਍      紀⤀㬀ഀഀ
਍      ⼀⼀ 伀瘀攀爀爀椀搀攀 琀栀攀 猀琀愀渀搀愀爀搀 ␀椀猀䔀洀瀀琀礀 戀攀挀愀甀猀攀 愀渀 攀洀瀀琀礀 愀爀爀愀礀 洀攀愀渀猀 琀栀攀 椀渀瀀甀琀 椀猀 攀洀瀀琀礀⸀ഀഀ
      ctrl.$isEmpty = function(value) {਍        爀攀琀甀爀渀 ℀瘀愀氀甀攀 簀簀 ℀瘀愀氀甀攀⸀氀攀渀最琀栀㬀ഀഀ
      };਍    紀ഀഀ
  };਍紀㬀ഀഀ
਍ഀഀ
var CONSTANT_VALUE_REGEXP = /^(true|false|\d+)$/;਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最嘀愀氀甀攀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Binds the given expression to the value of `input[select]` or `input[radio]`, so਍ ⨀ 琀栀愀琀 眀栀攀渀 琀栀攀 攀氀攀洀攀渀琀 椀猀 猀攀氀攀挀琀攀搀Ⰰ 琀栀攀 怀渀最䴀漀搀攀氀怀 漀昀 琀栀愀琀 攀氀攀洀攀渀琀 椀猀 猀攀琀 琀漀 琀栀攀ഀഀ
 * bound value.਍ ⨀ഀഀ
 * `ngValue` is useful when dynamically generating lists of radio buttons using `ng-repeat`, as਍ ⨀ 猀栀漀眀渀 戀攀氀漀眀⸀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 椀渀瀀甀琀ഀഀ
 * @param {string=} ngValue angular expression, whose value will be bound to the `value` attribute਍ ⨀   漀昀 琀栀攀 怀椀渀瀀甀琀怀 攀氀攀洀攀渀琀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
    <doc:example>਍      㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
       <script>਍          昀甀渀挀琀椀漀渀 䌀琀爀氀⠀␀猀挀漀瀀攀⤀ 笀ഀഀ
            $scope.names = ['pizza', 'unicorns', 'robots'];਍            ␀猀挀漀瀀攀⸀洀礀 㴀 笀 昀愀瘀漀爀椀琀攀㨀 ✀甀渀椀挀漀爀渀猀✀ 紀㬀ഀഀ
          }਍       㰀⼀猀挀爀椀瀀琀㸀ഀഀ
        <form ng-controller="Ctrl">਍          㰀栀㈀㸀圀栀椀挀栀 椀猀 礀漀甀爀 昀愀瘀漀爀椀琀攀㼀㰀⼀栀㈀㸀ഀഀ
            <label ng-repeat="name in names" for="{{name}}">਍              笀笀渀愀洀攀紀紀ഀഀ
              <input type="radio"਍                     渀最ⴀ洀漀搀攀氀㴀∀洀礀⸀昀愀瘀漀爀椀琀攀∀ഀഀ
                     ng-value="name"਍                     椀搀㴀∀笀笀渀愀洀攀紀紀∀ഀഀ
                     name="favorite">਍            㰀⼀氀愀戀攀氀㸀ഀഀ
          <div>You chose {{my.favorite}}</div>਍        㰀⼀昀漀爀洀㸀ഀഀ
      </doc:source>਍      㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
        it('should initialize to model', function() {਍          攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀⸀昀愀瘀漀爀椀琀攀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀甀渀椀挀漀爀渀猀✀⤀㬀ഀഀ
        });਍        椀琀⠀✀猀栀漀甀氀搀 戀椀渀搀 琀栀攀 瘀愀氀甀攀猀 琀漀 琀栀攀 椀渀瀀甀琀猀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          input('my.favorite').select('pizza');਍          攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀洀礀⸀昀愀瘀漀爀椀琀攀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀瀀椀稀稀愀✀⤀㬀ഀഀ
        });਍      㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
    </doc:example>਍ ⨀⼀ഀഀ
var ngValueDirective = function() {਍  爀攀琀甀爀渀 笀ഀഀ
    priority: 100,਍    挀漀洀瀀椀氀攀㨀 昀甀渀挀琀椀漀渀⠀琀瀀氀Ⰰ 琀瀀氀䄀琀琀爀⤀ 笀ഀഀ
      if (CONSTANT_VALUE_REGEXP.test(tplAttr.ngValue)) {਍        爀攀琀甀爀渀 昀甀渀挀琀椀漀渀 渀最嘀愀氀甀攀䌀漀渀猀琀愀渀琀䰀椀渀欀⠀猀挀漀瀀攀Ⰰ 攀氀洀Ⰰ 愀琀琀爀⤀ 笀ഀഀ
          attr.$set('value', scope.$eval(attr.ngValue));਍        紀㬀ഀഀ
      } else {਍        爀攀琀甀爀渀 昀甀渀挀琀椀漀渀 渀最嘀愀氀甀攀䰀椀渀欀⠀猀挀漀瀀攀Ⰰ 攀氀洀Ⰰ 愀琀琀爀⤀ 笀ഀഀ
          scope.$watch(attr.ngValue, function valueWatchAction(value) {਍            愀琀琀爀⸀␀猀攀琀⠀✀瘀愀氀甀攀✀Ⰰ 瘀愀氀甀攀⤀㬀ഀഀ
          });਍        紀㬀ഀഀ
      }਍    紀ഀഀ
  };਍紀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䈀椀渀搀ഀഀ
 * @restrict AC਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀攀 怀渀最䈀椀渀搀怀 愀琀琀爀椀戀甀琀攀 琀攀氀氀猀 䄀渀最甀氀愀爀 琀漀 爀攀瀀氀愀挀攀 琀栀攀 琀攀砀琀 挀漀渀琀攀渀琀 漀昀 琀栀攀 猀瀀攀挀椀昀椀攀搀 䠀吀䴀䰀 攀氀攀洀攀渀琀ഀഀ
 * with the value of a given expression, and to update the text content when the value of that਍ ⨀ 攀砀瀀爀攀猀猀椀漀渀 挀栀愀渀最攀猀⸀ഀഀ
 *਍ ⨀ 吀礀瀀椀挀愀氀氀礀Ⰰ 礀漀甀 搀漀渀✀琀 甀猀攀 怀渀最䈀椀渀搀怀 搀椀爀攀挀琀氀礀Ⰰ 戀甀琀 椀渀猀琀攀愀搀 礀漀甀 甀猀攀 琀栀攀 搀漀甀戀氀攀 挀甀爀氀礀 洀愀爀欀甀瀀 氀椀欀攀ഀഀ
 * `{{ expression }}` which is similar but less verbose.਍ ⨀ഀഀ
 * It is preferrable to use `ngBind` instead of `{{ expression }}` when a template is momentarily਍ ⨀ 搀椀猀瀀氀愀礀攀搀 戀礀 琀栀攀 戀爀漀眀猀攀爀 椀渀 椀琀猀 爀愀眀 猀琀愀琀攀 戀攀昀漀爀攀 䄀渀最甀氀愀爀 挀漀洀瀀椀氀攀猀 椀琀⸀ 匀椀渀挀攀 怀渀最䈀椀渀搀怀 椀猀 愀渀ഀഀ
 * element attribute, it makes the bindings invisible to the user while the page is loading.਍ ⨀ഀഀ
 * An alternative solution to this problem would be using the਍ ⨀ 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䌀氀漀愀欀 渀最䌀氀漀愀欀紀 搀椀爀攀挀琀椀瘀攀⸀ഀഀ
 *਍ ⨀ഀഀ
 * @element ANY਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䈀椀渀搀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 琀漀 攀瘀愀氀甀愀琀攀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
 * Enter a name in the Live Preview text box; the greeting below the text box changes instantly.਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍       㰀猀挀爀椀瀀琀㸀ഀഀ
         function Ctrl($scope) {਍           ␀猀挀漀瀀攀⸀渀愀洀攀 㴀 ✀圀栀椀爀氀攀搀✀㬀ഀഀ
         }਍       㰀⼀猀挀爀椀瀀琀㸀ഀഀ
       <div ng-controller="Ctrl">਍         䔀渀琀攀爀 渀愀洀攀㨀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀琀攀砀琀∀ 渀最ⴀ洀漀搀攀氀㴀∀渀愀洀攀∀㸀㰀戀爀㸀ഀഀ
         Hello <span ng-bind="name"></span>!਍       㰀⼀搀椀瘀㸀ഀഀ
     </doc:source>਍     㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
       it('should check ng-bind', function() {਍         攀砀瀀攀挀琀⠀甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀戀椀渀搀椀渀最⠀✀渀愀洀攀✀⤀⤀⸀琀漀䈀攀⠀✀圀栀椀爀氀攀搀✀⤀㬀ഀഀ
         using('.doc-example-live').input('name').enter('world');਍         攀砀瀀攀挀琀⠀甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀戀椀渀搀椀渀最⠀✀渀愀洀攀✀⤀⤀⸀琀漀䈀攀⠀✀眀漀爀氀搀✀⤀㬀ഀഀ
       });਍     㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
   </doc:example>਍ ⨀⼀ഀഀ
var ngBindDirective = ngDirective(function(scope, element, attr) {਍  攀氀攀洀攀渀琀⸀愀搀搀䌀氀愀猀猀⠀✀渀最ⴀ戀椀渀搀椀渀最✀⤀⸀搀愀琀愀⠀✀␀戀椀渀搀椀渀最✀Ⰰ 愀琀琀爀⸀渀最䈀椀渀搀⤀㬀ഀഀ
  scope.$watch(attr.ngBind, function ngBindWatchAction(value) {਍    ⼀⼀ 圀攀 愀爀攀 瀀甀爀瀀漀猀攀昀甀氀氀礀 甀猀椀渀最 㴀㴀 栀攀爀攀 爀愀琀栀攀爀 琀栀愀渀 㴀㴀㴀 戀攀挀愀甀猀攀 眀攀 眀愀渀琀 琀漀ഀഀ
    // catch when value is "null or undefined"਍    ⼀⼀ 樀猀栀椀渀琀 ⴀ圀　㐀㄀ഀഀ
    element.text(value == undefined ? '' : value);਍  紀⤀㬀ഀഀ
});਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䈀椀渀搀吀攀洀瀀氀愀琀攀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * The `ngBindTemplate` directive specifies that the element਍ ⨀ 琀攀砀琀 挀漀渀琀攀渀琀 猀栀漀甀氀搀 戀攀 爀攀瀀氀愀挀攀搀 眀椀琀栀 琀栀攀 椀渀琀攀爀瀀漀氀愀琀椀漀渀 漀昀 琀栀攀 琀攀洀瀀氀愀琀攀ഀഀ
 * in the `ngBindTemplate` attribute.਍ ⨀ 唀渀氀椀欀攀 怀渀最䈀椀渀搀怀Ⰰ 琀栀攀 怀渀最䈀椀渀搀吀攀洀瀀氀愀琀攀怀 挀愀渀 挀漀渀琀愀椀渀 洀甀氀琀椀瀀氀攀 怀笀笀怀 怀紀紀怀ഀഀ
 * expressions. This directive is needed since some HTML elements਍ ⨀ ⠀猀甀挀栀 愀猀 吀䤀吀䰀䔀 愀渀搀 伀倀吀䤀伀一⤀ 挀愀渀渀漀琀 挀漀渀琀愀椀渀 匀倀䄀一 攀氀攀洀攀渀琀猀⸀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䄀一夀ഀഀ
 * @param {string} ngBindTemplate template of form਍ ⨀   㰀琀琀㸀笀笀㰀⼀琀琀㸀 㰀琀琀㸀攀砀瀀爀攀猀猀椀漀渀㰀⼀琀琀㸀 㰀琀琀㸀紀紀㰀⼀琀琀㸀 琀漀 攀瘀愀氀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
 * Try it here: enter text in text box and watch the greeting change.਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍       㰀猀挀爀椀瀀琀㸀ഀഀ
         function Ctrl($scope) {਍           ␀猀挀漀瀀攀⸀猀愀氀甀琀愀琀椀漀渀 㴀 ✀䠀攀氀氀漀✀㬀ഀഀ
           $scope.name = 'World';਍         紀ഀഀ
       </script>਍       㰀搀椀瘀 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀䌀琀爀氀∀㸀ഀഀ
        Salutation: <input type="text" ng-model="salutation"><br>਍        一愀洀攀㨀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀琀攀砀琀∀ 渀最ⴀ洀漀搀攀氀㴀∀渀愀洀攀∀㸀㰀戀爀㸀ഀഀ
        <pre ng-bind-template="{{salutation}} {{name}}!"></pre>਍       㰀⼀搀椀瘀㸀ഀഀ
     </doc:source>਍     㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
       it('should check ng-bind', function() {਍         攀砀瀀攀挀琀⠀甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀戀椀渀搀椀渀最⠀✀猀愀氀甀琀愀琀椀漀渀✀⤀⤀⸀ഀഀ
           toBe('Hello');਍         攀砀瀀攀挀琀⠀甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀戀椀渀搀椀渀最⠀✀渀愀洀攀✀⤀⤀⸀ഀഀ
           toBe('World');਍         甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀椀渀瀀甀琀⠀✀猀愀氀甀琀愀琀椀漀渀✀⤀⸀攀渀琀攀爀⠀✀䜀爀攀攀琀椀渀最猀✀⤀㬀ഀഀ
         using('.doc-example-live').input('name').enter('user');਍         攀砀瀀攀挀琀⠀甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀戀椀渀搀椀渀最⠀✀猀愀氀甀琀愀琀椀漀渀✀⤀⤀⸀ഀഀ
           toBe('Greetings');਍         攀砀瀀攀挀琀⠀甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀戀椀渀搀椀渀最⠀✀渀愀洀攀✀⤀⤀⸀ഀഀ
           toBe('user');਍       紀⤀㬀ഀഀ
     </doc:scenario>਍   㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍瘀愀爀 渀最䈀椀渀搀吀攀洀瀀氀愀琀攀䐀椀爀攀挀琀椀瘀攀 㴀 嬀✀␀椀渀琀攀爀瀀漀氀愀琀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀椀渀琀攀爀瀀漀氀愀琀攀⤀ 笀ഀഀ
  return function(scope, element, attr) {਍    ⼀⼀ 吀伀䐀伀㨀 洀漀瘀攀 琀栀椀猀 琀漀 猀挀攀渀愀爀椀漀 爀甀渀渀攀爀ഀഀ
    var interpolateFn = $interpolate(element.attr(attr.$attr.ngBindTemplate));਍    攀氀攀洀攀渀琀⸀愀搀搀䌀氀愀猀猀⠀✀渀最ⴀ戀椀渀搀椀渀最✀⤀⸀搀愀琀愀⠀✀␀戀椀渀搀椀渀最✀Ⰰ 椀渀琀攀爀瀀漀氀愀琀攀䘀渀⤀㬀ഀഀ
    attr.$observe('ngBindTemplate', function(value) {਍      攀氀攀洀攀渀琀⸀琀攀砀琀⠀瘀愀氀甀攀⤀㬀ഀഀ
    });਍  紀㬀ഀഀ
}];਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䈀椀渀搀䠀琀洀氀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Creates a binding that will innerHTML the result of evaluating the `expression` into the current਍ ⨀ 攀氀攀洀攀渀琀 椀渀 愀 猀攀挀甀爀攀 眀愀礀⸀  䈀礀 搀攀昀愀甀氀琀Ⰰ 琀栀攀 椀渀渀攀爀䠀吀䴀䰀ⴀ攀搀 挀漀渀琀攀渀琀 眀椀氀氀 戀攀 猀愀渀椀琀椀稀攀搀 甀猀椀渀最 琀栀攀 笀䀀氀椀渀欀ഀഀ
 * ngSanitize.$sanitize $sanitize} service.  To utilize this functionality, ensure that `$sanitize`਍ ⨀ 椀猀 愀瘀愀椀氀愀戀氀攀Ⰰ 昀漀爀 攀砀愀洀瀀氀攀Ⰰ 戀礀 椀渀挀氀甀搀椀渀最 笀䀀氀椀渀欀 渀最匀愀渀椀琀椀稀攀紀 椀渀 礀漀甀爀 洀漀搀甀氀攀✀猀 搀攀瀀攀渀搀攀渀挀椀攀猀 ⠀渀漀琀 椀渀ഀഀ
 * core Angular.)  You may also bypass sanitization for values you know are safe. To do so, bind to਍ ⨀ 愀渀 攀砀瀀氀椀挀椀琀氀礀 琀爀甀猀琀攀搀 瘀愀氀甀攀 瘀椀愀 笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀䠀琀洀氀 ␀猀挀攀⸀琀爀甀猀琀䄀猀䠀琀洀氀紀⸀  匀攀攀 琀栀攀 攀砀愀洀瀀氀攀ഀഀ
 * under {@link ng.$sce#Example Strict Contextual Escaping (SCE)}.਍ ⨀ഀഀ
 * Note: If a `$sanitize` service is unavailable and the bound value isn't explicitly trusted, you਍ ⨀ 眀椀氀氀 栀愀瘀攀 愀渀 攀砀挀攀瀀琀椀漀渀 ⠀椀渀猀琀攀愀搀 漀昀 愀渀 攀砀瀀氀漀椀琀⸀⤀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䄀一夀ഀഀ
 * @param {expression} ngBindHtml {@link guide/expression Expression} to evaluate.਍ ⨀ഀഀ
 * @example਍   吀爀礀 椀琀 栀攀爀攀㨀 攀渀琀攀爀 琀攀砀琀 椀渀 琀攀砀琀 戀漀砀 愀渀搀 眀愀琀挀栀 琀栀攀 最爀攀攀琀椀渀最 挀栀愀渀最攀⸀ഀഀ
 ਍   㰀攀砀愀洀瀀氀攀 洀漀搀甀氀攀㴀∀渀最䈀椀渀搀䠀琀洀氀䔀砀愀洀瀀氀攀∀ 搀攀瀀猀㴀∀愀渀最甀氀愀爀ⴀ猀愀渀椀琀椀稀攀⸀樀猀∀㸀ഀഀ
     <file name="index.html">਍       㰀搀椀瘀 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀渀最䈀椀渀搀䠀琀洀氀䌀琀爀氀∀㸀ഀഀ
        <p ng-bind-html="myHTML"></p>਍       㰀⼀搀椀瘀㸀ഀഀ
     </file>਍     ഀഀ
     <file name="script.js">਍       愀渀最甀氀愀爀⸀洀漀搀甀氀攀⠀✀渀最䈀椀渀搀䠀琀洀氀䔀砀愀洀瀀氀攀✀Ⰰ 嬀✀渀最匀愀渀椀琀椀稀攀✀崀⤀ഀഀ
਍       ⸀挀漀渀琀爀漀氀氀攀爀⠀✀渀最䈀椀渀搀䠀琀洀氀䌀琀爀氀✀Ⰰ 嬀✀␀猀挀漀瀀攀✀Ⰰ 昀甀渀挀琀椀漀渀 渀最䈀椀渀搀䠀琀洀氀䌀琀爀氀⠀␀猀挀漀瀀攀⤀ 笀ഀഀ
         $scope.myHTML =਍            ✀䤀 愀洀 愀渀 㰀挀漀搀攀㸀䠀吀䴀䰀㰀⼀挀漀搀攀㸀猀琀爀椀渀最 眀椀琀栀 㰀愀 栀爀攀昀㴀∀⌀∀㸀氀椀渀欀猀℀㰀⼀愀㸀 愀渀搀 漀琀栀攀爀 㰀攀洀㸀猀琀甀昀昀㰀⼀攀洀㸀✀㬀ഀഀ
       }]);਍     㰀⼀昀椀氀攀㸀ഀഀ
਍     㰀昀椀氀攀 渀愀洀攀㴀∀猀挀攀渀愀爀椀漀⸀樀猀∀㸀ഀഀ
       it('should check ng-bind-html', function() {਍         攀砀瀀攀挀琀⠀甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀戀椀渀搀椀渀最⠀✀洀礀䠀吀䴀䰀✀⤀⤀⸀ഀഀ
           toBe(਍           ✀䤀 愀洀 愀渀 㰀挀漀搀攀㸀䠀吀䴀䰀㰀⼀挀漀搀攀㸀猀琀爀椀渀最 眀椀琀栀 㰀愀 栀爀攀昀㴀∀⌀∀㸀氀椀渀欀猀℀㰀⼀愀㸀 愀渀搀 漀琀栀攀爀 㰀攀洀㸀猀琀甀昀昀㰀⼀攀洀㸀✀ഀഀ
           );਍       紀⤀㬀ഀഀ
     </file>਍   㰀⼀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍瘀愀爀 渀最䈀椀渀搀䠀琀洀氀䐀椀爀攀挀琀椀瘀攀 㴀 嬀✀␀猀挀攀✀Ⰰ ✀␀瀀愀爀猀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀猀挀攀Ⰰ ␀瀀愀爀猀攀⤀ 笀ഀഀ
  return function(scope, element, attr) {਍    攀氀攀洀攀渀琀⸀愀搀搀䌀氀愀猀猀⠀✀渀最ⴀ戀椀渀搀椀渀最✀⤀⸀搀愀琀愀⠀✀␀戀椀渀搀椀渀最✀Ⰰ 愀琀琀爀⸀渀最䈀椀渀搀䠀琀洀氀⤀㬀ഀഀ
਍    瘀愀爀 瀀愀爀猀攀搀 㴀 ␀瀀愀爀猀攀⠀愀琀琀爀⸀渀最䈀椀渀搀䠀琀洀氀⤀㬀ഀഀ
    function getStringValue() { return (parsed(scope) || '').toString(); }਍ഀഀ
    scope.$watch(getStringValue, function ngBindHtmlWatchAction(value) {਍      攀氀攀洀攀渀琀⸀栀琀洀氀⠀␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀䠀琀洀氀⠀瀀愀爀猀攀搀⠀猀挀漀瀀攀⤀⤀ 簀簀 ✀✀⤀㬀ഀഀ
    });਍  紀㬀ഀഀ
}];਍ഀഀ
function classDirective(name, selector) {਍  渀愀洀攀 㴀 ✀渀最䌀氀愀猀猀✀ ⬀ 渀愀洀攀㬀ഀഀ
  return function() {਍    爀攀琀甀爀渀 笀ഀഀ
      restrict: 'AC',਍      氀椀渀欀㨀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀⤀ 笀ഀഀ
        var oldVal;਍ഀഀ
        scope.$watch(attr[name], ngClassWatchAction, true);਍ഀഀ
        attr.$observe('class', function(value) {਍          渀最䌀氀愀猀猀圀愀琀挀栀䄀挀琀椀漀渀⠀猀挀漀瀀攀⸀␀攀瘀愀氀⠀愀琀琀爀嬀渀愀洀攀崀⤀⤀㬀ഀഀ
        });਍ഀഀ
਍        椀昀 ⠀渀愀洀攀 ℀㴀㴀 ✀渀最䌀氀愀猀猀✀⤀ 笀ഀഀ
          scope.$watch('$index', function($index, old$index) {਍            ⼀⼀ 樀猀栀椀渀琀 戀椀琀眀椀猀攀㨀 昀愀氀猀攀ഀഀ
            var mod = $index & 1;਍            椀昀 ⠀洀漀搀 ℀㴀㴀 漀氀搀␀椀渀搀攀砀 ☀ ㄀⤀ 笀ഀഀ
              var classes = flattenClasses(scope.$eval(attr[name]));਍              洀漀搀 㴀㴀㴀 猀攀氀攀挀琀漀爀 㼀ഀഀ
                attr.$addClass(classes) :਍                愀琀琀爀⸀␀爀攀洀漀瘀攀䌀氀愀猀猀⠀挀氀愀猀猀攀猀⤀㬀ഀഀ
            }਍          紀⤀㬀ഀഀ
        }਍ഀഀ
਍        昀甀渀挀琀椀漀渀 渀最䌀氀愀猀猀圀愀琀挀栀䄀挀琀椀漀渀⠀渀攀眀嘀愀氀⤀ 笀ഀഀ
          if (selector === true || scope.$index % 2 === selector) {਍            瘀愀爀 渀攀眀䌀氀愀猀猀攀猀 㴀 昀氀愀琀琀攀渀䌀氀愀猀猀攀猀⠀渀攀眀嘀愀氀 簀簀 ✀✀⤀㬀ഀഀ
            if(!oldVal) {਍              愀琀琀爀⸀␀愀搀搀䌀氀愀猀猀⠀渀攀眀䌀氀愀猀猀攀猀⤀㬀ഀഀ
            } else if(!equals(newVal,oldVal)) {਍              愀琀琀爀⸀␀甀瀀搀愀琀攀䌀氀愀猀猀⠀渀攀眀䌀氀愀猀猀攀猀Ⰰ 昀氀愀琀琀攀渀䌀氀愀猀猀攀猀⠀漀氀搀嘀愀氀⤀⤀㬀ഀഀ
            }਍          紀ഀഀ
          oldVal = copy(newVal);਍        紀ഀഀ
਍ഀഀ
        function flattenClasses(classVal) {਍          椀昀⠀椀猀䄀爀爀愀礀⠀挀氀愀猀猀嘀愀氀⤀⤀ 笀ഀഀ
            return classVal.join(' ');਍          紀 攀氀猀攀 椀昀 ⠀椀猀伀戀樀攀挀琀⠀挀氀愀猀猀嘀愀氀⤀⤀ 笀ഀഀ
            var classes = [], i = 0;਍            昀漀爀䔀愀挀栀⠀挀氀愀猀猀嘀愀氀Ⰰ 昀甀渀挀琀椀漀渀⠀瘀Ⰰ 欀⤀ 笀ഀഀ
              if (v) {਍                挀氀愀猀猀攀猀⸀瀀甀猀栀⠀欀⤀㬀ഀഀ
              }਍            紀⤀㬀ഀഀ
            return classes.join(' ');਍          紀ഀഀ
਍          爀攀琀甀爀渀 挀氀愀猀猀嘀愀氀㬀ഀഀ
        }਍      紀ഀഀ
    };਍  紀㬀ഀഀ
}਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngClass਍ ⨀ 䀀爀攀猀琀爀椀挀琀 䄀䌀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * The `ngClass` directive allows you to dynamically set CSS classes on an HTML element by databinding਍ ⨀ 愀渀 攀砀瀀爀攀猀猀椀漀渀 琀栀愀琀 爀攀瀀爀攀猀攀渀琀猀 愀氀氀 挀氀愀猀猀攀猀 琀漀 戀攀 愀搀搀攀搀⸀ഀഀ
 *਍ ⨀ 吀栀攀 搀椀爀攀挀琀椀瘀攀 眀漀渀✀琀 愀搀搀 搀甀瀀氀椀挀愀琀攀 挀氀愀猀猀攀猀 椀昀 愀 瀀愀爀琀椀挀甀氀愀爀 挀氀愀猀猀 眀愀猀 愀氀爀攀愀搀礀 猀攀琀⸀ഀഀ
 *਍ ⨀ 圀栀攀渀 琀栀攀 攀砀瀀爀攀猀猀椀漀渀 挀栀愀渀最攀猀Ⰰ 琀栀攀 瀀爀攀瘀椀漀甀猀氀礀 愀搀搀攀搀 挀氀愀猀猀攀猀 愀爀攀 爀攀洀漀瘀攀搀 愀渀搀 漀渀氀礀 琀栀攀渀 琀栀攀ഀഀ
 * new classes are added.਍ ⨀ഀഀ
 * @animations਍ ⨀ 愀搀搀 ⴀ 栀愀瀀瀀攀渀猀 樀甀猀琀 戀攀昀漀爀攀 琀栀攀 挀氀愀猀猀 椀猀 愀瀀瀀氀椀攀搀 琀漀 琀栀攀 攀氀攀洀攀渀琀ഀഀ
 * remove - happens just before the class is removed from the element਍ ⨀ഀഀ
 * @element ANY਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䌀氀愀猀猀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 琀漀 攀瘀愀氀⸀ 吀栀攀 爀攀猀甀氀琀ഀഀ
 *   of the evaluation can be a string representing space delimited class਍ ⨀   渀愀洀攀猀Ⰰ 愀渀 愀爀爀愀礀Ⰰ 漀爀 愀 洀愀瀀 漀昀 挀氀愀猀猀 渀愀洀攀猀 琀漀 戀漀漀氀攀愀渀 瘀愀氀甀攀猀⸀ 䤀渀 琀栀攀 挀愀猀攀 漀昀 愀 洀愀瀀Ⰰ 琀栀攀ഀഀ
 *   names of the properties whose values are truthy will be added as css classes to the਍ ⨀   攀氀攀洀攀渀琀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀 䔀砀愀洀瀀氀攀 琀栀愀琀 搀攀洀漀渀猀琀爀愀琀攀猀 戀愀猀椀挀 戀椀渀搀椀渀最猀 瘀椀愀 渀最䌀氀愀猀猀 搀椀爀攀挀琀椀瘀攀⸀ഀഀ
   <example>਍     㰀昀椀氀攀 渀愀洀攀㴀∀椀渀搀攀砀⸀栀琀洀氀∀㸀ഀഀ
       <p ng-class="{strike: deleted, bold: important, red: error}">Map Syntax Example</p>਍       㰀椀渀瀀甀琀 琀礀瀀攀㴀∀挀栀攀挀欀戀漀砀∀ 渀最ⴀ洀漀搀攀氀㴀∀搀攀氀攀琀攀搀∀㸀 搀攀氀攀琀攀搀 ⠀愀瀀瀀氀礀 ∀猀琀爀椀欀攀∀ 挀氀愀猀猀⤀㰀戀爀㸀ഀഀ
       <input type="checkbox" ng-model="important"> important (apply "bold" class)<br>਍       㰀椀渀瀀甀琀 琀礀瀀攀㴀∀挀栀攀挀欀戀漀砀∀ 渀最ⴀ洀漀搀攀氀㴀∀攀爀爀漀爀∀㸀 攀爀爀漀爀 ⠀愀瀀瀀氀礀 ∀爀攀搀∀ 挀氀愀猀猀⤀ഀഀ
       <hr>਍       㰀瀀 渀最ⴀ挀氀愀猀猀㴀∀猀琀礀氀攀∀㸀唀猀椀渀最 匀琀爀椀渀最 匀礀渀琀愀砀㰀⼀瀀㸀ഀഀ
       <input type="text" ng-model="style" placeholder="Type: bold strike red">਍       㰀栀爀㸀ഀഀ
       <p ng-class="[style1, style2, style3]">Using Array Syntax</p>਍       㰀椀渀瀀甀琀 渀最ⴀ洀漀搀攀氀㴀∀猀琀礀氀攀㄀∀ 瀀氀愀挀攀栀漀氀搀攀爀㴀∀吀礀瀀攀㨀 戀漀氀搀Ⰰ 猀琀爀椀欀攀 漀爀 爀攀搀∀㸀㰀戀爀㸀ഀഀ
       <input ng-model="style2" placeholder="Type: bold, strike or red"><br>਍       㰀椀渀瀀甀琀 渀最ⴀ洀漀搀攀氀㴀∀猀琀礀氀攀㌀∀ 瀀氀愀挀攀栀漀氀搀攀爀㴀∀吀礀瀀攀㨀 戀漀氀搀Ⰰ 猀琀爀椀欀攀 漀爀 爀攀搀∀㸀㰀戀爀㸀ഀഀ
     </file>਍     㰀昀椀氀攀 渀愀洀攀㴀∀猀琀礀氀攀⸀挀猀猀∀㸀ഀഀ
       .strike {਍         琀攀砀琀ⴀ搀攀挀漀爀愀琀椀漀渀㨀 氀椀渀攀ⴀ琀栀爀漀甀最栀㬀ഀഀ
       }਍       ⸀戀漀氀搀 笀ഀഀ
           font-weight: bold;਍       紀ഀഀ
       .red {਍           挀漀氀漀爀㨀 爀攀搀㬀ഀഀ
       }਍     㰀⼀昀椀氀攀㸀ഀഀ
     <file name="scenario.js">਍       椀琀⠀✀猀栀漀甀氀搀 氀攀琀 礀漀甀 琀漀最最氀攀 琀栀攀 挀氀愀猀猀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 瀀㨀昀椀爀猀琀✀⤀⸀瀀爀漀瀀⠀✀挀氀愀猀猀一愀洀攀✀⤀⤀⸀渀漀琀⠀⤀⸀琀漀䴀愀琀挀栀⠀⼀戀漀氀搀⼀⤀㬀ഀഀ
         expect(element('.doc-example-live p:first').prop('className')).not().toMatch(/red/);਍ഀഀ
         input('important').check();਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 瀀㨀昀椀爀猀琀✀⤀⸀瀀爀漀瀀⠀✀挀氀愀猀猀一愀洀攀✀⤀⤀⸀琀漀䴀愀琀挀栀⠀⼀戀漀氀搀⼀⤀㬀ഀഀ
਍         椀渀瀀甀琀⠀✀攀爀爀漀爀✀⤀⸀挀栀攀挀欀⠀⤀㬀ഀഀ
         expect(element('.doc-example-live p:first').prop('className')).toMatch(/red/);਍       紀⤀㬀ഀഀ
਍       椀琀⠀✀猀栀漀甀氀搀 氀攀琀 礀漀甀 琀漀最最氀攀 猀琀爀椀渀最 攀砀愀洀瀀氀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         expect(element('.doc-example-live p:nth-of-type(2)').prop('className')).toBe('');਍         椀渀瀀甀琀⠀✀猀琀礀氀攀✀⤀⸀攀渀琀攀爀⠀✀爀攀搀✀⤀㬀ഀഀ
         expect(element('.doc-example-live p:nth-of-type(2)').prop('className')).toBe('red');਍       紀⤀㬀ഀഀ
਍       椀琀⠀✀愀爀爀愀礀 攀砀愀洀瀀氀攀 猀栀漀甀氀搀 栀愀瘀攀 ㌀ 挀氀愀猀猀攀猀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         expect(element('.doc-example-live p:last').prop('className')).toBe('');਍         椀渀瀀甀琀⠀✀猀琀礀氀攀㄀✀⤀⸀攀渀琀攀爀⠀✀戀漀氀搀✀⤀㬀ഀഀ
         input('style2').enter('strike');਍         椀渀瀀甀琀⠀✀猀琀礀氀攀㌀✀⤀⸀攀渀琀攀爀⠀✀爀攀搀✀⤀㬀ഀഀ
         expect(element('.doc-example-live p:last').prop('className')).toBe('bold strike red');਍       紀⤀㬀ഀഀ
     </file>਍   㰀⼀攀砀愀洀瀀氀攀㸀ഀഀ
਍   ⌀⌀ 䄀渀椀洀愀琀椀漀渀猀ഀഀ
਍   吀栀攀 攀砀愀洀瀀氀攀 戀攀氀漀眀 搀攀洀漀渀猀琀爀愀琀攀猀 栀漀眀 琀漀 瀀攀爀昀漀爀洀 愀渀椀洀愀琀椀漀渀猀 甀猀椀渀最 渀最䌀氀愀猀猀⸀ഀഀ
਍   㰀攀砀愀洀瀀氀攀 愀渀椀洀愀琀椀漀渀猀㴀∀琀爀甀攀∀㸀ഀഀ
     <file name="index.html">਍      㰀椀渀瀀甀琀 琀礀瀀攀㴀∀戀甀琀琀漀渀∀ 瘀愀氀甀攀㴀∀猀攀琀∀ 渀最ⴀ挀氀椀挀欀㴀∀洀礀嘀愀爀㴀✀洀礀ⴀ挀氀愀猀猀✀∀㸀ഀഀ
      <input type="button" value="clear" ng-click="myVar=''">਍      㰀戀爀㸀ഀഀ
      <span class="base-class" ng-class="myVar">Sample Text</span>਍     㰀⼀昀椀氀攀㸀ഀഀ
     <file name="style.css">਍       ⸀戀愀猀攀ⴀ挀氀愀猀猀 笀ഀഀ
         -webkit-transition:all cubic-bezier(0.250, 0.460, 0.450, 0.940) 0.5s;਍         琀爀愀渀猀椀琀椀漀渀㨀愀氀氀 挀甀戀椀挀ⴀ戀攀稀椀攀爀⠀　⸀㈀㔀　Ⰰ 　⸀㐀㘀　Ⰰ 　⸀㐀㔀　Ⰰ 　⸀㤀㐀　⤀ 　⸀㔀猀㬀ഀഀ
       }਍ഀഀ
       .base-class.my-class {਍         挀漀氀漀爀㨀 爀攀搀㬀ഀഀ
         font-size:3em;਍       紀ഀഀ
     </file>਍     㰀昀椀氀攀 渀愀洀攀㴀∀猀挀攀渀愀爀椀漀⸀樀猀∀㸀ഀഀ
       it('should check ng-class', function() {਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 猀瀀愀渀✀⤀⸀瀀爀漀瀀⠀✀挀氀愀猀猀一愀洀攀✀⤀⤀⸀渀漀琀⠀⤀⸀ഀഀ
           toMatch(/my-class/);਍ഀഀ
         using('.doc-example-live').element(':button:first').click();਍ഀഀ
         expect(element('.doc-example-live span').prop('className')).਍           琀漀䴀愀琀挀栀⠀⼀洀礀ⴀ挀氀愀猀猀⼀⤀㬀ഀഀ
਍         甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀攀氀攀洀攀渀琀⠀✀㨀戀甀琀琀漀渀㨀氀愀猀琀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 猀瀀愀渀✀⤀⸀瀀爀漀瀀⠀✀挀氀愀猀猀一愀洀攀✀⤀⤀⸀渀漀琀⠀⤀⸀ഀഀ
           toMatch(/my-class/);਍       紀⤀㬀ഀഀ
     </file>਍   㰀⼀攀砀愀洀瀀氀攀㸀ഀഀ
਍ഀഀ
   ## ngClass and pre-existing CSS3 Transitions/Animations਍   吀栀攀 渀最䌀氀愀猀猀 搀椀爀攀挀琀椀瘀攀 猀琀椀氀氀 猀甀瀀瀀漀爀琀猀 䌀匀匀㌀ 吀爀愀渀猀椀琀椀漀渀猀⼀䄀渀椀洀愀琀椀漀渀猀 攀瘀攀渀 椀昀 琀栀攀礀 搀漀 渀漀琀 昀漀氀氀漀眀 琀栀攀 渀最䄀渀椀洀愀琀攀 䌀匀匀 渀愀洀椀渀最 猀琀爀甀挀琀甀爀攀⸀ഀഀ
   Upon animation ngAnimate will apply supplementary CSS classes to track the start and end of an animation, but this will not hinder਍   愀渀礀 瀀爀攀ⴀ攀砀椀猀琀椀渀最 䌀匀匀 琀爀愀渀猀椀琀椀漀渀猀 愀氀爀攀愀搀礀 漀渀 琀栀攀 攀氀攀洀攀渀琀⸀ 吀漀 最攀琀 愀渀 椀搀攀愀 漀昀 眀栀愀琀 栀愀瀀瀀攀渀猀 搀甀爀椀渀最 愀 挀氀愀猀猀ⴀ戀愀猀攀搀 愀渀椀洀愀琀椀漀渀Ⰰ 戀攀 猀甀爀攀ഀഀ
   to view the step by step details of {@link ngAnimate.$animate#methods_addclass $animate.addClass} and਍   笀䀀氀椀渀欀 渀最䄀渀椀洀愀琀攀⸀␀愀渀椀洀愀琀攀⌀洀攀琀栀漀搀猀开爀攀洀漀瘀攀挀氀愀猀猀 ␀愀渀椀洀愀琀攀⸀爀攀洀漀瘀攀䌀氀愀猀猀紀⸀ഀഀ
 */਍瘀愀爀 渀最䌀氀愀猀猀䐀椀爀攀挀琀椀瘀攀 㴀 挀氀愀猀猀䐀椀爀攀挀琀椀瘀攀⠀✀✀Ⰰ 琀爀甀攀⤀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䌀氀愀猀猀伀搀搀ഀഀ
 * @restrict AC਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀攀 怀渀最䌀氀愀猀猀伀搀搀怀 愀渀搀 怀渀最䌀氀愀猀猀䔀瘀攀渀怀 搀椀爀攀挀琀椀瘀攀猀 眀漀爀欀 攀砀愀挀琀氀礀 愀猀ഀഀ
 * {@link ng.directive:ngClass ngClass}, except they work in਍ ⨀ 挀漀渀樀甀渀挀琀椀漀渀 眀椀琀栀 怀渀最刀攀瀀攀愀琀怀 愀渀搀 琀愀欀攀 攀昀昀攀挀琀 漀渀氀礀 漀渀 漀搀搀 ⠀攀瘀攀渀⤀ 爀漀眀猀⸀ഀഀ
 *਍ ⨀ 吀栀椀猀 搀椀爀攀挀琀椀瘀攀 挀愀渀 戀攀 愀瀀瀀氀椀攀搀 漀渀氀礀 眀椀琀栀椀渀 琀栀攀 猀挀漀瀀攀 漀昀 愀渀ഀഀ
 * {@link ng.directive:ngRepeat ngRepeat}.਍ ⨀ഀഀ
 * @element ANY਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䌀氀愀猀猀伀搀搀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 琀漀 攀瘀愀氀⸀ 吀栀攀 爀攀猀甀氀琀ഀഀ
 *   of the evaluation can be a string representing space delimited class names or an array.਍ ⨀ഀഀ
 * @example਍   㰀攀砀愀洀瀀氀攀㸀ഀഀ
     <file name="index.html">਍        㰀漀氀 渀最ⴀ椀渀椀琀㴀∀渀愀洀攀猀㴀嬀✀䨀漀栀渀✀Ⰰ ✀䴀愀爀礀✀Ⰰ ✀䌀愀琀攀✀Ⰰ ✀匀甀稀✀崀∀㸀ഀഀ
          <li ng-repeat="name in names">਍           㰀猀瀀愀渀 渀最ⴀ挀氀愀猀猀ⴀ漀搀搀㴀∀✀漀搀搀✀∀ 渀最ⴀ挀氀愀猀猀ⴀ攀瘀攀渀㴀∀✀攀瘀攀渀✀∀㸀ഀഀ
             {{name}}਍           㰀⼀猀瀀愀渀㸀ഀഀ
          </li>਍        㰀⼀漀氀㸀ഀഀ
     </file>਍     㰀昀椀氀攀 渀愀洀攀㴀∀猀琀礀氀攀⸀挀猀猀∀㸀ഀഀ
       .odd {਍         挀漀氀漀爀㨀 爀攀搀㬀ഀഀ
       }਍       ⸀攀瘀攀渀 笀ഀഀ
         color: blue;਍       紀ഀഀ
     </file>਍     㰀昀椀氀攀 渀愀洀攀㴀∀猀挀攀渀愀爀椀漀⸀樀猀∀㸀ഀഀ
       it('should check ng-class-odd and ng-class-even', function() {਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 氀椀㨀昀椀爀猀琀 猀瀀愀渀✀⤀⸀瀀爀漀瀀⠀✀挀氀愀猀猀一愀洀攀✀⤀⤀⸀ഀഀ
           toMatch(/odd/);਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 氀椀㨀氀愀猀琀 猀瀀愀渀✀⤀⸀瀀爀漀瀀⠀✀挀氀愀猀猀一愀洀攀✀⤀⤀⸀ഀഀ
           toMatch(/even/);਍       紀⤀㬀ഀഀ
     </file>਍   㰀⼀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍瘀愀爀 渀最䌀氀愀猀猀伀搀搀䐀椀爀攀挀琀椀瘀攀 㴀 挀氀愀猀猀䐀椀爀攀挀琀椀瘀攀⠀✀伀搀搀✀Ⰰ 　⤀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䌀氀愀猀猀䔀瘀攀渀ഀഀ
 * @restrict AC਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀攀 怀渀最䌀氀愀猀猀伀搀搀怀 愀渀搀 怀渀最䌀氀愀猀猀䔀瘀攀渀怀 搀椀爀攀挀琀椀瘀攀猀 眀漀爀欀 攀砀愀挀琀氀礀 愀猀ഀഀ
 * {@link ng.directive:ngClass ngClass}, except they work in਍ ⨀ 挀漀渀樀甀渀挀琀椀漀渀 眀椀琀栀 怀渀最刀攀瀀攀愀琀怀 愀渀搀 琀愀欀攀 攀昀昀攀挀琀 漀渀氀礀 漀渀 漀搀搀 ⠀攀瘀攀渀⤀ 爀漀眀猀⸀ഀഀ
 *਍ ⨀ 吀栀椀猀 搀椀爀攀挀琀椀瘀攀 挀愀渀 戀攀 愀瀀瀀氀椀攀搀 漀渀氀礀 眀椀琀栀椀渀 琀栀攀 猀挀漀瀀攀 漀昀 愀渀ഀഀ
 * {@link ng.directive:ngRepeat ngRepeat}.਍ ⨀ഀഀ
 * @element ANY਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䌀氀愀猀猀䔀瘀攀渀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 琀漀 攀瘀愀氀⸀ 吀栀攀ഀഀ
 *   result of the evaluation can be a string representing space delimited class names or an array.਍ ⨀ഀഀ
 * @example਍   㰀攀砀愀洀瀀氀攀㸀ഀഀ
     <file name="index.html">਍        㰀漀氀 渀最ⴀ椀渀椀琀㴀∀渀愀洀攀猀㴀嬀✀䨀漀栀渀✀Ⰰ ✀䴀愀爀礀✀Ⰰ ✀䌀愀琀攀✀Ⰰ ✀匀甀稀✀崀∀㸀ഀഀ
          <li ng-repeat="name in names">਍           㰀猀瀀愀渀 渀最ⴀ挀氀愀猀猀ⴀ漀搀搀㴀∀✀漀搀搀✀∀ 渀最ⴀ挀氀愀猀猀ⴀ攀瘀攀渀㴀∀✀攀瘀攀渀✀∀㸀ഀഀ
             {{name}} &nbsp; &nbsp; &nbsp;਍           㰀⼀猀瀀愀渀㸀ഀഀ
          </li>਍        㰀⼀漀氀㸀ഀഀ
     </file>਍     㰀昀椀氀攀 渀愀洀攀㴀∀猀琀礀氀攀⸀挀猀猀∀㸀ഀഀ
       .odd {਍         挀漀氀漀爀㨀 爀攀搀㬀ഀഀ
       }਍       ⸀攀瘀攀渀 笀ഀഀ
         color: blue;਍       紀ഀഀ
     </file>਍     㰀昀椀氀攀 渀愀洀攀㴀∀猀挀攀渀愀爀椀漀⸀樀猀∀㸀ഀഀ
       it('should check ng-class-odd and ng-class-even', function() {਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 氀椀㨀昀椀爀猀琀 猀瀀愀渀✀⤀⸀瀀爀漀瀀⠀✀挀氀愀猀猀一愀洀攀✀⤀⤀⸀ഀഀ
           toMatch(/odd/);਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 氀椀㨀氀愀猀琀 猀瀀愀渀✀⤀⸀瀀爀漀瀀⠀✀挀氀愀猀猀一愀洀攀✀⤀⤀⸀ഀഀ
           toMatch(/even/);਍       紀⤀㬀ഀഀ
     </file>਍   㰀⼀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍瘀愀爀 渀最䌀氀愀猀猀䔀瘀攀渀䐀椀爀攀挀琀椀瘀攀 㴀 挀氀愀猀猀䐀椀爀攀挀琀椀瘀攀⠀✀䔀瘀攀渀✀Ⰰ ㄀⤀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䌀氀漀愀欀ഀഀ
 * @restrict AC਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀攀 怀渀最䌀氀漀愀欀怀 搀椀爀攀挀琀椀瘀攀 椀猀 甀猀攀搀 琀漀 瀀爀攀瘀攀渀琀 琀栀攀 䄀渀最甀氀愀爀 栀琀洀氀 琀攀洀瀀氀愀琀攀 昀爀漀洀 戀攀椀渀最 戀爀椀攀昀氀礀ഀഀ
 * displayed by the browser in its raw (uncompiled) form while your application is loading. Use this਍ ⨀ 搀椀爀攀挀琀椀瘀攀 琀漀 愀瘀漀椀搀 琀栀攀 甀渀搀攀猀椀爀愀戀氀攀 昀氀椀挀欀攀爀 攀昀昀攀挀琀 挀愀甀猀攀搀 戀礀 琀栀攀 栀琀洀氀 琀攀洀瀀氀愀琀攀 搀椀猀瀀氀愀礀⸀ഀഀ
 *਍ ⨀ 吀栀攀 搀椀爀攀挀琀椀瘀攀 挀愀渀 戀攀 愀瀀瀀氀椀攀搀 琀漀 琀栀攀 怀㰀戀漀搀礀㸀怀 攀氀攀洀攀渀琀Ⰰ 戀甀琀 琀栀攀 瀀爀攀昀攀爀爀攀搀 甀猀愀最攀 椀猀 琀漀 愀瀀瀀氀礀ഀഀ
 * multiple `ngCloak` directives to small portions of the page to permit progressive rendering਍ ⨀ 漀昀 琀栀攀 戀爀漀眀猀攀爀 瘀椀攀眀⸀ഀഀ
 *਍ ⨀ 怀渀最䌀氀漀愀欀怀 眀漀爀欀猀 椀渀 挀漀漀瀀攀爀愀琀椀漀渀 眀椀琀栀 琀栀攀 昀漀氀氀漀眀椀渀最 挀猀猀 爀甀氀攀 攀洀戀攀搀搀攀搀 眀椀琀栀椀渀 怀愀渀最甀氀愀爀⸀樀猀怀 愀渀搀ഀഀ
 * `angular.min.js`.਍ ⨀ 䘀漀爀 䌀匀倀 洀漀搀攀 瀀氀攀愀猀攀 愀搀搀 怀愀渀最甀氀愀爀ⴀ挀猀瀀⸀挀猀猀怀 琀漀 礀漀甀爀 栀琀洀氀 昀椀氀攀 ⠀猀攀攀 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䌀猀瀀 渀最䌀猀瀀紀⤀⸀ഀഀ
 *਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 * [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {਍ ⨀   搀椀猀瀀氀愀礀㨀 渀漀渀攀 ℀椀洀瀀漀爀琀愀渀琀㬀ഀഀ
 * }਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 圀栀攀渀 琀栀椀猀 挀猀猀 爀甀氀攀 椀猀 氀漀愀搀攀搀 戀礀 琀栀攀 戀爀漀眀猀攀爀Ⰰ 愀氀氀 栀琀洀氀 攀氀攀洀攀渀琀猀 ⠀椀渀挀氀甀搀椀渀最 琀栀攀椀爀 挀栀椀氀搀爀攀渀⤀ 琀栀愀琀ഀഀ
 * are tagged with the `ngCloak` directive are hidden. When Angular encounters this directive਍ ⨀ 搀甀爀椀渀最 琀栀攀 挀漀洀瀀椀氀愀琀椀漀渀 漀昀 琀栀攀 琀攀洀瀀氀愀琀攀 椀琀 搀攀氀攀琀攀猀 琀栀攀 怀渀最䌀氀漀愀欀怀 攀氀攀洀攀渀琀 愀琀琀爀椀戀甀琀攀Ⰰ 洀愀欀椀渀最ഀഀ
 * the compiled element visible.਍ ⨀ഀഀ
 * For the best result, the `angular.js` script must be loaded in the head section of the html਍ ⨀ 搀漀挀甀洀攀渀琀㬀 愀氀琀攀爀渀愀琀椀瘀攀氀礀Ⰰ 琀栀攀 挀猀猀 爀甀氀攀 愀戀漀瘀攀 洀甀猀琀 戀攀 椀渀挀氀甀搀攀搀 椀渀 琀栀攀 攀砀琀攀爀渀愀氀 猀琀礀氀攀猀栀攀攀琀 漀昀 琀栀攀ഀഀ
 * application.਍ ⨀ഀഀ
 * Legacy browsers, like IE7, do not provide attribute selector support (added in CSS 2.1) so they਍ ⨀ 挀愀渀渀漀琀 洀愀琀挀栀 琀栀攀 怀嬀渀最尀㨀挀氀漀愀欀崀怀 猀攀氀攀挀琀漀爀⸀ 吀漀 眀漀爀欀 愀爀漀甀渀搀 琀栀椀猀 氀椀洀椀琀愀琀椀漀渀Ⰰ 礀漀甀 洀甀猀琀 愀搀搀 琀栀攀 挀猀猀ഀഀ
 * class `ng-cloak` in addition to the `ngCloak` directive as shown in the example below.਍ ⨀ഀഀ
 * @element ANY਍ ⨀ഀഀ
 * @example਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍        㰀搀椀瘀 椀搀㴀∀琀攀洀瀀氀愀琀攀㄀∀ 渀最ⴀ挀氀漀愀欀㸀笀笀 ✀栀攀氀氀漀✀ 紀紀㰀⼀搀椀瘀㸀ഀഀ
        <div id="template2" ng-cloak class="ng-cloak">{{ 'hello IE7' }}</div>਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
     <doc:scenario>਍       椀琀⠀✀猀栀漀甀氀搀 爀攀洀漀瘀攀 琀栀攀 琀攀洀瀀氀愀琀攀 搀椀爀攀挀琀椀瘀攀 愀渀搀 挀猀猀 挀氀愀猀猀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         expect(element('.doc-example-live #template1').attr('ng-cloak')).਍           渀漀琀⠀⤀⸀琀漀䈀攀䐀攀昀椀渀攀搀⠀⤀㬀ഀഀ
         expect(element('.doc-example-live #template2').attr('ng-cloak')).਍           渀漀琀⠀⤀⸀琀漀䈀攀䐀攀昀椀渀攀搀⠀⤀㬀ഀഀ
       });਍     㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
   </doc:example>਍ ⨀ഀഀ
 */਍瘀愀爀 渀最䌀氀漀愀欀䐀椀爀攀挀琀椀瘀攀 㴀 渀最䐀椀爀攀挀琀椀瘀攀⠀笀ഀഀ
  compile: function(element, attr) {਍    愀琀琀爀⸀␀猀攀琀⠀✀渀最䌀氀漀愀欀✀Ⰰ 甀渀搀攀昀椀渀攀搀⤀㬀ഀഀ
    element.removeClass('ng-cloak');਍  紀ഀഀ
});਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngController਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀攀 怀渀最䌀漀渀琀爀漀氀氀攀爀怀 搀椀爀攀挀琀椀瘀攀 愀琀琀愀挀栀攀猀 愀 挀漀渀琀爀漀氀氀攀爀 挀氀愀猀猀 琀漀 琀栀攀 瘀椀攀眀⸀ 吀栀椀猀 椀猀 愀 欀攀礀 愀猀瀀攀挀琀 漀昀 栀漀眀 愀渀最甀氀愀爀ഀഀ
 * supports the principles behind the Model-View-Controller design pattern.਍ ⨀ഀഀ
 * MVC components in angular:਍ ⨀ഀഀ
 * * Model �� The Model is scope properties; scopes are attached to the DOM where scope properties਍ ⨀   愀爀攀 愀挀挀攀猀猀攀搀 琀栀爀漀甀最栀 戀椀渀搀椀渀最猀⸀ഀഀ
 * * View �� The template (HTML with data bindings) that is rendered into the View.਍ ⨀ ⨀ 䌀漀渀琀爀漀氀氀攀爀 ﴀ﷿⃿吀栀攀 怀渀最䌀漀渀琀爀漀氀氀攀爀怀 搀椀爀攀挀琀椀瘀攀 猀瀀攀挀椀昀椀攀猀 愀 䌀漀渀琀爀漀氀氀攀爀 挀氀愀猀猀㬀 琀栀攀 挀氀愀猀猀 挀漀渀琀愀椀渀猀 戀甀猀椀渀攀猀猀ഀഀ
 *   logic behind the application to decorate the scope with functions and values਍ ⨀ഀഀ
 * Note that you can also attach controllers to the DOM by declaring it in a route definition਍ ⨀ 瘀椀愀 琀栀攀 笀䀀氀椀渀欀 渀最刀漀甀琀攀⸀␀爀漀甀琀攀 ␀爀漀甀琀攀紀 猀攀爀瘀椀挀攀⸀ 䄀 挀漀洀洀漀渀 洀椀猀琀愀欀攀 椀猀 琀漀 搀攀挀氀愀爀攀 琀栀攀 挀漀渀琀爀漀氀氀攀爀ഀഀ
 * again using `ng-controller` in the template itself.  This will cause the controller to be attached਍ ⨀ 愀渀搀 攀砀攀挀甀琀攀搀 琀眀椀挀攀⸀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䄀一夀ഀഀ
 * @scope਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䌀漀渀琀爀漀氀氀攀爀 一愀洀攀 漀昀 愀 最氀漀戀愀氀氀礀 愀挀挀攀猀猀椀戀氀攀 挀漀渀猀琀爀甀挀琀漀爀 昀甀渀挀琀椀漀渀 漀爀 愀渀ഀഀ
 *     {@link guide/expression expression} that on the current scope evaluates to a਍ ⨀     挀漀渀猀琀爀甀挀琀漀爀 昀甀渀挀琀椀漀渀⸀ 吀栀攀 挀漀渀琀爀漀氀氀攀爀 椀渀猀琀愀渀挀攀 挀愀渀 戀攀 瀀甀戀氀椀猀栀攀搀 椀渀琀漀 愀 猀挀漀瀀攀 瀀爀漀瀀攀爀琀礀ഀഀ
 *     by specifying `as propertyName`.਍ ⨀ഀഀ
 * @example਍ ⨀ 䠀攀爀攀 椀猀 愀 猀椀洀瀀氀攀 昀漀爀洀 昀漀爀 攀搀椀琀椀渀最 甀猀攀爀 挀漀渀琀愀挀琀 椀渀昀漀爀洀愀琀椀漀渀⸀ 䄀搀搀椀渀最Ⰰ 爀攀洀漀瘀椀渀最Ⰰ 挀氀攀愀爀椀渀最Ⰰ 愀渀搀ഀഀ
 * greeting are methods declared on the controller (see source tab). These methods can਍ ⨀ 攀愀猀椀氀礀 戀攀 挀愀氀氀攀搀 昀爀漀洀 琀栀攀 愀渀最甀氀愀爀 洀愀爀欀甀瀀⸀ 一漀琀椀挀攀 琀栀愀琀 琀栀攀 猀挀漀瀀攀 戀攀挀漀洀攀猀 琀栀攀 怀琀栀椀猀怀 昀漀爀 琀栀攀ഀഀ
 * controller's instance. This allows for easy access to the view data from the controller. Also਍ ⨀ 渀漀琀椀挀攀 琀栀愀琀 愀渀礀 挀栀愀渀最攀猀 琀漀 琀栀攀 搀愀琀愀 愀爀攀 愀甀琀漀洀愀琀椀挀愀氀氀礀 爀攀昀氀攀挀琀攀搀 椀渀 琀栀攀 嘀椀攀眀 眀椀琀栀漀甀琀 琀栀攀 渀攀攀搀ഀഀ
 * for a manual update. The example is shown in two different declaration styles you may use਍ ⨀ 愀挀挀漀爀搀椀渀最 琀漀 瀀爀攀昀攀爀攀渀挀攀⸀ഀഀ
   <doc:example>਍     㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      <script>਍        昀甀渀挀琀椀漀渀 匀攀琀琀椀渀最猀䌀漀渀琀爀漀氀氀攀爀㄀⠀⤀ 笀ഀഀ
          this.name = "John Smith";਍          琀栀椀猀⸀挀漀渀琀愀挀琀猀 㴀 嬀ഀഀ
            {type: 'phone', value: '408 555 1212'},਍            笀琀礀瀀攀㨀 ✀攀洀愀椀氀✀Ⰰ 瘀愀氀甀攀㨀 ✀樀漀栀渀⸀猀洀椀琀栀䀀攀砀愀洀瀀氀攀⸀漀爀最✀紀 崀㬀ഀഀ
          };਍ഀഀ
        SettingsController1.prototype.greet = function() {਍          愀氀攀爀琀⠀琀栀椀猀⸀渀愀洀攀⤀㬀ഀഀ
        };਍ഀഀ
        SettingsController1.prototype.addContact = function() {਍          琀栀椀猀⸀挀漀渀琀愀挀琀猀⸀瀀甀猀栀⠀笀琀礀瀀攀㨀 ✀攀洀愀椀氀✀Ⰰ 瘀愀氀甀攀㨀 ✀礀漀甀爀渀愀洀攀䀀攀砀愀洀瀀氀攀⸀漀爀最✀紀⤀㬀ഀഀ
        };਍ഀഀ
        SettingsController1.prototype.removeContact = function(contactToRemove) {਍         瘀愀爀 椀渀搀攀砀 㴀 琀栀椀猀⸀挀漀渀琀愀挀琀猀⸀椀渀搀攀砀伀昀⠀挀漀渀琀愀挀琀吀漀刀攀洀漀瘀攀⤀㬀ഀഀ
          this.contacts.splice(index, 1);਍        紀㬀ഀഀ
਍        匀攀琀琀椀渀最猀䌀漀渀琀爀漀氀氀攀爀㄀⸀瀀爀漀琀漀琀礀瀀攀⸀挀氀攀愀爀䌀漀渀琀愀挀琀 㴀 昀甀渀挀琀椀漀渀⠀挀漀渀琀愀挀琀⤀ 笀ഀഀ
          contact.type = 'phone';਍          挀漀渀琀愀挀琀⸀瘀愀氀甀攀 㴀 ✀✀㬀ഀഀ
        };਍      㰀⼀猀挀爀椀瀀琀㸀ഀഀ
      <div id="ctrl-as-exmpl" ng-controller="SettingsController1 as settings">਍        一愀洀攀㨀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀琀攀砀琀∀ 渀最ⴀ洀漀搀攀氀㴀∀猀攀琀琀椀渀最猀⸀渀愀洀攀∀⼀㸀ഀഀ
        [ <a href="" ng-click="settings.greet()">greet</a> ]<br/>਍        䌀漀渀琀愀挀琀㨀ഀഀ
        <ul>਍          㰀氀椀 渀最ⴀ爀攀瀀攀愀琀㴀∀挀漀渀琀愀挀琀 椀渀 猀攀琀琀椀渀最猀⸀挀漀渀琀愀挀琀猀∀㸀ഀഀ
            <select ng-model="contact.type">਍               㰀漀瀀琀椀漀渀㸀瀀栀漀渀攀㰀⼀漀瀀琀椀漀渀㸀ഀഀ
               <option>email</option>਍            㰀⼀猀攀氀攀挀琀㸀ഀഀ
            <input type="text" ng-model="contact.value"/>਍            嬀 㰀愀 栀爀攀昀㴀∀∀ 渀最ⴀ挀氀椀挀欀㴀∀猀攀琀琀椀渀最猀⸀挀氀攀愀爀䌀漀渀琀愀挀琀⠀挀漀渀琀愀挀琀⤀∀㸀挀氀攀愀爀㰀⼀愀㸀ഀഀ
            | <a href="" ng-click="settings.removeContact(contact)">X</a> ]਍          㰀⼀氀椀㸀ഀഀ
          <li>[ <a href="" ng-click="settings.addContact()">add</a> ]</li>਍       㰀⼀甀氀㸀ഀഀ
      </div>਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
     <doc:scenario>਍       椀琀⠀✀猀栀漀甀氀搀 挀栀攀挀欀 挀漀渀琀爀漀氀氀攀爀 愀猀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         expect(element('#ctrl-as-exmpl>:input').val()).toBe('John Smith');਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⌀挀琀爀氀ⴀ愀猀ⴀ攀砀洀瀀氀 氀椀㨀渀琀栀ⴀ挀栀椀氀搀⠀㄀⤀ 椀渀瀀甀琀✀⤀⸀瘀愀氀⠀⤀⤀ഀഀ
           .toBe('408 555 1212');਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⌀挀琀爀氀ⴀ愀猀ⴀ攀砀洀瀀氀 氀椀㨀渀琀栀ⴀ挀栀椀氀搀⠀㈀⤀ 椀渀瀀甀琀✀⤀⸀瘀愀氀⠀⤀⤀ഀഀ
           .toBe('john.smith@example.org');਍ഀഀ
         element('#ctrl-as-exmpl li:first a:contains("clear")').click();਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⌀挀琀爀氀ⴀ愀猀ⴀ攀砀洀瀀氀 氀椀㨀昀椀爀猀琀 椀渀瀀甀琀✀⤀⸀瘀愀氀⠀⤀⤀⸀琀漀䈀攀⠀✀✀⤀㬀ഀഀ
਍         攀氀攀洀攀渀琀⠀✀⌀挀琀爀氀ⴀ愀猀ⴀ攀砀洀瀀氀 氀椀㨀氀愀猀琀 愀㨀挀漀渀琀愀椀渀猀⠀∀愀搀搀∀⤀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
         expect(element('#ctrl-as-exmpl li:nth-child(3) input').val())਍           ⸀琀漀䈀攀⠀✀礀漀甀爀渀愀洀攀䀀攀砀愀洀瀀氀攀⸀漀爀最✀⤀㬀ഀഀ
       });਍     㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
   </doc:example>਍    㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍      㰀猀挀爀椀瀀琀㸀ഀഀ
        function SettingsController2($scope) {਍          ␀猀挀漀瀀攀⸀渀愀洀攀 㴀 ∀䨀漀栀渀 匀洀椀琀栀∀㬀ഀഀ
          $scope.contacts = [਍            笀琀礀瀀攀㨀✀瀀栀漀渀攀✀Ⰰ 瘀愀氀甀攀㨀✀㐀　㠀 㔀㔀㔀 ㄀㈀㄀㈀✀紀Ⰰഀഀ
            {type:'email', value:'john.smith@example.org'} ];਍ഀഀ
          $scope.greet = function() {਍           愀氀攀爀琀⠀琀栀椀猀⸀渀愀洀攀⤀㬀ഀഀ
          };਍ഀഀ
          $scope.addContact = function() {਍           琀栀椀猀⸀挀漀渀琀愀挀琀猀⸀瀀甀猀栀⠀笀琀礀瀀攀㨀✀攀洀愀椀氀✀Ⰰ 瘀愀氀甀攀㨀✀礀漀甀爀渀愀洀攀䀀攀砀愀洀瀀氀攀⸀漀爀最✀紀⤀㬀ഀഀ
          };਍ഀഀ
          $scope.removeContact = function(contactToRemove) {਍           瘀愀爀 椀渀搀攀砀 㴀 琀栀椀猀⸀挀漀渀琀愀挀琀猀⸀椀渀搀攀砀伀昀⠀挀漀渀琀愀挀琀吀漀刀攀洀漀瘀攀⤀㬀ഀഀ
           this.contacts.splice(index, 1);਍          紀㬀ഀഀ
਍          ␀猀挀漀瀀攀⸀挀氀攀愀爀䌀漀渀琀愀挀琀 㴀 昀甀渀挀琀椀漀渀⠀挀漀渀琀愀挀琀⤀ 笀ഀഀ
           contact.type = 'phone';਍           挀漀渀琀愀挀琀⸀瘀愀氀甀攀 㴀 ✀✀㬀ഀഀ
          };਍        紀ഀഀ
      </script>਍      㰀搀椀瘀 椀搀㴀∀挀琀爀氀ⴀ攀砀洀瀀氀∀ 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀匀攀琀琀椀渀最猀䌀漀渀琀爀漀氀氀攀爀㈀∀㸀ഀഀ
        Name: <input type="text" ng-model="name"/>਍        嬀 㰀愀 栀爀攀昀㴀∀∀ 渀最ⴀ挀氀椀挀欀㴀∀最爀攀攀琀⠀⤀∀㸀最爀攀攀琀㰀⼀愀㸀 崀㰀戀爀⼀㸀ഀഀ
        Contact:਍        㰀甀氀㸀ഀഀ
          <li ng-repeat="contact in contacts">਍            㰀猀攀氀攀挀琀 渀最ⴀ洀漀搀攀氀㴀∀挀漀渀琀愀挀琀⸀琀礀瀀攀∀㸀ഀഀ
               <option>phone</option>਍               㰀漀瀀琀椀漀渀㸀攀洀愀椀氀㰀⼀漀瀀琀椀漀渀㸀ഀഀ
            </select>਍            㰀椀渀瀀甀琀 琀礀瀀攀㴀∀琀攀砀琀∀ 渀最ⴀ洀漀搀攀氀㴀∀挀漀渀琀愀挀琀⸀瘀愀氀甀攀∀⼀㸀ഀഀ
            [ <a href="" ng-click="clearContact(contact)">clear</a>਍            簀 㰀愀 栀爀攀昀㴀∀∀ 渀最ⴀ挀氀椀挀欀㴀∀爀攀洀漀瘀攀䌀漀渀琀愀挀琀⠀挀漀渀琀愀挀琀⤀∀㸀堀㰀⼀愀㸀 崀ഀഀ
          </li>਍          㰀氀椀㸀嬀 㰀愀 栀爀攀昀㴀∀∀ 渀最ⴀ挀氀椀挀欀㴀∀愀搀搀䌀漀渀琀愀挀琀⠀⤀∀㸀愀搀搀㰀⼀愀㸀 崀㰀⼀氀椀㸀ഀഀ
       </ul>਍      㰀⼀搀椀瘀㸀ഀഀ
     </doc:source>਍     㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
       it('should check controller', function() {਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⌀挀琀爀氀ⴀ攀砀洀瀀氀㸀㨀椀渀瀀甀琀✀⤀⸀瘀愀氀⠀⤀⤀⸀琀漀䈀攀⠀✀䨀漀栀渀 匀洀椀琀栀✀⤀㬀ഀഀ
         expect(element('#ctrl-exmpl li:nth-child(1) input').val())਍           ⸀琀漀䈀攀⠀✀㐀　㠀 㔀㔀㔀 ㄀㈀㄀㈀✀⤀㬀ഀഀ
         expect(element('#ctrl-exmpl li:nth-child(2) input').val())਍           ⸀琀漀䈀攀⠀✀樀漀栀渀⸀猀洀椀琀栀䀀攀砀愀洀瀀氀攀⸀漀爀最✀⤀㬀ഀഀ
਍         攀氀攀洀攀渀琀⠀✀⌀挀琀爀氀ⴀ攀砀洀瀀氀 氀椀㨀昀椀爀猀琀 愀㨀挀漀渀琀愀椀渀猀⠀∀挀氀攀愀爀∀⤀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
         expect(element('#ctrl-exmpl li:first input').val()).toBe('');਍ഀഀ
         element('#ctrl-exmpl li:last a:contains("add")').click();਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⌀挀琀爀氀ⴀ攀砀洀瀀氀 氀椀㨀渀琀栀ⴀ挀栀椀氀搀⠀㌀⤀ 椀渀瀀甀琀✀⤀⸀瘀愀氀⠀⤀⤀ഀഀ
           .toBe('yourname@example.org');਍       紀⤀㬀ഀഀ
     </doc:scenario>਍   㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
਍ ⨀⼀ഀഀ
var ngControllerDirective = [function() {਍  爀攀琀甀爀渀 笀ഀഀ
    scope: true,਍    挀漀渀琀爀漀氀氀攀爀㨀 ✀䀀✀Ⰰഀഀ
    priority: 500਍  紀㬀ഀഀ
}];਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngCsp਍ ⨀ഀഀ
 * @element html਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Enables [CSP (Content Security Policy)](https://developer.mozilla.org/en/Security/CSP) support.਍ ⨀ഀഀ
 * This is necessary when developing things like Google Chrome Extensions.਍ ⨀ഀഀ
 * CSP forbids apps to use `eval` or `Function(string)` generated functions (among other things).਍ ⨀ 䘀漀爀 甀猀 琀漀 戀攀 挀漀洀瀀愀琀椀戀氀攀Ⰰ 眀攀 樀甀猀琀 渀攀攀搀 琀漀 椀洀瀀氀攀洀攀渀琀 琀栀攀 ∀最攀琀琀攀爀䘀渀∀ 椀渀 ␀瀀愀爀猀攀 眀椀琀栀漀甀琀 瘀椀漀氀愀琀椀渀最ഀഀ
 * any of these restrictions.਍ ⨀ഀഀ
 * AngularJS uses `Function(string)` generated functions as a speed optimization. Applying the `ngCsp`਍ ⨀ 搀椀爀攀挀琀椀瘀攀 眀椀氀氀 挀愀甀猀攀 䄀渀最甀氀愀爀 琀漀 甀猀攀 䌀匀倀 挀漀洀瀀愀琀椀戀椀氀椀琀礀 洀漀搀攀⸀ 圀栀攀渀 琀栀椀猀 洀漀搀攀 椀猀 漀渀 䄀渀最甀氀愀爀䨀匀 眀椀氀氀ഀഀ
 * evaluate all expressions up to 30% slower than in non-CSP mode, but no security violations will਍ ⨀ 戀攀 爀愀椀猀攀搀⸀ഀഀ
 *਍ ⨀ 䌀匀倀 昀漀爀戀椀搀猀 䨀愀瘀愀匀挀爀椀瀀琀 琀漀 椀渀氀椀渀攀 猀琀礀氀攀猀栀攀攀琀 爀甀氀攀猀⸀ 䤀渀 渀漀渀 䌀匀倀 洀漀搀攀 䄀渀最甀氀愀爀 愀甀琀漀洀愀琀椀挀愀氀氀礀ഀഀ
 * includes some CSS rules (e.g. {@link ng.directive:ngCloak ngCloak}).਍ ⨀ 吀漀 洀愀欀攀 琀栀漀猀攀 搀椀爀攀挀琀椀瘀攀猀 眀漀爀欀 椀渀 䌀匀倀 洀漀搀攀Ⰰ 椀渀挀氀甀搀攀 琀栀攀 怀愀渀最甀氀愀爀ⴀ挀猀瀀⸀挀猀猀怀 洀愀渀甀愀氀氀礀⸀ഀഀ
 *਍ ⨀ 䤀渀 漀爀搀攀爀 琀漀 甀猀攀 琀栀椀猀 昀攀愀琀甀爀攀 瀀甀琀 琀栀攀 怀渀最䌀猀瀀怀 搀椀爀攀挀琀椀瘀攀 漀渀 琀栀攀 爀漀漀琀 攀氀攀洀攀渀琀 漀昀 琀栀攀 愀瀀瀀氀椀挀愀琀椀漀渀⸀ഀഀ
 *਍ ⨀ ⨀一漀琀攀㨀 吀栀椀猀 搀椀爀攀挀琀椀瘀攀 椀猀 漀渀氀礀 愀瘀愀椀氀愀戀氀攀 椀渀 琀栀攀 怀渀最ⴀ挀猀瀀怀 愀渀搀 怀搀愀琀愀ⴀ渀最ⴀ挀猀瀀怀 愀琀琀爀椀戀甀琀攀 昀漀爀洀⸀⨀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
 * This example shows how to apply the `ngCsp` directive to the `html` tag.਍   㰀瀀爀攀㸀ഀഀ
     <!doctype html>਍     㰀栀琀洀氀 渀最ⴀ愀瀀瀀 渀最ⴀ挀猀瀀㸀ഀഀ
     ...਍     ⸀⸀⸀ഀഀ
     </html>਍   㰀⼀瀀爀攀㸀ഀഀ
 */਍ഀഀ
// ngCsp is not implemented as a proper directive any more, because we need it be processed while we bootstrap਍⼀⼀ 琀栀攀 猀礀猀琀攀洀 ⠀戀攀昀漀爀攀 ␀瀀愀爀猀攀 椀猀 椀渀猀琀愀渀琀椀愀琀攀搀⤀Ⰰ 昀漀爀 琀栀椀猀 爀攀愀猀漀渀 眀攀 樀甀猀琀 栀愀瘀攀 愀 挀猀瀀⠀⤀ 昀渀 琀栀愀琀 氀漀漀欀猀 昀漀爀 渀最ⴀ挀猀瀀 愀琀琀爀椀戀甀琀攀ഀഀ
// anywhere in the current doc਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngClick਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀攀 渀最䌀氀椀挀欀 搀椀爀攀挀琀椀瘀攀 愀氀氀漀眀猀 礀漀甀 琀漀 猀瀀攀挀椀昀礀 挀甀猀琀漀洀 戀攀栀愀瘀椀漀爀 眀栀攀渀ഀഀ
 * an element is clicked.਍ ⨀ഀഀ
 * @element ANY਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䌀氀椀挀欀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 琀漀 攀瘀愀氀甀愀琀攀 甀瀀漀渀ഀഀ
 * click. (Event object is available as `$event`)਍ ⨀ഀഀ
 * @example਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍      㰀戀甀琀琀漀渀 渀最ⴀ挀氀椀挀欀㴀∀挀漀甀渀琀 㴀 挀漀甀渀琀 ⬀ ㄀∀ 渀最ⴀ椀渀椀琀㴀∀挀漀甀渀琀㴀　∀㸀ഀഀ
        Increment਍      㰀⼀戀甀琀琀漀渀㸀ഀഀ
      count: {{count}}਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
     <doc:protractor>਍       椀琀⠀✀猀栀漀甀氀搀 挀栀攀挀欀 渀最ⴀ挀氀椀挀欀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         expect(element(by.binding('count')).getText()).toMatch('0');਍         攀氀攀洀攀渀琀⠀戀礀⸀挀猀猀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 戀甀琀琀漀渀✀⤀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
         expect(element(by.binding('count')).getText()).toMatch('1');਍       紀⤀㬀ഀഀ
     </doc:protractor>਍   㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍⼀⨀ഀഀ
 * A directive that allows creation of custom onclick handlers that are defined as angular਍ ⨀ 攀砀瀀爀攀猀猀椀漀渀猀 愀渀搀 愀爀攀 挀漀洀瀀椀氀攀搀 愀渀搀 攀砀攀挀甀琀攀搀 眀椀琀栀椀渀 琀栀攀 挀甀爀爀攀渀琀 猀挀漀瀀攀⸀ഀഀ
 *਍ ⨀ 䔀瘀攀渀琀猀 琀栀愀琀 愀爀攀 栀愀渀搀氀攀搀 瘀椀愀 琀栀攀猀攀 栀愀渀搀氀攀爀 愀爀攀 愀氀眀愀礀猀 挀漀渀昀椀最甀爀攀搀 渀漀琀 琀漀 瀀爀漀瀀愀最愀琀攀 昀甀爀琀栀攀爀⸀ഀഀ
 */਍瘀愀爀 渀最䔀瘀攀渀琀䐀椀爀攀挀琀椀瘀攀猀 㴀 笀紀㬀ഀഀ
forEach(਍  ✀挀氀椀挀欀 搀戀氀挀氀椀挀欀 洀漀甀猀攀搀漀眀渀 洀漀甀猀攀甀瀀 洀漀甀猀攀漀瘀攀爀 洀漀甀猀攀漀甀琀 洀漀甀猀攀洀漀瘀攀 洀漀甀猀攀攀渀琀攀爀 洀漀甀猀攀氀攀愀瘀攀 欀攀礀搀漀眀渀 欀攀礀甀瀀 欀攀礀瀀爀攀猀猀 猀甀戀洀椀琀 昀漀挀甀猀 戀氀甀爀 挀漀瀀礀 挀甀琀 瀀愀猀琀攀✀⸀猀瀀氀椀琀⠀✀ ✀⤀Ⰰഀഀ
  function(name) {਍    瘀愀爀 搀椀爀攀挀琀椀瘀攀一愀洀攀 㴀 搀椀爀攀挀琀椀瘀攀一漀爀洀愀氀椀稀攀⠀✀渀最ⴀ✀ ⬀ 渀愀洀攀⤀㬀ഀഀ
    ngEventDirectives[directiveName] = ['$parse', function($parse) {਍      爀攀琀甀爀渀 笀ഀഀ
        compile: function($element, attr) {਍          瘀愀爀 昀渀 㴀 ␀瀀愀爀猀攀⠀愀琀琀爀嬀搀椀爀攀挀琀椀瘀攀一愀洀攀崀⤀㬀ഀഀ
          return function(scope, element, attr) {਍            攀氀攀洀攀渀琀⸀漀渀⠀氀漀眀攀爀挀愀猀攀⠀渀愀洀攀⤀Ⰰ 昀甀渀挀琀椀漀渀⠀攀瘀攀渀琀⤀ 笀ഀഀ
              scope.$apply(function() {਍                昀渀⠀猀挀漀瀀攀Ⰰ 笀␀攀瘀攀渀琀㨀攀瘀攀渀琀紀⤀㬀ഀഀ
              });਍            紀⤀㬀ഀഀ
          };਍        紀ഀഀ
      };਍    紀崀㬀ഀഀ
  }਍⤀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䐀戀氀挀氀椀挀欀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * The `ngDblclick` directive allows you to specify custom behavior on a dblclick event.਍ ⨀ഀഀ
 * @element ANY਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䐀戀氀挀氀椀挀欀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 琀漀 攀瘀愀氀甀愀琀攀 甀瀀漀渀ഀഀ
 * a dblclick. (The Event object is available as `$event`)਍ ⨀ഀഀ
 * @example਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍      㰀戀甀琀琀漀渀 渀最ⴀ搀戀氀挀氀椀挀欀㴀∀挀漀甀渀琀 㴀 挀漀甀渀琀 ⬀ ㄀∀ 渀最ⴀ椀渀椀琀㴀∀挀漀甀渀琀㴀　∀㸀ഀഀ
        Increment (on double click)਍      㰀⼀戀甀琀琀漀渀㸀ഀഀ
      count: {{count}}਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
   </doc:example>਍ ⨀⼀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngMousedown਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀攀 渀最䴀漀甀猀攀搀漀眀渀 搀椀爀攀挀琀椀瘀攀 愀氀氀漀眀猀 礀漀甀 琀漀 猀瀀攀挀椀昀礀 挀甀猀琀漀洀 戀攀栀愀瘀椀漀爀 漀渀 洀漀甀猀攀搀漀眀渀 攀瘀攀渀琀⸀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䄀一夀ഀഀ
 * @param {expression} ngMousedown {@link guide/expression Expression} to evaluate upon਍ ⨀ 洀漀甀猀攀搀漀眀渀⸀ ⠀䔀瘀攀渀琀 漀戀樀攀挀琀 椀猀 愀瘀愀椀氀愀戀氀攀 愀猀 怀␀攀瘀攀渀琀怀⤀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
   <doc:example>਍     㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      <button ng-mousedown="count = count + 1" ng-init="count=0">਍        䤀渀挀爀攀洀攀渀琀 ⠀漀渀 洀漀甀猀攀 搀漀眀渀⤀ഀഀ
      </button>਍      挀漀甀渀琀㨀 笀笀挀漀甀渀琀紀紀ഀഀ
     </doc:source>਍   㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䴀漀甀猀攀甀瀀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Specify custom behavior on mouseup event.਍ ⨀ഀഀ
 * @element ANY਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䴀漀甀猀攀甀瀀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 琀漀 攀瘀愀氀甀愀琀攀 甀瀀漀渀ഀഀ
 * mouseup. (Event object is available as `$event`)਍ ⨀ഀഀ
 * @example਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍      㰀戀甀琀琀漀渀 渀最ⴀ洀漀甀猀攀甀瀀㴀∀挀漀甀渀琀 㴀 挀漀甀渀琀 ⬀ ㄀∀ 渀最ⴀ椀渀椀琀㴀∀挀漀甀渀琀㴀　∀㸀ഀഀ
        Increment (on mouse up)਍      㰀⼀戀甀琀琀漀渀㸀ഀഀ
      count: {{count}}਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
   </doc:example>਍ ⨀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䴀漀甀猀攀漀瘀攀爀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Specify custom behavior on mouseover event.਍ ⨀ഀഀ
 * @element ANY਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䴀漀甀猀攀漀瘀攀爀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 琀漀 攀瘀愀氀甀愀琀攀 甀瀀漀渀ഀഀ
 * mouseover. (Event object is available as `$event`)਍ ⨀ഀഀ
 * @example਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍      㰀戀甀琀琀漀渀 渀最ⴀ洀漀甀猀攀漀瘀攀爀㴀∀挀漀甀渀琀 㴀 挀漀甀渀琀 ⬀ ㄀∀ 渀最ⴀ椀渀椀琀㴀∀挀漀甀渀琀㴀　∀㸀ഀഀ
        Increment (when mouse is over)਍      㰀⼀戀甀琀琀漀渀㸀ഀഀ
      count: {{count}}਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
   </doc:example>਍ ⨀⼀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngMouseenter਍ ⨀ഀഀ
 * @description਍ ⨀ 匀瀀攀挀椀昀礀 挀甀猀琀漀洀 戀攀栀愀瘀椀漀爀 漀渀 洀漀甀猀攀攀渀琀攀爀 攀瘀攀渀琀⸀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䄀一夀ഀഀ
 * @param {expression} ngMouseenter {@link guide/expression Expression} to evaluate upon਍ ⨀ 洀漀甀猀攀攀渀琀攀爀⸀ ⠀䔀瘀攀渀琀 漀戀樀攀挀琀 椀猀 愀瘀愀椀氀愀戀氀攀 愀猀 怀␀攀瘀攀渀琀怀⤀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
   <doc:example>਍     㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      <button ng-mouseenter="count = count + 1" ng-init="count=0">਍        䤀渀挀爀攀洀攀渀琀 ⠀眀栀攀渀 洀漀甀猀攀 攀渀琀攀爀猀⤀ഀഀ
      </button>਍      挀漀甀渀琀㨀 笀笀挀漀甀渀琀紀紀ഀഀ
     </doc:source>਍   㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䴀漀甀猀攀氀攀愀瘀攀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Specify custom behavior on mouseleave event.਍ ⨀ഀഀ
 * @element ANY਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䴀漀甀猀攀氀攀愀瘀攀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 琀漀 攀瘀愀氀甀愀琀攀 甀瀀漀渀ഀഀ
 * mouseleave. (Event object is available as `$event`)਍ ⨀ഀഀ
 * @example਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍      㰀戀甀琀琀漀渀 渀最ⴀ洀漀甀猀攀氀攀愀瘀攀㴀∀挀漀甀渀琀 㴀 挀漀甀渀琀 ⬀ ㄀∀ 渀最ⴀ椀渀椀琀㴀∀挀漀甀渀琀㴀　∀㸀ഀഀ
        Increment (when mouse leaves)਍      㰀⼀戀甀琀琀漀渀㸀ഀഀ
      count: {{count}}਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
   </doc:example>਍ ⨀⼀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngMousemove਍ ⨀ഀഀ
 * @description਍ ⨀ 匀瀀攀挀椀昀礀 挀甀猀琀漀洀 戀攀栀愀瘀椀漀爀 漀渀 洀漀甀猀攀洀漀瘀攀 攀瘀攀渀琀⸀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䄀一夀ഀഀ
 * @param {expression} ngMousemove {@link guide/expression Expression} to evaluate upon਍ ⨀ 洀漀甀猀攀洀漀瘀攀⸀ ⠀䔀瘀攀渀琀 漀戀樀攀挀琀 椀猀 愀瘀愀椀氀愀戀氀攀 愀猀 怀␀攀瘀攀渀琀怀⤀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
   <doc:example>਍     㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      <button ng-mousemove="count = count + 1" ng-init="count=0">਍        䤀渀挀爀攀洀攀渀琀 ⠀眀栀攀渀 洀漀甀猀攀 洀漀瘀攀猀⤀ഀഀ
      </button>਍      挀漀甀渀琀㨀 笀笀挀漀甀渀琀紀紀ഀഀ
     </doc:source>਍   㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䬀攀礀搀漀眀渀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Specify custom behavior on keydown event.਍ ⨀ഀഀ
 * @element ANY਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䬀攀礀搀漀眀渀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 琀漀 攀瘀愀氀甀愀琀攀 甀瀀漀渀ഀഀ
 * keydown. (Event object is available as `$event` and can be interrogated for keyCode, altKey, etc.)਍ ⨀ഀഀ
 * @example਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍      㰀椀渀瀀甀琀 渀最ⴀ欀攀礀搀漀眀渀㴀∀挀漀甀渀琀 㴀 挀漀甀渀琀 ⬀ ㄀∀ 渀最ⴀ椀渀椀琀㴀∀挀漀甀渀琀㴀　∀㸀ഀഀ
      key down count: {{count}}਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
   </doc:example>਍ ⨀⼀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngKeyup਍ ⨀ഀഀ
 * @description਍ ⨀ 匀瀀攀挀椀昀礀 挀甀猀琀漀洀 戀攀栀愀瘀椀漀爀 漀渀 欀攀礀甀瀀 攀瘀攀渀琀⸀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䄀一夀ഀഀ
 * @param {expression} ngKeyup {@link guide/expression Expression} to evaluate upon਍ ⨀ 欀攀礀甀瀀⸀ ⠀䔀瘀攀渀琀 漀戀樀攀挀琀 椀猀 愀瘀愀椀氀愀戀氀攀 愀猀 怀␀攀瘀攀渀琀怀 愀渀搀 挀愀渀 戀攀 椀渀琀攀爀爀漀最愀琀攀搀 昀漀爀 欀攀礀䌀漀搀攀Ⰰ 愀氀琀䬀攀礀Ⰰ 攀琀挀⸀⤀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
   <doc:example>਍     㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      <input ng-keyup="count = count + 1" ng-init="count=0">਍      欀攀礀 甀瀀 挀漀甀渀琀㨀 笀笀挀漀甀渀琀紀紀ഀഀ
     </doc:source>਍   㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䬀攀礀瀀爀攀猀猀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Specify custom behavior on keypress event.਍ ⨀ഀഀ
 * @element ANY਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䬀攀礀瀀爀攀猀猀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 琀漀 攀瘀愀氀甀愀琀攀 甀瀀漀渀ഀഀ
 * keypress. (Event object is available as `$event` and can be interrogated for keyCode, altKey, etc.)਍ ⨀ഀഀ
 * @example਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍      㰀椀渀瀀甀琀 渀最ⴀ欀攀礀瀀爀攀猀猀㴀∀挀漀甀渀琀 㴀 挀漀甀渀琀 ⬀ ㄀∀ 渀最ⴀ椀渀椀琀㴀∀挀漀甀渀琀㴀　∀㸀ഀഀ
      key press count: {{count}}਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
   </doc:example>਍ ⨀⼀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngSubmit਍ ⨀ഀഀ
 * @description਍ ⨀ 䔀渀愀戀氀攀猀 戀椀渀搀椀渀最 愀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀猀 琀漀 漀渀猀甀戀洀椀琀 攀瘀攀渀琀猀⸀ഀഀ
 *਍ ⨀ 䄀搀搀椀琀椀漀渀愀氀氀礀 椀琀 瀀爀攀瘀攀渀琀猀 琀栀攀 搀攀昀愀甀氀琀 愀挀琀椀漀渀 ⠀眀栀椀挀栀 昀漀爀 昀漀爀洀 洀攀愀渀猀 猀攀渀搀椀渀最 琀栀攀 爀攀焀甀攀猀琀 琀漀 琀栀攀ഀഀ
 * server and reloading the current page) **but only if the form does not contain an `action`਍ ⨀ 愀琀琀爀椀戀甀琀攀⨀⨀⸀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 昀漀爀洀ഀഀ
 * @param {expression} ngSubmit {@link guide/expression Expression} to eval. (Event object is available as `$event`)਍ ⨀ഀഀ
 * @example਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍      㰀猀挀爀椀瀀琀㸀ഀഀ
        function Ctrl($scope) {਍          ␀猀挀漀瀀攀⸀氀椀猀琀 㴀 嬀崀㬀ഀഀ
          $scope.text = 'hello';਍          ␀猀挀漀瀀攀⸀猀甀戀洀椀琀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
            if (this.text) {਍              琀栀椀猀⸀氀椀猀琀⸀瀀甀猀栀⠀琀栀椀猀⸀琀攀砀琀⤀㬀ഀഀ
              this.text = '';਍            紀ഀഀ
          };਍        紀ഀഀ
      </script>਍      㰀昀漀爀洀 渀最ⴀ猀甀戀洀椀琀㴀∀猀甀戀洀椀琀⠀⤀∀ 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀䌀琀爀氀∀㸀ഀഀ
        Enter text and hit enter:਍        㰀椀渀瀀甀琀 琀礀瀀攀㴀∀琀攀砀琀∀ 渀最ⴀ洀漀搀攀氀㴀∀琀攀砀琀∀ 渀愀洀攀㴀∀琀攀砀琀∀ ⼀㸀ഀഀ
        <input type="submit" id="submit" value="Submit" />਍        㰀瀀爀攀㸀氀椀猀琀㴀笀笀氀椀猀琀紀紀㰀⼀瀀爀攀㸀ഀഀ
      </form>਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
     <doc:scenario>਍       椀琀⠀✀猀栀漀甀氀搀 挀栀攀挀欀 渀最ⴀ猀甀戀洀椀琀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         expect(binding('list')).toBe('[]');਍         攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 ⌀猀甀戀洀椀琀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
         expect(binding('list')).toBe('["hello"]');਍         攀砀瀀攀挀琀⠀椀渀瀀甀琀⠀✀琀攀砀琀✀⤀⸀瘀愀氀⠀⤀⤀⸀琀漀䈀攀⠀✀✀⤀㬀ഀഀ
       });਍       椀琀⠀✀猀栀漀甀氀搀 椀最渀漀爀攀 攀洀瀀琀礀 猀琀爀椀渀最猀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         expect(binding('list')).toBe('[]');਍         攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 ⌀猀甀戀洀椀琀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
         element('.doc-example-live #submit').click();਍         攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀氀椀猀琀✀⤀⤀⸀琀漀䈀攀⠀✀嬀∀栀攀氀氀漀∀崀✀⤀㬀ഀഀ
       });਍     㰀⼀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
   </doc:example>਍ ⨀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䘀漀挀甀猀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Specify custom behavior on focus event.਍ ⨀ഀഀ
 * @element window, input, select, textarea, a਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䘀漀挀甀猀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 琀漀 攀瘀愀氀甀愀琀攀 甀瀀漀渀ഀഀ
 * focus. (Event object is available as `$event`)਍ ⨀ഀഀ
 * @example਍ ⨀ 匀攀攀 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䌀氀椀挀欀 渀最䌀氀椀挀欀紀ഀഀ
 */਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngBlur਍ ⨀ഀഀ
 * @description਍ ⨀ 匀瀀攀挀椀昀礀 挀甀猀琀漀洀 戀攀栀愀瘀椀漀爀 漀渀 戀氀甀爀 攀瘀攀渀琀⸀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 眀椀渀搀漀眀Ⰰ 椀渀瀀甀琀Ⰰ 猀攀氀攀挀琀Ⰰ 琀攀砀琀愀爀攀愀Ⰰ 愀ഀഀ
 * @param {expression} ngBlur {@link guide/expression Expression} to evaluate upon਍ ⨀ 戀氀甀爀⸀ ⠀䔀瘀攀渀琀 漀戀樀攀挀琀 椀猀 愀瘀愀椀氀愀戀氀攀 愀猀 怀␀攀瘀攀渀琀怀⤀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
 * See {@link ng.directive:ngClick ngClick}਍ ⨀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䌀漀瀀礀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Specify custom behavior on copy event.਍ ⨀ഀഀ
 * @element window, input, select, textarea, a਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䌀漀瀀礀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 琀漀 攀瘀愀氀甀愀琀攀 甀瀀漀渀ഀഀ
 * copy. (Event object is available as `$event`)਍ ⨀ഀഀ
 * @example਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍      㰀椀渀瀀甀琀 渀最ⴀ挀漀瀀礀㴀∀挀漀瀀椀攀搀㴀琀爀甀攀∀ 渀最ⴀ椀渀椀琀㴀∀挀漀瀀椀攀搀㴀昀愀氀猀攀㬀 瘀愀氀甀攀㴀✀挀漀瀀礀 洀攀✀∀ 渀最ⴀ洀漀搀攀氀㴀∀瘀愀氀甀攀∀㸀ഀഀ
      copied: {{copied}}਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
   </doc:example>਍ ⨀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䌀甀琀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Specify custom behavior on cut event.਍ ⨀ഀഀ
 * @element window, input, select, textarea, a਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䌀甀琀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 琀漀 攀瘀愀氀甀愀琀攀 甀瀀漀渀ഀഀ
 * cut. (Event object is available as `$event`)਍ ⨀ഀഀ
 * @example਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍      㰀椀渀瀀甀琀 渀最ⴀ挀甀琀㴀∀挀甀琀㴀琀爀甀攀∀ 渀最ⴀ椀渀椀琀㴀∀挀甀琀㴀昀愀氀猀攀㬀 瘀愀氀甀攀㴀✀挀甀琀 洀攀✀∀ 渀最ⴀ洀漀搀攀氀㴀∀瘀愀氀甀攀∀㸀ഀഀ
      cut: {{cut}}਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
   </doc:example>਍ ⨀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最倀愀猀琀攀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Specify custom behavior on paste event.਍ ⨀ഀഀ
 * @element window, input, select, textarea, a਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最倀愀猀琀攀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 琀漀 攀瘀愀氀甀愀琀攀 甀瀀漀渀ഀഀ
 * paste. (Event object is available as `$event`)਍ ⨀ഀഀ
 * @example਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍      㰀椀渀瀀甀琀 渀最ⴀ瀀愀猀琀攀㴀∀瀀愀猀琀攀㴀琀爀甀攀∀ 渀最ⴀ椀渀椀琀㴀∀瀀愀猀琀攀㴀昀愀氀猀攀∀ 瀀氀愀挀攀栀漀氀搀攀爀㴀✀瀀愀猀琀攀 栀攀爀攀✀㸀ഀഀ
      pasted: {{paste}}਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
   </doc:example>਍ ⨀⼀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䤀昀ഀഀ
 * @restrict A਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀攀 怀渀最䤀昀怀 搀椀爀攀挀琀椀瘀攀 爀攀洀漀瘀攀猀 漀爀 爀攀挀爀攀愀琀攀猀 愀 瀀漀爀琀椀漀渀 漀昀 琀栀攀 䐀伀䴀 琀爀攀攀 戀愀猀攀搀 漀渀 愀渀ഀഀ
 * {expression}. If the expression assigned to `ngIf` evaluates to a false਍ ⨀ 瘀愀氀甀攀 琀栀攀渀 琀栀攀 攀氀攀洀攀渀琀 椀猀 爀攀洀漀瘀攀搀 昀爀漀洀 琀栀攀 䐀伀䴀Ⰰ 漀琀栀攀爀眀椀猀攀 愀 挀氀漀渀攀 漀昀 琀栀攀ഀഀ
 * element is reinserted into the DOM.਍ ⨀ഀഀ
 * `ngIf` differs from `ngShow` and `ngHide` in that `ngIf` completely removes and recreates the਍ ⨀ 攀氀攀洀攀渀琀 椀渀 琀栀攀 䐀伀䴀 爀愀琀栀攀爀 琀栀愀渀 挀栀愀渀最椀渀最 椀琀猀 瘀椀猀椀戀椀氀椀琀礀 瘀椀愀 琀栀攀 怀搀椀猀瀀氀愀礀怀 挀猀猀 瀀爀漀瀀攀爀琀礀⸀  䄀 挀漀洀洀漀渀ഀഀ
 * case when this difference is significant is when using css selectors that rely on an element's਍ ⨀ 瀀漀猀椀琀椀漀渀 眀椀琀栀椀渀 琀栀攀 䐀伀䴀Ⰰ 猀甀挀栀 愀猀 琀栀攀 怀㨀昀椀爀猀琀ⴀ挀栀椀氀搀怀 漀爀 怀㨀氀愀猀琀ⴀ挀栀椀氀搀怀 瀀猀攀甀搀漀ⴀ挀氀愀猀猀攀猀⸀ഀഀ
 *਍ ⨀ 一漀琀攀 琀栀愀琀 眀栀攀渀 愀渀 攀氀攀洀攀渀琀 椀猀 爀攀洀漀瘀攀搀 甀猀椀渀最 怀渀最䤀昀怀 椀琀猀 猀挀漀瀀攀 椀猀 搀攀猀琀爀漀礀攀搀 愀渀搀 愀 渀攀眀 猀挀漀瀀攀ഀഀ
 * is created when the element is restored.  The scope created within `ngIf` inherits from਍ ⨀ 椀琀猀 瀀愀爀攀渀琀 猀挀漀瀀攀 甀猀椀渀最ഀഀ
 * {@link https://github.com/angular/angular.js/wiki/The-Nuances-of-Scope-Prototypal-Inheritance prototypal inheritance}.਍ ⨀ 䄀渀 椀洀瀀漀爀琀愀渀琀 椀洀瀀氀椀挀愀琀椀漀渀 漀昀 琀栀椀猀 椀猀 椀昀 怀渀最䴀漀搀攀氀怀 椀猀 甀猀攀搀 眀椀琀栀椀渀 怀渀最䤀昀怀 琀漀 戀椀渀搀 琀漀ഀഀ
 * a javascript primitive defined in the parent scope. In this case any modifications made to the਍ ⨀ 瘀愀爀椀愀戀氀攀 眀椀琀栀椀渀 琀栀攀 挀栀椀氀搀 猀挀漀瀀攀 眀椀氀氀 漀瘀攀爀爀椀搀攀 ⠀栀椀搀攀⤀ 琀栀攀 瘀愀氀甀攀 椀渀 琀栀攀 瀀愀爀攀渀琀 猀挀漀瀀攀⸀ഀഀ
 *਍ ⨀ 䄀氀猀漀Ⰰ 怀渀最䤀昀怀 爀攀挀爀攀愀琀攀猀 攀氀攀洀攀渀琀猀 甀猀椀渀最 琀栀攀椀爀 挀漀洀瀀椀氀攀搀 猀琀愀琀攀⸀ 䄀渀 攀砀愀洀瀀氀攀 漀昀 琀栀椀猀 戀攀栀愀瘀椀漀爀ഀഀ
 * is if an element's class attribute is directly modified after it's compiled, using something like਍ ⨀ 樀儀甀攀爀礀✀猀 怀⸀愀搀搀䌀氀愀猀猀⠀⤀怀 洀攀琀栀漀搀Ⰰ 愀渀搀 琀栀攀 攀氀攀洀攀渀琀 椀猀 氀愀琀攀爀 爀攀洀漀瘀攀搀⸀ 圀栀攀渀 怀渀最䤀昀怀 爀攀挀爀攀愀琀攀猀 琀栀攀 攀氀攀洀攀渀琀ഀഀ
 * the added class will be lost because the original compiled state is used to regenerate the element.਍ ⨀ഀഀ
 * Additionally, you can provide animations via the `ngAnimate` module to animate the `enter`਍ ⨀ 愀渀搀 怀氀攀愀瘀攀怀 攀昀昀攀挀琀猀⸀ഀഀ
 *਍ ⨀ 䀀愀渀椀洀愀琀椀漀渀猀ഀഀ
 * enter - happens just after the ngIf contents change and a new DOM element is created and injected into the ngIf container਍ ⨀ 氀攀愀瘀攀 ⴀ 栀愀瀀瀀攀渀猀 樀甀猀琀 戀攀昀漀爀攀 琀栀攀 渀最䤀昀 挀漀渀琀攀渀琀猀 愀爀攀 爀攀洀漀瘀攀搀 昀爀漀洀 琀栀攀 䐀伀䴀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䄀一夀ഀഀ
 * @scope਍ ⨀ 䀀瀀爀椀漀爀椀琀礀 㘀　　ഀഀ
 * @param {expression} ngIf If the {@link guide/expression expression} is falsy then਍ ⨀     琀栀攀 攀氀攀洀攀渀琀 椀猀 爀攀洀漀瘀攀搀 昀爀漀洀 琀栀攀 䐀伀䴀 琀爀攀攀⸀ 䤀昀 椀琀 椀猀 琀爀甀琀栀礀 愀 挀漀瀀礀 漀昀 琀栀攀 挀漀洀瀀椀氀攀搀ഀഀ
 *     element is added to the DOM tree.਍ ⨀ഀഀ
 * @example਍  㰀攀砀愀洀瀀氀攀 愀渀椀洀愀琀椀漀渀猀㴀∀琀爀甀攀∀㸀ഀഀ
    <file name="index.html">਍      䌀氀椀挀欀 洀攀㨀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀挀栀攀挀欀戀漀砀∀ 渀最ⴀ洀漀搀攀氀㴀∀挀栀攀挀欀攀搀∀ 渀最ⴀ椀渀椀琀㴀∀挀栀攀挀欀攀搀㴀琀爀甀攀∀ ⼀㸀㰀戀爀⼀㸀ഀഀ
      Show when checked:਍      㰀猀瀀愀渀 渀最ⴀ椀昀㴀∀挀栀攀挀欀攀搀∀ 挀氀愀猀猀㴀∀愀渀椀洀愀琀攀ⴀ椀昀∀㸀ഀഀ
        I'm removed when the checkbox is unchecked.਍      㰀⼀猀瀀愀渀㸀ഀഀ
    </file>਍    㰀昀椀氀攀 渀愀洀攀㴀∀愀渀椀洀愀琀椀漀渀猀⸀挀猀猀∀㸀ഀഀ
      .animate-if {਍        戀愀挀欀最爀漀甀渀搀㨀眀栀椀琀攀㬀ഀഀ
        border:1px solid black;਍        瀀愀搀搀椀渀最㨀㄀　瀀砀㬀ഀഀ
      }਍ഀഀ
      .animate-if.ng-enter, .animate-if.ng-leave {਍        ⴀ眀攀戀欀椀琀ⴀ琀爀愀渀猀椀琀椀漀渀㨀愀氀氀 挀甀戀椀挀ⴀ戀攀稀椀攀爀⠀　⸀㈀㔀　Ⰰ 　⸀㐀㘀　Ⰰ 　⸀㐀㔀　Ⰰ 　⸀㤀㐀　⤀ 　⸀㔀猀㬀ഀഀ
        transition:all cubic-bezier(0.250, 0.460, 0.450, 0.940) 0.5s;਍      紀ഀഀ
਍      ⸀愀渀椀洀愀琀攀ⴀ椀昀⸀渀最ⴀ攀渀琀攀爀Ⰰഀഀ
      .animate-if.ng-leave.ng-leave-active {਍        漀瀀愀挀椀琀礀㨀　㬀ഀഀ
      }਍ഀഀ
      .animate-if.ng-leave,਍      ⸀愀渀椀洀愀琀攀ⴀ椀昀⸀渀最ⴀ攀渀琀攀爀⸀渀最ⴀ攀渀琀攀爀ⴀ愀挀琀椀瘀攀 笀ഀഀ
        opacity:1;਍      紀ഀഀ
    </file>਍  㰀⼀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍瘀愀爀 渀最䤀昀䐀椀爀攀挀琀椀瘀攀 㴀 嬀✀␀愀渀椀洀愀琀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀愀渀椀洀愀琀攀⤀ 笀ഀഀ
  return {਍    琀爀愀渀猀挀氀甀搀攀㨀 ✀攀氀攀洀攀渀琀✀Ⰰഀഀ
    priority: 600,਍    琀攀爀洀椀渀愀氀㨀 琀爀甀攀Ⰰഀഀ
    restrict: 'A',਍    ␀␀琀氀戀㨀 琀爀甀攀Ⰰഀഀ
    link: function ($scope, $element, $attr, ctrl, $transclude) {਍        瘀愀爀 戀氀漀挀欀Ⰰ 挀栀椀氀搀匀挀漀瀀攀㬀ഀഀ
        $scope.$watch($attr.ngIf, function ngIfWatchAction(value) {਍ഀഀ
          if (toBoolean(value)) {਍            椀昀 ⠀℀挀栀椀氀搀匀挀漀瀀攀⤀ 笀ഀഀ
              childScope = $scope.$new();਍              ␀琀爀愀渀猀挀氀甀搀攀⠀挀栀椀氀搀匀挀漀瀀攀Ⰰ 昀甀渀挀琀椀漀渀 ⠀挀氀漀渀攀⤀ 笀ഀഀ
                clone[clone.length++] = document.createComment(' end ngIf: ' + $attr.ngIf + ' ');਍                ⼀⼀ 一漀琀攀㨀 圀攀 漀渀氀礀 渀攀攀搀 琀栀攀 昀椀爀猀琀⼀氀愀猀琀 渀漀搀攀 漀昀 琀栀攀 挀氀漀渀攀搀 渀漀搀攀猀⸀ഀഀ
                // However, we need to keep the reference to the jqlite wrapper as it might be changed later਍                ⼀⼀ 戀礀 愀 搀椀爀攀挀琀椀瘀攀 眀椀琀栀 琀攀洀瀀氀愀琀攀唀爀氀 眀栀攀渀 椀琀✀猀 琀攀洀瀀氀愀琀攀 愀爀爀椀瘀攀猀⸀ഀഀ
                block = {਍                  挀氀漀渀攀㨀 挀氀漀渀攀ഀഀ
                };਍                ␀愀渀椀洀愀琀攀⸀攀渀琀攀爀⠀挀氀漀渀攀Ⰰ ␀攀氀攀洀攀渀琀⸀瀀愀爀攀渀琀⠀⤀Ⰰ ␀攀氀攀洀攀渀琀⤀㬀ഀഀ
              });਍            紀ഀഀ
          } else {਍ഀഀ
            if (childScope) {਍              挀栀椀氀搀匀挀漀瀀攀⸀␀搀攀猀琀爀漀礀⠀⤀㬀ഀഀ
              childScope = null;਍            紀ഀഀ
਍            椀昀 ⠀戀氀漀挀欀⤀ 笀ഀഀ
              $animate.leave(getBlockElements(block.clone));਍              戀氀漀挀欀 㴀 渀甀氀氀㬀ഀഀ
            }਍          紀ഀഀ
        });਍    紀ഀഀ
  };਍紀崀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䤀渀挀氀甀搀攀ഀഀ
 * @restrict ECA਍ ⨀ഀഀ
 * @description਍ ⨀ 䘀攀琀挀栀攀猀Ⰰ 挀漀洀瀀椀氀攀猀 愀渀搀 椀渀挀氀甀搀攀猀 愀渀 攀砀琀攀爀渀愀氀 䠀吀䴀䰀 昀爀愀最洀攀渀琀⸀ഀഀ
 *਍ ⨀ 䈀礀 搀攀昀愀甀氀琀Ⰰ 琀栀攀 琀攀洀瀀氀愀琀攀 唀刀䰀 椀猀 爀攀猀琀爀椀挀琀攀搀 琀漀 琀栀攀 猀愀洀攀 搀漀洀愀椀渀 愀渀搀 瀀爀漀琀漀挀漀氀 愀猀 琀栀攀ഀഀ
 * application document. This is done by calling {@link ng.$sce#methods_getTrustedResourceUrl਍ ⨀ ␀猀挀攀⸀最攀琀吀爀甀猀琀攀搀刀攀猀漀甀爀挀攀唀爀氀紀 漀渀 椀琀⸀ 吀漀 氀漀愀搀 琀攀洀瀀氀愀琀攀猀 昀爀漀洀 漀琀栀攀爀 搀漀洀愀椀渀猀 漀爀 瀀爀漀琀漀挀漀氀猀ഀഀ
 * you may either {@link ng.$sceDelegateProvider#methods_resourceUrlWhitelist whitelist them} or਍ ⨀ 笀䀀氀椀渀欀 渀最⸀␀猀挀攀⌀洀攀琀栀漀搀猀开琀爀甀猀琀䄀猀刀攀猀漀甀爀挀攀唀爀氀 眀爀愀瀀 琀栀攀洀紀 愀猀 琀爀甀猀琀攀搀 瘀愀氀甀攀猀⸀ 刀攀昀攀爀 琀漀 䄀渀最甀氀愀爀✀猀 笀䀀氀椀渀欀ഀഀ
 * ng.$sce Strict Contextual Escaping}.਍ ⨀ഀഀ
 * In addition, the browser's਍ ⨀ 笀䀀氀椀渀欀 栀琀琀瀀猀㨀⼀⼀挀漀搀攀⸀最漀漀最氀攀⸀挀漀洀⼀瀀⼀戀爀漀眀猀攀爀猀攀挀⼀眀椀欀椀⼀倀愀爀琀㈀⌀匀愀洀攀ⴀ漀爀椀最椀渀开瀀漀氀椀挀礀开昀漀爀开堀䴀䰀䠀琀琀瀀刀攀焀甀攀猀琀ഀഀ
 * Same Origin Policy} and {@link http://www.w3.org/TR/cors/ Cross-Origin Resource Sharing਍ ⨀ ⠀䌀伀刀匀⤀紀 瀀漀氀椀挀礀 洀愀礀 昀甀爀琀栀攀爀 爀攀猀琀爀椀挀琀 眀栀攀琀栀攀爀 琀栀攀 琀攀洀瀀氀愀琀攀 椀猀 猀甀挀挀攀猀猀昀甀氀氀礀 氀漀愀搀攀搀⸀ഀഀ
 * For example, `ngInclude` won't work for cross-domain requests on all browsers and for `file://`਍ ⨀ 愀挀挀攀猀猀 漀渀 猀漀洀攀 戀爀漀眀猀攀爀猀⸀ഀഀ
 *਍ ⨀ 䀀愀渀椀洀愀琀椀漀渀猀ഀഀ
 * enter - animation is used to bring new content into the browser.਍ ⨀ 氀攀愀瘀攀 ⴀ 愀渀椀洀愀琀椀漀渀 椀猀 甀猀攀搀 琀漀 愀渀椀洀愀琀攀 攀砀椀猀琀椀渀最 挀漀渀琀攀渀琀 愀眀愀礀⸀ഀഀ
 *਍ ⨀ 吀栀攀 攀渀琀攀爀 愀渀搀 氀攀愀瘀攀 愀渀椀洀愀琀椀漀渀 漀挀挀甀爀 挀漀渀挀甀爀爀攀渀琀氀礀⸀ഀഀ
 *਍ ⨀ 䀀猀挀漀瀀攀ഀഀ
 * @priority 400਍ ⨀ഀഀ
 * @param {string} ngInclude|src angular expression evaluating to URL. If the source is a string constant,਍ ⨀                 洀愀欀攀 猀甀爀攀 礀漀甀 眀爀愀瀀 椀琀 椀渀 焀甀漀琀攀猀Ⰰ 攀⸀最⸀ 怀猀爀挀㴀∀✀洀礀倀愀爀琀椀愀氀吀攀洀瀀氀愀琀攀⸀栀琀洀氀✀∀怀⸀ഀഀ
 * @param {string=} onload Expression to evaluate when a new partial is loaded.਍ ⨀ഀഀ
 * @param {string=} autoscroll Whether `ngInclude` should call {@link ng.$anchorScroll਍ ⨀                  ␀愀渀挀栀漀爀匀挀爀漀氀氀紀 琀漀 猀挀爀漀氀氀 琀栀攀 瘀椀攀眀瀀漀爀琀 愀昀琀攀爀 琀栀攀 挀漀渀琀攀渀琀 椀猀 氀漀愀搀攀搀⸀ഀഀ
 *਍ ⨀                  ⴀ 䤀昀 琀栀攀 愀琀琀爀椀戀甀琀攀 椀猀 渀漀琀 猀攀琀Ⰰ 搀椀猀愀戀氀攀 猀挀爀漀氀氀椀渀最⸀ഀഀ
 *                  - If the attribute is set without value, enable scrolling.਍ ⨀                  ⴀ 伀琀栀攀爀眀椀猀攀 攀渀愀戀氀攀 猀挀爀漀氀氀椀渀最 漀渀氀礀 椀昀 琀栀攀 攀砀瀀爀攀猀猀椀漀渀 攀瘀愀氀甀愀琀攀猀 琀漀 琀爀甀琀栀礀 瘀愀氀甀攀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
  <example animations="true">਍    㰀昀椀氀攀 渀愀洀攀㴀∀椀渀搀攀砀⸀栀琀洀氀∀㸀ഀഀ
     <div ng-controller="Ctrl">਍       㰀猀攀氀攀挀琀 渀最ⴀ洀漀搀攀氀㴀∀琀攀洀瀀氀愀琀攀∀ 渀最ⴀ漀瀀琀椀漀渀猀㴀∀琀⸀渀愀洀攀 昀漀爀 琀 椀渀 琀攀洀瀀氀愀琀攀猀∀㸀ഀഀ
        <option value="">(blank)</option>਍       㰀⼀猀攀氀攀挀琀㸀ഀഀ
       url of the template: <tt>{{template.url}}</tt>਍       㰀栀爀⼀㸀ഀഀ
       <div class="slide-animate-container">਍         㰀搀椀瘀 挀氀愀猀猀㴀∀猀氀椀搀攀ⴀ愀渀椀洀愀琀攀∀ 渀最ⴀ椀渀挀氀甀搀攀㴀∀琀攀洀瀀氀愀琀攀⸀甀爀氀∀㸀㰀⼀搀椀瘀㸀ഀഀ
       </div>਍     㰀⼀搀椀瘀㸀ഀഀ
    </file>਍    㰀昀椀氀攀 渀愀洀攀㴀∀猀挀爀椀瀀琀⸀樀猀∀㸀ഀഀ
      function Ctrl($scope) {਍        ␀猀挀漀瀀攀⸀琀攀洀瀀氀愀琀攀猀 㴀ഀഀ
          [ { name: 'template1.html', url: 'template1.html'}਍          Ⰰ 笀 渀愀洀攀㨀 ✀琀攀洀瀀氀愀琀攀㈀⸀栀琀洀氀✀Ⰰ 甀爀氀㨀 ✀琀攀洀瀀氀愀琀攀㈀⸀栀琀洀氀✀紀 崀㬀ഀഀ
        $scope.template = $scope.templates[0];਍      紀ഀഀ
     </file>਍    㰀昀椀氀攀 渀愀洀攀㴀∀琀攀洀瀀氀愀琀攀㄀⸀栀琀洀氀∀㸀ഀഀ
      Content of template1.html਍    㰀⼀昀椀氀攀㸀ഀഀ
    <file name="template2.html">਍      䌀漀渀琀攀渀琀 漀昀 琀攀洀瀀氀愀琀攀㈀⸀栀琀洀氀ഀഀ
    </file>਍    㰀昀椀氀攀 渀愀洀攀㴀∀愀渀椀洀愀琀椀漀渀猀⸀挀猀猀∀㸀ഀഀ
      .slide-animate-container {਍        瀀漀猀椀琀椀漀渀㨀爀攀氀愀琀椀瘀攀㬀ഀഀ
        background:white;਍        戀漀爀搀攀爀㨀㄀瀀砀 猀漀氀椀搀 戀氀愀挀欀㬀ഀഀ
        height:40px;਍        漀瘀攀爀昀氀漀眀㨀栀椀搀搀攀渀㬀ഀഀ
      }਍ഀഀ
      .slide-animate {਍        瀀愀搀搀椀渀最㨀㄀　瀀砀㬀ഀഀ
      }਍ഀഀ
      .slide-animate.ng-enter, .slide-animate.ng-leave {਍        ⴀ眀攀戀欀椀琀ⴀ琀爀愀渀猀椀琀椀漀渀㨀愀氀氀 挀甀戀椀挀ⴀ戀攀稀椀攀爀⠀　⸀㈀㔀　Ⰰ 　⸀㐀㘀　Ⰰ 　⸀㐀㔀　Ⰰ 　⸀㤀㐀　⤀ 　⸀㔀猀㬀ഀഀ
        transition:all cubic-bezier(0.250, 0.460, 0.450, 0.940) 0.5s;਍ഀഀ
        position:absolute;਍        琀漀瀀㨀　㬀ഀഀ
        left:0;਍        爀椀最栀琀㨀　㬀ഀഀ
        bottom:0;਍        搀椀猀瀀氀愀礀㨀戀氀漀挀欀㬀ഀഀ
        padding:10px;਍      紀ഀഀ
਍      ⸀猀氀椀搀攀ⴀ愀渀椀洀愀琀攀⸀渀最ⴀ攀渀琀攀爀 笀ഀഀ
        top:-50px;਍      紀ഀഀ
      .slide-animate.ng-enter.ng-enter-active {਍        琀漀瀀㨀　㬀ഀഀ
      }਍ഀഀ
      .slide-animate.ng-leave {਍        琀漀瀀㨀　㬀ഀഀ
      }਍      ⸀猀氀椀搀攀ⴀ愀渀椀洀愀琀攀⸀渀最ⴀ氀攀愀瘀攀⸀渀最ⴀ氀攀愀瘀攀ⴀ愀挀琀椀瘀攀 笀ഀഀ
        top:50px;਍      紀ഀഀ
    </file>਍    㰀昀椀氀攀 渀愀洀攀㴀∀猀挀攀渀愀爀椀漀⸀樀猀∀㸀ഀഀ
      it('should load template1.html', function() {਍       攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 嬀渀最ⴀ椀渀挀氀甀搀攀崀✀⤀⸀琀攀砀琀⠀⤀⤀⸀ഀഀ
         toMatch(/Content of template1.html/);਍      紀⤀㬀ഀഀ
      it('should load template2.html', function() {਍       猀攀氀攀挀琀⠀✀琀攀洀瀀氀愀琀攀✀⤀⸀漀瀀琀椀漀渀⠀✀㄀✀⤀㬀ഀഀ
       expect(element('.doc-example-live [ng-include]').text()).਍         琀漀䴀愀琀挀栀⠀⼀䌀漀渀琀攀渀琀 漀昀 琀攀洀瀀氀愀琀攀㈀⸀栀琀洀氀⼀⤀㬀ഀഀ
      });਍      椀琀⠀✀猀栀漀甀氀搀 挀栀愀渀最攀 琀漀 戀氀愀渀欀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
       select('template').option('');਍       攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 嬀渀最ⴀ椀渀挀氀甀搀攀崀✀⤀⤀⸀琀漀䈀攀⠀甀渀搀攀昀椀渀攀搀⤀㬀ഀഀ
      });਍    㰀⼀昀椀氀攀㸀ഀഀ
  </example>਍ ⨀⼀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 攀瘀攀渀琀ഀഀ
 * @name ng.directive:ngInclude#$includeContentRequested਍ ⨀ 䀀攀瘀攀渀琀伀昀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䤀渀挀氀甀搀攀ഀഀ
 * @eventType emit on the scope ngInclude was declared in਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Emitted every time the ngInclude content is requested.਍ ⨀⼀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 攀瘀攀渀琀ഀഀ
 * @name ng.directive:ngInclude#$includeContentLoaded਍ ⨀ 䀀攀瘀攀渀琀伀昀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䤀渀挀氀甀搀攀ഀഀ
 * @eventType emit on the current ngInclude scope਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Emitted every time the ngInclude content is reloaded.਍ ⨀⼀ഀഀ
var ngIncludeDirective = ['$http', '$templateCache', '$anchorScroll', '$animate', '$sce',਍                  昀甀渀挀琀椀漀渀⠀␀栀琀琀瀀Ⰰ   ␀琀攀洀瀀氀愀琀攀䌀愀挀栀攀Ⰰ   ␀愀渀挀栀漀爀匀挀爀漀氀氀Ⰰ   ␀愀渀椀洀愀琀攀Ⰰ   ␀猀挀攀⤀ 笀ഀഀ
  return {਍    爀攀猀琀爀椀挀琀㨀 ✀䔀䌀䄀✀Ⰰഀഀ
    priority: 400,਍    琀攀爀洀椀渀愀氀㨀 琀爀甀攀Ⰰഀഀ
    transclude: 'element',਍    挀漀渀琀爀漀氀氀攀爀㨀 愀渀最甀氀愀爀⸀渀漀漀瀀Ⰰഀഀ
    compile: function(element, attr) {਍      瘀愀爀 猀爀挀䔀砀瀀 㴀 愀琀琀爀⸀渀最䤀渀挀氀甀搀攀 簀簀 愀琀琀爀⸀猀爀挀Ⰰഀഀ
          onloadExp = attr.onload || '',਍          愀甀琀漀匀挀爀漀氀氀䔀砀瀀 㴀 愀琀琀爀⸀愀甀琀漀猀挀爀漀氀氀㬀ഀഀ
਍      爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ ␀攀氀攀洀攀渀琀Ⰰ ␀愀琀琀爀Ⰰ 挀琀爀氀Ⰰ ␀琀爀愀渀猀挀氀甀搀攀⤀ 笀ഀഀ
        var changeCounter = 0,਍            挀甀爀爀攀渀琀匀挀漀瀀攀Ⰰഀഀ
            currentElement;਍ഀഀ
        var cleanupLastIncludeContent = function() {਍          椀昀 ⠀挀甀爀爀攀渀琀匀挀漀瀀攀⤀ 笀ഀഀ
            currentScope.$destroy();਍            挀甀爀爀攀渀琀匀挀漀瀀攀 㴀 渀甀氀氀㬀ഀഀ
          }਍          椀昀⠀挀甀爀爀攀渀琀䔀氀攀洀攀渀琀⤀ 笀ഀഀ
            $animate.leave(currentElement);਍            挀甀爀爀攀渀琀䔀氀攀洀攀渀琀 㴀 渀甀氀氀㬀ഀഀ
          }਍        紀㬀ഀഀ
਍        猀挀漀瀀攀⸀␀眀愀琀挀栀⠀␀猀挀攀⸀瀀愀爀猀攀䄀猀刀攀猀漀甀爀挀攀唀爀氀⠀猀爀挀䔀砀瀀⤀Ⰰ 昀甀渀挀琀椀漀渀 渀最䤀渀挀氀甀搀攀圀愀琀挀栀䄀挀琀椀漀渀⠀猀爀挀⤀ 笀ഀഀ
          var afterAnimation = function() {਍            椀昀 ⠀椀猀䐀攀昀椀渀攀搀⠀愀甀琀漀匀挀爀漀氀氀䔀砀瀀⤀ ☀☀ ⠀℀愀甀琀漀匀挀爀漀氀氀䔀砀瀀 簀簀 猀挀漀瀀攀⸀␀攀瘀愀氀⠀愀甀琀漀匀挀爀漀氀氀䔀砀瀀⤀⤀⤀ 笀ഀഀ
              $anchorScroll();਍            紀ഀഀ
          };਍          瘀愀爀 琀栀椀猀䌀栀愀渀最攀䤀搀 㴀 ⬀⬀挀栀愀渀最攀䌀漀甀渀琀攀爀㬀ഀഀ
਍          椀昀 ⠀猀爀挀⤀ 笀ഀഀ
            $http.get(src, {cache: $templateCache}).success(function(response) {਍              椀昀 ⠀琀栀椀猀䌀栀愀渀最攀䤀搀 ℀㴀㴀 挀栀愀渀最攀䌀漀甀渀琀攀爀⤀ 爀攀琀甀爀渀㬀ഀഀ
              var newScope = scope.$new();਍              挀琀爀氀⸀琀攀洀瀀氀愀琀攀 㴀 爀攀猀瀀漀渀猀攀㬀ഀഀ
਍              ⼀⼀ 一漀琀攀㨀 吀栀椀猀 眀椀氀氀 愀氀猀漀 氀椀渀欀 愀氀氀 挀栀椀氀搀爀攀渀 漀昀 渀最ⴀ椀渀挀氀甀搀攀 琀栀愀琀 眀攀爀攀 挀漀渀琀愀椀渀攀搀 椀渀 琀栀攀 漀爀椀最椀渀愀氀ഀഀ
              // html. If that content contains controllers, ... they could pollute/change the scope.਍              ⼀⼀ 䠀漀眀攀瘀攀爀Ⰰ 甀猀椀渀最 渀最ⴀ椀渀挀氀甀搀攀 漀渀 愀渀 攀氀攀洀攀渀琀 眀椀琀栀 愀搀搀椀琀椀漀渀愀氀 挀漀渀琀攀渀琀 搀漀攀猀 渀漀琀 洀愀欀攀 猀攀渀猀攀⸀⸀⸀ഀഀ
              // Note: We can't remove them in the cloneAttchFn of $transclude as that਍              ⼀⼀ 昀甀渀挀琀椀漀渀 椀猀 挀愀氀氀攀搀 戀攀昀漀爀攀 氀椀渀欀椀渀最 琀栀攀 挀漀渀琀攀渀琀Ⰰ 眀栀椀挀栀 眀漀甀氀搀 愀瀀瀀氀礀 挀栀椀氀搀ഀഀ
              // directives to non existing elements.਍              瘀愀爀 挀氀漀渀攀 㴀 ␀琀爀愀渀猀挀氀甀搀攀⠀渀攀眀匀挀漀瀀攀Ⰰ 昀甀渀挀琀椀漀渀⠀挀氀漀渀攀⤀ 笀ഀഀ
                cleanupLastIncludeContent();਍                ␀愀渀椀洀愀琀攀⸀攀渀琀攀爀⠀挀氀漀渀攀Ⰰ 渀甀氀氀Ⰰ ␀攀氀攀洀攀渀琀Ⰰ 愀昀琀攀爀䄀渀椀洀愀琀椀漀渀⤀㬀ഀഀ
              });਍ഀഀ
              currentScope = newScope;਍              挀甀爀爀攀渀琀䔀氀攀洀攀渀琀 㴀 挀氀漀渀攀㬀ഀഀ
਍              挀甀爀爀攀渀琀匀挀漀瀀攀⸀␀攀洀椀琀⠀✀␀椀渀挀氀甀搀攀䌀漀渀琀攀渀琀䰀漀愀搀攀搀✀⤀㬀ഀഀ
              scope.$eval(onloadExp);਍            紀⤀⸀攀爀爀漀爀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
              if (thisChangeId === changeCounter) cleanupLastIncludeContent();਍            紀⤀㬀ഀഀ
            scope.$emit('$includeContentRequested');਍          紀 攀氀猀攀 笀ഀഀ
            cleanupLastIncludeContent();਍            挀琀爀氀⸀琀攀洀瀀氀愀琀攀 㴀 渀甀氀氀㬀ഀഀ
          }਍        紀⤀㬀ഀഀ
      };਍    紀ഀഀ
  };਍紀崀㬀ഀഀ
਍⼀⼀ 吀栀椀猀 搀椀爀攀挀琀椀瘀攀 椀猀 挀愀氀氀攀搀 搀甀爀椀渀最 琀栀攀 ␀琀爀愀渀猀挀氀甀搀攀 挀愀氀氀 漀昀 琀栀攀 昀椀爀猀琀 怀渀最䤀渀挀氀甀搀攀怀 搀椀爀攀挀琀椀瘀攀⸀ഀഀ
// It will replace and compile the content of the element with the loaded template.਍⼀⼀ 圀攀 渀攀攀搀 琀栀椀猀 搀椀爀攀挀琀椀瘀攀 猀漀 琀栀愀琀 琀栀攀 攀氀攀洀攀渀琀 挀漀渀琀攀渀琀 椀猀 愀氀爀攀愀搀礀 昀椀氀氀攀搀 眀栀攀渀ഀഀ
// the link function of another directive on the same element as ngInclude਍⼀⼀ 椀猀 挀愀氀氀攀搀⸀ഀഀ
var ngIncludeFillContentDirective = ['$compile',਍  昀甀渀挀琀椀漀渀⠀␀挀漀洀瀀椀氀攀⤀ 笀ഀഀ
    return {਍      爀攀猀琀爀椀挀琀㨀 ✀䔀䌀䄀✀Ⰰഀഀ
      priority: -400,਍      爀攀焀甀椀爀攀㨀 ✀渀最䤀渀挀氀甀搀攀✀Ⰰഀഀ
      link: function(scope, $element, $attr, ctrl) {਍        ␀攀氀攀洀攀渀琀⸀栀琀洀氀⠀挀琀爀氀⸀琀攀洀瀀氀愀琀攀⤀㬀ഀഀ
        $compile($element.contents())(scope);਍      紀ഀഀ
    };਍  紀崀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䤀渀椀琀ഀഀ
 * @restrict AC਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀攀 怀渀最䤀渀椀琀怀 搀椀爀攀挀琀椀瘀攀 愀氀氀漀眀猀 礀漀甀 琀漀 攀瘀愀氀甀愀琀攀 愀渀 攀砀瀀爀攀猀猀椀漀渀 椀渀 琀栀攀ഀഀ
 * current scope.਍ ⨀ഀഀ
 * <div class="alert alert-error">਍ ⨀ 吀栀攀 漀渀氀礀 愀瀀瀀爀漀瀀爀椀愀琀攀 甀猀攀 漀昀 怀渀最䤀渀椀琀怀 椀猀 昀漀爀 愀氀椀愀猀椀渀最 猀瀀攀挀椀愀氀 瀀爀漀瀀攀爀琀椀攀猀 漀昀ഀഀ
 * {@link api/ng.directive:ngRepeat `ngRepeat`}, as seen in the demo below. Besides this case, you਍ ⨀ 猀栀漀甀氀搀 甀猀攀 笀䀀氀椀渀欀 最甀椀搀攀⼀挀漀渀琀爀漀氀氀攀爀 挀漀渀琀爀漀氀氀攀爀猀紀 爀愀琀栀攀爀 琀栀愀渀 怀渀最䤀渀椀琀怀ഀഀ
 * to initialize values on a scope.਍ ⨀ 㰀⼀搀椀瘀㸀ഀഀ
 *਍ ⨀ 䀀瀀爀椀漀爀椀琀礀 㐀㔀　ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䄀一夀ഀഀ
 * @param {expression} ngInit {@link guide/expression Expression} to eval.਍ ⨀ഀഀ
 * @example਍   㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
     <doc:source>਍   㰀猀挀爀椀瀀琀㸀ഀഀ
     function Ctrl($scope) {਍       ␀猀挀漀瀀攀⸀氀椀猀琀 㴀 嬀嬀✀愀✀Ⰰ ✀戀✀崀Ⰰ 嬀✀挀✀Ⰰ ✀搀✀崀崀㬀ഀഀ
     }਍   㰀⼀猀挀爀椀瀀琀㸀ഀഀ
   <div ng-controller="Ctrl">਍     㰀搀椀瘀 渀最ⴀ爀攀瀀攀愀琀㴀∀椀渀渀攀爀䰀椀猀琀 椀渀 氀椀猀琀∀ 渀最ⴀ椀渀椀琀㴀∀漀甀琀攀爀䤀渀搀攀砀 㴀 ␀椀渀搀攀砀∀㸀ഀഀ
       <div ng-repeat="value in innerList" ng-init="innerIndex = $index">਍          㰀猀瀀愀渀 挀氀愀猀猀㴀∀攀砀愀洀瀀氀攀ⴀ椀渀椀琀∀㸀氀椀猀琀嬀 笀笀漀甀琀攀爀䤀渀搀攀砀紀紀 崀嬀 笀笀椀渀渀攀爀䤀渀搀攀砀紀紀 崀 㴀 笀笀瘀愀氀甀攀紀紀㬀㰀⼀猀瀀愀渀㸀ഀഀ
       </div>਍     㰀⼀搀椀瘀㸀ഀഀ
   </div>਍     㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
     <doc:scenario>਍       椀琀⠀✀猀栀漀甀氀搀 愀氀椀愀猀 椀渀搀攀砀 瀀漀猀椀琀椀漀渀猀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         expect(element('.example-init').text())਍           ⸀琀漀䈀攀⠀✀氀椀猀琀嬀 　 崀嬀 　 崀 㴀 愀㬀✀ ⬀ഀഀ
                 'list[ 0 ][ 1 ] = b;' +਍                 ✀氀椀猀琀嬀 ㄀ 崀嬀 　 崀 㴀 挀㬀✀ ⬀ഀഀ
                 'list[ 1 ][ 1 ] = d;');਍       紀⤀㬀ഀഀ
     </doc:scenario>਍   㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍瘀愀爀 渀最䤀渀椀琀䐀椀爀攀挀琀椀瘀攀 㴀 渀最䐀椀爀攀挀琀椀瘀攀⠀笀ഀഀ
  priority: 450,਍  挀漀洀瀀椀氀攀㨀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    return {਍      瀀爀攀㨀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀猀⤀ 笀ഀഀ
        scope.$eval(attrs.ngInit);਍      紀ഀഀ
    };਍  紀ഀഀ
});਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngNonBindable਍ ⨀ 䀀爀攀猀琀爀椀挀琀 䄀䌀ഀഀ
 * @priority 1000਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀攀 怀渀最一漀渀䈀椀渀搀愀戀氀攀怀 搀椀爀攀挀琀椀瘀攀 琀攀氀氀猀 䄀渀最甀氀愀爀 渀漀琀 琀漀 挀漀洀瀀椀氀攀 漀爀 戀椀渀搀 琀栀攀 挀漀渀琀攀渀琀猀 漀昀 琀栀攀 挀甀爀爀攀渀琀ഀഀ
 * DOM element. This is useful if the element contains what appears to be Angular directives and਍ ⨀ 戀椀渀搀椀渀最猀 戀甀琀 眀栀椀挀栀 猀栀漀甀氀搀 戀攀 椀最渀漀爀攀搀 戀礀 䄀渀最甀氀愀爀⸀ 吀栀椀猀 挀漀甀氀搀 戀攀 琀栀攀 挀愀猀攀 椀昀 礀漀甀 栀愀瘀攀 愀 猀椀琀攀 琀栀愀琀ഀഀ
 * displays snippets of code, for instance.਍ ⨀ഀഀ
 * @element ANY਍ ⨀ഀഀ
 * @example਍ ⨀ 䤀渀 琀栀椀猀 攀砀愀洀瀀氀攀 琀栀攀爀攀 愀爀攀 琀眀漀 氀漀挀愀琀椀漀渀猀 眀栀攀爀攀 愀 猀椀洀瀀氀攀 椀渀琀攀爀瀀漀氀愀琀椀漀渀 戀椀渀搀椀渀最 ⠀怀笀笀紀紀怀⤀ 椀猀 瀀爀攀猀攀渀琀Ⰰഀഀ
 * but the one wrapped in `ngNonBindable` is left alone.਍ ⨀ഀഀ
 * @example਍    㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
      <doc:source>਍        㰀搀椀瘀㸀一漀爀洀愀氀㨀 笀笀㄀ ⬀ ㈀紀紀㰀⼀搀椀瘀㸀ഀഀ
        <div ng-non-bindable>Ignored: {{1 + 2}}</div>਍      㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      <doc:scenario>਍       椀琀⠀✀猀栀漀甀氀搀 挀栀攀挀欀 渀最ⴀ渀漀渀ⴀ戀椀渀搀愀戀氀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         expect(using('.doc-example-live').binding('1 + 2')).toBe('3');਍         攀砀瀀攀挀琀⠀甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀攀氀攀洀攀渀琀⠀✀搀椀瘀㨀氀愀猀琀✀⤀⸀琀攀砀琀⠀⤀⤀⸀ഀഀ
           toMatch(/1 \+ 2/);਍       紀⤀㬀ഀഀ
      </doc:scenario>਍    㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍瘀愀爀 渀最一漀渀䈀椀渀搀愀戀氀攀䐀椀爀攀挀琀椀瘀攀 㴀 渀最䐀椀爀攀挀琀椀瘀攀⠀笀 琀攀爀洀椀渀愀氀㨀 琀爀甀攀Ⰰ 瀀爀椀漀爀椀琀礀㨀 ㄀　　　 紀⤀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最倀氀甀爀愀氀椀稀攀ഀഀ
 * @restrict EA਍ ⨀ഀഀ
 * @description਍ ⨀ ⌀ 伀瘀攀爀瘀椀攀眀ഀഀ
 * `ngPluralize` is a directive that displays messages according to en-US localization rules.਍ ⨀ 吀栀攀猀攀 爀甀氀攀猀 愀爀攀 戀甀渀搀氀攀搀 眀椀琀栀 愀渀最甀氀愀爀⸀樀猀Ⰰ 戀甀琀 挀愀渀 戀攀 漀瘀攀爀爀椀搀搀攀渀ഀഀ
 * (see {@link guide/i18n Angular i18n} dev guide). You configure ngPluralize directive਍ ⨀ 戀礀 猀瀀攀挀椀昀礀椀渀最 琀栀攀 洀愀瀀瀀椀渀最猀 戀攀琀眀攀攀渀ഀഀ
 * {@link http://unicode.org/repos/cldr-tmp/trunk/diff/supplemental/language_plural_rules.html਍ ⨀ 瀀氀甀爀愀氀 挀愀琀攀最漀爀椀攀猀紀 愀渀搀 琀栀攀 猀琀爀椀渀最猀 琀漀 戀攀 搀椀猀瀀氀愀礀攀搀⸀ഀഀ
 *਍ ⨀ ⌀ 倀氀甀爀愀氀 挀愀琀攀最漀爀椀攀猀 愀渀搀 攀砀瀀氀椀挀椀琀 渀甀洀戀攀爀 爀甀氀攀猀ഀഀ
 * There are two਍ ⨀ 笀䀀氀椀渀欀 栀琀琀瀀㨀⼀⼀甀渀椀挀漀搀攀⸀漀爀最⼀爀攀瀀漀猀⼀挀氀搀爀ⴀ琀洀瀀⼀琀爀甀渀欀⼀搀椀昀昀⼀猀甀瀀瀀氀攀洀攀渀琀愀氀⼀氀愀渀最甀愀最攀开瀀氀甀爀愀氀开爀甀氀攀猀⸀栀琀洀氀ഀഀ
 * plural categories} in Angular's default en-US locale: "one" and "other".਍ ⨀ഀഀ
 * While a plural category may match many numbers (for example, in en-US locale, "other" can match਍ ⨀ 愀渀礀 渀甀洀戀攀爀 琀栀愀琀 椀猀 渀漀琀 ㄀⤀Ⰰ 愀渀 攀砀瀀氀椀挀椀琀 渀甀洀戀攀爀 爀甀氀攀 挀愀渀 漀渀氀礀 洀愀琀挀栀 漀渀攀 渀甀洀戀攀爀⸀ 䘀漀爀 攀砀愀洀瀀氀攀Ⰰ 琀栀攀ഀഀ
 * explicit number rule for "3" matches the number 3. There are examples of plural categories਍ ⨀ 愀渀搀 攀砀瀀氀椀挀椀琀 渀甀洀戀攀爀 爀甀氀攀猀 琀栀爀漀甀最栀漀甀琀 琀栀攀 爀攀猀琀 漀昀 琀栀椀猀 搀漀挀甀洀攀渀琀愀琀椀漀渀⸀ഀഀ
 *਍ ⨀ ⌀ 䌀漀渀昀椀最甀爀椀渀最 渀最倀氀甀爀愀氀椀稀攀ഀഀ
 * You configure ngPluralize by providing 2 attributes: `count` and `when`.਍ ⨀ 夀漀甀 挀愀渀 愀氀猀漀 瀀爀漀瘀椀搀攀 愀渀 漀瀀琀椀漀渀愀氀 愀琀琀爀椀戀甀琀攀Ⰰ 怀漀昀昀猀攀琀怀⸀ഀഀ
 *਍ ⨀ 吀栀攀 瘀愀氀甀攀 漀昀 琀栀攀 怀挀漀甀渀琀怀 愀琀琀爀椀戀甀琀攀 挀愀渀 戀攀 攀椀琀栀攀爀 愀 猀琀爀椀渀最 漀爀 愀渀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀ഀഀ
 * Angular expression}; these are evaluated on the current scope for its bound value.਍ ⨀ഀഀ
 * The `when` attribute specifies the mappings between plural categories and the actual਍ ⨀ 猀琀爀椀渀最 琀漀 戀攀 搀椀猀瀀氀愀礀攀搀⸀ 吀栀攀 瘀愀氀甀攀 漀昀 琀栀攀 愀琀琀爀椀戀甀琀攀 猀栀漀甀氀搀 戀攀 愀 䨀匀伀一 漀戀樀攀挀琀⸀ഀഀ
 *਍ ⨀ 吀栀攀 昀漀氀氀漀眀椀渀最 攀砀愀洀瀀氀攀 猀栀漀眀猀 栀漀眀 琀漀 挀漀渀昀椀最甀爀攀 渀最倀氀甀爀愀氀椀稀攀㨀ഀഀ
 *਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 * <ng-pluralize count="personCount"਍                 眀栀攀渀㴀∀笀✀　✀㨀 ✀一漀戀漀搀礀 椀猀 瘀椀攀眀椀渀最⸀✀Ⰰഀഀ
 *                      'one': '1 person is viewing.',਍ ⨀                      ✀漀琀栀攀爀✀㨀 ✀笀紀 瀀攀漀瀀氀攀 愀爀攀 瘀椀攀眀椀渀最⸀✀紀∀㸀ഀഀ
 * </ng-pluralize>਍ ⨀㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 䤀渀 琀栀攀 攀砀愀洀瀀氀攀Ⰰ 怀∀　㨀 一漀戀漀搀礀 椀猀 瘀椀攀眀椀渀最⸀∀怀 椀猀 愀渀 攀砀瀀氀椀挀椀琀 渀甀洀戀攀爀 爀甀氀攀⸀ 䤀昀 礀漀甀 搀椀搀 渀漀琀ഀഀ
 * specify this rule, 0 would be matched to the "other" category and "0 people are viewing"਍ ⨀ 眀漀甀氀搀 戀攀 猀栀漀眀渀 椀渀猀琀攀愀搀 漀昀 ∀一漀戀漀搀礀 椀猀 瘀椀攀眀椀渀最∀⸀ 夀漀甀 挀愀渀 猀瀀攀挀椀昀礀 愀渀 攀砀瀀氀椀挀椀琀 渀甀洀戀攀爀 爀甀氀攀 昀漀爀ഀഀ
 * other numbers, for example 12, so that instead of showing "12 people are viewing", you can਍ ⨀ 猀栀漀眀 ∀愀 搀漀稀攀渀 瀀攀漀瀀氀攀 愀爀攀 瘀椀攀眀椀渀最∀⸀ഀഀ
 *਍ ⨀ 夀漀甀 挀愀渀 甀猀攀 愀 猀攀琀 漀昀 挀氀漀猀攀搀 戀爀愀挀攀猀 ⠀怀笀紀怀⤀ 愀猀 愀 瀀氀愀挀攀栀漀氀搀攀爀 昀漀爀 琀栀攀 渀甀洀戀攀爀 琀栀愀琀 礀漀甀 眀愀渀琀 猀甀戀猀琀椀琀甀琀攀搀ഀഀ
 * into pluralized strings. In the previous example, Angular will replace `{}` with਍ ⨀ 㰀猀瀀愀渀 渀最ⴀ渀漀渀ⴀ戀椀渀搀愀戀氀攀㸀怀笀笀瀀攀爀猀漀渀䌀漀甀渀琀紀紀怀㰀⼀猀瀀愀渀㸀⸀ 吀栀攀 挀氀漀猀攀搀 戀爀愀挀攀猀 怀笀紀怀 椀猀 愀 瀀氀愀挀攀栀漀氀搀攀爀ഀഀ
 * for <span ng-non-bindable>{{numberExpression}}</span>.਍ ⨀ഀഀ
 * # Configuring ngPluralize with offset਍ ⨀ 吀栀攀 怀漀昀昀猀攀琀怀 愀琀琀爀椀戀甀琀攀 愀氀氀漀眀猀 昀甀爀琀栀攀爀 挀甀猀琀漀洀椀稀愀琀椀漀渀 漀昀 瀀氀甀爀愀氀椀稀攀搀 琀攀砀琀Ⰰ 眀栀椀挀栀 挀愀渀 爀攀猀甀氀琀 椀渀ഀഀ
 * a better user experience. For example, instead of the message "4 people are viewing this document",਍ ⨀ 礀漀甀 洀椀最栀琀 搀椀猀瀀氀愀礀 ∀䨀漀栀渀Ⰰ 䬀愀琀攀 愀渀搀 ㈀ 漀琀栀攀爀猀 愀爀攀 瘀椀攀眀椀渀最 琀栀椀猀 搀漀挀甀洀攀渀琀∀⸀ഀഀ
 * The offset attribute allows you to offset a number by any desired value.਍ ⨀ 䰀攀琀✀猀 琀愀欀攀 愀 氀漀漀欀 愀琀 愀渀 攀砀愀洀瀀氀攀㨀ഀഀ
 *਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 * <ng-pluralize count="personCount" offset=2਍ ⨀               眀栀攀渀㴀∀笀✀　✀㨀 ✀一漀戀漀搀礀 椀猀 瘀椀攀眀椀渀最⸀✀Ⰰഀഀ
 *                      '1': '{{person1}} is viewing.',਍ ⨀                      ✀㈀✀㨀 ✀笀笀瀀攀爀猀漀渀㄀紀紀 愀渀搀 笀笀瀀攀爀猀漀渀㈀紀紀 愀爀攀 瘀椀攀眀椀渀最⸀✀Ⰰഀഀ
 *                      'one': '{{person1}}, {{person2}} and one other person are viewing.',਍ ⨀                      ✀漀琀栀攀爀✀㨀 ✀笀笀瀀攀爀猀漀渀㄀紀紀Ⰰ 笀笀瀀攀爀猀漀渀㈀紀紀 愀渀搀 笀紀 漀琀栀攀爀 瀀攀漀瀀氀攀 愀爀攀 瘀椀攀眀椀渀最⸀✀紀∀㸀ഀഀ
 * </ng-pluralize>਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 一漀琀椀挀攀 琀栀愀琀 眀攀 愀爀攀 猀琀椀氀氀 甀猀椀渀最 琀眀漀 瀀氀甀爀愀氀 挀愀琀攀最漀爀椀攀猀⠀漀渀攀Ⰰ 漀琀栀攀爀⤀Ⰰ 戀甀琀 眀攀 愀搀搀攀搀ഀഀ
 * three explicit number rules 0, 1 and 2.਍ ⨀ 圀栀攀渀 漀渀攀 瀀攀爀猀漀渀Ⰰ 瀀攀爀栀愀瀀猀 䨀漀栀渀Ⰰ 瘀椀攀眀猀 琀栀攀 搀漀挀甀洀攀渀琀Ⰰ ∀䨀漀栀渀 椀猀 瘀椀攀眀椀渀最∀ 眀椀氀氀 戀攀 猀栀漀眀渀⸀ഀഀ
 * When three people view the document, no explicit number rule is found, so਍ ⨀ 愀渀 漀昀昀猀攀琀 漀昀 ㈀ 椀猀 琀愀欀攀渀 漀昀昀 ㌀Ⰰ 愀渀搀 䄀渀最甀氀愀爀 甀猀攀猀 ㄀ 琀漀 搀攀挀椀搀攀 琀栀攀 瀀氀甀爀愀氀 挀愀琀攀最漀爀礀⸀ഀഀ
 * In this case, plural category 'one' is matched and "John, Marry and one other person are viewing"਍ ⨀ 椀猀 猀栀漀眀渀⸀ഀഀ
 *਍ ⨀ 一漀琀攀 琀栀愀琀 眀栀攀渀 礀漀甀 猀瀀攀挀椀昀礀 漀昀昀猀攀琀猀Ⰰ 礀漀甀 洀甀猀琀 瀀爀漀瘀椀搀攀 攀砀瀀氀椀挀椀琀 渀甀洀戀攀爀 爀甀氀攀猀 昀漀爀ഀഀ
 * numbers from 0 up to and including the offset. If you use an offset of 3, for example,਍ ⨀ 礀漀甀 洀甀猀琀 瀀爀漀瘀椀搀攀 攀砀瀀氀椀挀椀琀 渀甀洀戀攀爀 爀甀氀攀猀 昀漀爀 　Ⰰ ㄀Ⰰ ㈀ 愀渀搀 ㌀⸀ 夀漀甀 洀甀猀琀 愀氀猀漀 瀀爀漀瘀椀搀攀 瀀氀甀爀愀氀 猀琀爀椀渀最猀 昀漀爀ഀഀ
 * plural categories "one" and "other".਍ ⨀ഀഀ
 * @param {string|expression} count The variable to be bounded to.਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 眀栀攀渀 吀栀攀 洀愀瀀瀀椀渀最 戀攀琀眀攀攀渀 瀀氀甀爀愀氀 挀愀琀攀最漀爀礀 琀漀 椀琀猀 挀漀爀爀攀猀瀀漀渀搀椀渀最 猀琀爀椀渀最猀⸀ഀഀ
 * @param {number=} offset Offset to deduct from the total number.਍ ⨀ഀഀ
 * @example਍    㰀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
      <doc:source>਍        㰀猀挀爀椀瀀琀㸀ഀഀ
          function Ctrl($scope) {਍            ␀猀挀漀瀀攀⸀瀀攀爀猀漀渀㄀ 㴀 ✀䤀最漀爀✀㬀ഀഀ
            $scope.person2 = 'Misko';਍            ␀猀挀漀瀀攀⸀瀀攀爀猀漀渀䌀漀甀渀琀 㴀 ㄀㬀ഀഀ
          }਍        㰀⼀猀挀爀椀瀀琀㸀ഀഀ
        <div ng-controller="Ctrl">਍          倀攀爀猀漀渀 ㄀㨀㰀椀渀瀀甀琀 琀礀瀀攀㴀∀琀攀砀琀∀ 渀最ⴀ洀漀搀攀氀㴀∀瀀攀爀猀漀渀㄀∀ 瘀愀氀甀攀㴀∀䤀最漀爀∀ ⼀㸀㰀戀爀⼀㸀ഀഀ
          Person 2:<input type="text" ng-model="person2" value="Misko" /><br/>਍          一甀洀戀攀爀 漀昀 倀攀漀瀀氀攀㨀㰀椀渀瀀甀琀 琀礀瀀攀㴀∀琀攀砀琀∀ 渀最ⴀ洀漀搀攀氀㴀∀瀀攀爀猀漀渀䌀漀甀渀琀∀ 瘀愀氀甀攀㴀∀㄀∀ ⼀㸀㰀戀爀⼀㸀ഀഀ
਍          㰀℀ⴀⴀⴀ 䔀砀愀洀瀀氀攀 眀椀琀栀 猀椀洀瀀氀攀 瀀氀甀爀愀氀椀稀愀琀椀漀渀 爀甀氀攀猀 昀漀爀 攀渀 氀漀挀愀氀攀 ⴀⴀⴀ㸀ഀഀ
          Without Offset:਍          㰀渀最ⴀ瀀氀甀爀愀氀椀稀攀 挀漀甀渀琀㴀∀瀀攀爀猀漀渀䌀漀甀渀琀∀ഀഀ
                        when="{'0': 'Nobody is viewing.',਍                               ✀漀渀攀✀㨀 ✀㄀ 瀀攀爀猀漀渀 椀猀 瘀椀攀眀椀渀最⸀✀Ⰰഀഀ
                               'other': '{} people are viewing.'}">਍          㰀⼀渀最ⴀ瀀氀甀爀愀氀椀稀攀㸀㰀戀爀㸀ഀഀ
਍          㰀℀ⴀⴀⴀ 䔀砀愀洀瀀氀攀 眀椀琀栀 漀昀昀猀攀琀 ⴀⴀⴀ㸀ഀഀ
          With Offset(2):਍          㰀渀最ⴀ瀀氀甀爀愀氀椀稀攀 挀漀甀渀琀㴀∀瀀攀爀猀漀渀䌀漀甀渀琀∀ 漀昀昀猀攀琀㴀㈀ഀഀ
                        when="{'0': 'Nobody is viewing.',਍                               ✀㄀✀㨀 ✀笀笀瀀攀爀猀漀渀㄀紀紀 椀猀 瘀椀攀眀椀渀最⸀✀Ⰰഀഀ
                               '2': '{{person1}} and {{person2}} are viewing.',਍                               ✀漀渀攀✀㨀 ✀笀笀瀀攀爀猀漀渀㄀紀紀Ⰰ 笀笀瀀攀爀猀漀渀㈀紀紀 愀渀搀 漀渀攀 漀琀栀攀爀 瀀攀爀猀漀渀 愀爀攀 瘀椀攀眀椀渀最⸀✀Ⰰഀഀ
                               'other': '{{person1}}, {{person2}} and {} other people are viewing.'}">਍          㰀⼀渀最ⴀ瀀氀甀爀愀氀椀稀攀㸀ഀഀ
        </div>਍      㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      <doc:scenario>਍        椀琀⠀✀猀栀漀甀氀搀 猀栀漀眀 挀漀爀爀攀挀琀 瀀氀甀爀愀氀椀稀攀搀 猀琀爀椀渀最✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
          expect(element('.doc-example-live ng-pluralize:first').text()).਍                                             琀漀䈀攀⠀✀㄀ 瀀攀爀猀漀渀 椀猀 瘀椀攀眀椀渀最⸀✀⤀㬀ഀഀ
          expect(element('.doc-example-live ng-pluralize:last').text()).਍                                                琀漀䈀攀⠀✀䤀最漀爀 椀猀 瘀椀攀眀椀渀最⸀✀⤀㬀ഀഀ
਍          甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀椀渀瀀甀琀⠀✀瀀攀爀猀漀渀䌀漀甀渀琀✀⤀⸀攀渀琀攀爀⠀✀　✀⤀㬀ഀഀ
          expect(element('.doc-example-live ng-pluralize:first').text()).਍                                               琀漀䈀攀⠀✀一漀戀漀搀礀 椀猀 瘀椀攀眀椀渀最⸀✀⤀㬀ഀഀ
          expect(element('.doc-example-live ng-pluralize:last').text()).਍                                              琀漀䈀攀⠀✀一漀戀漀搀礀 椀猀 瘀椀攀眀椀渀最⸀✀⤀㬀ഀഀ
਍          甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀椀渀瀀甀琀⠀✀瀀攀爀猀漀渀䌀漀甀渀琀✀⤀⸀攀渀琀攀爀⠀✀㈀✀⤀㬀ഀഀ
          expect(element('.doc-example-live ng-pluralize:first').text()).਍                                            琀漀䈀攀⠀✀㈀ 瀀攀漀瀀氀攀 愀爀攀 瘀椀攀眀椀渀最⸀✀⤀㬀ഀഀ
          expect(element('.doc-example-live ng-pluralize:last').text()).਍                              琀漀䈀攀⠀✀䤀最漀爀 愀渀搀 䴀椀猀欀漀 愀爀攀 瘀椀攀眀椀渀最⸀✀⤀㬀ഀഀ
਍          甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀椀渀瀀甀琀⠀✀瀀攀爀猀漀渀䌀漀甀渀琀✀⤀⸀攀渀琀攀爀⠀✀㌀✀⤀㬀ഀഀ
          expect(element('.doc-example-live ng-pluralize:first').text()).਍                                            琀漀䈀攀⠀✀㌀ 瀀攀漀瀀氀攀 愀爀攀 瘀椀攀眀椀渀最⸀✀⤀㬀ഀഀ
          expect(element('.doc-example-live ng-pluralize:last').text()).਍                              琀漀䈀攀⠀✀䤀最漀爀Ⰰ 䴀椀猀欀漀 愀渀搀 漀渀攀 漀琀栀攀爀 瀀攀爀猀漀渀 愀爀攀 瘀椀攀眀椀渀最⸀✀⤀㬀ഀഀ
਍          甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀椀渀瀀甀琀⠀✀瀀攀爀猀漀渀䌀漀甀渀琀✀⤀⸀攀渀琀攀爀⠀✀㐀✀⤀㬀ഀഀ
          expect(element('.doc-example-live ng-pluralize:first').text()).਍                                            琀漀䈀攀⠀✀㐀 瀀攀漀瀀氀攀 愀爀攀 瘀椀攀眀椀渀最⸀✀⤀㬀ഀഀ
          expect(element('.doc-example-live ng-pluralize:last').text()).਍                              琀漀䈀攀⠀✀䤀最漀爀Ⰰ 䴀椀猀欀漀 愀渀搀 ㈀ 漀琀栀攀爀 瀀攀漀瀀氀攀 愀爀攀 瘀椀攀眀椀渀最⸀✀⤀㬀ഀഀ
        });਍ഀഀ
        it('should show data-binded names', function() {਍          甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀椀渀瀀甀琀⠀✀瀀攀爀猀漀渀䌀漀甀渀琀✀⤀⸀攀渀琀攀爀⠀✀㐀✀⤀㬀ഀഀ
          expect(element('.doc-example-live ng-pluralize:last').text()).਍              琀漀䈀攀⠀✀䤀最漀爀Ⰰ 䴀椀猀欀漀 愀渀搀 ㈀ 漀琀栀攀爀 瀀攀漀瀀氀攀 愀爀攀 瘀椀攀眀椀渀最⸀✀⤀㬀ഀഀ
਍          甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀椀渀瀀甀琀⠀✀瀀攀爀猀漀渀㄀✀⤀⸀攀渀琀攀爀⠀✀䐀椀✀⤀㬀ഀഀ
          using('.doc-example-live').input('person2').enter('Vojta');਍          攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 渀最ⴀ瀀氀甀爀愀氀椀稀攀㨀氀愀猀琀✀⤀⸀琀攀砀琀⠀⤀⤀⸀ഀഀ
              toBe('Di, Vojta and 2 other people are viewing.');਍        紀⤀㬀ഀഀ
      </doc:scenario>਍    㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍瘀愀爀 渀最倀氀甀爀愀氀椀稀攀䐀椀爀攀挀琀椀瘀攀 㴀 嬀✀␀氀漀挀愀氀攀✀Ⰰ ✀␀椀渀琀攀爀瀀漀氀愀琀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀氀漀挀愀氀攀Ⰰ ␀椀渀琀攀爀瀀漀氀愀琀攀⤀ 笀ഀഀ
  var BRACE = /{}/g;਍  爀攀琀甀爀渀 笀ഀഀ
    restrict: 'EA',਍    氀椀渀欀㨀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀⤀ 笀ഀഀ
      var numberExp = attr.count,਍          眀栀攀渀䔀砀瀀 㴀 愀琀琀爀⸀␀愀琀琀爀⸀眀栀攀渀 ☀☀ 攀氀攀洀攀渀琀⸀愀琀琀爀⠀愀琀琀爀⸀␀愀琀琀爀⸀眀栀攀渀⤀Ⰰ ⼀⼀ 眀攀 栀愀瘀攀 笀笀紀紀 椀渀 愀琀琀爀猀ഀഀ
          offset = attr.offset || 0,਍          眀栀攀渀猀 㴀 猀挀漀瀀攀⸀␀攀瘀愀氀⠀眀栀攀渀䔀砀瀀⤀ 簀簀 笀紀Ⰰഀഀ
          whensExpFns = {},਍          猀琀愀爀琀匀礀洀戀漀氀 㴀 ␀椀渀琀攀爀瀀漀氀愀琀攀⸀猀琀愀爀琀匀礀洀戀漀氀⠀⤀Ⰰഀഀ
          endSymbol = $interpolate.endSymbol(),਍          椀猀圀栀攀渀 㴀 ⼀帀眀栀攀渀⠀䴀椀渀甀猀⤀㼀⠀⸀⬀⤀␀⼀㬀ഀഀ
਍      昀漀爀䔀愀挀栀⠀愀琀琀爀Ⰰ 昀甀渀挀琀椀漀渀⠀攀砀瀀爀攀猀猀椀漀渀Ⰰ 愀琀琀爀椀戀甀琀攀一愀洀攀⤀ 笀ഀഀ
        if (isWhen.test(attributeName)) {਍          眀栀攀渀猀嬀氀漀眀攀爀挀愀猀攀⠀愀琀琀爀椀戀甀琀攀一愀洀攀⸀爀攀瀀氀愀挀攀⠀✀眀栀攀渀✀Ⰰ ✀✀⤀⸀爀攀瀀氀愀挀攀⠀✀䴀椀渀甀猀✀Ⰰ ✀ⴀ✀⤀⤀崀 㴀ഀഀ
            element.attr(attr.$attr[attributeName]);਍        紀ഀഀ
      });਍      昀漀爀䔀愀挀栀⠀眀栀攀渀猀Ⰰ 昀甀渀挀琀椀漀渀⠀攀砀瀀爀攀猀猀椀漀渀Ⰰ 欀攀礀⤀ 笀ഀഀ
        whensExpFns[key] =਍          ␀椀渀琀攀爀瀀漀氀愀琀攀⠀攀砀瀀爀攀猀猀椀漀渀⸀爀攀瀀氀愀挀攀⠀䈀刀䄀䌀䔀Ⰰ 猀琀愀爀琀匀礀洀戀漀氀 ⬀ 渀甀洀戀攀爀䔀砀瀀 ⬀ ✀ⴀ✀ ⬀ഀഀ
            offset + endSymbol));਍      紀⤀㬀ഀഀ
਍      猀挀漀瀀攀⸀␀眀愀琀挀栀⠀昀甀渀挀琀椀漀渀 渀最倀氀甀爀愀氀椀稀攀圀愀琀挀栀⠀⤀ 笀ഀഀ
        var value = parseFloat(scope.$eval(numberExp));਍ഀഀ
        if (!isNaN(value)) {਍          ⼀⼀椀昀 攀砀瀀氀椀挀椀琀 渀甀洀戀攀爀 爀甀氀攀 猀甀挀栀 愀猀 ㄀Ⰰ ㈀Ⰰ ㌀⸀⸀⸀ 椀猀 搀攀昀椀渀攀搀Ⰰ 樀甀猀琀 甀猀攀 椀琀⸀ 伀琀栀攀爀眀椀猀攀Ⰰഀഀ
          //check it against pluralization rules in $locale service਍          椀昀 ⠀℀⠀瘀愀氀甀攀 椀渀 眀栀攀渀猀⤀⤀ 瘀愀氀甀攀 㴀 ␀氀漀挀愀氀攀⸀瀀氀甀爀愀氀䌀愀琀⠀瘀愀氀甀攀 ⴀ 漀昀昀猀攀琀⤀㬀ഀഀ
           return whensExpFns[value](scope, element, true);਍        紀 攀氀猀攀 笀ഀഀ
          return '';਍        紀ഀഀ
      }, function ngPluralizeWatchAction(newVal) {਍        攀氀攀洀攀渀琀⸀琀攀砀琀⠀渀攀眀嘀愀氀⤀㬀ഀഀ
      });਍    紀ഀഀ
  };਍紀崀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最刀攀瀀攀愀琀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * The `ngRepeat` directive instantiates a template once per item from a collection. Each template਍ ⨀ 椀渀猀琀愀渀挀攀 最攀琀猀 椀琀猀 漀眀渀 猀挀漀瀀攀Ⰰ 眀栀攀爀攀 琀栀攀 最椀瘀攀渀 氀漀漀瀀 瘀愀爀椀愀戀氀攀 椀猀 猀攀琀 琀漀 琀栀攀 挀甀爀爀攀渀琀 挀漀氀氀攀挀琀椀漀渀 椀琀攀洀Ⰰഀഀ
 * and `$index` is set to the item index or key.਍ ⨀ഀഀ
 * Special properties are exposed on the local scope of each template instance, including:਍ ⨀ഀഀ
 * | Variable  | Type            | Details                                                                     |਍ ⨀ 簀ⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀ簀ⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀ簀ⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀⴀ簀ഀഀ
 * | `$index`  | {@type number}  | iterator offset of the repeated element (0..length-1)                       |਍ ⨀ 簀 怀␀昀椀爀猀琀怀  簀 笀䀀琀礀瀀攀 戀漀漀氀攀愀渀紀 簀 琀爀甀攀 椀昀 琀栀攀 爀攀瀀攀愀琀攀搀 攀氀攀洀攀渀琀 椀猀 昀椀爀猀琀 椀渀 琀栀攀 椀琀攀爀愀琀漀爀⸀                      簀ഀഀ
 * | `$middle` | {@type boolean} | true if the repeated element is between the first and last in the iterator. |਍ ⨀ 簀 怀␀氀愀猀琀怀   簀 笀䀀琀礀瀀攀 戀漀漀氀攀愀渀紀 簀 琀爀甀攀 椀昀 琀栀攀 爀攀瀀攀愀琀攀搀 攀氀攀洀攀渀琀 椀猀 氀愀猀琀 椀渀 琀栀攀 椀琀攀爀愀琀漀爀⸀                       簀ഀഀ
 * | `$even`   | {@type boolean} | true if the iterator position `$index` is even (otherwise false).           |਍ ⨀ 簀 怀␀漀搀搀怀    簀 笀䀀琀礀瀀攀 戀漀漀氀攀愀渀紀 簀 琀爀甀攀 椀昀 琀栀攀 椀琀攀爀愀琀漀爀 瀀漀猀椀琀椀漀渀 怀␀椀渀搀攀砀怀 椀猀 漀搀搀 ⠀漀琀栀攀爀眀椀猀攀 昀愀氀猀攀⤀⸀            簀ഀഀ
 *਍ ⨀ 䌀爀攀愀琀椀渀最 愀氀椀愀猀攀猀 昀漀爀 琀栀攀猀攀 瀀爀漀瀀攀爀琀椀攀猀 椀猀 瀀漀猀猀椀戀氀攀 眀椀琀栀 笀䀀氀椀渀欀 愀瀀椀⼀渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䤀渀椀琀 怀渀最䤀渀椀琀怀紀⸀ഀഀ
 * This may be useful when, for instance, nesting ngRepeats.਍ ⨀ഀഀ
 * # Special repeat start and end points਍ ⨀ 吀漀 爀攀瀀攀愀琀 愀 猀攀爀椀攀猀 漀昀 攀氀攀洀攀渀琀猀 椀渀猀琀攀愀搀 漀昀 樀甀猀琀 漀渀攀 瀀愀爀攀渀琀 攀氀攀洀攀渀琀Ⰰ 渀最刀攀瀀攀愀琀 ⠀愀猀 眀攀氀氀 愀猀 漀琀栀攀爀 渀最 搀椀爀攀挀琀椀瘀攀猀⤀ 猀甀瀀瀀漀爀琀猀 攀砀琀攀渀搀椀渀最ഀഀ
 * the range of the repeater by defining explicit start and end points by using **ng-repeat-start** and **ng-repeat-end** respectively.਍ ⨀ 吀栀攀 ⨀⨀渀最ⴀ爀攀瀀攀愀琀ⴀ猀琀愀爀琀⨀⨀ 搀椀爀攀挀琀椀瘀攀 眀漀爀欀猀 琀栀攀 猀愀洀攀 愀猀 ⨀⨀渀最ⴀ爀攀瀀攀愀琀⨀⨀Ⰰ 戀甀琀 眀椀氀氀 爀攀瀀攀愀琀 愀氀氀 琀栀攀 䠀吀䴀䰀 挀漀搀攀 ⠀椀渀挀氀甀搀椀渀最 琀栀攀 琀愀最 椀琀✀猀 搀攀昀椀渀攀搀 漀渀⤀ഀഀ
 * up to and including the ending HTML tag where **ng-repeat-end** is placed.਍ ⨀ഀഀ
 * The example below makes use of this feature:਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 *   <header ng-repeat-start="item in items">਍ ⨀     䠀攀愀搀攀爀 笀笀 椀琀攀洀 紀紀ഀഀ
 *   </header>਍ ⨀   㰀搀椀瘀 挀氀愀猀猀㴀∀戀漀搀礀∀㸀ഀഀ
 *     Body {{ item }}਍ ⨀   㰀⼀搀椀瘀㸀ഀഀ
 *   <footer ng-repeat-end>਍ ⨀     䘀漀漀琀攀爀 笀笀 椀琀攀洀 紀紀ഀഀ
 *   </footer>਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 䄀渀搀 眀椀琀栀 愀渀 椀渀瀀甀琀 漀昀 笀䀀琀礀瀀攀 嬀✀䄀✀Ⰰ✀䈀✀崀紀 昀漀爀 琀栀攀 椀琀攀洀猀 瘀愀爀椀愀戀氀攀 椀渀 琀栀攀 攀砀愀洀瀀氀攀 愀戀漀瘀攀Ⰰ 琀栀攀 漀甀琀瀀甀琀 眀椀氀氀 攀瘀愀氀甀愀琀攀 琀漀㨀ഀഀ
 * <pre>਍ ⨀   㰀栀攀愀搀攀爀㸀ഀഀ
 *     Header A਍ ⨀   㰀⼀栀攀愀搀攀爀㸀ഀഀ
 *   <div class="body">਍ ⨀     䈀漀搀礀 䄀ഀഀ
 *   </div>਍ ⨀   㰀昀漀漀琀攀爀㸀ഀഀ
 *     Footer A਍ ⨀   㰀⼀昀漀漀琀攀爀㸀ഀഀ
 *   <header>਍ ⨀     䠀攀愀搀攀爀 䈀ഀഀ
 *   </header>਍ ⨀   㰀搀椀瘀 挀氀愀猀猀㴀∀戀漀搀礀∀㸀ഀഀ
 *     Body B਍ ⨀   㰀⼀搀椀瘀㸀ഀഀ
 *   <footer>਍ ⨀     䘀漀漀琀攀爀 䈀ഀഀ
 *   </footer>਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 吀栀攀 挀甀猀琀漀洀 猀琀愀爀琀 愀渀搀 攀渀搀 瀀漀椀渀琀猀 昀漀爀 渀最刀攀瀀攀愀琀 愀氀猀漀 猀甀瀀瀀漀爀琀 愀氀氀 漀琀栀攀爀 䠀吀䴀䰀 搀椀爀攀挀琀椀瘀攀 猀礀渀琀愀砀 昀氀愀瘀漀爀猀 瀀爀漀瘀椀搀攀搀 椀渀 䄀渀最甀氀愀爀䨀匀 ⠀猀甀挀栀ഀഀ
 * as **data-ng-repeat-start**, **x-ng-repeat-start** and **ng:repeat-start**).਍ ⨀ഀഀ
 * @animations਍ ⨀ 攀渀琀攀爀 ⴀ 眀栀攀渀 愀 渀攀眀 椀琀攀洀 椀猀 愀搀搀攀搀 琀漀 琀栀攀 氀椀猀琀 漀爀 眀栀攀渀 愀渀 椀琀攀洀 椀猀 爀攀瘀攀愀氀攀搀 愀昀琀攀爀 愀 昀椀氀琀攀爀ഀഀ
 * leave - when an item is removed from the list or when an item is filtered out਍ ⨀ 洀漀瘀攀 ⴀ 眀栀攀渀 愀渀 愀搀樀愀挀攀渀琀 椀琀攀洀 椀猀 昀椀氀琀攀爀攀搀 漀甀琀 挀愀甀猀椀渀最 愀 爀攀漀爀搀攀爀 漀爀 眀栀攀渀 琀栀攀 椀琀攀洀 挀漀渀琀攀渀琀猀 愀爀攀 爀攀漀爀搀攀爀攀搀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䄀一夀ഀഀ
 * @scope਍ ⨀ 䀀瀀爀椀漀爀椀琀礀 ㄀　　　ഀഀ
 * @param {repeat_expression} ngRepeat The expression indicating how to enumerate a collection. These਍ ⨀   昀漀爀洀愀琀猀 愀爀攀 挀甀爀爀攀渀琀氀礀 猀甀瀀瀀漀爀琀攀搀㨀ഀഀ
 *਍ ⨀   ⨀ 怀瘀愀爀椀愀戀氀攀 椀渀 攀砀瀀爀攀猀猀椀漀渀怀 ﴀ﷿⃿眀栀攀爀攀 瘀愀爀椀愀戀氀攀 椀猀 琀栀攀 甀猀攀爀 搀攀昀椀渀攀搀 氀漀漀瀀 瘀愀爀椀愀戀氀攀 愀渀搀 怀攀砀瀀爀攀猀猀椀漀渀怀ഀഀ
 *     is a scope expression giving the collection to enumerate.਍ ⨀ഀഀ
 *     For example: `album in artist.albums`.਍ ⨀ഀഀ
 *   * `(key, value) in expression` �� where `key` and `value` can be any user defined identifiers,਍ ⨀     愀渀搀 怀攀砀瀀爀攀猀猀椀漀渀怀 椀猀 琀栀攀 猀挀漀瀀攀 攀砀瀀爀攀猀猀椀漀渀 最椀瘀椀渀最 琀栀攀 挀漀氀氀攀挀琀椀漀渀 琀漀 攀渀甀洀攀爀愀琀攀⸀ഀഀ
 *਍ ⨀     䘀漀爀 攀砀愀洀瀀氀攀㨀 怀⠀渀愀洀攀Ⰰ 愀最攀⤀ 椀渀 笀✀愀搀愀洀✀㨀㄀　Ⰰ ✀愀洀愀氀椀攀✀㨀㄀㈀紀怀⸀ഀഀ
 *਍ ⨀   ⨀ 怀瘀愀爀椀愀戀氀攀 椀渀 攀砀瀀爀攀猀猀椀漀渀 琀爀愀挀欀 戀礀 琀爀愀挀欀椀渀最开攀砀瀀爀攀猀猀椀漀渀怀 ﴀ﷿⃿夀漀甀 挀愀渀 愀氀猀漀 瀀爀漀瘀椀搀攀 愀渀 漀瀀琀椀漀渀愀氀 琀爀愀挀欀椀渀最 昀甀渀挀琀椀漀渀ഀഀ
 *     which can be used to associate the objects in the collection with the DOM elements. If no tracking function਍ ⨀     椀猀 猀瀀攀挀椀昀椀攀搀 琀栀攀 渀最ⴀ爀攀瀀攀愀琀 愀猀猀漀挀椀愀琀攀猀 攀氀攀洀攀渀琀猀 戀礀 椀搀攀渀琀椀琀礀 椀渀 琀栀攀 挀漀氀氀攀挀琀椀漀渀⸀ 䤀琀 椀猀 愀渀 攀爀爀漀爀 琀漀 栀愀瘀攀ഀഀ
 *     more than one tracking function to resolve to the same key. (This would mean that two distinct objects are਍ ⨀     洀愀瀀瀀攀搀 琀漀 琀栀攀 猀愀洀攀 䐀伀䴀 攀氀攀洀攀渀琀Ⰰ 眀栀椀挀栀 椀猀 渀漀琀 瀀漀猀猀椀戀氀攀⸀⤀  䘀椀氀琀攀爀猀 猀栀漀甀氀搀 戀攀 愀瀀瀀氀椀攀搀 琀漀 琀栀攀 攀砀瀀爀攀猀猀椀漀渀Ⰰഀഀ
 *     before specifying a tracking expression.਍ ⨀ഀഀ
 *     For example: `item in items` is equivalent to `item in items track by $id(item)'. This implies that the DOM elements਍ ⨀     眀椀氀氀 戀攀 愀猀猀漀挀椀愀琀攀搀 戀礀 椀琀攀洀 椀搀攀渀琀椀琀礀 椀渀 琀栀攀 愀爀爀愀礀⸀ഀഀ
 *਍ ⨀     䘀漀爀 攀砀愀洀瀀氀攀㨀 怀椀琀攀洀 椀渀 椀琀攀洀猀 琀爀愀挀欀 戀礀 ␀椀搀⠀椀琀攀洀⤀怀⸀ 䄀 戀甀椀氀琀 椀渀 怀␀椀搀⠀⤀怀 昀甀渀挀琀椀漀渀 挀愀渀 戀攀 甀猀攀搀 琀漀 愀猀猀椀最渀 愀 甀渀椀焀甀攀ഀഀ
 *     `$$hashKey` property to each item in the array. This property is then used as a key to associated DOM elements਍ ⨀     眀椀琀栀 琀栀攀 挀漀爀爀攀猀瀀漀渀搀椀渀最 椀琀攀洀 椀渀 琀栀攀 愀爀爀愀礀 戀礀 椀搀攀渀琀椀琀礀⸀ 䴀漀瘀椀渀最 琀栀攀 猀愀洀攀 漀戀樀攀挀琀 椀渀 愀爀爀愀礀 眀漀甀氀搀 洀漀瘀攀 琀栀攀 䐀伀䴀ഀഀ
 *     element in the same way in the DOM.਍ ⨀ഀഀ
 *     For example: `item in items track by item.id` is a typical pattern when the items come from the database. In this਍ ⨀     挀愀猀攀 琀栀攀 漀戀樀攀挀琀 椀搀攀渀琀椀琀礀 搀漀攀猀 渀漀琀 洀愀琀琀攀爀⸀ 吀眀漀 漀戀樀攀挀琀猀 愀爀攀 挀漀渀猀椀搀攀爀攀搀 攀焀甀椀瘀愀氀攀渀琀 愀猀 氀漀渀最 愀猀 琀栀攀椀爀 怀椀搀怀ഀഀ
 *     property is same.਍ ⨀ഀഀ
 *     For example: `item in items | filter:searchText track by item.id` is a pattern that might be used to apply a filter਍ ⨀     琀漀 椀琀攀洀猀 椀渀 挀漀渀樀甀渀挀琀椀漀渀 眀椀琀栀 愀 琀爀愀挀欀椀渀最 攀砀瀀爀攀猀猀椀漀渀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
 * This example initializes the scope to a list of names and਍ ⨀ 琀栀攀渀 甀猀攀猀 怀渀最刀攀瀀攀愀琀怀 琀漀 搀椀猀瀀氀愀礀 攀瘀攀爀礀 瀀攀爀猀漀渀㨀ഀഀ
  <example animations="true">਍    㰀昀椀氀攀 渀愀洀攀㴀∀椀渀搀攀砀⸀栀琀洀氀∀㸀ഀഀ
      <div ng-init="friends = [਍        笀渀愀洀攀㨀✀䨀漀栀渀✀Ⰰ 愀最攀㨀㈀㔀Ⰰ 最攀渀搀攀爀㨀✀戀漀礀✀紀Ⰰഀഀ
        {name:'Jessie', age:30, gender:'girl'},਍        笀渀愀洀攀㨀✀䨀漀栀愀渀渀愀✀Ⰰ 愀最攀㨀㈀㠀Ⰰ 最攀渀搀攀爀㨀✀最椀爀氀✀紀Ⰰഀഀ
        {name:'Joy', age:15, gender:'girl'},਍        笀渀愀洀攀㨀✀䴀愀爀礀✀Ⰰ 愀最攀㨀㈀㠀Ⰰ 最攀渀搀攀爀㨀✀最椀爀氀✀紀Ⰰഀഀ
        {name:'Peter', age:95, gender:'boy'},਍        笀渀愀洀攀㨀✀匀攀戀愀猀琀椀愀渀✀Ⰰ 愀最攀㨀㔀　Ⰰ 最攀渀搀攀爀㨀✀戀漀礀✀紀Ⰰഀഀ
        {name:'Erika', age:27, gender:'girl'},਍        笀渀愀洀攀㨀✀倀愀琀爀椀挀欀✀Ⰰ 愀最攀㨀㐀　Ⰰ 最攀渀搀攀爀㨀✀戀漀礀✀紀Ⰰഀഀ
        {name:'Samantha', age:60, gender:'girl'}਍      崀∀㸀ഀഀ
        I have {{friends.length}} friends. They are:਍        㰀椀渀瀀甀琀 琀礀瀀攀㴀∀猀攀愀爀挀栀∀ 渀最ⴀ洀漀搀攀氀㴀∀焀∀ 瀀氀愀挀攀栀漀氀搀攀爀㴀∀昀椀氀琀攀爀 昀爀椀攀渀搀猀⸀⸀⸀∀ ⼀㸀ഀഀ
        <ul class="example-animate-container">਍          㰀氀椀 挀氀愀猀猀㴀∀愀渀椀洀愀琀攀ⴀ爀攀瀀攀愀琀∀ 渀最ⴀ爀攀瀀攀愀琀㴀∀昀爀椀攀渀搀 椀渀 昀爀椀攀渀搀猀 簀 昀椀氀琀攀爀㨀焀∀㸀ഀഀ
            [{{$index + 1}}] {{friend.name}} who is {{friend.age}} years old.਍          㰀⼀氀椀㸀ഀഀ
        </ul>਍      㰀⼀搀椀瘀㸀ഀഀ
    </file>਍    㰀昀椀氀攀 渀愀洀攀㴀∀愀渀椀洀愀琀椀漀渀猀⸀挀猀猀∀㸀ഀഀ
      .example-animate-container {਍        戀愀挀欀最爀漀甀渀搀㨀眀栀椀琀攀㬀ഀഀ
        border:1px solid black;਍        氀椀猀琀ⴀ猀琀礀氀攀㨀渀漀渀攀㬀ഀഀ
        margin:0;਍        瀀愀搀搀椀渀最㨀　 ㄀　瀀砀㬀ഀഀ
      }਍ഀഀ
      .animate-repeat {਍        氀椀渀攀ⴀ栀攀椀最栀琀㨀㐀　瀀砀㬀ഀഀ
        list-style:none;਍        戀漀砀ⴀ猀椀稀椀渀最㨀戀漀爀搀攀爀ⴀ戀漀砀㬀ഀഀ
      }਍ഀഀ
      .animate-repeat.ng-move,਍      ⸀愀渀椀洀愀琀攀ⴀ爀攀瀀攀愀琀⸀渀最ⴀ攀渀琀攀爀Ⰰഀഀ
      .animate-repeat.ng-leave {਍        ⴀ眀攀戀欀椀琀ⴀ琀爀愀渀猀椀琀椀漀渀㨀愀氀氀 氀椀渀攀愀爀 　⸀㔀猀㬀ഀഀ
        transition:all linear 0.5s;਍      紀ഀഀ
਍      ⸀愀渀椀洀愀琀攀ⴀ爀攀瀀攀愀琀⸀渀最ⴀ氀攀愀瘀攀⸀渀最ⴀ氀攀愀瘀攀ⴀ愀挀琀椀瘀攀Ⰰഀഀ
      .animate-repeat.ng-move,਍      ⸀愀渀椀洀愀琀攀ⴀ爀攀瀀攀愀琀⸀渀最ⴀ攀渀琀攀爀 笀ഀഀ
        opacity:0;਍        洀愀砀ⴀ栀攀椀最栀琀㨀　㬀ഀഀ
      }਍ഀഀ
      .animate-repeat.ng-leave,਍      ⸀愀渀椀洀愀琀攀ⴀ爀攀瀀攀愀琀⸀渀最ⴀ洀漀瘀攀⸀渀最ⴀ洀漀瘀攀ⴀ愀挀琀椀瘀攀Ⰰഀഀ
      .animate-repeat.ng-enter.ng-enter-active {਍        漀瀀愀挀椀琀礀㨀㄀㬀ഀഀ
        max-height:40px;਍      紀ഀഀ
    </file>਍    㰀昀椀氀攀 渀愀洀攀㴀∀猀挀攀渀愀爀椀漀⸀樀猀∀㸀ഀഀ
       it('should render initial data set', function() {਍         瘀愀爀 爀 㴀 甀猀椀渀最⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀✀⤀⸀爀攀瀀攀愀琀攀爀⠀✀甀氀 氀椀✀⤀㬀ഀഀ
         expect(r.count()).toBe(10);਍         攀砀瀀攀挀琀⠀爀⸀爀漀眀⠀　⤀⤀⸀琀漀䔀焀甀愀氀⠀嬀∀㄀∀Ⰰ∀䨀漀栀渀∀Ⰰ∀㈀㔀∀崀⤀㬀ഀഀ
         expect(r.row(1)).toEqual(["2","Jessie","30"]);਍         攀砀瀀攀挀琀⠀爀⸀爀漀眀⠀㤀⤀⤀⸀琀漀䔀焀甀愀氀⠀嬀∀㄀　∀Ⰰ∀匀愀洀愀渀琀栀愀∀Ⰰ∀㘀　∀崀⤀㬀ഀഀ
         expect(binding('friends.length')).toBe("10");਍       紀⤀㬀ഀഀ
਍       椀琀⠀✀猀栀漀甀氀搀 甀瀀搀愀琀攀 爀攀瀀攀愀琀攀爀 眀栀攀渀 昀椀氀琀攀爀 瀀爀攀搀椀挀愀琀攀 挀栀愀渀最攀猀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         var r = using('.doc-example-live').repeater('ul li');਍         攀砀瀀攀挀琀⠀爀⸀挀漀甀渀琀⠀⤀⤀⸀琀漀䈀攀⠀㄀　⤀㬀ഀഀ
਍         椀渀瀀甀琀⠀✀焀✀⤀⸀攀渀琀攀爀⠀✀洀愀✀⤀㬀ഀഀ
਍         攀砀瀀攀挀琀⠀爀⸀挀漀甀渀琀⠀⤀⤀⸀琀漀䈀攀⠀㈀⤀㬀ഀഀ
         expect(r.row(0)).toEqual(["1","Mary","28"]);਍         攀砀瀀攀挀琀⠀爀⸀爀漀眀⠀㄀⤀⤀⸀琀漀䔀焀甀愀氀⠀嬀∀㈀∀Ⰰ∀匀愀洀愀渀琀栀愀∀Ⰰ∀㘀　∀崀⤀㬀ഀഀ
       });਍      㰀⼀昀椀氀攀㸀ഀഀ
    </example>਍ ⨀⼀ഀഀ
var ngRepeatDirective = ['$parse', '$animate', function($parse, $animate) {਍  瘀愀爀 一䜀开刀䔀䴀伀嘀䔀䐀 㴀 ✀␀␀一䜀开刀䔀䴀伀嘀䔀䐀✀㬀ഀഀ
  var ngRepeatMinErr = minErr('ngRepeat');਍  爀攀琀甀爀渀 笀ഀഀ
    transclude: 'element',਍    瀀爀椀漀爀椀琀礀㨀 ㄀　　　Ⰰഀഀ
    terminal: true,਍    ␀␀琀氀戀㨀 琀爀甀攀Ⰰഀഀ
    link: function($scope, $element, $attr, ctrl, $transclude){਍        瘀愀爀 攀砀瀀爀攀猀猀椀漀渀 㴀 ␀愀琀琀爀⸀渀最刀攀瀀攀愀琀㬀ഀഀ
        var match = expression.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?\s*$/),਍          琀爀愀挀欀䈀礀䔀砀瀀Ⰰ 琀爀愀挀欀䈀礀䔀砀瀀䜀攀琀琀攀爀Ⰰ 琀爀愀挀欀䈀礀䤀搀䔀砀瀀䘀渀Ⰰ 琀爀愀挀欀䈀礀䤀搀䄀爀爀愀礀䘀渀Ⰰ 琀爀愀挀欀䈀礀䤀搀伀戀樀䘀渀Ⰰഀഀ
          lhs, rhs, valueIdentifier, keyIdentifier,਍          栀愀猀栀䘀渀䰀漀挀愀氀猀 㴀 笀␀椀搀㨀 栀愀猀栀䬀攀礀紀㬀ഀഀ
਍        椀昀 ⠀℀洀愀琀挀栀⤀ 笀ഀഀ
          throw ngRepeatMinErr('iexp', "Expected expression in form of '_item_ in _collection_[ track by _id_]' but got '{0}'.",਍            攀砀瀀爀攀猀猀椀漀渀⤀㬀ഀഀ
        }਍ഀഀ
        lhs = match[1];਍        爀栀猀 㴀 洀愀琀挀栀嬀㈀崀㬀ഀഀ
        trackByExp = match[3];਍ഀഀ
        if (trackByExp) {਍          琀爀愀挀欀䈀礀䔀砀瀀䜀攀琀琀攀爀 㴀 ␀瀀愀爀猀攀⠀琀爀愀挀欀䈀礀䔀砀瀀⤀㬀ഀഀ
          trackByIdExpFn = function(key, value, index) {਍            ⼀⼀ 愀猀猀椀最渀 欀攀礀Ⰰ 瘀愀氀甀攀Ⰰ 愀渀搀 ␀椀渀搀攀砀 琀漀 琀栀攀 氀漀挀愀氀猀 猀漀 琀栀愀琀 琀栀攀礀 挀愀渀 戀攀 甀猀攀搀 椀渀 栀愀猀栀 昀甀渀挀琀椀漀渀猀ഀഀ
            if (keyIdentifier) hashFnLocals[keyIdentifier] = key;਍            栀愀猀栀䘀渀䰀漀挀愀氀猀嬀瘀愀氀甀攀䤀搀攀渀琀椀昀椀攀爀崀 㴀 瘀愀氀甀攀㬀ഀഀ
            hashFnLocals.$index = index;਍            爀攀琀甀爀渀 琀爀愀挀欀䈀礀䔀砀瀀䜀攀琀琀攀爀⠀␀猀挀漀瀀攀Ⰰ 栀愀猀栀䘀渀䰀漀挀愀氀猀⤀㬀ഀഀ
          };਍        紀 攀氀猀攀 笀ഀഀ
          trackByIdArrayFn = function(key, value) {਍            爀攀琀甀爀渀 栀愀猀栀䬀攀礀⠀瘀愀氀甀攀⤀㬀ഀഀ
          };਍          琀爀愀挀欀䈀礀䤀搀伀戀樀䘀渀 㴀 昀甀渀挀琀椀漀渀⠀欀攀礀⤀ 笀ഀഀ
            return key;਍          紀㬀ഀഀ
        }਍ഀഀ
        match = lhs.match(/^(?:([\$\w]+)|\(([\$\w]+)\s*,\s*([\$\w]+)\))$/);਍        椀昀 ⠀℀洀愀琀挀栀⤀ 笀ഀഀ
          throw ngRepeatMinErr('iidexp', "'_item_' in '_item_ in _collection_' should be an identifier or '(_key_, _value_)' expression, but got '{0}'.",਍                                                                    氀栀猀⤀㬀ഀഀ
        }਍        瘀愀氀甀攀䤀搀攀渀琀椀昀椀攀爀 㴀 洀愀琀挀栀嬀㌀崀 簀簀 洀愀琀挀栀嬀㄀崀㬀ഀഀ
        keyIdentifier = match[2];਍ഀഀ
        // Store a list of elements from previous run. This is a hash where key is the item from the਍        ⼀⼀ 椀琀攀爀愀琀漀爀Ⰰ 愀渀搀 琀栀攀 瘀愀氀甀攀 椀猀 漀戀樀攀挀琀猀 眀椀琀栀 昀漀氀氀漀眀椀渀最 瀀爀漀瀀攀爀琀椀攀猀⸀ഀഀ
        //   - scope: bound scope਍        ⼀⼀   ⴀ 攀氀攀洀攀渀琀㨀 瀀爀攀瘀椀漀甀猀 攀氀攀洀攀渀琀⸀ഀഀ
        //   - index: position਍        瘀愀爀 氀愀猀琀䈀氀漀挀欀䴀愀瀀 㴀 笀紀㬀ഀഀ
਍        ⼀⼀眀愀琀挀栀 瀀爀漀瀀猀ഀഀ
        $scope.$watchCollection(rhs, function ngRepeatAction(collection){਍          瘀愀爀 椀渀搀攀砀Ⰰ 氀攀渀最琀栀Ⰰഀഀ
              previousNode = $element[0],     // current position of the node਍              渀攀砀琀一漀搀攀Ⰰഀഀ
              // Same as lastBlockMap but it has the current state. It will become the਍              ⼀⼀ 氀愀猀琀䈀氀漀挀欀䴀愀瀀 漀渀 琀栀攀 渀攀砀琀 椀琀攀爀愀琀椀漀渀⸀ഀഀ
              nextBlockMap = {},਍              愀爀爀愀礀䰀攀渀最琀栀Ⰰഀഀ
              childScope,਍              欀攀礀Ⰰ 瘀愀氀甀攀Ⰰ ⼀⼀ 欀攀礀⼀瘀愀氀甀攀 漀昀 椀琀攀爀愀琀椀漀渀ഀഀ
              trackById,਍              琀爀愀挀欀䈀礀䤀搀䘀渀Ⰰഀഀ
              collectionKeys,਍              戀氀漀挀欀Ⰰ       ⼀⼀ 氀愀猀琀 漀戀樀攀挀琀 椀渀昀漀爀洀愀琀椀漀渀 笀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 椀搀紀ഀഀ
              nextBlockOrder = [],਍              攀氀攀洀攀渀琀猀吀漀刀攀洀漀瘀攀㬀ഀഀ
਍ഀഀ
          if (isArrayLike(collection)) {਍            挀漀氀氀攀挀琀椀漀渀䬀攀礀猀 㴀 挀漀氀氀攀挀琀椀漀渀㬀ഀഀ
            trackByIdFn = trackByIdExpFn || trackByIdArrayFn;਍          紀 攀氀猀攀 笀ഀഀ
            trackByIdFn = trackByIdExpFn || trackByIdObjFn;਍            ⼀⼀ 椀昀 漀戀樀攀挀琀Ⰰ 攀砀琀爀愀挀琀 欀攀礀猀Ⰰ 猀漀爀琀 琀栀攀洀 愀渀搀 甀猀攀 琀漀 搀攀琀攀爀洀椀渀攀 漀爀搀攀爀 漀昀 椀琀攀爀愀琀椀漀渀 漀瘀攀爀 漀戀樀 瀀爀漀瀀猀ഀഀ
            collectionKeys = [];਍            昀漀爀 ⠀欀攀礀 椀渀 挀漀氀氀攀挀琀椀漀渀⤀ 笀ഀഀ
              if (collection.hasOwnProperty(key) && key.charAt(0) != '$') {਍                挀漀氀氀攀挀琀椀漀渀䬀攀礀猀⸀瀀甀猀栀⠀欀攀礀⤀㬀ഀഀ
              }਍            紀ഀഀ
            collectionKeys.sort();਍          紀ഀഀ
਍          愀爀爀愀礀䰀攀渀最琀栀 㴀 挀漀氀氀攀挀琀椀漀渀䬀攀礀猀⸀氀攀渀最琀栀㬀ഀഀ
਍          ⼀⼀ 氀漀挀愀琀攀 攀砀椀猀琀椀渀最 椀琀攀洀猀ഀഀ
          length = nextBlockOrder.length = collectionKeys.length;਍          昀漀爀⠀椀渀搀攀砀 㴀 　㬀 椀渀搀攀砀 㰀 氀攀渀最琀栀㬀 椀渀搀攀砀⬀⬀⤀ 笀ഀഀ
           key = (collection === collectionKeys) ? index : collectionKeys[index];਍           瘀愀氀甀攀 㴀 挀漀氀氀攀挀琀椀漀渀嬀欀攀礀崀㬀ഀഀ
           trackById = trackByIdFn(key, value, index);਍           愀猀猀攀爀琀一漀琀䠀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀琀爀愀挀欀䈀礀䤀搀Ⰰ ✀怀琀爀愀挀欀 戀礀怀 椀搀✀⤀㬀ഀഀ
           if(lastBlockMap.hasOwnProperty(trackById)) {਍             戀氀漀挀欀 㴀 氀愀猀琀䈀氀漀挀欀䴀愀瀀嬀琀爀愀挀欀䈀礀䤀搀崀㬀ഀഀ
             delete lastBlockMap[trackById];਍             渀攀砀琀䈀氀漀挀欀䴀愀瀀嬀琀爀愀挀欀䈀礀䤀搀崀 㴀 戀氀漀挀欀㬀ഀഀ
             nextBlockOrder[index] = block;਍           紀 攀氀猀攀 椀昀 ⠀渀攀砀琀䈀氀漀挀欀䴀愀瀀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀琀爀愀挀欀䈀礀䤀搀⤀⤀ 笀ഀഀ
             // restore lastBlockMap਍             昀漀爀䔀愀挀栀⠀渀攀砀琀䈀氀漀挀欀伀爀搀攀爀Ⰰ 昀甀渀挀琀椀漀渀⠀戀氀漀挀欀⤀ 笀ഀഀ
               if (block && block.scope) lastBlockMap[block.id] = block;਍             紀⤀㬀ഀഀ
             // This is a duplicate and we need to throw an error਍             琀栀爀漀眀 渀最刀攀瀀攀愀琀䴀椀渀䔀爀爀⠀✀搀甀瀀攀猀✀Ⰰ ∀䐀甀瀀氀椀挀愀琀攀猀 椀渀 愀 爀攀瀀攀愀琀攀爀 愀爀攀 渀漀琀 愀氀氀漀眀攀搀⸀ 唀猀攀 ✀琀爀愀挀欀 戀礀✀ 攀砀瀀爀攀猀猀椀漀渀 琀漀 猀瀀攀挀椀昀礀 甀渀椀焀甀攀 欀攀礀猀⸀ 刀攀瀀攀愀琀攀爀㨀 笀　紀Ⰰ 䐀甀瀀氀椀挀愀琀攀 欀攀礀㨀 笀㄀紀∀Ⰰഀഀ
                                                                                                                                                    expression,       trackById);਍           紀 攀氀猀攀 笀ഀഀ
             // new never before seen block਍             渀攀砀琀䈀氀漀挀欀伀爀搀攀爀嬀椀渀搀攀砀崀 㴀 笀 椀搀㨀 琀爀愀挀欀䈀礀䤀搀 紀㬀ഀഀ
             nextBlockMap[trackById] = false;਍           紀ഀഀ
         }਍ഀഀ
          // remove existing items਍          昀漀爀 ⠀欀攀礀 椀渀 氀愀猀琀䈀氀漀挀欀䴀愀瀀⤀ 笀ഀഀ
            // lastBlockMap is our own object so we don't need to use special hasOwnPropertyFn਍            椀昀 ⠀氀愀猀琀䈀氀漀挀欀䴀愀瀀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀欀攀礀⤀⤀ 笀ഀഀ
              block = lastBlockMap[key];਍              攀氀攀洀攀渀琀猀吀漀刀攀洀漀瘀攀 㴀 最攀琀䈀氀漀挀欀䔀氀攀洀攀渀琀猀⠀戀氀漀挀欀⸀挀氀漀渀攀⤀㬀ഀഀ
              $animate.leave(elementsToRemove);਍              昀漀爀䔀愀挀栀⠀攀氀攀洀攀渀琀猀吀漀刀攀洀漀瘀攀Ⰰ 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀⤀ 笀 攀氀攀洀攀渀琀嬀一䜀开刀䔀䴀伀嘀䔀䐀崀 㴀 琀爀甀攀㬀 紀⤀㬀ഀഀ
              block.scope.$destroy();਍            紀ഀഀ
          }਍ഀഀ
          // we are not using forEach for perf reasons (trying to avoid #call)਍          昀漀爀 ⠀椀渀搀攀砀 㴀 　Ⰰ 氀攀渀最琀栀 㴀 挀漀氀氀攀挀琀椀漀渀䬀攀礀猀⸀氀攀渀最琀栀㬀 椀渀搀攀砀 㰀 氀攀渀最琀栀㬀 椀渀搀攀砀⬀⬀⤀ 笀ഀഀ
            key = (collection === collectionKeys) ? index : collectionKeys[index];਍            瘀愀氀甀攀 㴀 挀漀氀氀攀挀琀椀漀渀嬀欀攀礀崀㬀ഀഀ
            block = nextBlockOrder[index];਍            椀昀 ⠀渀攀砀琀䈀氀漀挀欀伀爀搀攀爀嬀椀渀搀攀砀 ⴀ ㄀崀⤀ 瀀爀攀瘀椀漀甀猀一漀搀攀 㴀 最攀琀䈀氀漀挀欀䔀渀搀⠀渀攀砀琀䈀氀漀挀欀伀爀搀攀爀嬀椀渀搀攀砀 ⴀ ㄀崀⤀㬀ഀഀ
਍            椀昀 ⠀戀氀漀挀欀⸀猀挀漀瀀攀⤀ 笀ഀഀ
              // if we have already seen this object, then we need to reuse the਍              ⼀⼀ 愀猀猀漀挀椀愀琀攀搀 猀挀漀瀀攀⼀攀氀攀洀攀渀琀ഀഀ
              childScope = block.scope;਍ഀഀ
              nextNode = previousNode;਍              搀漀 笀ഀഀ
                nextNode = nextNode.nextSibling;਍              紀 眀栀椀氀攀⠀渀攀砀琀一漀搀攀 ☀☀ 渀攀砀琀一漀搀攀嬀一䜀开刀䔀䴀伀嘀䔀䐀崀⤀㬀ഀഀ
਍              椀昀 ⠀最攀琀䈀氀漀挀欀匀琀愀爀琀⠀戀氀漀挀欀⤀ ℀㴀 渀攀砀琀一漀搀攀⤀ 笀ഀഀ
                // existing item which got moved਍                ␀愀渀椀洀愀琀攀⸀洀漀瘀攀⠀最攀琀䈀氀漀挀欀䔀氀攀洀攀渀琀猀⠀戀氀漀挀欀⸀挀氀漀渀攀⤀Ⰰ 渀甀氀氀Ⰰ 樀焀䰀椀琀攀⠀瀀爀攀瘀椀漀甀猀一漀搀攀⤀⤀㬀ഀഀ
              }਍              瀀爀攀瘀椀漀甀猀一漀搀攀 㴀 最攀琀䈀氀漀挀欀䔀渀搀⠀戀氀漀挀欀⤀㬀ഀഀ
            } else {਍              ⼀⼀ 渀攀眀 椀琀攀洀 眀栀椀挀栀 眀攀 搀漀渀✀琀 欀渀漀眀 愀戀漀甀琀ഀഀ
              childScope = $scope.$new();਍            紀ഀഀ
਍            挀栀椀氀搀匀挀漀瀀攀嬀瘀愀氀甀攀䤀搀攀渀琀椀昀椀攀爀崀 㴀 瘀愀氀甀攀㬀ഀഀ
            if (keyIdentifier) childScope[keyIdentifier] = key;਍            挀栀椀氀搀匀挀漀瀀攀⸀␀椀渀搀攀砀 㴀 椀渀搀攀砀㬀ഀഀ
            childScope.$first = (index === 0);਍            挀栀椀氀搀匀挀漀瀀攀⸀␀氀愀猀琀 㴀 ⠀椀渀搀攀砀 㴀㴀㴀 ⠀愀爀爀愀礀䰀攀渀最琀栀 ⴀ ㄀⤀⤀㬀ഀഀ
            childScope.$middle = !(childScope.$first || childScope.$last);਍            ⼀⼀ 樀猀栀椀渀琀 戀椀琀眀椀猀攀㨀 昀愀氀猀攀ഀഀ
            childScope.$odd = !(childScope.$even = (index&1) === 0);਍            ⼀⼀ 樀猀栀椀渀琀 戀椀琀眀椀猀攀㨀 琀爀甀攀ഀഀ
਍            椀昀 ⠀℀戀氀漀挀欀⸀猀挀漀瀀攀⤀ 笀ഀഀ
              $transclude(childScope, function(clone) {਍                挀氀漀渀攀嬀挀氀漀渀攀⸀氀攀渀最琀栀⬀⬀崀 㴀 搀漀挀甀洀攀渀琀⸀挀爀攀愀琀攀䌀漀洀洀攀渀琀⠀✀ 攀渀搀 渀最刀攀瀀攀愀琀㨀 ✀ ⬀ 攀砀瀀爀攀猀猀椀漀渀 ⬀ ✀ ✀⤀㬀ഀഀ
                $animate.enter(clone, null, jqLite(previousNode));਍                瀀爀攀瘀椀漀甀猀一漀搀攀 㴀 挀氀漀渀攀㬀ഀഀ
                block.scope = childScope;਍                ⼀⼀ 一漀琀攀㨀 圀攀 漀渀氀礀 渀攀攀搀 琀栀攀 昀椀爀猀琀⼀氀愀猀琀 渀漀搀攀 漀昀 琀栀攀 挀氀漀渀攀搀 渀漀搀攀猀⸀ഀഀ
                // However, we need to keep the reference to the jqlite wrapper as it might be changed later਍                ⼀⼀ 戀礀 愀 搀椀爀攀挀琀椀瘀攀 眀椀琀栀 琀攀洀瀀氀愀琀攀唀爀氀 眀栀攀渀 椀琀✀猀 琀攀洀瀀氀愀琀攀 愀爀爀椀瘀攀猀⸀ഀഀ
                block.clone = clone;਍                渀攀砀琀䈀氀漀挀欀䴀愀瀀嬀戀氀漀挀欀⸀椀搀崀 㴀 戀氀漀挀欀㬀ഀഀ
              });਍            紀ഀഀ
          }਍          氀愀猀琀䈀氀漀挀欀䴀愀瀀 㴀 渀攀砀琀䈀氀漀挀欀䴀愀瀀㬀ഀഀ
        });਍    紀ഀഀ
  };਍ഀഀ
  function getBlockStart(block) {਍    爀攀琀甀爀渀 戀氀漀挀欀⸀挀氀漀渀攀嬀　崀㬀ഀഀ
  }਍ഀഀ
  function getBlockEnd(block) {਍    爀攀琀甀爀渀 戀氀漀挀欀⸀挀氀漀渀攀嬀戀氀漀挀欀⸀挀氀漀渀攀⸀氀攀渀最琀栀 ⴀ ㄀崀㬀ഀഀ
  }਍紀崀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最匀栀漀眀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * The `ngShow` directive shows or hides the given HTML element based on the expression਍ ⨀ 瀀爀漀瘀椀搀攀搀 琀漀 琀栀攀 渀最匀栀漀眀 愀琀琀爀椀戀甀琀攀⸀ 吀栀攀 攀氀攀洀攀渀琀 椀猀 猀栀漀眀渀 漀爀 栀椀搀搀攀渀 戀礀 爀攀洀漀瘀椀渀最 漀爀 愀搀搀椀渀最ഀഀ
 * the `ng-hide` CSS class onto the element. The `.ng-hide` CSS class is predefined਍ ⨀ 椀渀 䄀渀最甀氀愀爀䨀匀 愀渀搀 猀攀琀猀 琀栀攀 搀椀猀瀀氀愀礀 猀琀礀氀攀 琀漀 渀漀渀攀 ⠀甀猀椀渀最 愀渀 ℀椀洀瀀漀爀琀愀渀琀 昀氀愀最⤀⸀ഀഀ
 * For CSP mode please add `angular-csp.css` to your html file (see {@link ng.directive:ngCsp ngCsp}).਍ ⨀ഀഀ
 * <pre>਍ ⨀ 㰀℀ⴀⴀ 眀栀攀渀 ␀猀挀漀瀀攀⸀洀礀嘀愀氀甀攀 椀猀 琀爀甀琀栀礀 ⠀攀氀攀洀攀渀琀 椀猀 瘀椀猀椀戀氀攀⤀ ⴀⴀ㸀ഀഀ
 * <div ng-show="myValue"></div>਍ ⨀ഀഀ
 * <!-- when $scope.myValue is falsy (element is hidden) -->਍ ⨀ 㰀搀椀瘀 渀最ⴀ猀栀漀眀㴀∀洀礀嘀愀氀甀攀∀ 挀氀愀猀猀㴀∀渀最ⴀ栀椀搀攀∀㸀㰀⼀搀椀瘀㸀ഀഀ
 * </pre>਍ ⨀ഀഀ
 * When the ngShow expression evaluates to false then the ng-hide CSS class is added to the class attribute਍ ⨀ 漀渀 琀栀攀 攀氀攀洀攀渀琀 挀愀甀猀椀渀最 椀琀 琀漀 戀攀挀漀洀攀 栀椀搀搀攀渀⸀ 圀栀攀渀 琀爀甀攀Ⰰ 琀栀攀 渀最ⴀ栀椀搀攀 䌀匀匀 挀氀愀猀猀 椀猀 爀攀洀漀瘀攀搀ഀഀ
 * from the element causing the element not to appear hidden.਍ ⨀ഀഀ
 * ## Why is !important used?਍ ⨀ഀഀ
 * You may be wondering why !important is used for the .ng-hide CSS class. This is because the `.ng-hide` selector਍ ⨀ 挀愀渀 戀攀 攀愀猀椀氀礀 漀瘀攀爀爀椀搀搀攀渀 戀礀 栀攀愀瘀椀攀爀 猀攀氀攀挀琀漀爀猀⸀ 䘀漀爀 攀砀愀洀瀀氀攀Ⰰ 猀漀洀攀琀栀椀渀最 愀猀 猀椀洀瀀氀攀ഀഀ
 * as changing the display style on a HTML list item would make hidden elements appear visible.਍ ⨀ 吀栀椀猀 愀氀猀漀 戀攀挀漀洀攀猀 愀 戀椀最最攀爀 椀猀猀甀攀 眀栀攀渀 搀攀愀氀椀渀最 眀椀琀栀 䌀匀匀 昀爀愀洀攀眀漀爀欀猀⸀ഀഀ
 *਍ ⨀ 䈀礀 甀猀椀渀最 ℀椀洀瀀漀爀琀愀渀琀Ⰰ 琀栀攀 猀栀漀眀 愀渀搀 栀椀搀攀 戀攀栀愀瘀椀漀爀 眀椀氀氀 眀漀爀欀 愀猀 攀砀瀀攀挀琀攀搀 搀攀猀瀀椀琀攀 愀渀礀 挀氀愀猀栀 戀攀琀眀攀攀渀 䌀匀匀 猀攀氀攀挀琀漀爀ഀഀ
 * specificity (when !important isn't used with any conflicting styles). If a developer chooses to override the਍ ⨀ 猀琀礀氀椀渀最 琀漀 挀栀愀渀最攀 栀漀眀 琀漀 栀椀搀攀 愀渀 攀氀攀洀攀渀琀 琀栀攀渀 椀琀 椀猀 樀甀猀琀 愀 洀愀琀琀攀爀 漀昀 甀猀椀渀最 ℀椀洀瀀漀爀琀愀渀琀 椀渀 琀栀攀椀爀 漀眀渀 䌀匀匀 挀漀搀攀⸀ഀഀ
 *਍ ⨀ ⌀⌀⌀ 伀瘀攀爀爀椀搀椀渀最 ⸀渀最ⴀ栀椀搀攀ഀഀ
 *਍ ⨀ 䤀昀 礀漀甀 眀椀猀栀 琀漀 挀栀愀渀最攀 琀栀攀 栀椀搀攀 戀攀栀愀瘀椀漀爀 眀椀琀栀 渀最匀栀漀眀⼀渀最䠀椀搀攀 琀栀攀渀 琀栀椀猀 挀愀渀 戀攀 愀挀栀椀攀瘀攀搀 戀礀ഀഀ
 * restating the styles for the .ng-hide class in CSS:਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 * .ng-hide {਍ ⨀   ⼀⼀℀愀渀渀漀琀愀琀攀 䌀匀匀 匀瀀攀挀椀昀椀挀椀琀礀簀一漀琀 琀漀 眀漀爀爀礀Ⰰ 琀栀椀猀 眀椀氀氀 漀瘀攀爀爀椀搀攀 琀栀攀 䄀渀最甀氀愀爀䨀匀 搀攀昀愀甀氀琀⸀⸀⸀ഀഀ
 *   display:block!important;਍ ⨀ഀഀ
 *   //this is just another form of hiding an element਍ ⨀   瀀漀猀椀琀椀漀渀㨀愀戀猀漀氀甀琀攀㬀ഀഀ
 *   top:-9999px;਍ ⨀   氀攀昀琀㨀ⴀ㤀㤀㤀㤀瀀砀㬀ഀഀ
 * }਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 䨀甀猀琀 爀攀洀攀洀戀攀爀 琀漀 椀渀挀氀甀搀攀 琀栀攀 椀洀瀀漀爀琀愀渀琀 昀氀愀最 猀漀 琀栀攀 䌀匀匀 漀瘀攀爀爀椀搀攀 眀椀氀氀 昀甀渀挀琀椀漀渀⸀ഀഀ
 *਍ ⨀ ⌀⌀ 䄀 渀漀琀攀 愀戀漀甀琀 愀渀椀洀愀琀椀漀渀猀 眀椀琀栀 渀最匀栀漀眀ഀഀ
 *਍ ⨀ 䄀渀椀洀愀琀椀漀渀猀 椀渀 渀最匀栀漀眀⼀渀最䠀椀搀攀 眀漀爀欀 眀椀琀栀 琀栀攀 猀栀漀眀 愀渀搀 栀椀搀攀 攀瘀攀渀琀猀 琀栀愀琀 愀爀攀 琀爀椀最最攀爀攀搀 眀栀攀渀 琀栀攀 搀椀爀攀挀琀椀瘀攀 攀砀瀀爀攀猀猀椀漀渀ഀഀ
 * is true and false. This system works like the animation system present with ngClass except that਍ ⨀ 礀漀甀 洀甀猀琀 愀氀猀漀 椀渀挀氀甀搀攀 琀栀攀 ℀椀洀瀀漀爀琀愀渀琀 昀氀愀最 琀漀 漀瘀攀爀爀椀搀攀 琀栀攀 搀椀猀瀀氀愀礀 瀀爀漀瀀攀爀琀礀ഀഀ
 * so that you can perform an animation when the element is hidden during the time of the animation.਍ ⨀ഀഀ
 * <pre>਍ ⨀ ⼀⼀ഀഀ
 * //a working example can be found at the bottom of this page਍ ⨀ ⼀⼀ഀഀ
 * .my-element.ng-hide-add, .my-element.ng-hide-remove {਍ ⨀   琀爀愀渀猀椀琀椀漀渀㨀　⸀㔀猀 氀椀渀攀愀爀 愀氀氀㬀ഀഀ
 *   display:block!important;਍ ⨀ 紀ഀഀ
 *਍ ⨀ ⸀洀礀ⴀ攀氀攀洀攀渀琀⸀渀最ⴀ栀椀搀攀ⴀ愀搀搀 笀 ⸀⸀⸀ 紀ഀഀ
 * .my-element.ng-hide-add.ng-hide-add-active { ... }਍ ⨀ ⸀洀礀ⴀ攀氀攀洀攀渀琀⸀渀最ⴀ栀椀搀攀ⴀ爀攀洀漀瘀攀 笀 ⸀⸀⸀ 紀ഀഀ
 * .my-element.ng-hide-remove.ng-hide-remove-active { ... }਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 䀀愀渀椀洀愀琀椀漀渀猀ഀഀ
 * addClass: .ng-hide - happens after the ngShow expression evaluates to a truthy value and the just before contents are set to visible਍ ⨀ 爀攀洀漀瘀攀䌀氀愀猀猀㨀 ⸀渀最ⴀ栀椀搀攀 ⴀ 栀愀瀀瀀攀渀猀 愀昀琀攀爀 琀栀攀 渀最匀栀漀眀 攀砀瀀爀攀猀猀椀漀渀 攀瘀愀氀甀愀琀攀猀 琀漀 愀 渀漀渀 琀爀甀琀栀礀 瘀愀氀甀攀 愀渀搀 樀甀猀琀 戀攀昀漀爀攀 琀栀攀 挀漀渀琀攀渀琀猀 愀爀攀 猀攀琀 琀漀 栀椀搀搀攀渀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䄀一夀ഀഀ
 * @param {expression} ngShow If the {@link guide/expression expression} is truthy਍ ⨀     琀栀攀渀 琀栀攀 攀氀攀洀攀渀琀 椀猀 猀栀漀眀渀 漀爀 栀椀搀搀攀渀 爀攀猀瀀攀挀琀椀瘀攀氀礀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
  <example animations="true">਍    㰀昀椀氀攀 渀愀洀攀㴀∀椀渀搀攀砀⸀栀琀洀氀∀㸀ഀഀ
      Click me: <input type="checkbox" ng-model="checked"><br/>਍      㰀搀椀瘀㸀ഀഀ
        Show:਍        㰀搀椀瘀 挀氀愀猀猀㴀∀挀栀攀挀欀ⴀ攀氀攀洀攀渀琀 愀渀椀洀愀琀攀ⴀ猀栀漀眀∀ 渀最ⴀ猀栀漀眀㴀∀挀栀攀挀欀攀搀∀㸀ഀഀ
          <span class="icon-thumbs-up"></span> I show up when your checkbox is checked.਍        㰀⼀搀椀瘀㸀ഀഀ
      </div>਍      㰀搀椀瘀㸀ഀഀ
        Hide:਍        㰀搀椀瘀 挀氀愀猀猀㴀∀挀栀攀挀欀ⴀ攀氀攀洀攀渀琀 愀渀椀洀愀琀攀ⴀ猀栀漀眀∀ 渀最ⴀ栀椀搀攀㴀∀挀栀攀挀欀攀搀∀㸀ഀഀ
          <span class="icon-thumbs-down"></span> I hide when your checkbox is checked.਍        㰀⼀搀椀瘀㸀ഀഀ
      </div>਍    㰀⼀昀椀氀攀㸀ഀഀ
    <file name="animations.css">਍      ⸀愀渀椀洀愀琀攀ⴀ猀栀漀眀 笀ഀഀ
        -webkit-transition:all linear 0.5s;਍        琀爀愀渀猀椀琀椀漀渀㨀愀氀氀 氀椀渀攀愀爀 　⸀㔀猀㬀ഀഀ
        line-height:20px;਍        漀瀀愀挀椀琀礀㨀㄀㬀ഀഀ
        padding:10px;਍        戀漀爀搀攀爀㨀㄀瀀砀 猀漀氀椀搀 戀氀愀挀欀㬀ഀഀ
        background:white;਍      紀ഀഀ
਍      ⸀愀渀椀洀愀琀攀ⴀ猀栀漀眀⸀渀最ⴀ栀椀搀攀ⴀ愀搀搀Ⰰഀഀ
      .animate-show.ng-hide-remove {਍        搀椀猀瀀氀愀礀㨀戀氀漀挀欀℀椀洀瀀漀爀琀愀渀琀㬀ഀഀ
      }਍ഀഀ
      .animate-show.ng-hide {਍        氀椀渀攀ⴀ栀攀椀最栀琀㨀　㬀ഀഀ
        opacity:0;਍        瀀愀搀搀椀渀最㨀　 ㄀　瀀砀㬀ഀഀ
      }਍ഀഀ
      .check-element {਍        瀀愀搀搀椀渀最㨀㄀　瀀砀㬀ഀഀ
        border:1px solid black;਍        戀愀挀欀最爀漀甀渀搀㨀眀栀椀琀攀㬀ഀഀ
      }਍    㰀⼀昀椀氀攀㸀ഀഀ
    <file name="scenario.js">਍       椀琀⠀✀猀栀漀甀氀搀 挀栀攀挀欀 渀最ⴀ猀栀漀眀 ⼀ 渀最ⴀ栀椀搀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
         expect(element('.doc-example-live span:first:hidden').count()).toEqual(1);਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 猀瀀愀渀㨀氀愀猀琀㨀瘀椀猀椀戀氀攀✀⤀⸀挀漀甀渀琀⠀⤀⤀⸀琀漀䔀焀甀愀氀⠀㄀⤀㬀ഀഀ
਍         椀渀瀀甀琀⠀✀挀栀攀挀欀攀搀✀⤀⸀挀栀攀挀欀⠀⤀㬀ഀഀ
਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 猀瀀愀渀㨀昀椀爀猀琀㨀瘀椀猀椀戀氀攀✀⤀⸀挀漀甀渀琀⠀⤀⤀⸀琀漀䔀焀甀愀氀⠀㄀⤀㬀ഀഀ
         expect(element('.doc-example-live span:last:hidden').count()).toEqual(1);਍       紀⤀㬀ഀഀ
    </file>਍  㰀⼀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍瘀愀爀 渀最匀栀漀眀䐀椀爀攀挀琀椀瘀攀 㴀 嬀✀␀愀渀椀洀愀琀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀愀渀椀洀愀琀攀⤀ 笀ഀഀ
  return function(scope, element, attr) {਍    猀挀漀瀀攀⸀␀眀愀琀挀栀⠀愀琀琀爀⸀渀最匀栀漀眀Ⰰ 昀甀渀挀琀椀漀渀 渀最匀栀漀眀圀愀琀挀栀䄀挀琀椀漀渀⠀瘀愀氀甀攀⤀笀ഀഀ
      $animate[toBoolean(value) ? 'removeClass' : 'addClass'](element, 'ng-hide');਍    紀⤀㬀ഀഀ
  };਍紀崀㬀ഀഀ
਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngHide਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀攀 怀渀最䠀椀搀攀怀 搀椀爀攀挀琀椀瘀攀 猀栀漀眀猀 漀爀 栀椀搀攀猀 琀栀攀 最椀瘀攀渀 䠀吀䴀䰀 攀氀攀洀攀渀琀 戀愀猀攀搀 漀渀 琀栀攀 攀砀瀀爀攀猀猀椀漀渀ഀഀ
 * provided to the ngHide attribute. The element is shown or hidden by removing or adding਍ ⨀ 琀栀攀 怀渀最ⴀ栀椀搀攀怀 䌀匀匀 挀氀愀猀猀 漀渀琀漀 琀栀攀 攀氀攀洀攀渀琀⸀ 吀栀攀 怀⸀渀最ⴀ栀椀搀攀怀 䌀匀匀 挀氀愀猀猀 椀猀 瀀爀攀搀攀昀椀渀攀搀ഀഀ
 * in AngularJS and sets the display style to none (using an !important flag).਍ ⨀ 䘀漀爀 䌀匀倀 洀漀搀攀 瀀氀攀愀猀攀 愀搀搀 怀愀渀最甀氀愀爀ⴀ挀猀瀀⸀挀猀猀怀 琀漀 礀漀甀爀 栀琀洀氀 昀椀氀攀 ⠀猀攀攀 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䌀猀瀀 渀最䌀猀瀀紀⤀⸀ഀഀ
 *਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 * <!-- when $scope.myValue is truthy (element is hidden) -->਍ ⨀ 㰀搀椀瘀 渀最ⴀ栀椀搀攀㴀∀洀礀嘀愀氀甀攀∀㸀㰀⼀搀椀瘀㸀ഀഀ
 *਍ ⨀ 㰀℀ⴀⴀ 眀栀攀渀 ␀猀挀漀瀀攀⸀洀礀嘀愀氀甀攀 椀猀 昀愀氀猀礀 ⠀攀氀攀洀攀渀琀 椀猀 瘀椀猀椀戀氀攀⤀ ⴀⴀ㸀ഀഀ
 * <div ng-hide="myValue" class="ng-hide"></div>਍ ⨀ 㰀⼀瀀爀攀㸀ഀഀ
 *਍ ⨀ 圀栀攀渀 琀栀攀 渀最䠀椀搀攀 攀砀瀀爀攀猀猀椀漀渀 攀瘀愀氀甀愀琀攀猀 琀漀 琀爀甀攀 琀栀攀渀 琀栀攀 ⸀渀最ⴀ栀椀搀攀 䌀匀匀 挀氀愀猀猀 椀猀 愀搀搀攀搀 琀漀 琀栀攀 挀氀愀猀猀 愀琀琀爀椀戀甀琀攀ഀഀ
 * on the element causing it to become hidden. When false, the ng-hide CSS class is removed਍ ⨀ 昀爀漀洀 琀栀攀 攀氀攀洀攀渀琀 挀愀甀猀椀渀最 琀栀攀 攀氀攀洀攀渀琀 渀漀琀 琀漀 愀瀀瀀攀愀爀 栀椀搀搀攀渀⸀ഀഀ
 *਍ ⨀ ⌀⌀ 圀栀礀 椀猀 ℀椀洀瀀漀爀琀愀渀琀 甀猀攀搀㼀ഀഀ
 *਍ ⨀ 夀漀甀 洀愀礀 戀攀 眀漀渀搀攀爀椀渀最 眀栀礀 ℀椀洀瀀漀爀琀愀渀琀 椀猀 甀猀攀搀 昀漀爀 琀栀攀 ⸀渀最ⴀ栀椀搀攀 䌀匀匀 挀氀愀猀猀⸀ 吀栀椀猀 椀猀 戀攀挀愀甀猀攀 琀栀攀 怀⸀渀最ⴀ栀椀搀攀怀 猀攀氀攀挀琀漀爀ഀഀ
 * can be easily overridden by heavier selectors. For example, something as simple਍ ⨀ 愀猀 挀栀愀渀最椀渀最 琀栀攀 搀椀猀瀀氀愀礀 猀琀礀氀攀 漀渀 愀 䠀吀䴀䰀 氀椀猀琀 椀琀攀洀 眀漀甀氀搀 洀愀欀攀 栀椀搀搀攀渀 攀氀攀洀攀渀琀猀 愀瀀瀀攀愀爀 瘀椀猀椀戀氀攀⸀ഀഀ
 * This also becomes a bigger issue when dealing with CSS frameworks.਍ ⨀ഀഀ
 * By using !important, the show and hide behavior will work as expected despite any clash between CSS selector਍ ⨀ 猀瀀攀挀椀昀椀挀椀琀礀 ⠀眀栀攀渀 ℀椀洀瀀漀爀琀愀渀琀 椀猀渀✀琀 甀猀攀搀 眀椀琀栀 愀渀礀 挀漀渀昀氀椀挀琀椀渀最 猀琀礀氀攀猀⤀⸀ 䤀昀 愀 搀攀瘀攀氀漀瀀攀爀 挀栀漀漀猀攀猀 琀漀 漀瘀攀爀爀椀搀攀 琀栀攀ഀഀ
 * styling to change how to hide an element then it is just a matter of using !important in their own CSS code.਍ ⨀ഀഀ
 * ### Overriding .ng-hide਍ ⨀ഀഀ
 * If you wish to change the hide behavior with ngShow/ngHide then this can be achieved by਍ ⨀ 爀攀猀琀愀琀椀渀最 琀栀攀 猀琀礀氀攀猀 昀漀爀 琀栀攀 ⸀渀最ⴀ栀椀搀攀 挀氀愀猀猀 椀渀 䌀匀匀㨀ഀഀ
 * <pre>਍ ⨀ ⸀渀最ⴀ栀椀搀攀 笀ഀഀ
 *   //!annotate CSS Specificity|Not to worry, this will override the AngularJS default...਍ ⨀   搀椀猀瀀氀愀礀㨀戀氀漀挀欀℀椀洀瀀漀爀琀愀渀琀㬀ഀഀ
 *਍ ⨀   ⼀⼀琀栀椀猀 椀猀 樀甀猀琀 愀渀漀琀栀攀爀 昀漀爀洀 漀昀 栀椀搀椀渀最 愀渀 攀氀攀洀攀渀琀ഀഀ
 *   position:absolute;਍ ⨀   琀漀瀀㨀ⴀ㤀㤀㤀㤀瀀砀㬀ഀഀ
 *   left:-9999px;਍ ⨀ 紀ഀഀ
 * </pre>਍ ⨀ഀഀ
 * Just remember to include the important flag so the CSS override will function.਍ ⨀ഀഀ
 * ## A note about animations with ngHide਍ ⨀ഀഀ
 * Animations in ngShow/ngHide work with the show and hide events that are triggered when the directive expression਍ ⨀ 椀猀 琀爀甀攀 愀渀搀 昀愀氀猀攀⸀ 吀栀椀猀 猀礀猀琀攀洀 眀漀爀欀猀 氀椀欀攀 琀栀攀 愀渀椀洀愀琀椀漀渀 猀礀猀琀攀洀 瀀爀攀猀攀渀琀 眀椀琀栀 渀最䌀氀愀猀猀Ⰰ 攀砀挀攀瀀琀 琀栀愀琀ഀഀ
 * you must also include the !important flag to override the display property so਍ ⨀ 琀栀愀琀 礀漀甀 挀愀渀 瀀攀爀昀漀爀洀 愀渀 愀渀椀洀愀琀椀漀渀 眀栀攀渀 琀栀攀 攀氀攀洀攀渀琀 椀猀 栀椀搀搀攀渀 搀甀爀椀渀最 琀栀攀 琀椀洀攀 漀昀 琀栀攀 愀渀椀洀愀琀椀漀渀⸀ഀഀ
 *਍ ⨀ 㰀瀀爀攀㸀ഀഀ
 * //਍ ⨀ ⼀⼀愀 眀漀爀欀椀渀最 攀砀愀洀瀀氀攀 挀愀渀 戀攀 昀漀甀渀搀 愀琀 琀栀攀 戀漀琀琀漀洀 漀昀 琀栀椀猀 瀀愀最攀ഀഀ
 * //਍ ⨀ ⸀洀礀ⴀ攀氀攀洀攀渀琀⸀渀最ⴀ栀椀搀攀ⴀ愀搀搀Ⰰ ⸀洀礀ⴀ攀氀攀洀攀渀琀⸀渀最ⴀ栀椀搀攀ⴀ爀攀洀漀瘀攀 笀ഀഀ
 *   transition:0.5s linear all;਍ ⨀   搀椀猀瀀氀愀礀㨀戀氀漀挀欀℀椀洀瀀漀爀琀愀渀琀㬀ഀഀ
 * }਍ ⨀ഀഀ
 * .my-element.ng-hide-add { ... }਍ ⨀ ⸀洀礀ⴀ攀氀攀洀攀渀琀⸀渀最ⴀ栀椀搀攀ⴀ愀搀搀⸀渀最ⴀ栀椀搀攀ⴀ愀搀搀ⴀ愀挀琀椀瘀攀 笀 ⸀⸀⸀ 紀ഀഀ
 * .my-element.ng-hide-remove { ... }਍ ⨀ ⸀洀礀ⴀ攀氀攀洀攀渀琀⸀渀最ⴀ栀椀搀攀ⴀ爀攀洀漀瘀攀⸀渀最ⴀ栀椀搀攀ⴀ爀攀洀漀瘀攀ⴀ愀挀琀椀瘀攀 笀 ⸀⸀⸀ 紀ഀഀ
 * </pre>਍ ⨀ഀഀ
 * @animations਍ ⨀ 爀攀洀漀瘀攀䌀氀愀猀猀㨀 ⸀渀最ⴀ栀椀搀攀 ⴀ 栀愀瀀瀀攀渀猀 愀昀琀攀爀 琀栀攀 渀最䠀椀搀攀 攀砀瀀爀攀猀猀椀漀渀 攀瘀愀氀甀愀琀攀猀 琀漀 愀 琀爀甀琀栀礀 瘀愀氀甀攀 愀渀搀 樀甀猀琀 戀攀昀漀爀攀 琀栀攀 挀漀渀琀攀渀琀猀 愀爀攀 猀攀琀 琀漀 栀椀搀搀攀渀ഀഀ
 * addClass: .ng-hide - happens after the ngHide expression evaluates to a non truthy value and just before the contents are set to visible਍ ⨀ഀഀ
 * @element ANY਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最䠀椀搀攀 䤀昀 琀栀攀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 攀砀瀀爀攀猀猀椀漀渀紀 椀猀 琀爀甀琀栀礀 琀栀攀渀ഀഀ
 *     the element is shown or hidden respectively.਍ ⨀ഀഀ
 * @example਍  㰀攀砀愀洀瀀氀攀 愀渀椀洀愀琀椀漀渀猀㴀∀琀爀甀攀∀㸀ഀഀ
    <file name="index.html">਍      䌀氀椀挀欀 洀攀㨀 㰀椀渀瀀甀琀 琀礀瀀攀㴀∀挀栀攀挀欀戀漀砀∀ 渀最ⴀ洀漀搀攀氀㴀∀挀栀攀挀欀攀搀∀㸀㰀戀爀⼀㸀ഀഀ
      <div>਍        匀栀漀眀㨀ഀഀ
        <div class="check-element animate-hide" ng-show="checked">਍          㰀猀瀀愀渀 挀氀愀猀猀㴀∀椀挀漀渀ⴀ琀栀甀洀戀猀ⴀ甀瀀∀㸀㰀⼀猀瀀愀渀㸀 䤀 猀栀漀眀 甀瀀 眀栀攀渀 礀漀甀爀 挀栀攀挀欀戀漀砀 椀猀 挀栀攀挀欀攀搀⸀ഀഀ
        </div>਍      㰀⼀搀椀瘀㸀ഀഀ
      <div>਍        䠀椀搀攀㨀ഀഀ
        <div class="check-element animate-hide" ng-hide="checked">਍          㰀猀瀀愀渀 挀氀愀猀猀㴀∀椀挀漀渀ⴀ琀栀甀洀戀猀ⴀ搀漀眀渀∀㸀㰀⼀猀瀀愀渀㸀 䤀 栀椀搀攀 眀栀攀渀 礀漀甀爀 挀栀攀挀欀戀漀砀 椀猀 挀栀攀挀欀攀搀⸀ഀഀ
        </div>਍      㰀⼀搀椀瘀㸀ഀഀ
    </file>਍    㰀昀椀氀攀 渀愀洀攀㴀∀愀渀椀洀愀琀椀漀渀猀⸀挀猀猀∀㸀ഀഀ
      .animate-hide {਍        ⴀ眀攀戀欀椀琀ⴀ琀爀愀渀猀椀琀椀漀渀㨀愀氀氀 氀椀渀攀愀爀 　⸀㔀猀㬀ഀഀ
        transition:all linear 0.5s;਍        氀椀渀攀ⴀ栀攀椀最栀琀㨀㈀　瀀砀㬀ഀഀ
        opacity:1;਍        瀀愀搀搀椀渀最㨀㄀　瀀砀㬀ഀഀ
        border:1px solid black;਍        戀愀挀欀最爀漀甀渀搀㨀眀栀椀琀攀㬀ഀഀ
      }਍ഀഀ
      .animate-hide.ng-hide-add,਍      ⸀愀渀椀洀愀琀攀ⴀ栀椀搀攀⸀渀最ⴀ栀椀搀攀ⴀ爀攀洀漀瘀攀 笀ഀഀ
        display:block!important;਍      紀ഀഀ
਍      ⸀愀渀椀洀愀琀攀ⴀ栀椀搀攀⸀渀最ⴀ栀椀搀攀 笀ഀഀ
        line-height:0;਍        漀瀀愀挀椀琀礀㨀　㬀ഀഀ
        padding:0 10px;਍      紀ഀഀ
਍      ⸀挀栀攀挀欀ⴀ攀氀攀洀攀渀琀 笀ഀഀ
        padding:10px;਍        戀漀爀搀攀爀㨀㄀瀀砀 猀漀氀椀搀 戀氀愀挀欀㬀ഀഀ
        background:white;਍      紀ഀഀ
    </file>਍    㰀昀椀氀攀 渀愀洀攀㴀∀猀挀攀渀愀爀椀漀⸀樀猀∀㸀ഀഀ
       it('should check ng-show / ng-hide', function() {਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 ⸀挀栀攀挀欀ⴀ攀氀攀洀攀渀琀㨀昀椀爀猀琀㨀栀椀搀搀攀渀✀⤀⸀挀漀甀渀琀⠀⤀⤀⸀琀漀䔀焀甀愀氀⠀㄀⤀㬀ഀഀ
         expect(element('.doc-example-live .check-element:last:visible').count()).toEqual(1);਍ഀഀ
         input('checked').check();਍ഀഀ
         expect(element('.doc-example-live .check-element:first:visible').count()).toEqual(1);਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 ⸀挀栀攀挀欀ⴀ攀氀攀洀攀渀琀㨀氀愀猀琀㨀栀椀搀搀攀渀✀⤀⸀挀漀甀渀琀⠀⤀⤀⸀琀漀䔀焀甀愀氀⠀㄀⤀㬀ഀഀ
       });਍    㰀⼀昀椀氀攀㸀ഀഀ
  </example>਍ ⨀⼀ഀഀ
var ngHideDirective = ['$animate', function($animate) {਍  爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀⤀ 笀ഀഀ
    scope.$watch(attr.ngHide, function ngHideWatchAction(value){਍      ␀愀渀椀洀愀琀攀嬀琀漀䈀漀漀氀攀愀渀⠀瘀愀氀甀攀⤀ 㼀 ✀愀搀搀䌀氀愀猀猀✀ 㨀 ✀爀攀洀漀瘀攀䌀氀愀猀猀✀崀⠀攀氀攀洀攀渀琀Ⰰ ✀渀最ⴀ栀椀搀攀✀⤀㬀ഀഀ
    });਍  紀㬀ഀഀ
}];਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:ngStyle਍ ⨀ 䀀爀攀猀琀爀椀挀琀 䄀䌀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * The `ngStyle` directive allows you to set CSS style on an HTML element conditionally.਍ ⨀ഀഀ
 * @element ANY਍ ⨀ 䀀瀀愀爀愀洀 笀攀砀瀀爀攀猀猀椀漀渀紀 渀最匀琀礀氀攀 笀䀀氀椀渀欀 最甀椀搀攀⼀攀砀瀀爀攀猀猀椀漀渀 䔀砀瀀爀攀猀猀椀漀渀紀 眀栀椀挀栀 攀瘀愀氀猀 琀漀 愀渀ഀഀ
 *      object whose keys are CSS style names and values are corresponding values for those CSS਍ ⨀      欀攀礀猀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
   <example>਍     㰀昀椀氀攀 渀愀洀攀㴀∀椀渀搀攀砀⸀栀琀洀氀∀㸀ഀഀ
        <input type="button" value="set" ng-click="myStyle={color:'red'}">਍        㰀椀渀瀀甀琀 琀礀瀀攀㴀∀戀甀琀琀漀渀∀ 瘀愀氀甀攀㴀∀挀氀攀愀爀∀ 渀最ⴀ挀氀椀挀欀㴀∀洀礀匀琀礀氀攀㴀笀紀∀㸀ഀഀ
        <br/>਍        㰀猀瀀愀渀 渀最ⴀ猀琀礀氀攀㴀∀洀礀匀琀礀氀攀∀㸀匀愀洀瀀氀攀 吀攀砀琀㰀⼀猀瀀愀渀㸀ഀഀ
        <pre>myStyle={{myStyle}}</pre>਍     㰀⼀昀椀氀攀㸀ഀഀ
     <file name="style.css">਍       猀瀀愀渀 笀ഀഀ
         color: black;਍       紀ഀഀ
     </file>਍     㰀昀椀氀攀 渀愀洀攀㴀∀猀挀攀渀愀爀椀漀⸀樀猀∀㸀ഀഀ
       it('should check ng-style', function() {਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 猀瀀愀渀✀⤀⸀挀猀猀⠀✀挀漀氀漀爀✀⤀⤀⸀琀漀䈀攀⠀✀爀最戀⠀　Ⰰ 　Ⰰ 　⤀✀⤀㬀ഀഀ
         element('.doc-example-live :button[value=set]').click();਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 猀瀀愀渀✀⤀⸀挀猀猀⠀✀挀漀氀漀爀✀⤀⤀⸀琀漀䈀攀⠀✀爀最戀⠀㈀㔀㔀Ⰰ 　Ⰰ 　⤀✀⤀㬀ഀഀ
         element('.doc-example-live :button[value=clear]').click();਍         攀砀瀀攀挀琀⠀攀氀攀洀攀渀琀⠀✀⸀搀漀挀ⴀ攀砀愀洀瀀氀攀ⴀ氀椀瘀攀 猀瀀愀渀✀⤀⸀挀猀猀⠀✀挀漀氀漀爀✀⤀⤀⸀琀漀䈀攀⠀✀爀最戀⠀　Ⰰ 　Ⰰ 　⤀✀⤀㬀ഀഀ
       });਍     㰀⼀昀椀氀攀㸀ഀഀ
   </example>਍ ⨀⼀ഀഀ
var ngStyleDirective = ngDirective(function(scope, element, attr) {਍  猀挀漀瀀攀⸀␀眀愀琀挀栀⠀愀琀琀爀⸀渀最匀琀礀氀攀Ⰰ 昀甀渀挀琀椀漀渀 渀最匀琀礀氀攀圀愀琀挀栀䄀挀琀椀漀渀⠀渀攀眀匀琀礀氀攀猀Ⰰ 漀氀搀匀琀礀氀攀猀⤀ 笀ഀഀ
    if (oldStyles && (newStyles !== oldStyles)) {਍      昀漀爀䔀愀挀栀⠀漀氀搀匀琀礀氀攀猀Ⰰ 昀甀渀挀琀椀漀渀⠀瘀愀氀Ⰰ 猀琀礀氀攀⤀ 笀 攀氀攀洀攀渀琀⸀挀猀猀⠀猀琀礀氀攀Ⰰ ✀✀⤀㬀紀⤀㬀ഀഀ
    }਍    椀昀 ⠀渀攀眀匀琀礀氀攀猀⤀ 攀氀攀洀攀渀琀⸀挀猀猀⠀渀攀眀匀琀礀氀攀猀⤀㬀ഀഀ
  }, true);਍紀⤀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最匀眀椀琀挀栀ഀഀ
 * @restrict EA਍ ⨀ഀഀ
 * @description਍ ⨀ 吀栀攀 怀渀最匀眀椀琀挀栀怀 搀椀爀攀挀琀椀瘀攀 椀猀 甀猀攀搀 琀漀 挀漀渀搀椀琀椀漀渀愀氀氀礀 猀眀愀瀀 䐀伀䴀 猀琀爀甀挀琀甀爀攀 漀渀 礀漀甀爀 琀攀洀瀀氀愀琀攀 戀愀猀攀搀 漀渀 愀 猀挀漀瀀攀 攀砀瀀爀攀猀猀椀漀渀⸀ഀഀ
 * Elements within `ngSwitch` but without `ngSwitchWhen` or `ngSwitchDefault` directives will be preserved at the location਍ ⨀ 愀猀 猀瀀攀挀椀昀椀攀搀 椀渀 琀栀攀 琀攀洀瀀氀愀琀攀⸀ഀഀ
 *਍ ⨀ 吀栀攀 搀椀爀攀挀琀椀瘀攀 椀琀猀攀氀昀 眀漀爀欀猀 猀椀洀椀氀愀爀 琀漀 渀最䤀渀挀氀甀搀攀Ⰰ 栀漀眀攀瘀攀爀Ⰰ 椀渀猀琀攀愀搀 漀昀 搀漀眀渀氀漀愀搀椀渀最 琀攀洀瀀氀愀琀攀 挀漀搀攀 ⠀漀爀 氀漀愀搀椀渀最 椀琀ഀഀ
 * from the template cache), `ngSwitch` simply choses one of the nested elements and makes it visible based on which element਍ ⨀ 洀愀琀挀栀攀猀 琀栀攀 瘀愀氀甀攀 漀戀琀愀椀渀攀搀 昀爀漀洀 琀栀攀 攀瘀愀氀甀愀琀攀搀 攀砀瀀爀攀猀猀椀漀渀⸀ 䤀渀 漀琀栀攀爀 眀漀爀搀猀Ⰰ 礀漀甀 搀攀昀椀渀攀 愀 挀漀渀琀愀椀渀攀爀 攀氀攀洀攀渀琀ഀഀ
 * (where you place the directive), place an expression on the **`on="..."` attribute**਍ ⨀ ⠀漀爀 琀栀攀 ⨀⨀怀渀最ⴀ猀眀椀琀挀栀㴀∀⸀⸀⸀∀怀 愀琀琀爀椀戀甀琀攀⨀⨀⤀Ⰰ 搀攀昀椀渀攀 愀渀礀 椀渀渀攀爀 攀氀攀洀攀渀琀猀 椀渀猀椀搀攀 漀昀 琀栀攀 搀椀爀攀挀琀椀瘀攀 愀渀搀 瀀氀愀挀攀ഀഀ
 * a when attribute per element. The when attribute is used to inform ngSwitch which element to display when the on਍ ⨀ 攀砀瀀爀攀猀猀椀漀渀 椀猀 攀瘀愀氀甀愀琀攀搀⸀ 䤀昀 愀 洀愀琀挀栀椀渀最 攀砀瀀爀攀猀猀椀漀渀 椀猀 渀漀琀 昀漀甀渀搀 瘀椀愀 愀 眀栀攀渀 愀琀琀爀椀戀甀琀攀 琀栀攀渀 愀渀 攀氀攀洀攀渀琀 眀椀琀栀 琀栀攀 搀攀昀愀甀氀琀ഀഀ
 * attribute is displayed.਍ ⨀ഀഀ
 * <div class="alert alert-info">਍ ⨀ 䈀攀 愀眀愀爀攀 琀栀愀琀 琀栀攀 愀琀琀爀椀戀甀琀攀 瘀愀氀甀攀猀 琀漀 洀愀琀挀栀 愀最愀椀渀猀琀 挀愀渀渀漀琀 戀攀 攀砀瀀爀攀猀猀椀漀渀猀⸀ 吀栀攀礀 愀爀攀 椀渀琀攀爀瀀爀攀琀攀搀ഀഀ
 * as literal string values to match against.਍ ⨀ 䘀漀爀 攀砀愀洀瀀氀攀Ⰰ ⨀⨀怀渀最ⴀ猀眀椀琀挀栀ⴀ眀栀攀渀㴀∀猀漀洀攀嘀愀氀∀怀⨀⨀ 眀椀氀氀 洀愀琀挀栀 愀最愀椀渀猀琀 琀栀攀 猀琀爀椀渀最 怀∀猀漀洀攀嘀愀氀∀怀 渀漀琀 愀最愀椀渀猀琀 琀栀攀ഀഀ
 * value of the expression `$scope.someVal`.਍ ⨀ 㰀⼀搀椀瘀㸀ഀഀ
਍ ⨀ 䀀愀渀椀洀愀琀椀漀渀猀ഀഀ
 * enter - happens after the ngSwitch contents change and the matched child element is placed inside the container਍ ⨀ 氀攀愀瘀攀 ⴀ 栀愀瀀瀀攀渀猀 樀甀猀琀 愀昀琀攀爀 琀栀攀 渀最匀眀椀琀挀栀 挀漀渀琀攀渀琀猀 挀栀愀渀最攀 愀渀搀 樀甀猀琀 戀攀昀漀爀攀 琀栀攀 昀漀爀洀攀爀 挀漀渀琀攀渀琀猀 愀爀攀 爀攀洀漀瘀攀搀 昀爀漀洀 琀栀攀 䐀伀䴀ഀഀ
 *਍ ⨀ 䀀甀猀愀最攀ഀഀ
 * <ANY ng-switch="expression">਍ ⨀   㰀䄀一夀 渀最ⴀ猀眀椀琀挀栀ⴀ眀栀攀渀㴀∀洀愀琀挀栀嘀愀氀甀攀㄀∀㸀⸀⸀⸀㰀⼀䄀一夀㸀ഀഀ
 *   <ANY ng-switch-when="matchValue2">...</ANY>਍ ⨀   㰀䄀一夀 渀最ⴀ猀眀椀琀挀栀ⴀ搀攀昀愀甀氀琀㸀⸀⸀⸀㰀⼀䄀一夀㸀ഀഀ
 * </ANY>਍ ⨀ഀഀ
 *਍ ⨀ 䀀猀挀漀瀀攀ഀഀ
 * @priority 800਍ ⨀ 䀀瀀愀爀愀洀 笀⨀紀 渀最匀眀椀琀挀栀簀漀渀 攀砀瀀爀攀猀猀椀漀渀 琀漀 洀愀琀挀栀 愀最愀椀渀猀琀 㰀琀琀㸀渀最ⴀ猀眀椀琀挀栀ⴀ眀栀攀渀㰀⼀琀琀㸀⸀ഀഀ
 * @paramDescription਍ ⨀ 伀渀 挀栀椀氀搀 攀氀攀洀攀渀琀猀 愀搀搀㨀ഀഀ
 *਍ ⨀ ⨀ 怀渀最匀眀椀琀挀栀圀栀攀渀怀㨀 琀栀攀 挀愀猀攀 猀琀愀琀攀洀攀渀琀 琀漀 洀愀琀挀栀 愀最愀椀渀猀琀⸀ 䤀昀 洀愀琀挀栀 琀栀攀渀 琀栀椀猀ഀഀ
 *   case will be displayed. If the same match appears multiple times, all the਍ ⨀   攀氀攀洀攀渀琀猀 眀椀氀氀 戀攀 搀椀猀瀀氀愀礀攀搀⸀ഀഀ
 * * `ngSwitchDefault`: the default case when no other case match. If there਍ ⨀   愀爀攀 洀甀氀琀椀瀀氀攀 搀攀昀愀甀氀琀 挀愀猀攀猀Ⰰ 愀氀氀 漀昀 琀栀攀洀 眀椀氀氀 戀攀 搀椀猀瀀氀愀礀攀搀 眀栀攀渀 渀漀 漀琀栀攀爀ഀഀ
 *   case match.਍ ⨀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
  <example animations="true">਍    㰀昀椀氀攀 渀愀洀攀㴀∀椀渀搀攀砀⸀栀琀洀氀∀㸀ഀഀ
      <div ng-controller="Ctrl">਍        㰀猀攀氀攀挀琀 渀最ⴀ洀漀搀攀氀㴀∀猀攀氀攀挀琀椀漀渀∀ 渀最ⴀ漀瀀琀椀漀渀猀㴀∀椀琀攀洀 昀漀爀 椀琀攀洀 椀渀 椀琀攀洀猀∀㸀ഀഀ
        </select>਍        㰀琀琀㸀猀攀氀攀挀琀椀漀渀㴀笀笀猀攀氀攀挀琀椀漀渀紀紀㰀⼀琀琀㸀ഀഀ
        <hr/>਍        㰀搀椀瘀 挀氀愀猀猀㴀∀愀渀椀洀愀琀攀ⴀ猀眀椀琀挀栀ⴀ挀漀渀琀愀椀渀攀爀∀ഀഀ
          ng-switch on="selection">਍            㰀搀椀瘀 挀氀愀猀猀㴀∀愀渀椀洀愀琀攀ⴀ猀眀椀琀挀栀∀ 渀最ⴀ猀眀椀琀挀栀ⴀ眀栀攀渀㴀∀猀攀琀琀椀渀最猀∀㸀匀攀琀琀椀渀最猀 䐀椀瘀㰀⼀搀椀瘀㸀ഀഀ
            <div class="animate-switch" ng-switch-when="home">Home Span</div>਍            㰀搀椀瘀 挀氀愀猀猀㴀∀愀渀椀洀愀琀攀ⴀ猀眀椀琀挀栀∀ 渀最ⴀ猀眀椀琀挀栀ⴀ搀攀昀愀甀氀琀㸀搀攀昀愀甀氀琀㰀⼀搀椀瘀㸀ഀഀ
        </div>਍      㰀⼀搀椀瘀㸀ഀഀ
    </file>਍    㰀昀椀氀攀 渀愀洀攀㴀∀猀挀爀椀瀀琀⸀樀猀∀㸀ഀഀ
      function Ctrl($scope) {਍        ␀猀挀漀瀀攀⸀椀琀攀洀猀 㴀 嬀✀猀攀琀琀椀渀最猀✀Ⰰ ✀栀漀洀攀✀Ⰰ ✀漀琀栀攀爀✀崀㬀ഀഀ
        $scope.selection = $scope.items[0];਍      紀ഀഀ
    </file>਍    㰀昀椀氀攀 渀愀洀攀㴀∀愀渀椀洀愀琀椀漀渀猀⸀挀猀猀∀㸀ഀഀ
      .animate-switch-container {਍        瀀漀猀椀琀椀漀渀㨀爀攀氀愀琀椀瘀攀㬀ഀഀ
        background:white;਍        戀漀爀搀攀爀㨀㄀瀀砀 猀漀氀椀搀 戀氀愀挀欀㬀ഀഀ
        height:40px;਍        漀瘀攀爀昀氀漀眀㨀栀椀搀搀攀渀㬀ഀഀ
      }਍ഀഀ
      .animate-switch {਍        瀀愀搀搀椀渀最㨀㄀　瀀砀㬀ഀഀ
      }਍ഀഀ
      .animate-switch.ng-animate {਍        ⴀ眀攀戀欀椀琀ⴀ琀爀愀渀猀椀琀椀漀渀㨀愀氀氀 挀甀戀椀挀ⴀ戀攀稀椀攀爀⠀　⸀㈀㔀　Ⰰ 　⸀㐀㘀　Ⰰ 　⸀㐀㔀　Ⰰ 　⸀㤀㐀　⤀ 　⸀㔀猀㬀ഀഀ
        transition:all cubic-bezier(0.250, 0.460, 0.450, 0.940) 0.5s;਍ഀഀ
        position:absolute;਍        琀漀瀀㨀　㬀ഀഀ
        left:0;਍        爀椀最栀琀㨀　㬀ഀഀ
        bottom:0;਍      紀ഀഀ
਍      ⸀愀渀椀洀愀琀攀ⴀ猀眀椀琀挀栀⸀渀最ⴀ氀攀愀瘀攀⸀渀最ⴀ氀攀愀瘀攀ⴀ愀挀琀椀瘀攀Ⰰഀഀ
      .animate-switch.ng-enter {਍        琀漀瀀㨀ⴀ㔀　瀀砀㬀ഀഀ
      }਍      ⸀愀渀椀洀愀琀攀ⴀ猀眀椀琀挀栀⸀渀最ⴀ氀攀愀瘀攀Ⰰഀഀ
      .animate-switch.ng-enter.ng-enter-active {਍        琀漀瀀㨀　㬀ഀഀ
      }਍    㰀⼀昀椀氀攀㸀ഀഀ
    <file name="scenario.js">਍      椀琀⠀✀猀栀漀甀氀搀 猀琀愀爀琀 椀渀 猀攀琀琀椀渀最猀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
        expect(element('.doc-example-live [ng-switch]').text()).toMatch(/Settings Div/);਍      紀⤀㬀ഀഀ
      it('should change to home', function() {਍        猀攀氀攀挀琀⠀✀猀攀氀攀挀琀椀漀渀✀⤀⸀漀瀀琀椀漀渀⠀✀栀漀洀攀✀⤀㬀ഀഀ
        expect(element('.doc-example-live [ng-switch]').text()).toMatch(/Home Span/);਍      紀⤀㬀ഀഀ
      it('should select default', function() {਍        猀攀氀攀挀琀⠀✀猀攀氀攀挀琀椀漀渀✀⤀⸀漀瀀琀椀漀渀⠀✀漀琀栀攀爀✀⤀㬀ഀഀ
        expect(element('.doc-example-live [ng-switch]').text()).toMatch(/default/);਍      紀⤀㬀ഀഀ
    </file>਍  㰀⼀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍瘀愀爀 渀最匀眀椀琀挀栀䐀椀爀攀挀琀椀瘀攀 㴀 嬀✀␀愀渀椀洀愀琀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀愀渀椀洀愀琀攀⤀ 笀ഀഀ
  return {਍    爀攀猀琀爀椀挀琀㨀 ✀䔀䄀✀Ⰰഀഀ
    require: 'ngSwitch',਍ഀഀ
    // asks for $scope to fool the BC controller module਍    挀漀渀琀爀漀氀氀攀爀㨀 嬀✀␀猀挀漀瀀攀✀Ⰰ 昀甀渀挀琀椀漀渀 渀最匀眀椀琀挀栀䌀漀渀琀爀漀氀氀攀爀⠀⤀ 笀ഀഀ
     this.cases = {};਍    紀崀Ⰰഀഀ
    link: function(scope, element, attr, ngSwitchController) {਍      瘀愀爀 眀愀琀挀栀䔀砀瀀爀 㴀 愀琀琀爀⸀渀最匀眀椀琀挀栀 簀簀 愀琀琀爀⸀漀渀Ⰰഀഀ
          selectedTranscludes,਍          猀攀氀攀挀琀攀搀䔀氀攀洀攀渀琀猀Ⰰഀഀ
          selectedScopes = [];਍ഀഀ
      scope.$watch(watchExpr, function ngSwitchWatchAction(value) {਍        昀漀爀 ⠀瘀愀爀 椀㴀 　Ⰰ 椀椀㴀猀攀氀攀挀琀攀搀匀挀漀瀀攀猀⸀氀攀渀最琀栀㬀 椀㰀椀椀㬀 椀⬀⬀⤀ 笀ഀഀ
          selectedScopes[i].$destroy();਍          ␀愀渀椀洀愀琀攀⸀氀攀愀瘀攀⠀猀攀氀攀挀琀攀搀䔀氀攀洀攀渀琀猀嬀椀崀⤀㬀ഀഀ
        }਍ഀഀ
        selectedElements = [];਍        猀攀氀攀挀琀攀搀匀挀漀瀀攀猀 㴀 嬀崀㬀ഀഀ
਍        椀昀 ⠀⠀猀攀氀攀挀琀攀搀吀爀愀渀猀挀氀甀搀攀猀 㴀 渀最匀眀椀琀挀栀䌀漀渀琀爀漀氀氀攀爀⸀挀愀猀攀猀嬀✀℀✀ ⬀ 瘀愀氀甀攀崀 簀簀 渀最匀眀椀琀挀栀䌀漀渀琀爀漀氀氀攀爀⸀挀愀猀攀猀嬀✀㼀✀崀⤀⤀ 笀ഀഀ
          scope.$eval(attr.change);਍          昀漀爀䔀愀挀栀⠀猀攀氀攀挀琀攀搀吀爀愀渀猀挀氀甀搀攀猀Ⰰ 昀甀渀挀琀椀漀渀⠀猀攀氀攀挀琀攀搀吀爀愀渀猀挀氀甀搀攀⤀ 笀ഀഀ
            var selectedScope = scope.$new();਍            猀攀氀攀挀琀攀搀匀挀漀瀀攀猀⸀瀀甀猀栀⠀猀攀氀攀挀琀攀搀匀挀漀瀀攀⤀㬀ഀഀ
            selectedTransclude.transclude(selectedScope, function(caseElement) {਍              瘀愀爀 愀渀挀栀漀爀 㴀 猀攀氀攀挀琀攀搀吀爀愀渀猀挀氀甀搀攀⸀攀氀攀洀攀渀琀㬀ഀഀ
਍              猀攀氀攀挀琀攀搀䔀氀攀洀攀渀琀猀⸀瀀甀猀栀⠀挀愀猀攀䔀氀攀洀攀渀琀⤀㬀ഀഀ
              $animate.enter(caseElement, anchor.parent(), anchor);਍            紀⤀㬀ഀഀ
          });਍        紀ഀഀ
      });਍    紀ഀഀ
  };਍紀崀㬀ഀഀ
਍瘀愀爀 渀最匀眀椀琀挀栀圀栀攀渀䐀椀爀攀挀琀椀瘀攀 㴀 渀最䐀椀爀攀挀琀椀瘀攀⠀笀ഀഀ
  transclude: 'element',਍  瀀爀椀漀爀椀琀礀㨀 㠀　　Ⰰഀഀ
  require: '^ngSwitch',਍  氀椀渀欀㨀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀猀Ⰰ 挀琀爀氀Ⰰ ␀琀爀愀渀猀挀氀甀搀攀⤀ 笀ഀഀ
    ctrl.cases['!' + attrs.ngSwitchWhen] = (ctrl.cases['!' + attrs.ngSwitchWhen] || []);਍    挀琀爀氀⸀挀愀猀攀猀嬀✀℀✀ ⬀ 愀琀琀爀猀⸀渀最匀眀椀琀挀栀圀栀攀渀崀⸀瀀甀猀栀⠀笀 琀爀愀渀猀挀氀甀搀攀㨀 ␀琀爀愀渀猀挀氀甀搀攀Ⰰ 攀氀攀洀攀渀琀㨀 攀氀攀洀攀渀琀 紀⤀㬀ഀഀ
  }਍紀⤀㬀ഀഀ
਍瘀愀爀 渀最匀眀椀琀挀栀䐀攀昀愀甀氀琀䐀椀爀攀挀琀椀瘀攀 㴀 渀最䐀椀爀攀挀琀椀瘀攀⠀笀ഀഀ
  transclude: 'element',਍  瀀爀椀漀爀椀琀礀㨀 㠀　　Ⰰഀഀ
  require: '^ngSwitch',਍  氀椀渀欀㨀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀Ⰰ 挀琀爀氀Ⰰ ␀琀爀愀渀猀挀氀甀搀攀⤀ 笀ഀഀ
    ctrl.cases['?'] = (ctrl.cases['?'] || []);਍    挀琀爀氀⸀挀愀猀攀猀嬀✀㼀✀崀⸀瀀甀猀栀⠀笀 琀爀愀渀猀挀氀甀搀攀㨀 ␀琀爀愀渀猀挀氀甀搀攀Ⰰ 攀氀攀洀攀渀琀㨀 攀氀攀洀攀渀琀 紀⤀㬀ഀഀ
   }਍紀⤀㬀ഀഀ
਍⼀⨀⨀ഀഀ
 * @ngdoc directive਍ ⨀ 䀀渀愀洀攀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最吀爀愀渀猀挀氀甀搀攀ഀഀ
 * @restrict AC਍ ⨀ഀഀ
 * @description਍ ⨀ 䐀椀爀攀挀琀椀瘀攀 琀栀愀琀 洀愀爀欀猀 琀栀攀 椀渀猀攀爀琀椀漀渀 瀀漀椀渀琀 昀漀爀 琀栀攀 琀爀愀渀猀挀氀甀搀攀搀 䐀伀䴀 漀昀 琀栀攀 渀攀愀爀攀猀琀 瀀愀爀攀渀琀 搀椀爀攀挀琀椀瘀攀 琀栀愀琀 甀猀攀猀 琀爀愀渀猀挀氀甀猀椀漀渀⸀ഀഀ
 *਍ ⨀ 䄀渀礀 攀砀椀猀琀椀渀最 挀漀渀琀攀渀琀 漀昀 琀栀攀 攀氀攀洀攀渀琀 琀栀愀琀 琀栀椀猀 搀椀爀攀挀琀椀瘀攀 椀猀 瀀氀愀挀攀搀 漀渀 眀椀氀氀 戀攀 爀攀洀漀瘀攀搀 戀攀昀漀爀攀 琀栀攀 琀爀愀渀猀挀氀甀搀攀搀 挀漀渀琀攀渀琀 椀猀 椀渀猀攀爀琀攀搀⸀ഀഀ
 *਍ ⨀ 䀀攀氀攀洀攀渀琀 䄀一夀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
   <doc:example module="transclude">਍     㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
       <script>਍         昀甀渀挀琀椀漀渀 䌀琀爀氀⠀␀猀挀漀瀀攀⤀ 笀ഀഀ
           $scope.title = 'Lorem Ipsum';਍           ␀猀挀漀瀀攀⸀琀攀砀琀 㴀 ✀一攀焀甀攀 瀀漀爀爀漀 焀甀椀猀焀甀愀洀 攀猀琀 焀甀椀 搀漀氀漀爀攀洀 椀瀀猀甀洀 焀甀椀愀 搀漀氀漀爀⸀⸀⸀✀㬀ഀഀ
         }਍ഀഀ
         angular.module('transclude', [])਍          ⸀搀椀爀攀挀琀椀瘀攀⠀✀瀀愀渀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀笀ഀഀ
             return {਍               爀攀猀琀爀椀挀琀㨀 ✀䔀✀Ⰰഀഀ
               transclude: true,਍               猀挀漀瀀攀㨀 笀 琀椀琀氀攀㨀✀䀀✀ 紀Ⰰഀഀ
               template: '<div style="border: 1px solid black;">' +਍                           ✀㰀搀椀瘀 猀琀礀氀攀㴀∀戀愀挀欀最爀漀甀渀搀ⴀ挀漀氀漀爀㨀 最爀愀礀∀㸀笀笀琀椀琀氀攀紀紀㰀⼀搀椀瘀㸀✀ ⬀ഀഀ
                           '<div ng-transclude></div>' +਍                         ✀㰀⼀搀椀瘀㸀✀ഀഀ
             };਍         紀⤀㬀ഀഀ
       </script>਍       㰀搀椀瘀 渀最ⴀ挀漀渀琀爀漀氀氀攀爀㴀∀䌀琀爀氀∀㸀ഀഀ
         <input ng-model="title"><br>਍         㰀琀攀砀琀愀爀攀愀 渀最ⴀ洀漀搀攀氀㴀∀琀攀砀琀∀㸀㰀⼀琀攀砀琀愀爀攀愀㸀 㰀戀爀⼀㸀ഀഀ
         <pane title="{{title}}">{{text}}</pane>਍       㰀⼀搀椀瘀㸀ഀഀ
     </doc:source>਍     㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
        it('should have transcluded', function() {਍          椀渀瀀甀琀⠀✀琀椀琀氀攀✀⤀⸀攀渀琀攀爀⠀✀吀䤀吀䰀䔀✀⤀㬀ഀഀ
          input('text').enter('TEXT');਍          攀砀瀀攀挀琀⠀戀椀渀搀椀渀最⠀✀琀椀琀氀攀✀⤀⤀⸀琀漀䔀焀甀愀氀⠀✀吀䤀吀䰀䔀✀⤀㬀ഀഀ
          expect(binding('text')).toEqual('TEXT');਍        紀⤀㬀ഀഀ
     </doc:scenario>਍   㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 *਍ ⨀⼀ഀഀ
var ngTranscludeDirective = ngDirective({਍  挀漀渀琀爀漀氀氀攀爀㨀 嬀✀␀攀氀攀洀攀渀琀✀Ⰰ ✀␀琀爀愀渀猀挀氀甀搀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀攀氀攀洀攀渀琀Ⰰ ␀琀爀愀渀猀挀氀甀搀攀⤀ 笀ഀഀ
    if (!$transclude) {਍      琀栀爀漀眀 洀椀渀䔀爀爀⠀✀渀最吀爀愀渀猀挀氀甀搀攀✀⤀⠀✀漀爀瀀栀愀渀✀Ⰰഀഀ
          'Illegal use of ngTransclude directive in the template! ' +਍          ✀一漀 瀀愀爀攀渀琀 搀椀爀攀挀琀椀瘀攀 琀栀愀琀 爀攀焀甀椀爀攀猀 愀 琀爀愀渀猀挀氀甀猀椀漀渀 昀漀甀渀搀⸀ ✀ ⬀ഀഀ
          'Element: {0}',਍          猀琀愀爀琀椀渀最吀愀最⠀␀攀氀攀洀攀渀琀⤀⤀㬀ഀഀ
    }਍ഀഀ
    // remember the transclusion fn but call it during linking so that we don't process transclusion before directives on਍    ⼀⼀ 琀栀攀 瀀愀爀攀渀琀 攀氀攀洀攀渀琀 攀瘀攀渀 眀栀攀渀 琀栀攀 琀爀愀渀猀挀氀甀猀椀漀渀 爀攀瀀氀愀挀攀猀 琀栀攀 挀甀爀爀攀渀琀 攀氀攀洀攀渀琀⸀ ⠀眀攀 挀愀渀✀琀 甀猀攀 瀀爀椀漀爀椀琀礀 栀攀爀攀 戀攀挀愀甀猀攀ഀഀ
    // that applies only to compile fns and not controllers਍    琀栀椀猀⸀␀琀爀愀渀猀挀氀甀搀攀 㴀 ␀琀爀愀渀猀挀氀甀搀攀㬀ഀഀ
  }],਍ഀഀ
  link: function($scope, $element, $attrs, controller) {਍    挀漀渀琀爀漀氀氀攀爀⸀␀琀爀愀渀猀挀氀甀搀攀⠀昀甀渀挀琀椀漀渀⠀挀氀漀渀攀⤀ 笀ഀഀ
      $element.empty();਍      ␀攀氀攀洀攀渀琀⸀愀瀀瀀攀渀搀⠀挀氀漀渀攀⤀㬀ഀഀ
    });਍  紀ഀഀ
});਍ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:script਍ ⨀ 䀀爀攀猀琀爀椀挀琀 䔀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * Load the content of a `<script>` element into {@link api/ng.$templateCache `$templateCache`}, so that the਍ ⨀ 琀攀洀瀀氀愀琀攀 挀愀渀 戀攀 甀猀攀搀 戀礀 笀䀀氀椀渀欀 愀瀀椀⼀渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最䤀渀挀氀甀搀攀 怀渀最䤀渀挀氀甀搀攀怀紀Ⰰഀഀ
 * {@link api/ngRoute.directive:ngView `ngView`}, or {@link guide/directive directives}. The type of the਍ ⨀ 怀㰀猀挀爀椀瀀琀㸀怀 攀氀攀洀攀渀琀 洀甀猀琀 戀攀 猀瀀攀挀椀昀椀攀搀 愀猀 怀琀攀砀琀⼀渀最ⴀ琀攀洀瀀氀愀琀攀怀Ⰰ 愀渀搀 愀 挀愀挀栀攀 渀愀洀攀 昀漀爀 琀栀攀 琀攀洀瀀氀愀琀攀 洀甀猀琀 戀攀ഀഀ
 * assigned through the element's `id`, which can then be used as a directive's `templateUrl`.਍ ⨀ഀഀ
 * @param {'text/ng-template'} type Must be set to `'text/ng-template'`.਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 椀搀 䌀愀挀栀攀 渀愀洀攀 漀昀 琀栀攀 琀攀洀瀀氀愀琀攀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
  <doc:example>਍    㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      <script type="text/ng-template" id="/tpl.html">਍        䌀漀渀琀攀渀琀 漀昀 琀栀攀 琀攀洀瀀氀愀琀攀⸀ഀഀ
      </script>਍ഀഀ
      <a ng-click="currentTpl='/tpl.html'" id="tpl-link">Load inlined template</a>਍      㰀搀椀瘀 椀搀㴀∀琀瀀氀ⴀ挀漀渀琀攀渀琀∀ 渀最ⴀ椀渀挀氀甀搀攀 猀爀挀㴀∀挀甀爀爀攀渀琀吀瀀氀∀㸀㰀⼀搀椀瘀㸀ഀഀ
    </doc:source>਍    㰀搀漀挀㨀猀挀攀渀愀爀椀漀㸀ഀഀ
      it('should load template defined inside script tag', function() {਍        攀氀攀洀攀渀琀⠀✀⌀琀瀀氀ⴀ氀椀渀欀✀⤀⸀挀氀椀挀欀⠀⤀㬀ഀഀ
        expect(element('#tpl-content').text()).toMatch(/Content of the template/);਍      紀⤀㬀ഀഀ
    </doc:scenario>਍  㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍瘀愀爀 猀挀爀椀瀀琀䐀椀爀攀挀琀椀瘀攀 㴀 嬀✀␀琀攀洀瀀氀愀琀攀䌀愀挀栀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀琀攀洀瀀氀愀琀攀䌀愀挀栀攀⤀ 笀ഀഀ
  return {਍    爀攀猀琀爀椀挀琀㨀 ✀䔀✀Ⰰഀഀ
    terminal: true,਍    挀漀洀瀀椀氀攀㨀 昀甀渀挀琀椀漀渀⠀攀氀攀洀攀渀琀Ⰰ 愀琀琀爀⤀ 笀ഀഀ
      if (attr.type == 'text/ng-template') {਍        瘀愀爀 琀攀洀瀀氀愀琀攀唀爀氀 㴀 愀琀琀爀⸀椀搀Ⰰഀഀ
            // IE is not consistent, in scripts we have to read .text but in other nodes we have to read .textContent਍            琀攀砀琀 㴀 攀氀攀洀攀渀琀嬀　崀⸀琀攀砀琀㬀ഀഀ
਍        ␀琀攀洀瀀氀愀琀攀䌀愀挀栀攀⸀瀀甀琀⠀琀攀洀瀀氀愀琀攀唀爀氀Ⰰ 琀攀砀琀⤀㬀ഀഀ
      }਍    紀ഀഀ
  };਍紀崀㬀ഀഀ
਍瘀愀爀 渀最伀瀀琀椀漀渀猀䴀椀渀䔀爀爀 㴀 洀椀渀䔀爀爀⠀✀渀最伀瀀琀椀漀渀猀✀⤀㬀ഀഀ
/**਍ ⨀ 䀀渀最搀漀挀 搀椀爀攀挀琀椀瘀攀ഀഀ
 * @name ng.directive:select਍ ⨀ 䀀爀攀猀琀爀椀挀琀 䔀ഀഀ
 *਍ ⨀ 䀀搀攀猀挀爀椀瀀琀椀漀渀ഀഀ
 * HTML `SELECT` element with angular data-binding.਍ ⨀ഀഀ
 * # `ngOptions`਍ ⨀ഀഀ
 * The `ngOptions` attribute can be used to dynamically generate a list of `<option>`਍ ⨀ 攀氀攀洀攀渀琀猀 昀漀爀 琀栀攀 怀㰀猀攀氀攀挀琀㸀怀 攀氀攀洀攀渀琀 甀猀椀渀最 琀栀攀 愀爀爀愀礀 漀爀 漀戀樀攀挀琀 漀戀琀愀椀渀攀搀 戀礀 攀瘀愀氀甀愀琀椀渀最 琀栀攀ഀഀ
 * `ngOptions` comprehension_expression.਍ ⨀ഀഀ
 * When an item in the `<select>` menu is selected, the array element or object property਍ ⨀ 爀攀瀀爀攀猀攀渀琀攀搀 戀礀 琀栀攀 猀攀氀攀挀琀攀搀 漀瀀琀椀漀渀 眀椀氀氀 戀攀 戀漀甀渀搀 琀漀 琀栀攀 洀漀搀攀氀 椀搀攀渀琀椀昀椀攀搀 戀礀 琀栀攀 怀渀最䴀漀搀攀氀怀ഀഀ
 * directive.਍ ⨀ഀഀ
 * Optionally, a single hard-coded `<option>` element, with the value set to an empty string, can਍ ⨀ 戀攀 渀攀猀琀攀搀 椀渀琀漀 琀栀攀 怀㰀猀攀氀攀挀琀㸀怀 攀氀攀洀攀渀琀⸀ 吀栀椀猀 攀氀攀洀攀渀琀 眀椀氀氀 琀栀攀渀 爀攀瀀爀攀猀攀渀琀 琀栀攀 怀渀甀氀氀怀 漀爀 ∀渀漀琀 猀攀氀攀挀琀攀搀∀ഀഀ
 * option. See example below for demonstration.਍ ⨀ഀഀ
 * Note: `ngOptions` provides iterator facility for `<option>` element which should be used instead਍ ⨀ 漀昀 笀䀀氀椀渀欀 渀最⸀搀椀爀攀挀琀椀瘀攀㨀渀最刀攀瀀攀愀琀 渀最刀攀瀀攀愀琀紀 眀栀攀渀 礀漀甀 眀愀渀琀 琀栀攀ഀഀ
 * `select` model to be bound to a non-string value. This is because an option element can only਍ ⨀ 戀攀 戀漀甀渀搀 琀漀 猀琀爀椀渀最 瘀愀氀甀攀猀 愀琀 瀀爀攀猀攀渀琀⸀ഀഀ
 *਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最紀 渀最䴀漀搀攀氀 䄀猀猀椀最渀愀戀氀攀 愀渀最甀氀愀爀 攀砀瀀爀攀猀猀椀漀渀 琀漀 搀愀琀愀ⴀ戀椀渀搀 琀漀⸀ഀഀ
 * @param {string=} name Property name of the form under which the control is published.਍ ⨀ 䀀瀀愀爀愀洀 笀猀琀爀椀渀最㴀紀 爀攀焀甀椀爀攀搀 吀栀攀 挀漀渀琀爀漀氀 椀猀 挀漀渀猀椀搀攀爀攀搀 瘀愀氀椀搀 漀渀氀礀 椀昀 瘀愀氀甀攀 椀猀 攀渀琀攀爀攀搀⸀ഀഀ
 * @param {string=} ngRequired Adds `required` attribute and `required` validation constraint to਍ ⨀    琀栀攀 攀氀攀洀攀渀琀 眀栀攀渀 琀栀攀 渀最刀攀焀甀椀爀攀搀 攀砀瀀爀攀猀猀椀漀渀 攀瘀愀氀甀愀琀攀猀 琀漀 琀爀甀攀⸀ 唀猀攀 怀渀最刀攀焀甀椀爀攀搀怀 椀渀猀琀攀愀搀 漀昀ഀഀ
 *    `required` when you want to data-bind to the `required` attribute.਍ ⨀ 䀀瀀愀爀愀洀 笀挀漀洀瀀爀攀栀攀渀猀椀漀渀开攀砀瀀爀攀猀猀椀漀渀㴀紀 渀最伀瀀琀椀漀渀猀 椀渀 漀渀攀 漀昀 琀栀攀 昀漀氀氀漀眀椀渀最 昀漀爀洀猀㨀ഀഀ
 *਍ ⨀   ⨀ 昀漀爀 愀爀爀愀礀 搀愀琀愀 猀漀甀爀挀攀猀㨀ഀഀ
 *     * `label` **`for`** `value` **`in`** `array`਍ ⨀     ⨀ 怀猀攀氀攀挀琀怀 ⨀⨀怀愀猀怀⨀⨀ 怀氀愀戀攀氀怀 ⨀⨀怀昀漀爀怀⨀⨀ 怀瘀愀氀甀攀怀 ⨀⨀怀椀渀怀⨀⨀ 怀愀爀爀愀礀怀ഀഀ
 *     * `label`  **`group by`** `group` **`for`** `value` **`in`** `array`਍ ⨀     ⨀ 怀猀攀氀攀挀琀怀 ⨀⨀怀愀猀怀⨀⨀ 怀氀愀戀攀氀怀 ⨀⨀怀最爀漀甀瀀 戀礀怀⨀⨀ 怀最爀漀甀瀀怀 ⨀⨀怀昀漀爀怀⨀⨀ 怀瘀愀氀甀攀怀 ⨀⨀怀椀渀怀⨀⨀ 怀愀爀爀愀礀怀 ⨀⨀怀琀爀愀挀欀 戀礀怀⨀⨀ 怀琀爀愀挀欀攀砀瀀爀怀ഀഀ
 *   * for object data sources:਍ ⨀     ⨀ 怀氀愀戀攀氀怀 ⨀⨀怀昀漀爀 ⠀怀⨀⨀怀欀攀礀怀 ⨀⨀怀Ⰰ怀⨀⨀ 怀瘀愀氀甀攀怀⨀⨀怀⤀ 椀渀怀⨀⨀ 怀漀戀樀攀挀琀怀ഀഀ
 *     * `select` **`as`** `label` **`for (`**`key` **`,`** `value`**`) in`** `object`਍ ⨀     ⨀ 怀氀愀戀攀氀怀 ⨀⨀怀最爀漀甀瀀 戀礀怀⨀⨀ 怀最爀漀甀瀀怀 ⨀⨀怀昀漀爀 ⠀怀⨀⨀怀欀攀礀怀⨀⨀怀Ⰰ怀⨀⨀ 怀瘀愀氀甀攀怀⨀⨀怀⤀ 椀渀怀⨀⨀ 怀漀戀樀攀挀琀怀ഀഀ
 *     * `select` **`as`** `label` **`group by`** `group`਍ ⨀         ⨀⨀怀昀漀爀怀 怀⠀怀⨀⨀怀欀攀礀怀⨀⨀怀Ⰰ怀⨀⨀ 怀瘀愀氀甀攀怀⨀⨀怀⤀ 椀渀怀⨀⨀ 怀漀戀樀攀挀琀怀ഀഀ
 *਍ ⨀ 圀栀攀爀攀㨀ഀഀ
 *਍ ⨀   ⨀ 怀愀爀爀愀礀怀 ⼀ 怀漀戀樀攀挀琀怀㨀 愀渀 攀砀瀀爀攀猀猀椀漀渀 眀栀椀挀栀 攀瘀愀氀甀愀琀攀猀 琀漀 愀渀 愀爀爀愀礀 ⼀ 漀戀樀攀挀琀 琀漀 椀琀攀爀愀琀攀 漀瘀攀爀⸀ഀഀ
 *   * `value`: local variable which will refer to each item in the `array` or each property value਍ ⨀      漀昀 怀漀戀樀攀挀琀怀 搀甀爀椀渀最 椀琀攀爀愀琀椀漀渀⸀ഀഀ
 *   * `key`: local variable which will refer to a property name in `object` during iteration.਍ ⨀   ⨀ 怀氀愀戀攀氀怀㨀 吀栀攀 爀攀猀甀氀琀 漀昀 琀栀椀猀 攀砀瀀爀攀猀猀椀漀渀 眀椀氀氀 戀攀 琀栀攀 氀愀戀攀氀 昀漀爀 怀㰀漀瀀琀椀漀渀㸀怀 攀氀攀洀攀渀琀⸀ 吀栀攀ഀഀ
 *     `expression` will most likely refer to the `value` variable (e.g. `value.propertyName`).਍ ⨀   ⨀ 怀猀攀氀攀挀琀怀㨀 吀栀攀 爀攀猀甀氀琀 漀昀 琀栀椀猀 攀砀瀀爀攀猀猀椀漀渀 眀椀氀氀 戀攀 戀漀甀渀搀 琀漀 琀栀攀 洀漀搀攀氀 漀昀 琀栀攀 瀀愀爀攀渀琀 怀㰀猀攀氀攀挀琀㸀怀ഀഀ
 *      element. If not specified, `select` expression will default to `value`.਍ ⨀   ⨀ 怀最爀漀甀瀀怀㨀 吀栀攀 爀攀猀甀氀琀 漀昀 琀栀椀猀 攀砀瀀爀攀猀猀椀漀渀 眀椀氀氀 戀攀 甀猀攀搀 琀漀 最爀漀甀瀀 漀瀀琀椀漀渀猀 甀猀椀渀最 琀栀攀 怀㰀漀瀀琀最爀漀甀瀀㸀怀ഀഀ
 *      DOM element.਍ ⨀   ⨀ 怀琀爀愀挀欀攀砀瀀爀怀㨀 唀猀攀搀 眀栀攀渀 眀漀爀欀椀渀最 眀椀琀栀 愀渀 愀爀爀愀礀 漀昀 漀戀樀攀挀琀猀⸀ 吀栀攀 爀攀猀甀氀琀 漀昀 琀栀椀猀 攀砀瀀爀攀猀猀椀漀渀 眀椀氀氀 戀攀ഀഀ
 *      used to identify the objects in the array. The `trackexpr` will most likely refer to the਍ ⨀     怀瘀愀氀甀攀怀 瘀愀爀椀愀戀氀攀 ⠀攀⸀最⸀ 怀瘀愀氀甀攀⸀瀀爀漀瀀攀爀琀礀一愀洀攀怀⤀⸀ഀഀ
 *਍ ⨀ 䀀攀砀愀洀瀀氀攀ഀഀ
    <doc:example>਍      㰀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
        <script>਍        昀甀渀挀琀椀漀渀 䴀礀䌀渀琀爀氀⠀␀猀挀漀瀀攀⤀ 笀ഀഀ
          $scope.colors = [਍            笀渀愀洀攀㨀✀戀氀愀挀欀✀Ⰰ 猀栀愀搀攀㨀✀搀愀爀欀✀紀Ⰰഀഀ
            {name:'white', shade:'light'},਍            笀渀愀洀攀㨀✀爀攀搀✀Ⰰ 猀栀愀搀攀㨀✀搀愀爀欀✀紀Ⰰഀഀ
            {name:'blue', shade:'dark'},਍            笀渀愀洀攀㨀✀礀攀氀氀漀眀✀Ⰰ 猀栀愀搀攀㨀✀氀椀最栀琀✀紀ഀഀ
          ];਍          ␀猀挀漀瀀攀⸀挀漀氀漀爀 㴀 ␀猀挀漀瀀攀⸀挀漀氀漀爀猀嬀㈀崀㬀 ⼀⼀ 爀攀搀ഀഀ
        }਍        㰀⼀猀挀爀椀瀀琀㸀ഀഀ
        <div ng-controller="MyCntrl">਍          㰀甀氀㸀ഀഀ
            <li ng-repeat="color in colors">਍              一愀洀攀㨀 㰀椀渀瀀甀琀 渀最ⴀ洀漀搀攀氀㴀∀挀漀氀漀爀⸀渀愀洀攀∀㸀ഀഀ
              [<a href ng-click="colors.splice($index, 1)">X</a>]਍            㰀⼀氀椀㸀ഀഀ
            <li>਍              嬀㰀愀 栀爀攀昀 渀最ⴀ挀氀椀挀欀㴀∀挀漀氀漀爀猀⸀瀀甀猀栀⠀笀紀⤀∀㸀愀搀搀㰀⼀愀㸀崀ഀഀ
            </li>਍          㰀⼀甀氀㸀ഀഀ
          <hr/>਍          䌀漀氀漀爀 ⠀渀甀氀氀 渀漀琀 愀氀氀漀眀攀搀⤀㨀ഀഀ
          <select ng-model="color" ng-options="c.name for c in colors"></select><br>਍ഀഀ
          Color (null allowed):਍          㰀猀瀀愀渀  挀氀愀猀猀㴀∀渀甀氀氀愀戀氀攀∀㸀ഀഀ
            <select ng-model="color" ng-options="c.name for c in colors">਍              㰀漀瀀琀椀漀渀 瘀愀氀甀攀㴀∀∀㸀ⴀⴀ 挀栀漀漀猀攀 挀漀氀漀爀 ⴀⴀ㰀⼀漀瀀琀椀漀渀㸀ഀഀ
            </select>਍          㰀⼀猀瀀愀渀㸀㰀戀爀⼀㸀ഀഀ
਍          䌀漀氀漀爀 最爀漀甀瀀攀搀 戀礀 猀栀愀搀攀㨀ഀഀ
          <select ng-model="color" ng-options="c.name group by c.shade for c in colors">਍          㰀⼀猀攀氀攀挀琀㸀㰀戀爀⼀㸀ഀഀ
਍ഀഀ
          Select <a href ng-click="color={name:'not in list'}">bogus</a>.<br>਍          㰀栀爀⼀㸀ഀഀ
          Currently selected: {{ {selected_color:color}  }}਍          㰀搀椀瘀 猀琀礀氀攀㴀∀戀漀爀搀攀爀㨀猀漀氀椀搀 ㄀瀀砀 戀氀愀挀欀㬀 栀攀椀最栀琀㨀㈀　瀀砀∀ഀഀ
               ng-style="{'background-color':color.name}">਍          㰀⼀搀椀瘀㸀ഀഀ
        </div>਍      㰀⼀搀漀挀㨀猀漀甀爀挀攀㸀ഀഀ
      <doc:scenario>਍         椀琀⠀✀猀栀漀甀氀搀 挀栀攀挀欀 渀最ⴀ漀瀀琀椀漀渀猀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
           expect(binding('{selected_color:color}')).toMatch('red');਍           猀攀氀攀挀琀⠀✀挀漀氀漀爀✀⤀⸀漀瀀琀椀漀渀⠀✀　✀⤀㬀ഀഀ
           expect(binding('{selected_color:color}')).toMatch('black');਍           甀猀椀渀最⠀✀⸀渀甀氀氀愀戀氀攀✀⤀⸀猀攀氀攀挀琀⠀✀挀漀氀漀爀✀⤀⸀漀瀀琀椀漀渀⠀✀✀⤀㬀ഀഀ
           expect(binding('{selected_color:color}')).toMatch('null');਍         紀⤀㬀ഀഀ
      </doc:scenario>਍    㰀⼀搀漀挀㨀攀砀愀洀瀀氀攀㸀ഀഀ
 */਍ഀഀ
var ngOptionsDirective = valueFn({ terminal: true });਍⼀⼀ 樀猀栀椀渀琀 洀愀砀氀攀渀㨀 昀愀氀猀攀ഀഀ
var selectDirective = ['$compile', '$parse', function($compile,   $parse) {਍                         ⼀⼀　　　　㄀㄀㄀㄀㄀㄀㄀㄀㄀㄀　　　　　　　　　　　㈀㈀㈀㈀㈀㈀㈀㈀㈀㈀　　　　　　　　　　　　　　　　　　　　　㌀㌀㌀㌀㌀㌀㌀㌀㌀㌀　　　　　　　　　　　　　　㐀㐀㐀㐀㐀㐀㐀㐀㐀㐀㐀㐀㐀㐀㐀　　　　　　　　　㔀㔀㔀㔀㔀㔀㔀㔀㔀㔀㔀㔀㔀㔀㔀　　　　　　　㘀㘀㘀㘀㘀㘀㘀㘀㘀㘀㘀㘀㘀㘀㘀　　　　　　　　　　　　　　　㜀㜀㜀㜀㜀㜀㜀㜀㜀㜀　　　　　　　　　　　　　　　　　　　㠀㠀㠀㠀㠀㠀㠀㠀㠀㠀ഀഀ
  var NG_OPTIONS_REGEXP = /^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/,਍      渀甀氀氀䴀漀搀攀氀䌀琀爀氀 㴀 笀␀猀攀琀嘀椀攀眀嘀愀氀甀攀㨀 渀漀漀瀀紀㬀ഀഀ
// jshint maxlen: 100਍ഀഀ
  return {਍    爀攀猀琀爀椀挀琀㨀 ✀䔀✀Ⰰഀഀ
    require: ['select', '?ngModel'],਍    挀漀渀琀爀漀氀氀攀爀㨀 嬀✀␀攀氀攀洀攀渀琀✀Ⰰ ✀␀猀挀漀瀀攀✀Ⰰ ✀␀愀琀琀爀猀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀攀氀攀洀攀渀琀Ⰰ ␀猀挀漀瀀攀Ⰰ ␀愀琀琀爀猀⤀ 笀ഀഀ
      var self = this,਍          漀瀀琀椀漀渀猀䴀愀瀀 㴀 笀紀Ⰰഀഀ
          ngModelCtrl = nullModelCtrl,਍          渀甀氀氀伀瀀琀椀漀渀Ⰰഀഀ
          unknownOption;਍ഀഀ
਍      猀攀氀昀⸀搀愀琀愀戀漀甀渀搀 㴀 ␀愀琀琀爀猀⸀渀最䴀漀搀攀氀㬀ഀഀ
਍ഀഀ
      self.init = function(ngModelCtrl_, nullOption_, unknownOption_) {਍        渀最䴀漀搀攀氀䌀琀爀氀 㴀 渀最䴀漀搀攀氀䌀琀爀氀开㬀ഀഀ
        nullOption = nullOption_;਍        甀渀欀渀漀眀渀伀瀀琀椀漀渀 㴀 甀渀欀渀漀眀渀伀瀀琀椀漀渀开㬀ഀഀ
      };਍ഀഀ
਍      猀攀氀昀⸀愀搀搀伀瀀琀椀漀渀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
        assertNotHasOwnProperty(value, '"option value"');਍        漀瀀琀椀漀渀猀䴀愀瀀嬀瘀愀氀甀攀崀 㴀 琀爀甀攀㬀ഀഀ
਍        椀昀 ⠀渀最䴀漀搀攀氀䌀琀爀氀⸀␀瘀椀攀眀嘀愀氀甀攀 㴀㴀 瘀愀氀甀攀⤀ 笀ഀഀ
          $element.val(value);਍          椀昀 ⠀甀渀欀渀漀眀渀伀瀀琀椀漀渀⸀瀀愀爀攀渀琀⠀⤀⤀ 甀渀欀渀漀眀渀伀瀀琀椀漀渀⸀爀攀洀漀瘀攀⠀⤀㬀ഀഀ
        }਍      紀㬀ഀഀ
਍ഀഀ
      self.removeOption = function(value) {਍        椀昀 ⠀琀栀椀猀⸀栀愀猀伀瀀琀椀漀渀⠀瘀愀氀甀攀⤀⤀ 笀ഀഀ
          delete optionsMap[value];਍          椀昀 ⠀渀最䴀漀搀攀氀䌀琀爀氀⸀␀瘀椀攀眀嘀愀氀甀攀 㴀㴀 瘀愀氀甀攀⤀ 笀ഀഀ
            this.renderUnknownOption(value);਍          紀ഀഀ
        }਍      紀㬀ഀഀ
਍ഀഀ
      self.renderUnknownOption = function(val) {਍        瘀愀爀 甀渀欀渀漀眀渀嘀愀氀 㴀 ✀㼀 ✀ ⬀ 栀愀猀栀䬀攀礀⠀瘀愀氀⤀ ⬀ ✀ 㼀✀㬀ഀഀ
        unknownOption.val(unknownVal);਍        ␀攀氀攀洀攀渀琀⸀瀀爀攀瀀攀渀搀⠀甀渀欀渀漀眀渀伀瀀琀椀漀渀⤀㬀ഀഀ
        $element.val(unknownVal);਍        甀渀欀渀漀眀渀伀瀀琀椀漀渀⸀瀀爀漀瀀⠀✀猀攀氀攀挀琀攀搀✀Ⰰ 琀爀甀攀⤀㬀 ⼀⼀ 渀攀攀搀攀搀 昀漀爀 䤀䔀ഀഀ
      };਍ഀഀ
਍      猀攀氀昀⸀栀愀猀伀瀀琀椀漀渀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
        return optionsMap.hasOwnProperty(value);਍      紀㬀ഀഀ
਍      ␀猀挀漀瀀攀⸀␀漀渀⠀✀␀搀攀猀琀爀漀礀✀Ⰰ 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
        // disable unknown option so that we don't do work when the whole select is being destroyed਍        猀攀氀昀⸀爀攀渀搀攀爀唀渀欀渀漀眀渀伀瀀琀椀漀渀 㴀 渀漀漀瀀㬀ഀഀ
      });਍    紀崀Ⰰഀഀ
਍    氀椀渀欀㨀 昀甀渀挀琀椀漀渀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 愀琀琀爀Ⰰ 挀琀爀氀猀⤀ 笀ഀഀ
      // if ngModel is not defined, we don't need to do anything਍      椀昀 ⠀℀挀琀爀氀猀嬀㄀崀⤀ 爀攀琀甀爀渀㬀ഀഀ
਍      瘀愀爀 猀攀氀攀挀琀䌀琀爀氀 㴀 挀琀爀氀猀嬀　崀Ⰰഀഀ
          ngModelCtrl = ctrls[1],਍          洀甀氀琀椀瀀氀攀 㴀 愀琀琀爀⸀洀甀氀琀椀瀀氀攀Ⰰഀഀ
          optionsExp = attr.ngOptions,਍          渀甀氀氀伀瀀琀椀漀渀 㴀 昀愀氀猀攀Ⰰ ⼀⼀ 椀昀 昀愀氀猀攀Ⰰ 甀猀攀爀 眀椀氀氀 渀漀琀 戀攀 愀戀氀攀 琀漀 猀攀氀攀挀琀 椀琀 ⠀甀猀攀搀 戀礀 渀最伀瀀琀椀漀渀猀⤀ഀഀ
          emptyOption,਍          ⼀⼀ 眀攀 挀愀渀✀琀 樀甀猀琀 樀焀䰀椀琀攀⠀✀㰀漀瀀琀椀漀渀㸀✀⤀ 猀椀渀挀攀 樀焀䰀椀琀攀 椀猀 渀漀琀 猀洀愀爀琀 攀渀漀甀最栀ഀഀ
          // to create it in <select> and IE barfs otherwise.਍          漀瀀琀椀漀渀吀攀洀瀀氀愀琀攀 㴀 樀焀䰀椀琀攀⠀搀漀挀甀洀攀渀琀⸀挀爀攀愀琀攀䔀氀攀洀攀渀琀⠀✀漀瀀琀椀漀渀✀⤀⤀Ⰰഀഀ
          optGroupTemplate =jqLite(document.createElement('optgroup')),਍          甀渀欀渀漀眀渀伀瀀琀椀漀渀 㴀 漀瀀琀椀漀渀吀攀洀瀀氀愀琀攀⸀挀氀漀渀攀⠀⤀㬀ഀഀ
਍      ⼀⼀ 昀椀渀搀 ∀渀甀氀氀∀ 漀瀀琀椀漀渀ഀഀ
      for(var i = 0, children = element.children(), ii = children.length; i < ii; i++) {਍        椀昀 ⠀挀栀椀氀搀爀攀渀嬀椀崀⸀瘀愀氀甀攀 㴀㴀㴀 ✀✀⤀ 笀ഀഀ
          emptyOption = nullOption = children.eq(i);਍          戀爀攀愀欀㬀ഀഀ
        }਍      紀ഀഀ
਍      猀攀氀攀挀琀䌀琀爀氀⸀椀渀椀琀⠀渀最䴀漀搀攀氀䌀琀爀氀Ⰰ 渀甀氀氀伀瀀琀椀漀渀Ⰰ 甀渀欀渀漀眀渀伀瀀琀椀漀渀⤀㬀ഀഀ
਍      ⼀⼀ 爀攀焀甀椀爀攀搀 瘀愀氀椀搀愀琀漀爀ഀഀ
      if (multiple) {਍        渀最䴀漀搀攀氀䌀琀爀氀⸀␀椀猀䔀洀瀀琀礀 㴀 昀甀渀挀琀椀漀渀⠀瘀愀氀甀攀⤀ 笀ഀഀ
          return !value || value.length === 0;਍        紀㬀ഀഀ
      }਍ഀഀ
      if (optionsExp) setupAsOptions(scope, element, ngModelCtrl);਍      攀氀猀攀 椀昀 ⠀洀甀氀琀椀瀀氀攀⤀ 猀攀琀甀瀀䄀猀䴀甀氀琀椀瀀氀攀⠀猀挀漀瀀攀Ⰰ 攀氀攀洀攀渀琀Ⰰ 渀最䴀漀搀攀氀䌀琀爀氀⤀㬀ഀഀ
      else setupAsSingle(scope, element, ngModelCtrl, selectCtrl);਍ഀഀ
਍      ⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀⼀ഀഀ
਍ഀഀ
਍      昀甀渀挀琀椀漀渀 猀攀琀甀瀀䄀猀匀椀渀最氀攀⠀猀挀漀瀀攀Ⰰ 猀攀氀攀挀琀䔀氀攀洀攀渀琀Ⰰ 渀最䴀漀搀攀氀䌀琀爀氀Ⰰ 猀攀氀攀挀琀䌀琀爀氀⤀ 笀ഀഀ
        ngModelCtrl.$render = function() {਍          瘀愀爀 瘀椀攀眀嘀愀氀甀攀 㴀 渀最䴀漀搀攀氀䌀琀爀氀⸀␀瘀椀攀眀嘀愀氀甀攀㬀ഀഀ
਍          椀昀 ⠀猀攀氀攀挀琀䌀琀爀氀⸀栀愀猀伀瀀琀椀漀渀⠀瘀椀攀眀嘀愀氀甀攀⤀⤀ 笀ഀഀ
            if (unknownOption.parent()) unknownOption.remove();਍            猀攀氀攀挀琀䔀氀攀洀攀渀琀⸀瘀愀氀⠀瘀椀攀眀嘀愀氀甀攀⤀㬀ഀഀ
            if (viewValue === '') emptyOption.prop('selected', true); // to make IE9 happy਍          紀 攀氀猀攀 笀ഀഀ
            if (isUndefined(viewValue) && emptyOption) {਍              猀攀氀攀挀琀䔀氀攀洀攀渀琀⸀瘀愀氀⠀✀✀⤀㬀ഀഀ
            } else {਍              猀攀氀攀挀琀䌀琀爀氀⸀爀攀渀搀攀爀唀渀欀渀漀眀渀伀瀀琀椀漀渀⠀瘀椀攀眀嘀愀氀甀攀⤀㬀ഀഀ
            }਍          紀ഀഀ
        };਍ഀഀ
        selectElement.on('change', function() {਍          猀挀漀瀀攀⸀␀愀瀀瀀氀礀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
            if (unknownOption.parent()) unknownOption.remove();਍            渀最䴀漀搀攀氀䌀琀爀氀⸀␀猀攀琀嘀椀攀眀嘀愀氀甀攀⠀猀攀氀攀挀琀䔀氀攀洀攀渀琀⸀瘀愀氀⠀⤀⤀㬀ഀഀ
          });਍        紀⤀㬀ഀഀ
      }਍ഀഀ
      function setupAsMultiple(scope, selectElement, ctrl) {਍        瘀愀爀 氀愀猀琀嘀椀攀眀㬀ഀഀ
        ctrl.$render = function() {਍          瘀愀爀 椀琀攀洀猀 㴀 渀攀眀 䠀愀猀栀䴀愀瀀⠀挀琀爀氀⸀␀瘀椀攀眀嘀愀氀甀攀⤀㬀ഀഀ
          forEach(selectElement.find('option'), function(option) {਍            漀瀀琀椀漀渀⸀猀攀氀攀挀琀攀搀 㴀 椀猀䐀攀昀椀渀攀搀⠀椀琀攀洀猀⸀最攀琀⠀漀瀀琀椀漀渀⸀瘀愀氀甀攀⤀⤀㬀ഀഀ
          });਍        紀㬀ഀഀ
਍        ⼀⼀ 眀攀 栀愀瘀攀 琀漀 搀漀 椀琀 漀渀 攀愀挀栀 眀愀琀挀栀 猀椀渀挀攀 渀最䴀漀搀攀氀 眀愀琀挀栀攀猀 爀攀昀攀爀攀渀挀攀Ⰰ 戀甀琀ഀഀ
        // we need to work of an array, so we need to see if anything was inserted/removed਍        猀挀漀瀀攀⸀␀眀愀琀挀栀⠀昀甀渀挀琀椀漀渀 猀攀氀攀挀琀䴀甀氀琀椀瀀氀攀圀愀琀挀栀⠀⤀ 笀ഀഀ
          if (!equals(lastView, ctrl.$viewValue)) {਍            氀愀猀琀嘀椀攀眀 㴀 挀漀瀀礀⠀挀琀爀氀⸀␀瘀椀攀眀嘀愀氀甀攀⤀㬀ഀഀ
            ctrl.$render();਍          紀ഀഀ
        });਍ഀഀ
        selectElement.on('change', function() {਍          猀挀漀瀀攀⸀␀愀瀀瀀氀礀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
            var array = [];਍            昀漀爀䔀愀挀栀⠀猀攀氀攀挀琀䔀氀攀洀攀渀琀⸀昀椀渀搀⠀✀漀瀀琀椀漀渀✀⤀Ⰰ 昀甀渀挀琀椀漀渀⠀漀瀀琀椀漀渀⤀ 笀ഀഀ
              if (option.selected) {਍                愀爀爀愀礀⸀瀀甀猀栀⠀漀瀀琀椀漀渀⸀瘀愀氀甀攀⤀㬀ഀഀ
              }਍            紀⤀㬀ഀഀ
            ctrl.$setViewValue(array);਍          紀⤀㬀ഀഀ
        });਍      紀ഀഀ
਍      昀甀渀挀琀椀漀渀 猀攀琀甀瀀䄀猀伀瀀琀椀漀渀猀⠀猀挀漀瀀攀Ⰰ 猀攀氀攀挀琀䔀氀攀洀攀渀琀Ⰰ 挀琀爀氀⤀ 笀ഀഀ
        var match;਍ഀഀ
        if (! (match = optionsExp.match(NG_OPTIONS_REGEXP))) {਍          琀栀爀漀眀 渀最伀瀀琀椀漀渀猀䴀椀渀䔀爀爀⠀✀椀攀砀瀀✀Ⰰഀഀ
            "Expected expression in form of " +਍            ∀✀开猀攀氀攀挀琀开 ⠀愀猀 开氀愀戀攀氀开⤀㼀 昀漀爀 ⠀开欀攀礀开Ⰰ⤀㼀开瘀愀氀甀攀开 椀渀 开挀漀氀氀攀挀琀椀漀渀开✀∀ ⬀ഀഀ
            " but got '{0}'. Element: {1}",਍            漀瀀琀椀漀渀猀䔀砀瀀Ⰰ 猀琀愀爀琀椀渀最吀愀最⠀猀攀氀攀挀琀䔀氀攀洀攀渀琀⤀⤀㬀ഀഀ
        }਍ഀഀ
        var displayFn = $parse(match[2] || match[1]),਍            瘀愀氀甀攀一愀洀攀 㴀 洀愀琀挀栀嬀㐀崀 簀簀 洀愀琀挀栀嬀㘀崀Ⰰഀഀ
            keyName = match[5],਍            最爀漀甀瀀䈀礀䘀渀 㴀 ␀瀀愀爀猀攀⠀洀愀琀挀栀嬀㌀崀 簀簀 ✀✀⤀Ⰰഀഀ
            valueFn = $parse(match[2] ? match[1] : valueName),਍            瘀愀氀甀攀猀䘀渀 㴀 ␀瀀愀爀猀攀⠀洀愀琀挀栀嬀㜀崀⤀Ⰰഀഀ
            track = match[8],਍            琀爀愀挀欀䘀渀 㴀 琀爀愀挀欀 㼀 ␀瀀愀爀猀攀⠀洀愀琀挀栀嬀㠀崀⤀ 㨀 渀甀氀氀Ⰰഀഀ
            // This is an array of array of existing option groups in DOM.਍            ⼀⼀ 圀攀 琀爀礀 琀漀 爀攀甀猀攀 琀栀攀猀攀 椀昀 瀀漀猀猀椀戀氀攀ഀഀ
            // - optionGroupsCache[0] is the options with no option group਍            ⼀⼀ ⴀ 漀瀀琀椀漀渀䜀爀漀甀瀀猀䌀愀挀栀攀嬀㼀崀嬀　崀 椀猀 琀栀攀 瀀愀爀攀渀琀㨀 攀椀琀栀攀爀 琀栀攀 匀䔀䰀䔀䌀吀 漀爀 伀倀吀䜀刀伀唀倀 攀氀攀洀攀渀琀ഀഀ
            optionGroupsCache = [[{element: selectElement, label:''}]];਍ഀഀ
        if (nullOption) {਍          ⼀⼀ 挀漀洀瀀椀氀攀 琀栀攀 攀氀攀洀攀渀琀 猀椀渀挀攀 琀栀攀爀攀 洀椀最栀琀 戀攀 戀椀渀搀椀渀最猀 椀渀 椀琀ഀഀ
          $compile(nullOption)(scope);਍ഀഀ
          // remove the class, which is added automatically because we recompile the element and it਍          ⼀⼀ 戀攀挀漀洀攀猀 琀栀攀 挀漀洀瀀椀氀愀琀椀漀渀 爀漀漀琀ഀഀ
          nullOption.removeClass('ng-scope');਍ഀഀ
          // we need to remove it before calling selectElement.empty() because otherwise IE will਍          ⼀⼀ 爀攀洀漀瘀攀 琀栀攀 氀愀戀攀氀 昀爀漀洀 琀栀攀 攀氀攀洀攀渀琀⸀ 眀琀昀㼀ഀഀ
          nullOption.remove();਍        紀ഀഀ
਍        ⼀⼀ 挀氀攀愀爀 挀漀渀琀攀渀琀猀Ⰰ 眀攀✀氀氀 愀搀搀 眀栀愀琀✀猀 渀攀攀搀攀搀 戀愀猀攀搀 漀渀 琀栀攀 洀漀搀攀氀ഀഀ
        selectElement.empty();਍ഀഀ
        selectElement.on('change', function() {਍          猀挀漀瀀攀⸀␀愀瀀瀀氀礀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
            var optionGroup,਍                挀漀氀氀攀挀琀椀漀渀 㴀 瘀愀氀甀攀猀䘀渀⠀猀挀漀瀀攀⤀ 簀簀 嬀崀Ⰰഀഀ
                locals = {},਍                欀攀礀Ⰰ 瘀愀氀甀攀Ⰰ 漀瀀琀椀漀渀䔀氀攀洀攀渀琀Ⰰ 椀渀搀攀砀Ⰰ 最爀漀甀瀀䤀渀搀攀砀Ⰰ 氀攀渀最琀栀Ⰰ 最爀漀甀瀀䰀攀渀最琀栀Ⰰ 琀爀愀挀欀䤀渀搀攀砀㬀ഀഀ
਍            椀昀 ⠀洀甀氀琀椀瀀氀攀⤀ 笀ഀഀ
              value = [];਍              昀漀爀 ⠀最爀漀甀瀀䤀渀搀攀砀 㴀 　Ⰰ 最爀漀甀瀀䰀攀渀最琀栀 㴀 漀瀀琀椀漀渀䜀爀漀甀瀀猀䌀愀挀栀攀⸀氀攀渀最琀栀㬀ഀഀ
                   groupIndex < groupLength;਍                   最爀漀甀瀀䤀渀搀攀砀⬀⬀⤀ 笀ഀഀ
                // list of options for that group. (first item has the parent)਍                漀瀀琀椀漀渀䜀爀漀甀瀀 㴀 漀瀀琀椀漀渀䜀爀漀甀瀀猀䌀愀挀栀攀嬀最爀漀甀瀀䤀渀搀攀砀崀㬀ഀഀ
਍                昀漀爀⠀椀渀搀攀砀 㴀 ㄀Ⰰ 氀攀渀最琀栀 㴀 漀瀀琀椀漀渀䜀爀漀甀瀀⸀氀攀渀最琀栀㬀 椀渀搀攀砀 㰀 氀攀渀最琀栀㬀 椀渀搀攀砀⬀⬀⤀ 笀ഀഀ
                  if ((optionElement = optionGroup[index].element)[0].selected) {਍                    欀攀礀 㴀 漀瀀琀椀漀渀䔀氀攀洀攀渀琀⸀瘀愀氀⠀⤀㬀ഀഀ
                    if (keyName) locals[keyName] = key;਍                    椀昀 ⠀琀爀愀挀欀䘀渀⤀ 笀ഀഀ
                      for (trackIndex = 0; trackIndex < collection.length; trackIndex++) {਍                        氀漀挀愀氀猀嬀瘀愀氀甀攀一愀洀攀崀 㴀 挀漀氀氀攀挀琀椀漀渀嬀琀爀愀挀欀䤀渀搀攀砀崀㬀ഀഀ
                        if (trackFn(scope, locals) == key) break;਍                      紀ഀഀ
                    } else {਍                      氀漀挀愀氀猀嬀瘀愀氀甀攀一愀洀攀崀 㴀 挀漀氀氀攀挀琀椀漀渀嬀欀攀礀崀㬀ഀഀ
                    }਍                    瘀愀氀甀攀⸀瀀甀猀栀⠀瘀愀氀甀攀䘀渀⠀猀挀漀瀀攀Ⰰ 氀漀挀愀氀猀⤀⤀㬀ഀഀ
                  }਍                紀ഀഀ
              }਍            紀 攀氀猀攀 笀ഀഀ
              key = selectElement.val();਍              椀昀 ⠀欀攀礀 㴀㴀 ✀㼀✀⤀ 笀ഀഀ
                value = undefined;਍              紀 攀氀猀攀 椀昀 ⠀欀攀礀 㴀㴀㴀 ✀✀⤀笀ഀഀ
                value = null;਍              紀 攀氀猀攀 笀ഀഀ
                if (trackFn) {਍                  昀漀爀 ⠀琀爀愀挀欀䤀渀搀攀砀 㴀 　㬀 琀爀愀挀欀䤀渀搀攀砀 㰀 挀漀氀氀攀挀琀椀漀渀⸀氀攀渀最琀栀㬀 琀爀愀挀欀䤀渀搀攀砀⬀⬀⤀ 笀ഀഀ
                    locals[valueName] = collection[trackIndex];਍                    椀昀 ⠀琀爀愀挀欀䘀渀⠀猀挀漀瀀攀Ⰰ 氀漀挀愀氀猀⤀ 㴀㴀 欀攀礀⤀ 笀ഀഀ
                      value = valueFn(scope, locals);਍                      戀爀攀愀欀㬀ഀഀ
                    }਍                  紀ഀഀ
                } else {਍                  氀漀挀愀氀猀嬀瘀愀氀甀攀一愀洀攀崀 㴀 挀漀氀氀攀挀琀椀漀渀嬀欀攀礀崀㬀ഀഀ
                  if (keyName) locals[keyName] = key;਍                  瘀愀氀甀攀 㴀 瘀愀氀甀攀䘀渀⠀猀挀漀瀀攀Ⰰ 氀漀挀愀氀猀⤀㬀ഀഀ
                }਍              紀ഀഀ
            }਍            挀琀爀氀⸀␀猀攀琀嘀椀攀眀嘀愀氀甀攀⠀瘀愀氀甀攀⤀㬀ഀഀ
          });਍        紀⤀㬀ഀഀ
਍        挀琀爀氀⸀␀爀攀渀搀攀爀 㴀 爀攀渀搀攀爀㬀ഀഀ
਍        ⼀⼀ 吀伀䐀伀⠀瘀漀樀琀愀⤀㨀 挀愀渀✀琀 眀攀 漀瀀琀椀洀椀稀攀 琀栀椀猀 㼀ഀഀ
        scope.$watch(render);਍ഀഀ
        function render() {਍              ⼀⼀ 吀攀洀瀀漀爀愀爀礀 氀漀挀愀琀椀漀渀 昀漀爀 琀栀攀 漀瀀琀椀漀渀 最爀漀甀瀀猀 戀攀昀漀爀攀 眀攀 爀攀渀搀攀爀 琀栀攀洀ഀഀ
          var optionGroups = {'':[]},਍              漀瀀琀椀漀渀䜀爀漀甀瀀一愀洀攀猀 㴀 嬀✀✀崀Ⰰഀഀ
              optionGroupName,਍              漀瀀琀椀漀渀䜀爀漀甀瀀Ⰰഀഀ
              option,਍              攀砀椀猀琀椀渀最倀愀爀攀渀琀Ⰰ 攀砀椀猀琀椀渀最伀瀀琀椀漀渀猀Ⰰ 攀砀椀猀琀椀渀最伀瀀琀椀漀渀Ⰰഀഀ
              modelValue = ctrl.$modelValue,਍              瘀愀氀甀攀猀 㴀 瘀愀氀甀攀猀䘀渀⠀猀挀漀瀀攀⤀ 簀簀 嬀崀Ⰰഀഀ
              keys = keyName ? sortedKeys(values) : values,਍              欀攀礀Ⰰഀഀ
              groupLength, length,਍              最爀漀甀瀀䤀渀搀攀砀Ⰰ 椀渀搀攀砀Ⰰഀഀ
              locals = {},਍              猀攀氀攀挀琀攀搀Ⰰഀഀ
              selectedSet = false, // nothing is selected yet਍              氀愀猀琀䔀氀攀洀攀渀琀Ⰰഀഀ
              element,਍              氀愀戀攀氀㬀ഀഀ
਍          椀昀 ⠀洀甀氀琀椀瀀氀攀⤀ 笀ഀഀ
            if (trackFn && isArray(modelValue)) {਍              猀攀氀攀挀琀攀搀匀攀琀 㴀 渀攀眀 䠀愀猀栀䴀愀瀀⠀嬀崀⤀㬀ഀഀ
              for (var trackIndex = 0; trackIndex < modelValue.length; trackIndex++) {਍                氀漀挀愀氀猀嬀瘀愀氀甀攀一愀洀攀崀 㴀 洀漀搀攀氀嘀愀氀甀攀嬀琀爀愀挀欀䤀渀搀攀砀崀㬀ഀഀ
                selectedSet.put(trackFn(scope, locals), modelValue[trackIndex]);਍              紀ഀഀ
            } else {਍              猀攀氀攀挀琀攀搀匀攀琀 㴀 渀攀眀 䠀愀猀栀䴀愀瀀⠀洀漀搀攀氀嘀愀氀甀攀⤀㬀ഀഀ
            }਍          紀ഀഀ
਍          ⼀⼀ 圀攀 渀漀眀 戀甀椀氀搀 甀瀀 琀栀攀 氀椀猀琀 漀昀 漀瀀琀椀漀渀猀 眀攀 渀攀攀搀 ⠀眀攀 洀攀爀最攀 氀愀琀攀爀⤀ഀഀ
          for (index = 0; length = keys.length, index < length; index++) {਍            ഀഀ
            key = index;਍            椀昀 ⠀欀攀礀一愀洀攀⤀ 笀ഀഀ
              key = keys[index];਍              椀昀 ⠀ 欀攀礀⸀挀栀愀爀䄀琀⠀　⤀ 㴀㴀㴀 ✀␀✀ ⤀ 挀漀渀琀椀渀甀攀㬀ഀഀ
              locals[keyName] = key;਍            紀ഀഀ
਍            氀漀挀愀氀猀嬀瘀愀氀甀攀一愀洀攀崀 㴀 瘀愀氀甀攀猀嬀欀攀礀崀㬀ഀഀ
਍            漀瀀琀椀漀渀䜀爀漀甀瀀一愀洀攀 㴀 最爀漀甀瀀䈀礀䘀渀⠀猀挀漀瀀攀Ⰰ 氀漀挀愀氀猀⤀ 簀簀 ✀✀㬀ഀഀ
            if (!(optionGroup = optionGroups[optionGroupName])) {਍              漀瀀琀椀漀渀䜀爀漀甀瀀 㴀 漀瀀琀椀漀渀䜀爀漀甀瀀猀嬀漀瀀琀椀漀渀䜀爀漀甀瀀一愀洀攀崀 㴀 嬀崀㬀ഀഀ
              optionGroupNames.push(optionGroupName);਍            紀ഀഀ
            if (multiple) {਍              猀攀氀攀挀琀攀搀 㴀 椀猀䐀攀昀椀渀攀搀⠀ഀഀ
                selectedSet.remove(trackFn ? trackFn(scope, locals) : valueFn(scope, locals))਍              ⤀㬀ഀഀ
            } else {਍              椀昀 ⠀琀爀愀挀欀䘀渀⤀ 笀ഀഀ
                var modelCast = {};਍                洀漀搀攀氀䌀愀猀琀嬀瘀愀氀甀攀一愀洀攀崀 㴀 洀漀搀攀氀嘀愀氀甀攀㬀ഀഀ
                selected = trackFn(scope, modelCast) === trackFn(scope, locals);਍              紀 攀氀猀攀 笀ഀഀ
                selected = modelValue === valueFn(scope, locals);਍              紀ഀഀ
              selectedSet = selectedSet || selected; // see if at least one item is selected਍            紀ഀഀ
            label = displayFn(scope, locals); // what will be seen by the user਍ഀഀ
            // doing displayFn(scope, locals) || '' overwrites zero values਍            氀愀戀攀氀 㴀 椀猀䐀攀昀椀渀攀搀⠀氀愀戀攀氀⤀ 㼀 氀愀戀攀氀 㨀 ✀✀㬀ഀഀ
            optionGroup.push({਍              ⼀⼀ 攀椀琀栀攀爀 琀栀攀 椀渀搀攀砀 椀渀琀漀 愀爀爀愀礀 漀爀 欀攀礀 昀爀漀洀 漀戀樀攀挀琀ഀഀ
              id: trackFn ? trackFn(scope, locals) : (keyName ? keys[index] : index),਍              氀愀戀攀氀㨀 氀愀戀攀氀Ⰰഀഀ
              selected: selected                   // determine if we should be selected਍            紀⤀㬀ഀഀ
          }਍          椀昀 ⠀℀洀甀氀琀椀瀀氀攀⤀ 笀ഀഀ
            if (nullOption || modelValue === null) {਍              ⼀⼀ 椀渀猀攀爀琀 渀甀氀氀 漀瀀琀椀漀渀 椀昀 眀攀 栀愀瘀攀 愀 瀀氀愀挀攀栀漀氀搀攀爀Ⰰ 漀爀 琀栀攀 洀漀搀攀氀 椀猀 渀甀氀氀ഀഀ
              optionGroups[''].unshift({id:'', label:'', selected:!selectedSet});਍            紀 攀氀猀攀 椀昀 ⠀℀猀攀氀攀挀琀攀搀匀攀琀⤀ 笀ഀഀ
              // option could not be found, we have to insert the undefined item਍              漀瀀琀椀漀渀䜀爀漀甀瀀猀嬀✀✀崀⸀甀渀猀栀椀昀琀⠀笀椀搀㨀✀㼀✀Ⰰ 氀愀戀攀氀㨀✀✀Ⰰ 猀攀氀攀挀琀攀搀㨀琀爀甀攀紀⤀㬀ഀഀ
            }਍          紀ഀഀ
਍          ⼀⼀ 一漀眀 眀攀 渀攀攀搀 琀漀 甀瀀搀愀琀攀 琀栀攀 氀椀猀琀 漀昀 䐀伀䴀 渀漀搀攀猀 琀漀 洀愀琀挀栀 琀栀攀 漀瀀琀椀漀渀䜀爀漀甀瀀猀 眀攀 挀漀洀瀀甀琀攀搀 愀戀漀瘀攀ഀഀ
          for (groupIndex = 0, groupLength = optionGroupNames.length;਍               最爀漀甀瀀䤀渀搀攀砀 㰀 最爀漀甀瀀䰀攀渀最琀栀㬀ഀഀ
               groupIndex++) {਍            ⼀⼀ 挀甀爀爀攀渀琀 漀瀀琀椀漀渀 最爀漀甀瀀 渀愀洀攀 漀爀 ✀✀ 椀昀 渀漀 最爀漀甀瀀ഀഀ
            optionGroupName = optionGroupNames[groupIndex];਍ഀഀ
            // list of options for that group. (first item has the parent)਍            漀瀀琀椀漀渀䜀爀漀甀瀀 㴀 漀瀀琀椀漀渀䜀爀漀甀瀀猀嬀漀瀀琀椀漀渀䜀爀漀甀瀀一愀洀攀崀㬀ഀഀ
਍            椀昀 ⠀漀瀀琀椀漀渀䜀爀漀甀瀀猀䌀愀挀栀攀⸀氀攀渀最琀栀 㰀㴀 最爀漀甀瀀䤀渀搀攀砀⤀ 笀ഀഀ
              // we need to grow the optionGroups਍              攀砀椀猀琀椀渀最倀愀爀攀渀琀 㴀 笀ഀഀ
                element: optGroupTemplate.clone().attr('label', optionGroupName),਍                氀愀戀攀氀㨀 漀瀀琀椀漀渀䜀爀漀甀瀀⸀氀愀戀攀氀ഀഀ
              };਍              攀砀椀猀琀椀渀最伀瀀琀椀漀渀猀 㴀 嬀攀砀椀猀琀椀渀最倀愀爀攀渀琀崀㬀ഀഀ
              optionGroupsCache.push(existingOptions);਍              猀攀氀攀挀琀䔀氀攀洀攀渀琀⸀愀瀀瀀攀渀搀⠀攀砀椀猀琀椀渀最倀愀爀攀渀琀⸀攀氀攀洀攀渀琀⤀㬀ഀഀ
            } else {਍              攀砀椀猀琀椀渀最伀瀀琀椀漀渀猀 㴀 漀瀀琀椀漀渀䜀爀漀甀瀀猀䌀愀挀栀攀嬀最爀漀甀瀀䤀渀搀攀砀崀㬀ഀഀ
              existingParent = existingOptions[0];  // either SELECT (no group) or OPTGROUP element਍ഀഀ
              // update the OPTGROUP label if not the same.਍              椀昀 ⠀攀砀椀猀琀椀渀最倀愀爀攀渀琀⸀氀愀戀攀氀 ℀㴀 漀瀀琀椀漀渀䜀爀漀甀瀀一愀洀攀⤀ 笀ഀഀ
                existingParent.element.attr('label', existingParent.label = optionGroupName);਍              紀ഀഀ
            }਍ഀഀ
            lastElement = null;  // start at the beginning਍            昀漀爀⠀椀渀搀攀砀 㴀 　Ⰰ 氀攀渀最琀栀 㴀 漀瀀琀椀漀渀䜀爀漀甀瀀⸀氀攀渀最琀栀㬀 椀渀搀攀砀 㰀 氀攀渀最琀栀㬀 椀渀搀攀砀⬀⬀⤀ 笀ഀഀ
              option = optionGroup[index];਍              椀昀 ⠀⠀攀砀椀猀琀椀渀最伀瀀琀椀漀渀 㴀 攀砀椀猀琀椀渀最伀瀀琀椀漀渀猀嬀椀渀搀攀砀⬀㄀崀⤀⤀ 笀ഀഀ
                // reuse elements਍                氀愀猀琀䔀氀攀洀攀渀琀 㴀 攀砀椀猀琀椀渀最伀瀀琀椀漀渀⸀攀氀攀洀攀渀琀㬀ഀഀ
                if (existingOption.label !== option.label) {਍                  氀愀猀琀䔀氀攀洀攀渀琀⸀琀攀砀琀⠀攀砀椀猀琀椀渀最伀瀀琀椀漀渀⸀氀愀戀攀氀 㴀 漀瀀琀椀漀渀⸀氀愀戀攀氀⤀㬀ഀഀ
                }਍                椀昀 ⠀攀砀椀猀琀椀渀最伀瀀琀椀漀渀⸀椀搀 ℀㴀㴀 漀瀀琀椀漀渀⸀椀搀⤀ 笀ഀഀ
                  lastElement.val(existingOption.id = option.id);਍                紀ഀഀ
                // lastElement.prop('selected') provided by jQuery has side-effects਍                椀昀 ⠀氀愀猀琀䔀氀攀洀攀渀琀嬀　崀⸀猀攀氀攀挀琀攀搀 ℀㴀㴀 漀瀀琀椀漀渀⸀猀攀氀攀挀琀攀搀⤀ 笀ഀഀ
                  lastElement.prop('selected', (existingOption.selected = option.selected));਍                紀ഀഀ
              } else {਍                ⼀⼀ 最爀漀眀 攀氀攀洀攀渀琀猀ഀഀ
਍                ⼀⼀ 椀昀 椀琀✀猀 愀 渀甀氀氀 漀瀀琀椀漀渀ഀഀ
                if (option.id === '' && nullOption) {਍                  ⼀⼀ 瀀甀琀 戀愀挀欀 琀栀攀 瀀爀攀ⴀ挀漀洀瀀椀氀攀搀 攀氀攀洀攀渀琀ഀഀ
                  element = nullOption;਍                紀 攀氀猀攀 笀ഀഀ
                  // jQuery(v1.4.2) Bug: We should be able to chain the method calls, but਍                  ⼀⼀ 椀渀 琀栀椀猀 瘀攀爀猀椀漀渀 漀昀 樀儀甀攀爀礀 漀渀 猀漀洀攀 戀爀漀眀猀攀爀 琀栀攀 ⸀琀攀砀琀⠀⤀ 爀攀琀甀爀渀猀 愀 猀琀爀椀渀最ഀഀ
                  // rather then the element.਍                  ⠀攀氀攀洀攀渀琀 㴀 漀瀀琀椀漀渀吀攀洀瀀氀愀琀攀⸀挀氀漀渀攀⠀⤀⤀ഀഀ
                      .val(option.id)਍                      ⸀愀琀琀爀⠀✀猀攀氀攀挀琀攀搀✀Ⰰ 漀瀀琀椀漀渀⸀猀攀氀攀挀琀攀搀⤀ഀഀ
                      .text(option.label);਍                紀ഀഀ
਍                攀砀椀猀琀椀渀最伀瀀琀椀漀渀猀⸀瀀甀猀栀⠀攀砀椀猀琀椀渀最伀瀀琀椀漀渀 㴀 笀ഀഀ
                    element: element,਍                    氀愀戀攀氀㨀 漀瀀琀椀漀渀⸀氀愀戀攀氀Ⰰഀഀ
                    id: option.id,਍                    猀攀氀攀挀琀攀搀㨀 漀瀀琀椀漀渀⸀猀攀氀攀挀琀攀搀ഀഀ
                });਍                椀昀 ⠀氀愀猀琀䔀氀攀洀攀渀琀⤀ 笀ഀഀ
                  lastElement.after(element);਍                紀 攀氀猀攀 笀ഀഀ
                  existingParent.element.append(element);਍                紀ഀഀ
                lastElement = element;਍              紀ഀഀ
            }਍            ⼀⼀ 爀攀洀漀瘀攀 愀渀礀 攀砀挀攀猀猀椀瘀攀 伀倀吀䤀伀一猀 椀渀 愀 最爀漀甀瀀ഀഀ
            index++; // increment since the existingOptions[0] is parent element not OPTION਍            眀栀椀氀攀⠀攀砀椀猀琀椀渀最伀瀀琀椀漀渀猀⸀氀攀渀最琀栀 㸀 椀渀搀攀砀⤀ 笀ഀഀ
              existingOptions.pop().element.remove();਍            紀ഀഀ
          }਍          ⼀⼀ 爀攀洀漀瘀攀 愀渀礀 攀砀挀攀猀猀椀瘀攀 伀倀吀䜀刀伀唀倀猀 昀爀漀洀 猀攀氀攀挀琀ഀഀ
          while(optionGroupsCache.length > groupIndex) {਍            漀瀀琀椀漀渀䜀爀漀甀瀀猀䌀愀挀栀攀⸀瀀漀瀀⠀⤀嬀　崀⸀攀氀攀洀攀渀琀⸀爀攀洀漀瘀攀⠀⤀㬀ഀഀ
          }਍        紀ഀഀ
      }਍    紀ഀഀ
  };਍紀崀㬀ഀഀ
਍瘀愀爀 漀瀀琀椀漀渀䐀椀爀攀挀琀椀瘀攀 㴀 嬀✀␀椀渀琀攀爀瀀漀氀愀琀攀✀Ⰰ 昀甀渀挀琀椀漀渀⠀␀椀渀琀攀爀瀀漀氀愀琀攀⤀ 笀ഀഀ
  var nullSelectCtrl = {਍    愀搀搀伀瀀琀椀漀渀㨀 渀漀漀瀀Ⰰഀഀ
    removeOption: noop਍  紀㬀ഀഀ
਍  爀攀琀甀爀渀 笀ഀഀ
    restrict: 'E',਍    瀀爀椀漀爀椀琀礀㨀 ㄀　　Ⰰഀഀ
    compile: function(element, attr) {਍      椀昀 ⠀椀猀唀渀搀攀昀椀渀攀搀⠀愀琀琀爀⸀瘀愀氀甀攀⤀⤀ 笀ഀഀ
        var interpolateFn = $interpolate(element.text(), true);਍        椀昀 ⠀℀椀渀琀攀爀瀀漀氀愀琀攀䘀渀⤀ 笀ഀഀ
          attr.$set('value', element.text());਍        紀ഀഀ
      }਍ഀഀ
      return function (scope, element, attr) {਍        瘀愀爀 猀攀氀攀挀琀䌀琀爀氀一愀洀攀 㴀 ✀␀猀攀氀攀挀琀䌀漀渀琀爀漀氀氀攀爀✀Ⰰഀഀ
            parent = element.parent(),਍            猀攀氀攀挀琀䌀琀爀氀 㴀 瀀愀爀攀渀琀⸀搀愀琀愀⠀猀攀氀攀挀琀䌀琀爀氀一愀洀攀⤀ 簀簀ഀഀ
              parent.parent().data(selectCtrlName); // in case we are in optgroup਍ഀഀ
        if (selectCtrl && selectCtrl.databound) {਍          ⼀⼀ 䘀漀爀 猀漀洀攀 爀攀愀猀漀渀 伀瀀攀爀愀 搀攀昀愀甀氀琀猀 琀漀 琀爀甀攀 愀渀搀 椀昀 渀漀琀 漀瘀攀爀爀椀搀搀攀渀 琀栀椀猀 洀攀猀猀攀猀 甀瀀 琀栀攀 爀攀瀀攀愀琀攀爀⸀ഀഀ
          // We don't want the view to drive the initialization of the model anyway.਍          攀氀攀洀攀渀琀⸀瀀爀漀瀀⠀✀猀攀氀攀挀琀攀搀✀Ⰰ 昀愀氀猀攀⤀㬀ഀഀ
        } else {਍          猀攀氀攀挀琀䌀琀爀氀 㴀 渀甀氀氀匀攀氀攀挀琀䌀琀爀氀㬀ഀഀ
        }਍ഀഀ
        if (interpolateFn) {਍          猀挀漀瀀攀⸀␀眀愀琀挀栀⠀椀渀琀攀爀瀀漀氀愀琀攀䘀渀Ⰰ 昀甀渀挀琀椀漀渀 椀渀琀攀爀瀀漀氀愀琀攀圀愀琀挀栀䄀挀琀椀漀渀⠀渀攀眀嘀愀氀Ⰰ 漀氀搀嘀愀氀⤀ 笀ഀഀ
            attr.$set('value', newVal);਍            椀昀 ⠀渀攀眀嘀愀氀 ℀㴀㴀 漀氀搀嘀愀氀⤀ 猀攀氀攀挀琀䌀琀爀氀⸀爀攀洀漀瘀攀伀瀀琀椀漀渀⠀漀氀搀嘀愀氀⤀㬀ഀഀ
            selectCtrl.addOption(newVal);਍          紀⤀㬀ഀഀ
        } else {਍          猀攀氀攀挀琀䌀琀爀氀⸀愀搀搀伀瀀琀椀漀渀⠀愀琀琀爀⸀瘀愀氀甀攀⤀㬀ഀഀ
        }਍ഀഀ
        element.on('$destroy', function() {਍          猀攀氀攀挀琀䌀琀爀氀⸀爀攀洀漀瘀攀伀瀀琀椀漀渀⠀愀琀琀爀⸀瘀愀氀甀攀⤀㬀ഀഀ
        });਍      紀㬀ഀഀ
    }਍  紀㬀ഀഀ
}];਍ഀഀ
var styleDirective = valueFn({਍  爀攀猀琀爀椀挀琀㨀 ✀䔀✀Ⰰഀഀ
  terminal: true਍紀⤀㬀ഀഀ
਍  ⼀⼀琀爀礀 琀漀 戀椀渀搀 琀漀 樀焀甀攀爀礀 渀漀眀 猀漀 琀栀愀琀 漀渀攀 挀愀渀 眀爀椀琀攀 愀渀最甀氀愀爀⸀攀氀攀洀攀渀琀⠀⤀⸀爀攀愀搀⠀⤀ഀഀ
  //but we will rebind on bootstrap again.਍  戀椀渀搀䨀儀甀攀爀礀⠀⤀㬀ഀഀ
਍  瀀甀戀氀椀猀栀䔀砀琀攀爀渀愀氀䄀倀䤀⠀愀渀最甀氀愀爀⤀㬀ഀഀ
਍  樀焀䰀椀琀攀⠀搀漀挀甀洀攀渀琀⤀⸀爀攀愀搀礀⠀昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    angularInit(document, bootstrap);਍  紀⤀㬀ഀഀ
਍紀⤀⠀眀椀渀搀漀眀Ⰰ 搀漀挀甀洀攀渀琀⤀㬀ഀഀ
਍℀愀渀最甀氀愀爀⸀␀␀挀猀瀀⠀⤀ ☀☀ 愀渀最甀氀愀爀⸀攀氀攀洀攀渀琀⠀搀漀挀甀洀攀渀琀⤀⸀昀椀渀搀⠀✀栀攀愀搀✀⤀⸀瀀爀攀瀀攀渀搀⠀✀㰀猀琀礀氀攀 琀礀瀀攀㴀∀琀攀砀琀⼀挀猀猀∀㸀䀀挀栀愀爀猀攀琀 ∀唀吀䘀ⴀ㠀∀㬀嬀渀最尀尀㨀挀氀漀愀欀崀Ⰰ嬀渀最ⴀ挀氀漀愀欀崀Ⰰ嬀搀愀琀愀ⴀ渀最ⴀ挀氀漀愀欀崀Ⰰ嬀砀ⴀ渀最ⴀ挀氀漀愀欀崀Ⰰ⸀渀最ⴀ挀氀漀愀欀Ⰰ⸀砀ⴀ渀最ⴀ挀氀漀愀欀Ⰰ⸀渀最ⴀ栀椀搀攀笀搀椀猀瀀氀愀礀㨀渀漀渀攀 ℀椀洀瀀漀爀琀愀渀琀㬀紀渀最尀尀㨀昀漀爀洀笀搀椀猀瀀氀愀礀㨀戀氀漀挀欀㬀紀㰀⼀猀琀礀氀攀㸀✀⤀㬀�