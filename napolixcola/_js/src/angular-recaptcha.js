/**਍ ⨀ 愀渀最甀氀愀爀ⴀ爀攀挀愀瀀琀挀栀愀 戀甀椀氀搀㨀㈀　㄀㔀ⴀ　㐀ⴀ㈀㠀 ഀ
 * https://github.com/vividcortex/angular-recaptcha ਍ ⨀ 䌀漀瀀礀爀椀最栀琀 ⠀挀⤀ ㈀　㄀㔀 嘀椀瘀椀搀䌀漀爀琀攀砀 ഀ
**/਍ഀ
/*global angular, Recaptcha */਍⠀昀甀渀挀琀椀漀渀 ⠀渀最⤀ 笀ഀ
    'use strict';਍ഀ
    ng.module('vcRecaptcha', []);਍ഀ
}(angular));਍ഀ
/*global angular */਍⠀昀甀渀挀琀椀漀渀 ⠀渀最⤀ 笀ഀ
    'use strict';਍ഀ
    var app = ng.module('vcRecaptcha');਍ഀ
    /**਍     ⨀ 䄀渀 愀渀最甀氀愀爀 猀攀爀瘀椀挀攀 琀漀 眀爀愀瀀 琀栀攀 爀攀䌀愀瀀琀挀栀愀 䄀倀䤀ഀ
     */਍    愀瀀瀀⸀猀攀爀瘀椀挀攀⠀✀瘀挀刀攀挀愀瀀琀挀栀愀匀攀爀瘀椀挀攀✀Ⰰ 嬀✀␀眀椀渀搀漀眀✀Ⰰ ✀␀焀✀Ⰰ 昀甀渀挀琀椀漀渀 ⠀␀眀椀渀搀漀眀Ⰰ ␀焀⤀ 笀ഀ
        var deferred = $q.defer(), promise = deferred.promise, recaptcha;਍ഀ
        $window.vcRecaptchaApiLoaded = function () {਍            爀攀挀愀瀀琀挀栀愀 㴀 ␀眀椀渀搀漀眀⸀最爀攀挀愀瀀琀挀栀愀㬀ഀ
਍            搀攀昀攀爀爀攀搀⸀爀攀猀漀氀瘀攀⠀爀攀挀愀瀀琀挀栀愀⤀㬀ഀ
        };਍ഀ
਍        昀甀渀挀琀椀漀渀 最攀琀刀攀挀愀瀀琀挀栀愀⠀⤀ 笀ഀ
            if (!!recaptcha) {਍                爀攀琀甀爀渀 ␀焀⸀眀栀攀渀⠀爀攀挀愀瀀琀挀栀愀⤀㬀ഀ
            }਍ഀ
            return promise;਍        紀ഀ
਍        昀甀渀挀琀椀漀渀 瘀愀氀椀搀愀琀攀刀攀挀愀瀀琀挀栀愀䤀渀猀琀愀渀挀攀⠀⤀ 笀ഀ
            if (!recaptcha) {਍                琀栀爀漀眀 渀攀眀 䔀爀爀漀爀⠀✀爀攀䌀愀瀀琀挀栀愀 栀愀猀 渀漀琀 戀攀攀渀 氀漀愀搀攀搀 礀攀琀⸀✀⤀㬀ഀ
            }਍        紀ഀ
਍ഀ
        // Check if grecaptcha is not defined already.਍        椀昀 ⠀渀最⸀椀猀䐀攀昀椀渀攀搀⠀␀眀椀渀搀漀眀⸀最爀攀挀愀瀀琀挀栀愀⤀⤀ 笀ഀ
            $window.vcRecaptchaApiLoaded();਍        紀ഀ
਍        爀攀琀甀爀渀 笀ഀ
਍            ⼀⨀⨀ഀ
             * Creates a new reCaptcha object਍             ⨀ഀ
             * @param elm  the DOM element where to put the captcha਍             ⨀ 䀀瀀愀爀愀洀 欀攀礀  琀栀攀 爀攀挀愀瀀琀挀栀愀 瀀甀戀氀椀挀 欀攀礀 ⠀爀攀昀攀爀 琀漀 琀栀攀 刀䔀䄀䐀䴀䔀 昀椀氀攀 椀昀 礀漀甀 搀漀渀✀琀 欀渀漀眀 眀栀愀琀 琀栀椀猀 椀猀⤀ഀ
             * @param fn   a callback function to call when the captcha is resolved਍             ⨀ 䀀瀀愀爀愀洀 挀漀渀昀 琀栀攀 挀愀瀀琀挀栀愀 漀戀樀攀挀琀 挀漀渀昀椀最甀爀愀琀椀漀渀ഀ
             */਍            挀爀攀愀琀攀㨀 昀甀渀挀琀椀漀渀 ⠀攀氀洀Ⰰ 欀攀礀Ⰰ 昀渀Ⰰ 挀漀渀昀⤀ 笀ഀ
                conf.callback = fn;਍                挀漀渀昀⸀猀椀琀攀欀攀礀 㴀 欀攀礀㬀ഀ
਍                爀攀琀甀爀渀 最攀琀刀攀挀愀瀀琀挀栀愀⠀⤀⸀琀栀攀渀⠀昀甀渀挀琀椀漀渀 ⠀爀攀挀愀瀀琀挀栀愀⤀ 笀ഀ
                    return recaptcha.render(elm, conf);਍                紀⤀㬀ഀ
            },਍ഀ
            /**਍             ⨀ 刀攀氀漀愀搀猀 琀栀攀 爀攀䌀愀瀀琀挀栀愀ഀ
             */਍            爀攀氀漀愀搀㨀 昀甀渀挀琀椀漀渀 ⠀眀椀搀最攀琀䤀搀⤀ 笀ഀ
                validateRecaptchaInstance();਍ഀ
                // $log.info('Reloading captcha');਍                爀攀挀愀瀀琀挀栀愀⸀爀攀猀攀琀⠀眀椀搀最攀琀䤀搀⤀㬀ഀ
਍                ⼀⼀ 爀攀䌀愀瀀琀挀栀愀 眀椀氀氀 挀愀氀氀 琀栀攀 猀愀洀攀 挀愀氀氀戀愀挀欀 瀀爀漀瘀椀搀攀搀 琀漀 琀栀攀ഀ
                // create function once this new captcha is resolved.਍            紀Ⰰഀ
਍            ⼀⨀⨀ഀ
             * Gets the response from the reCaptcha widget.਍             ⨀ഀ
             * @see https://developers.google.com/recaptcha/docs/display#js_api਍             ⨀ഀ
             * @returns {String}਍             ⨀⼀ഀ
            getResponse: function (widgetId) {਍                瘀愀氀椀搀愀琀攀刀攀挀愀瀀琀挀栀愀䤀渀猀琀愀渀挀攀⠀⤀㬀ഀ
਍                爀攀琀甀爀渀 爀攀挀愀瀀琀挀栀愀⸀最攀琀刀攀猀瀀漀渀猀攀⠀眀椀搀最攀琀䤀搀⤀㬀ഀ
            }਍        紀㬀ഀ
਍    紀崀⤀㬀ഀ
਍紀⠀愀渀最甀氀愀爀⤀⤀㬀ഀ
਍⼀⨀最氀漀戀愀氀 愀渀最甀氀愀爀Ⰰ 刀攀挀愀瀀琀挀栀愀 ⨀⼀ഀ
(function (ng) {਍    ✀甀猀攀 猀琀爀椀挀琀✀㬀ഀ
਍    昀甀渀挀琀椀漀渀 琀栀爀漀眀一漀䬀攀礀䔀砀挀攀瀀琀椀漀渀⠀⤀ 笀ഀ
        throw new Error('You need to set the "key" attribute to your public reCaptcha key. If you don\'t have a key, please get one from https://www.google.com/recaptcha/admin/create');਍    紀ഀ
਍    瘀愀爀 愀瀀瀀 㴀 渀最⸀洀漀搀甀氀攀⠀✀瘀挀刀攀挀愀瀀琀挀栀愀✀⤀㬀ഀ
਍    愀瀀瀀⸀搀椀爀攀挀琀椀瘀攀⠀✀瘀挀刀攀挀愀瀀琀挀栀愀✀Ⰰ 嬀✀␀搀漀挀甀洀攀渀琀✀Ⰰ ✀␀琀椀洀攀漀甀琀✀Ⰰ ✀瘀挀刀攀挀愀瀀琀挀栀愀匀攀爀瘀椀挀攀✀Ⰰ 昀甀渀挀琀椀漀渀 ⠀␀搀漀挀甀洀攀渀琀Ⰰ ␀琀椀洀攀漀甀琀Ⰰ 瘀挀刀攀挀愀瀀琀挀栀愀⤀ 笀ഀ
਍        爀攀琀甀爀渀 笀ഀ
            restrict: 'A',਍            爀攀焀甀椀爀攀㨀 ∀㼀帀帀昀漀爀洀∀Ⰰഀ
            scope: {਍                爀攀猀瀀漀渀猀攀㨀 ✀㴀㼀渀最䴀漀搀攀氀✀Ⰰഀ
                key: '=',਍                琀栀攀洀攀㨀 ✀㴀㼀✀Ⰰഀ
                tabindex: '=?',਍                漀渀䌀爀攀愀琀攀㨀 ✀☀✀Ⰰഀ
                onSuccess: '&',਍                漀渀䔀砀瀀椀爀攀㨀 ✀☀✀ഀ
            },਍            氀椀渀欀㨀 昀甀渀挀琀椀漀渀 ⠀猀挀漀瀀攀Ⰰ 攀氀洀Ⰰ 愀琀琀爀猀Ⰰ 挀琀爀氀⤀ 笀ഀ
                if (!attrs.hasOwnProperty('key')) {਍                    琀栀爀漀眀一漀䬀攀礀䔀砀挀攀瀀琀椀漀渀⠀⤀㬀ഀ
                }਍ഀ
                scope.widgetId = null;਍ഀ
                var removeCreationListener = scope.$watch('key', function (key) {਍                    椀昀 ⠀℀欀攀礀⤀ 笀ഀ
                        return;਍                    紀ഀ
਍                    椀昀 ⠀欀攀礀⸀氀攀渀最琀栀 ℀㴀㴀 㐀　⤀ 笀ഀ
                        throwNoKeyException();਍                    紀ഀ
਍                    瘀愀爀 挀愀氀氀戀愀挀欀 㴀 昀甀渀挀琀椀漀渀 ⠀最刀攀挀愀瀀琀挀栀愀刀攀猀瀀漀渀猀攀⤀ 笀ഀ
                        // Safe $apply਍                        ␀琀椀洀攀漀甀琀⠀昀甀渀挀琀椀漀渀 ⠀⤀ 笀ഀ
                            if(ctrl){਍                                挀琀爀氀⸀␀猀攀琀嘀愀氀椀搀椀琀礀⠀✀爀攀挀愀瀀琀挀栀愀✀Ⰰ琀爀甀攀⤀㬀ഀ
                            }਍                            猀挀漀瀀攀⸀爀攀猀瀀漀渀猀攀 㴀 最刀攀挀愀瀀琀挀栀愀刀攀猀瀀漀渀猀攀㬀ഀ
                            // Notify about the response availability਍                            猀挀漀瀀攀⸀漀渀匀甀挀挀攀猀猀⠀笀爀攀猀瀀漀渀猀攀㨀 最刀攀挀愀瀀琀挀栀愀刀攀猀瀀漀渀猀攀Ⰰ 眀椀搀最攀琀䤀搀㨀 猀挀漀瀀攀⸀眀椀搀最攀琀䤀搀紀⤀㬀ഀ
                        });਍ഀ
                        // captcha session lasts 2 mins after set.਍                        ␀琀椀洀攀漀甀琀⠀昀甀渀挀琀椀漀渀 ⠀⤀笀ഀ
                            if(ctrl){਍                                挀琀爀氀⸀␀猀攀琀嘀愀氀椀搀椀琀礀⠀✀爀攀挀愀瀀琀挀栀愀✀Ⰰ昀愀氀猀攀⤀㬀ഀ
                            }਍                            猀挀漀瀀攀⸀爀攀猀瀀漀渀猀攀 㴀 ∀∀㬀ഀ
                            // Notify about the response availability਍                            猀挀漀瀀攀⸀漀渀䔀砀瀀椀爀攀⠀笀眀椀搀最攀琀䤀搀㨀 猀挀漀瀀攀⸀眀椀搀最攀琀䤀搀紀⤀㬀ഀ
                        }, 2 * 60 * 1000);਍                    紀㬀ഀ
਍                    瘀挀刀攀挀愀瀀琀挀栀愀⸀挀爀攀愀琀攀⠀攀氀洀嬀　崀Ⰰ 欀攀礀Ⰰ 挀愀氀氀戀愀挀欀Ⰰ 笀ഀ
਍                        琀栀攀洀攀㨀 猀挀漀瀀攀⸀琀栀攀洀攀 簀簀 愀琀琀爀猀⸀琀栀攀洀攀 簀簀 渀甀氀氀Ⰰഀ
                        tabindex: scope.tabindex || attrs.tabindex || null਍ഀ
                    }).then(function (widgetId) {਍                        ⼀⼀ 吀栀攀 眀椀搀最攀琀 栀愀猀 戀攀攀渀 挀爀攀愀琀攀搀ഀ
                        if(ctrl){਍                            挀琀爀氀⸀␀猀攀琀嘀愀氀椀搀椀琀礀⠀✀爀攀挀愀瀀琀挀栀愀✀Ⰰ昀愀氀猀攀⤀㬀ഀ
                        }਍                        猀挀漀瀀攀⸀眀椀搀最攀琀䤀搀 㴀 眀椀搀最攀琀䤀搀㬀ഀ
                        scope.onCreate({widgetId: widgetId});਍ഀ
                        scope.$on('$destroy', cleanup);਍ഀ
                    });਍ഀ
                    // Remove this listener to avoid creating the widget more than once.਍                    爀攀洀漀瘀攀䌀爀攀愀琀椀漀渀䰀椀猀琀攀渀攀爀⠀⤀㬀ഀ
                });਍ഀ
                function cleanup(){਍                  ⼀⼀ 爀攀洀漀瘀攀猀 攀氀攀洀攀渀琀猀 爀攀䌀愀瀀琀挀栀愀 愀搀搀攀搀⸀ഀ
                  angular.element($document[0].querySelectorAll('.pls-container')).parent().remove();਍                紀ഀ
            }਍        紀㬀ഀ
    }]);਍ഀ
}(angular));਍�