
const defaultStartDate = "defaultStartDate";
const defaultEndDate = "defaultEndDate";
const loginID = "loginID";
const pagesets = "pagesetting"

$(document).ready(function () {

    defineApp('tmnewa')
});

function defineApp(appName) {
    const app = siteDefine.find(element => element.name == appName);
    let para = {};
    try {
        sitePrefix = app.prefix;
        let edt = moment().subtract(1, 'days').format("YYYY-MM-DD");
        let sdt = moment().subtract(1, 'days').format("YYYY-MM-DD");
        sessionStorage.setItem('appName', appName);
        sessionStorage.setItem('sitePrefix', sitePrefix);
        sessionStorage.setItem('memberKey', app.memberKey);
        sessionStorage.setItem('GoogleremoteUrl', app.GoogleremoteUrl);
        sessionStorage.setItem('localurl', app.localurl);
        sessionStorage.setItem('AuthremoteUrl', app.AuthremoteUrl);
        sessionStorage.setItem('TealeafremoteUrl', app.TealeafremoteUrl);
        sessionStorage.setItem('LabelremoteUrl', app.LabelremoteUrl);
        sessionStorage.setItem(defaultStartDate, sdt);
        sessionStorage.setItem(defaultEndDate, edt);
        sessionStorage.setItem('daySpan', "1");
        document.location.href = sessionStorage.getItem("localurl") + "analytics/interactive.html"

    } catch (e) {
        console.log(e);
        sitePrefix = "";
    }




}
