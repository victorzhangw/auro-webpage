const L10n = ej.base.L10n;
const widgetitems = (document.getElementById('dataWidget'));
const chartitems = (document.getElementById('dataChart'));
const prefix = sessionStorage.getItem("sitePrefix") + "-";
DramaCore.loadCultureFiles('zh');
//DramaCore.sideBar();
L10n.load({
  'zh': {
    'daterangepicker': {
      placeholder: '選擇日期區間',
      startLabel: '起始日期',
      endLabel: '截止日期',
      applyText: '確認',
      cancelText: '取消',
      selectedDays: '選擇日期',
      days: '天',
      customRange: '自定義區間'
    }
  }
});
/* 
   Process: 漏斗圖 ,
   DimHours :各時段柱狀圖,
   DimMerge:各險種柱狀圖,
   DimGeography:地理圖,
   DimDevice:設備圖,
   DimSource:來源圖,
*/
const reportDefinition = [
  {
    "Name": "ReportforreportTMNIProcessSessionCounts住火險",
    "CName": "住火險投保漏斗",
    "ChartId": "chart1",
    "ReportType": "Process",
    "ColName": {
      "Col1": "住火險 - 填寫投保資料（房屋資料）",
      "Col2": "住火險 - 保費試算（商品試算頁）",
      "Col3": "住火險 - 確認投保資料頁（資料總覽）",
      "Col4": "住火險 - 目標完成",
    }
  },
  {
    "Name": "ReportforreportTMNIProcessSessionCounts傷害險",
    "CName": "傷害險投保漏斗",
    "ChartId": "chart2",
    "ReportType": "Process",
    "ColName":
    {
      "Col1": "傷害險 - 首頁",
      "Col2": "傷害險 - 資料填寫頁",
      "Col3": "傷害險 - 健康狀態調查",
      "Col4": "傷害險 - 確認要保",
      "Col5": "傷害險 - 完成要保",
    }
  },
  {
    "Name": "ReportforreportTMNIProcessSessionCounts機車險",
    "CName": "機車險投保漏斗",
    "ChartId": "chart3",
    "ReportType": "Process",
    "ColName":
    {
      "Col1": "機車險 - 車主車籍頁",
      "Col2": "機車險 - 商品選擇頁",
      "Col3": "機車險 - 投保確認（資料總覽）",
      "Col4": "機車險 - 目標完成",
    }
  },
  {
    "Name": "ReportforreportTMNIProcessSessionCounts汽車險",
    "CName": "汽車險投保漏斗",
    "ChartId": "chart4",
    "ReportType": "Process",
    "ColName": {
      "Col1": "汽車險 - 車主頁",
      "Col2": "汽車險 - 商品選擇頁",
      "Col3": "汽車險 - 投保確認（資料總覽）",
      "Col4": "汽車險 - 完成要保頁",
    }
  },
  {
    "Name": "ReportforreportTMNIProcessSessionCounts旅平險",
    "CName": "旅平險投保漏斗",
    "ChartId": "chart5",
    "ReportType": "Process",
    "ColName":
    {
      "Col1": "旅平險 - 試算頁",
      "Col2": "旅平險 - 身分證填寫頁",
      "Col3": "旅平險 - 旅遊明細頁",
      "Col4": "旅平險 - 旅遊明細確認頁",
      "Col5": "旅平險 - 完成要保",
    }
  },
  {
    "Name": "ReportTMNI全險種成交客戶樣貌來源網域",
    "CName": "各險種成交客戶來源-來源網域",
    "ChartId": "chart6",
    "ReportType": "DimSource",
    "ColName": {}
  },
  {
    "Name": "ReportTMNI全險種成交客戶樣貌使用裝置",
    "CName": "各險種成交客戶來源-使用裝置",
    "ChartId": "chart7",
    "ReportType": "DimDevice",
    "ColName": {}
  },

  {
    "Name": "ReportforreportTMNI傷害險單一工作階段內試算並完成保單件數",
    "CName": "各險種在單一工作階段完成保單",
    "ChartId": "chart8",
    "ReportType": "DimMerge",
    "ColName": {}
  },
  {
    "Name": "ReportforreportTMNI住火險單一工作階段內試算並完成保單件數",
    "CName": "各險種在單一工作階段完成保單",
    "ChartId": "chart8",
    "ReportType": "DimMerge",
    "ColName": {}
  },

  {
    "Name": "ReportforreportTMNI旅平險單一工作階段內試算並完成保單件數",
    "CName": "各險種在單一工作階段完成保單",
    "ChartId": "chart8",
    "ReportType": "DimMerge",
    "ColName": {}
  },

  {
    "Name": "ReportforreportTMNI汽車險單一工作階段內試算並完成保單件數",
    "CName": "各險種在單一工作階段完成保單",
    "ChartId": "chart8",
    "ReportType": "DimMerge",
    "ColName": {}
  },
  {
    "Name": "ReportforreportTMNI機車險單一工作階段內試算並完成保單件數",
    "CName": "機車險單一工作階段內試算並完成保單件數",
    "ChartId": "chart8",
    "ReportType": "DimMerge",
    "ColName": {}
  },
  {
    "Name": "ReportforreportTMNI旅平險單一工作階段完成保單客戶樣貌來源網域",
    "CName": "旅平險在單一工作階段完成保單之來源",
    "ChartId": "chart9",
    "ReportType": "DimSource",
    "ColName": {}
  },
  {
    "Name": "ReportforreportTMNI機車險單一工作階段完成保單客戶樣貌來源網域",
    "CName": "機車險在單一工作階段完成保單之來源",
    "ChartId": "chart10",
    "ReportType": "DimSource",
    "ColName": {}
  },
  {
    "Name": "ReportforreportTMNI汽車險單一工作階段完成保單客戶樣貌來源網域",
    "CName": "汽車險在單一工作階段完成保單之來源",
    "ChartId": "chart11",
    "ReportType": "DimSource",
    "ColName": {}
  },
  {
    "Name": "ReportforreportTMNI進站數汽車險",
    "CName": "汽車險各時段進站數",
    "ChartId": "chart12",
    "ReportType": "DimHours",
    "ColName": [
    ]
  },
  {
    "Name": "ReportforreportTMNI進站數住火險",
    "CName": "住火險各時段進站數",
    "ChartId": "chart13",
    "ReportType": "DimHours",
    "ColName": {}
  },

  {
    "Name": "ReportforreportTMNI進站數傷害險",
    "CName": "傷害險各時段進站數",
    "ChartId": "chart14",
    "ReportType": "DimHours",
    "ColName": {}
  },
  {
    "Name": "ReportforreportTMNI住火險單一工作階段完成保單客戶樣貌來源網域",
    "CName": "住火險在單一工作階段完成保單之來源",
    "ChartId": "chart15",
    "ReportType": "DimSource",
    "ColName": {}
  },

  {
    "Name": "ReportforreportTMNI進站數機車險",
    "CName": "機車險各時段進站數",
    "ChartId": "chart17",
    "ReportType": "DimHours",
    "ColName": {}
  },

  {
    "Name": "ReportforreportTMNI進站數旅平險",
    "CName": "旅平險各時段進站數",
    "ChartId": "chart16",
    "ReportType": "DimHours",
    "ColName": {}
  },

  {
    "Name": "ReportTMNI全險種成交客戶樣貌國家城市",
    "CName": "各險種成交客戶來源-國家/城市",
    "ChartId": "chart18",
    "ReportType": "DimGeography",
    "ColName": {}
  }


]
const tealeafReporturl = TealeafremoteUrl + 'TealeafReport';
$(document).ready(function () {
  let sdt = sessionStorage.getItem(defaultStartDate)
  let edt = sessionStorage.getItem(defaultEndDate)
  let dp = DramaCore.createDatePicker(sdt, edt);
  var tealeafKey = prefix + "tealeaf-" + sdt + "-" + edt + "-" + sessionStorage.getItem(daySpan);
  if (sessionStorage.getItem(defaultStartDate) && sessionStorage.getItem(defaultEndDate) && sessionStorage.getItem(daySpan)) {
    dp.value = [sessionStorage.getItem(defaultStartDate), sessionStorage.getItem(defaultEndDate)];
    //initInteractivePage.widget();
    initInteractivePage.chart();
    if (localStorage.getItem(tealeafKey)) {
      DramaCore.renderTealeafChart(tealeafKey);
    } else {
      let type = "post";
      para = {
        "MemberKey": memberKey,
        "StartDate": sessionStorage.getItem(defaultStartDate),
        "EndDate": sessionStorage.getItem(defaultEndDate),
        "LoginId": loginID,
      };
      // console.log("TEALEAFkey:"+tealeafKey);
      global_Tealeaf_Key = DramaCore.getRemoteData(tealeafReporturl, type, para, tealeafKey, fromTealeaf);
      let timeoutID = window.setInterval(function () {
        if (global_Tealeaf_Key) {
          window.clearInterval(timeoutID);
          DramaCore.renderTealeafChart(global_Tealeaf_Key);
          global_Tealeaf_Key = "";
        }
      }, 1000);
    }
  }
  dp.addEventListener("change", function () {
    let dayRange = this.getSelectedRange();
    if (dayRange.daySpan > 0) {
      let _sd = moment(this.startDate).format("YYYY-MM-DD");
      let _ed = moment(this.endDate).format("YYYY-MM-DD");
      //sessionStorage.setItem(defaultStartDate, _sd);
      //sessionStorage.setItem(defaultEndDate, _ed);
      //sessionStorage.setItem('daySpan', dayRange.daySpan);
      let type = "post";
      let para = {
        "MemberKey": memberKey,
        "StartDate": _sd,
        "EndDate": _ed,
        "LoginId": loginID,
      };
      if (_sd && _ed) {
        let newtealeafKey = prefix + "tealeaf-" + _sd + "-" + _ed + "-" + dayRange.daySpan;
        if (localStorage.getItem(newtealeafKey)) {
          //initInteractivePage.widget();
          initInteractivePage.chart();
          DramaCore.renderTealeafChart(newtealeafKey);
        } else {
          para = {
            "MemberKey": memberKey,
            "StartDate": _sd,
            "EndDate": _ed,
            "LoginId": loginID,
          };
          global_Tealeaf_Key = DramaCore.getRemoteData(tealeafReporturl, type, para, newtealeafKey, fromTealeaf);
          let timeoutID = window.setInterval(function () {
            if (global_Tealeaf_Key) {
              /// console.log(global_Tealeaf_Key);
              window.clearInterval(timeoutID);
              DramaCore.renderTealeafChart(global_Tealeaf_Key);
              global_Tealeaf_Key = "";
            }
          }, 1000);
        }
      }
    }
  });
});
function randomGenerateColor(selector, index) {
  let colorArray1 = ["#280E3B", "#632A7E", "#792A7E", "#A13E97", "#CC76B5", "#D3B7D8"];
  let colorArray2 = ["#003853", "#194b64", "#3d82ab", "#45b299", "#43ccaa", "#91d4c2"];
  let rtnColorCode;
  //console.log(selector);
  //selector : fixed or random color array,sequence (1,2,3(random)): fix color array index 
  switch (selector) {
    case 1:
      rtnColorCode = colorArray1[index];
      break;
    case 2:
      rtnColorCode = colorArray2[index];
      break;
    case 3:
      rtnColorCode = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
      break;
  }
  return rtnColorCode;
}
var initInteractivePage = {
  widget: function () {
    let widgetMetaData = [
      { id: 'w1', icons: 'e-icons fas fa-desktop' },
      { id: 'w2', icons: 'e-icons fas fa-desktop' },
      { id: 'w3', icons: 'e-icons fas fa-desktop' },
      { id: 'w4', icons: 'e-icons fas fa-desktop' },
      { id: 'w5', icons: 'e-icons fas fa-mobile-alt' },
      { id: 'w6', icons: 'e-icons fas fa-mobile-alt' },
      { id: 'w7', icons: 'e-icons fas fa-mobile-alt' },
      { id: 'w8', icons: 'e-icons fas fa-mobile-alt' },
      { id: 'w9', icons: 'e-icons fas fa-tablet' },
      { id: 'w10', icons: 'e-icons fas fa-tablet' },
      { id: 'w11', icons: 'e-icons fas fa-tablet' },
      { id: 'w12', icons: 'e-icons fas fa-tablet' }
    ];
    let widgetMetaData2 = [
      { id: 'w13', icons: 'e-icons fas fa-desktop' },
      { id: 'w14', icons: 'e-icons fas fa-desktop' },
      { id: 'w15', icons: 'e-icons fas fa-desktop' },
    ];
    var getWidgetString = ej.base.compile('<div class="col-sm-12 col-md-3 mb-1"> <div id=${id} class="card card_animation"> <div class="card-header widget-head "> <h5 class="widget-text align-middle"></h5> </div><div class="card-body"> <i class="${icons}"></i> <h5 class="widget-number align-middle"></h5> </div></div></div>');
    var getWidgetString2 = ej.base.compile('<div class="col-sm-12 col-md-4 mb-1"> <div id=${id} class="card card_animation"> <div class="card-header widget-head "> <h5 class="widget-text align-middle"></h5> </div><div class="card-body"> <i class="${icons}"></i> <h5 class="widget-number align-middle"></h5> </div></div></div>');
    while (widgetitems.firstChild) {
      widgetitems.removeChild(widgetitems.firstChild);
    }
    widgetMetaData2.forEach(data => {
      widgetitems.appendChild(getWidgetString2(data)[0]);
    });
    widgetMetaData.forEach(data => {
      widgetitems.appendChild(getWidgetString(data)[0]);
    });
  },
  chart: function () {
    let chartMetaData = [
      { id: 'chart1', size: 'col-md-6' },
      { id: 'chart2', size: 'col-md-6' },
      { id: 'chart3', size: 'col-md-6' },
      { id: 'chart4', size: 'col-md-6' },
      { id: 'chart5', size: 'col-md-6' },
      { id: 'chart6', size: 'col-md-6' },
      { id: 'chart7', size: 'col-md-6' },
      { id: 'chart8', size: 'col-md-6' },
      { id: 'chart9', size: 'col-md-4' },
      { id: 'chart10', size: 'col-md-4' },
      { id: 'chart11', size: 'col-md-4' },
      { id: 'chart12', size: 'col-md-4' },
      { id: 'chart13', size: 'col-md-4' },
      { id: 'chart14', size: 'col-md-4' },
      { id: 'chart15', size: 'col-md-4' },
      { id: 'chart16', size: 'col-md-4' },
      { id: 'chart17', size: 'col-md-4' },

    ];
    let getChartString = ej.base.compile('<div class="col-sm-12 ${size} mb-1 d-flex"> <div id=${id} class="card card_animation flex-fill"> <div class="card-header widget-head"> </div><div class="charts card-body chart-height"> </div> </div>  </div>');
    while (chartitems.firstChild) {
      chartitems.removeChild(chartitems.firstChild);
    }
    chartMetaData.forEach(data => {
      chartitems.appendChild(getChartString(data)[0]);
    });
  }
};
var TealeafChart = {
  Process: function (reportDefinition, tlt) {
    data = tlt.Data
    let dataAry = [0, 0, 0, 0, 0, 0], metric = [];
    let colcorAry = [];
    let _nameAry = (Object.values(reportDefinition.ColName))
    for (let i = 0; i < data.length; i++) {
      colcorAry.push(randomGenerateColor(1, i));
      dataAry[0] += Number(data[i].Col1);
      dataAry[1] += Number(data[i].Col2);
      dataAry[2] += Number(data[i].Col3);
      dataAry[3] += Number(data[i].Col4);
      dataAry[4] += Number(data[i].Col5);
      dataAry[5] += Number(data[i].Col6);
    }
    for (let i = 0; i < _nameAry.length; i++) {
      metric.push({
        name: _nameAry[i],
        value: dataAry[i]
      });
    }
    let setting = {
      cardHead: reportDefinition.CName,
      titleText: "",
      color: colcorAry,
      metric: metric,
      legend: _nameAry,
      chartid: reportDefinition.ChartId,
      toolbar: true
    };
    let chart = echarts.init(document.getElementById(reportDefinition.ChartId).getElementsByClassName('card-body')[0]);
    let option = chartSetting.funnelChart(setting);
    if (option && typeof option === "object") {
      $("#" + reportDefinition.ChartId).children(".card-header").text(setting.cardHead);
      chart.setOption(option, true);
      window['tooltip' + reportDefinition.ChartId] = new ej.popups.Tooltip({
        width: '180px',
        height: '40px',
        content: ''
      });
      //window['tooltip' + reportDefinition.ChartId].appendTo("#" + reportDefinition.ChartId);
    }
  },
  HourAnalysis: function (reportDefinition, tlt) {

    const hourAry = []
    const metricAry = []
    const metricAry2 = []
    tlt.Data.sort(function (a, b) {
      return a.Col1 - b.Col1;
    });
    const objectArray = Object.entries(tlt.Data);
    objectArray.forEach(([key, value]) => {
      //console.log(key); // 'one'
      hourAry.push(parseInt(value.Col1, 10))
      metricAry.push(parseInt(value.Col2, 10))
      metricAry2.push(parseInt(value.Col2, 10) + 20)
    });
    const _ary = DramaCore.aryMinMaxAvg(metricAry)
    let setting = {
      cardHead: reportDefinition.CName,
      category: hourAry,
      min: [_ary[0], _ary[0]],
      max: [_ary[1] + 50, _ary[1] + 50],
      interval: [Math.floor(_ary[2]), Math.floor(_ary[2])],
      name: ['人數', '人數'],
      chartid: reportDefinition.ChartId,
      data: [metricAry, metricAry2]
    };
    let chart = echarts.init(document.getElementById(reportDefinition.ChartId).getElementsByClassName('card-body')[0]);
    let option = chartSetting.mixLineAndBar(setting);
    if (option && typeof option === "object") {
      $("#" + reportDefinition.ChartId).children(".card-header").text(setting.cardHead);
      chart.setOption(option, true);
      window['tooltip' + reportDefinition.ChartId] = new ej.popups.Tooltip({
        width: '180px',
        height: '40px',
        content: ''
      });
      //window['tooltip' + reportDefinition.ChartId].appendTo("#" + reportDefinition.ChartId);
    }
  },
  DeviceAnalysis: function (reportDefinition, tlt) {

    let pieAry = []
    for (const element of tlt.Data) {
      switch (element.Col1) {
        case '[Others]':
          pieAry.push({ name: '無法確認', value: element.Col2 })
          break
        case 'Desktop':
          pieAry.push({ name: '桌機', value: element.Col2 })
          break
        case 'MobilePhone':
          pieAry.push({ name: '手機', value: element.Col2 })
          break
        case 'Tablet':
          pieAry.push({ name: '平板', value: element.Col2 })
          break
      }

    }
    let data = [];
    let color = ['#00ffff', '#00cfff', '#006ced', '#ffe000', '#ffa800', '#ff5b00', '#ff3000']
    for (var i = 0; i < pieAry.length; i++) {
      data.push({
        value: pieAry[i].value,
        name: pieAry[i].name,
        itemStyle: {
          normal: {
            borderWidth: 5,
            shadowBlur: 20,
            borderColor: color[i],
            shadowColor: color[i]
          }
        }
      }, {
        value: 2,
        name: '',
        itemStyle: {
          normal: {
            label: {
              show: false
            },
            labelLine: {
              show: false
            },
            color: 'rgba(0, 0, 0, 0)',
            borderColor: 'rgba(0, 0, 0, 0)',
            borderWidth: 0
          }
        }
      });
    }
    let seriesOption = [{
      name: '',
      type: 'pie',
      clockWise: false,
      radius: [40, 50],
      hoverAnimation: false,
      itemStyle: {
        normal: {
          label: {
            show: true,
            position: 'outside',
            color: '#111',
            formatter: function (params) {
              var percent = 0;
              var total = 0;
              for (var i = 0; i < pieAry.length; i++) {
                total += pieAry[i].value;
              }
              percent = ((params.value / total) * 100).toFixed(1);
              if (params.name !== '') {
                return params.name + '\n' + '數量' + params.value + '\n' + '占百分比：' + percent + '%';
              } else {
                return '';
              }
            },
          },
          labelLine: {
            length: 40,
            length2: 50,
            show: true,
            color: '#111'
          }
        }
      },
      data: data
    }];
    let setting = {
      title: "進站裝置數量分析",
      data: ['桌機', '平板', '手機', '無法確認'],
      cardHead: reportDefinition.CName,
      color: color,
      chartid: reportDefinition.ChartId,

    };
    let chart = echarts.init(document.getElementById(reportDefinition.ChartId).getElementsByClassName('card-body')[0]);
    let option = chartSetting.multiDimensionPieChart(setting, seriesOption);
    if (option && typeof option === "object") {
      $("#" + reportDefinition.ChartId).children(".card-header").text(setting.cardHead);
      chart.setOption(option, true);
      window['tooltip' + reportDefinition.ChartId] = new ej.popups.Tooltip({
        width: '180px',
        height: '40px',
        content: ''
      });
      //window['tooltip' + reportDefinition.ChartId].appendTo("#" + reportDefinition.ChartId);
    }
  },
  GeographyAnalysis: function (reportDefinition, tlt) {

  },
  SourceAnalysis: function (reportDefinition, tlt) {
    //console.log(tlt)
    let category = [], metric = [], color = ["#4A90E2"];
    tlt.Data.sort(function (a, b) {
      return a.Col1 - b.Col1;
    });
    const objectArray = Object.entries(tlt.Data);
    objectArray.forEach(([key, value]) => {
      //console.log(key); // 'one'
      category.push(value.Col1)
      metric.push(parseInt(value.Col2, 10))
    });


    let seriesOption = [{

      type: 'bar',
      barWidth: '75%',
      data: metric.slice(0, 9),
      itemStyle: {
        normal: {
          barBorderRadius: 4,
          color: color
        }
      },

      label: {
        normal: {
          show: true,
          position: 'inside',
          fontSize: 10,
        }
      },
    }]

    let setting = {
      cardHead: reportDefinition.CName,
      chartid: reportDefinition.ChartId,
      grid: { top: 15, left: 100 },
      xAxistype: 'value',
      yAxistype: 'category',
      xFormatter: function (value, index) {
        // 將數值轉換為 K,M
        function intlFormat(num) {
          return new Intl.NumberFormat().format(Math.round(num * 10) / 10);
        }

        if (value >= 1000000)
          return intlFormat(value / 1000000) + 'M';
        if (value >= 1000)
          return intlFormat(value / 1000) + 'k';
        return intlFormat(value);

      },
      yFormatter: function (param) {
        return param
      },
      xValue: '',
      yValue: category.slice(0, 9),
      interval: 0,
      rotate: 35,
      fontSize: 9,

    };
    let chart = echarts.init(document.getElementById(reportDefinition.ChartId).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartV2(setting, seriesOption);
    if (option && typeof option === "object") {
      $("#" + reportDefinition.ChartId).children(".card-header").text(setting.cardHead);
      chart.setOption(option, true);
      window['tooltip' + reportDefinition.ChartId] = new ej.popups.Tooltip({
        width: '180px',
        height: '40px',
        content: ''
      });
      // window['tooltip' + reportDefinition.ChartId].appendTo("#" + reportDefinition.ChartId);
    }
  },
  Merge: function (header, chartId, tlt) {

    let dashedPic = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAM8AAAAOBAMAAAB6G1V9AAAAD1BMVEX////Kysrk5OTj4+TJycoJ0iFPAAAAG0lEQVQ4y2MYBaNgGAMTQQVFOiABhlEwCugOAMqzCykGOeENAAAAAElFTkSuQmCC';
    let color = ['#FF8700', '#ffc300', '#00e473', '#009DFF', '#0034ff'];
    let sum = 0; dataChart = {}, dataAry = [], categorary = []
    let pieSeries = [],
      lineYAxis = [];
    for (item of tlt) {
      let _metricsum = 0
      dataChart = {}

      //console.log(item.Data)
      for (let i = 0; i < item.Data.length; i++) {
        categorary.push(item.Name.substring(19, 22))
        _metricsum += Number(item.Data[i].Col2)
      }

      sum += _metricsum
      dataChart.dimenson = item.Name.substring(19, 22)
      dataChart.metric = _metricsum
      dataAry.push(dataChart)
    }

    dataAry.forEach((v, i) => {
      pieSeries.push({
        name: '',
        type: 'pie',
        clockWise: false,
        hoverAnimation: false,
        radius: [75 - i * 15 + '%', 67 - i * 15 + '%'],
        center: ["40%", "50%"],
        label: {
          show: false
        },
        data: [{
          value: v.metric,
          name: v.dimenson
        }, {
          value: sum - v.metric,
          name: '',
          itemStyle: {
            color: "rgba(0,0,0,0)"
          }
        }]
      });
      pieSeries.push({
        name: '',
        type: 'pie',
        silent: true,
        z: 1,
        clockWise: false, //顺时加载
        hoverAnimation: false, //鼠标移入变大
        radius: [75 - i * 15 + '%', 67 - i * 15 + '%'],
        center: ["40%", "50%"],
        label: {
          show: false
        },
        data: [{
          value: 7.5,
          itemStyle: {
            color: "#E3F0FF"
          }
        }, {
          value: 2.5,
          name: '',
          itemStyle: {
            color: "rgba(0,0,0,0)"
          }
        }]
      });
      v.percent = (v.metric / sum * 100).toFixed(1) + "%";
      lineYAxis.push({
        value: i,
        textStyle: {
          rich: {
            circle: {
              color: color[i],
              padding: [0, 5]
            }
          }
        }
      });
    })

    let setting = {
      cardHead: header,
      titleText: '',
      legend: '',
      color: color,
      dashedPic: dashedPic,
      chartData: dataAry,
      lineYAxis: lineYAxis,
      //color: ['#4A90E2'],
      chartid: chartId,
      //value: metricary
    };
    let chart = echarts.init(document.getElementById(chartId).getElementsByClassName('card-body')[0]);
    let option = chartSetting.fivePieChart(setting, pieSeries);
    if (option && typeof option === "object") {
      $("#" + chartId).children(".card-header").text(setting.cardHead);
      chart.setOption(option, true);
      window['tooltip' + chartId] = new ej.popups.Tooltip({
        width: '180px',
        height: '40px',
        content: ''
      });
      //window['tooltip' + chartId].appendTo("#" + chartId);
    }
  }


};
/* 
   Process: 漏斗圖 ,
   DimHours :各時段柱狀圖,
   DimMerge:各險種柱狀圖,
   DimGeography:地理圖,
   DimDevice:設備圖,
   DimSource:來源圖,
*/
var DrawChart = (function (TLTData) {
  let mergeAry = [], len = 1
  for (const tlt of TLTData) {

    const item = reportDefinition.find(ele => ele.Name == tlt.Name);
    //console.log('item', item)
    switch (item.ReportType) {
      case 'Process':
        //console.log(tlt);
        TealeafChart.Process(item, tlt)
        break
      case 'DimHours':
        TealeafChart.HourAnalysis(item, tlt)
        break
      case 'DimMerge':
        mergeAry.push(tlt)
        //console.log('Mangoes and papayas are $2.79 a pound.');
        // expected output: "Mangoes and papayas are $2.79 a pound."
        break;
      case 'DimGeography':
        // console.log('DimGeography');
        // expected output: "Mangoes and papayas are $2.79 a pound."
        break
      case 'DimDevice':
        TealeafChart.DeviceAnalysis(item, tlt)
        // console.log('DimDevice');
        // expected output: "Mangoes and papayas are $2.79 a pound."
        break
      case 'DimSource':

        TealeafChart.SourceAnalysis(item, tlt)
        break

    }
    if (len == TLTData.length) {
      TealeafChart.Merge('各險種單一工作階段完成', 'chart8', mergeAry)
    } else {
      len += 1
    }
  }

});
