DramaCore.loadCultureFiles('zh');
//DramaCore.sideBar();
const widgetitems_l = (document.getElementById('dataWidget_l'));
const widgetitems_r = (document.getElementById('dataWidget_r'));
const chartitems = (document.getElementById('dataChart'));
const prefix =sessionStorage.getItem("sitePrefix");
const appName =sessionStorage.getItem("appName")
var refIndex1 = 30000; // 訪客數參考指標
var L10n = ej.base.L10n;
var dp,DataObj, Dataobj1;

const gridColumn = [{
    field: 'ADGroup',
    headerText: '廣告群組',
    textAlign: 'center',
    width: 180,
    format: 'N'
  },
  {
    field: 'impression',
    headerText: '曝光',
    textAlign: 'left',
    width: 80,
    format: 'N'
  },
  {
    field: 'adClick',
    width: 80,
    headerText: '廣告點擊',
    format: 'N'
  },
  {
    field: 'adCost',
    width: 80,
    headerText: '廣告成本',
    format: 'N',
    
  },
  {
    field: 'CPM',
    headerText: 'CPM',
    textAlign: 'left',
    width: 80,
    format: 'N'
  },
  {
    field: 'CPC',
    headerText: 'CPC',
    textAlign: 'left',
    width: 80,
    format: 'N'
  },
  {
    field: 'costPerGoalConversion',
    headerText: '廣告轉換成本',
    textAlign: 'left',
    width: 80,
    format: 'N'
  },
  {
    field: 'costPerTransaction',
    headerText: '交易成本',
    textAlign: 'left',
    width: 80,
    format: 'N'
  },
];
var customgridColumn = [{
  field: 'referral',
  headerText: '參考來源',
  textAlign: 'center',
  width: 100,
  type: 'string'
},
{
  field: 'sessions',
  width: 80,
  headerText: '工作階段',
  type: 'N'
},
{
  field: 'uniquePageviews',
  width: 100,
  headerText: '不重複瀏覽量',
  type: 'N'
},
{
  field: 'users',
  width: 100,
  headerText: '訪客數',
  type: 'string',
  template: '#gridTemplate'
},
{
  field: 'newUsers',
  headerText: '新訪客數',
  textAlign: 'left',
  width: 100,
  format: 'N'
},
{
  field: 'bounces',
  headerText: '跳出數',
  textAlign: 'left',
  width: 100,
  format: 'N'
},
{
  field: 'avgSessionDuration',
  headerText: '平均停留時間',
  textAlign: 'left',
  width: 100,
  format: 'string'
}
];



$(document).ready(function () {
  
    //DramaCore.validateTokenCall(validateSetting());
    let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
    dp = DramaCore.createDatePicker(dt, dt);
    let allBtnGroups= document.getElementsByClassName("allbtngroup");
    let defaultkey;
    for (let i = 0; i < allBtnGroups.length; i++) {
        
      allBtnGroups[i].addEventListener('click', function(event){
        let keyType=(event.srcElement.dataset.filter);
        
        for(let j=0 ;j<allBtnGroups.length;j++ ){
          allBtnGroups[j].style.color="#fff";
          allBtnGroups[j].style.fontSize="18px";
          allBtnGroups[j].style.fontWeight="200";
        }
        event.srcElement.style.color="#0B0500";
        event.srcElement.style.fontSize="26px";
        event.srcElement.style.fontWeight="700";
        switch (keyType){
          case 'default':
            
            //defaultkey =prefix+"-"+"default-" + sessionStorage.getItem(defaultStartDate) + "-" + sessionStorage.getItem(defaultEndDate) + "-" + sessionStorage.getItem(daySpan);
            defaultkey =prefix+"-"+"default-" + dt + "-" + dt+ "-1"; 
            dp.value=[sessionStorage.getItem(defaultStartDate),sessionStorage.getItem(defaultEndDate)];  
            initDefaultGAPage.widget();
            initDefaultGAPage.chart();
            console.log('defaultkey',defaultkey)
            DramaCore.renderDefaultGAChart(defaultkey);
            document.getElementById("_ad01").click();          
            break;
          case 'realtime':

            let realtimedt = moment().format("YYYY-MM-DD");
            let realtimeKey= prefix+"-"+keyType+"-" + realtimedt+ "-" + realtimedt + "-" + '1';
            dp.value=[realtimedt,realtimedt];  
            initDefaultGAPage.realtime(realtimeKey,appName);
            
            break;
        }
      }, false);
    }
    
    //let key = prefix+"default-" + dt + "-" + dt + "-" + "1";
    
    document.getElementById("_all01").click();
    
 
});


// url --> remote url ; type:post or get ; para:parameters
var initDefaultGAPage={
    widget:function(){
        let  widgetMetaData=[
            {id:'w1',icons:'e-icons session'},
            {id:'w2',icons:'e-icons views'},
            {id:'w3',icons:'e-icons profile'},
            {id:'w4',icons:'e-icons profile'},
            {id:'w5',icons:'e-icons avgtime'},
            {id:'w6',icons:'e-icons session1'},
            {id:'w7',icons:'e-icons profile1'},
            {id:'w8',icons:'e-icons bounce'}
          
        ];
        let  widgetMetaData2=[
          {id:'w9',icons:'e-icons session',chartid:'w9c'},
          {id:'w10',icons:'e-icons views',chartid:'w10c'},
          {id:'w11',icons:'e-icons profile',chartid:'w11c'},
          {id:'w12',icons:'e-icons profile',chartid:'w12c'},
          {id:'w13',icons:'e-icons avgtime',chartid:'w13c'},
          {id:'w14',icons:'e-icons session1',chartid:'w14c'},
          {id:'w15',icons:'e-icons profile1',chartid:'w15c'},
          {id:'w16',icons:'e-icons bounce',chartid:'w16c'}
        
      ];
      var getWidgetStringl = ej.base.compile('<div class="col-sm-6 col-md-3 mb-1"> <div id=${id} class="card card_animation"> <div class="card-header widget-head "> <h5 class="widget-text align-middle"></h5> </div><div class="card-body"> <span class="${icons}"></span> <h5 class="widget-number align-middle"></h5> </div></div></div>');
        var getWidgetStringr = ej.base.compile('<div class="col-sm-6 col-md-3 mb-1"> <div id=${id} class="card card_animation"> <div class="card-header widget-head "> <h5 class="widget-text align-middle"></h5> </div><div class="card-body"> <span id="${chartid}" class="widgetchartbox"></span> <h5 class="widget-number align-middle"></h5> </div></div></div>');
        
        while (widgetitems_l.firstChild) {
          widgetitems_l.removeChild(widgetitems_l.firstChild);
        }
        widgetMetaData.forEach(data => {
          widgetitems_l.appendChild(getWidgetStringl(data)[0]);
        });
        while (widgetitems_r.firstChild) {
          widgetitems_r.removeChild(widgetitems_r.firstChild);
        }
        widgetMetaData2.forEach(data => {
          widgetitems_r.appendChild(getWidgetStringr(data)[0]);
        });
        
    },
    chart:function(){
        let  chartMetaData=[
            {id:'chart1',size:'col-md-6'},
            {id:'chart2',size:'col-md-6'},
            {id:'chart3',size:'col-md-6'},
            {id:'chart4',size:'col-md-6'},
            {id:'chart5',size:'col-md-6'},
            {id:'chart6',size:'col-md-6'},
            {id:'chart7',size:'col-md-12'},
            {id:'chart8',size:'col-md-12'},
      
          
        ];
        let getChartString = ej.base.compile('<div class="col-sm-12 ${size} mb-1 d-flex"> <div id=${id} class="card card_animation flex-fill"> <div class="card-header widget-head"> </div><div class="charts card-body chart-height"> </div> </div>  </div>');
        while (chartitems.firstChild) {
          chartitems.removeChild(chartitems.firstChild);
        }
        chartMetaData.forEach(data => {
            chartitems.appendChild(getChartString(data)[0]);
        });
    },
    realtime:function(siteKey,siteName){
     
      let para = {};
      const app = siteDefine.find(element => element.name == siteName);
      localStorage.removeItem(siteKey);
      let realtimedt = moment().format("YYYY-MM-DD");
      let GA_Dimensions1 = ["date","sourceMedium"]; // dimension 空白表示為日期合計
      let GA_Metrics1 = ["uniquePageviews", "bounces", "users", "newUsers", "sessions", "pageviewsPerSession", "avgSessionDuration", "avgTimeOnPage"];
      let GA_Dimensions2 = ["adGroup"];
      let GA_Metrics2 = ["impressions", "adClicks", "adCost", "CPM", "CPC", "costPerGoalConversion", "costPerTransaction", "RPC", "ROAS"];
      let GA_Dimensions3 = ["sourceMedium"];
      let GA_Metrics3 = ["transactions", "transactionRevenue", "transactionsPerSession", "revenuePerTransaction", "goal1Completions", "goal2Completions", "goal3Completions", "goal4Completions"];
      let GA_Dimensions4 = ["campaign"];
      let GA_Metrics4 = ["transactions", "transactionRevenue", "transactionsPerSession", "revenuePerTransaction", "goal1Completions", "goal2Completions", "goal3Completions", "goal4Completions"];
      let GA_Dimensions5 = ["sourceMedium","campaign"];
      let GA_Metrics5 = ["impressions", "adClicks", "adCost", "CPM", "CPC", "costPerGoalConversion", "costPerTransaction", "RPC", "ROAS"];

      try {
          
          para = {
              "MemberKey": app.memberKey,
              "StartDate": realtimedt,
              "EndDate": realtimedt,
              "LoginId": sessionStorage.getItem("loginID"),
              "ReqDimension1": GA_Dimensions1.join(),
              "ReqMetric1": GA_Metrics1.join(),
              "ReqDimension2": GA_Dimensions2.join(),
              "ReqMetric2": GA_Metrics2.join(),
              "ReqDimension3": GA_Dimensions3.join(),
              "ReqMetric3": GA_Metrics3.join(),
              "ReqDimension4": GA_Dimensions4.join(),
              "ReqMetric4": GA_Metrics4.join(),
              "ReqDimension5": GA_Dimensions5.join(),
              "ReqMetric5": GA_Metrics5.join(),
          };
          if (siteKey != "") {
            let myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
  
            let urlencoded = new URLSearchParams();
            urlencoded.append("MemberKey", para.MemberKey);
            urlencoded.append("StartDate", para.StartDate);
            urlencoded.append("EndDate", para.EndDate);
            urlencoded.append("LoginId", para.LoginId);
            urlencoded.append("ReqDimension1", para.ReqDimension1);
            urlencoded.append("ReqMetric1", para.ReqMetric1);
            urlencoded.append("ReqDimension2", para.ReqDimension2);
            urlencoded.append("ReqMetric2", para.ReqMetric2);
            urlencoded.append("ReqDimension3", para.ReqDimension3);
            urlencoded.append("ReqMetric3", para.ReqMetric3);
            urlencoded.append("ReqDimension4", para.ReqDimension4);
            urlencoded.append("ReqMetric4", para.ReqMetric4);
            urlencoded.append("ReqDimension5", para.ReqDimension5);
            urlencoded.append("ReqMetric5", para.ReqMetric5);
  
            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: urlencoded,
                redirect: 'follow'
            };
            //spinner.removeAttribute('hidden');
            $("body").addClass("loading"); 
            fetch((sessionStorage.getItem("GoogleremoteUrl") + "CustomGAReport"), requestOptions)
                .then(response => response.text())
                .then(result => {
                  
                    localStorage.setItem(siteKey, result);
                    //spinner.setAttribute('hidden', '');
                    initDefaultGAPage.widget();
                    initDefaultGAPage.chart();
                    DramaCore.renderDefaultGAChart(siteKey);
                    $("body").removeClass("loading"); 
                    document.getElementById("_ad01").click();      
                })
                .catch(error => console.log('error', error));
        }
      } catch (e) {
          console.log(e);
          sitePrefix = "";
      }
      
    }

};


// 客製化表格欄位值

var DefaultCharts = {
  chart1: function (data,location, rowMax, header,chartid) {
    rowMax += 1; // 因迴圈需求，資料筆數再加 1
	
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
      console.log('chart1');
	  console.log(rst.ListResult);
    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[0].value;
    }
    
    let entries = Object.entries(chartObj);
	console.log('chartobj',chartObj)
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
	 
	if(arr!=undefined){
		if (arr[1]==0){
			//產生偽隨機值
			arr[1]=Math.floor(Math.random() * 10000);
		}
		dimension.push(arr[0]);
		metric.push(arr[1]);
	}	
      
    }
	
    let color = ["#4A90E2"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: [],
      chartid:chartid
    };
    
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart2: function (data,location, rowMax, header,chartid) {
    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
      
    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[1].value;
    }
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
		if(arr!=undefined){
		if (arr[1]==0){
			//產生偽隨機值
			arr[1]=Math.floor(Math.random() * 10000);
		}
		dimension.push(arr[0]);
      metric.push( Math.round(arr[1]));
	}	
      
    }
    let color = ["#7ED321"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: [],
      chartid:chartid
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      //console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart3: function (data,location, rowMax, header,chartid) {
    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
      
    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[4].value;
    }
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
	  if(arr!=undefined){
		if (arr[1]==0){
			//產生偽隨機值
			arr[1]=Math.floor(Math.random() * 10000);
		}
		dimension.push(arr[0]);
		metric.push( Math.round(arr[1]));
		}	
  
    }
    let color = ["#FB7507"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: [],
      chartid:chartid
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart4: function (data,location, rowMax, header,chartid) {
    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
      
    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[0].value;
    }
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
       if(arr!=undefined){
		if (arr[1]==0){
			//產生偽隨機值
			arr[1]=Math.floor(Math.random() * 10000);
		}
		dimension.push(arr[0]);
		metric.push( Math.round(arr[1]));
		}	
    }
    let color = ["#4A90E2"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: [],
      chartid:chartid
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart5:function (data,location, rowMax, header,chartid) {
    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
     
    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[1].value;
    }
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
       if(arr!=undefined){
		if (arr[1]==0){
			//產生偽隨機值
			arr[1]=Math.floor(Math.random() * 10000);
		}
		dimension.push(arr[0]);
		metric.push( Math.round(arr[1]));
		}	
    }
    let color = ["#7ED321"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: [],
      chartid:chartid
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart6:function (data,location, rowMax, header,chartid) {
    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
      
    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[4].value;
    }
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
       if(arr!=undefined){
		if (arr[1]==0){
			//產生偽隨機值
			arr[1]=Math.floor(Math.random() * 10000);
		}
		dimension.push(arr[0]);
		metric.push( Math.round(arr[1]));
		}	
    }
    let color = ["#FB7507"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: [],
      chartid:chartid
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
     
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart7: function (data,location, rowMax, header,chartid) {
    let rst = data[location],
    
      chartObj = {},
      list = [],
      metric = [];

    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj = {};
      chartObj.ADGroup=rst.ListResult[i].dimension[1];
      for (let j = 0; j < rst.ListResult[i].metrics[0].metric.length; j++) {
        chartObj.impression =Number(rst.ListResult[i].metrics[0].metric[0].value) ;
        chartObj.adClick = Number(rst.ListResult[i].metrics[0].metric[1].value) ;
        chartObj.adCost = Number(rst.ListResult[i].metrics[0].metric[2].value)  ;
        chartObj.CPM = Number(rst.ListResult[i].metrics[0].metric[3].value)  ;
        chartObj.CPC = Number(rst.ListResult[i].metrics[0].metric[4].value)  ;
        chartObj.costPerGoalConversion = Number(rst.ListResult[i].metrics[0].metric[5].value)  ;
        chartObj.costPerTransaction = Number(rst.ListResult[i].metrics[0].metric[6].value)  ;
   

      }
      list.push(chartObj);

    }
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: gridColumn,
      chartid:chartid
    };
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart7 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
  },
  chart8: function (data,location, rowMax, header,chartid) {
    let rst = data[location],
    
      chartObj = {},
      list = [],
      metric = [];

    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj = {};
      chartObj.ADGroup=rst.ListResult[i].dimension[0];
      for (let j = 0; j < rst.ListResult[i].metrics[0].metric.length; j++) {
        chartObj.impression =Number(rst.ListResult[i].metrics[0].metric[0].value) ;
        chartObj.adClick = Number(rst.ListResult[i].metrics[0].metric[1].value) ;
        chartObj.adCost = Number(rst.ListResult[i].metrics[0].metric[2].value)  ;
        chartObj.CPM = Number(rst.ListResult[i].metrics[0].metric[3].value)  ;
        chartObj.CPC = Number(rst.ListResult[i].metrics[0].metric[4].value)  ;
        chartObj.costPerGoalConversion = Number(rst.ListResult[i].metrics[0].metric[5].value)  ;
        chartObj.costPerTransaction = Number(rst.ListResult[i].metrics[0].metric[6].value)  ;
   

      }
      list.push(chartObj);

    }
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: gridColumn,
      chartid:chartid
    };
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart8 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
  }
  
};

function validateSetting(){
    let bearerToken =sessionStorage.getItem("token");
    let urlTo= AuthremoteUrl+"GetUserName";
    let Setting={
        async: true,
        type: "GET",
        url: urlTo,
        headers: {
            Authorization: "Bearer" +" "+ bearerToken
           
          }
    };
    return Setting;
}
function loadingScreen(responseTime) {
  var html = '<div id="load"></div>';
  $('#lodinghint').append(html); 
  setTimeout(function() {
      $('#load').remove();
  }, responseTime);
}
