var L10n = ej.base.L10n;
var isLabel=false; // check  label exist; if label is exist,the manual paging not work ! 
DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();
const memberGridpageSize=25;
ej.grids.Grid.Inject(ej.grids.Page, ej.grids.Sort, ej.grids.Filter, ej.grids.Resize);
var tagArr = [],
    listViewData, customerData, acrdnObj,grid1;

var dashboard = new ej.layouts.DashboardLayout({
    columns: 16,
    allowDragging: false,
    cellSpacing: [2, 2],
    cellAspectRatio: 100 / 95,
    panels: [{
            'sizeX': 5,
            'sizeY': 1,
            'row': 0,
            'col': 0,
            content: '<br><div id=""><button id="secondSearch">標籤集群</button><button id="clearLabel" >清除選取</button></div>'
        },
        {
            'sizeX': 5,
            'sizeY': 12,
            'row': 1,
            'col': 0,
            header: '<div>標籤選擇器</div>',
            content: '<div id="selector2" class="chart-responsive" ><table id="pg3chkTabel" class=""></table>'
        },
        {
            'sizeX': 11,
            'sizeY': 13,
            'row': 0,
            'col': 5,
            header: '<div>清單列表</div>',
            content: '<div id="grid1" class="chart-responsive" ></div>'
        }



    ]
});
const gridUrl = LabelremoteUrl + "GetGridData";
var labelCate = {
    "Source": "labelCategory",
    "Clause": "",
    "Limit": "0",
    "Offset": "0"
};
var memberData = {
    "Source": "memberlabel",
    "Clause": "",
    "Limit": memberGridpageSize,
    "Offset": "0"
};
var renderComponet=function renderDashBoardComponent(){
    try{
        acrdnObj = new ej.navigations.Accordion({
            expandMode: 'Single',
            width: '93%',
            items: getAccordinItems()
    
        });
        acrdnObj.appendTo('#pg3chkTabel');
        acrdnObj.addEventListener("expanded", function (e) {
            let isEmpty = document.getElementById(e.item.cssClass).innerHTML == "";
            //console.log(isEmpty);
            let i = e.item.cssClass.match(/\d+/g);
            if (isEmpty) {
                document.getElementById(e.item.cssClass).innerHTML = "<i></i>";
                //console.log(document.getElementById(e.item.cssClass));
                window['chklistObj' + i].appendTo('#' + e.item.cssClass);
            }
    
        });
        
        //console.log(customerData[0]);
        grid1 = new ej.grids.Grid({
            dataSource: customerData[0],
            //actionComplete: sort,
            allowExcelExport: true,
            toolbar: [{ text: '下載 CSV', tooltipText: 'Download CSV',prefixIcon: "e-csvexport" ,id: 'CsvExport' },{ text: '下載 Excel', tooltipText: 'Download excel',prefixIcon: "e-excelexport" ,id: 'ExcelExport' }],
            //toolbar: ["CsvExport"],
            Offline: true,
            columns: [{
                    field: 'memberid',
                    headerText: '會員編號',
                    textAlign: 'left',
                    width: 250,
                    type: 'string'
                },
                {
                    field: 'membername',
                    width: 80,
                    headerText: '姓名',
                    type: 'string'
                },

                {
                    field: 'gender',
                    headerText: '性別',
                    textAlign: 'left',
                    width: 20,
                    format: 'string'
                },
                {
                    field: 'membermail',
                    headerText: '電子郵件',
                    textAlign: 'left',
                    width: 250,
                    format: 'string'
                },
                {
                    field: 'shippingaddress',
                    headerText: '遞送地址',
                    textAlign: 'left',
                    width: 250,
                    format: 'string'
                },
                {
                    field: 'label',
                    headerText: '標籤',
                    textAlign: 'left',
                    width: 300,
                    format: 'string',
                    valueAccessor: tagsFormatter,
                    disableHtmlEncode: false
                },
                {
                    field: 'label',
                    headerText: '標籤',
                    textAlign: 'left',
                    width: 300,
                    format: 'string',
                    visible:false,
                    
                },

            ],
            rowHeight: 28,
            //enableVirtualization: true,
            //enableColumnVirtualization: true,
            height: "100%",
            allowPaging: true,
            pageSettings: {
                pageSize: memberGridpageSize
            },
            //allowSorting: true,
            allowTextWrap: true,
            textWrapSettings: {
                wrapMode: 'Content'
            },
            //queryCellInfo: customiseCell
        });

        grid1.dataBound = function () {
            grid1.autoFitColumns(['memberid', 'membername']);
            //let pkColumn = grid1.column[0];
            //pkColumn.isPrimaryKey='true';
        };
        //console.log(grid1);
        grid1.appendTo('#grid1');
        grid1.toolbarClick = function (args) {
           
           // console.log(grid1);
            if (args.item.id === 'CsvExport') {
                grid1.showSpinner();
               // console.log(grid1);
                grid1.columns[5].visible = false;
                let excelExportProperties = {
                    fileName:"資料時間:"+sessionStorage.getItem(defaultStartDate)+".csv"
                };
               // grid1.excelExport(getExcelExportProperties());
               grid1.csvExport(excelExportProperties);
              
            }
            if (args.item.id === 'ExcelExport') {
                
                grid1.columns[5].visible = false;
                grid1.columns[6].visible = true;
                
                let excelExportProperties = {
                    fileName:"資料時間_"+sessionStorage.getItem(defaultStartDate)+".xlsx"
                };
                // grid1.excelExport(getExcelExportProperties());
                grid1.excelExport(excelExportProperties);
                
                
             }

        };
        grid1.beforeExcelExport=()=>{
            //console.log("before");
            
            grid1.showSpinner();
        };
        grid1.excelExportComplete = () => {
          grid1.hideSpinner();  
          grid1.columns[5].visible = true;
          grid1.columns[6].visible = false;
        };
        grid1.csvExportComplete = () => {
            grid1.hideSpinner();  
            grid1.columns[5].visible = true;
            grid1.columns[6].visible = false;
          };
        grid1.actionBegin=function(args){
            console.log(args);
            switch(args.requestType){
            case "paging":
                if(!isLabel){
                    let offset=(args.currentPage-1)*memberGridpageSize;
                   // console.log(offset);
                    memberData.Offset=offset;
                    customerData =JSON.parse(isSession(memberData).responseText);
                   // console.log(customerData);
                    grid1.dataSource=(customerData[0]);
                }
                
               //grid1.refresh();
                break;
           } 
        };

    }catch(ex){

    }
   

};
//Render initialized Accordion component

dashboard.appendTo('#editLayout');

$(document).ready(function () {
    let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
    let labelKey = "labelKey-" + dt + "-" + dt + "-" + "1";
    dp = DramaCore.createDatePicker(dt, dt);
    DramaCore.validateTokenCall(validateSetting());
    if (localStorage.getItem(labelKey)) {
        listViewData = JSON.parse(localStorage.getItem(labelKey));
        customerData =JSON.parse(isSession(memberData).responseText);
        renderComponet();
    } else {
        customerData =JSON.parse(isSession(memberData).responseText);
        global_Label_key = DramaCore.getRemoteData(gridUrl, "post", labelCate, labelKey, "Label");
        //customerData = DramaCore.getGridData(gridUrl, "post", memberData, labelKey, "Label",false);
        
        let timeoutID = window.setInterval(function () {
            //console.log(customerData);
            if (global_Label_key&&customerData) {
                window.clearInterval(timeoutID);
                listViewData = JSON.parse(localStorage.getItem(global_Label_key));
                renderComponet();
                global_Label_key = "";
            }
        }, 1000);
    }
    

    
    button1 = new ej.buttons.Button({
        cssClass: 'e-warning'
    }, '#clearLabel');
    button2 = new ej.buttons.Button({
        cssClass: 'e-info'
    }, '#secondSearch');
    
    


    /**  age selector
    var listObj = new ej.dropdowns.MultiSelect({
        // set the placeholder to MultiSelect input element
        placeholder: 'Select Area ',
        // set the type of mode for how to visualized the selected items in input element.
        mode: 'Box'
    });*/



    //var template='<div>${name}</div><div>${value}</div>';




    //var predicate = new ej.data.Predicate('tags', 'contains', 'COUPON');
    //  predicate = predicate.or('tags', 'contains', '跨平台');
    //  predicate = predicate.or('tags', 'contains', 'T恤');
    //  var result = new ej.data.DataManager(customerData).executeLocal(new ej.data.Query().where(predicate));
    //  console.log(result);




});

function validateSetting() {
    let bearerToken = sessionStorage.getItem("token");
    let urlTo = AuthremoteUrl + "GetUserName";
    let Setting = {
        async: true,
        type: "GET",
        url: urlTo,
        headers: {
            Authorization: "Bearer" + " " + bearerToken

        }
    };
    return Setting;
}

function sort(args) {
    if (args.requestType === 'sorting') {
        for (var i = 0, a = grid.getColumns(); i < a.length; i++) {
            var columns = a[i];
            for (var j = 0, b = grid.sortSettings.columns; j < b.length; j++) {
                var sortcolumns = b[j];
                if (sortcolumns.field === columns.field) {
                    check(sortcolumns.field, true);
                    break;
                }
                else {
                    check(columns.field,false);
                }
            }
        }
}
}
function isSession(data) {
    return $.ajax({
        type: "POST",
        url: gridUrl,
        data: data,
        dataType: "html",
        async: !1,
        error: function() {
            alert("資料庫維護中");
            document.location.href = localurl + loginUrl;

        }
    });
}
function getExcelExportProperties() {
    let Today = new Date();
    let exportDate = Today.getFullYear() + " 年 " + (Today.getMonth() + 1) + " 月 " + Today.getDate() + " 日";
    return {
        header: {
            headerRows: 3,
            rows: [{
                    index: 1,
                    cells: [{
                        index: 1,
                        colSpan: 7,
                        value: 'CACO',
                        style: {
                            fontColor: '#C25050',
                            fontSize: 25,
                            hAlign: 'Center',
                            bold: true
                        }
                    }]
                },
                {
                    index: 3,
                    cells: [{
                            index: 1,
                            colSpan: 2,
                            value: "日期",
                            style: {
                                fontColor: '#C67878',
                                fontSize: 16,
                                bold: true
                            }
                        },
                        {
                            index: 4,
                            value: exportDate,
                            style: {
                                fontColor: '#C67878',
                                bold: true
                            }
                        },

                    ]
                },

            ]
        },



        fileName: "Member.xlsx"
    };
}

function tagsFormatter(field, data, column) {
    var tmp_Str = data.label;
    var tmp_Str2 = '';
    var rtn_Str = '';
    var arr = tmp_Str.split(":");
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] !== "") {
            let tagcolor="";
            
            switch (i%3){
                case 0:
                    tagcolor=" tag-infoB";
                    break;
                case 1:
                    tagcolor=" tag-infoY";
                    break;
                case 2:
                    tagcolor=" tag-infoG";
                    break;
                
            }

            tmp_Str2 = '<span class="tagStyles  m-r-5 m-t-5'+' '+tagcolor + '">' + arr[i] + '</span>';
        }


        rtn_Str = tmp_Str2 + rtn_Str;
    }

    return rtn_Str;
}

function customiseCell(args) {
    //console.log(args.cell);
    if (args.column.field === 'duration') {
        var tmp_Duration = args.data.duration + '%';
        args.cell.querySelector("#bounce").textContent = tmp_Duration;
        if (parseInt(args.data.duration) <= refIndex1) {
            args.cell.querySelector("#bounce").classList.add("bouncestatus");
        }

    }
}
$("#secondSearch").on("click", function () {
    tagArr = getCheckedValue();
    
    if (tagArr.length > 0) {
        memberData.Clause="";
        memberData.Offset=0;
        memberData.Limit=0;
        var predicate = new ej.data.Predicate('label', 'contains', tagArr);
        console.log(predicate);
        /*
        if (tagArr.length > 1) {
            for (var i = 1; i < tagArr.length; i++) {
                
                predicate = predicate.or('label', 'contains', tagArr[i]);
            }
        }*/
        memberData.Clause=tagArr.join('|');
        customerData =JSON.parse(isSession(memberData).responseText);
        //var result = new ej.data.DataManager(customerData).executeLocal(new ej.data.Query().where(predicate));
        console.log(customerData[0].result);
        grid1.dataSource = customerData[0].result;
        grid1.allowSorting=true;
        grid1.goToPage(1);
        isLabel=true;
        memberData.Limit=memberGridpageSize;
    
        
    }else{
        memberData.Clause="";
        memberData.Offset=0;
        memberData.Limit=memberGridpageSize;
        isLabel=false;
        customerData =JSON.parse(isSession(memberData).responseText);
        grid1.dataSource = customerData[0];
        grid1.allowSorting=false;
    }
});
$("#clearLabel").on("click", function () {

    for (let i = 1; i <= listViewData.length; i++) {
        console.log(window['chklistObj' + i]);
        if (window['chklistObj' + i].isRendered === true) {
            window['chklistObj' + i].selectAll(false);
        }


    }
    //var result = new ej.data.DataManager(customerData).executeLocal(new ej.data.Query());
    //grid1.dataSource = result;
    memberData.Clause="";
    memberData.Offset=0;
    memberData.Limit=memberGridpageSize;
    isLabel=false;
    customerData =JSON.parse(isSession(memberData).responseText);
    grid1.dataSource = customerData[0];
    grid1.allowSorting=false;

});

function getCheckedValue() {

    let cbxTags = [];

    /*
    $('input:checkbox:checked[name="tag_active_col[]"]').each(function(i) {
         cbxTags[i] = this.value; 
        
    });*/
    for (let i = 1; i <= listViewData.length; i++) {
        if (typeof window['chklistObj' + i].value !== 'undefined' && window['chklistObj' + i].value.length > 0) {
            cbxTags = cbxTags.concat(window['chklistObj' + i].value);
           // console.log(window['chklistObj' + i].value);
        }


    }

    console.log(cbxTags);
    return cbxTags;
}

function getAccordinItems() {
    let accAry = [],
        j;
    try {
         
        
        for (let i = 0; i < listViewData.length; i++) {
            let itemObj = {};
            itemObj.header = listViewData[i].text;
            itemObj.content = '<div id="' + listViewData[i].id + '"></div>';
            itemObj.cssClass = listViewData[i].id;
            accAry.push(itemObj);
            j = i + 1;
            window['chklistObj' + j] = new ej.dropdowns.ListBox({
                // Set the dataSource property.
                dataSource: listViewData[i].child,
                // Set the selection settings with showCheckbox as enabled.
                selectionSettings: {
                    showCheckbox: true
                }

            });

        }
    } catch (ex) {
        console.log(ex);
    }

    return accAry;
}
function sortArray(arr) {
    var tempArr = [], n;
    for (var i in arr) {
        tempArr[i] = arr[i].match(/([^0-9]+)|([0-9]+)/g);
        for (var j in tempArr[i]) {
            if( ! isNaN(n = parseInt(tempArr[i][j])) ){
                tempArr[i][j] = n;
            }
        }
    }
    tempArr.sort(function (x, y) {
        for (var i in x) {
            if (y.length < i || x[i] < y[i]) {
                return -1; // x is longer
            }
            if (x[i] > y[i]) {
                return 1;
            }
        }
        return 0;
    });
    for (var i in tempArr) {
        arr[i] = tempArr[i].join('');
    }
    return arr;
}