const L10n = ej.base.L10n;
const appName = "tmnewa";
const widgetitems = (document.getElementById('dataWidget'));
const chartitems = (document.getElementById('dataChart'));
const prefix = sessionStorage.getItem("sitePrefix") + "-";
DramaCore.loadCultureFiles('zh');
L10n.load({
  'zh': {
    'daterangepicker': {
      placeholder: '選擇日期區間',
      startLabel: '起始日期',
      endLabel: '截止日期',
      applyText: '確認',
      cancelText: '取消',
      selectedDays: '選擇日期',
      days: '天',
      customRange: '自定義區間'

    }
  }
});
defineApp(appName);
const tealeafReporturl = TealeafremoteUrl + 'TealeafReport';
const funnelLegendA = ["填寫投保資料(房屋資料)","保費試算","確認投保資料頁","目標完成"];//住火險
const funnelLegendB = ["車主頁","商品選擇頁","投保確認（資料總覽）","完成要保頁"];//汽車險
const funnelLegendC = ["試算頁","身分證填寫頁","旅遊明細頁","旅遊明細確認頁","完成要保"];//旅平險
const funnelLegendD = ["資料填寫頁","健康狀態調查","確認要保","完成要保"];//傷害險
const funnelLegendE = ["車主車籍頁","商品選擇頁","投保確認（資料總覽）","目標完成"];//機車險
var dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
sessionStorage.setItem(defaultStartDate, dt);
sessionStorage.setItem(defaultEndDate, dt);
sessionStorage.setItem('daySpan', "1");
$(document).ready(function () {


  dp = DramaCore.createDatePicker(dt, dt);
  var tealeafKey = "tealeaf-" + sessionStorage.getItem(defaultStartDate) + "-" + sessionStorage.getItem(defaultEndDate) + "-" + sessionStorage.getItem(daySpan);
  if (sessionStorage.getItem(defaultStartDate) && sessionStorage.getItem(defaultEndDate) && sessionStorage.getItem(daySpan)) {
    dp.value = [sessionStorage.getItem(defaultStartDate), sessionStorage.getItem(defaultEndDate)];

    initInteractivePage.widget();
    initInteractivePage.chart();
    if (localStorage.getItem(tealeafKey)) {

      DramaCore.renderTealeafChart(tealeafKey);
    } else {

      let type = "post";
      para = {
        "MemberKey": memberKey,
        "StartDate": sessionStorage.getItem(defaultStartDate),
        "EndDate": sessionStorage.getItem(defaultEndDate),
        "LoginId": loginID,

      };
      console.log("TEALEAFkey:" + tealeafKey);
      global_Tealeaf_Key = DramaCore.getRemoteData(tealeafReporturl, type, para, tealeafKey, fromTealeaf);
      let timeoutID = window.setInterval(function () {
        if (global_Tealeaf_Key) {

          window.clearInterval(timeoutID);
          DramaCore.renderTealeafChart(global_Tealeaf_Key);
          global_Tealeaf_Key = "";
        }
      }, 1000);



    }
  }

  dp.addEventListener("change", function () {
    let dayRange = this.getSelectedRange();

    if (dayRange.daySpan > 0) {
      let _sd = moment(this.startDate).format("YYYY-MM-DD");
      let _ed = moment(this.endDate).format("YYYY-MM-DD");

      //sessionStorage.setItem(defaultStartDate, _sd);
      //sessionStorage.setItem(defaultEndDate, _ed);
      //sessionStorage.setItem('daySpan', dayRange.daySpan);
      let type = "post";
      let para = {
        "MemberKey": memberKey,
        "StartDate": _sd,
        "EndDate": _ed,
        "LoginId": loginID,
      };

      if (_sd && _ed) {
        let newtealeafKey = "tealeaf-" + _sd + "-" + _ed + "-" + dayRange.daySpan;


        if (localStorage.getItem(newtealeafKey)) {
          initInteractivePage.chart();
          DramaCore.renderTealeafChart(newtealeafKey);

        } else {

          para = {
            "MemberKey": memberKey,
            "StartDate": _sd,
            "EndDate": _ed,
            "LoginId": loginID,

          };
          global_Tealeaf_Key = DramaCore.getRemoteData(tealeafReporturl, type, para, newtealeafKey, fromTealeaf);
          let timeoutID = window.setInterval(function () {
            if (global_Tealeaf_Key) {
              /// console.log(global_Tealeaf_Key);
              window.clearInterval(timeoutID);
              DramaCore.renderTealeafChart(global_Tealeaf_Key);
              global_Tealeaf_Key = "";
            }
          }, 1000);
        }

      }
    }
  });
});


function randomGenerateColor(selector, index) {
  let colorArray1 = ["#280E3B", "#632A7E", "#792A7E", "#A13E97", "#CC76B5", "#D3B7D8"];
  let colorArray2 = ["#003853", "#194b64", "#3d82ab", "#45b299", "#43ccaa", "#91d4c2"];
  let rtnColorCode;
  //console.log(selector);
  //selector : fixed or random color array,sequence (1,2,3(random)): fix color array index 
  switch (selector) {
    case 1:

      rtnColorCode = colorArray1[index];

      break;
    case 2:
      rtnColorCode = colorArray2[index];
      break;
    case 3:
      rtnColorCode = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
      break;
  }
  return rtnColorCode;
}
var initInteractivePage = {
  widget: function () {
    let widgetMetaData = [
      { id: 'w1', icons: 'e-icons fas fa-desktop' },
      { id: 'w2', icons: 'e-icons fas fa-desktop' },
      { id: 'w3', icons: 'e-icons fas fa-desktop' },
      { id: 'w4', icons: 'e-icons fas fa-desktop' },
      { id: 'w5', icons: 'e-icons fas fa-mobile-alt' },
      { id: 'w6', icons: 'e-icons fas fa-mobile-alt' },
      { id: 'w7', icons: 'e-icons fas fa-mobile-alt' },
      { id: 'w8', icons: 'e-icons fas fa-mobile-alt' },
      { id: 'w9', icons: 'e-icons fas fa-tablet' },
      { id: 'w10', icons: 'e-icons fas fa-tablet' },
      { id: 'w11', icons: 'e-icons fas fa-tablet' },
      { id: 'w12', icons: 'e-icons fas fa-tablet' }


    ];
    let widgetMetaData2 = [
      { id: 'w13', icons: 'e-icons fas fa-desktop' },
      { id: 'w14', icons: 'e-icons fas fa-desktop' },
      { id: 'w15', icons: 'e-icons fas fa-desktop' },


    ];
    /*
      var getWidgetString = ej.base.compile('<div class="col-sm-12 col-md-3 mb-1"> <div id=${id} class="card card_animation"> <div class="card-header widget-head "> <h5 class="widget-text align-middle"></h5> </div><div class="card-body"> <i class="${icons}"></i> <h5 class="widget-number align-middle"></h5> </div></div></div>');
      var getWidgetString2 = ej.base.compile('<div class="col-sm-12 col-md-4 mb-1"> <div id=${id} class="card card_animation"> <div class="card-header widget-head "> <h5 class="widget-text align-middle"></h5> </div><div class="card-body"> <i class="${icons}"></i> <h5 class="widget-number align-middle"></h5> </div></div></div>');
        
        while (widgetitems.firstChild) {
          widgetitems.removeChild(widgetitems.firstChild);
        }
        widgetMetaData2.forEach(data => {
          widgetitems.appendChild(getWidgetString2(data)[0]);
      });
        widgetMetaData.forEach(data => {
            widgetitems.appendChild(getWidgetString(data)[0]);
        });*/

  },
  chart: function () {
    let chartMetaData = [

      { id: 'chart1', size: 'col-md-4' },
      { id: 'chart2', size: 'col-md-4' },
      { id: 'chart3', size: 'col-md-4' },
      { id: 'chart4', size: 'col-md-6' },
      { id: 'chart5', size: 'col-md-6' },
      { id: 'chart6', size: 'col-md-6' },
      { id: 'chart7', size: 'col-md-6' },
      { id: 'chart8', size: 'col-md-6' },
      { id: 'chart9', size: 'col-md-6' },
      { id: 'chart10', size: 'col-md-6' },
      { id: 'chart11', size: 'col-md-6' },
      { id: 'chart12', size: 'col-md-6' },
      { id: 'chart13', size: 'col-md-6' }
    ];
    let getChartString = ej.base.compile('<div class="col-sm-12 ${size} mb-1 d-flex"> <div id=${id} class="card card_animation flex-fill"> <div class="card-header widget-head"> </div><div class="charts card-body chart-height"> </div> </div>  </div>');
    while (chartitems.firstChild) {
      chartitems.removeChild(chartitems.firstChild);
    }
    chartMetaData.forEach(data => {
      chartitems.appendChild(getChartString(data)[0]);
    });
  }

};
function chartExportCsv(csvObj) {
  //console.log("chartexport");

  //console.log(csvObj)
}

var TealeafChart = {
  chart1: function (data, location, rowMax, header, chartid, desc) {

    let rst = data[location],
      chartObj = {},
      list = [],
      metric = [];

    let gridColumn = [{
      field: 'col1',
      headerText: '裝置類別',
      textAlign: 'center',
      width: 180,
      format: 'N'
    }, {
      field: 'col2',
      headerText: '進站人數',
      textAlign: 'center',
      width: 180,
      format: 'N'
    }];
    for (var i = 0; i < rst.Data.length; i++) {
      chartObj = {};
      switch (rst.Data[i].Col1) {
        case 'Desktop':
          chartObj.col1 = '桌機';
          chartObj.col2 = rst.Data[i].Col2;
          break;
        case 'MobilePhone':
          chartObj.col1 = '手機';
          chartObj.col2 = rst.Data[i].Col2;
          break;
        case 'Tablet':
          chartObj.col1 = '平板';
          chartObj.col2 = rst.Data[i].Col2;
          break;
      }
      list.push(chartObj);

    }
    //為了對其底部分頁
    for (var i = 0; i < 2; i++) {
      chartObj = {};
      list.push(chartObj);
    }

    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: gridColumn,
      chartid: chartid
    };
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart1 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart2: function (data, location, rowMax, header, chartid, desc) {

    let rst = data[location],
      chartObj = {},
      list = [],
      metric = [];

    let gridColumn = [{
      field: 'col1',
      headerText: '來源網域',
      textAlign: 'center',
      width: 180,
      format: 'N'
    }, {
      field: 'col2',
      headerText: '進站人數',
      textAlign: 'center',
      width: 180,
      format: 'N'
    }];
    for (var i = 0; i < rst.Data.length; i++) {
      chartObj = {};
      chartObj.col1 = rst.Data[i].Col1;
      chartObj.col2 = rst.Data[i].Col2;
      list.push(chartObj);

    }
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: gridColumn,
      chartid: chartid
    };
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart2 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart3: function (data, location, rowMax, header, chartid, desc) {

    let rst = data[location],
      chartObj = {},
      list = [],
      metric = [];
    
    let gridColumn = [{
      field: 'col1',
      headerText: '國家',
      textAlign: 'left',
      width: 60,
      format: 'N'
    }, {
      field: 'col2',
      headerText: '城市',
      textAlign: 'left',
      width: 140,
      format: 'N'
    }, {
      field: 'col3',
      headerText: '進站人數',
      textAlign: 'center',
      width: 60,
      format: 'N'
    }];
    for (var i = 0; i < rst.Data.length; i++) {
      chartObj = {};
      chartObj.col1 = rst.Data[i].Col1;
      chartObj.col2 = rst.Data[i].Col2;
      chartObj.col3 = rst.Data[i].Col3;
      list.push(chartObj);

    }
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: gridColumn,
      chartid: chartid
    };
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart3 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart4: function (data, location, rowMax, header, chartid, desc) {
    
    let rst =  Object.values(convertIntObj(data[location].Data)),
      chartObj = {},
      dimension = [],
      metric = [];

    for (let i = 0; i < rst.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
     chartObj[rst[i].Col1] = rst[i].Col2;
    }
    
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[0] - b[0]);
    
    for (let i = 0; i < sorted.length ; i++) {
      let arr = sorted[i];

      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
   
    let color = ["#4A90E2"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: [],
      chartid: chartid
    };

    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#" + chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart5: function (data, location, rowMax, header, chartid, desc) {
    let rst = data[location];
    let dataAry=[0,0,0,0,0,0],metric=[];
    let colcorAry=[];
    
    for(let i=0;i<rst.Data.length;i++){
      colcorAry.push(randomGenerateColor(1,i));
      dataAry[0]+=Number(rst.Data[i].Col1);
      dataAry[1]+=Number(rst.Data[i].Col2);
      dataAry[2]+=Number(rst.Data[i].Col3);
      dataAry[3]+=Number(rst.Data[i].Col4);
      dataAry[4]+=Number(rst.Data[i].Col5);
      dataAry[5]+=Number(rst.Data[i].Col6);
      
    }
    for(let i=0;i<funnelLegendA.length;i++){
      metric.push({
        name:funnelLegendA[i],
        value:dataAry[i]
      });		    			
    }
    let setting = {
      cardHead: header,
      titleText: "",
      color: colcorAry,
      metric: metric,
      legend: funnelLegendA,
      chartid:chartid,
      toolbar:true
    };
    
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.funnelChart(setting);
    if (option && typeof option === "object") {
      $("#"+chartid).children(".card-header").text(setting.cardHead);
      chart.setOption(option, true);
      window['tooltip'+chartid] = new ej.popups.Tooltip({
        width: '180px',
        height: '40px',
        content: desc
      });
      window['tooltip'+chartid].appendTo("#"+chartid);
    }

  },
  chart6: function (data, location, rowMax, header, chartid, desc) {
    
    let rst =  Object.values(convertIntObj(data[location].Data)),
      chartObj = {},
      dimension = [],
      metric = [];

    for (let i = 0; i < rst.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
     chartObj[rst[i].Col1] = rst[i].Col2;
    }
    
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[0] - b[0]);
    
    for (let i = 0; i < sorted.length ; i++) {
      let arr = sorted[i];

      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
   
    let color = ["#4A90E2"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: [],
      chartid: chartid
    };

    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#" + chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart7: function (data, location, rowMax, header, chartid, desc) {
    let rst = data[location];
    let dataAry=[0,0,0,0,0,0],metric=[];
    let colcorAry=[];
    
    for(let i=0;i<rst.Data.length;i++){
      colcorAry.push(randomGenerateColor(1,i));
      dataAry[0]+=Number(rst.Data[i].Col1);
      dataAry[1]+=Number(rst.Data[i].Col2);
      dataAry[2]+=Number(rst.Data[i].Col3);
      dataAry[3]+=Number(rst.Data[i].Col4);
      dataAry[4]+=Number(rst.Data[i].Col5);
      dataAry[5]+=Number(rst.Data[i].Col6);
      
    }
    for(let i=0;i<funnelLegendB.length;i++){
      metric.push({
        name:funnelLegendA[i],
        value:dataAry[i]
      });		    			
    }
    let setting = {
      cardHead: header,
      titleText: "",
      color: colcorAry,
      metric: metric,
      legend: funnelLegendB,
      chartid:chartid,
      toolbar:true
    };
    
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.funnelChart(setting);
    if (option && typeof option === "object") {
      $("#"+chartid).children(".card-header").text(setting.cardHead);
      chart.setOption(option, true);
      window['tooltip'+chartid] = new ej.popups.Tooltip({
        width: '180px',
        height: '40px',
        content: desc
      });
      window['tooltip'+chartid].appendTo("#"+chartid);
    }

  },
  chart8: function (data, location, rowMax, header, chartid, desc) {
    
    let rst =  Object.values(convertIntObj(data[location].Data)),
      chartObj = {},
      dimension = [],
      metric = [];

    for (let i = 0; i < rst.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
     chartObj[rst[i].Col1] = rst[i].Col2;
    }
    
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[0] - b[0]);
    
    for (let i = 0; i < sorted.length ; i++) {
      let arr = sorted[i];

      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
   
    let color = ["#4A90E2"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: [],
      chartid: chartid
    };

    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#" + chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart9: function (data, location, rowMax, header, chartid, desc) {
    let rst = data[location];
    let dataAry=[0,0,0,0,0,0],metric=[];
    let colcorAry=[];
    
    for(let i=0;i<rst.Data.length;i++){
      colcorAry.push(randomGenerateColor(1,i));
      dataAry[0]+=Number(rst.Data[i].Col1);
      dataAry[1]+=Number(rst.Data[i].Col2);
      dataAry[2]+=Number(rst.Data[i].Col3);
      dataAry[3]+=Number(rst.Data[i].Col4);
      dataAry[4]+=Number(rst.Data[i].Col5);
      dataAry[5]+=Number(rst.Data[i].Col6);
      
    }
    for(let i=0;i<funnelLegendA.length;i++){
      metric.push({
        name:funnelLegendC[i],
        value:dataAry[i]
      });		    			
    }
    let setting = {
      cardHead: header,
      titleText: "",
      color: colcorAry,
      metric: metric,
      legend: funnelLegendC,
      chartid:chartid,
      toolbar:true
    };
    
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.funnelChart(setting);
    if (option && typeof option === "object") {
      $("#"+chartid).children(".card-header").text(setting.cardHead);
      chart.setOption(option, true);
      window['tooltip'+chartid] = new ej.popups.Tooltip({
        width: '180px',
        height: '40px',
        content: desc
      });
      window['tooltip'+chartid].appendTo("#"+chartid);
    }

  },
  chart10: function (data, location, rowMax, header, chartid, desc) {
    
    let rst =  Object.values(convertIntObj(data[location].Data)),
      chartObj = {},
      dimension = [],
      metric = [];

    for (let i = 0; i < rst.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
     chartObj[rst[i].Col1] = rst[i].Col2;
    }
    
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[0] - b[0]);
    
    for (let i = 0; i < sorted.length ; i++) {
      let arr = sorted[i];

      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
   
    let color = ["#4A90E2"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: [],
      chartid: chartid
    };

    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#" + chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart11: function (data, location, rowMax, header, chartid, desc) {
    let rst = data[location];
    let dataAry=[0,0,0,0,0,0],metric=[];
    let colcorAry=[];
    
    for(let i=0;i<rst.Data.length;i++){
      colcorAry.push(randomGenerateColor(1,i));
      dataAry[0]+=Number(rst.Data[i].Col1);
      dataAry[1]+=Number(rst.Data[i].Col2);
      dataAry[2]+=Number(rst.Data[i].Col3);
      dataAry[3]+=Number(rst.Data[i].Col4);
      dataAry[4]+=Number(rst.Data[i].Col5);
      dataAry[5]+=Number(rst.Data[i].Col6);
      
    }
    for(let i=0;i<funnelLegendA.length;i++){
      metric.push({
        name:funnelLegendD[i],
        value:dataAry[i]
      });		    			
    }
    let setting = {
      cardHead: header,
      titleText: "",
      color: colcorAry,
      metric: metric,
      legend: funnelLegendD,
      chartid:chartid,
      toolbar:true
    };
    
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.funnelChart(setting);
    if (option && typeof option === "object") {
      $("#"+chartid).children(".card-header").text(setting.cardHead);
      chart.setOption(option, true);
      window['tooltip'+chartid] = new ej.popups.Tooltip({
        width: '180px',
        height: '40px',
        content: desc
      });
      window['tooltip'+chartid].appendTo("#"+chartid);
    }

  },
  chart12: function (data, location, rowMax, header, chartid, desc) {
    
    let rst =  Object.values(convertIntObj(data[location].Data)),
      chartObj = {},
      dimension = [],
      metric = [];

    for (let i = 0; i < rst.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
     chartObj[rst[i].Col1] = rst[i].Col2;
    }
    
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[0] - b[0]);
    
    for (let i = 0; i < sorted.length ; i++) {
      let arr = sorted[i];

      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
   
    let color = ["#4A90E2"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: [],
      chartid: chartid
    };

    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#" + chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart13: function (data, location, rowMax, header, chartid, desc) {
    let rst = data[location];
    let dataAry=[0,0,0,0,0,0],metric=[];
    let colcorAry=[];
    
    for(let i=0;i<rst.Data.length;i++){
      colcorAry.push(randomGenerateColor(1,i));
      dataAry[0]+=Number(rst.Data[i].Col1);
      dataAry[1]+=Number(rst.Data[i].Col2);
      dataAry[2]+=Number(rst.Data[i].Col3);
      dataAry[3]+=Number(rst.Data[i].Col4);
      dataAry[4]+=Number(rst.Data[i].Col5);
      dataAry[5]+=Number(rst.Data[i].Col6);
      
    }
    for(let i=0;i<funnelLegendA.length;i++){
      metric.push({
        name:funnelLegendE[i],
        value:dataAry[i]
      });		    			
    }
    let setting = {
      cardHead: header,
      titleText: "",
      color: colcorAry,
      metric: metric,
      legend: funnelLegendE,
      chartid:chartid,
      toolbar:true
    };
    
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.funnelChart(setting);
    if (option && typeof option === "object") {
      $("#"+chartid).children(".card-header").text(setting.cardHead);
      chart.setOption(option, true);
      window['tooltip'+chartid] = new ej.popups.Tooltip({
        width: '180px',
        height: '40px',
        content: desc
      });
      window['tooltip'+chartid].appendTo("#"+chartid);
    }

  }
};
function defineApp(appName) {
  const app = siteDefine.find(element => element.name == appName);
  let para = {};
  try {
    sitePrefix = app.prefix;
    let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
    sessionStorage.setItem('appName', appName);
    sessionStorage.setItem('sitePrefix', sitePrefix);
    sessionStorage.setItem('memberKey', app.memberKey);
    sessionStorage.setItem('GoogleremoteUrl', app.GoogleremoteUrl);
    sessionStorage.setItem('localurl', app.localurl);
    sessionStorage.setItem('AuthremoteUrl', app.AuthremoteUrl);
    sessionStorage.setItem('TealeafremoteUrl', app.TealeafremoteUrl);
    sessionStorage.setItem('LabelremoteUrl', app.LabelremoteUrl);
    sessionStorage.setItem(defaultStartDate, dt);
    sessionStorage.setItem(defaultEndDate, dt);
    sessionStorage.setItem('daySpan', "1");

    var key = "tealeaf-" + dt + "-" + dt + "-" + "1";

  } catch (e) {
    console.log(e);
    sitePrefix = "";
  }

}
function convertIntObj(obj) {
  const res = {}
  for (const key in obj) {
    res[key] = {};
    for (const prop in obj[key]) {
      const parsed = parseInt(obj[key][prop], 10);
      res[key][prop] = isNaN(parsed) ? obj[key][prop] : parsed;
    }
  }
  return res;
}
function randomGenerateColor(selector,index){
  let colorArray1=["#280E3B","#632A7E","#792A7E","#A13E97","#CC76B5","#D3B7D8"];
  let colorArray2=["#003853","#194b64","#3d82ab","#45b299","#43ccaa","#91d4c2"];
  let rtnColorCode;
  //console.log(selector);
  //selector : fixed or random color array,sequence (1,2,3(random)): fix color array index 
  switch(selector){
    case 1:
      
      rtnColorCode= colorArray1[index];
      
      break;
    case 2:
      rtnColorCode= colorArray2[index];
      break;
    case 3:
      rtnColorCode= '#'+(Math.random()*0xFFFFFF<<0).toString(16);
      break;
  }
    return rtnColorCode;
}

