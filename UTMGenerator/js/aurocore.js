var formUrl = company[0].FormKey;
var redirecturl = company[0].redirecturl;
var prefixUrl = company[0].Shorturlprefix;
var fdlUrl=company[0].FDLKey;
//var replace = "/[&\/\\#,+()$~%.'":*?<>{}]";


var dialog, unshorturl, issendable = false;
var utmurl, url, startDate, endDate, description, campaign, source, medium, term, content, queryStringArray = [],
	combinedUrl, shortUrl;

$.ajaxSetup({
	beforeSend: function () {
		$("#ajaxspinner").css("display", "block");
	},
	complete: function () {
		$("#ajaxspinner").css("display", "none");
	},

});
$(document).ready(function () {
	new ClipboardJS('.copybtn');
	dialog = $('#dialog').dialog({
		uiLibrary: 'bootstrap4',
		iconsLibrary: 'fontawesome',
		autoOpen: false,
		resizable: true,
		width: "65%",
		modal: true
	});
	$('[data-toggle="tooltip"]').tooltip();
});
$('#sdatepicker').datepicker({
	format: "yyyy-mm-dd",
	uiLibrary: 'bootstrap4'
});
$('#edatepicker').datepicker({
	format: "yyyy-mm-dd",
	uiLibrary: 'bootstrap4'
});
$("#link1").click(function () {
	queryStringArray = [];
	url = $("#url").val();
	utmurl = $("#utmurl");
	startDate = $("#sdatepicker").val();
	endDate = $("#edatepicker").val();
	description = $("#description").val();
	campaign = $("#campaign").val().replace(/[&\/\\#,+()。，\-=$~%.!@^'":*?<>{}]/g, "");
	source = $("#source").val().replace(/[&\/\\#,+()。，\-=$~%.!@^'":*?<>{}]/g, "");
	medium = $("#medium").val().replace(/[&\/\\#,+()。，\-=$~%.!@^'":*?<>{}]/g, "");
	term = $("#term").val().replace(/[&\/\\#,+()。，\-=$~%.!@^'":*?<>{}]/g, "");
	content = $("#contents").val().replace(/[&\/\\#,+()。，\-=$~%.!@^'":*?<>{}]/g, "");

	if (campaign) {

		let campaignStr = "utm_campaign=" + campaign;
		queryStringArray.push(campaignStr);
	}
	if (source) {
		let sourceStr = "utm_source=" + source;
		queryStringArray.push(sourceStr);
	}
	if (medium) {
		mediumStr = "utm_medium=" + medium;
		queryStringArray.push(mediumStr);
	}
	if (term) {
		termStr = "utm_term=" + term;
		queryStringArray.push(termStr);
	}
	if (content) {
		contentStr = "utm_content=" + content;
		queryStringArray.push(contentStr);
	}
	if (campaign && source && medium && description && startDate && endDate && url) {

		combinedUrl = "";
		$("#utmurl").val = "";
		combinedUrl = url + "/?" + (queryStringArray.join("&"));
		console.log(combinedUrl);
		utmurl.val(combinedUrl);
		issendable = true;

	} else {
		queryStringArray = [];
		combinedUrl = "";
		issendable = false;
	}


});
$("#link2").click(function () {
	if (issendable) {

		unshorturl = $("#unshorturl");
		unshorturl.val(utmurl.val())
		GetshorturlbyFDL();

	}

});
$("#link3").click(function () {
	queryStringArray = [];
	combinedUrl = "";
	url = "";
	startDate = "";
	endDate = "";
	description = "";
	campaign = "";
	source = "";
	medium = "";
	term = "";
	content = "";
	issendable = false;
	document.location.href = redirecturl;
});
$('#btnSave').on('click', Save);
$('#btnCancel').on('click', function () {
	dialog.close();
});

function Save(data) {
	shorturl = $("#shorturl").val();
	if (shorturl) {
		var record = {

			'entry.1318400122': utmurl.val(),
			'entry.80647765': shorturl,
			'entry.1084804545': description,
			'entry.1771392138': startDate,
			'entry.1000325551': endDate,
			'entry.1916917179': source,
			'entry.15459253': medium,
			'entry.1732939039': term,
			'entry.1223899473': content,
			'entry.127574301': campaign,


		};
		console.log(record);
		jQuery.ajax({
			type: 'POST',
			url: formUrl,
			data: record,
			headers: {
				"cache-control": "no-cache"
			},

			contentType: 'application/json',
			dataType: 'jsonp',
			jsonp: "jsonpCallback",
			jsonpCallback: "success_jsonpCallback",
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("error");
				alert("資料已送出！");
				document.location.href = redirecturl;
			},
			success: function () {
				dialog.close();

			},
			complete: function () {
				queryStringArray = [];
				combinedUrl = "";
				url = "";
				startDate = "";
				endDate = "";
				description = "";
				campaign = "";
				source = "";
				medium = "";
				term = "";
				content = "";
				issendable = false;
				document.location.href = redirecturl;

			},

		});


	}

}

function Getshorturlbyreurl() {
	let urlObj = {
		"url": url,
		"utm_source": source,
		"utm_medium": medium,
		"utm_campaign": campaign,
		"utm_term": term,
		"utm_content": content
	}

	let settings = {
		"url": "https://api.reurl.cc/shorten",
		"method": "POST",
		"timeout": 0,
		"async": false,
		"headers": {
			"Content-Type": "application/x-www-form-urlencoded",
			"reurl-api-key": "4070ff49d794e335145f3b663c974755ecdab53496870ab1af3cb190330e5e66c4b8"
		},
		"data": JSON.stringify(urlObj),
	}


	console.log(settings.data);
	jQuery.ajax({
		type: 'POST',
		"url": "https://api.reurl.cc/shorten",
		"timeout": 0,
		"headers": {
			"Content-Type": "application/x-www-form-urlencoded",
			"reurl-api-key": "4070ff49d794e335145f3b663c974755ecdab53496870ab1af3cb190330e5e66c4b8"
		},
		"data": JSON.stringify(urlObj),
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			alert("縮址失敗");
			dialog.open('短網址');
		},
		success: function (response) {
			console.log(response);
			console.log(typeof (response));
			if (response.res === "success") {

				$('#shorturl').val(response.short_url);
			}
			dialog.open('短網址');

		}

	});

}

function GetshorturlbyFDL() {
	/* -- 參數式
	let dataObj = {
		"dynamicLinkInfo": {
			"domainUriPrefix": prefixUrl,
			"link": url,
			"analyticsInfo": {
				"googlePlayAnalytics": {
					"utmSource": source,
					"utmMedium": medium,
					"utmCampaign": campaign,
					"utmTerm": term,
					"utmContent": content,
					"gclid": ""
				}
			}
		}
	};*/
	let dynamicLink =prefixUrl+encodeURIComponent(utmurl.val());
	let dataObj={
		"longDynamicLink": dynamicLink
	};
	$.ajax({
		type: 'POST',
		"url": fdlUrl,
		"timeout": 0,
		"headers": {
			"Content-Type": "application/x-www-form-urlencoded"
		},
		//"data": JSON.stringify(dataObj),
		"data": dataObj,
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			alert("縮址失敗");
			console.log("textStatus:"+textStatus);
			console.log("errorThrown:"+errorThrown);
			dialog.open('短網址');
		},
		success: function (response) {
			console.log(response);
			//console.log(typeof (response));
			$('#shorturl').val(response.shortLink);
			
			dialog.open('短網址');

		}

	});
	

}