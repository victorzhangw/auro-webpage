var googlesheetUrl = "https://script.google.com/macros/s/AKfycbznTz-lpUUw4GhJA4jsljpL0tzDGEEKlfFBmSiKNnXbX1PXjOM/exec";
var labelDatas;
var hash = function (s) {
	/* Simple hash function. */
	var a = 1,
		c = 0,
		h, o;
	if (s) {
		a = 0;
		/*jshint plusplus:false bitwise:false*/
		for (h = s.length - 1; h >= 0; h--) {
			o = s.charCodeAt(h);
			a = (a << 6 & 268435455) + o + (o << 14);
			c = a & 266338304;
			a = c !== 0 ? a ^ c >> 21 : a;
		}
	}
	return String(a);
};

var grid, dialog;

function Edit(e) {
	$('#tag').val(e.data.tag);
	$('#Name').val(e.data.record.Name);
	$('#PlaceOfBirth').val(e.data.record.PlaceOfBirth);
	dialog.open('Edit Player');
}

function Save() {
	var record = {

		'entry.378427957': $('#tag').val(),
		'entry.1449016483': $('#taghash').val()
	};
	jQuery.ajax({
		type: 'POST',
		url: 'https://docs.google.com/forms/d/e/1FAIpQLSfpOqamfjwYthpZ6792VX73hfXoYv18_mSRBuST90FGQZEnCg/formResponse',
		data: record,
		headers: {
			"cache-control": "no-cache"
		},

		contentType: 'application/json',
		dataType: 'jsonp',
		jsonp: "jsonpCallback",
		jsonpCallback: "success_jsonpCallback",
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log("error");
			document.location.href = "./";
		},
		success: function () {
			dialog.close();
			grid.reload();
		},
		complete: function () {
			grid.reload();




		},

	});

}

function Delete(e) {
	if (confirm('Are you sure?')) {
		$.ajax({
				url: '/Players/Delete',
				data: {
					id: e.data.id
				},
				method: 'POST'
			})
			.done(function () {
				grid.reload();
			})
			.fail(function () {
				alert('Failed to delete.');
			});
	}
}

function insert_value() {

	$("#re").css("visibility", "hidden");
	document.getElementById("loader").style.visibility = "visible";
	$('#mySpinner').addClass('spinner');

	let tag = $("#tag").val();
	let taghash = $("#taghash").val();
	let url = googlesheetUrl + "?callback=ctrlq&tag=" + tag + "&taghash=" + taghash + "&action=insert";
	let request = jQuery.ajax({
		crossDomain: true,
		url: url,
		method: "GET",
		dataType: "jsonp"
	});

}
function update_value() {

	$("#re").css("visibility", "hidden");
	document.getElementById("loader").style.visibility = "visible";
	$('#mySpinner').addClass('spinner');

	let tag = $("#tag").val();
	let taghash = $("#taghash").val();
	let url = googlesheetUrl + "?callback=ctrlq&tag=" + tag + "&taghash=" + taghash + "&action=update";

	let request = jQuery.ajax({
		crossDomain: true,
		url: url,
		method: "GET",
		dataType: "jsonp"
	});


}
function delete_value() {

	$("#re").css("visibility", "hidden");
	document.getElementById("loader").style.visibility = "visible";
	$('#mySpinner').addClass('spinner');

	let tag = $("#tag").val();
	let taghash = $("#taghash").val();
	let url = googlesheetUrl + "?callback=ctrlq&tag=" + tag + "&taghash=" + taghash + "&action=delete";

	let request = jQuery.ajax({
		crossDomain: true,
		url: url,
		method: "GET",
		dataType: "jsonp"
	});

}
// print the returned data
function ctrlq(e) {


	$("#re").html(e.result);
	$("#re").css("visibility", "visible");
	read_value();

}
function read_value() {

	$("#re").css("visibility", "hidden");

	document.getElementById("loader").style.visibility = "visible";
	var url = googlesheetUrl + "?action=read";
	console.log(url);
	$.getJSON(url, function (json) {

		
		document.getElementById("loader").style.visibility = "hidden";
		$("#re").css("visibility", "visible");
		//console.log(json);
		dataStorage(json);
	});

}
function dataStorage(data)
{
	console.log(data);
  return data;
}
$(document).ready(function () {
	read_value();
	let labelary = dataStorage();

	//console.log(labelary);
	grid = $('#grid').grid({
		primaryKey: 'ID',
		dataSource: labelary,
		uiLibrary: 'bootstrap4',
		columns: [
			{
				field: 'ID',
				width: 48
			},
			{
				field: 'tag',
				title: '標籤',
				sortable: true
			},
			{
				field: 'taghash',
				title: '標籤代碼',
				sortable: true
			},

                ],
		pager: {
			limit: 10,
			sizes: [2, 5, 10, 20]
		}
	});
	dialog = $('#dialog').dialog({
		uiLibrary: 'bootstrap4',
		iconsLibrary: 'fontawesome',
		autoOpen: false,
		resizable: false,
		modal: true
	});
	$('#btnAdd').on('click', function () {
		let timeInMs = Date.now().toString();
		$('#ID').val('');
		$('#tag').val('');
		$('#taghash').val(hash(timeInMs));

		dialog.open('添加標籤');

	});
	$('#btnSave').on('click', Save);
	$('#btnCancel').on('click', function () {
		dialog.close();
	});
	$('#btnSearch').on('click', function () {
		grid.reload({
			tag: $('#tag').val(),
			taghash: $('#taghash').val()
		});
	});
	$('#btnClear').on('click', function () {
		$('#tag').val('');
		$('#taghash').val('');
		grid.reload({
			tag: '',
			taghash: ''
		});
	});

});