var formUrl="https://docs.google.com/forms/d/e/1FAIpQLSe0r7NQD310c98uKaa9oaFQ57-OmCDwi4ZdCC4zR_BKbC4PPw/formResponse";
var redirecturl="./tmnewa.html"
var dialog,unshorturl,issendable=false;
var utmurl,url,startDate,endDate,description,campaign,source,medium,term,content,queryStringArray=[],combinedUrl,shortUrl;
$(document).ready(function(){

	dialog = $('#dialog').dialog({
		uiLibrary: 'bootstrap4',
		iconsLibrary: 'fontawesome',
		autoOpen: false,
		resizable: true,
		width:"65%",
		modal: true
	}); 
	$('#sdatepicker').datepicker({
		format: "yyyy-mm-dd",
		uiLibrary: 'bootstrap4'
	 });
	$('#edatepicker').datepicker({
		format: "yyyy-mm-dd",
		uiLibrary: 'bootstrap4'
	 });
	$("#link1").click(function(){
		/*
		url=$("#url").val();
		utmurl=$("#utmurl");
		startDate=$("#sdatepicker").val();
		endDate=$("#edatepicker").val();
		description=$("#description").val();
		campaign=$("#campaign").val();
		source=$("#source").val();
		medium=$("#medium").val();
		term=$("#term").val();
		content=$("#contents").val();
		*/
		url="https://www.yahoo.com.tw";
		utmurl=$("#utmurl");
		startDate="2020-02-19";
		endDate="2020-02-22";
		description="檔期活動敘述";
		campaign="檔期";
		source="來源";
		medium="媒體";
		term="關鍵字";
		content="內文";
		console.log("url: "+url);
		console.log("edate: " +endDate);
		if(campaign){
			campaign="utm_campaign="+campaign;
			queryStringArray.push(campaign);
		}
		if(source){
			source="utm_source="+source;
			queryStringArray.push(source);
		}
		if(medium){
			medium="utm_medium="+medium;
			queryStringArray.push(medium);
		}
		if(term){
			term="utm_term="+term;
			queryStringArray.push(term);
		}
		if(content){
			content="utm_content="+content;
			queryStringArray.push(term);
		}
		if(campaign&&source&&medium&&description&&startDate&&endDate&&url){
			
			combinedUrl = url+"/?"+(queryStringArray.join("&"));
			console.log(combinedUrl);
			utmurl.val(combinedUrl);
			issendable=true;
			
		}else{
			queryStringArray=[];
			combinedUrl="";
			issendable=false;
		}
		
		
	});
	$("#link2").click(function(){
		if(issendable){
			new ClipboardJS('.copybtn');
			unshorturl=$("#unshorturl");
			unshorturl.val(utmurl.val())
			dialog.open('短網址');
		}
		
	});
	$('#btnSave').on('click', Save);
	$('#btnCancel').on('click', function () {
		dialog.close();
	});
})
function Save(data){
	shorturl=$("#shorturl").val();
	if(shorturl){
		var record = {

		'entry.1318400122':utmurl.val() ,
		'entry.80647765': shorturl,
		'entry.1084804545':description ,
		'entry.1771392138':startDate ,
		'entry.1000325551':endDate,
		'entry.1916917179':source ,
		'entry.15459253': medium,
		'entry.1732939039': term,
		'entry.1223899473': content,
		'entry.127574301': campaign,
	
		
	};
		console.log(record);
	jQuery.ajax({
		type: 'POST',
		url: formUrl,
		data: record,
		headers: {
			"cache-control": "no-cache"
		},

		contentType: 'application/json',
		dataType: 'jsonp',
		jsonp: "jsonpCallback",
		jsonpCallback: "success_jsonpCallback",
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log("error");
			alert("error");
			document.location.href = "./generatorV1.html";
		},
		success: function () {
			dialog.close();
			
		},
		complete: function () {
			document.location.href = "./generatorV1.html";




		},

	});

		
	}
	
}