﻿function getRewardList()
{
    $.getJSON("https://spreadsheets.google.com/feeds/list/1hKvkW0IoDszCnrXFGyBDRg3Cuxv2KIt6c8VDb6sNP4k/1/public/values?alt=json-in-script&callback=?", function (json)
    {
        var e = json.feed.entry,        
        totalPeoples = e.length;    // 總參加人數
        var dataArr = new Array();  // 所有參加人員列表
		console.log(e);
        for (i = 0; i < totalPeoples; i++)
        {
            // 取得值
            entry = e[i]; 

            invoice = entry.gsx$發票號碼.$t;
            name = entry.gsx$姓名.$t;
            phone = entry.gsx$電話.$t;
            mail = entry.gsx$電子郵件.$t;
            city = entry.gsx$縣市.$t;
            area = entry.gsx$區.$t;
            store = entry.gsx$門市.$t;
            cost = entry.gsx$消費金額.$t;
            time = entry.gsx$timestamp.$t;


            // 把值存入陣列
            dataArr[i] = new Array();
            dataArr[i][0] = invoice;
            dataArr[i][1] = name;
            dataArr[i][2] = phone
            dataArr[i][3] = mail;
            dataArr[i][4] = city;
            dataArr[i][5] = area;
            dataArr[i][6] = store;
            dataArr[i][7] = cost;
            dataArr[i][8] = time;
        }


        var arrReward = [
                         ['特獎', 'Iphone8(Product)(市價NT$25500元)  ', 1],
                         ['一獎', '拿坡里全家炸雞餐1年份(12張餐券) (市價NT$8400元)', 3],
						 ['二獎', '「可口可樂」懶人沙發 (市價NT$4500元)', 10],
                         ['三獎', '「可口可樂」藍牙耳機(市價NT$1799元)', 15],
                         ['四獎', '「GOOD YEAR」20吋登機箱 (市價NT$1450元)', 15],
                         ['五獎', '「可口可樂」保冷提袋', 500],
                         ['六獎', '「可口可樂」飲料掛袋(市價NT$199元)', 500],
                         ['七獎', '「可口可樂」捲線器 (市價NT$159元)', 500]
                        ];  // [ [獎項 , 獎品名稱 , 數量] , [獎項 , 獎品名稱 , 數量] ,  ........ ]


        var totalRrewardCount = 0; // 獎品總數量

        for (var i = 0; i <= arrReward.length - 1; i++)
        {
            totalRrewardCount += arrReward[i][2];
        }


        var arrRewardList = new Array(); // 獎項列表

        for (var j = 0; j <= arrReward.length - 1; j++)
        {
            for (var k = 0; k <= arrReward[j][2] - 1; k++)
            {
                arrRewardList.push([arrReward[j][0], arrReward[j][1]]);
            }
        }



        getRandomArray(0, totalPeoples, totalRrewardCount, dataArr, arrRewardList);

    });
}

//篩選多維陣列
function filterByPosition(array, number, position) {
   return array.filter(innerArray => innerArray[position - 1] >= number);
}

//隨機產生不重覆的列表
function getRandomArray(minNum, maxNum, totalRrewardCount, dataArr, arrRewardList)
{
    if (totalRrewardCount > maxNum) //如果獎品數量大於總參加人數, 則只抽出總參加人數數量的獎品就好
        totalRrewardCount = maxNum;

    var rdmArr = [totalRrewardCount];		//儲存產生的陣列    

    for (var i = 0; i < totalRrewardCount ; i++)
    {
        var rdm = 0;		//暫存的亂數
		/*console.log(dataArr);
		var filteredary= filterByPosition(dataArr,600,7);
		console.log(filteredary);*/
        do
        {
            var exist = false;			//此亂數是否已存在
           	//取得亂數
			if(i==0)
			{
				
			
				for(var j=0 ;j<dataArr.length;j++){
					rdm = getRandom(minNum, (maxNum -1));
					console.log("Current Cost:" + dataArr[rdm][7]);
					if (dataArr[rdm][7] >= 600)
					{
						console.log("Greater than 600 Cost:"+dataArr[rdm][7]);
						break;
					}
				}	
			}
			else
			{
				rdm = getRandom(minNum, (maxNum -1));
				if (rdmArr.indexOf(rdm) != -1) exist = true;
			}
			
          
        }
        while (exist);	//產生沒出現過的亂數時離開迴圈

        rdmArr[i] = rdm;
    }


    // 依所取得的亂數陣列取出資料陣列裡的資料 , 並組合成 Html 的 Table
    var tableHead = "<table style=\"width:100%; text-align:center; \">" +
                       "<tr class=\"border_bottom\">" +
                           "<th style=\"width:40px;\">序號</th>" +
                           "<th style=\"width:40px;\">獎項</th>" +
                           "<th style=\"width:200px;\">獎品名稱</th>" +
                           "<th style=\"width:120px;\">發票號碼</th>" +
                           "<th style=\"width:120px;\">姓名</th>" +
                           "<th style=\"width:120px;\">電話</th>" +
                           "<th style=\"width:150px;\">電子郵件</th>" +
                           "<th style=\"width:70px;\">縣市</th>" +
                           "<th style=\"width:60px;\">區</th>" +
                           "<th style=\"width:60px;\">門市</th>" +
                           "<th style=\"width:60px;\">消費金額</th>" +
                           "<th style=\"width:60px;\">登錄時間</th>" 
                       "</tr>";

    var tmpStr = '';
    var arrStr = '';
    var html = '';
    var rewardTitle = ''; // 獎項
    var rewardName = '';  // 獎品名稱

        
    for (var i = 0; i < totalRrewardCount ; i++)
    {
        html += "<tr class=\"border_bottom\">";

        arrStr = dataArr[rdmArr[i]];
       
        rewardTitle = arrRewardList[i][0];
        rewardName = arrRewardList[i][1];

        tmpStr = "<td>" + (i + 1) + "</td>" +
                 "<td>" + rewardTitle + "</td>" +
                 "<td>" + rewardName + "</td>" +
                 "<td>" + arrStr[0] + "</td>" +
                 "<td>" + arrStr[1] + "</td>" +
                 "<td>" + arrStr[2] + "</td>" +
                 "<td>" + arrStr[3] + "</td>" +
                 "<td>" + arrStr[4] + "</td>" +
                 "<td>" + arrStr[5] + "</td>" +
                 "<td>" + arrStr[6] + "</td>" +
                 "<td>" + arrStr[7] + "</td>" +
                 "<td>" + arrStr[8] + "</td>";

        html += tmpStr + "</tr>";
    }


   

    html = tableHead + html + "</table>";
    
    $("#test").html(html);
}

//取得 minNum(最小值) ~ maxNum(最大值) 之間的亂數
function getRandom(minNum, maxNum)
{	
    return Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum;
}