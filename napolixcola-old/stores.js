﻿var stateObject = {
  "基隆市": {
    "仁愛區": [ "基隆" ],
    "信義區": [ "深溪" ]
  },
  "台北市": {
    "士林區": [ "士林" ],
    "大同區": [ "民生" ],
    "大安區": [ "和平" ],
    "中山區": [ "龍江" ],
    "中正區": [ 
		"南昌",
		"和平" 
	],
    "內湖區": [
	  "內湖",
	  "東湖"
	],
    "文山區": [ "興隆" ],
    "北投區": [
      "北投",
      "天母"
    ],
    "松山區": [ "八德" ],
    "信義區": [
      "莊敬",
      "永吉"
    ],
    "南港區": [ "南港" ],
    "萬華區": [ "西園" ]
  },
  "新北市": {
    "三重區": [ "三重" ],
    "三峽區": [ "三峽" ],
    "土城區": [ "土城" ],
    "中和區": [
      "中和",
      "景新"
    ],
    "永和區": [ "永和" ],
    "汐止區": [ "汐止" ],
    "板橋區": [
      "三民",
      "板橋忠孝"
    ],
    "泰山區": [ "泰山" ],
    "淡水區": [
      "淡水",
      "竹圍"
    ],
    "新店區": [
      "新店",
      "安康"
    ],
    "新莊區": [
      "新泰",
      "民安"
    ],
    "樹林區": [ "樹林" ],
    "蘆洲區": [ "蘆洲" ],
    "鶯歌區": [ "鶯桃" ]
  },
  "宜蘭縣": {
    "宜蘭市": [ "宜蘭" ],
    "羅東鎮": [ "羅東" ]
  },
  "桃園市": {
    "八德區": [ "大湳" ],
    "中壢區": [
      "中原",
      "中壢",
      "內壢",
	  "新屋"
    ],
    "桃園區": [
      "桃園",
      "桃園二",
	  "國際"
    ],
    "楊梅區": [ "楊梅" ],
	"大園區": [ "大園" ],
    "龍潭區": [ "龍潭" ],
    "龜山區": [ "林口" ],
    "蘆竹區": [ "南崁" ]
  },
  "新竹市": {
    "北區": [ "北大" ],
    "東區": [ "光復" ]
  },
  "新竹縣": {
    "竹北市": [ "竹北" ],
    "新豐鄉": [ "新豐" ]
  },
  "花蓮縣": {
    "花蓮市": [ "花蓮" ]
  },
  "苗栗縣": {
    "竹南鎮": [ "竹南" ]
  },
  "台中市": {
    "大里區": [ "大里" ],
    "北屯區": [ "北屯" ],
    "北區": [ "學士" ],
    "西屯": [ "永福" ],
    "西屯區": [ "逢甲" ],
    "西區": [ "美村" ],
    "沙鹿區": [ "沙鹿" ],
    "東區": [ "精武" ],
    "南屯區": [ "黎明" ],
    "南區": [ "復興" ],
    "豐原區": [ "豐原" ],
    "大雅區": [ "大雅" ]
  },
  "彰化縣": {
    "員林鎮": [ "員林" ],
    "彰化市": [ "彰化" ],
    "福興鄉": [ "鹿港" ]
  },
  "南投縣": {
    "南投市": [ "南投" ],
    "草屯鎮": [ "草屯" ]
  },
  "雲林縣": {
    "斗六市": [ "斗六" ],
    "虎尾鎮": [ "虎尾" ]
  },
  "嘉義市": {
    "西區": [ "民族" ]
  },
  "嘉義縣": {
    "朴子市": [ "朴子" ],
	"民雄鄉": [ "民雄" ]
  },
  "台南市": {
    "仁德區": [ "仁德" ],
    "北區": [ "成功" ],
    "永康區": [
      "永康",
      "永康2"
    ],
	"安南區": [ "安南" ],
    "東區": [ "東寧" ],
    "新營區": [ "新營" ]
  },
  "高雄市": {
    "三民區": [
      "澄清",
      "鼎山"
    ],
    "小港區": [ "小港" ],
	"仁武區": [ "仁雄" ],
    "左營區": [ "左營" ],
    "岡山區": [ "岡山" ],
	"前金區": [ "中華" ],
    "苓雅區": [ "三多" ],
    "楠梓區": [
        "楠梓",
        "後昌"
    ],
    "鳳山區": [
      "鳳山",
      "五甲"
    ]
  },
  "屏東縣": {
    "屏東市": [ "屏東" ]
  }
}
window.onload = function () {
    var stateSel = document.getElementById("entry.1329025438"),
        countySel = document.getElementById("entry.874662454"),
        citySel = document.getElementById("entry.182018262");
    for (var state in stateObject) {
        stateSel.options[stateSel.options.length] = new Option(state, state);
    }
    stateSel.onchange = function () {
        countySel.length = 1; // remove all options bar first
        citySel.length = 1; // remove all options bar first
        if (this.selectedIndex < 1) return; // done   
        for (var county in stateObject[this.value]) {
            countySel.options[countySel.options.length] = new Option(county, county);
        }
    }
    stateSel.onchange(); // reset in case page is reloaded
    countySel.onchange = function () {
        citySel.length = 1; // remove all options bar first
        if (this.selectedIndex < 1) return; // done   
        var cities = stateObject[stateSel.value][this.value];
        for (var i = 0; i < cities.length; i++) {
            citySel.options[citySel.options.length] = new Option(cities[i], cities[i]);
        }
    }
}