﻿var app = angular.module('TONKATSU_Proj', ['angularValidator']);

app.controller('storeCntrl', function ($scope, $http)
{
	$scope.countries = [];
	$http.get('store.json').success(function (data)
	{
		$scope.countries = data;
	});

});


app.filter('filter1', function ()
{
	return function (fit1)
	{
		return fit1.substring(1);
	};
});

app.controller('validCntrl', function ($scope, $http)
{
	var store;
	$scope.changeStore = function (item)
	{
		$scope.store = item;
		//  console.log($scope.store);
	};

	$scope.submitMyForm = function ()
	{
		$scope.dt = Date.now();
		var dataObj = {
			invoice: $scope.form.invoice,
			amount: $scope.form.amount,
			cname: $scope.form.name,
			phone: $scope.form.cellphone,
			email: $scope.form.emailAddress,
			ename: $scope.form.ename,
			store: $scope.store,
			log: $scope.dt
		};
		console.log(dataObj)
		var res = $http.post('http://napoli1.azurewebsites.net/api/InvoiceData/', dataObj);
		res.success(function (data, status, headers, config)
		{
			$scope.message = data;

			$scope.form = null;
			alert("已登錄");
			window.location.reload();
		});
		res.error(function(data, status, headers, config) {
			alert("錯誤訊息: " +JSON.stringify({data: data
			}));
			});
	};

	});




