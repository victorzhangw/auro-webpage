var refIndex1=39; // 跳出率參考指標
ej.grids.Grid.Inject(ej.grids.Page, ej.grids.Sort, ej.grids.Filter,ej.grids.Resize);
var grid1 = new ej.grids.Grid({
    dataSource: customerData,
    Offline: true, 
    columns: [
                { field: 'memberid', headerText: '會員編號', textAlign: 'center', width: 200, type: 'string' },
                { field: 'membername', width: 80, headerText: '姓名', type: 'string' },
               
                { field: '', headerText: '性別', textAlign: 'left', width: 20, format: 'string' },
                { field: 'memberemail', headerText: '電子郵件', textAlign: 'left', width: 200, format: 'string' },
                { field: 'shippingaddress', headerText: '遞送地址', textAlign: 'left', width: 210, format: 'string' },
                { field: 'contactaddress', headerText: '通訊地址', textAlign: 'left', width: 210, format: 'string' },
                { field: 'label', headerText: '標籤', textAlign: 'left', width: 160, format: 'string',valueAccessor: tagsFormatter,disableHtmlEncode:false},
               
                
    ],
    rowHeight: 20,
    //enableVirtualization: true,
    //enableColumnVirtualization: true,
    height: 750,
    allowPaging: true,
    pageSettings: { pageSize: 30 },
    allowSorting: true,
    allowTextWrap:true,
    textWrapSettings:{wrapMode:'Content'},
    //queryCellInfo: customiseCell
});
grid1.appendTo('#grid1');
grid1.dataBound=function(){
    grid1.autoFitColumns(['memberid','membername']);
    //let pkColumn = grid1.column[0];
    //pkColumn.isPrimaryKey='true';
};
function tagsFormatter(field, data, column){
    var tmp_Str = data.label;
    var tmp_Str2='';
    var rtn_Str='';
    var arr = tmp_Str.split(":");  
    for(var i=0;i<arr.length;i++){
        tmp_Str2='<span class="tagStyles tag-info  m-r-5 m-t-5">'+arr[i]+'</span>';
        
        rtn_Str=tmp_Str2+rtn_Str;
    }
    
    return rtn_Str;
}
function customiseCell(args){
    //console.log(args.cell);
    if(args.column.field==='duration'){
        var tmp_Duration =args.data.duration+'%';
        args.cell.querySelector("#bounce").textContent=tmp_Duration;
        if(parseInt(args.data.duration)<=refIndex1){
            args.cell.querySelector("#bounce").classList.add("bouncestatus");
        }
        
    }
}
$("#secondSearch").on("click",function(){  
    var tagArr = getCheckedValue();
    
    if (tagArr.length>0){
        var predicate = new ej.data.Predicate('tags', 'contains', tagArr[0]);
        if(tagArr.length>1){
            for(var i=1;i<tagArr.length;i++){
                predicate = predicate.or('tags', 'contains',  tagArr[i]);
            }
        }
        var result = new ej.data.DataManager(customerData).executeLocal(new ej.data.Query().where(predicate));
        grid1.dataSource=result;
    }
});
function getCheckedValue(){
    var cbxTags = [];
    $('input:checkbox:checked[name="tag_active_col[]"]').each(function(i) {
         cbxTags[i] = this.value; 
        
    });
    return cbxTags;
}