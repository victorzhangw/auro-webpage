/*
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/


export default {
    toolbox: {
        brush: {
            title: {
                rect: '矩形选择',
                polygon: '圈選',
                lineX: '橫向選擇',
                lineY: '縱向選擇',
                keep: '保持選擇',
                clear: '清除選擇'
            }
        },
        dataView: {
            title: '資料圖表',
            lang: ['資料圖表', '關閉', '更新']
        },
        dataZoom: {
            title: {
                zoom: '区域缩放',
                back: '区域缩放还原'
            }
        },
        magicType: {
            title: {
                line: '切换为折线圖',
                bar: '切换为柱状圖',
                stack: '切换为堆叠',
                tiled: '切换为平铺'
            }
        },
        restore: {
            title: '还原'
        },
        saveAsImage: {
            title: '保存为圖片',
            lang: ['右键另存为圖片']
        }
    },
    series: {
        typeNames: {
            pie: '圓餅圖',
            bar: '柱状圖',
            line: '折线圖',
            scatter: '散点圖',
            effectScatter: '涟漪散点圖',
            radar: '雷达圖',
            tree: '树圖',
            treemap: '矩形树圖',
            boxplot: '箱型圖',
            candlestick: 'K线圖',
            k: 'K线圖',
            heatmap: '热力圖',
            map: '地圖',
            parallel: '平行坐标圖',
            lines: '线圖',
            graph: '关系圖',
            sankey: '桑基圖',
            funnel: '漏斗圖',
            gauge: '仪表盘圖',
            pictorialBar: '象形柱圖',
            themeRiver: '主题河流圖',
            sunburst: '旭日圖'
        }
    },
    aria: {
        general: {
            withTitle: '这是一个关于“{title}”的圖表。',
            withoutTitle: '这是一个圖表，'
        },
        series: {
            single: {
                prefix: '',
                withName: '圖表类型是{seriesType}，表示{seriesName}。',
                withoutName: '圖表类型是{seriesType}。'
            },
            multiple: {
                prefix: '它由{seriesCount}个圖表系列组成。',
                withName: '第{seriesId}个系列是一个表示{seriesName}的{seriesType}，',
                withoutName: '第{seriesId}个系列是一个{seriesType}，',
                separator: {
                    middle: '；',
                    end: '。'
                }
            }
        },
        data: {
            allData: '其数据是——',
            partialData: '其中，前{displayCnt}项是——',
            withName: '{name}的数据是{value}',
            withoutName: '{value}',
            separator: {
                middle: '，',
                end: ''
            }
        }
    }
};
