ej.base.enableRipple(true);

/**
 * Sample
 */

    var dashboard = new ej.layouts.DashboardLayout({
            columns: 12,
            cellSpacing: [3, 3],
            cellAspectRatio: 100 / 75,
            created:appendElement,
            panels: [
                {
                    'sizeX': 3, 'sizeY': 1, 'row': 0, 'col': 0,
                    content: '#card1'
                },
                {
                    'sizeX': 3, 'sizeY': 1, 'row': 0, 'col': 3,
                    content: '#card2'
                },
                {
                    'sizeX': 3, 'sizeY': 1, 'row': 0, 'col': 6,
                    content: '#card3'
                },
                {
                    'sizeX': 3, 'sizeY': 1, 'row': 0, 'col': 9,
                    content: '#card4'
                },
                {
                    'sizeX': 3, 'sizeY': 1, 'row': 1, 'col': 0,
                    content: '#card5'
                },
                {
                    'sizeX': 3, 'sizeY': 1, 'row': 1, 'col': 3,
                    content: '#card6'
                },
                {
                    'sizeX': 3, 'sizeY': 1, 'row': 1, 'col': 6,
                    content: '#card7'
                },
                {
                    'sizeX': 3, 'sizeY': 1, 'row': 1, 'col': 9,
                    content: '#card8'
                },
                {
                    'sizeX': 3, 'sizeY': 1, 'row': 2, 'col': 0,
                    content: '#card9'
                },
                {
                    'sizeX': 3, 'sizeY': 1, 'row': 2, 'col': 3,
                    content: '#card10'
                },
                {
                    'sizeX': 3, 'sizeY': 1, 'row': 2, 'col': 6,
                    content: '#card11'
                },
                {
                    'sizeX': 3, 'sizeY': 1, 'row': 2, 'col': 9,
                    content: '#card12'
                },
                {
                    'sizeX': 6, 'sizeY': 4, 'row': 3, 'col': 0,
                    header: '<div>最多到達路徑熱點圖</div>', content: '#heatmap'
                },
                {
                    'sizeX': 6, 'sizeY': 4, 'row': 3, 'col': 6,
                    header: '<div>最多到達路徑重點停留區塊</div>', content: '#attentionmap'
                },
                {
                    'sizeX': 4, 'sizeY': 4, 'row': 10, 'col': 0,
                    header: '<div>商品搜尋關鍵字排行榜(</div>', content: '<div id="chart1" class="chart-responsive"></div>'
                    
                },
                {
                    'sizeX': 4, 'sizeY': 4, 'row': 10, 'col': 4,
                    header: '<div>註冊會員</div>', content: '<div id="chart2" class="chart-responsive"></div>'
                },
                {
                    'sizeX': 4, 'sizeY': 4, 'row': 10, 'col': 8,
                    header: '<div>購物流程綜覽 </div>', content: '<div id="chart3" class="chart-responsive"></div>'
                },
                {
                    'sizeX': 4, 'sizeY': 4, 'row': 14, 'col': 0,
                    header: '<div>電話格式錯誤分析（地區）</div>', content: '<div id="chart4" class="chart-responsive"></div>'
                    
                },
                {
                    'sizeX': 8, 'sizeY': 4, 'row': 14, 'col': 4,
                    header: '<div>工作階段分析</div>', content: '<div id="chart5" class="chart-responsive"></div>'
                },
                
               
            ]
    });
    
    dashboard.appendTo('#editLayout');
    function appendElement() {
        for(let i=0;i<mobileWidgetData.length;i++){
            let j=i+1;        
            $("#w"+j).children(".text").text(mobileWidgetData[i].name);
            $("#w"+j).children(".number").text(mobileWidgetData[i].value);

        }
        for(let i=0;i<desktopWidgetData.length;i++){
            let j=i+5;        
            $("#w"+j).children(".text").text(desktopWidgetData[i].name);
            $("#w"+j).children(".number").text(desktopWidgetData[i].value);
        }
        for(let i=0;i<tabletWidgetData.length;i++){
            let j=i+9;        
            $("#w"+j).children(".text").text(tabletWidgetData[i].name);
            $("#w"+j).children(".number").text(tabletWidgetData[i].value);
        }
       
    }
    var sidebarInstance = new ej.navigations.Sidebar({
        type: 'Over',
        dockSize: '65px',
        enableDock: true,
        target: '#target',
        closeOnDocumentClick: true
    });
    sidebarInstance.appendTo('#dockSidebar');

    var atcObj = new ej.dropdowns.AutoComplete({
        placeholder: 'Search Here',
        width: '215px'
    });
    atcObj.appendTo('#search');

   

