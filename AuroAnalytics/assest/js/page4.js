ej.base.enableRipple(true);

/**
 * Sample
 */ 

    var dashboard = new ej.layouts.DashboardLayout({
            columns: 12,
            cellSpacing: [3, 3],
            cellAspectRatio: 100 / 85,
            created:appendElement,
            allowDragging:false,
            allowResizing: false,
          
            panels: [
                
                {
                    'sizeX': 2, 'sizeY': 1, 'row': 0, 'col': 0,
                    content: '#card1'
                },
                {
                    'sizeX': 2, 'sizeY': 1, 'row': 1, 'col': 0,
                    content: '#card2'
                },
                {
                    'sizeX': 8, 'sizeY': 5, 'row': 0, 'col': 3,
                    header: '<div>每日城市 / 店鋪營收</div>',  content:'<div id="chart1" class="chart-responsive"><div id="chart1a" style="width:50%;height:100%;float:left;"></div><div id="chart1b" style="width:50%;height:100%;z-index:99;float:left;" ></div></div>'
                },
                {
                    'sizeX': 2, 'sizeY': 1, 'row': 0, 'col': 10,
                    content: '#card3'
                },
                {
                    'sizeX': 2, 'sizeY': 1, 'row': 1, 'col': 10,
                    content: '#card4'
                },
                {
                    'sizeX': 2, 'sizeY': 3, 'row': 2, 'col': 0,
                    header: '<div>四周會員/新訪客收益</div>',  content:'<div id="chart2" class="chart-responsive"></div>'
                },
               
                {
                    'sizeX': 2, 'sizeY': 3, 'row': 5, 'col': 10,
                    header: '<div>四周會員/新訪客購買量</div>',  content:'<div id="chart4" class="chart-responsive"></div>'
                },
                {
                    'sizeX': 3, 'sizeY': 3, 'row': 9, 'col': 0,
                    header: '<div>訪客/跳出率四象限</div>',  content:'<div id="chart5" class="chart-responsive"></div>'
                },
                {
                    'sizeX': 6, 'sizeY': 3, 'row': 9, 'col':3,
                    header: '<div>廣告來源/活動分析</div>',  content:'<div id="chart6" class="chart-responsive"></div>'
                },
               
                {
                    'sizeX': 3, 'sizeY': 3, 'row': 9, 'col':10,
                    header: '<div>來源媒體/營收四象限</div>',  content:'<div id="chart7" class="chart-responsive"></div>'
                },
                
               
            ]
    });
    
    dashboard.appendTo('#editLayout');
    function appendElement() {
        $("#w1").children(".text").text(widgetData[0].transaction.name);
        $("#w1").children(".number").text(widgetData[0].transaction.value);
        $("#w2").children(".text").text(widgetData[0].conversionRate.name);
        $("#w2").children(".number").text(widgetData[0].conversionRate.value);
        $("#w3").children(".text").text(widgetData[0].revenue.name);
        $("#w3").children(".number").text(widgetData[0].revenue.value);
        $("#w4").children(".text").text(widgetData[0].avgOrderValue.name);
        $("#w4").children(".number").text(widgetData[0].avgOrderValue.value);
        
       
    }
    var sidebarInstance = new ej.navigations.Sidebar({
        type: 'Over',
        dockSize: '65px',
        enableDock: true,
        target: '#target',
        closeOnDocumentClick: true
    });
    sidebarInstance.appendTo('#dockSidebar');

    var atcObj = new ej.dropdowns.AutoComplete({
        placeholder: 'Search Here',
        width: '215px'
    });
    atcObj.appendTo('#search');

   

