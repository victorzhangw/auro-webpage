// Chart1 
var chart1a = echarts.init(document.getElementById('chart1a'));
var chart1b = echarts.init(document.getElementById('chart1b'));
var chart2 = echarts.init(document.getElementById('chart2'));
var chart4 = echarts.init(document.getElementById('chart4'));
var chart5 = echarts.init(document.getElementById('chart5'));
var chart6 = echarts.init(document.getElementById('chart6'));
var chart7 = echarts.init(document.getElementById('chart7'));

//chart1a.showLoading();
(function () {
    var chart1a_width = $("#chart1a").width();
    // var chart1_height = $("#chart1a").height();
    calcAmt("TAIWAN");

    $.get('./assest/js/geo/taiwan3.json', function (twJson) {
        //chart1a.hideLoading();

        echarts.registerMap('TAIWAN', twJson, {});
        mapoption = {
            /*
            title: {
                //text: '各城市/店鋪銷售量',
                //subtext: 'Ecommerce',
               // sublink: '',
                left: 'right'
            },*/
            tooltip: {
                trigger: 'item',
                showDelay: 0,
                transitionDuration: 0,
                formatter: function (params) {
                    var value = (params.value + '').split('.');
                    value = value[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,');
                    return params.seriesName + '<br/>' + params.name + ': ' + value;
                }
            },
            visualMap: {

                min: 0,
                max: 29000,
                itemWidth: 14,
                inRange: {
                    color: ['#50E372', '#BD10E0', '#FB7507', '#4A90E2']
                },
                top: 'top',
                left: 'left',
                text: ['High', 'Low'], // 文本，默认为数值文本
                calculable: true
            },
            toolbox: {
                show: false,
                //orient: 'vertical',
                left: 'left',
                top: 'top',
                feature: {
                    dataView: {
                        readOnly: false
                    },
                    restore: {},
                    saveAsImage: {}
                }
            },

            series: [{
                name: 'sales',
                type: 'map3D',
                roam: false,
                map: 'TAIWAN',
                aspectScale: 0.8,
                bottom: 40,
                left: 0,

                itemStyle: {
                    emphasis: {
                        label: {
                            show: true
                        }
                    }
                },
                light: {
                    main: {
                        intensity: 1,
                        shadow: true,
                        alpha: 150,
                        beta: 70
                    }
                },
                ambient: {
                    intensity: 0
                },
                postEffect: { //為畫面新增高光，景深，環境光遮蔽（SSAO），調色等效果
                    enable: true, //是否開啟
                    SSAO: { //環境光遮蔽
                        radius: 1, //環境光遮蔽的取樣半徑。半徑越大效果越自然
                        intensity: 1, //環境光遮蔽的強度
                        enable: true
                    }
                },
                temporalSuperSampling: { //分幀超取樣。在開啟 postEffect 後，WebGL 預設的 MSAA 會無法使用,分幀超取樣用來解決鋸齒的問題
                    enable: true
                },
                viewControl: { //用於滑鼠的旋轉，縮放等視角控制
                    distance: 208, //預設視角距離主體的距離
                    zoomSensitivity: 0,
                    rotateMouseButton: 'right', //旋轉操作使用的滑鼠按鍵
                    alpha: 31 // 讓canvas在x軸有一定的傾斜角度
                },
                data: salesbyCity
            }]
        };

        setTimeout(function () {
            chart1a.setOption(mapoption);
        }, 300);
    });


})();

chart1a.on('click', function (params) {
    var barColor = ['#FFA483', '#F97F53', '#F45922'];
    var barWidth = '20%';
    chart1b.clear();
    calcAmt(params.data.name);
    amtStoreoption = {
        xAxis: {
            type: 'category',
            data: store_dim_data,
            show: true,
            axisTick: {
                show: false
            },
            axisLabel: {
                fontSize: 11,
                color: '#333',
                margin: 4,
                interval: 0,
                formatter: function (val) {
                    return val.split("").join("\n");
                }
            },
        },
        yAxis: {

            type: 'value'
        },
        tooltip: {
            trigger: 'axis'
        },
        series: [{
            data: store_metrics_data[0],
            barMaxWidth: '25%',
            itemStyle: {
                normal: {
                    color: function (params) {
                        var num = barColor.length;
                        return barColor[params.dataIndex % num]
                    }
                }
            },
            markPoint: {
                data: [{
                        type: 'max',
                        name: '最大值'
                    },
                    {
                        type: 'min',
                        name: '最小值'
                    }
                ]
            },
            markLine: {
                data: [{
                    type: 'average',
                    name: '平均值'
                }]
            },
            type: 'bar'
        }]
    };


    chart1b.setOption(amtStoreoption, true);
});


(function () {
    var giftImageUrl = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iNDBweCIgaGVpZ2h0PSI0N3B4IiB2aWV3Qm94PSIwIDAgNDAgNDciIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8IS0tIEdlbmVyYXRvcjogU2tldGNoIDUyLjUgKDY3NDY5KSAtIGh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaCAtLT4KICAgIDx0aXRsZT5Hcm91cCAxNCBDb3B5IDM8L3RpdGxlPgogICAgPGRlc2M+Q3JlYXRlZCB3aXRoIFNrZXRjaC48L2Rlc2M+CiAgICA8ZGVmcz4KICAgICAgICA8cG9seWdvbiBpZD0icGF0aC0xIiBwb2ludHM9IjAgNDcgNDAgNDcgNDAgMCAwIDAiPjwvcG9seWdvbj4KICAgIDwvZGVmcz4KICAgIDxnIGlkPSJQYWdlLTEiIHN0cm9rZT0ibm9uZSIgc3Ryb2tlLXdpZHRoPSIxIiBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPgogICAgICAgIDxnIGlkPSLplKbpsqTnuqIt5a6M56i/IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtOTMuMDAwMDAwLCAtNDM0Mi4wMDAwMDApIj4KICAgICAgICAgICAgPGcgaWQ9Ikdyb3VwLTE0LUNvcHktMyIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoOTMuMDAwMDAwLCA0MzQyLjAwMDAwMCkiPgogICAgICAgICAgICAgICAgPHBhdGggZD0iTTMyLDIxLjk0MjQ2OTIgQzMyLDE1LjM0Njg0OTIgMjYuNjI3MzkzMiwxMCAyMC4wMDExNzgyLDEwIEMxMy4zNzI2MDY4LDEwIDgsMTUuMzQ2ODQ5MiA4LDIxLjk0MjQ2OTIgQzgsMjUuODI3MTQyNyA5Ljg3MjE2NDk1LDI5LjI2NzQxODEgMTIuNzU3NTg0NywzMS40NDgzNjk4IEMxMy44MTU2MTEyLDMyLjc1MzQyMzEgMTUuMDIwOTEzMSwzNC43MzM4Njc5IDE1LjAyMDkxMzEsMzYuOTg2MzQ1NCBMMTUuMDIwOTEzMSw0MSBMMjQuOTc5MDg2OSw0MSBMMjQuOTc5MDg2OSwzNi45ODYzNDU0IEMyNC45NzkwODY5LDM0LjczMzg2NzkgMjYuMTg0Mzg4OCwzMi43NTM0MjMxIDI3LjI0MjQxNTMsMzEuNDQ4MzY5OCBDMzAuMTI3ODM1MSwyOS4yNjc0MTgxIDMyLDI1LjgyNzE0MjcgMzIsMjEuOTQyNDY5MiIgaWQ9IkZpbGwtMSIgZmlsbD0iI0ZGRkZGRiI+PC9wYXRoPgogICAgICAgICAgICAgICAgPHBhdGggZD0iTTE2LjA4MDEyMDIsMzkuOTIwNTA3MyBMMjQuOTE5ODc5OCwzOS45MjA1MDczIEwyNC45MTk4Nzk4LDM2LjQ1NjMwNjQgQzI0LjkxOTg3OTgsMzQuMDM1MzQyMyAyNi4yMTEzODQsMzEuOTMyNDk1MiAyNy4yOTUwNzM1LDMwLjU5MzM2MjggTDI3LjM5MDE3NTIsMzAuNTAyMTM4MSBDMzAuMjY2NzA3NCwyOC4zMjc5NDg1IDMxLjkxNjMxMDUsMjUuMDI4NjUzOSAzMS45MTYzMTA1LDIxLjQ1MjE3NjUgQzMxLjkxNjMxMDUsMTUuMTgyMjMwMiAyNi43OTQ5MDkxLDEwLjA4MDY2MjMgMjAuNTAwNTg3LDEwLjA4MDY2MjMgQzE0LjIwNTA5MDksMTAuMDgwNjYyMyA5LjA4MzY4OTQ4LDE1LjE4MjIzMDIgOS4wODM2ODk0OCwyMS40NTIxNzY1IEM5LjA4MzY4OTQ4LDI1LjAyNzQ4NDQgMTAuNzM0NDY2NywyOC4zMjc5NDg1IDEzLjYwOTgyNDgsMzAuNTAyMTM4MSBMMTMuNzAzNzUyNCwzMC41OTMzNjI4IEMxNC43ODg2MTYsMzEuOTMxMzI1NiAxNi4wODAxMjAyLDM0LjAzNDE3MjcgMTYuMDgwMTIwMiwzNi40NTYzMDY0IEwxNi4wODAxMjAyLDM5LjkyMDUwNzMgWiBNMjYuMDAzNTY5Miw0MSBMMTQuOTk2NDMwOCw0MSBMMTQuOTk2NDMwOCwzNi40NTYzMDY0IEMxNC45OTY0MzA4LDM0LjM3OTE4OTQgMTMuODY2OTUxNiwzMi41MjY2MjU1IDEyLjkwMTg0NTcsMzEuMzIxOTkxMiBDOS43ODU3OTgxNSwyOC45Mzk2MjIxIDgsMjUuMzQ2NzcxIDgsMjEuNDUyMTc2NSBDOCwxNC41ODY5MzAzIDEzLjYwNzQ3NjYsOSAyMC41MDA1ODcsOSBDMjcuMzkyNTIzNCw5IDMzLDE0LjU4NjkzMDMgMzMsMjEuNDUyMTc2NSBDMzMsMjUuMzQ2NzcxIDMxLjIxNDIwMTksMjguOTM5NjIyMSAyOC4wOTgxNTQzLDMxLjMyMTk5MTIgQzI3LjEzMzA0ODQsMzIuNTI2NjI1NSAyNi4wMDM1NjkyLDM0LjM4MDM1ODkgMjYuMDAzNTY5MiwzNi40NTYzMDY0IEwyNi4wMDM1NjkyLDQxIFoiIGlkPSJGaWxsLTMiIGZpbGw9IiMzQzNDM0MiPjwvcGF0aD4KICAgICAgICAgICAgICAgIDxtYXNrIGlkPSJtYXNrLTIiIGZpbGw9IndoaXRlIj4KICAgICAgICAgICAgICAgICAgICA8dXNlIHhsaW5rOmhyZWY9IiNwYXRoLTEiPjwvdXNlPgogICAgICAgICAgICAgICAgPC9tYXNrPgogICAgICAgICAgICAgICAgPGcgaWQ9IkNsaXAtNiI+PC9nPgogICAgICAgICAgICAgICAgPHBvbHlnb24gaWQ9IkZpbGwtNSIgZmlsbD0iIzNDM0MzQyIgbWFzaz0idXJsKCNtYXNrLTIpIiBwb2ludHM9IjE1IDQzLjkyMyAyNSA0My45MjMgMjUgNDMgMTUgNDMiPjwvcG9seWdvbj4KICAgICAgICAgICAgICAgIDxwb2x5Z29uIGlkPSJGaWxsLTciIGZpbGw9IiMzQzNDM0MiIG1hc2s9InVybCgjbWFzay0yKSIgcG9pbnRzPSIxOCA0Ni45MjMgMjIgNDYuOTIzIDIyIDQ2IDE4IDQ2Ij48L3BvbHlnb24+CiAgICAgICAgICAgICAgICA8cG9seWdvbiBpZD0iRmlsbC04IiBmaWxsPSIjM0MzQzNDIiBtYXNrPSJ1cmwoI21hc2stMikiIHBvaW50cz0iMTUuNjM1MDM4NCAyNiAxMyAyMi4yNzEzMjcgMTMuOTE1MzggMjEuNjQ2OTE5NCAxNS42MzUwMzg0IDI0LjA4MDU2ODcgMTcuODEzNTk0NCAyMSAxOS45OTIxNTA0IDI0LjA4MDU2ODcgMjIuMTcxOTE0MSAyMS4wMDExODQ4IDI0LjM1NjUwODIgMjQuMDgyOTM4NCAyNi4wODQ2MiAyMS42NDY5MTk0IDI3IDIyLjI3MjUxMTggMjQuMzU2NTA4MiAyNS45OTc2MzAzIDIyLjE3MzEyMTcgMjIuOTE3MDYxNiAxOS45OTA5NDI4IDI2IDE3LjgxMzU5NDQgMjIuOTE5NDMxMyI+PC9wb2x5Z29uPgogICAgICAgICAgICAgICAgPHBvbHlnb24gaWQ9IkZpbGwtOSIgZmlsbD0iIzNDM0MzQyIgbWFzaz0idXJsKCNtYXNrLTIpIiBwb2ludHM9IjE5IDYgMTkuOTIzIDYgMTkuOTIzIDAgMTkgMCI+PC9wb2x5Z29uPgogICAgICAgICAgICAgICAgPHBvbHlnb24gaWQ9IkZpbGwtMTAiIGZpbGw9IiMzQzNDM0MiIG1hc2s9InVybCgjbWFzay0yKSIgcG9pbnRzPSIwIDE4LjkyMyA2IDE4LjkyMyA2IDE4IDAgMTgiPjwvcG9seWdvbj4KICAgICAgICAgICAgICAgIDxwb2x5Z29uIGlkPSJGaWxsLTExIiBmaWxsPSIjM0MzQzNDIiBtYXNrPSJ1cmwoI21hc2stMikiIHBvaW50cz0iMTAuMTk2OTk5NSA5IDYgNC44MDMwMDA0OSA2LjgwMzAwMDQ5IDQgMTEgOC4xOTY5OTk1MSI+PC9wb2x5Z29uPgogICAgICAgICAgICAgICAgPHBvbHlnb24gaWQ9IkZpbGwtMTIiIGZpbGw9IiMzQzNDM0MiIG1hc2s9InVybCgjbWFzay0yKSIgcG9pbnRzPSIzNCAxOC45MjMgNDAgMTguOTIzIDQwIDE4IDM0IDE4Ij48L3BvbHlnb24+CiAgICAgICAgICAgICAgICA8cG9seWdvbiBpZD0iRmlsbC0xMyIgZmlsbD0iIzNDM0MzQyIgbWFzaz0idXJsKCNtYXNrLTIpIiBwb2ludHM9IjMwLjgwMzAwMDUgOSAzMCA4LjE5Njk5OTUxIDM0LjE5Njk5OTUgNCAzNSA0LjgwMzAwMDQ5Ij48L3BvbHlnb24+CiAgICAgICAgICAgIDwvZz4KICAgICAgICA8L2c+CiAgICA8L2c+Cjwvc3ZnPg==";
    var chartName = ['北部', '中部', '南部', '東部'];
    var data = [];
    var legendName = [];
    for (var i = 0; i < chartName.length; i++) {

        var c = {
            value: totalamtbyStore[i],
            name: chartName[i] + totalamtbyStore[i]
        };
        data[i] = c;
        legendName[i] = chartName[i] + totalamtbyStore[i];
    }
    amtRegionoption = {
        backgroundColor: '#fff',
        tooltip: {
            trigger: 'item',
            formatter: "{b} -> {d}% "
        },
        graphic: {
            elements: [{
                type: 'image',
                style: {
                    image: giftImageUrl,
                    width: 80,
                    height: 85,
                },
                left: 'center',
                top: '50%'
            }]
        },
        legend: {
            orient: 'vertical',
            x: '76%',
            y: 'top',
            itemWidth: 15,
            itemHeight: 15,
            align: 'right',
            textStyle: {
                fontSize: 14,
                color: '#000'
            },
            data: legendName
        },
        series: [{
            type: 'pie',
            radius: ['39%', '65%'],
            center: ['50%', '60%'],
            color: ['#FB7507', '#7ED321', '#4A90E2', '#BD10E0'],
            data: data,
            labelLine: {
                normal: {
                    show: false,
                    length: 20,
                    length2: 20,
                    lineStyle: {
                        color: '#12EABE',
                        width: 2
                    }
                }
            },
            label: {
                normal: {
                    show: false,

                    rich: {
                        b: {
                            fontSize: 20,
                            color: '#12EABE',
                            align: 'left',
                            padding: 4
                        },
                        hr: {
                            borderColor: '#12EABE',
                            width: '100%',
                            borderWidth: 2,
                            height: 0
                        },
                        d: {
                            fontSize: 20,
                            color: '#fff',
                            align: 'left',
                            padding: 4
                        },
                        c: {
                            fontSize: 20,
                            color: '#fff',
                            align: 'left',
                            padding: 4
                        }
                    }
                }
            }
        }]
    };
    chart1b.clear();

})();
(function () {
    var seriesData = quadDig1.map(function (item, index, array) {
        return {
            name: item['name'],
            value: [item['users'], item['bouncerate']]
        }
    });

    var computebouncerateAvgLine = function () {
        let sum = 0;
        quadDig1.forEach(function (item) {
            sum += item['bouncerate']
        });
        return sum / quadDig1.length;
    };

    var computeusersAvgLine = function () {
        let sum = 0;
        quadDig1.forEach(function (item) {
            sum += item['users'];
        });
        return sum / quadDig1.length;
    };
    var avg = {
        bouncerateAvgLine: computebouncerateAvgLine(),
        usersAvgLine: computeusersAvgLine()
    };

    chart5option = {

        tooltip: {
            trigger: 'item',
            axisPointer: {
                show: true,
                type: 'cross',
                lineStyle: {
                    type: 'dashed',
                    width: 1
                },
            },
            formatter: function (obj) {
                if (obj.componentType == "series") {
                    return '<div style="border-bottom: 1px solid rgba(255,255,255,.3); font-size: 18px;padding-bottom: 7px;margin-bottom: 7px">' +
                        obj.name +
                        '</div>' +
                        '<span>' +
                        '訪客數' +
                        '</span>' +
                        ' : ' + obj.data.value[0] + '（人）' +
                        '<br/>' +
                        '<span>' +
                        '跳出率' +
                        '</span>' +
                        ' : ' + obj.data.value[1] + '%';
                }
            }
        },
        label: {
            normal: {
                show: true,
                position: 'bottom',
                formatter: function (params) {
                    return params.name;
                }
            },
            emphasis: {
                show: true,
                position: 'bottom',
            }
        },
        xAxis: {
            name: '訪客',
            type: 'value',
            scale: true,
            axisLabel: {
                formatter: '{value} ',
                fontSize: 10

            },
            splitLine: {
                show: false
            },
            axisLine: {
                lineStyle: {
                    color: '#3259B8'
                }
            }
        },
        yAxis: {
            name: '跳出率',
            type: 'value',
            scale: true,
            axisLabel: {
                formatter: '{value} %',
                fontSize: 10
            },
            splitLine: {
                show: false
            },
            axisLine: {
                lineStyle: {
                    color: '#3259B8'
                }
            }
        },


        series: [{
            type: 'scatter',
            data: seriesData,
            symbolSize: 12,

            markLine: {
                label: {
                    normal: {
                        formatter: function (params) {
                            if (params.dataIndex == 0) {
                                return params.value + "人";
                            } else if (params.dataIndex == 1) {
                                return params.value + "%";
                            }
                            return params.value;
                        }
                    }
                },
                lineStyle: {
                    normal: {
                        color: "#626c91",
                        type: 'solid',
                        width: 1,
                    },
                    emphasis: {
                        color: "#d9def7"
                    }
                },
                data: [{
                    xAxis: avg.usersAvgLine,
                    name: '訪客平均線',
                    itemStyle: {
                        normal: {
                            color: "#b84a58",
                        }
                    }
                }, {
                    yAxis: avg.bouncerateAvgLine,
                    name: '跳出率平均線',
                    itemStyle: {
                        normal: {
                            color: "#b84a58",
                        }
                    }
                }]
            },
            color: '#FB7507'
            /*
             markArea: {
                 silent: true,
                 data: [
                     [{
                         name: '優異',
                         itemStyle: {
                             normal: {
                                 color: 'transparent'
                             },
                         },
                         label: {
                             normal: {
                                 show: true,
                                 position: 'insideTopLeft',
                                 fontStyle: 'normal',
                                 color: "#409EFF",
                                 fontSize: 14,
                             }
                         },
                         coord: [avg.usersAvgLine, avg.bouncerateAvgLine],
                     }, {
                         coord: [Number.MAX_VALUE, 0],
                     }],
                     [{
                         name: '淘汰',
                         itemStyle: {
                             normal: {
                                 color: 'transparent',
                             },
                         },
                         label: {
                             normal: {
                                 show: true,
                                 position: 'insideTopRight',
                                 fontStyle: 'normal',
                                 color: "#409EFF",
                                 fontSize: 14,
                             }
                         },
                         coord: [0, 0],
                     }, {
                         coord: [avg.usersAvgLine, avg.bouncerateAvgLine],
                     }],
                     [{
                         name: '保持',
                         itemStyle: {
                             normal: {
                                 color: 'transparent',
                             },
                         },
                         label: {
                             normal: {
                                 show: true,
                                 position: 'insideBottomLeft',
                                 fontStyle: 'normal',
                                 color: "#409EFF",
                                 fontSize: 14,
                             }
                         },
                         coord: [avg.usersAvgLine, avg.bouncerateAvgLine],
                     }, {
                         coord: [Number.MAX_VALUE, Number.MAX_VALUE],
                     }],
                     [{
                         name: '激励',
                         itemStyle: {
                             normal: {
                                 color: 'transparent',
                             },
                         },
                         label: {
                             normal: {
                                 show: true,
                                 position: 'insideBottomRight',
                                 fontStyle: 'normal',
                                 color: "#409EFF",
                                 fontSize: 14,
                             }
                         },
                         coord: [0, Number.MAX_VALUE],
                     }, {
                         coord: [avg.usersAvgLine, avg.bouncerateAvgLine],
                     }],
                 ]
             }*/
        }]
    };
    chart5.setOption(chart5option);
})();
(function () {
    var seriesData = quadDig2.map(function (item, index, array) {
        return {
            name: item['name'],
            value: [item['users'], item['bouncerate']]
        }
    });
    console.log(seriesData);
    var computebouncerateAvgLine = function () {
        let sum = 0;
        quadDig2.forEach(function (item) {
            sum += item['bouncerate'];
        });
        return sum / quadDig2.length;
    };

    var computeusersAvgLine = function () {
        let sum = 0;
        quadDig2.forEach(function (item) {
            sum += item['users'];
        });
        return sum / quadDig2.length;
    };
    var avg = {
        bouncerateAvgLine: computebouncerateAvgLine(),
        usersAvgLine: computeusersAvgLine()
    };

    chart7option = {

        tooltip: {
            trigger: 'item',
            axisPointer: {
                show: true,
                type: 'cross',
                lineStyle: {
                    type: 'dashed',
                    width: 1
                },
            },
            formatter: function (obj) {

                if (obj.componentType == "series") {

                    return '<div style="border-bottom: 1px solid rgba(255,255,255,.3); font-size: 18px;padding-bottom: 7px;margin-bottom: 7px">' +
                        obj.name +
                        '</div>' +
                        '<span>' +
                        '收益' +
                        '</span>' +
                        ' : ' + obj.data.value[0] +
                        '<br/>' +
                        '<span>' +
                        '購買數量' +
                        '</span>' +
                        ' : ' + obj.data.value[1];
                }
            }
        },

        xAxis: {
            name: '收益',
            type: 'value',
            scale: true,
            axisLabel: {
                formatter: '{value} ',
                fontSize: 10

            },
            splitLine: {
                show: false
            },
            axisLine: {
                lineStyle: {
                    color: '#3259B8'
                }
            }
        },
        yAxis: {
            name: '購買數量',
            type: 'value',
            scale: true,
            axisLabel: {
                formatter: '{value} ',
                fontSize: 10
            },
            splitLine: {
                show: false
            },
            axisLine: {
                lineStyle: {
                    color: '#3259B8'
                }
            }
        },
        series: [{
            type: 'scatter',
            data: seriesData,
            symbolSize: 12,

            markLine: {
                label: {
                    normal: {
                        formatter: function (params) {
                            if (params.dataIndex == 0) {
                                return params.value;
                            } else if (params.dataIndex == 1) {
                                return params.value;
                            }
                            return params.value;
                        }
                    }
                },
                lineStyle: {
                    normal: {
                        color: "#626c91",
                        type: 'solid',
                        width: 1,
                    },
                    emphasis: {
                        color: "#d9def7"
                    }
                },
                data: [{
                    xAxis: avg.usersAvgLine,
                    name: '購買數量平均線',
                    itemStyle: {
                        normal: {
                            color: "#b84a58",
                        }
                    }
                }, {
                    yAxis: avg.bouncerateAvgLine,
                    name: '收益平均線',
                    itemStyle: {
                        normal: {
                            color: "#b84a58",
                        }
                    }
                }]
            },
            color: '#FB7507'

        }]
    };
    chart7.setOption(chart7option);
})();
(function () {
    var barColor = ['#FB7507'];
    var barWidth = '100%';
    chart6option = {
        xAxis: {
            type: 'category',
            data: campaign_dim_data,
            show: true,
            axisTick: {
                show: false
            },
            axisLabel: {
                fontSize: 9,
                color: '#333',
                rotate: 25,
            },
        },
        yAxis: {

            type: 'value'
        },
        tooltip: {
            trigger: 'axis'
        },
        series: [{
            data: campaign_metrics_data,
            barMaxWidth: '100%',
            itemStyle: {
                normal: {
                    color: function (params) {
                        var num = barColor.length;
                        return barColor[params.dataIndex % num];
                    }
                }
            },
            markPoint: {
                data: [{
                        type: 'max',
                        name: '最大值'
                    },
                    {
                        type: 'min',
                        name: '最小值'
                    }
                ]
            },
            markLine: {
                data: [{
                    type: 'average',
                    name: '平均值'
                }]
            },
            type: 'bar'
        }]
    };


    chart6.setOption(chart6option, true);

})();
(function () {
    chart2option = {

        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        legend: {
            data: ['新訪客', '會員'],

        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: {
            type: 'category',
            data: ['第一週', '第二週', '第三週', '第四週'],
            axisLabel: {
                fontSize: 10,
                color: '#333',
                margin: 4,
                interval: 0,
                formatter: function (val) {
                    return val.split("").join("\n");
                }
            }
        },
        yAxis: {
            type: 'value',
            boundaryGap: [0, 0.01]
        },
        series: [

            {
                name: '新訪客',
                type: 'bar',
                barMaxWidth: '25%',
                color: '#FB7507',
                data: [8203, 3489, 9034, 14970, 11744]
            },
            {
                name: '會員',
                type: 'bar',
                barMaxWidth: '25%',
                color: '#BD10E0',
                data: [19325, 23438, 31000, 23141]
            }
        ]
    };
    chart2.setOption(chart2option);
})();
(function () {
    chart4option = {

        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        legend: {
            data: ['新訪客', '會員'],

        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: {
            type: 'category',
            data: ['第一週', '第二週', '第三週', '第四週'],
            axisLabel: {
                fontSize: 10,
                color: '#333',
                margin: 2,
                interval: 0,
                formatter: function (val) {
                    return val.split("").join("\n");
                }
            },

        },
        yAxis: {
            type: 'value',
            boundaryGap: [0, 0.01]
        },
        series: [

            {
                name: '新訪客',
                type: 'bar',
                barMaxWidth: '25%',
                color: '#4A90E2',
                data: [252, 183, 310, 388]
            },
            {
                name: '會員',
                type: 'bar',
                barMaxWidth: '25%',
                color: '#7ED321',
                data: [631, 678, 911, 788]
            }
        ]
    };
    chart4.setOption(chart4option);
})();
setTimeout((function () {
    chart1b.setOption(amtRegionoption);
}), 600);