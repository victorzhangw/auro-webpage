var store_dim_data=[],store_metrics_data=[],totalamtbyStore=[];
var source_dim_data=[],medium_dim_data=[],source_medium_dim_data=[],campaign_dim_data=[],source_campaign_dim_data=[];
var source_metrics_data=[],medium_metrics_data=[],source_medium_metrics_data=[],campaign_metrics_data=[],source_campaign_metrics_data=[];
var widgetData=[
    {
      "transaction": {
        "name":"交易次數",
        "value":"282"
      },
      "conversionRate": {
        "name":"電子商務轉換率",
        "value":"1.114"
      },
      "revenue": {
        "name":"收益",
        "value":"360,796"
      },
      "avgOrderValue": {
        "name":"平均訂單價值",
        "value":"1,279.42"
      },
     
    }
  ];



  var quadDig1=[{
    name: 'google / cpc',
    users: 6040,
    bouncerate: 77.66
  }, {
    name: '(direct) / (none)',
    users: 3402,
    bouncerate: 70.27
  }, {
    name: 'google / organic',
    users: 347,
    bouncerate: 56.76
  }, {
    name: '104.com.tw / referral',
    users: 1310,
    bouncerate: 37.5
  }, {
    name: 'aurobase.com / referral',
    users: 137,
    bouncerate: 100
  }, {
    name: 'yahoo / organic',
    users: 35,
    bouncerate: 100
  }, {
    name: 'l.facebook.com / referral',
    users:29,
    bouncerate: 50
  },
  {
    name: 'FB / Oracle_Post171221', 
    users: 16,
    bouncerate: 0
  },
  {
    name: 'FB / Oracle_Post171221',
    users: 21,
    bouncerate: 79
  },
  {
    name: 'ibm.com / referral',
    users: 11,
    bouncerate: 97
  }
  
  ];


  var quadDig2=[{
    name: 'google / cpc',
    users: 100276,
    bouncerate: 310
  }, {
    name: '(direct) / (none)',
    users: 91120,
    bouncerate: 278
  }, {
    name: 'google / organic',
    users: 24670,
    bouncerate: 78
  }, {
    name: '104.com.tw / referral',
    users: 17659,
    bouncerate: 56
  }, {
    name: 'aurobase.com / referral',
    users: 13710,
    bouncerate: 34
  }, {
    name: 'yahoo / organic',
    users: 12100,
    bouncerate: 31
  }, {
    name: 'l.facebook.com / referral',
    users:8210,
    bouncerate: 28
  },
  {
    name: 'FB / Oracle_Post171221',
    users: 8100,
    bouncerate: 27
  },
  {
    name: 'FB / Oracle_Post171221',
    users: 7363,
    bouncerate: 19
  },
  {
    name: 'ibm.com / referral',
    users: 6431,
    bouncerate: 16
  }
  
  ];




var salesbyCity=[
{name: '宜蘭縣', value: 1500},
{name: '彰化縣', value: 1100},
{name: '南投縣', value: 2000},
{name: '雲林縣', value: 2800},
{name: '屏東縣', value: 1800},
{name: '臺東縣', value: 4500},
{name: '花蓮縣', value: 3500},
{name: '基隆市', value: 3900},
{name: '新竹市', value: 3300},
{name: '臺北市', value: 23390},
{name: '新北市', value: 27000},
{name: '臺中市', value: 24000},
{name: '臺南市', value: 19100},
{name: '桃園市', value: 15000},
{name: '苗栗縣', value: 1000},
{name: '新竹縣', value: 5300},
{name: '嘉義市', value: 2200},
{name: '嘉義縣', value: 1100},
{name: '高雄市', value: 26000}];
var salesbyStore = [
    {
    "基隆市": {
      "基隆":2700,
      "基隆愛買":1200}
    },
    {"臺北市": {
      "士林":  2100,
      "大同":  1600 ,
      "大安":  1900,
      "中山":  1100,
      "中正":  1670,
      "內湖":  1330,
      "東湖":  1190,
      "文山":  1660,
      "北投":  1780,
      "天母":  1540,
      "八德":  1900,
      "莊敬":  1240, 
      "永吉": 1210,
      "南港":  1870 ,
      "西園":  1300}
    },
    {"新北市": {
      "三重": 1900 ,
      "三峽": 1200 ,
      "土城": 1200 ,
      "中和": 1110,
      "景新":1790,
      "永和":  1100 ,
      "汐止":  1200 ,
      "板橋":1400, 
      "三民": 1000,
      "板橋忠孝": 1000,
      "泰山":  1700 ,
      "淡水": 1900,
      "竹圍":1300,
      "新店":1780,
      "安康":2100,
      "新莊": 3300,
      "民安":900,
      "樹林":  2100 ,
      "蘆洲":  1700 ,
      "鶯歌":  1300 }
    },
    {"宜蘭縣": {
      "宜蘭":  800,
      "羅東":  1200}
    },
    {"桃園市": {
        "大湳":  1100,
        "中原":1000,
        "中壢":1000,
        "內壢":1000,
		"新屋":1000,
        "桃園":1000,
        "桃園二":1000,
        "國際":1000,
	    "楊梅":  1000 ,
	    "大園":  1000 ,
	    "龍潭":  1000 ,
	    "龜山":  1000 ,
	    "蘆竹":  1000 }
    },
    {"新竹市": {
      "北大":  1200 ,
      "光復":  2100}
    },
    {"新竹縣": {
      "竹北":  2200,
      "新豐":  3100}
    },
    {"花蓮縣": {
      "花蓮":  1000 }
    },
    {"苗栗縣": {
      "竹南":  1000}
    },
    {"臺中市": {
      "大里":  1100 ,
      "北屯":  1200 ,
      "學士":  1610 ,
      "永福":  1520 ,
      "逢甲":  1320 ,
      "向上":  2100 ,
      "沙鹿":  2700 ,
      "精武":  2230 ,
      "黎明":  1350 ,
      "復興":  1780 ,
      "潭子":  2190 ,
      "豐原":  2560 ,
      "大雅":  2450 }
    },
    {"彰化縣": {
      "員林":  1800 ,
      "彰化":  3300 ,
      "鹿港":  700 }
    },
    {"南投縣": {
      "南投":  1890 ,
      "草屯":  2100 }
    },
    {"雲林縣": {
      "斗六":  3300 ,
      "虎尾":  2300} 
    },
    {"嘉義市": {
      "民族":  1100 }
    },
    {"嘉義縣": {
      "朴子":  3100 ,
    "民雄":  1000 }
    },
    {"臺南市": {
      "仁德":  2100 ,
      "成功":  3300 ,
        "永康":3400,
        "永康2":1200 ,

    "永華":  1300 ,
    "安南":  2100 ,
      "東寧":  2200 ,
      "新營":  1560 }
    },
    {"高雄市": {
        "澄清":2100,
        "鼎山":1790,
    "大寮":  1110 ,
      "小港":  2450 ,
    "仁武":  2560 ,
      "左營":  1100 ,
      "岡山":  1200 ,
    "中華":  1400 ,
          "楠梓":1700,
          "後昌":1000,
        "鳳山":600,
        "五甲":2900}
      
    },
    {"屏東縣": {
      "屏東":  1300 }
    },
	  {"臺東縣": {
      "台東":  1200 
    }
  }];
var utmSource=[
  {
    "A1": "server=cht8.cool3c.com",
    "A2": "ip=10.28.45.103",
    "A3": "mac=4c:5e:0c:d1:4a:75",
    "A4": "gwaddr=10.16.0.1:2060",
    "A5": "token=b637e0e0d99840fec2b9de5c858e2544",
    "A6": "timekey=77cc7e29a4da44786b9e7319aa95c4aa",
    "A7": "utm_source=%E6%A1%83%E5%9C%92%E5%B8%82%E6%A5%8A%E6%A2%85%E5",
    "A8": "utm_source=%E6%A1%83%E5%9C%92%E5%B8%82%E6%A5%8A%E6%A2%85%E5",
    "A9": "utm_source=%E6%A1%83%E5%9C%92%E5%B8%82%E6%A5%8A%E6%A2%85%E5"
  },
  {
    "A1": "utm_source=urAD",
    "A2": "utm_medium=GDN",
    "A3": "utm_medium=GDN",
    "A4": "utm_medium=GDN",
    "A5": "utm_medium=GDN",
    "A6": "utm_medium=GDN",
    "A7": "utm_medium=GDN",
    "A8": "utm_medium=GDN",
    "A9": "utm_medium=GDN"
  },
  {
    "A1": "utm_source=urAD",
    "A2": "utm_medium=google_responsive",
    "A3": "utm_medium=google_responsive",
    "A4": "utm_medium=google_responsive",
    "A5": "utm_medium=google_responsive",
    "A6": "utm_medium=google_responsive",
    "A7": "utm_medium=google_responsive",
    "A8": "utm_medium=google_responsive",
    "A9": "utm_medium=google_responsive"
  },
  {
    "A1": "utm_source=urAD",
    "A2": "utm_medium=res_epaper",
    "A3": "utm_medium=res_epaper",
    "A4": "utm_medium=res_epaper",
    "A5": "utm_medium=res_epaper",
    "A6": "utm_medium=res_epaper",
    "A7": "utm_medium=res_epaper",
    "A8": "utm_medium=res_epaper",
    "A9": "utm_medium=res_epaper"
  },
  {
    "A1": "client=ca-pub-4485239425924787",
    "A2": "output=html",
    "A3": "h=100",
    "A4": "slotname=3602339221%2F4096031941",
    "A5": "adk=2053275180",
    "A6": "adf=406415463",
    "A7": "w=320",
    "A8": "guci=2.2.0.0.2.2.0.0",
    "A9": "url=https%3A%2F%2Fhealth.tvbs.com.tw%2Freview%2F307313%3Futm_source%3DL"
  },
  {
    "A1": "client=ca-pub-4485239425924787",
    "A2": "output=html",
    "A3": "h=90",
    "A4": "slotname=3602339221%2F1002863461",
    "A5": "adk=64173763",
    "A6": "adf=406415464",
    "A7": "w=970",
    "A8": "guci=2.2.0.0.2.2.0.0",
    "A9": "url=https%3A%2F%2Fsupertaste.tvbs.com.tw%2Ftopic%2Farticle%2F316334%3Futm_"
  },
  {
    "A1": "client=ca-pub-9406432372950751",
    "A2": "output=html",
    "A3": "h=100",
    "A4": "slotname=7381969427",
    "A5": "adk=3353076391",
    "A6": "adf=3038315349",
    "A7": "w=320",
    "A8": "lmt=1559726592",
    "A9": "guci=2.2.0.0.2.2.0.0"
  },
  {
    "A1": "utm_source=Facebook_PicSee",
    "A2": "utm_source=Facebook_PicSee",
    "A3": "utm_source=Facebook_PicSee",
    "A4": "utm_source=Facebook_PicSee",
    "A5": "utm_source=Facebook_PicSee",
    "A6": "utm_source=Facebook_PicSee",
    "A7": "utm_source=Facebook_PicSee",
    "A8": "utm_source=Facebook_PicSee",
    "A9": "utm_source=Facebook_PicSee"
  },
  {
    "A1": "pfbxCustomerInfoId=PFBC20160601001",
    "A2": "positionId=PFBP201606140001",
    "A3": "sampleId=us_201904090001",
    "A4": "tproId=c_x05_pad_tpro_0128",
    "A5": "format=0",
    "A6": "page=1",
    "A7": "padHeight=250",
    "A8": "padWidth=300",
    "A9": "keyword="
  },
  {
    "A1": "pfbxCustomerInfoId=PFBC20160601001",
    "A2": "positionId=PFBP201701040002",
    "A3": "sampleId=us_201701040002",
    "A4": "tproId=c_x05_pad_tpro_0145",
    "A5": "format=0",
    "A6": "page=1",
    "A7": "padHeight=250",
    "A8": "padWidth=300",
    "A9": "keyword="
  },
  {
    "A1": "pfbxCustomerInfoId=PFBC20160601001",
    "A2": "positionId=PFBP201804200001",
    "A3": "sampleId=us_201804200001",
    "A4": "tproId=c_x05_pad_tpro_0128",
    "A5": "format=0",
    "A6": "page=1",
    "A7": "padHeight=250",
    "A8": "padWidth=300",
    "A9": "keyword="
  },
  {
    "A1": "pfbxCustomerInfoId=PFBC20160601001",
    "A2": "positionId=PFBP201811080014",
    "A3": "sampleId=us_201811220007",
    "A4": "tproId=c_x05_pad_tpro_0120",
    "A5": "format=0",
    "A6": "page=1",
    "A7": "padHeight=90",
    "A8": "padWidth=728",
    "A9": "keyword="
  },
  {
    "A1": "pfbxCustomerInfoId=PFBC20160601001",
    "A2": "positionId=PFBP201905290004",
    "A3": "sampleId=us_201905300001",
    "A4": "tproId=c_x05_pad_tpro_0128",
    "A5": "format=0",
    "A6": "page=1",
    "A7": "padHeight=250",
    "A8": "padWidth=300",
    "A9": "keyword="
  },
  {
    "A1": "pfbxCustomerInfoId=PFBC20160601001",
    "A2": "positionId=PFBP201905290004",
    "A3": "sampleId=us_201905300001",
    "A4": "tproId=c_x05_tp_tpro_0001",
    "A5": "format=0",
    "A6": "page=1",
    "A7": "padHeight=250",
    "A8": "padWidth=300",
    "A9": "keyword="
  },
  {
    "A1": "utm_medium=M",
    "A2": "utm_campaign=SHARE",
    "A3": "utm_source=LINE",
    "A4": "utm_source=LINE",
    "A5": "utm_source=LINE",
    "A6": "utm_source=LINE",
    "A7": "utm_source=LINE",
    "A8": "utm_source=LINE",
    "A9": "utm_source=LINE"
  },
  {
    "A1": "utm_source=LINE",
    "A2": "utm_medium=APP",
    "A3": "utm_campaign=SHARE",
    "A4": "utm_campaign=SHARE",
    "A5": "utm_campaign=SHARE",
    "A6": "utm_campaign=SHARE",
    "A7": "utm_campaign=SHARE",
    "A8": "utm_campaign=SHARE",
    "A9": "utm_campaign=SHARE"
  },
  {
    "A1": "utm_source=line",
    "A2": "utm_medium=social",
    "A3": "utm_campaign=twad_social_appledaily.tw",
    "A4": "utm_content=message",
    "A5": "utm_content=message",
    "A6": "utm_content=message",
    "A7": "utm_content=message",
    "A8": "utm_content=message",
    "A9": "utm_content=message"
  },
  {
    "A1": "utm_campaign=twad_social_appledaily.tw",
    "A2": "utm_medium=social",
    "A3": "utm_source=facebook",
    "A4": "utm_content=link_post",
    "A5": "utm_content=link_post",
    "A6": "utm_content=link_post",
    "A7": "utm_content=link_post",
    "A8": "utm_content=link_post",
    "A9": "utm_content=link_post"
  },
  {
    "A1": "utm_source=line",
    "A2": "utm_medium=social",
    "A3": "utm_campaign=twad_social_appledaily.tw",
    "A4": "utm_content=message",
    "A5": "utm_content=message",
    "A6": "utm_content=message",
    "A7": "utm_content=message",
    "A8": "utm_content=message",
    "A9": "utm_content=message"
  },
  {
    "A1": "id=ARTL000141028",
    "A2": "utm_source=line",
    "A3": "utm_medium=social",
    "A4": "utm_content=wealth",
    "A5": "utm_campaign=content",
    "A6": "utm_campaign=content",
    "A7": "utm_campaign=content",
    "A8": "utm_campaign=content",
    "A9": "utm_campaign=content"
  },
  {
    "A1": "utm_source=line",
    "A2": "utm_medium=message--",
    "A3": "utm_campaign=57738",
    "A4": "utm_content=-",
    "A5": "utm_content=-",
    "A6": "utm_content=-",
    "A7": "utm_content=-",
    "A8": "utm_content=-",
    "A9": "utm_content=-"
  },
  {
    "A1": "NewsID=550778",
    "A2": "utm_source=facebook",
    "A3": "utm_medium=mainpage",
    "A4": "utm_medium=mainpage",
    "A5": "utm_medium=mainpage",
    "A6": "utm_medium=mainpage",
    "A7": "utm_medium=mainpage",
    "A8": "utm_medium=mainpage",
    "A9": "utm_medium=mainpage"
  },
  {
    "A1": "NewsID=550840",
    "A2": "utm_source=facebook",
    "A3": "utm_medium=ent",
    "A4": "utm_medium=ent",
    "A5": "utm_medium=ent",
    "A6": "utm_medium=ent",
    "A7": "utm_medium=ent",
    "A8": "utm_medium=ent",
    "A9": "utm_medium=ent"
  },
  {
    "A1": "newsid=551012",
    "A2": "utm_source=newssuite",
    "A3": "utm_medium=newssuite",
    "A4": "utm_campaign=newssuite",
    "A5": "utm_campaign=newssuite",
    "A6": "utm_campaign=newssuite",
    "A7": "utm_campaign=newssuite",
    "A8": "utm_campaign=newssuite",
    "A9": "utm_campaign=newssuite"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=b7da7f7ac71",
    "A9": "fk=b7da7f7ac71"
  },
  {
    "A1": "utm_source=facebook.com",
    "A2": "utm_medium=FB%20promote",
    "A3": "utm_campaign=motor%2020190603",
    "A4": "utm_campaign=motor%2020190603",
    "A5": "utm_campaign=motor%2020190603",
    "A6": "utm_campaign=motor%2020190603",
    "A7": "utm_campaign=motor%2020190603",
    "A8": "utm_campaign=motor%2020190603",
    "A9": "utm_campaign=motor%2020190603"
  },
  {
    "A1": "utm_source=facebook.com",
    "A2": "utm_medium=FB%20promote",
    "A3": "utm_campaign=motor%2020190603",
    "A4": "fbclid=IwAR2CZowQDqatYLgcoGxGhjqr_I_ZTC004C57sETQjxVYlNhczvJazh4tJyM",
    "A5": "fbclid=IwAR2CZowQDqatYLgcoGxGhjqr_I_ZTC004C57sETQjxVYlNhczvJazh4tJyM",
    "A6": "fbclid=IwAR2CZowQDqatYLgcoGxGhjqr_I_ZTC004C57sETQjxVYlNhczvJazh4tJyM",
    "A7": "fbclid=IwAR2CZowQDqatYLgcoGxGhjqr_I_ZTC004C57sETQjxVYlNhczvJazh4tJyM",
    "A8": "fbclid=IwAR2CZowQDqatYLgcoGxGhjqr_I_ZTC004C57sETQjxVYlNhczvJazh4tJyM",
    "A9": "fbclid=IwAR2CZowQDqatYLgcoGxGhjqr_I_ZTC004C57sETQjxVYlNhczvJazh4tJyM"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail10d-201906",
    "A6": "fk=44706a9e229f443281b02dfbeeda820b",
    "A7": "fk=44706a9e229f443281b02dfbeeda820b",
    "A8": "fk=44706a9e229f443281b02dfbeeda820b",
    "A9": "fk=44706a9e229f443281b02dfbeeda820b"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail20d-201904",
    "A6": "fk=cbc0ff16be4f4417bdbad93d355e1aa8",
    "A7": "fk=cbc0ff16be4f4417bdbad93d355e1aa8",
    "A8": "fk=cbc0ff16be4f4417bdbad93d355e1aa8",
    "A9": "fk=cbc0ff16be4f4417bdbad93d355e1aa8"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail20d-201906",
    "A6": "fk=1912751f1ca74be1ab6f2ad064f6e577",
    "A7": "fk=1912751f1ca74be1ab6f2ad064f6e577",
    "A8": "fk=1912751f1ca74be1ab6f2ad064f6e577",
    "A9": "fk=1912751f1ca74be1ab6f2ad064f6e577"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail20d-201906",
    "A6": "fk=8cef76369d154f5193df5eff746f6825",
    "A7": "fk=8cef76369d154f5193df5eff746f6825",
    "A8": "fk=8cef76369d154f5193df5eff746f6825",
    "A9": "fk=8cef76369d154f5193df5eff746f6825"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail25d-201906",
    "A6": "fk=2260d7c7103d4d589bcc52f095361c79",
    "A7": "fk=2260d7c7103d4d589bcc52f095361c79",
    "A8": "fk=2260d7c7103d4d589bcc52f095361c79",
    "A9": "fk=2260d7c7103d4d589bcc52f095361c79"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail25d-201906",
    "A6": "fk=d7513217f0d14b3397ee8cd5c420701e",
    "A7": "fk=d7513217f0d14b3397ee8cd5c420701e",
    "A8": "fk=d7513217f0d14b3397ee8cd5c420701e",
    "A9": "fk=d7513217f0d14b3397ee8cd5c420701e"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail35d-201904",
    "A6": "fk=8818f38eb4be4a338fb6c819511ca4a6",
    "A7": "fk=8818f38eb4be4a338fb6c819511ca4a6",
    "A8": "fk=8818f38eb4be4a338fb6c819511ca4a6",
    "A9": "fk=8818f38eb4be4a338fb6c819511ca4a6"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail35d-201905",
    "A6": "fk=8367946a565546d1b8781151b64c0f31",
    "A7": "fk=8367946a565546d1b8781151b64c0f31",
    "A8": "fk=8367946a565546d1b8781151b64c0f31",
    "A9": "fk=8367946a565546d1b8781151b64c0f31"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail35d-201906",
    "A6": "fk=066ee5ab3a1348db82490141cdafe07c",
    "A7": "fk=066ee5ab3a1348db82490141cdafe07c",
    "A8": "fk=066ee5ab3a1348db82490141cdafe07c",
    "A9": "fk=066ee5ab3a1348db82490141cdafe07c"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail35d-201906",
    "A6": "fk=ce38f091af094cde9bf2de7d68059101",
    "A7": "fk=ce38f091af094cde9bf2de7d68059101",
    "A8": "fk=ce38f091af094cde9bf2de7d68059101",
    "A9": "fk=ce38f091af094cde9bf2de7d68059101"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail35d-201907",
    "A6": "fk=0d0e11156cf7427cbc39b34132595d37",
    "A7": "fk=0d0e11156cf7427cbc39b34132595d37",
    "A8": "fk=0d0e11156cf7427cbc39b34132595d37",
    "A9": "fk=0d0e11156cf7427cbc39b34132595d37"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail35d-201907",
    "A6": "fk=161b6d2aeceb4da494fb2308cf987d10",
    "A7": "fk=161b6d2aeceb4da494fb2308cf987d10",
    "A8": "fk=161b6d2aeceb4da494fb2308cf987d10",
    "A9": "fk=161b6d2aeceb4da494fb2308cf987d10"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail35d-201907",
    "A6": "fk=16b4416a131c4b48b2a23fc9e6c48145",
    "A7": "fk=16b4416a131c4b48b2a23fc9e6c48145",
    "A8": "fk=16b4416a131c4b48b2a23fc9e6c48145",
    "A9": "fk=16b4416a131c4b48b2a23fc9e6c48145"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail35d-201907",
    "A6": "fk=3c756a02959d46268ac39adc72f6bca1",
    "A7": "fk=3c756a02959d46268ac39adc72f6bca1",
    "A8": "fk=3c756a02959d46268ac39adc72f6bca1",
    "A9": "fk=3c756a02959d46268ac39adc72f6bca1"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail35d-201907",
    "A6": "fk=56b44b1afc0f4001a38f7bbb6c048f1c",
    "A7": "fk=56b44b1afc0f4001a38f7bbb6c048f1c",
    "A8": "fk=56b44b1afc0f4001a38f7bbb6c048f1c",
    "A9": "fk=56b44b1afc0f4001a38f7bbb6c048f1c"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail35d-201907",
    "A6": "fk=e8efc60f5abf4ca4a48d4796e825b135",
    "A7": "fk=e8efc60f5abf4ca4a48d4796e825b135",
    "A8": "fk=e8efc60f5abf4ca4a48d4796e825b135",
    "A9": "fk=e8efc60f5abf4ca4a48d4796e825b135"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=cRenew",
    "A5": "utm_campaign=cRenewmail35d-201907",
    "A6": "fk=ed2822ca0eb242e4ab8eac358edc2dc0",
    "A7": "fk=ed2822ca0eb242e4ab8eac358edc2dc0",
    "A8": "fk=ed2822ca0eb242e4ab8eac358edc2dc0",
    "A9": "fk=ed2822ca0eb242e4ab8eac358edc2dc0"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail10d-201906",
    "A6": "fk=03a00f5a6fac430d94423b9889824869",
    "A7": "fk=03a00f5a6fac430d94423b9889824869",
    "A8": "fk=03a00f5a6fac430d94423b9889824869",
    "A9": "fk=03a00f5a6fac430d94423b9889824869"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail10d-201906",
    "A6": "fk=45ac609972c047e184955bf3abbb29d3",
    "A7": "fk=45ac609972c047e184955bf3abbb29d3",
    "A8": "fk=45ac609972c047e184955bf3abbb29d3",
    "A9": "fk=45ac609972c047e184955bf3abbb29d3"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail10d-201906",
    "A6": "fk=ec72c74931f04d858c5031c69922e41f",
    "A7": "fk=ec72c74931f04d858c5031c69922e41f",
    "A8": "fk=ec72c74931f04d858c5031c69922e41f",
    "A9": "fk=ec72c74931f04d858c5031c69922e41f"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail20d-201906",
    "A6": "fk=0d9d765291664596803b68ff96280d2f",
    "A7": "fk=0d9d765291664596803b68ff96280d2f",
    "A8": "fk=0d9d765291664596803b68ff96280d2f",
    "A9": "fk=0d9d765291664596803b68ff96280d2f"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail20d-201906",
    "A6": "fk=254eeb3891f849ae8266dfe3967faff6",
    "A7": "fk=254eeb3891f849ae8266dfe3967faff6",
    "A8": "fk=254eeb3891f849ae8266dfe3967faff6",
    "A9": "fk=254eeb3891f849ae8266dfe3967faff6"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail20d-201906",
    "A6": "fk=36778e01e8e947d7826228a5d429160a",
    "A7": "fk=36778e01e8e947d7826228a5d429160a",
    "A8": "fk=36778e01e8e947d7826228a5d429160a",
    "A9": "fk=36778e01e8e947d7826228a5d429160a"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail20d-201906",
    "A6": "fk=84785f402081430db638551b3afb43c6",
    "A7": "fk=84785f402081430db638551b3afb43c6",
    "A8": "fk=84785f402081430db638551b3afb43c6",
    "A9": "fk=84785f402081430db638551b3afb43c6"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail25d-201907",
    "A6": "fk=840cb66c5e88424b87adcc7a9312a423",
    "A7": "fk=840cb66c5e88424b87adcc7a9312a423",
    "A8": "fk=840cb66c5e88424b87adcc7a9312a423",
    "A9": "fk=840cb66c5e88424b87adcc7a9312a423"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail25d-201907",
    "A6": "fk=9868d636144b4a1e9ea2848a942568a4",
    "A7": "fk=9868d636144b4a1e9ea2848a942568a4",
    "A8": "fk=9868d636144b4a1e9ea2848a942568a4",
    "A9": "fk=9868d636144b4a1e9ea2848a942568a4"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail25d-201907",
    "A6": "fk=a313046eb7a54bc6855fc05240b79a2a",
    "A7": "fk=a313046eb7a54bc6855fc05240b79a2a",
    "A8": "fk=a313046eb7a54bc6855fc05240b79a2a",
    "A9": "fk=a313046eb7a54bc6855fc05240b79a2a"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail35d-201901",
    "A6": "fk=5d7dc94c2af3402bb984ee67d35f7480",
    "A7": "fk=5d7dc94c2af3402bb984ee67d35f7480",
    "A8": "fk=5d7dc94c2af3402bb984ee67d35f7480",
    "A9": "fk=5d7dc94c2af3402bb984ee67d35f7480"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail35d-201904",
    "A6": "fk=67b99962b5114350a8adc147b1c2e0e4",
    "A7": "fk=67b99962b5114350a8adc147b1c2e0e4",
    "A8": "fk=67b99962b5114350a8adc147b1c2e0e4",
    "A9": "fk=67b99962b5114350a8adc147b1c2e0e4"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail35d-201906",
    "A6": "fk=efcdf795059940e588cc65ce779b8fd9",
    "A7": "fk=efcdf795059940e588cc65ce779b8fd9",
    "A8": "fk=efcdf795059940e588cc65ce779b8fd9",
    "A9": "fk=efcdf795059940e588cc65ce779b8fd9"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail35d-201907",
    "A6": "fk=4a4de767b43d444989e38659ca950d50",
    "A7": "fk=4a4de767b43d444989e38659ca950d50",
    "A8": "fk=4a4de767b43d444989e38659ca950d50",
    "A9": "fk=4a4de767b43d444989e38659ca950d50"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail35d-201907",
    "A6": "fk=620c1654cd2f486ba9af7ff45bdaf8b3",
    "A7": "fk=620c1654cd2f486ba9af7ff45bdaf8b3",
    "A8": "fk=620c1654cd2f486ba9af7ff45bdaf8b3",
    "A9": "fk=620c1654cd2f486ba9af7ff45bdaf8b3"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail35d-201907",
    "A6": "fk=7fcce43662964afda3363b90b58262ba",
    "A7": "fk=7fcce43662964afda3363b90b58262ba",
    "A8": "fk=7fcce43662964afda3363b90b58262ba",
    "A9": "fk=7fcce43662964afda3363b90b58262ba"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail35d-201907",
    "A6": "fk=c3b47c44c33f455b992469b87e766f53",
    "A7": "fk=c3b47c44c33f455b992469b87e766f53",
    "A8": "fk=c3b47c44c33f455b992469b87e766f53",
    "A9": "fk=c3b47c44c33f455b992469b87e766f53"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail35d-201907",
    "A6": "fk=e0f49b921ebc4a19b8414bade609f9d9",
    "A7": "fk=e0f49b921ebc4a19b8414bade609f9d9",
    "A8": "fk=e0f49b921ebc4a19b8414bade609f9d9",
    "A9": "fk=e0f49b921ebc4a19b8414bade609f9d9"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=Email",
    "A3": "utm_source=email",
    "A4": "utm_medium=mRenew",
    "A5": "utm_campaign=mRenewmail35d-201907",
    "A6": "fk=fe2f3e06ef5f40a1b7b34ae77ed8c2d1",
    "A7": "fk=fe2f3e06ef5f40a1b7b34ae77ed8c2d1",
    "A8": "fk=fe2f3e06ef5f40a1b7b34ae77ed8c2d1",
    "A9": "fk=fe2f3e06ef5f40a1b7b34ae77ed8c2d1"
  },
  {
    "A1": "utm_source=letter",
    "A2": "utm_medium=pRenew",
    "A3": "utm_campaign=letter-pRenew",
    "A4": "fk=6a33fbeaf200406698b39251afe1c9b8",
    "A5": "fk=6a33fbeaf200406698b39251afe1c9b8",
    "A6": "fk=6a33fbeaf200406698b39251afe1c9b8",
    "A7": "fk=6a33fbeaf200406698b39251afe1c9b8",
    "A8": "fk=6a33fbeaf200406698b39251afe1c9b8",
    "A9": "fk=6a33fbeaf200406698b39251afe1c9b8"
  },
  {
    "A1": "utm_source=letter",
    "A2": "utm_medium=pRenew",
    "A3": "utm_campaign=letter-pRenew",
    "A4": "fk=9c1ef8efcb644f9d8c60fbd6b01970b0",
    "A5": "fk=9c1ef8efcb644f9d8c60fbd6b01970b0",
    "A6": "fk=9c1ef8efcb644f9d8c60fbd6b01970b0",
    "A7": "fk=9c1ef8efcb644f9d8c60fbd6b01970b0",
    "A8": "fk=9c1ef8efcb644f9d8c60fbd6b01970b0",
    "A9": "fk=9c1ef8efcb644f9d8c60fbd6b01970b0"
  },
  {
    "A1": "utm_source=letter",
    "A2": "utm_medium=pRenew",
    "A3": "utm_campaign=letter-pRenew",
    "A4": "fk=bc83e65225104e3cbda8e3681c807573",
    "A5": "fk=bc83e65225104e3cbda8e3681c807573",
    "A6": "fk=bc83e65225104e3cbda8e3681c807573",
    "A7": "fk=bc83e65225104e3cbda8e3681c807573",
    "A8": "fk=bc83e65225104e3cbda8e3681c807573",
    "A9": "fk=bc83e65225104e3cbda8e3681c807573"
  },
  {
    "A1": "utm_source=letter",
    "A2": "utm_medium=pRenew",
    "A3": "utm_campaign=letter-pRenew",
    "A4": "fk=c792eb5a581449a498f5540cfd881152",
    "A5": "fk=c792eb5a581449a498f5540cfd881152",
    "A6": "fk=c792eb5a581449a498f5540cfd881152",
    "A7": "fk=c792eb5a581449a498f5540cfd881152",
    "A8": "fk=c792eb5a581449a498f5540cfd881152",
    "A9": "fk=c792eb5a581449a498f5540cfd881152"
  },
  {
    "A1": "utm_source=letter",
    "A2": "utm_medium=pRenew",
    "A3": "utm_campaign=letter-pRenew",
    "A4": "fk=cd4bb6a75cf34f03ab735271b8706c8c",
    "A5": "fk=cd4bb6a75cf34f03ab735271b8706c8c",
    "A6": "fk=cd4bb6a75cf34f03ab735271b8706c8c",
    "A7": "fk=cd4bb6a75cf34f03ab735271b8706c8c",
    "A8": "fk=cd4bb6a75cf34f03ab735271b8706c8c",
    "A9": "fk=cd4bb6a75cf34f03ab735271b8706c8c"
  },
  {
    "A1": "utm_source=letter",
    "A2": "utm_medium=pRenew",
    "A3": "utm_campaign=letter-pRenew",
    "A4": "fk=f1b1c539ebad4a0dbdb3f5093a4160ef",
    "A5": "fk=f1b1c539ebad4a0dbdb3f5093a4160ef",
    "A6": "fk=f1b1c539ebad4a0dbdb3f5093a4160ef",
    "A7": "fk=f1b1c539ebad4a0dbdb3f5093a4160ef",
    "A8": "fk=f1b1c539ebad4a0dbdb3f5093a4160ef",
    "A9": "fk=f1b1c539ebad4a0dbdb3f5093a4160ef"
  },
  {
    "A1": "iChannels=family",
    "A2": "utm_source=family",
    "A3": "utm_medium=family-referral-icon",
    "A4": "utm_campaign=family-referral-b2b2c-icon",
    "A5": "utm_campaign=family-referral-b2b2c-icon",
    "A6": "utm_campaign=family-referral-b2b2c-icon",
    "A7": "utm_campaign=family-referral-b2b2c-icon",
    "A8": "utm_campaign=family-referral-b2b2c-icon",
    "A9": "utm_campaign=family-referral-b2b2c-icon"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=price",
    "A4": "utm_campaign=email-price",
    "A5": "EstimateId=17N19060400519",
    "A6": "fk=9e8c010c5eba4588aaf3b6a3fb88097c",
    "A7": "fk=9e8c010c5eba4588aaf3b6a3fb88097c",
    "A8": "fk=9e8c010c5eba4588aaf3b6a3fb88097c",
    "A9": "fk=9e8c010c5eba4588aaf3b6a3fb88097c"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=price",
    "A4": "utm_campaign=email-price",
    "A5": "EstimateId=17N19060700392",
    "A6": "fk=11e19a17b57546419a4842f626c7aa48",
    "A7": "fk=11e19a17b57546419a4842f626c7aa48",
    "A8": "fk=11e19a17b57546419a4842f626c7aa48",
    "A9": "fk=11e19a17b57546419a4842f626c7aa48"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=price",
    "A4": "utm_campaign=email-price",
    "A5": "EstimateId=17N19060800185",
    "A6": "fk=be2556318ece432a82d4b30de65e77c6",
    "A7": "fk=be2556318ece432a82d4b30de65e77c6",
    "A8": "fk=be2556318ece432a82d4b30de65e77c6",
    "A9": "fk=be2556318ece432a82d4b30de65e77c6"
  },
  {
    "A1": "InsCategory=Car",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=Car_event_count",
    "A6": "ActivityCode=PCHOME",
    "A7": "fk=75b3e6ae9b2349c48253686ce5df1ead",
    "A8": "fk=75b3e6ae9b2349c48253686ce5df1ead",
    "A9": "fk=75b3e6ae9b2349c48253686ce5df1ead"
  },
  {
    "A1": "InsCategory=Car",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-Carad",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=dac48e7739c74833a2a422644b",
    "A9": "fk=dac48e7739c74833a2a422644b"
  },
  {
    "A1": "InsCategory=Car",
    "A2": "Project=Default",
    "A3": "utm_source=wewanted",
    "A4": "utm_medium=wewanted-helper",
    "A5": "utm_campaign=wewanted-helper",
    "A6": "fk=446f8954cc6d4beeacdf819f41ae1694",
    "A7": "fk=446f8954cc6d4beeacdf819f41ae1694",
    "A8": "fk=446f8954cc6d4beeacdf819f41ae1694",
    "A9": "fk=446f8954cc6d4beeacdf819f41ae1694"
  },
  {
    "A1": "InsCategory=Car",
    "A2": "Project=Default",
    "A3": "utm_source=wewanted",
    "A4": "utm_medium=wewanted-helper",
    "A5": "utm_campaign=wewanted-helper",
    "A6": "fk=44e0e01952d14985a0e38c3400f7ad0e",
    "A7": "fk=44e0e01952d14985a0e38c3400f7ad0e",
    "A8": "fk=44e0e01952d14985a0e38c3400f7ad0e",
    "A9": "fk=44e0e01952d14985a0e38c3400f7ad0e"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=004bfbf2ca7640359a49dab09c88061b",
    "A8": "fk=004bfbf2ca7640359a49dab09c88061b",
    "A9": "fk=004bfbf2ca7640359a49dab09c88061b"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=0129c34609df417db82430c5fc4c8700",
    "A8": "fk=0129c34609df417db82430c5fc4c8700",
    "A9": "fk=0129c34609df417db82430c5fc4c8700"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=01bdf544cb1745b2b61057d1c4a2b15e",
    "A8": "fk=01bdf544cb1745b2b61057d1c4a2b15e",
    "A9": "fk=01bdf544cb1745b2b61057d1c4a2b15e"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=052d5fbd0af34c7f99dd18738d88f265",
    "A8": "fk=052d5fbd0af34c7f99dd18738d88f265",
    "A9": "fk=052d5fbd0af34c7f99dd18738d88f265"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=05c60836176849df9c94be98ef06acf8",
    "A8": "fk=05c60836176849df9c94be98ef06acf8",
    "A9": "fk=05c60836176849df9c94be98ef06acf8"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=0fa3c4a34a2b4d80ba5daf2c5e1af6e8",
    "A8": "fk=0fa3c4a34a2b4d80ba5daf2c5e1af6e8",
    "A9": "fk=0fa3c4a34a2b4d80ba5daf2c5e1af6e8"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=1132b17497a2423198ac980c3d3dfa86",
    "A8": "fk=1132b17497a2423198ac980c3d3dfa86",
    "A9": "fk=1132b17497a2423198ac980c3d3dfa86"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=1160970d8b284a2fad5650c4e7a5ef80",
    "A8": "fk=1160970d8b284a2fad5650c4e7a5ef80",
    "A9": "fk=1160970d8b284a2fad5650c4e7a5ef80"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=1622fc699adc42bc94875f2c2ed426c7",
    "A8": "fk=1622fc699adc42bc94875f2c2ed426c7",
    "A9": "fk=1622fc699adc42bc94875f2c2ed426c7"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=18db068d60944c1dae2ce12c689f6834",
    "A8": "fk=18db068d60944c1dae2ce12c689f6834",
    "A9": "fk=18db068d60944c1dae2ce12c689f6834"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=1c539ca3f3854b6e9f081ac92a6db9a6",
    "A8": "fk=1c539ca3f3854b6e9f081ac92a6db9a6",
    "A9": "fk=1c539ca3f3854b6e9f081ac92a6db9a6"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=1cfa897c3e0e4617818c81231f0d2f2e",
    "A8": "fk=1cfa897c3e0e4617818c81231f0d2f2e",
    "A9": "fk=1cfa897c3e0e4617818c81231f0d2f2e"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=1e1e423890fc47adbe7c1eebc1b335c9",
    "A8": "fk=1e1e423890fc47adbe7c1eebc1b335c9",
    "A9": "fk=1e1e423890fc47adbe7c1eebc1b335c9"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=1e6e22a075704870997d41530378faae",
    "A8": "fk=1e6e22a075704870997d41530378faae",
    "A9": "fk=1e6e22a075704870997d41530378faae"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=1ec773ab929646b8826694b5e08cc1d2",
    "A8": "fk=1ec773ab929646b8826694b5e08cc1d2",
    "A9": "fk=1ec773ab929646b8826694b5e08cc1d2"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=1fa0d9a70b7c4a7a81e968fe289a1248",
    "A8": "fk=1fa0d9a70b7c4a7a81e968fe289a1248",
    "A9": "fk=1fa0d9a70b7c4a7a81e968fe289a1248"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=2320b9a1d1cd4e438380993c3694f034",
    "A8": "fk=2320b9a1d1cd4e438380993c3694f034",
    "A9": "fk=2320b9a1d1cd4e438380993c3694f034"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=267ed4ff411e464f827f8110dc4ae4a7",
    "A8": "fk=267ed4ff411e464f827f8110dc4ae4a7",
    "A9": "fk=267ed4ff411e464f827f8110dc4ae4a7"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=28d539a553574c5e9ee18a6156a21bb0",
    "A8": "fk=28d539a553574c5e9ee18a6156a21bb0",
    "A9": "fk=28d539a553574c5e9ee18a6156a21bb0"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=2976118f858a4dbb843ceccc732a0d73",
    "A8": "fk=2976118f858a4dbb843ceccc732a0d73",
    "A9": "fk=2976118f858a4dbb843ceccc732a0d73"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=2a58703d0f444cdab746b18ee90ddcad",
    "A8": "fk=2a58703d0f444cdab746b18ee90ddcad",
    "A9": "fk=2a58703d0f444cdab746b18ee90ddcad"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=2b1e41c393d446338456dd8407c16ba1",
    "A8": "fk=2b1e41c393d446338456dd8407c16ba1",
    "A9": "fk=2b1e41c393d446338456dd8407c16ba1"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=2b3f5d0063e8436597590d54e57fb423",
    "A8": "fk=2b3f5d0063e8436597590d54e57fb423",
    "A9": "fk=2b3f5d0063e8436597590d54e57fb423"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=2d560e32c0864d64a1b0ca23a52bec0d",
    "A8": "fk=2d560e32c0864d64a1b0ca23a52bec0d",
    "A9": "fk=2d560e32c0864d64a1b0ca23a52bec0d"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=2e5dab1ff3534635bd8284cdc8ec21b6",
    "A8": "fk=2e5dab1ff3534635bd8284cdc8ec21b6",
    "A9": "fk=2e5dab1ff3534635bd8284cdc8ec21b6"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=2eb9d34ae2234ee3a5c682d0f18b5492",
    "A8": "fk=2eb9d34ae2234ee3a5c682d0f18b5492",
    "A9": "fk=2eb9d34ae2234ee3a5c682d0f18b5492"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=2f00aa30a1c54c4aa8202d9917ada80b",
    "A8": "fk=2f00aa30a1c54c4aa8202d9917ada80b",
    "A9": "fk=2f00aa30a1c54c4aa8202d9917ada80b"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=3094ed4633af48a98ea9e0598e423d1d",
    "A8": "fk=3094ed4633af48a98ea9e0598e423d1d",
    "A9": "fk=3094ed4633af48a98ea9e0598e423d1d"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=3168793e1ec0485596ca2ab2ab1e0912",
    "A8": "fk=3168793e1ec0485596ca2ab2ab1e0912",
    "A9": "fk=3168793e1ec0485596ca2ab2ab1e0912"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=3556b8525b7a4f26ab3e18cc433b84a0",
    "A8": "fk=3556b8525b7a4f26ab3e18cc433b84a0",
    "A9": "fk=3556b8525b7a4f26ab3e18cc433b84a0"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=38a5bd89fea043f4a27fe4fcc19f3c23",
    "A8": "fk=38a5bd89fea043f4a27fe4fcc19f3c23",
    "A9": "fk=38a5bd89fea043f4a27fe4fcc19f3c23"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=38d15a3177fe478ba834896f9f0ae227",
    "A8": "fk=38d15a3177fe478ba834896f9f0ae227",
    "A9": "fk=38d15a3177fe478ba834896f9f0ae227"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=38dd688ba3ae458dab6ca68b915dcba4",
    "A8": "fk=38dd688ba3ae458dab6ca68b915dcba4",
    "A9": "fk=38dd688ba3ae458dab6ca68b915dcba4"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=3d539287706d409c8e66d65b0399e0c4",
    "A8": "fk=3d539287706d409c8e66d65b0399e0c4",
    "A9": "fk=3d539287706d409c8e66d65b0399e0c4"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=4019f4059e0b44a19e47c938d67375e6",
    "A8": "fk=4019f4059e0b44a19e47c938d67375e6",
    "A9": "fk=4019f4059e0b44a19e47c938d67375e6"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=41af504bf11946f28f0fc361e47e532c",
    "A8": "fk=41af504bf11946f28f0fc361e47e532c",
    "A9": "fk=41af504bf11946f28f0fc361e47e532c"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=43fed74821e84467ad43808c36cee8df",
    "A8": "fk=43fed74821e84467ad43808c36cee8df",
    "A9": "fk=43fed74821e84467ad43808c36cee8df"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=450e42c84a7141d28f2fb37b6e86dfa1",
    "A8": "fk=450e42c84a7141d28f2fb37b6e86dfa1",
    "A9": "fk=450e42c84a7141d28f2fb37b6e86dfa1"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=48010f788f0a4c3d8c246f5f71836057",
    "A8": "fk=48010f788f0a4c3d8c246f5f71836057",
    "A9": "fk=48010f788f0a4c3d8c246f5f71836057"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=4869d58dc0144034843596b5f294890f",
    "A8": "fk=4869d58dc0144034843596b5f294890f",
    "A9": "fk=4869d58dc0144034843596b5f294890f"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=51d1a62328cc4501a64c594730a8a645",
    "A8": "fk=51d1a62328cc4501a64c594730a8a645",
    "A9": "fk=51d1a62328cc4501a64c594730a8a645"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=5433c8683eff45ac99903c32849f962d",
    "A8": "fk=5433c8683eff45ac99903c32849f962d",
    "A9": "fk=5433c8683eff45ac99903c32849f962d"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=545606eb5d4f40caa4045bb2ee8467f8",
    "A8": "fk=545606eb5d4f40caa4045bb2ee8467f8",
    "A9": "fk=545606eb5d4f40caa4045bb2ee8467f8"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=54f2637d407d4134a787452beaf61ee2",
    "A8": "fk=54f2637d407d4134a787452beaf61ee2",
    "A9": "fk=54f2637d407d4134a787452beaf61ee2"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=55469b86ceb84195bd7749e43f037d47",
    "A8": "fk=55469b86ceb84195bd7749e43f037d47",
    "A9": "fk=55469b86ceb84195bd7749e43f037d47"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=59116e145915406db9c5c785565b572d",
    "A8": "fk=59116e145915406db9c5c785565b572d",
    "A9": "fk=59116e145915406db9c5c785565b572d"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=59444685dfe64d9b9f7fdeff56d8a8cc",
    "A8": "fk=59444685dfe64d9b9f7fdeff56d8a8cc",
    "A9": "fk=59444685dfe64d9b9f7fdeff56d8a8cc"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=59f5477646254d9b9c158d61b330ad9e",
    "A8": "fk=59f5477646254d9b9c158d61b330ad9e",
    "A9": "fk=59f5477646254d9b9c158d61b330ad9e"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=59fe6022c6d94842b141ab8cd6b1b472",
    "A8": "fk=59fe6022c6d94842b141ab8cd6b1b472",
    "A9": "fk=59fe6022c6d94842b141ab8cd6b1b472"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=5b7f0b69068b4265b1968c429916b5f4",
    "A8": "fk=5b7f0b69068b4265b1968c429916b5f4",
    "A9": "fk=5b7f0b69068b4265b1968c429916b5f4"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=5d05a8e6485a476188acdef20c1006d6",
    "A8": "fk=5d05a8e6485a476188acdef20c1006d6",
    "A9": "fk=5d05a8e6485a476188acdef20c1006d6"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=5e2832d548574eda9921743070991c0f",
    "A8": "fk=5e2832d548574eda9921743070991c0f",
    "A9": "fk=5e2832d548574eda9921743070991c0f"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=60986c6b971942e58df8bcd89c8cbd00",
    "A8": "fk=60986c6b971942e58df8bcd89c8cbd00",
    "A9": "fk=60986c6b971942e58df8bcd89c8cbd00"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=636d70bfc05b47fdbb9c45cd5016b748",
    "A8": "fk=636d70bfc05b47fdbb9c45cd5016b748",
    "A9": "fk=636d70bfc05b47fdbb9c45cd5016b748"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=6486152643d14da398f17dd5d512d3a0",
    "A8": "fk=6486152643d14da398f17dd5d512d3a0",
    "A9": "fk=6486152643d14da398f17dd5d512d3a0"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=69bfc5e7f1b7421480098be7c0e10f72",
    "A8": "fk=69bfc5e7f1b7421480098be7c0e10f72",
    "A9": "fk=69bfc5e7f1b7421480098be7c0e10f72"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=6b3d60b9c84c482bb18ae30b2c42aae4",
    "A8": "fk=6b3d60b9c84c482bb18ae30b2c42aae4",
    "A9": "fk=6b3d60b9c84c482bb18ae30b2c42aae4"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=6c62688698114e9fa93ee7426a1a6585",
    "A8": "fk=6c62688698114e9fa93ee7426a1a6585",
    "A9": "fk=6c62688698114e9fa93ee7426a1a6585"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=713d245e49a14a65ac575655ed93c52f",
    "A8": "fk=713d245e49a14a65ac575655ed93c52f",
    "A9": "fk=713d245e49a14a65ac575655ed93c52f"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=71a67c1125a64cae9fa49a5d73fbcb4f",
    "A8": "fk=71a67c1125a64cae9fa49a5d73fbcb4f",
    "A9": "fk=71a67c1125a64cae9fa49a5d73fbcb4f"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=71c75ac1b2914e1e9ca822b1fac62abc",
    "A8": "fk=71c75ac1b2914e1e9ca822b1fac62abc",
    "A9": "fk=71c75ac1b2914e1e9ca822b1fac62abc"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=72e4e305975e4eb9881cb991d7a5577e",
    "A8": "fk=72e4e305975e4eb9881cb991d7a5577e",
    "A9": "fk=72e4e305975e4eb9881cb991d7a5577e"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=7a1f157da06f4730b429c616a01691ea",
    "A8": "fk=7a1f157da06f4730b429c616a01691ea",
    "A9": "fk=7a1f157da06f4730b429c616a01691ea"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=7ebab44710ae4800ad839fb33a5a4a8e",
    "A8": "fk=7ebab44710ae4800ad839fb33a5a4a8e",
    "A9": "fk=7ebab44710ae4800ad839fb33a5a4a8e"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=7fd0afe77b624016b07be36f34387642",
    "A8": "fk=7fd0afe77b624016b07be36f34387642",
    "A9": "fk=7fd0afe77b624016b07be36f34387642"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=8157e782841041d5abbdea096f2da6b5",
    "A8": "fk=8157e782841041d5abbdea096f2da6b5",
    "A9": "fk=8157e782841041d5abbdea096f2da6b5"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=82fae3e443784b3c8c8133c12aa17cb9",
    "A8": "fk=82fae3e443784b3c8c8133c12aa17cb9",
    "A9": "fk=82fae3e443784b3c8c8133c12aa17cb9"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=835ec1b676df4f5fb3a3a5e0ec5e1623",
    "A8": "fk=835ec1b676df4f5fb3a3a5e0ec5e1623",
    "A9": "fk=835ec1b676df4f5fb3a3a5e0ec5e1623"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=83d2ced966de4093b80e35e66bcac8c3",
    "A8": "fk=83d2ced966de4093b80e35e66bcac8c3",
    "A9": "fk=83d2ced966de4093b80e35e66bcac8c3"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=846cc0830a014d4094e679c4945ef960",
    "A8": "fk=846cc0830a014d4094e679c4945ef960",
    "A9": "fk=846cc0830a014d4094e679c4945ef960"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=854f173d0ac54379b75b99871fe6e626",
    "A8": "fk=854f173d0ac54379b75b99871fe6e626",
    "A9": "fk=854f173d0ac54379b75b99871fe6e626"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=85ccdd5e6e8d4a17a8a7d2e585dbde4e",
    "A8": "fk=85ccdd5e6e8d4a17a8a7d2e585dbde4e",
    "A9": "fk=85ccdd5e6e8d4a17a8a7d2e585dbde4e"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=868f56a38b9948f6add5230c7ecb2457",
    "A8": "fk=868f56a38b9948f6add5230c7ecb2457",
    "A9": "fk=868f56a38b9948f6add5230c7ecb2457"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=86d9a1a4bb77469197b102497602c3a4",
    "A8": "fk=86d9a1a4bb77469197b102497602c3a4",
    "A9": "fk=86d9a1a4bb77469197b102497602c3a4"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=86e325eedd394ac299406f0a18736df1",
    "A8": "fk=86e325eedd394ac299406f0a18736df1",
    "A9": "fk=86e325eedd394ac299406f0a18736df1"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=8722c05882224eaa9402d1acbc1411b6",
    "A8": "fk=8722c05882224eaa9402d1acbc1411b6",
    "A9": "fk=8722c05882224eaa9402d1acbc1411b6"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=8c1171af3d804c48be5aeb6820b23757",
    "A8": "fk=8c1171af3d804c48be5aeb6820b23757",
    "A9": "fk=8c1171af3d804c48be5aeb6820b23757"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=8e794876872741fba2cebc3b8bc8139b",
    "A8": "fk=8e794876872741fba2cebc3b8bc8139b",
    "A9": "fk=8e794876872741fba2cebc3b8bc8139b"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=8e9f25fe8c2f4a528ffe855375cc6823",
    "A8": "fk=8e9f25fe8c2f4a528ffe855375cc6823",
    "A9": "fk=8e9f25fe8c2f4a528ffe855375cc6823"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=90be01dfc33a4383ae5960d1cffd8d8b",
    "A8": "fk=90be01dfc33a4383ae5960d1cffd8d8b",
    "A9": "fk=90be01dfc33a4383ae5960d1cffd8d8b"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=930da87945c041ff925718a4c07883b1",
    "A8": "fk=930da87945c041ff925718a4c07883b1",
    "A9": "fk=930da87945c041ff925718a4c07883b1"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=930ea1615303482fbdc161fb4f37de0a",
    "A8": "fk=930ea1615303482fbdc161fb4f37de0a",
    "A9": "fk=930ea1615303482fbdc161fb4f37de0a"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=93aa590232184b429b8341aafc3d145a",
    "A8": "fk=93aa590232184b429b8341aafc3d145a",
    "A9": "fk=93aa590232184b429b8341aafc3d145a"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=961185b256af46a795e07621f994b170",
    "A8": "fk=961185b256af46a795e07621f994b170",
    "A9": "fk=961185b256af46a795e07621f994b170"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=9e087556878446c2886dc69960c14c6f",
    "A8": "fk=9e087556878446c2886dc69960c14c6f",
    "A9": "fk=9e087556878446c2886dc69960c14c6f"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=a1536b30e69d4843b979829ba44799a1",
    "A8": "fk=a1536b30e69d4843b979829ba44799a1",
    "A9": "fk=a1536b30e69d4843b979829ba44799a1"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=a2b8eac83eb643719f9b80f6ca1f396b",
    "A8": "fk=a2b8eac83eb643719f9b80f6ca1f396b",
    "A9": "fk=a2b8eac83eb643719f9b80f6ca1f396b"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=a2c442b1bb524eada37abfda681f15fd",
    "A8": "fk=a2c442b1bb524eada37abfda681f15fd",
    "A9": "fk=a2c442b1bb524eada37abfda681f15fd"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=a3c1305d312c4ccdb7743f62ebab7e0e",
    "A8": "fk=a3c1305d312c4ccdb7743f62ebab7e0e",
    "A9": "fk=a3c1305d312c4ccdb7743f62ebab7e0e"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=a546d7a4f58748febac30d4a4b98f8a9",
    "A8": "fk=a546d7a4f58748febac30d4a4b98f8a9",
    "A9": "fk=a546d7a4f58748febac30d4a4b98f8a9"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=a62e7cd4b3ac458b8332b45cd013cf42",
    "A8": "fk=a62e7cd4b3ac458b8332b45cd013cf42",
    "A9": "fk=a62e7cd4b3ac458b8332b45cd013cf42"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=a72152073b7a415ea0ca72bd13d361de",
    "A8": "fk=a72152073b7a415ea0ca72bd13d361de",
    "A9": "fk=a72152073b7a415ea0ca72bd13d361de"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=a72a4c3099804e46a6a5ae9685aa513b",
    "A8": "fk=a72a4c3099804e46a6a5ae9685aa513b",
    "A9": "fk=a72a4c3099804e46a6a5ae9685aa513b"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=a7d8fd7f46c547afa29ec1518867875a",
    "A8": "fk=a7d8fd7f46c547afa29ec1518867875a",
    "A9": "fk=a7d8fd7f46c547afa29ec1518867875a"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=a8e1e38039344a1c90f4dc69ede052ef",
    "A8": "fk=a8e1e38039344a1c90f4dc69ede052ef",
    "A9": "fk=a8e1e38039344a1c90f4dc69ede052ef"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=a91d731b7f9d418fa2dcfc8e5c19e5f1",
    "A8": "fk=a91d731b7f9d418fa2dcfc8e5c19e5f1",
    "A9": "fk=a91d731b7f9d418fa2dcfc8e5c19e5f1"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=abaa43499a194b0d8593645bb86dc1b1",
    "A8": "fk=abaa43499a194b0d8593645bb86dc1b1",
    "A9": "fk=abaa43499a194b0d8593645bb86dc1b1"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=ae40785fb9db45d4b20666e9fcb23d2c",
    "A8": "fk=ae40785fb9db45d4b20666e9fcb23d2c",
    "A9": "fk=ae40785fb9db45d4b20666e9fcb23d2c"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=aefcdb74b974449eb9e740ac59ca27a1",
    "A8": "fk=aefcdb74b974449eb9e740ac59ca27a1",
    "A9": "fk=aefcdb74b974449eb9e740ac59ca27a1"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=af95b7ce0a88492bbfc7faf3b6521067",
    "A8": "fk=af95b7ce0a88492bbfc7faf3b6521067",
    "A9": "fk=af95b7ce0a88492bbfc7faf3b6521067"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=b309b2cc6204407e88baed970f7f89a4",
    "A8": "fk=b309b2cc6204407e88baed970f7f89a4",
    "A9": "fk=b309b2cc6204407e88baed970f7f89a4"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=b3f5a29973f04986b5fbb95da93e6c1c",
    "A8": "fk=b3f5a29973f04986b5fbb95da93e6c1c",
    "A9": "fk=b3f5a29973f04986b5fbb95da93e6c1c"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=b4dc885f2a884c35a272c2219779d197",
    "A8": "fk=b4dc885f2a884c35a272c2219779d197",
    "A9": "fk=b4dc885f2a884c35a272c2219779d197"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=b5277109fa794a8cb17964acf202e275",
    "A8": "fk=b5277109fa794a8cb17964acf202e275",
    "A9": "fk=b5277109fa794a8cb17964acf202e275"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=b64c008269634098a9865f32470b7ba1",
    "A8": "fk=b64c008269634098a9865f32470b7ba1",
    "A9": "fk=b64c008269634098a9865f32470b7ba1"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=b7073102a08747cfaae7b7d0cea6c768",
    "A8": "fk=b7073102a08747cfaae7b7d0cea6c768",
    "A9": "fk=b7073102a08747cfaae7b7d0cea6c768"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=b713f729b53e4bc2abdbf938da7309d6",
    "A8": "fk=b713f729b53e4bc2abdbf938da7309d6",
    "A9": "fk=b713f729b53e4bc2abdbf938da7309d6"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=b94669c83442448dae8db427119fc693",
    "A8": "fk=b94669c83442448dae8db427119fc693",
    "A9": "fk=b94669c83442448dae8db427119fc693"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=ba1fcac9ecd54e9e97b650925c48e9d1",
    "A8": "fk=ba1fcac9ecd54e9e97b650925c48e9d1",
    "A9": "fk=ba1fcac9ecd54e9e97b650925c48e9d1"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=bb1a31cb39b3422798047e48cc51eccb",
    "A8": "fk=bb1a31cb39b3422798047e48cc51eccb",
    "A9": "fk=bb1a31cb39b3422798047e48cc51eccb"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=bb4f231c3e1f4df8b3abffdb03db62ef",
    "A8": "fk=bb4f231c3e1f4df8b3abffdb03db62ef",
    "A9": "fk=bb4f231c3e1f4df8b3abffdb03db62ef"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=bf4afb9db1e3442cb4d981ff9206d872",
    "A8": "fk=bf4afb9db1e3442cb4d981ff9206d872",
    "A9": "fk=bf4afb9db1e3442cb4d981ff9206d872"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=c22670e2216442239d976787a6aad2cc",
    "A8": "fk=c22670e2216442239d976787a6aad2cc",
    "A9": "fk=c22670e2216442239d976787a6aad2cc"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=c2b0509c2ef14ed59dff1bbc1aed5226",
    "A8": "fk=c2b0509c2ef14ed59dff1bbc1aed5226",
    "A9": "fk=c2b0509c2ef14ed59dff1bbc1aed5226"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=c59bdfc210b5486b9a907c8325a37e55",
    "A8": "fk=c59bdfc210b5486b9a907c8325a37e55",
    "A9": "fk=c59bdfc210b5486b9a907c8325a37e55"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=c9e8df256aff46758f53925d4084eb67",
    "A8": "fk=c9e8df256aff46758f53925d4084eb67",
    "A9": "fk=c9e8df256aff46758f53925d4084eb67"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=cbec4163035147bbb68b817b4942c9b2",
    "A8": "fk=cbec4163035147bbb68b817b4942c9b2",
    "A9": "fk=cbec4163035147bbb68b817b4942c9b2"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=cc082176337c46bf9d5218f0d4ee8df3",
    "A8": "fk=cc082176337c46bf9d5218f0d4ee8df3",
    "A9": "fk=cc082176337c46bf9d5218f0d4ee8df3"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=ccabfc617c394db68397790c33a30c9b",
    "A8": "fk=ccabfc617c394db68397790c33a30c9b",
    "A9": "fk=ccabfc617c394db68397790c33a30c9b"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=cf7dafc0267f4b338a25d2df8db58150",
    "A8": "fk=cf7dafc0267f4b338a25d2df8db58150",
    "A9": "fk=cf7dafc0267f4b338a25d2df8db58150"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=d03fc4956abd409d802c56529bd6a91b",
    "A8": "fk=d03fc4956abd409d802c56529bd6a91b",
    "A9": "fk=d03fc4956abd409d802c56529bd6a91b"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=d2581f4f791a48ceb06abbefb545aae3",
    "A8": "fk=d2581f4f791a48ceb06abbefb545aae3",
    "A9": "fk=d2581f4f791a48ceb06abbefb545aae3"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=d292639690614b929dc861a57b202bed",
    "A8": "fk=d292639690614b929dc861a57b202bed",
    "A9": "fk=d292639690614b929dc861a57b202bed"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=d3b72eaeddab40349830d59793ee9d7c",
    "A8": "fk=d3b72eaeddab40349830d59793ee9d7c",
    "A9": "fk=d3b72eaeddab40349830d59793ee9d7c"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=d91a3739da98493a8b90e3be69c39659",
    "A8": "fk=d91a3739da98493a8b90e3be69c39659",
    "A9": "fk=d91a3739da98493a8b90e3be69c39659"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=d98fc13d29c7442c9ce14514880337cb",
    "A8": "fk=d98fc13d29c7442c9ce14514880337cb",
    "A9": "fk=d98fc13d29c7442c9ce14514880337cb"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=d9e636c46d684919be760d0d062f8a50",
    "A8": "fk=d9e636c46d684919be760d0d062f8a50",
    "A9": "fk=d9e636c46d684919be760d0d062f8a50"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=da0e579bfe4440c2991676363c4f56d6",
    "A8": "fk=da0e579bfe4440c2991676363c4f56d6",
    "A9": "fk=da0e579bfe4440c2991676363c4f56d6"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=daa2f002fb9f4830b34f8591b8d4abfa",
    "A8": "fk=daa2f002fb9f4830b34f8591b8d4abfa",
    "A9": "fk=daa2f002fb9f4830b34f8591b8d4abfa"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=dab43b9e43ef4e45b0b9c4bace43617d",
    "A8": "fk=dab43b9e43ef4e45b0b9c4bace43617d",
    "A9": "fk=dab43b9e43ef4e45b0b9c4bace43617d"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=dc72ede7454d43b382b34541754aaf83",
    "A8": "fk=dc72ede7454d43b382b34541754aaf83",
    "A9": "fk=dc72ede7454d43b382b34541754aaf83"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=de348a4830fb4efca284da66668863f2",
    "A8": "fk=de348a4830fb4efca284da66668863f2",
    "A9": "fk=de348a4830fb4efca284da66668863f2"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=de8773a49a3d4cc395d55075b21b948a",
    "A8": "fk=de8773a49a3d4cc395d55075b21b948a",
    "A9": "fk=de8773a49a3d4cc395d55075b21b948a"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=de9f76bcc2a44ec3baff558386e940cb",
    "A8": "fk=de9f76bcc2a44ec3baff558386e940cb",
    "A9": "fk=de9f76bcc2a44ec3baff558386e940cb"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=df087e4583384664869b43eee9cb8d2a",
    "A8": "fk=df087e4583384664869b43eee9cb8d2a",
    "A9": "fk=df087e4583384664869b43eee9cb8d2a"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=e0832d86f71d4d1583dbaa77d03e45c6",
    "A8": "fk=e0832d86f71d4d1583dbaa77d03e45c6",
    "A9": "fk=e0832d86f71d4d1583dbaa77d03e45c6"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=e4e7fa4ef2e74ae3b576c18b22cec530",
    "A8": "fk=e4e7fa4ef2e74ae3b576c18b22cec530",
    "A9": "fk=e4e7fa4ef2e74ae3b576c18b22cec530"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=e5a6ac12360948cca8d753a2033e7610",
    "A8": "fk=e5a6ac12360948cca8d753a2033e7610",
    "A9": "fk=e5a6ac12360948cca8d753a2033e7610"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=e63070b716384eb28e63eaed656bd9ba",
    "A8": "fk=e63070b716384eb28e63eaed656bd9ba",
    "A9": "fk=e63070b716384eb28e63eaed656bd9ba"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=e69f210a5ed5402dbaf0ad4c1fc8710b",
    "A8": "fk=e69f210a5ed5402dbaf0ad4c1fc8710b",
    "A9": "fk=e69f210a5ed5402dbaf0ad4c1fc8710b"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=e6e20bb33ec94add9d511ee0619ad7f5",
    "A8": "fk=e6e20bb33ec94add9d511ee0619ad7f5",
    "A9": "fk=e6e20bb33ec94add9d511ee0619ad7f5"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=eab415855f65449aa8ea2d0a299b9167",
    "A8": "fk=eab415855f65449aa8ea2d0a299b9167",
    "A9": "fk=eab415855f65449aa8ea2d0a299b9167"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=eb4d5c62ed2941b0be9643db61aba2e7",
    "A8": "fk=eb4d5c62ed2941b0be9643db61aba2e7",
    "A9": "fk=eb4d5c62ed2941b0be9643db61aba2e7"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=ec23f86a6aca456fb9d32090cd918078",
    "A8": "fk=ec23f86a6aca456fb9d32090cd918078",
    "A9": "fk=ec23f86a6aca456fb9d32090cd918078"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=ed83e179ef314e93b9cca4dc181e35ae",
    "A8": "fk=ed83e179ef314e93b9cca4dc181e35ae",
    "A9": "fk=ed83e179ef314e93b9cca4dc181e35ae"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=ed92a94c76204a06b7cec48f05e8a179",
    "A8": "fk=ed92a94c76204a06b7cec48f05e8a179",
    "A9": "fk=ed92a94c76204a06b7cec48f05e8a179"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=eefe0921c9fb46149baf698f89be7bc6",
    "A8": "fk=eefe0921c9fb46149baf698f89be7bc6",
    "A9": "fk=eefe0921c9fb46149baf698f89be7bc6"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=f20e989e1f244ce3ab9012fc877a85f1",
    "A8": "fk=f20e989e1f244ce3ab9012fc877a85f1",
    "A9": "fk=f20e989e1f244ce3ab9012fc877a85f1"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=f4c04039910f4846b07ddecf866856cd",
    "A8": "fk=f4c04039910f4846b07ddecf866856cd",
    "A9": "fk=f4c04039910f4846b07ddecf866856cd"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=f826bfc117f74fa7919dfffc9c5f8a72",
    "A8": "fk=f826bfc117f74fa7919dfffc9c5f8a72",
    "A9": "fk=f826bfc117f74fa7919dfffc9c5f8a72"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=f8b90ae6ab8c47dc81fb0a4ac488cefa",
    "A8": "fk=f8b90ae6ab8c47dc81fb0a4ac488cefa",
    "A9": "fk=f8b90ae6ab8c47dc81fb0a4ac488cefa"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=f8edaf88007443f986b0770df8d94ac0",
    "A8": "fk=f8edaf88007443f986b0770df8d94ac0",
    "A9": "fk=f8edaf88007443f986b0770df8d94ac0"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=f9743bd4657d4b2abd8e17313c833e3f",
    "A8": "fk=f9743bd4657d4b2abd8e17313c833e3f",
    "A9": "fk=f9743bd4657d4b2abd8e17313c833e3f"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=f98b2567d8604fc5a263c04b1d9394e6",
    "A8": "fk=f98b2567d8604fc5a263c04b1d9394e6",
    "A9": "fk=f98b2567d8604fc5a263c04b1d9394e6"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=fb2e12ae50c7485eb2fce3b9a01c3752",
    "A8": "fk=fb2e12ae50c7485eb2fce3b9a01c3752",
    "A9": "fk=fb2e12ae50c7485eb2fce3b9a01c3752"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=fcedf7b3c7a349d298be1c28b29409d1",
    "A8": "fk=fcedf7b3c7a349d298be1c28b29409d1",
    "A9": "fk=fcedf7b3c7a349d298be1c28b29409d1"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=fed524dee4234c96bad4f50263af18b5",
    "A8": "fk=fed524dee4234c96bad4f50263af18b5",
    "A9": "fk=fed524dee4234c96bad4f50263af18b5"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "fk",
    "A4": "utm_source=urAD",
    "A5": "utm_medium=GDN",
    "A6": "utm_campaign=moto",
    "A7": "fk=ff4933b843604dc180d3430d2684b922",
    "A8": "fk=ff4933b843604dc180d3430d2684b922",
    "A9": "fk=ff4933b843604dc180d3430d2684b922"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=01429d83a90",
    "A9": "fk=01429d83a90"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=02f957afcee",
    "A9": "fk=02f957afcee"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=0c65ef454b9",
    "A9": "fk=0c65ef454b9"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=0c7432db4d8",
    "A9": "fk=0c7432db4d8"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=0df899102b8",
    "A9": "fk=0df899102b8"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=13b0ff56aaf",
    "A9": "fk=13b0ff56aaf"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=170396d57af",
    "A9": "fk=170396d57af"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=1a6d4a3e627",
    "A9": "fk=1a6d4a3e627"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=1a7e7298775",
    "A9": "fk=1a7e7298775"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=1dfb0cfd2b0",
    "A9": "fk=1dfb0cfd2b0"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=1f3f13880e7",
    "A9": "fk=1f3f13880e7"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=20563fdb159",
    "A9": "fk=20563fdb159"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=21e9ad3a861",
    "A9": "fk=21e9ad3a861"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=2ad27d55ac9",
    "A9": "fk=2ad27d55ac9"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=2c16d2f26e8",
    "A9": "fk=2c16d2f26e8"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=2d14484c90f",
    "A9": "fk=2d14484c90f"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=31efdfc681c",
    "A9": "fk=31efdfc681c"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=33653210578",
    "A9": "fk=33653210578"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=35d84539691",
    "A9": "fk=35d84539691"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=3866df0136a",
    "A9": "fk=3866df0136a"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=3c243b79607",
    "A9": "fk=3c243b79607"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=3cc16dd3be8",
    "A9": "fk=3cc16dd3be8"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=408bdb937a4",
    "A9": "fk=408bdb937a4"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=438e60d23f1",
    "A9": "fk=438e60d23f1"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=45a2c378d8e",
    "A9": "fk=45a2c378d8e"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=46b96a978dc",
    "A9": "fk=46b96a978dc"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=4a1c8e14f82",
    "A9": "fk=4a1c8e14f82"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=4b81dabf974",
    "A9": "fk=4b81dabf974"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=4ca7b995bdc",
    "A9": "fk=4ca7b995bdc"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=4eaaa90ce1f",
    "A9": "fk=4eaaa90ce1f"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=5015e89398a",
    "A9": "fk=5015e89398a"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=57b2d5988ed",
    "A9": "fk=57b2d5988ed"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=59936aeafd0",
    "A9": "fk=59936aeafd0"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=5a22df8903e",
    "A9": "fk=5a22df8903e"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=5a8e6834a22",
    "A9": "fk=5a8e6834a22"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=5cf8b43ac7e",
    "A9": "fk=5cf8b43ac7e"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=5f1b862f492",
    "A9": "fk=5f1b862f492"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=600c035b505",
    "A9": "fk=600c035b505"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=600f8664066",
    "A9": "fk=600f8664066"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=64d7be0400e",
    "A9": "fk=64d7be0400e"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=67fb00c6e8b",
    "A9": "fk=67fb00c6e8b"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=6c3273ae7de",
    "A9": "fk=6c3273ae7de"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=6c6f1ec569f",
    "A9": "fk=6c6f1ec569f"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=6d915946178",
    "A9": "fk=6d915946178"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=6e0b745c564",
    "A9": "fk=6e0b745c564"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=6e49afc7632",
    "A9": "fk=6e49afc7632"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=73f0c03788d",
    "A9": "fk=73f0c03788d"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=7692d1c24d5",
    "A9": "fk=7692d1c24d5"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=7766da606a1",
    "A9": "fk=7766da606a1"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=78090afb192",
    "A9": "fk=78090afb192"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=78dd0299c7d",
    "A9": "fk=78dd0299c7d"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=7ab7a866b46",
    "A9": "fk=7ab7a866b46"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=7e213a10238",
    "A9": "fk=7e213a10238"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=8068efca414",
    "A9": "fk=8068efca414"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=820045f0191",
    "A9": "fk=820045f0191"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=85784e0bb61",
    "A9": "fk=85784e0bb61"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=86173dfdbe9",
    "A9": "fk=86173dfdbe9"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=8e3f8fedae3",
    "A9": "fk=8e3f8fedae3"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=902522a8f3a",
    "A9": "fk=902522a8f3a"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=904f8edab67",
    "A9": "fk=904f8edab67"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=92804922357",
    "A9": "fk=92804922357"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=97ba9692b36",
    "A9": "fk=97ba9692b36"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=9c5cead0ac3",
    "A9": "fk=9c5cead0ac3"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=9c8daafb55d",
    "A9": "fk=9c8daafb55d"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=9e62c696a5b",
    "A9": "fk=9e62c696a5b"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=a42f917c649",
    "A9": "fk=a42f917c649"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=a488708b507",
    "A9": "fk=a488708b507"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=a640703e52e",
    "A9": "fk=a640703e52e"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=a89f1e4e8e5",
    "A9": "fk=a89f1e4e8e5"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=a8a6b3662b9",
    "A9": "fk=a8a6b3662b9"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=a964adb2fec",
    "A9": "fk=a964adb2fec"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=aadf8af4f2d",
    "A9": "fk=aadf8af4f2d"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=ab9afdcb33d",
    "A9": "fk=ab9afdcb33d"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=ac7ddb6e89f",
    "A9": "fk=ac7ddb6e89f"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=acb359e5a25",
    "A9": "fk=acb359e5a25"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=acb8c2b4517",
    "A9": "fk=acb8c2b4517"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=ad16a7d7f09",
    "A9": "fk=ad16a7d7f09"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=b06a7c5e8cc",
    "A9": "fk=b06a7c5e8cc"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=b592ad77b50",
    "A9": "fk=b592ad77b50"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=bf8aa56fc71",
    "A9": "fk=bf8aa56fc71"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=c397d213447",
    "A9": "fk=c397d213447"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=cbd83b2ea79",
    "A9": "fk=cbd83b2ea79"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=cf2f9114223",
    "A9": "fk=cf2f9114223"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=d6f722f7e91",
    "A9": "fk=d6f722f7e91"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=d762adbca35",
    "A9": "fk=d762adbca35"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=da502ae2826",
    "A9": "fk=da502ae2826"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=df8e838c854",
    "A9": "fk=df8e838c854"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=dfd141b6a19",
    "A9": "fk=dfd141b6a19"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=dfd8e4774ad",
    "A9": "fk=dfd8e4774ad"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=e47f4061d8d",
    "A9": "fk=e47f4061d8d"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=e6123ce7841",
    "A9": "fk=e6123ce7841"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=e8f076b28ce",
    "A9": "fk=e8f076b28ce"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=e94c5c9f710",
    "A9": "fk=e94c5c9f710"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=ea68b5a199d",
    "A9": "fk=ea68b5a199d"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=ee1fe921c32",
    "A9": "fk=ee1fe921c32"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=eef44cdbf47",
    "A9": "fk=eef44cdbf47"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=f1e897862ed",
    "A9": "fk=f1e897862ed"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=f2bacd5d8bd",
    "A9": "fk=f2bacd5d8bd"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=f2ce794ce2e",
    "A9": "fk=f2ce794ce2e"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=f3490875fb3",
    "A9": "fk=f3490875fb3"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=f3c3be61234",
    "A9": "fk=f3c3be61234"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=f68b77603ce",
    "A9": "fk=f68b77603ce"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=f7a6f525267",
    "A9": "fk=f7a6f525267"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=fc333765527",
    "A9": "fk=fc333765527"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "Project=Default",
    "A3": "utm_source=PC_home",
    "A4": "utm_medium=PC_home-refferral",
    "A5": "utm_campaign=PC_home-refferral-index-mocount",
    "A6": "ActivityCode=PCHOME",
    "A7": "pc_source=kdcl",
    "A8": "fk=fdc3ce07f97",
    "A9": "fk=fdc3ce07f97"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "utm_source=rakuten",
    "A3": "utm_medium=rakuten-referral",
    "A4": "utm_campaign=rakuten-referral-moto",
    "A5": "ActivityCode=RAKUTEN",
    "A6": "fk=97cd3c02a0e74b8b97dc6f1f665f5c0c",
    "A7": "fk=97cd3c02a0e74b8b97dc6f1f665f5c0c",
    "A8": "fk=97cd3c02a0e74b8b97dc6f1f665f5c0c",
    "A9": "fk=97cd3c02a0e74b8b97dc6f1f665f5c0c"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "utm_source=rakuten",
    "A3": "utm_medium=rakuten-referral",
    "A4": "utm_campaign=rakuten-referral-moto",
    "A5": "ActivityCode=RAKUTEN",
    "A6": "fk=de1ef554ae2a457aa1d80e840112b73e",
    "A7": "fk=de1ef554ae2a457aa1d80e840112b73e",
    "A8": "fk=de1ef554ae2a457aa1d80e840112b73e",
    "A9": "fk=de1ef554ae2a457aa1d80e840112b73e"
  },
  {
    "A1": "InsCategory=Motorcycle",
    "A2": "utm_source=rakuten",
    "A3": "utm_medium=rakuten-referral",
    "A4": "utm_campaign=rakuten-referral-moto",
    "A5": "ActivityCode=RAKUTEN",
    "A6": "fk=f94a054a3bc84f99ae125624a43e4bcd",
    "A7": "fk=f94a054a3bc84f99ae125624a43e4bcd",
    "A8": "fk=f94a054a3bc84f99ae125624a43e4bcd",
    "A9": "fk=f94a054a3bc84f99ae125624a43e4bcd"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=00d4c7ef2d9e47a18772bb53444c78bf",
    "A6": "fk=00d4c7ef2d9e47a18772bb53444c78bf",
    "A7": "fk=00d4c7ef2d9e47a18772bb53444c78bf",
    "A8": "fk=00d4c7ef2d9e47a18772bb53444c78bf",
    "A9": "fk=00d4c7ef2d9e47a18772bb53444c78bf"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=08fdeaca29024266af9cce2816d11051",
    "A6": "fk=08fdeaca29024266af9cce2816d11051",
    "A7": "fk=08fdeaca29024266af9cce2816d11051",
    "A8": "fk=08fdeaca29024266af9cce2816d11051",
    "A9": "fk=08fdeaca29024266af9cce2816d11051"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=165ff8e1fae9464cb3bddb3094bb69da",
    "A6": "fk=165ff8e1fae9464cb3bddb3094bb69da",
    "A7": "fk=165ff8e1fae9464cb3bddb3094bb69da",
    "A8": "fk=165ff8e1fae9464cb3bddb3094bb69da",
    "A9": "fk=165ff8e1fae9464cb3bddb3094bb69da"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=310c6e7cdaad43f6aca465b54c4b37fc",
    "A6": "fk=310c6e7cdaad43f6aca465b54c4b37fc",
    "A7": "fk=310c6e7cdaad43f6aca465b54c4b37fc",
    "A8": "fk=310c6e7cdaad43f6aca465b54c4b37fc",
    "A9": "fk=310c6e7cdaad43f6aca465b54c4b37fc"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=3292ae07bb814b3d9fbaf886b1675ef8",
    "A6": "fk=3292ae07bb814b3d9fbaf886b1675ef8",
    "A7": "fk=3292ae07bb814b3d9fbaf886b1675ef8",
    "A8": "fk=3292ae07bb814b3d9fbaf886b1675ef8",
    "A9": "fk=3292ae07bb814b3d9fbaf886b1675ef8"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=34a4c77b0ff74092a566812aee6eec90",
    "A6": "fk=34a4c77b0ff74092a566812aee6eec90",
    "A7": "fk=34a4c77b0ff74092a566812aee6eec90",
    "A8": "fk=34a4c77b0ff74092a566812aee6eec90",
    "A9": "fk=34a4c77b0ff74092a566812aee6eec90"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=3704b2d7404b4279b35cb0bf0204440a",
    "A6": "fk=3704b2d7404b4279b35cb0bf0204440a",
    "A7": "fk=3704b2d7404b4279b35cb0bf0204440a",
    "A8": "fk=3704b2d7404b4279b35cb0bf0204440a",
    "A9": "fk=3704b2d7404b4279b35cb0bf0204440a"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=3781b98bf8b143b5a93cde00622a91d1",
    "A6": "fk=3781b98bf8b143b5a93cde00622a91d1",
    "A7": "fk=3781b98bf8b143b5a93cde00622a91d1",
    "A8": "fk=3781b98bf8b143b5a93cde00622a91d1",
    "A9": "fk=3781b98bf8b143b5a93cde00622a91d1"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=3808f10d1ec243f5af7f9d73dc8581f2",
    "A6": "fk=3808f10d1ec243f5af7f9d73dc8581f2",
    "A7": "fk=3808f10d1ec243f5af7f9d73dc8581f2",
    "A8": "fk=3808f10d1ec243f5af7f9d73dc8581f2",
    "A9": "fk=3808f10d1ec243f5af7f9d73dc8581f2"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=432a371c42614bbe9e9d64ebe159ab44",
    "A6": "fk=432a371c42614bbe9e9d64ebe159ab44",
    "A7": "fk=432a371c42614bbe9e9d64ebe159ab44",
    "A8": "fk=432a371c42614bbe9e9d64ebe159ab44",
    "A9": "fk=432a371c42614bbe9e9d64ebe159ab44"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=48e9063d687b403cb37dd29d19e26e06",
    "A6": "fk=48e9063d687b403cb37dd29d19e26e06",
    "A7": "fk=48e9063d687b403cb37dd29d19e26e06",
    "A8": "fk=48e9063d687b403cb37dd29d19e26e06",
    "A9": "fk=48e9063d687b403cb37dd29d19e26e06"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=55745f381f1c4ed3bcb114d64176e8a9",
    "A6": "fk=55745f381f1c4ed3bcb114d64176e8a9",
    "A7": "fk=55745f381f1c4ed3bcb114d64176e8a9",
    "A8": "fk=55745f381f1c4ed3bcb114d64176e8a9",
    "A9": "fk=55745f381f1c4ed3bcb114d64176e8a9"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=558a7a4755284c688b9fc86aa2374478",
    "A6": "fk=558a7a4755284c688b9fc86aa2374478",
    "A7": "fk=558a7a4755284c688b9fc86aa2374478",
    "A8": "fk=558a7a4755284c688b9fc86aa2374478",
    "A9": "fk=558a7a4755284c688b9fc86aa2374478"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=577d7911586c451781658f2406fcad70",
    "A6": "fk=577d7911586c451781658f2406fcad70",
    "A7": "fk=577d7911586c451781658f2406fcad70",
    "A8": "fk=577d7911586c451781658f2406fcad70",
    "A9": "fk=577d7911586c451781658f2406fcad70"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=5831e709de4849d1893b0611dea136d9",
    "A6": "fk=5831e709de4849d1893b0611dea136d9",
    "A7": "fk=5831e709de4849d1893b0611dea136d9",
    "A8": "fk=5831e709de4849d1893b0611dea136d9",
    "A9": "fk=5831e709de4849d1893b0611dea136d9"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=6f63c9a35f7745cf90b0291e6789bb16",
    "A6": "fk=6f63c9a35f7745cf90b0291e6789bb16",
    "A7": "fk=6f63c9a35f7745cf90b0291e6789bb16",
    "A8": "fk=6f63c9a35f7745cf90b0291e6789bb16",
    "A9": "fk=6f63c9a35f7745cf90b0291e6789bb16"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=7571b70f5b3a411a9b7d0a66a955a8ef",
    "A6": "fk=7571b70f5b3a411a9b7d0a66a955a8ef",
    "A7": "fk=7571b70f5b3a411a9b7d0a66a955a8ef",
    "A8": "fk=7571b70f5b3a411a9b7d0a66a955a8ef",
    "A9": "fk=7571b70f5b3a411a9b7d0a66a955a8ef"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=8337b48b4e9e468ca0b9383fe843b91f",
    "A6": "fk=8337b48b4e9e468ca0b9383fe843b91f",
    "A7": "fk=8337b48b4e9e468ca0b9383fe843b91f",
    "A8": "fk=8337b48b4e9e468ca0b9383fe843b91f",
    "A9": "fk=8337b48b4e9e468ca0b9383fe843b91f"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=8bbf2866ece7487a95a77239db5aa2ca",
    "A6": "fk=8bbf2866ece7487a95a77239db5aa2ca",
    "A7": "fk=8bbf2866ece7487a95a77239db5aa2ca",
    "A8": "fk=8bbf2866ece7487a95a77239db5aa2ca",
    "A9": "fk=8bbf2866ece7487a95a77239db5aa2ca"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=8d87024ca89345c5aea089c8e479db7e",
    "A6": "fk=8d87024ca89345c5aea089c8e479db7e",
    "A7": "fk=8d87024ca89345c5aea089c8e479db7e",
    "A8": "fk=8d87024ca89345c5aea089c8e479db7e",
    "A9": "fk=8d87024ca89345c5aea089c8e479db7e"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=9708dc4473d949588a725b60996bac71",
    "A6": "fk=9708dc4473d949588a725b60996bac71",
    "A7": "fk=9708dc4473d949588a725b60996bac71",
    "A8": "fk=9708dc4473d949588a725b60996bac71",
    "A9": "fk=9708dc4473d949588a725b60996bac71"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=a73a8d32846d49a1932b88b446a6f5f0",
    "A6": "fk=a73a8d32846d49a1932b88b446a6f5f0",
    "A7": "fk=a73a8d32846d49a1932b88b446a6f5f0",
    "A8": "fk=a73a8d32846d49a1932b88b446a6f5f0",
    "A9": "fk=a73a8d32846d49a1932b88b446a6f5f0"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=ac5d039bd5fa466fb774a72dc56552a6",
    "A6": "fk=ac5d039bd5fa466fb774a72dc56552a6",
    "A7": "fk=ac5d039bd5fa466fb774a72dc56552a6",
    "A8": "fk=ac5d039bd5fa466fb774a72dc56552a6",
    "A9": "fk=ac5d039bd5fa466fb774a72dc56552a6"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=b28dc3012bcb43db8ab358e12dfe7417",
    "A6": "fk=b28dc3012bcb43db8ab358e12dfe7417",
    "A7": "fk=b28dc3012bcb43db8ab358e12dfe7417",
    "A8": "fk=b28dc3012bcb43db8ab358e12dfe7417",
    "A9": "fk=b28dc3012bcb43db8ab358e12dfe7417"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=bc00a59b046847a7accd6fb0197831bb",
    "A6": "fk=bc00a59b046847a7accd6fb0197831bb",
    "A7": "fk=bc00a59b046847a7accd6fb0197831bb",
    "A8": "fk=bc00a59b046847a7accd6fb0197831bb",
    "A9": "fk=bc00a59b046847a7accd6fb0197831bb"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=c99d0115a8b94d00bed1d8053cf6a20b",
    "A6": "fk=c99d0115a8b94d00bed1d8053cf6a20b",
    "A7": "fk=c99d0115a8b94d00bed1d8053cf6a20b",
    "A8": "fk=c99d0115a8b94d00bed1d8053cf6a20b",
    "A9": "fk=c99d0115a8b94d00bed1d8053cf6a20b"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=caaae363449c4256ba8e26f7f3f355d6",
    "A6": "fk=caaae363449c4256ba8e26f7f3f355d6",
    "A7": "fk=caaae363449c4256ba8e26f7f3f355d6",
    "A8": "fk=caaae363449c4256ba8e26f7f3f355d6",
    "A9": "fk=caaae363449c4256ba8e26f7f3f355d6"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=d06faec597df4f0480672c11b2855dee",
    "A6": "fk=d06faec597df4f0480672c11b2855dee",
    "A7": "fk=d06faec597df4f0480672c11b2855dee",
    "A8": "fk=d06faec597df4f0480672c11b2855dee",
    "A9": "fk=d06faec597df4f0480672c11b2855dee"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=d53e9a652e1a4d2683eeaf97af7ac5ee",
    "A6": "fk=d53e9a652e1a4d2683eeaf97af7ac5ee",
    "A7": "fk=d53e9a652e1a4d2683eeaf97af7ac5ee",
    "A8": "fk=d53e9a652e1a4d2683eeaf97af7ac5ee",
    "A9": "fk=d53e9a652e1a4d2683eeaf97af7ac5ee"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=d754a9bbd87b4b8fbdcbd0e8b535da0e",
    "A6": "fk=d754a9bbd87b4b8fbdcbd0e8b535da0e",
    "A7": "fk=d754a9bbd87b4b8fbdcbd0e8b535da0e",
    "A8": "fk=d754a9bbd87b4b8fbdcbd0e8b535da0e",
    "A9": "fk=d754a9bbd87b4b8fbdcbd0e8b535da0e"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=d79c8e47b25c463db4eece5e4b1f03d5",
    "A6": "fk=d79c8e47b25c463db4eece5e4b1f03d5",
    "A7": "fk=d79c8e47b25c463db4eece5e4b1f03d5",
    "A8": "fk=d79c8e47b25c463db4eece5e4b1f03d5",
    "A9": "fk=d79c8e47b25c463db4eece5e4b1f03d5"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=ddb4db8b06c7445cbf0f34f85ee5434c",
    "A6": "fk=ddb4db8b06c7445cbf0f34f85ee5434c",
    "A7": "fk=ddb4db8b06c7445cbf0f34f85ee5434c",
    "A8": "fk=ddb4db8b06c7445cbf0f34f85ee5434c",
    "A9": "fk=ddb4db8b06c7445cbf0f34f85ee5434c"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=e70ac6277204476787668cb56f9d4a43",
    "A6": "fk=e70ac6277204476787668cb56f9d4a43",
    "A7": "fk=e70ac6277204476787668cb56f9d4a43",
    "A8": "fk=e70ac6277204476787668cb56f9d4a43",
    "A9": "fk=e70ac6277204476787668cb56f9d4a43"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=ea9972155fc348fbb57365b680d0f0a3",
    "A6": "fk=ea9972155fc348fbb57365b680d0f0a3",
    "A7": "fk=ea9972155fc348fbb57365b680d0f0a3",
    "A8": "fk=ea9972155fc348fbb57365b680d0f0a3",
    "A9": "fk=ea9972155fc348fbb57365b680d0f0a3"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=f9a7d0d97b8d4481bf8a26dc820f10e2",
    "A6": "fk=f9a7d0d97b8d4481bf8a26dc820f10e2",
    "A7": "fk=f9a7d0d97b8d4481bf8a26dc820f10e2",
    "A8": "fk=f9a7d0d97b8d4481bf8a26dc820f10e2",
    "A9": "fk=f9a7d0d97b8d4481bf8a26dc820f10e2"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=fba0ec53b6464976960b5c2aca319e85",
    "A6": "fk=fba0ec53b6464976960b5c2aca319e85",
    "A7": "fk=fba0ec53b6464976960b5c2aca319e85",
    "A8": "fk=fba0ec53b6464976960b5c2aca319e85",
    "A9": "fk=fba0ec53b6464976960b5c2aca319e85"
  },
  {
    "A1": "Project=Default",
    "A2": "utm_source=email",
    "A3": "utm_medium=Check",
    "A4": "utm_campaign=emailCheck",
    "A5": "fk=fff22d98663541c1b2d817eb9d923576",
    "A6": "fk=fff22d98663541c1b2d817eb9d923576",
    "A7": "fk=fff22d98663541c1b2d817eb9d923576",
    "A8": "fk=fff22d98663541c1b2d817eb9d923576",
    "A9": "fk=fff22d98663541c1b2d817eb9d923576"
  },
  {
    "A1": "noredirect=on",
    "A2": "utm_term=.35bda9f38615",
    "A3": "utm_term=.35bda9f38615",
    "A4": "utm_term=.35bda9f38615",
    "A5": "utm_term=.35bda9f38615",
    "A6": "utm_term=.35bda9f38615",
    "A7": "utm_term=.35bda9f38615",
    "A8": "utm_term=.35bda9f38615",
    "A9": "utm_term=.35bda9f38615"
  },
  {
    "A1": "caltype=car",
    "A2": "utm_source=yahoo",
    "A3": "utm_medium=yahoo-referral-autos",
    "A4": "utm_campaign=yahoo-referral-autos-ne",
    "A5": "utm_campaign=yahoo-referral-autos-ne",
    "A6": "utm_campaign=yahoo-referral-autos-ne",
    "A7": "utm_campaign=yahoo-referral-autos-ne",
    "A8": "utm_campaign=yahoo-referral-autos-ne",
    "A9": "utm_campaign=yahoo-referral-autos-ne"
  },
  {
    "A1": "caltype=moto",
    "A2": "cc=113",
    "A3": "utm_source=yahoo",
    "A4": "utm_medium=yahoo-referral-autos",
    "A5": "utm_campaign=yahoo-referral-autos-motor",
    "A6": "utm_campaign=yahoo-referral-autos-motor",
    "A7": "utm_campaign=yahoo-referral-autos-motor",
    "A8": "utm_campaign=yahoo-referral-autos-motor",
    "A9": "utm_campaign=yahoo-referral-autos-motor"
  },
  {
    "A1": "caltype=moto",
    "A2": "cc=124",
    "A3": "utm_source=yahoo",
    "A4": "utm_medium=yahoo-referral-autos",
    "A5": "utm_campaign=yahoo-referral-autos-motor",
    "A6": "utm_campaign=yahoo-referral-autos-motor",
    "A7": "utm_campaign=yahoo-referral-autos-motor",
    "A8": "utm_campaign=yahoo-referral-autos-motor",
    "A9": "utm_campaign=yahoo-referral-autos-motor"
  },
  {
    "A1": "caltype=moto",
    "A2": "cc=150",
    "A3": "utm_source=yahoo",
    "A4": "utm_medium=yahoo-referral-autos",
    "A5": "utm_campaign=yahoo-referral-autos-motor",
    "A6": "utm_campaign=yahoo-referral-autos-motor",
    "A7": "utm_campaign=yahoo-referral-autos-motor",
    "A8": "utm_campaign=yahoo-referral-autos-motor",
    "A9": "utm_campaign=yahoo-referral-autos-motor"
  },
  {
    "A1": "caltype=moto",
    "A2": "cc=276",
    "A3": "utm_source=yahoo",
    "A4": "utm_medium=yahoo-referral-autos",
    "A5": "utm_campaign=yahoo-referral-autos-motor",
    "A6": "utm_campaign=yahoo-referral-autos-motor",
    "A7": "utm_campaign=yahoo-referral-autos-motor",
    "A8": "utm_campaign=yahoo-referral-autos-motor",
    "A9": "utm_campaign=yahoo-referral-autos-motor"
  },
  {
    "A1": "caltype=moto",
    "A2": "cc=399",
    "A3": "utm_source=yahoo",
    "A4": "utm_medium=yahoo-referral-autos",
    "A5": "utm_campaign=yahoo-referral-autos-motor",
    "A6": "utm_campaign=yahoo-referral-autos-motor",
    "A7": "utm_campaign=yahoo-referral-autos-motor",
    "A8": "utm_campaign=yahoo-referral-autos-motor",
    "A9": "utm_campaign=yahoo-referral-autos-motor"
  },
  {
    "A1": "caltype=moto",
    "A2": "cc=649",
    "A3": "utm_source=yahoo",
    "A4": "utm_medium=yahoo-referral-autos",
    "A5": "utm_campaign=yahoo-referral-autos-motor",
    "A6": "utm_campaign=yahoo-referral-autos-motor",
    "A7": "utm_campaign=yahoo-referral-autos-motor",
    "A8": "utm_campaign=yahoo-referral-autos-motor",
    "A9": "utm_campaign=yahoo-referral-autos-motor"
  },
  {
    "A1": "caltype=moto",
    "A2": "cc=803",
    "A3": "utm_source=yahoo",
    "A4": "utm_medium=yahoo-referral-autos",
    "A5": "utm_campaign=yahoo-referral-autos-motor",
    "A6": "utm_campaign=yahoo-referral-autos-motor",
    "A7": "utm_campaign=yahoo-referral-autos-motor",
    "A8": "utm_campaign=yahoo-referral-autos-motor",
    "A9": "utm_campaign=yahoo-referral-autos-motor"
  }
];

function sumDataReduce(arr){
  return arr.reduce((a,b)=>a+b);
}
function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}
function calcAmt(regionName){
  let tmpArr,stores,i=0,j=0,tmpList;
  var amtbyStore=0,nAmtbyRegion=0,mAmtbyRegion=0,sAmtbyRegion=0,eAmtbyRegion=0;
  switch (regionName){
    case "TAIWAN":
        for(i=0;i<salesbyStore.length;i++){
          stores= salesbyStore[i];
          switch(Object.keys(stores)[0]){
            case "基隆市":case "台北市":case "新北市": case "桃園市":case "新竹市":case "新竹縣":
                nAmtbyRegion =sumDataReduce(Object.values(Object.values(stores)[0]))+nAmtbyRegion;
                
              break;
            case "苗栗縣":case "台中市":case "彰化縣":case "南投縣":case "雲林縣":
                mAmtbyRegion =sumDataReduce(Object.values(Object.values(stores)[0]))+mAmtbyRegion;
                
              break;
            case "嘉義市":case "嘉義縣":case "台南市":case "高雄市":case "屏東縣":
                sAmtbyRegion =sumDataReduce(Object.values(Object.values(stores)[0]))+sAmtbyRegion;
               
                break;
            case "宜蘭縣":case "花蓮縣":case "台東縣":
                eAmtbyRegion =sumDataReduce(Object.values(Object.values(stores)[0]))+eAmtbyRegion;
             
                break;
          }
         
        }

        totalamtbyStore.push(nAmtbyRegion);
        totalamtbyStore.push(mAmtbyRegion);
        totalamtbyStore.push(sAmtbyRegion);
        totalamtbyStore.push(eAmtbyRegion);
        
    break;
    default:
        for(i=0;i<salesbyStore.length;i++){
          stores= salesbyStore[i];
          let cityName = Object.keys(stores);
       
          if(cityName[0]===regionName){
            amtbyStore=Object.values(stores);

            store_dim_data = Object.keys(amtbyStore[0]).sort(
              function(a, b){ //針對物件排序
                 
                  return amtbyStore[0][b] - amtbyStore[0][a];
              }
          );
     
         
           // store_dim_data = amtbyStore.map(item=>Object.keys(item));
            store_metrics_data=amtbyStore.map(item=>Object.values(item));
            
            store_metrics_data[0].sort(function(a, b) {
            return b-a;
          });
          //console.log(store_metrics_data);
            
            break;
          }

         
        }

  }
  
}  
calcUtm();
function calcUtm(){
  for(let i=0;i<utmSource.length;i++){
    let sourceStr,sptStr,tmpStr,sptStr2,tmpStr2,tmp_dim;
    for(let k in utmSource[i]){
      sourceStr=utmSource[i][k];
      tmpStr=sourceStr.split("=");
      sptStr=tmpStr[1];
      if( sourceStr.includes("utm_source")){
        for(let m in utmSource[i]){
          if(utmSource[i][m].includes("utm_medium")){
            tmpStr2=utmSource[i][m].split("=");
            sptStr2=tmpStr2[1];
            
            if (medium_dim_data.indexOf(sptStr2)>=0){ // 將資料置入 medium 維度
              medium_metrics_data[medium_dim_data.indexOf(sptStr2)]=medium_metrics_data[medium_dim_data.indexOf(sptStr2)]+1;
            }else{
              
              medium_dim_data.push(sptStr2);
              medium_metrics_data.push(1);
            }
            tmp_dim=sptStr+'/'+sptStr2;
            if (source_medium_dim_data.indexOf(tmp_dim)>=0){ // 將資料置入 medium 維度
              source_medium_metrics_data[source_medium_dim_data.indexOf(tmp_dim)]=source_medium_metrics_data[source_medium_dim_data.indexOf(tmp_dim)]+1;
            }else{
              
              source_medium_dim_data.push(tmp_dim);
              source_medium_metrics_data.push(1);
            }
            
            break;
          }
          
          
        }
        //console.log(source_dim_data.indexOf(sptStr));
      
        if (source_dim_data.indexOf(sptStr)>=0){ // 將資料置入 source 維度
          source_metrics_data[source_dim_data.indexOf(sptStr)]=source_metrics_data[source_dim_data.indexOf(sptStr)]+1;
        }else{
          source_dim_data.push(sptStr);
          source_metrics_data.push(1);
        }
        
        //console.log(source_dim_data);
        //console.log(source_metrics_data);
        break;
      }
      
    }

    
   
    /*
    switch(sourceStr){
  
    }*/
  }
    
}
campaign_dim_data=["urAD/moto","PC_home/PC_home-refferral-index-mocount","email/emailCheck","Email/cRenewmail35d-201907","yahoo/yahoo-referral-autos-motor","Email/mRenewmail35d-201907","letter/letter-pRenew","Email/mRenewmail20d-201906","Email/mRenewmail10d-201906","Email/mRenewmail25d-201907","email/email-price","rakuten/rakuten-referral-moto","LINE/SHARE","line/twad_social_appledaily.tw","facebook.com/motor%2020190603","Email/cRenewmail20d-201906","Email/cRenewmail25d-201906","Email/cRenewmail35d-201906","wewanted/wewanted-helper","facebook/twad_social_appledaily.tw","line/content","line/57738","newssuite/newssuite","Email/cRenewmail10d-201906","Email/cRenewmail20d-201904","Email/cRenewmail35d-201904","Email/cRenewmail35d-201905","Email/mRenewmail35d-201901","Email/mRenewmail35d-201904","Email/mRenewmail35d-201906","family/family-referral-b2b2c-icon","PC_home/Car_event_count","PC_home/PC_home-refferral-Carad","yahoo/yahoo-referral-autos-ne"];
campaign_metrics_data=[158,106,37,7,7,6,6,4,3,3,3,3,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];


