// Chart1 
var chart1 = echarts.init(document.getElementById('chart1'));
var chart2 = echarts.init(document.getElementById('chart2'));
var chart3 = echarts.init(document.getElementById('chart3'));
var chart4 = echarts.init(document.getElementById('chart4'));
var chart5 = echarts.init(document.getElementById('chart5'));

// chart 1 shape ref :https://github.com/ecomfe/echarts-wordcloud
(function () {
    option = {
        
        tooltip: {},
        series: [{
            type: 'wordCloud',
            gridSize: 2,
            sizeRange: [14, 60],
            rotationRange: [-45, 90],
            shape: 'diamond', // circle star heart .... see ref
            width: '100%',
            height: '100%',
            textStyle: {
                normal: {
                    color: function () {
                        return 'rgb(' + [
                            Math.round(Math.random() * 160),
                            Math.round(Math.random() * 160),
                            Math.round(Math.random() * 160)
                        ].join(',') + ')';
                    }
                },
                emphasis: {
                    shadowBlur: 10,
                    shadowColor: '#333'
                }
            },
            data: data1
        }]
    };
    chart1.setOption(option);
})();
// chart 2 funnel
(function () {
    option = {
        color:['#FB7507','#7ED321','#4A90E2','#BD10E0','#50E3C2','#F8E71C'],
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c}%"
        },

        legend: {
            data: ['進入申請頁面', '身份驗證-證件', '身份驗證-個資', '完成申請', '共同條款確認', '身份驗證-簡訊']
        },
        series: [{
                name: ' ',
                type: 'funnel',
                left: '10%',
                height: '80%',
                width: '75%',
                label: {
                    normal: {
                        formatter: '{b}'
                    },
                    emphasis: {
                        position: 'inside',
                        formatter: '{b}预期: {c}%'
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                itemStyle: {
                    normal: {
                        opacity: 0.6
                    }
                },
                data: [{
                        value: 48,
                        name: '身份驗證-簡訊'
                    },
                    {
                        value: 32,
                        name: '共同條款確認'
                    },
                    {
                        value: 16,
                        name: '完成申請'
                    },
                    {
                        value: 65,
                        name: '身份驗證-個資'
                    },
                    {
                        value: 81,
                        name: '身份驗證-證件'
                    },
                    {
                        value: 100,
                        name: '申請頁面'
                    }
                ]
            },
            {
                name: '',
                type: 'funnel',
                height: '80%',
                left: '10%',
                width: '75%',
                maxSize: '80%',
                label: {
                    normal: {
                        position: 'left',
                        formatter: '{c}人',
                        textStyle: {
                            color: '#ff0000',

                        }
                    },
                    emphasis: {
                        position: 'inside',
                        formatter: '{b}: {c}%'
                    }
                },
                itemStyle: {
                    normal: {
                        opacity: 0.5,
                        borderColor: '#fff',
                        borderWidth: 2
                    }
                },
                data: data3
            }
        ]
    };
    chart2.setOption(option);
})();
// chart 3 funnel 
(function () {
    option = {
        color:['#FB7507','#7ED321','#4A90E2','#BD10E0','#50E3C2','#F8E71C'],
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c}%"
        },

        legend: {
            data: ['進入商品選購頁', '確認購物清單', '選擇付款方式', '填寫運送方式', '購物完成']
        },
        series: [{
                name: ' ',
                type: 'funnel',
                left: '10%',
                height: '80%',
                width: '75%',
                label: {
                    normal: {
                        formatter: '{b}'
                    },
                    emphasis: {
                        position: 'inside',
                        formatter: '{b}预期: {c}%'
                    }
                },
                labelLine: {
                    normal: {
                        show: false
                    }
                },
                itemStyle: {
                    normal: {
                        opacity: 0.6
                    }
                },
                data: [
                    {
                        value: 475,
                        name: '購物完成'
                    },
                    {
                        value: 633,
                        name: '填寫運送方式'
                    },
                    {
                        value: 787,
                        name: '選擇付款方式'
                    },
                    {
                        value: 1900,
                        name: '確認購物清單'
                    },
                    {
                        value: 21500,
                        name: '進入商品選購頁'
                    }
                ]
            },
            {
                name: '',
                type: 'funnel',
                height: '80%',
                left: '10%',
                width: '75%',
                maxSize: '80%',
                label: {
                    normal: {
                        position: 'left',
                        formatter: '{c}人',
                        textStyle: {
                            color: '#ff0000',

                        }
                    },
                    emphasis: {
                        position: 'inside',
                        formatter: '{b}: {c}%'
                    }
                },
                itemStyle: {
                    normal: {
                        opacity: 0.5,
                        borderColor: '#fff',
                        borderWidth: 2
                    }
                },
                data: data2
            }
        ]
    };
    chart3.setOption(option);
})();
//  chart4 Bar
(function () {
    
    
    option = {
        color:['#FB7507','#7ED321','#4A90E2','#BD10E0','#50E3C2','#F8E71C'],
        tooltip: {
            trigger: 'item',
            formatter: "{b}:{c}"
        },
        series: [{
            type: 'treemap',
            width: '100%',
            height: '85%',
            top: '15%',
            roam: false, // diable drag
            nodeClick: false, 
            breadcrumb: {
                show: false
            },
            label: { 
                normal: {
                    show: true,
                    position: ['10%', '40%']
                }
            },
            itemStyle: {
                normal: {
                    show: true,
                    textStyle: {
                        color: '#fff',
                        fontSize: 16,
                    },
                    borderWidth: 1,
                    borderColor: '#fff',
                },
    
                emphasis: {
                    label: {
                        show: true
                    }
                }
            },
            data: country_arr
        }]
    };
    chart4.setOption(option);
})();
// chart5 Line
(function () {
    option = {
        
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['一頁跳離', '交易錯誤']
        },
        toolbox: {
            show: true,
            feature: {
                dataView: {
                    readOnly: false
                },
                magicType: {
                    type: ['line', 'bar']
                },
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: date_dim_data
        },
        yAxis: {
            type: 'value',
            axisLabel: {
                formatter: '{value}'
            }
        },
        series: [{
                name: '一頁跳離',
                type: 'line',
                data: date_count_data,
                color:'#7ED321',
                markPoint: {
                    data: [{
                            type: 'max',
                            name: '最大值'
                        },
                        {
                            type: 'min',
                            name: '最小值'
                        }
                    ]
                },
                markLine: {
                    data: [{
                        type: 'average',
                        name: '平均值'
                    }]
                }
            },
            {
                name: '交易錯誤',
                type: 'line',
                color:'#FB7507',
                data: date_error_data,
                markPoint: {
                    data: [{
                        name: '周最低',
                        value: -2,
                        xAxis: 1,
                        yAxis: -1.5
                    }]
                },
                markLine: {
                    data: [{
                        type: 'average',
                        name: '平均值'
                    }]
                }
            }
        ]
    };
    chart5.setOption(option);
})();