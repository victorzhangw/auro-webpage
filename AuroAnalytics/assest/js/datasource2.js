var country_dim_data=[];
var country_error_data=[];
var date_dim_data=[];
var date_error_data=[];
var date_count_data=[];
var mobileWidgetData=[
    {name:'會員初次登入次數',value:2513},
    {name:'會員重複登入次數',value:1223},
    {name:'流量',value:45068},
    {name:'成交量',value:396}
    
];
var tabletWidgetData=[
    {name:'會員初次登入次數',value:47},
    {name:'會員重複登入次數',value:23},
    {name:'流量',value:618},
    {name:'成交量',value:5}
];
var desktopWidgetData=[
    {name:'會員初次登入次數',value:635},
    {name:'會員重複登入次數',value:322},
    {name:'流量',value:9531},
    {name:'成交量',value:173}
];

var data1=[
    {
        "name": "三眼怪",
        "value": 613
      },
      {
        "name": "叉奇",
        "value": 187
      },
      {
        "name": "背心",
        "value": 187
      },
      {
        "name": "吊帶褲",
        "value": 137
      },
      {
        "name": "襯衫",
        "value": 128
      },
      {
        "name": "TWS048 TWS049",
        "value": 103
      },
      {
        "name": "連身",
        "value": 102
      },
      {
        "name": "牛仔褲",
        "value": 96
      },
      {
        "name": "外套",
        "value": 88
      },
      {
        "name": "TWS070 TWS071 TWS088",
        "value": 88
      },
      {
        "name": "短褲",
        "value": 77
      },
      {
        "name": "POLO",
        "value": 70
      },
      {
        "name": "維尼",
        "value": 69
      },
      {
        "name": "草",
        "value": 64
      },
      {
        "name": "牛仔短褲",
        "value": 63
      },
      {
        "name": "連帽牛仔",
        "value": 61
      },
      {
        "name": "情侶",
        "value": 59
      },
      {
        "name": "牛仔",
        "value": 59
      },
      {
        "name": "帽",
        "value": 54
      },
      {
        "name": "飛行 MA-1 B-15",
        "value": 54
      },
      {
        "name": "吊帶",
        "value": 53
      },
      {
        "name": "格紋褲",
        "value": 52
      },
      {
        "name": "條紋",
        "value": 52
      },
      {
        "name": "玩具總動員",
        "value": 51
      },
      {
        "name": "吊帶裙",
        "value": 50
      },
      {
        "name": "超人",
        "value": 48
      },
      {
        "name": "吊帶短褲",
        "value": 47
      },
      {
        "name": "縮口",
        "value": 47
      },
      {
        "name": "流蘇",
        "value": 46
      },
      {
        "name": "Disney",
        "value": 40
      }
     
];



var data2=[
    {name:'進入商品選購頁',value:21500},
    {name:'確認購物清單',value:1900},
    {name:'選擇付款方式',value:787},
    {name:'填寫運送方式',value:633},
    {name:'購物完成',value:475},
   

];
var data3=[
    {name:'(FE) Main -  立即申請 - 進入到申請頁面 (COUNT)',value:425},
    {name:'(FE) Main - 身分驗證 - 填寫身分證號碼 (COUNT)',value:47},
    {name:'(FE) Ori - 條款確認 - 共同行銷條款確認 (COUNT)',value:4},
    {name:'(FE) Ori - 申請完成 (COUNT)',value:2},
    {name:'(FE) Ori - 資料填寫 - 申請卡片詳細資料填寫 ',value:4},
    {name:'(FE) Ori - 身分驗證 - 手機號碼簡訊驗證 (COUNT) ',value:6},
];

var data4=[
    {country:'australia',error:2},
    {country:'canada',error:3},
    {country:'china',error:9},
    {country:'czech republic',error:6},
    {country:'greece',error:1},
    {country:'hong kong',error:25},
    {country:'japan',error:14},
    {country:'korea (south)',error:3},
    {country:'malaysia',error:7},
    {country:'netherlands',error:3},
    {country:'philippines',error:4},
    {country:'russian federation',error:1},
    {country:'singapore',error:3},
    {country:'taiwan',error:313},
    {country:'thailand',error:3},
    {country:'united kingdom',error:1},
    {country:'united states',error:34},
    {country:'viet nam',error:9},
    
];

//交易邏輯錯誤 & 一頁跳離
var data5=[
    {date:'17 May 2019',error:35,count:513},
    {date:'18 May 2019',error:20,count:404},
    {date:'19 May 2019',error:14,count:661},
    {date:'20 May 2019',error:18,count:631},
    {date:'21 May 2019',error:32,count:536},
    {date:'22 May 2019',error:27,count:1197},
    {date:'23 May 2019',error:38,count:186},
];

for(var i=0;i<data4.length;i++){
    country_dim_data.push(data4[i].country);
    country_error_data.push(data4[i].error);
}
var country_arr=[];
for (var i = 0; i < data4.length; i++) {
    var item = {value: data4[i].error ,name:data4[i].country};
    country_arr.push(item);
}

for(var i=0;i<data5.length;i++){
    date_dim_data.push(moment(data5[i].date).format('YYYY-MM-DD'));
    date_error_data.push(data5[i].error);
    date_count_data.push(data5[i].count);  
}