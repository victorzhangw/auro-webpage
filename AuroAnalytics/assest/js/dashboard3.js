// age selector
    var listObj = new ej.dropdowns.MultiSelect({
        // set the placeholder to MultiSelect input element
        placeholder: 'Select Gender ',
        // set the type of mode for how to visualized the selected items in input element.
        mode: 'Box'
    });
    //age range 
    var rangeObj = new ej.inputs.Slider({
        ticks: { placement: 'After', largeStep: 10, smallStep: 5, showSmallTicks: true },
        tooltip: { placement: 'Before', isVisible: true, showOn: 'Focus' },
        min: 15,
        max: 85,
        value: 30,
        // Enables step
        step: 5,
        type: 'Range'
    });
    checkbox = new ej.buttons.CheckBox({ label: '一般' , cssClass: 'e-small' });
    checkbox.appendTo('#checkbox1');
    checkbox = new ej.buttons.CheckBox({ label: 'VIP' , cssClass: 'e-small' });
    checkbox.appendTo('#checkbox2');
    checkbox = new ej.buttons.CheckBox({ label: 'SVIP' , cssClass: 'e-small' });
    checkbox.appendTo('#checkbox3');
    listObj.appendTo('#gender');
    //rangeObj.appendTo('#ageslider');
    button1 = new ej.buttons.Button({ cssClass: 'e-info'}, '#primarySearch');
    button2 = new ej.buttons.Button({ cssClass: 'e-info'}, '#secondSearch');
    //var template='<div>${name}</div><div>${value}</div>';
    
    var template = '<tr><td class="col1"><input type="checkbox" class="check" id="square-checkbox-1" data-checkbox="icheckbox_square-blue" name="tag_active_col[]" value="${name}"></input><span class="p-l-10">${name}</span></td><td class="col2"><select id="conditionbox"><option></option><option>以及</option> <option>或者</option></select></td><td class="col3">${value}</td></tr>';
    
    var table = (document.getElementById('pg3chkTabel'));
    var compiledFunction = ej.base.compile(template);
    
    tagData.forEach(function(item, index, array){
        table.appendChild((compiledFunction(item)[0]));
      });
      
   
      //var predicate = new ej.data.Predicate('tags', 'contains', 'COUPON');
      //  predicate = predicate.or('tags', 'contains', '跨平台');
      //  predicate = predicate.or('tags', 'contains', 'T恤');
      //  var result = new ej.data.DataManager(customerData).executeLocal(new ej.data.Query().where(predicate));
      //  console.log(result);
