var refIndex1=39; // 跳出率參考指標
var gridColumn=[
                { field: 'referral', headerText: '參考來源', textAlign: 'center', width: 120, type: 'string' },
                { field: 'user', width: 140, headerText: '訪客數', type: 'string' },
                { field: 'newusers', headerText: '新訪客數', textAlign: 'left', width: 120, format: 'string' },
                { field: 'bouncerate', headerText: '跳出率', textAlign: 'left', width: 120, format: 'N' },
                { field: 'duration', headerText: '停留時間', textAlign: 'left', width: 120, format: 'string',template:'#bounceTemplate' },
                
    ];
ej.grids.Grid.Inject(ej.grids.Page, ej.grids.Sort, ej.grids.Filter);
var grid1 = new ej.grids.Grid({
    dataSource: data6,
    columns: gridColumn,
    rowHeight: 30,
    allowPaging: true,
    pageSettings: { pageSize: 4 },
    allowSorting: true,
    queryCellInfo: customiseCell
});
grid1.appendTo('#chart2');
function customiseCell(args){
    //console.log(args.cell);
    if(args.column.field==='duration'){
        var tmp_Duration =args.data.duration+'secs';
        args.cell.querySelector("#bounce").textContent=tmp_Duration;
        if(parseInt(args.data.duration)<=refIndex1){
            args.cell.querySelector("#bounce").classList.add("bouncestatus");
           
        }
        
    }
}