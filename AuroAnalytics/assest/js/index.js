ej.base.enableRipple(true);

/**
 * Sample
 */

    var dashboard = new ej.layouts.DashboardLayout({
            columns: 8,
            cellSpacing: [2, 2],
            created:appendElement,
            cellAspectRatio: 100 / 50,
            panels: [
                {
                    'sizeX': 2, 'sizeY': 1, 'row': 0, 'col': 0,
                    content: '#card1'
                },
                {
                    'sizeX': 2, 'sizeY': 1, 'row': 0, 'col': 2,
                    content: '#card2'
                },
                {
                    'sizeX': 2, 'sizeY': 1, 'row': 0, 'col': 4,
                    content: '#card3'
                },
                {
                    'sizeX': 2, 'sizeY': 1, 'row': 0, 'col': 6,
                    content: '#card4'
                },
                {
                    'sizeX': 2, 'sizeY': 1, 'row': 1, 'col': 0,
                    content: '#card5'
                },
                {
                    'sizeX': 2, 'sizeY': 1, 'row': 1, 'col': 2,
                    content: '#card6'
                },
                {
                    'sizeX': 2, 'sizeY': 1, 'row': 1, 'col': 4,
                    content: '#card7'
                },
                {
                    'sizeX': 2, 'sizeY': 1, 'row': 1, 'col': 6,
                    content: '#card8'
                },
                {
                    'sizeX': 2, 'sizeY': 3, 'row': 2, 'col': 0,
                    header: '<div>依性別合計總訪客數</div>', content: '<div id="chart6" class="chart-responsive" ></div>'
                },
                {
                    'sizeX': 2, 'sizeY': 3, 'row': 2, 'col': 2,
                    header: '<div>依性別/年齡合計新訪客</div>', content: '<div id="chart7" class="chart-responsive" ></div>'
                },
                {
                    'sizeX': 2, 'sizeY': 3, 'row': 2, 'col': 4,
                    header: '<div>依設備合計訪客數</div>', content: '<div id="chart8" class="chart-responsive" ></div>'
                },
                {
                    'sizeX': 2, 'sizeY': 3, 'row': 2, 'col': 6,
                    header: '<div>依設備合計工作階段停留時間</div>', content: '<div id="chart9" class="chart-responsive" ></div>'
                },
                {
                    'sizeX': 2, 'sizeY': 3, 'row': 3, 'col': 0,
                    header: '<div>Top 5 頁面跳出率</div>', content: '<div id="chart1" class="chart-responsive" ></div>'
                },
                {
                    'sizeX': 6, 'sizeY': 3, 'row': 3, 'col': 2,
                    header: '<div>網站流量表</div>', content: '<div id="chart2" class="chart-responsive"></div>'
                },
                {
                    'sizeX': 2, 'sizeY': 3, 'row': 5, 'col': 0,
                    header: '<div>Top 5 頁面平均停留時間</div>', content: '<div id="chart5" class="chart-responsive" ></div>'
                },
                {
                    'sizeX': 6, 'sizeY': 3, 'row':5, 'col': 2,
                    header: '<div>Top 10 來源流量(外站)</div>', content: '<div id="chart4" class="chart-responsive" ></div>'
                },
            ]
    });
   
    dashboard.appendTo('#editLayout');
    
    //appendElement();
    function appendElement() {
        for(var i=0;i<widgetData.length;i++){
            var j=i+1;
           $("#w"+j).children(".text").text(widgetData[i].name);
            $("#w"+j).children(".number").text(widgetData[i].value);
        
        }
       
    }
    var sidebarInstance = new ej.navigations.Sidebar({
        type: 'Over',
        dockSize: '65px',
        enableDock: true,
        target: '#target',
        closeOnDocumentClick: true
    });
    sidebarInstance.appendTo('#dockSidebar');

    var atcObj = new ej.dropdowns.AutoComplete({
        placeholder: 'Search Here',
        width: '215px'
    });


    atcObj.appendTo('#search');
    

   

