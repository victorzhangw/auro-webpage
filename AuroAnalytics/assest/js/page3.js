ej.base.enableRipple(true);

/**
 * Sample
 */

    var dashboard = new ej.layouts.DashboardLayout({
            columns: 16,
            allowDragging:false,
            cellSpacing: [2, 2],
            cellAspectRatio: 100 / 80,
            panels: [
                {
                    'sizeX': 4, 'sizeY': 4, 'row': 0, 'col': 0,
                    header: '<div>分類選擇器</div>', content: '#selector1'
                },
                {
                    'sizeX': 12, 'sizeY': 13, 'row': 0, 'col': 5,
                    header: '<div>清單列表</div>', content: '<div id="grid1" class="chart-responsive" ></div>'
                },
                {
                    'sizeX': 4, 'sizeY': 9, 'row': 6, 'col': 0,
                    header: '<div>標籤選擇器</div>', content: '<div id="selector2" class="chart-responsive" ><table id="pg3chkTabel" class=""></table><hr><div id=""><button id="secondSearch">標籤集群</button></div>'
                },
                
               
            ]
    });
    
    dashboard.appendTo('#editLayout');
   
    var sidebarInstance = new ej.navigations.Sidebar({
        type: 'Over',
        dockSize: '65px',
        enableDock: true,
        target: '#target',
        closeOnDocumentClick: true
    });
    sidebarInstance.appendTo('#dockSidebar');

    var atcObj = new ej.dropdowns.AutoComplete({
        placeholder: 'Search Here',
        width: '215px'
    });
    atcObj.appendTo('#search');
    


