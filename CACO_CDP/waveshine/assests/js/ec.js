DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();

const ecReporturl = GoogleremoteUrl + 'ECReport';
const gaReporturl = GoogleremoteUrl + 'CustomGAReport';
var ecKey,gaKey,dp,L10n = ej.base.L10n;
const twNorthRegionName=["基隆市","臺北市","新北市","桃園市","新竹市","新竹縣","宜蘭縣","台北縣"];
const twMiddleRegionName=["苗栗縣","臺中市","彰化縣","南投縣","雲林縣"];
const twSouthRegionName=["嘉義市","嘉義縣","臺南市","高雄市","屏東縣"];
const twEastRegionName=["花蓮縣","臺東縣"];
const prefix =sessionStorage.getItem("sitePrefix")+"-";

$(document).ready(function () {
  
    DramaCore.validateTokenCall(validateSetting());
    let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
    
    dp = DramaCore.createDatePicker(dt, dt);
    
    if(sessionStorage.getItem(defaultStartDate)&&sessionStorage.getItem(defaultEndDate)&&sessionStorage.getItem(daySpan) ){
      dp.value=[sessionStorage.getItem(defaultStartDate),sessionStorage.getItem(defaultEndDate)];  
      ecKey= prefix+"gaec-" + sessionStorage.getItem(defaultStartDate) + "-" + sessionStorage.getItem(defaultEndDate) + "-" + sessionStorage.getItem(daySpan);
        gaKey= prefix+"ga-" + sessionStorage.getItem(defaultStartDate) + "-" + sessionStorage.getItem(defaultEndDate) + "-" + sessionStorage.getItem(daySpan);
        if(localStorage.getItem(ecKey)){
            DramaCore.renderGAECChart(ecKey);
        }else{
            let type = "post";
            para = {
                "MemberKey": memberKey,
                "StartDate": sessionStorage.getItem(defaultStartDate),
                "EndDate": sessionStorage.getItem(defaultEndDate),
                "LoginId": loginID,
                "ReqDimension1":CustomGARequest.GA_Dimensions1.join(),
                "ReqMetric1":CustomGARequest.GA_Metrics1.join(),
                "ReqDimension2":CustomGARequest.GA_Dimensions2.join(),
                "ReqMetric2":CustomGARequest.GA_Metrics2.join(),
                "ReqDimension3":CustomGARequest.GA_Dimensions3.join(),
                "ReqMetric3":CustomGARequest.GA_Metrics3.join(),
                "ReqDimension4":CustomGARequest.GA_Dimensions4.join(),
                "ReqMetric4":CustomGARequest.GA_Metrics4.join(),
                "ReqDimension5":CustomGARequest.GA_Dimensions5.join(),
                "ReqMetric5":CustomGARequest.GA_Metrics5.join()
              };
                global_EC_Key=DramaCore.getRemoteData(ecReporturl, type, para,ecKey,fromEC);  
                
                let timeoutID = window.setInterval(function(){
                if(global_EC_Key){ 
                  
                  //global_custom_Key=DramaCore.getSilenceRemoteData(gaReporturl, type, para,gaKey);  
                  window.clearInterval(timeoutID);
                  DramaCore.renderGAECChart(global_EC_Key);
                  global_EC_Key="";
                }
              },500);
        }
    }

    dp.addEventListener("change",function()
    {
      let dayRange = this.getSelectedRange();
        if (dayRange.daySpan > 0) {
          let _sd = moment(this.startDate).format("YYYY-MM-DD");
          let _ed = moment(this.endDate).format("YYYY-MM-DD");
          
          sessionStorage.setItem(defaultStartDate ,_sd);
          sessionStorage.setItem(defaultEndDate, _ed);
          sessionStorage.setItem('daySpan', dayRange.daySpan);
          let type = "post";
          let para = {
            "MemberKey": memberKey,
            "StartDate": _sd,
            "EndDate": _ed,
            "LoginId": loginID,
            "ReqDimension1":CustomGARequest.GA_Dimensions1.join(),
            "ReqMetric1":CustomGARequest.GA_Metrics1.join(),
            "ReqDimension2":CustomGARequest.GA_Dimensions2.join(),
            "ReqMetric2":CustomGARequest.GA_Metrics2.join(),
            "ReqDimension3":CustomGARequest.GA_Dimensions3.join(),
            "ReqMetric3":CustomGARequest.GA_Metrics3.join(),
            "ReqDimension4":CustomGARequest.GA_Dimensions4.join(),
            "ReqMetric4":CustomGARequest.GA_Metrics4.join(),
            "ReqDimension5":CustomGARequest.GA_Dimensions5.join(),
            "ReqMetric5":CustomGARequest.GA_Metrics5.join()

          };
          if (_sd && _ed) {
            let newecKey = prefix+"gaec-" + _sd + "-" + _ed + "-" + dayRange.daySpan;
            let newgaKey = prefix+"ga-" + _sd + "-" + _ed + "-" + dayRange.daySpan;
            
            if (localStorage.getItem(newecKey)) {
                DramaCore.renderGAECChart(newecKey);
            } else {
             
              para = {
                "MemberKey": memberKey,
                "StartDate": _sd,
                "EndDate": _ed,
                "LoginId": loginID,
                "ReqDimension1":CustomGARequest.GA_Dimensions1.join(),
                "ReqMetric1":CustomGARequest.GA_Metrics1.join(),
                "ReqDimension2":CustomGARequest.GA_Dimensions2.join(),
                "ReqMetric2":CustomGARequest.GA_Metrics2.join(),
                "ReqDimension3":CustomGARequest.GA_Dimensions3.join(),
                "ReqMetric3":CustomGARequest.GA_Metrics3.join(),
                "ReqDimension4":CustomGARequest.GA_Dimensions4.join(),
                "ReqMetric4":CustomGARequest.GA_Metrics4.join(),
                "ReqDimension5":CustomGARequest.GA_Dimensions5.join(),
                "ReqMetric5":CustomGARequest.GA_Metrics5.join()
              };
              global_EC_Key=DramaCore.getRemoteData(ecReporturl, type, para,newecKey,fromEC);  
              let timeoutID = window.setInterval(function(){
                
                if(global_EC_Key){  
                  
                  global_custom_Key=DramaCore.getSilenceRemoteData(gaReporturl, type, para,newgaKey,fromGA); 
                  window.clearInterval(timeoutID);
                  DramaCore.renderGAECChart(global_EC_Key);
                  global_EC_Key="";
                }
              },500);
                 
            }

          }
        }
    });
});
var initDefaultECPage={
  widget:function(){
      var  widgetMetaData=[
          {id:'w1',icons:'e-icons session'},
          {id:'w2',icons:'e-icons views'},
          {id:'w3',icons:'e-icons profile'},
          {id:'w4',icons:'e-icons profile'},
          {id:'w5',icons:'e-icons avgtime'},
          {id:'w6',icons:'e-icons session1'},
          {id:'w7',icons:'e-icons profile1'},
          {id:'w8',icons:'e-icons bounce'}
        
      ];
      var getWidgetString = ej.base.compile('<div class="col-sm-12 col-md-3 mb-1"> <div id=${id} class="card card_animation"> <div class="card-header widget-head "> <h5 class="widget-text align-middle"></h5> </div><div class="card-body"> <span class="${icons}"></span> <h5 class="widget-number align-middle"></h5> </div></div></div>');
      
      while (widgetitems.firstChild) {
        widgetitems.removeChild(widgetitems.firstChild);
      }
      widgetMetaData.forEach(data => {
          widgetitems.appendChild(getWidgetString(data)[0]);
      });
  },
  chart:function(){
      let  chartMetaData=[
          {id:'chart1',size:'col-md-4'},
          {id:'chart2',size:'col-md-4'},
          {id:'chart3',size:'col-md-4'},
          {id:'chart4',size:'col-md-4'},
          {id:'chart5',size:'col-md-4'},
          {id:'chart6',size:'col-md-4'},
          {id:'chart7',size:'col-md-12'},
    
        
      ];
      let getChartString = ej.base.compile('<div class="col-sm-12 ${size} mb-1 d-flex"> <div id=${id} class="card card_animation flex-fill"> <div class="card-header widget-head"> </div><div class="charts card-body chart-height"> </div> </div>  </div>');
      while (chartitems.firstChild) {
        chartitems.removeChild(chartitems.firstChild);
      }
      chartMetaData.forEach(data => {
          chartitems.appendChild(getChartString(data)[0]);
      });
  }

};
var ECChart={
  chart1: function (data,location, rowMax, header,chartid) {
      rowMax += 1; // 因迴圈需求，資料筆數再加 1
      
      let rst1 = data[1].GA_Reports[location],rst2= data[2].GA_Reports[location],
        chartObj = {},
        legend=[],
        dimension = ["第一週","第二週","第三週","第四週"],
        metric1 = [],
        metric2 = [];

      for (let i = 0; i < rst1.ListResult.length; i++) {
          let arr = rst1.ListResult[i];
          if(arr.dimension[0]=="Returning Visitor"){
              legend.push("回訪客");
              for(let j=0 ;j< arr.metrics.length;j++){
              
                  metric2.push(arr.metrics[j].metric[1].value);
              }
              
          }else if(arr.dimension[0]=="New Visitor"){
              legend.push("新訪客");
              for(let j=0 ;j< arr.metrics.length;j++){
              
                  metric1.push(arr.metrics[j].metric[1].value);
              }
          }
          
         
          
        //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
        
      }
      for (let i = 0; i < rst2.ListResult.length; i++) {
        let arr = rst2.ListResult[i];
        if(arr.dimension[0]=="Returning Visitor"){
           
            for(let j=0 ;j< arr.metrics.length;j++){
            
                metric2.push(arr.metrics[j].metric[1].value);
            }
            
        }else if(arr.dimension[0]=="New Visitor"){
           
            for(let j=0 ;j< arr.metrics.length;j++){
            
                metric1.push(arr.metrics[j].metric[1].value);
            }
        }
        
       
        
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      
    }

      let color = ["#FB7507","#BD10E0"];
      let setting = {
        cardHead: header,
        titleText: "",
        category: dimension,
        value1: metric1,
        value2: metric2,
        color: color,
        legend: legend
      };
      let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
      let option = chartSetting.doubleBarChartVertical(setting);
      if (option && typeof option === "object") {
        console.info(chartid);
        chart.setOption(option, true);
        $("#"+chartid).children(".card-header").text(setting.cardHead);
  
      }
    },
  chart2: function (data,location, rowMax, header,chartid) {
    let rst = data[location];
    let legend=[],
        color=[],
        dimension=[],
        seriesdata=[],
        tmpObj1 = {},
        tmpObj2 = {},
        metric1 = [],
        metric2 = [];
    let symbol = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMTBweCIgaGVpZ2h0PSIxMHB4IiB2aWV3Qm94PSIwIDAgMTAgMTAiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8dGl0bGU+d3MtMTBweDwvdGl0bGU+CiAgICA8ZyBpZD0id3MtMTBweCIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTTAuNDY3Mzc2MDI5LDQuODYzOTQwMjEgQzAuNDc1OTU2OTg0LDUuMDI3MDY5MjUgMC40ODE5NjM2NTMsNS4xNzk4OTU0MSAwLjQ5MzExODg5NSw1LjMzMjcyMTU3IEMwLjU2MDgwNTY2Myw2LjM2MTc2MzU5IDAuOTg3ODU2NzU0LDcuMzM0MjU2NzQgMS42OTk2MDEyLDguMDgwMTU4MTUgQzIuMzg1MDI0NTcsOC44MzIwNjQyNiAzLjMxMTg3MTM5LDkuMzIwMTk4MyA0LjMxOTM2Njg0LDkuNDU5ODg2NDUgQzUuNjkyMzE5NjgsOS42NTczNTg0NiA2LjkyMTk3MDU3LDkuMjk4NDc0NTUgNy45NzM5OTU2OCw4LjM4NzUyNzYxIEM5LjI2NzI5MTMyLDcuMjYzMzQ3NDYgOS44MDgyNjMzNiw1LjUwMDYyNzM3IDkuMzY4NDAwOTEsMy44NDM5NTQzOCBDOS4zNjUzMjI4NywzLjgxMDY3MTcyIDkuMzQ4NDM2MTEsMy43ODAyMDYgOS4zMjE4NDg3OCwzLjc1OTk2ODc3IEM5LjI5NTI2MTQ1LDMuNzM5NzMxNTUgOS4yNjE0MTA2LDMuNzMxNTc3ODMgOS4yMjg1MzEzNCwzLjczNzQ5MTIxIEM4Ljg2MTI2NjQ1LDMuNzQzMjE1MDQgOC40OTQyODc2LDMuNzQzMjE1MDQgOC4xMjc1OTQ3OCwzLjczNzQ5MTIxIEM4LjA1MjIzNzQyLDMuNzI1ODM4NjMgNy45ODA5NTc2NiwzLjc3NTI3MTcyIDcuOTY1NDE0NzIsMy44NDk5NjQzOSBDNy41MDc3NjM3OCw1LjEwMTc2NTE4IDcuMDUwMTEyODMsNi4zNTM1NjU5NyA2LjU5MjQ2MTg4LDcuNjA1MzY2NzYgQzYuNTcwMTUxNCw3LjY2NTQ2Njk0IDYuNTUzODQ3NTksNy43MDE1MjcwNCA2LjQ1NDMwODUsNy43MDY2Nzg0OSBDNi4zODIyMjg0OCw3LjcwNjY3ODQ5IDYuMzI0OTk3MTQsNy42NzYzNjgxNCA2LjMwNDk5OTg4LDcuNjAzNjQ5NjIgQzUuODk2NTQ2NDEsNi41Nzc5Mzk5NiA1LjQ4NTIzMjYzLDUuNTUyODAyNjkgNS4wNzEwNTg1Miw0LjUyODIzNzggQzUuMDUxMzIyMzIsNC40Nzg0NDA1MSA1LjAyNzI5NTY1LDQuNDMwMzYwMzcgNC45OTU1NDYxMSw0LjM1NjUyMzAxIEM0Ljk2MDM2NDIsNC40NDIzODA0MSA0LjkzNDYyMTMzLDQuNTA0MTk3NzMgNC45MDk3MzY1Niw0LjU2Njg3MzYzIEM0LjUwMjk5OTI4LDUuNTg2ODU5NDUgNC4wOTM2ODc3Miw2LjYwNTEyODE0IDMuNjkxMjQwOTIsNy42MjU5NzI1NCBDMy42NTUyMDA5LDcuNzE4Njk4NTIgMy41OTU5OTIzMSw3LjcwMDY2ODQ3IDMuNTI5OTE4OTYsNy43MDY2Nzg0OSBDMy40NjM4NDU2LDcuNzEyNjg4NTEgMy40Mjg2NjM2OCw3LjY4OTUwNzAxIDMuNDAzNzc4OTEsNy42MjA4MjEwOSBDMi45NDcyNzIxLDYuMzY5MDIwMyAyLjQ4MjE4NDMyLDUuMTIxNTEyMzggMi4wMjk5Njc5OCwzLjg2ODg1MzAyIEMyLjAxMjYyNzA5LDMuNzgwOTgwMzEgMS45Mjg2MzU4OSwzLjcyMjgwMDE0IDEuODQwMzI4ODcsMy43Mzc0OTEyMSBDMS4zMjU0NzE1NSwzLjc0MzUwMTIzIDAuODA0NjA3NTcsMy43Mzc0OTEyMSAwLjI4NzE3NTk2OSwzLjczNzQ5MTIxIEwwLjE3MzA0OTI2NCwzLjczNzQ5MTIxIEMwLjU1NDkwMTc3MywyLjEwNDQ4MzU5IDEuODgxNTE3NDUsMC41MjA0MTQ2OTQgNC4wMzI3NjI5MywwLjA5NTQyMDU5ODggQzYuMzI0NzM2MDgsLTAuMzU3MDQ3ODYyIDguMzY3ODYxNTIsMC44NzQxNDcxNTMgOS4xNTMwMTg5MywyLjIxNzgxNTM1IEM4Ljk1OTk0NzQ0LDIuMjE3ODE1MzUgOC43MzMxNTYxNSwyLjIxNzgxNTM1IDguNTQ2MDkxMzMsMi4yMTc4MTUzNSBDOC41NDYwOTEzMywyLjIxNzgxNTM1IDguNTE5NzQ0NDMsMi4xNzU3NDUyMyA4LjQ5NDg1OTY2LDIuMTQ2NTUzNzIgQzcuNTAxMzYyLDAuOTMwNjQ3MDY2IDUuOTQ4MzEwNjQsMC4zMTc4MzU0MTMgNC4zOTI3NjIyMiwwLjUyNzkyNTI2NyBDMi44MzcyMTM4LDAuNzM4MDE1MTIxIDEuNTAyMTMxNzksMS43NDA4OTM0IDAuODY2MzkwNDQ4LDMuMTc2ODQyNDMgQzAuODQ1Nzk2MTU1LDMuMjIzMjA1NDMgMC44MjM2OTk2MjIsMy4yNzk4NzEzIDAuNzk4ODE0ODUxLDMuMzQyNTQ3MiBDMC44NDk0NDI0ODcsMy4zNDI1NDcyIDAuODkzODQ5NTA1LDMuMzQyNTQ3MiAwLjkzMjQ2MzgwMywzLjM0MjU0NzIgQzEuMzcwOTUwNjIsMy4zNDI1NDcyIDEuODEwMjk1NTMsMy4zNDI1NDcyIDIuMjQ5NjQwNDMsMy4zNDI1NDcyIEMyLjMxNjcxNTc4LDMuMzMxMjU2MzUgMi4zODA5OTMwNywzLjM3NDEzMTc3IDIuMzk2Mzc0NzcsMy40NDA0MjQ2MyBDMi43NTk2MzUyMSw0LjQ3NzAwOTU1IDMuMTI2MzI4MDMsNS41MTA0NDYzNyAzLjQ5NjQ1MzIzLDYuNTQwNzM1MDkgQzMuNTA3NjA4NDcsNi41NzA3ODUxOCAzLjUyMDQ3OTkxLDYuNjAwODM1MjcgMy41NDE5MzIyOSw2LjY1NTc4NCBDMy41NjU5NTg5Nyw2LjU5OTk3NjY5IDMuNTgyMjYyNzgsNi41Njk5MjY2IDMuNTk1MTM0MjIsNi41MzQ3MjUwNyBDNC4wMTA0NTI0NSw1LjUwNDQzNjM2IDQuNDI1MTk4NjIsNC40NzQxNDc2NCA0LjgzOTM3MjczLDMuNDQzODU4OTMgQzQuODU3NDcxNjMsMy4zNzQyMzgzNSA0LjkyNTM3NDkxLDMuMzI5Njg2NzggNC45OTY0MDQyMSwzLjM0MDgzMDA1IEM1LjA2NzQ3OTQyLDMuMzI5NzMxNTkgNS4xMzU3NjM0OCwzLjM3MzA3NTczIDUuMTU2MDA5OTgsMy40NDIxNDE3OCBDNS41NjczMjM3Niw0LjQ3NzU4MTk0IDUuOTgxNDk3ODcsNS41MTE4NzczMyA2LjM5ODUzMjMsNi41NDUwMjc5NiBDNi40MTIyNjE4Miw2LjU3ODUxMjM0IDYuNDI3NzA3NTQsNi42MTE5OTY3MyA2LjQ1NDMwODUsNi42NzIwOTY5IEM2LjQ4Nzc3NDIzLDYuNTg2MjM5NTEgNi41MTI2NTksNi41MjI3MDUwNCA2LjUzNDk2OTQ4LDYuNDYwODg3NzIgQzYuODg5MDc2OSw1LjQ1OTIxODEzIDcuMjQyODk4MjksNC40NTc1NDg1NSA3LjU5NjQzMzY1LDMuNDU1ODc4OTYgQzcuNjA4MzI2NjYsMy4zODA3OTkxOSA3LjY3ODI1OTk3LDMuMzI5MTgwMjggNy43NTM0NjUxMywzLjMzOTk3MTQ4IEM4LjM3MDE0OTc4LDMuMzQ1MTIyOTIgOC45ODc0MDY0OSwzLjM0NTEyMjkyIDkuNjA1MjM1MjcsMy4zMzk5NzE0OCBDOS42Mzk3MjQ0MiwzLjMzNDU0NjU5IDkuNjc0OTUyNzQsMy4zNDMwODk4NCA5LjcwMzEzMjExLDMuMzYzNzEyNTIgQzkuNzMxMzExNDcsMy4zODQzMzUyIDkuNzUwMTIwODcsMy40MTUzMzg3MyA5Ljc1NTQwMTk5LDMuNDQ5ODY4OTQgQzkuOTc5MTAzOTMsNC4xMjQ2OTc2NyAxMC4wNTA2MDg5LDQuODQwNzMyNTEgOS45NjQ3NzczLDUuNTQ2NTA2NDggQzkuNjgwNzQ3NjgsNy42MDcwODM5MSA4LjU0OTc3Nzc4LDkuMDAwNTQ5NCA2LjYwNDQ3NTIyLDkuNzI3NzYxNTIgQzUuOTY0NjA5MjEsOS45NjI1NDgwNSA1LjI3OTM5NDgsMTAuMDQ3MTQ2MSA0LjYwMTY4MDI3LDkuOTc1MDMwODEgQzIuNzY0NDk3NzUsOS43OTY0NDc0MyAxLjQxMjEzOTIsOC44ODEyMDc2MiAwLjU0MTE3MjI0NCw3LjI2MzY1NDM0IEMwLjIwMTMwMDA0OSw2LjYyMjg5MTk4IDAuMDE2NzA4NDMyNiw1LjkxMTE3MTAyIDAuMDAyMjg4MjU0NzMsNS4xODU5MDU0MyBDMC4wMDIyODgyNTQ3Myw1LjEwMDA0ODA0IC0wLjAwMjg2MDMxODQyLDQuOTQ4OTM5MDIgMC4wMDIyODgyNTQ3Myw0Ljg2NzM3NDUgQzAuMDAyMjg4MjU0NzMsNC44NjgyMzMwOCAwLjAzMjMyMTU5ODEsNC44NjgyMzMwOCAwLjA1Mzc3Mzk4NjIsNC44NjczNzQ1IEMwLjE5MDIxMTE3NSw0Ljg2MDUwNTkxIDAuMzI2NjQ4MzYzLDQuODYzOTQwMjEgMC40NjczNzYwMjksNC44NjM5NDAyMSBaIiBpZD0iUGF0aCIgZmlsbD0iI0M3MDA3NiIgZmlsbC1ydWxlPSJub256ZXJvIj48L3BhdGg+CiAgICA8L2c+Cjwvc3ZnPg==";   
    let areaDimesnion=["北部地區","中部地區","南部地區","東部地區","其他"],areaMetric=[0,0,0,0,0];
    rst.ListResult.forEach(function(item){
          
    if(twNorthRegionName.indexOf(item.dimension[0])>-1){
      tmpObj1.name=item.dimension[0];
      tmpObj1.value= item.metrics[0].metric[1].value;
      tmpObj2.name=areaDimesnion[0];
      tmpObj2.value=Number.parseInt(item.metrics[0].metric[1].value);
      areaMetric[0]  +=Number.parseInt(item.metrics[0].metric[1].value); 

    }else if(twMiddleRegionName.indexOf(item.dimension[0])>-1){
      tmpObj1.name=item.dimension[0];
      tmpObj1.value= item.metrics[0].metric[1].value;
      tmpObj2.name=areaDimesnion[1];
      tmpObj2.value=Number.parseInt(item.metrics[0].metric[1].value);
      areaMetric[1]  +=Number.parseInt(item.metrics[0].metric[1].value); 
    }else if(twSouthRegionName.indexOf(item.dimension[0])>-1){
      tmpObj1.name=item.dimension[0];
      tmpObj1.value= item.metrics[0].metric[1].value;
      tmpObj2.name=areaDimesnion[2];
      tmpObj2.value=Number.parseInt(item.metrics[0].metric[1].value);
      areaMetric[2]  +=Number.parseInt(item.metrics[0].metric[1].value); 
    }else if(twEastRegionName.indexOf(item.dimension[0])>-1){
      tmpObj1.name=item.dimension[0];
      tmpObj1.value= item.metrics[0].metric[1].value;
      tmpObj2.name=areaDimesnion[3];
      tmpObj2.value=Number.parseInt(item.metrics[0].metric[1].value);
      areaMetric[3]  +=Number.parseInt(item.metrics[0].metric[1].value); 
    }else{
      tmpObj2.name=areaDimesnion[4];
      tmpObj2.value=Number.parseInt(item.metrics[0].metric[1].value);
      areaMetric[4]  +=Number.parseInt(item.metrics[0].metric[1].value); 
    }
    if(Object.getOwnPropertyNames(tmpObj1).length != 0 ){
      metric1.push(tmpObj1);
      tmpObj1={};
    }
    if(Object.getOwnPropertyNames(tmpObj2).length != 0 ){
      let _index = metric2.findIndex(item=>item.name===tmpObj2.name); 
      if(_index>-1){
        metric2[_index].value += Number.parseInt(tmpObj2.value); 
      }else{
        metric2.push(tmpObj2);
      }
      tmpObj2={};
    }
    
  });

  color=['#50E372', '#BD10E0', '#FB7507', '#4A90E2'];
  color2=['#FB7507', '#7ED321', '#4A90E2', '#BD10E0'];
  let setting = {
    cardHead: header,
    titleText: "",
    category: dimension,
    value1: metric1,
    value2: metric2,
    color: color,
    color2: color2,
    legend: areaDimesnion,
    symbol:symbol
  };
  
  let chart = echarts.init(document.getElementById(chartid+"L"));
  let option = chartSetting.twMapwithPieChart(setting);
  if (option && typeof option === "object") {
    console.info(chartid);
   
    $.get('./assests/js/taiwan.json', function (twJson) {
      echarts.registerMap('TAIWAN', twJson, {});       
      setTimeout(function(){chart.setOption(option);},100);
  });
  
    $("#"+chartid).children(".card-header").text(setting.cardHead);

  }
  let chartR = echarts.init(document.getElementById(chartid+"R"));
  let optionR = chartSetting.singleCirclePiewithSymbol(setting);
  if (optionR && typeof optionR === "object") {
    chartR.setOption(optionR);

  }
        
    },
  chart3: function (data,location, rowMax, header,chartid) {
      rowMax += 1; // 因迴圈需求，資料筆數再加 1
      
      let rst1 = data[1].GA_Reports[location],rst2= data[2].GA_Reports[location],
        chartObj = {},
        legend=[],
        dimension = ["第一週","第二週","第三週","第四週"],
        metric1 = [],
        metric2 = [];
      for (let i = 0; i < rst1.ListResult.length; i++) {
          let arr = rst1.ListResult[i];
          if(arr.dimension[0]=="Returning Visitor"){
              legend.push("回訪客");
              for(let j=0 ;j< arr.metrics.length;j++){
              
                  metric2.push(arr.metrics[j].metric[0].value);
              }
              
          }else if(arr.dimension[0]=="New Visitor"){
              legend.push("新訪客");
              for(let j=0 ;j< arr.metrics.length;j++){
              
                  metric1.push(arr.metrics[j].metric[0].value);
              }
          }
          
         
          
        //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
        
      }
      for (let i = 0; i < rst2.ListResult.length; i++) {
        let arr = rst2.ListResult[i];
        if(arr.dimension[0]=="Returning Visitor"){
           
            for(let j=0 ;j< arr.metrics.length;j++){
            
                metric2.push(arr.metrics[j].metric[0].value);
            }
            
        }else if(arr.dimension[0]=="New Visitor"){
           
            for(let j=0 ;j< arr.metrics.length;j++){
            
                metric1.push(arr.metrics[j].metric[0].value);
            }
        }
        
       
        
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      
    }

      let color = ["#4A90E2","#7ED321"];
      let setting = {
        cardHead: header,
        titleText: "",
        category: dimension,
        value1: metric1,
        value2: metric2,
        color: color,
        legend: legend
      };
      let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
      let option = chartSetting.doubleBarChartVertical(setting);
      if (option && typeof option === "object") {
        console.info(chartid);
        chart.setOption(option, true);
        $("#"+chartid).children(".card-header").text(setting.cardHead);
  
      }
    },
  chart4:function(data,location, rowMax, header,chartid){
    let rst = data[location];
    let legend=["跳出率","平均訂單價值"],
        color=["#3259B8","#626c91", "#d9def7","#b84a58","#FB7507"],
        dimension=[],
        seriesdata=[],
        avg = {},
        metric1 = [],
        metric2 = [];
    let _sum1=0;
    let _sum2=0;
    let _count=0;
      rst.ListResult.forEach(function(item) {
          let x =Number.parseFloat(item.metrics[0].metric[5].value);
          let y =Number.parseFloat(item.metrics[0].metric[3].value);
          let _obj={
            name:"",
            value:[]
          };
          if (y!=0){
            _count+=1;
            _sum1 += x;
          _sum2 += y;
          metric1.push(x);
          metric2.push(y);
          _obj.name=item.dimension[0];
          _obj.value.push(y);
          _obj.value.push(x);
          seriesdata.push(_obj);
          }
          
      });
      avg.xAvgLine=(_sum2 / _count).toFixed(2);
      avg.yAvgLine=(_sum1 / _count).toFixed(2);
  
      
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      seriesdata:seriesdata,
      value1: metric1,
      value2: metric2,
      avg:avg,
      formatterString:["%","元"],
      color: color,
      legend: legend
    };
   
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.fourQuadrant(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart5:function(data,location, rowMax, header,chartid){
    let rst = data[location],dimension=[],metric=[],legend=[];
    let color = ["#FB7507"];
    
    rst.ListResult.forEach(function(item){
      dimension.push(item.dimension[0]);
      metric.push(item.metrics[0].metric[1]);
    });
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: legend
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithAvgLine(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart6:function(data,location, rowMax, header,chartid){
    let rst = data[location];
    let legend=["電商轉換率","平均訂單價值"],
        color=["#3259B8","#626c91", "#d9def7","#b84a58","#FB7507"],
        dimension=[],
        seriesdata=[],
        avg = {},
        metric1 = [],
        metric2 = [];
    let _sum1=0;
    let _sum2=0;
    let _count=0;
      rst.ListResult.forEach(function(item) {
          let x =Number.parseFloat(item.metrics[0].metric[2].value);
          let y =Number.parseFloat(item.metrics[0].metric[3].value);
          let _obj={
            name:"",
            value:[]
          };
          if(y!=0){
            _count+=1;
            _sum1 += x;
            _sum2 += y;
            metric1.push(x);
            metric2.push(y);
            _obj.name=item.dimension[0];
            _obj.value.push(y);
            _obj.value.push(x);
            seriesdata.push(_obj);
          }
          
      });
      avg.xAvgLine=(_sum2 / _count).toFixed(2);
      avg.yAvgLine=(_sum1 / _count).toFixed(2);
     
      
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      seriesdata:seriesdata,
      value1: metric1,
      value2: metric2,
      avg:avg,
      formatterString:["%","元"],
      color: color,
      legend: legend
    };
   
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.fourQuadrant(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart7:function(data,location, rowMax, header,chartid){
    $('#chart7 .card-body').empty();
    const chart7Column= [{
      field: 'productNO',
      headerText: '品號',
      textAlign: 'left',
      width: 110,
      format: 'N'
    },
    {
      field: 'productName',
      headerText: '品名',
      textAlign: 'left',
      width: 120,
      format: 'N'
    },
    {
      field: 'productRevenue',
      headerText: '產品營收',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'productClicks',
      headerText: '產品點擊次數',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'productAddCart',
      headerText: '產品加入購物車次數',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'productCheckouts',
      headerText: '實際購買次數',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'intentRatio',
      headerText: '潛力商機比',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'boRatio',
      headerText: '商機遺失比',
      textAlign: 'left',
      width: 80,
      format: 'N'
    }
    
  ];
    let rst = data[location],list=[];
    
    
    for (let i = 0; i < rst.ListResult.length; i++) {
      chartObj = {};
      chartObj.productNO=rst.ListResult[i].dimension[0];
      chartObj.productName=rst.ListResult[i].dimension[1];
      chartObj.productRevenue =Number(rst.ListResult[i].metrics[0].metric[0].value) ;
      chartObj.productClicks =Number(rst.ListResult[i].metrics[0].metric[1].value) ;
      chartObj.productAddCart =Number(rst.ListResult[i].metrics[0].metric[2].value) ;
      chartObj.productCheckouts =Number(rst.ListResult[i].metrics[0].metric[3].value) ;
      chartObj.intentRatio =String((((chartObj.productClicks-chartObj.productCheckouts)/chartObj.productClicks)*100).toFixed(1))+'%' ;
      chartObj.boRatio =String((((chartObj.productAddCart-chartObj.productCheckouts)/chartObj.productAddCart)*100).toFixed(1))+'%' ;
      
      list.push(chartObj);
    }
     
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart7Column
    };
    
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart7 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
    
  },
  chart8:function(data,location, rowMax, header,chartid){
    $('#chart8 .card-body').empty();
    const chart8Column= [{
      field: 'catesName',
      headerText: '大分類',
      textAlign: 'left',
      width: 120,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'clicks',
      headerText: '點擊次數',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'addCart',
      headerText: '加入購物車次數',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'checkouts',
      headerText: '實際購買次數',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'revenueRatio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'intentRatio',
      headerText: '潛力商機比',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'boRatio',
      headerText: '商機遺失比',
      textAlign: 'left',
      width: 80,
      format: 'N'
    }
    
  ];
    let rst = data[location],list=[];
    let totalRevenue= Number(rst.MetricHead[0]);
    let totalclicks= Number(rst.MetricHead[1]);
    let totaladdCart= Number(rst.MetricHead[2]);
    let totalcheckout= Number(rst.MetricHead[3]);
    
    for (let i = 0; i < rst.ListResult.length; i++) {
      chartObj = {};
      chartObj.catesName=rst.ListResult[i].dimension[0];
      if (chartObj.catesName==''){
        chartObj.catesName='無定義';
      }
      chartObj.revenue =Number(rst.ListResult[i].metrics[0].metric[0].value) ;
      chartObj.clicks =Number(rst.ListResult[i].metrics[0].metric[1].value) ;
      chartObj.addCart =Number(rst.ListResult[i].metrics[0].metric[2].value) ;
      chartObj.checkouts =Number(rst.ListResult[i].metrics[0].metric[3].value) ;
      chartObj.revenueRatio =String((((chartObj.revenue)/totalRevenue)*100).toFixed(1))+'%' ;
      chartObj.intentRatio =String((((chartObj.clicks-chartObj.checkouts)/chartObj.clicks)*100).toFixed(1))+'%' ;
      chartObj.boRatio =String((((chartObj.addCart-chartObj.checkouts)/chartObj.addCart)*100).toFixed(1))+'%' ;
      
      list.push(chartObj);
    }
     
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart8Column
    };
    
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart8 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
    
  },
  chart9:function(data,location, rowMax, header,chartid){
    $('#chart9 .card-body').empty();
    const chart9Column= [{
      field: 'catesName',
      headerText: '中分類',
      textAlign: 'left',
      width: 120,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'clicks',
      headerText: '點擊次數',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'addCart',
      headerText: '加入購物車次數',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'checkouts',
      headerText: '實際購買次數',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'revenueRatio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'intentRatio',
      headerText: '潛力商機比',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'boRatio',
      headerText: '商機遺失比',
      textAlign: 'left',
      width: 80,
      format: 'N'
    }
    
  ];
    let rst = data[location],list=[];
    let totalRevenue= Number(rst.MetricHead[0]);
    let totalclicks= Number(rst.MetricHead[1]);
    let totaladdCart= Number(rst.MetricHead[2]);
    let totalcheckout= Number(rst.MetricHead[3]);
    
    for (let i = 0; i < rst.ListResult.length; i++) {
      chartObj = {};
      chartObj.catesName=rst.ListResult[i].dimension[0];
      if (chartObj.catesName==''){
        chartObj.catesName='無定義';
      }
      chartObj.revenue =Number(rst.ListResult[i].metrics[0].metric[0].value) ;
      chartObj.clicks =Number(rst.ListResult[i].metrics[0].metric[1].value) ;
      chartObj.addCart =Number(rst.ListResult[i].metrics[0].metric[2].value) ;
      chartObj.checkouts =Number(rst.ListResult[i].metrics[0].metric[3].value) ;
      chartObj.revenueRatio =String((((chartObj.revenue)/totalRevenue)*100).toFixed(1))+'%' ;
      chartObj.intentRatio =String((((chartObj.clicks-chartObj.checkouts)/chartObj.clicks)*100).toFixed(1))+'%' ;
      chartObj.boRatio =String((((chartObj.addCart-chartObj.checkouts)/chartObj.addCart)*100).toFixed(1))+'%' ;
      
      list.push(chartObj);
    }
     
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart9Column
    };
    
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart9 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
    
  },
  chart10:function(data,location, rowMax, header,chartid){
    $('#chart10 .card-body').empty();
    const chart10Column= [{
      field: 'catesName',
      headerText: '品牌',
      textAlign: 'left',
      width: 120,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'clicks',
      headerText: '點擊次數',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'addCart',
      headerText: '加入購物車次數',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'checkouts',
      headerText: '結帳次數',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'revenueRatio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'intentRatio',
      headerText: '潛力商機比',
      textAlign: 'left',
      width: 80,
      format: 'N'
    },
    {
      field: 'boRatio',
      headerText: '商機遺失比',
      textAlign: 'left',
      width: 80,
      format: 'N'
    }
    
  ];
    let rst = data[location],list=[];
    let totalRevenue= Number(rst.MetricHead[0]);
    let totalclicks= Number(rst.MetricHead[1]);
    let totaladdCart= Number(rst.MetricHead[2]);
    let totalcheckout= Number(rst.MetricHead[3]);
    
    for (let i = 0; i < rst.ListResult.length; i++) {
      chartObj = {};
      chartObj.catesName=rst.ListResult[i].dimension[0];
      if (chartObj.catesName==''){
        chartObj.catesName='無定義';
      }
      chartObj.revenue =Number(rst.ListResult[i].metrics[0].metric[0].value) ;
      chartObj.clicks =Number(rst.ListResult[i].metrics[0].metric[1].value) ;
      chartObj.addCart =Number(rst.ListResult[i].metrics[0].metric[2].value) ;
      chartObj.checkouts =Number(rst.ListResult[i].metrics[0].metric[3].value) ;
      chartObj.revenueRatio =String((((chartObj.revenue)/totalRevenue)*100).toFixed(1))+'%' ;
      chartObj.intentRatio =String((((chartObj.clicks-chartObj.checkouts)/chartObj.clicks)*100).toFixed(1))+'%' ;
      chartObj.boRatio =String((((chartObj.addCart-chartObj.checkouts)/chartObj.addCart)*100).toFixed(1))+'%' ;
      
      list.push(chartObj);
    }
     
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart10Column
    };
    
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart10 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
    
  }
 
 
};
function validateSetting(){
    let bearerToken =sessionStorage.getItem("token");
    let urlTo= AuthremoteUrl+"GetUserName";
    let Setting={
        async: true,
        type: "GET",
        url: urlTo,
        headers: {
            Authorization: "Bearer" +" "+ bearerToken
           
          }
    };
    return Setting;
}
function calcAmt(regionName){
  let tmpArr,stores,i=0,j=0,tmpList;
  var amtbyStore=0,nAmtbyRegion=0,mAmtbyRegion=0,sAmtbyRegion=0,eAmtbyRegion=0;
  switch (regionName){
    case "TAIWAN":
        for(i=0;i<salesbyStore.length;i++){
          stores= salesbyStore[i];
          switch(Object.keys(stores)[0]){
            case "基隆市":case "台北市":case "新北市": case "桃園市":case "新竹市":case "新竹縣":
                nAmtbyRegion =sumDataReduce(Object.values(Object.values(stores)[0]))+nAmtbyRegion;
                
              break;
            case "苗栗縣":case "台中市":case "彰化縣":case "南投縣":case "雲林縣":
                mAmtbyRegion =sumDataReduce(Object.values(Object.values(stores)[0]))+mAmtbyRegion;
                
              break;
            case "嘉義市":case "嘉義縣":case "台南市":case "高雄市":case "屏東縣":
                sAmtbyRegion =sumDataReduce(Object.values(Object.values(stores)[0]))+sAmtbyRegion;
               
                break;
            case "宜蘭縣":case "花蓮縣":case "台東縣":
                eAmtbyRegion =sumDataReduce(Object.values(Object.values(stores)[0]))+eAmtbyRegion;
             
                break;
          }
         
        }

        totalamtbyStore.push(nAmtbyRegion);
        totalamtbyStore.push(mAmtbyRegion);
        totalamtbyStore.push(sAmtbyRegion);
        totalamtbyStore.push(eAmtbyRegion);
        
    break;
    default:
        for(i=0;i<salesbyStore.length;i++){
          stores= salesbyStore[i];
          let cityName = Object.keys(stores);
       
          if(cityName[0]===regionName){
            amtbyStore=Object.values(stores);

            store_dim_data = Object.keys(amtbyStore[0]).sort(
              function(a, b){ //針對物件排序
                 
                  return amtbyStore[0][b] - amtbyStore[0][a];
              }
          );
     
         
           // store_dim_data = amtbyStore.map(item=>Object.keys(item));
            store_metrics_data=amtbyStore.map(item=>Object.values(item));
            
            store_metrics_data[0].sort(function(a, b) {
            return b-a;
          });
          //console.log(store_metrics_data);
            
            break;
          }

         
        }

  }
  
}  
