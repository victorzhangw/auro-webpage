DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();

const ecReporturl = GoogleremoteUrl + 'ECReport';
const gaReporturl = GoogleremoteUrl + 'CustomGAReport';
var ecKey, gaKey, dp, L10n = ej.base.L10n;
const chartsDefine = [
  {
    id: 'chart7',
    title: '各品牌營收'
  },
  {
    id: 'chart8',
    title: '各類別營收'
  },
  {
    id: 'chart9',
    title: '各類別-顏色營收'
  },
  {
    id: 'chart10',
    title: '各通路營收'
  },
  {
    id: 'chart11',
    title: '各店鋪營收'
  },
  {
    id: 'chart12',
    title: '產品群組營收'
  },
  {
    id: 'chart13',
    title: '季節產品營收'
  },
  {
    id: 'chart14',
    title: '季節產品群組營收'
  },
  {
    id: 'chart15',
    title: '性別群組營收'
  },
  {
    id: 'chart16',
    title: '泳裝群組營收'
  },
  {
    id: 'chart17',
    title: '各品牌促銷活動統計營收'
  },
  {
    id: 'chart18',
    title: '服裝款式營收'
  }

]
const prefix = sessionStorage.getItem("sitePrefix") + "-";

$(document).ready(function () {

  DramaCore.validateTokenCall(validateSetting());

  let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");

  dp = DramaCore.createDatePicker(dt, dt);

  if (sessionStorage.getItem(defaultStartDate) && sessionStorage.getItem(defaultEndDate) && sessionStorage.getItem(daySpan)) {
    dp.value = [sessionStorage.getItem(defaultStartDate), sessionStorage.getItem(defaultEndDate)];
    ecKey = prefix + "gaec-" + sessionStorage.getItem(defaultStartDate) + "-" + sessionStorage.getItem(defaultEndDate) + "-" + sessionStorage.getItem(daySpan);
    gaKey = prefix + "ga-" + sessionStorage.getItem(defaultStartDate) + "-" + sessionStorage.getItem(defaultEndDate) + "-" + sessionStorage.getItem(daySpan);
    if (localStorage.getItem(ecKey)) {
      DramaCore.renderPOSECChart(ecKey, chartsDefine);
    } else {
      let type = "post";
      para = {
        "MemberKey": memberKey,
        "StartDate": sessionStorage.getItem(defaultStartDate),
        "EndDate": sessionStorage.getItem(defaultEndDate),
        "LoginId": loginID,
        "ReqDimension1": CustomGARequest.GA_Dimensions1.join(),
        "ReqMetric1": CustomGARequest.GA_Metrics1.join(),
        "ReqDimension2": CustomGARequest.GA_Dimensions2.join(),
        "ReqMetric2": CustomGARequest.GA_Metrics2.join(),
        "ReqDimension3": CustomGARequest.GA_Dimensions3.join(),
        "ReqMetric3": CustomGARequest.GA_Metrics3.join(),
        "ReqDimension4": CustomGARequest.GA_Dimensions4.join(),
        "ReqMetric4": CustomGARequest.GA_Metrics4.join(),
        "ReqDimension5": CustomGARequest.GA_Dimensions5.join(),
        "ReqMetric5": CustomGARequest.GA_Metrics5.join()
      };
      global_EC_Key = DramaCore.getRemoteData(ecReporturl, type, para, ecKey, fromEC);

      let timeoutID = window.setInterval(function () {
        if (global_EC_Key) {

          //global_custom_Key=DramaCore.getSilenceRemoteData(gaReporturl, type, para,gaKey);  
          window.clearInterval(timeoutID);
          DramaCore.renderPOSECChart(global_EC_Key, chartsDefine);
          global_EC_Key = "";
        }
      }, 500);
    }
  }

  dp.addEventListener("change", function () {
    let dayRange = this.getSelectedRange();
    if (dayRange.daySpan > 0) {
      let _sd = moment(this.startDate).format("YYYY-MM-DD");
      let _ed = moment(this.endDate).format("YYYY-MM-DD");
      sessionStorage.setItem(defaultStartDate, _sd);
      sessionStorage.setItem(defaultEndDate, _ed);
      sessionStorage.setItem('daySpan', dayRange.daySpan);
      let type = "post";
      let para = {
        "MemberKey": memberKey,
        "StartDate": _sd,
        "EndDate": _ed,
        "LoginId": loginID,
        "ReqDimension1": CustomGARequest.GA_Dimensions1.join(),
        "ReqMetric1": CustomGARequest.GA_Metrics1.join(),
        "ReqDimension2": CustomGARequest.GA_Dimensions2.join(),
        "ReqMetric2": CustomGARequest.GA_Metrics2.join(),
        "ReqDimension3": CustomGARequest.GA_Dimensions3.join(),
        "ReqMetric3": CustomGARequest.GA_Metrics3.join(),
        "ReqDimension4": CustomGARequest.GA_Dimensions4.join(),
        "ReqMetric4": CustomGARequest.GA_Metrics4.join(),
        "ReqDimension5": CustomGARequest.GA_Dimensions5.join(),
        "ReqMetric5": CustomGARequest.GA_Metrics5.join()

      };
      if (_sd && _ed) {
        let newecKey = prefix + "gaec-" + _sd + "-" + _ed + "-" + dayRange.daySpan;
        let newgaKey = prefix + "ga-" + _sd + "-" + _ed + "-" + dayRange.daySpan;

        if (localStorage.getItem(newecKey)) {
          DramaCore.renderPOSECChart(newecKey, chartsDefine);
        } else {

          para = {
            "MemberKey": memberKey,
            "StartDate": _sd,
            "EndDate": _ed,
            "LoginId": loginID,
            "ReqDimension1": CustomGARequest.GA_Dimensions1.join(),
            "ReqMetric1": CustomGARequest.GA_Metrics1.join(),
            "ReqDimension2": CustomGARequest.GA_Dimensions2.join(),
            "ReqMetric2": CustomGARequest.GA_Metrics2.join(),
            "ReqDimension3": CustomGARequest.GA_Dimensions3.join(),
            "ReqMetric3": CustomGARequest.GA_Metrics3.join(),
            "ReqDimension4": CustomGARequest.GA_Dimensions4.join(),
            "ReqMetric4": CustomGARequest.GA_Metrics4.join(),
            "ReqDimension5": CustomGARequest.GA_Dimensions5.join(),
            "ReqMetric5": CustomGARequest.GA_Metrics5.join()
          };
          global_EC_Key = DramaCore.getRemoteData(ecReporturl, type, para, newecKey, fromEC);
          let timeoutID = window.setInterval(function () {

            if (global_EC_Key) {

              global_custom_Key = DramaCore.getSilenceRemoteData(gaReporturl, type, para, newgaKey, fromGA);
              window.clearInterval(timeoutID);
              DramaCore.renderPOSECChart(global_EC_Key, chartsDefine);
              global_EC_Key = "";
            }
          }, 500);

        }

      }
    }
  });
  let h1tooltip = new ej.popups.Tooltip({
    content: 'GA 訂單金額'
  });
  let h2tooltip = new ej.popups.Tooltip({
    content: 'GA 結賬金額'
  });
  let h3tooltip = new ej.popups.Tooltip({
    content: '店面結賬金額'
  });
  //console.log(window['tooltipObj' + j] );
  h1tooltip.appendTo('#h1');
  h2tooltip.appendTo('#h2');
  h3tooltip.appendTo('#h3');
});

var ECChart = {

  chart7: function (data, location, rowMax, header, chartid) {
    $('#chart7 .card-body').empty();
    const chart7Column = [{
      field: 'category',
      headerText: '品牌',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location], list = [];


    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category = rst.ListResult[0].dimension[i];
      chartObj.revenue = Number(rst.ListResult[0].metrics[0].metric[i].value);
      chartObj.ratio = Number(rst.ListResult[0].metrics[1].metric[i].value);
      list.push(chartObj);
    }
    console.log(list);
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart7Column
    };

    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart7 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);

  },
  chart8: function (data, location, rowMax, header, chartid) {
    $('#chart8 .card-body').empty();
    const chart8Column = [{
      field: 'category',
      headerText: '類別',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location], list = [];
    console.log(rst.ListResult);

    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category = rst.ListResult[0].dimension[i];
      chartObj.revenue = Number(rst.ListResult[0].metrics[0].metric[i].value);
      chartObj.ratio = Number(rst.ListResult[0].metrics[1].metric[i].value);
      list.push(chartObj);
    }

    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart8Column
    };

    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart8 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);


  },
  chart9: function (data, location, rowMax, header, chartid) {
    $('#chart9 .card-body').empty();
    const chart9Column = [{
      field: 'category',
      headerText: '類別-顏色',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location], list = [];
    console.log(rst.ListResult);

    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category = rst.ListResult[0].dimension[i];
      chartObj.revenue = Number(rst.ListResult[0].metrics[0].metric[i].value);
      chartObj.ratio = Number(rst.ListResult[0].metrics[1].metric[i].value);
      list.push(chartObj);
    }
    console.log(list);
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart9Column
    };

    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart9 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart10: function (data, location, rowMax, header, chartid) {
    $('#chart10 .card-body').empty();
    const chart10Column = [{
      field: 'category',
      headerText: '通路',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'countnumber1',
      headerText: '交易次數',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location], list = [];


    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category = rst.ListResult[0].dimension[i];
      chartObj.revenue = Number(rst.ListResult[0].metrics[0].metric[i].value);
      chartObj.ratio = Number(rst.ListResult[0].metrics[1].metric[i].value);
      chartObj.countnumber1 = Number(rst.ListResult[0].metrics[3].metric[i].value);
      list.push(chartObj);
    }
    console.log(list);
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart10Column
    };

    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart10 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart11: function (data, location, rowMax, header, chartid) {
    $('#chart11 .card-body').empty();
    const chart11Column = [{
      field: 'category',
      headerText: '店鋪',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location], list = [];


    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category = rst.ListResult[0].dimension[i];
      chartObj.revenue = Number(rst.ListResult[0].metrics[0].metric[i].value);
      chartObj.ratio = Number(rst.ListResult[0].metrics[1].metric[i].value);
      list.push(chartObj);
    }
    console.log(list);
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart11Column
    };

    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart11 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart12: function (data, location, rowMax, header, chartid) {
    $('#chart12 .card-body').empty();
    const chart12Column = [
      {
        field: 'productimg',
        headerText: '',
        width: 80,
        template: '#productImgTemplate'
      },
      {
        field: 'id',
        headerText: '產品編號',
        textAlign: 'left',
        width: 90,

      },
      {
        field: 'category',
        headerText: '敘述',
        textAlign: 'left',
        width: 160,
        format: 'N'
      },
      {
        field: 'revenue',
        headerText: '營收',
        textAlign: 'left',
        width: 70,
        format: 'N'
      },
      {
        field: 'ratio',
        headerText: '佔比',
        textAlign: 'left',
        width: 70,
        format: 'N'
      }];
    let rst = data[location], list = [];


    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category = rst.ListResult[0].dimension[i];
      chartObj.revenue = Number(rst.ListResult[0].metrics[0].metric[i].value);
      chartObj.ratio = Number(rst.ListResult[0].metrics[1].metric[i].value);
      chartObj.id = rst.ListResult[0].metrics[2].metric[i].value;
      chartObj.productimg = rst.ListResult[0].metrics[4].metric[i].value;
      list.push(chartObj);
    }
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart12Column,
      allowResizing: true,
      pageSize: 4,
      pageCount: 4,
      customiseCell:function(args){
        if (args.column.field === 'productimg') {
          var tooltip = new ej.popups.Tooltip({
            
            content:'<img src='+args.data[args.column.field].toString()+' class="toolitipImg"></img>',  // add Essential JS2 tooltip for every cell.
        },args.cell);
          
        }
      }
    };

    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart12 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart13: function (data, location, rowMax, header, chartid) {
    $('#chart13 .card-body').empty();
    const chart13Column = [{
      field: 'category',
      headerText: '季節',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location], list = [];


    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category = rst.ListResult[0].dimension[i];
      chartObj.revenue = Number(rst.ListResult[0].metrics[0].metric[i].value);
      chartObj.ratio = Number(rst.ListResult[0].metrics[1].metric[i].value);
      list.push(chartObj);
    }

    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart13Column
    };

    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart13 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart14: function (data, location, rowMax, header, chartid) {
    $('#chart14 .card-body').empty();
    const chart14Column = [{
      field: 'category',
      headerText: '季節-產品',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location], list = [];


    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category = rst.ListResult[0].dimension[i];
      chartObj.revenue = Number(rst.ListResult[0].metrics[0].metric[i].value);
      chartObj.ratio = Number(rst.ListResult[0].metrics[1].metric[i].value);
      list.push(chartObj);
    }

    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart14Column
    };

    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart14 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart15: function (data, location, rowMax, header, chartid) {
    $('#chart15 .card-body').empty();
    const chart15Column = [{
      field: 'category',
      headerText: '性別',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location], list = [];


    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category = rst.ListResult[0].dimension[i];
      chartObj.revenue = Number(rst.ListResult[0].metrics[0].metric[i].value);
      chartObj.ratio = Number(rst.ListResult[0].metrics[1].metric[i].value);
      list.push(chartObj);
    }

    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart15Column
    };

    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart15 .card-body');

    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart16: function (data, location, rowMax, header, chartid) {
    $('#chart16 .card-body').empty();
    const chart16Column = [
      {
        field: 'category',
        headerText: '群組',
        textAlign: 'left',
        width: 140,
        format: 'N'
      },
      {
        field: 'revenue',
        headerText: '營收',
        textAlign: 'left',
        width: 50,
        format: 'N'
      },
      {
        field: 'ratio',
        headerText: '佔比',
        textAlign: 'left',
        width: 50,
        format: 'N'
      }];
    let rst = data[location], list = [];


    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category = rst.ListResult[0].dimension[i];
      chartObj.revenue = Number(rst.ListResult[0].metrics[0].metric[i].value);
      chartObj.ratio = Number(rst.ListResult[0].metrics[1].metric[i].value);

      list.push(chartObj);
    }

    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart16Column
    };

    let chart = chartSetting.gridChart(setting);

    chart.appendTo('#chart16 .card-body');

    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart17: function (data, location, rowMax, header, chartid) {
    $('#chart17 .card-body').empty();
    const chart16Column = [
      {
        field: 'category',
        headerText: '促銷活動',
        textAlign: 'left',
        width: 140,
        format: 'N'
      },
      {
        field: 'countnumber1',
        headerText: '次數',
        textAlign: 'left',
        width: 50,
        format: 'N'
      },
      {
        field: 'revenue',
        headerText: '收益',
        textAlign: 'left',
        width: 50,
        format: 'N'
      },
      {
        field: 'ratio',
        headerText: '佔比',
        textAlign: 'left',
        width: 50,
        format: 'N'
      }];
    let rst = data[location], list = [];


    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category = rst.ListResult[0].dimension[i];
      chartObj.revenue = Number(rst.ListResult[0].metrics[0].metric[i].value);
      chartObj.ratio = Number(rst.ListResult[0].metrics[1].metric[i].value);
      chartObj.countnumber1 = Number(rst.ListResult[0].metrics[3].metric[i].value);

      list.push(chartObj);
    }

    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart16Column
    };

    let chart = chartSetting.gridChart(setting);

    chart.appendTo('#chart17 .card-body');

    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart18: function (data, location, rowMax, header, chartid) {
    $('#chart18 .card-body').empty();
    const chart16Column = [
      {
        field: 'category',
        headerText: '服裝種類',
        textAlign: 'left',
        width: 140,
        format: 'N'
      },
      {
        field: 'revenue',
        headerText: '營收',
        textAlign: 'left',
        width: 50,
        format: 'N'
      },
      {
        field: 'ratio',
        headerText: '佔比',
        textAlign: 'left',
        width: 50,
        format: 'N'
      }];
    let rst = data[location], list = [];


    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category = rst.ListResult[0].dimension[i];
      chartObj.revenue = Number(rst.ListResult[0].metrics[0].metric[i].value);
      chartObj.ratio = Number(rst.ListResult[0].metrics[1].metric[i].value);

      list.push(chartObj);
    }

    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart16Column
    };

    let chart = chartSetting.gridChart(setting);

    chart.appendTo('#chart18 .card-body');

    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart19: function (data, location, rowMax, header, chartid) {

  },
  chart20: function (data, location, rowMax, header, chartid) {
    
  }
};
function validateSetting() {
  let bearerToken = sessionStorage.getItem("token");
  let urlTo = AuthremoteUrl + "GetUserName";
  let Setting = {
    async: true,
    type: "GET",
    url: urlTo,
    headers: {
      Authorization: "Bearer" + " " + bearerToken

    }
  };
  return Setting;
}

