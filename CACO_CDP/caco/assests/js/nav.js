const sideNavitems = (document.getElementById('sideNav'));
/**
 * Nav source
 */
var navData=[
    {id:'nav01',navItem:'流量',navUrl:'page1.html',navIcon:'e-icons home'},
    {id:'nav02',navItem:'互動',navUrl:'page2.html',navIcon:'e-icons analyticsChart'},
    {id:'nav03',navItem:'標籤',navUrl:'page3.html',navIcon:'e-icons analytics'},
    {id:'nav04',navItem:'交易',navUrl:'page4.html',navIcon:'e-icons filter'},
    {id:'nav05',navItem:'社群',navUrl:'page5.html',navIcon:'e-icons filter'},
    {id:'nav06',navItem:'設定',navUrl:'pageSetting.html',navIcon:'e-icons settings'}
];

navData1={id:'nav01',navItem:'流量',navUrl:'page1.html',navIcon:'e-icons home'};
var getNavstring = ej.base.compile('<li id=${id} class="sidebar-item"><a href="${navUrl}"><span class="${navIcon}"></span><h6 class="mtm5">${navItem}</h6></a></li>');

navData.forEach(data => {
    sideNavitems.appendChild(getNavstring(data)[0]);
});

const ulData = document.getElementsByTagName('ul');
    console.log(ulData[0].dataset.nav);
    document.addEventListener('DOMContentLoaded',()=>{
        switch(ulData[0].dataset.nav){
            case 'page1':
                document.getElementById('nav01').className='sidebar-item filterHover';
                break;
            case 'page2':
                document.getElementById('nav02').className='sidebar-item filterHover';
                break;
            case 'page3':
                document.getElementById('nav03').className='sidebar-item filterHover';
                break;
            case 'page4':
                document.getElementById('nav04').className='sidebar-item filterHover';    
                break;
            case 'page5':
                document.getElementById('nav05').className='sidebar-item filterHover';    
                break;
            case 'pageSetting':
                document.getElementById('navSetting').className='sidebar-item filterHover';    
                break;
        }
    });
    document.addEventListener('onunload',()=>{
        switch(ulData[0].dataset.nav){
            case 'page1':
                document.getElementById('nav01').className='sidebar-item ';
                break;
            case 'page2':
                document.getElementById('nav02').className='sidebar-item ';
                break;
            case 'page3':
                document.getElementById('nav03').className='sidebar-item ';
                break;
            case 'page4':
                document.getElementById('nav04').className='sidebar-item ';
                break;
            case 'page5':
                document.getElementById('nav05').className='sidebar-item ';
                break;
            case 'pageSetting':
                document.getElementById('navSetting').className='sidebar-item ';
                break;
        }
    });
									