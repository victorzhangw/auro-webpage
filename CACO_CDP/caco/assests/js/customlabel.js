var L10n = ej.base.L10n;
L10n.load({
    'cn-TW': {
        'querybuilder': {
            'AddGroup': '加入群組',
            'AddCondition': '增加條件',
            'DeleteRule': '刪除規則',
            'DeleteGroup': '刪除群組',
            'Edit': '編輯',
            'SelectField': '選擇欄位',
            'DeleteRule': '刪除規則',
            'DeleteGroup': '刪除群組',
            'SelectOperator': '選擇運算子',
            'StartsWith': '由此開始',
            'EndsWith': '由此結束',
            'Contains': '包含',
            'Equal': '等於',
            'NotEqual': '不等於',
            'LessThan': '少於',
            'LessThanOrEqual': '少於或等於',
            'GreaterThan': '大於',
            'GreaterThanOrEqual': '大於或等於',
            'Between': '之間',
            'NotBetween': '不在這之間',
            'In': '在',
            'NotIn': '不在',
            'Remove': '移除',
            'ValidationMessage': '驗證訊息',
            'Is Null': 'NULL',
            
        }
    }
});

var columnData =  [
    { field: 'totaldays', label: '時間區段', type: 'number' },
    { field: 'Name', label: 'Name', type: 'string' },
    { field: 'Category', label: 'Category', type: 'string' },
    { field: 'SerialNo', label: 'Serial No', type: 'string' },
    { field: 'InvoiceNo', label: 'Invoice No', type: 'string' },
    { field: 'Status', label: 'Status', type: 'string' }
];
    var importRules = {
    'condition': 'or',
    'rules': [{
        'label': '時間區段',
        'field': 'totaldays',
        'type': 'number',
        'operator': 'equal',
        'value': '1'
    }]
};

var hardwareData = [{
  'TaskID': 1,
  'Name': 'Lenovo Yoga',
  'Category': 'Laptop',
  'SerialNo': 'CB27932009',
  'InvoiceNo': 'INV-2878',
  'Status': 'Assigned'
},
{
  'TaskID': 2,
  'Name': 'Acer Aspire',
  'Category': 'Others',
  'SerialNo': 'CB35728290',
  'InvoiceNo': 'INV-3456',
  'Status': 'In-repair'
},
{
  'TaskID': 3,
  'Name': 'Apple MacBook',
  'Category': 'Laptop',
  'SerialNo': 'CB35628728',
  'InvoiceNo': 'INV-2763',
  'Status': 'In-repair'
}];
var isLabel=false; // check  label exist; if label is exist,the manual paging not work ! 
const prefix =sessionStorage.getItem("sitePrefix")+"-";
DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();
const memberGridpageSize=25;
ej.grids.Grid.Inject(ej.grids.Page, ej.grids.Sort, ej.grids.Filter, ej.grids.Resize);


var dashboard = new ej.layouts.DashboardLayout({
    columns: 16,
    allowDragging: false,
    cellSpacing: [1, 1],
    cellAspectRatio: 100 / 100,
    panels: [
        {
            'sizeX': 8,
            'sizeY': 15,
            'row': 0,
            'col': 0,
            header: '<div>客戶樣貌</div>',
            content: ' <div class="row"><div class="col-xs-12 offset-md-1 col-md-10" style="padding-top:8px"><input id="filled"></div></div>  <div id="selector1" class="chart-responsive" ></div>'
            
        
            
       
            
        
        },
        {
            'sizeX': 8,
            'sizeY': 15,
            'row': 0,
            'col': 8,
            header: '<div>受眾列表</div>',
            content: '<div id="grid1" class="chart-responsive" ></div>'
        }



    ]
});
const gridUrl = LabelremoteUrl + "GetGridData";

//Render initialized Accordion component

dashboard.appendTo('#editLayout');

$(document).ready(function () {
    var filledTextbox = new ej.inputs.TextBox({
        placeholder: '輸入資料天數(EX: 180)',
        cssClass: 'e-filled',
        floatLabelType: 'Auto',
    });
    filledTextbox.appendTo('#filled');
    
    var qryBldrObj1 = new ej.querybuilder.QueryBuilder({
        width: '100%',
        dataSource: hardwareData,
        locale: 'cn-TW',
        columns: columnData,
        rule: importRules,
        showButtons:{ ruleDelete: true , groupInsert: false, groupDelete: false }
    });
    qryBldrObj1.appendTo('#selector1');
    



});

function validateSetting() {
    let bearerToken = sessionStorage.getItem("token");
    let urlTo = AuthremoteUrl + "GetUserName";
    let Setting = {
        async: true,
        type: "GET",
        url: urlTo,
        headers: {
            Authorization: "Bearer" + " " + bearerToken

        }
    };
    return Setting;
}






