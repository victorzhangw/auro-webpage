loginUrl="/login.html";
const AuthremoteUrl = "https://localhost:5001/api/AuthenticationService/";
const localurl = "http://127.0.0.1:5501/";
var sitePrefix = "";
var siteDefine = [{
        "name": "caco",
        "prefix": "caco",
        "memberKey": "45488F6FA45856B7FA5184B3676B8",
        "GoogleremoteUrl": "https://localhost:5001/api/GoogleService/",
        "localurl": "http://127.0.0.1:5501/caco/",
        "AuthremoteUrl": "https://localhost:5001/api/AuthenticationService/",
        "TealeafremoteUrl": "https://localhost:5001/api/TealeafEvent/",
        "LabelremoteUrl": "https://localhost:5001/api/GridProvider/",
        "ApiremoteUrl": "https://localhost:5001/api/",

    },
    {
        "name": "waveshine",
        "prefix": "wave",
        "memberKey": "45488F6FA45856B7FA5184B3676B8",
        "GoogleremoteUrl": "https://localhost:5001/api/GoogleService/",
        "localurl": "http://127.0.0.1:5501/waveshine/",
        "AuthremoteUrl": "https://localhost:5001/api/AuthenticationService/",
        "TealeafremoteUrl": "https://localhost:5001/api/TealeafEvent/",
        "LabelremoteUrl": "https://localhost:5001/api/GridProvider/",
        "ApiremoteUrl": "https://localhost:5001/api/",
    },
    
    {
        "name": "demosite",
        "prefix": "demo",
        "memberKey": "45488F6FA45856B7FA5184B3676B8",
        "GoogleremoteUrl": "https://localhost:5001/api/GoogleService/",
        "localurl": "http://127.0.0.1:5501/demosite/",
        "AuthremoteUrl": "https://localhost:5001/api/AuthenticationService/",
        "TealeafremoteUrl": "https://localhost:5001/api/TealeafEvent/",
        "LabelremoteUrl": "https://localhost:5001/api/GridProvider/",
        "ApiremoteUrl": "https://localhost:5001/api/",
    }

];

