DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();
const widgetitems_l = (document.getElementById('dataWidget_l'));
const widgetitems_r = (document.getElementById('dataWidget_r'));
const chartitems = (document.getElementById('dataChart'));
const prefix =sessionStorage.getItem("sitePrefix")+"-";
var refIndex1 = 30000; // 訪客數參考指標
var L10n = ej.base.L10n;
var dp,DataObj, Dataobj1;

const gridColumn = [{
    field: 'ADGroup',
    headerText: '廣告群組',
    textAlign: 'center',
    width: 100,
    format: 'N'
  },
  {
    field: 'impression',
    headerText: '曝光',
    textAlign: 'left',
    width: 100,
    format: 'N'
  },
  {
    field: 'adClick',
    width: 90,
    headerText: '廣告點擊',
    format: 'N'
  },
  {
    field: 'adCost',
    width: 100,
    headerText: '廣告成本',
    format: 'N',
    
  },
  {
    field: 'CPM',
    headerText: 'CPM',
    textAlign: 'left',
    width: 100,
    format: 'N'
  },
  {
    field: 'CPC',
    headerText: 'CPC',
    textAlign: 'left',
    width: 100,
    format: 'N'
  },
  {
    field: 'costPerGoalConversion',
    headerText: '廣告轉換成本',
    textAlign: 'left',
    width: 120,
    format: 'N'
  },
  {
    field: 'costPerTransaction',
    headerText: '交易成本',
    textAlign: 'left',
    width: 100,
    format: 'N'
  },
];
var customgridColumn = [{
  field: 'referral',
  headerText: '參考來源',
  textAlign: 'center',
  width: 100,
  type: 'string'
},
{
  field: 'sessions',
  width: 80,
  headerText: '工作階段',
  type: 'N'
},
{
  field: 'uniquePageviews',
  width: 100,
  headerText: '不重複瀏覽量',
  type: 'N'
},
{
  field: 'users',
  width: 100,
  headerText: '訪客數',
  type: 'string',
  template: '#gridTemplate'
},
{
  field: 'newUsers',
  headerText: '新訪客數',
  textAlign: 'left',
  width: 100,
  format: 'N'
},
{
  field: 'bounces',
  headerText: '跳出數',
  textAlign: 'left',
  width: 100,
  format: 'N'
},
{
  field: 'avgSessionDuration',
  headerText: '平均停留時間',
  textAlign: 'left',
  width: 100,
  format: 'string'
}
];
var symbols = [
  'path://M203.5 1.1C200.9 1.7 198.4 2.4 197.9 2.6 197.5 2.9 193.6 3.6 189.4 4.2 185.2 4.7 180 5.9 178 6.6 166.2 11.1 157 21.8 152 36.8 149.4 44.8 147.9 54.8 147.4 69 147.1 75.8 146.4 83.7 145.7 86.6 144.6 91.7 141.5 98.6 138.9 101.6 134.3 107 124.8 109.8 111 109.7 102.7 109.7 101.3 110.5 108.8 111 110.4 111.1 109.3 111.4 105.8 111.8 93.8 113.2 83.3 116.3 79.1 119.6 76.9 121.4 76.9 121.4 83.3 119.2 86.8 118 90.8 117.1 92.3 117.1 94.2 117.1 94.4 117.3 93 117.5 88.1 118.4 80.6 121.3 77 123.7 71.6 127.4 68.8 132.4 68.4 138.4 68 144 69.2 147.4 70.1 143 70.6 140.1 73.9 136.8 76.9 136.1 79.2 135.4 84.7 136.1 85.6 137 86 137.5 85.4 138.9 83.9 140.7 81.1 144.2 78.8 148.7 78.8 150.5 78.8 151.4 79.9 150.7 82.4 148.1 84.4 146 87.2 143.8 88.6 143L91.1 141.7 88.9 143.3C79.2 150.6 74.3 160.6 74.3 173 74.3 176.2 74.6 178.5 75 178.5 75.4 178.5 75.8 178 75.8 177.4 75.8 176.2 78.5 170.3 79.1 170.3 79.3 170.3 78.8 171.8 77.9 173.8 74.6 181.2 77.3 193.1 83.6 198.6 86.9 201.5 88 201.7 86.6 199 85.4 196.6 85.8 193.8 88.3 188.6L90 184.9 90.1 195.4 90.8 191.1C91.4 187.4 93.2 182.3 94.1 182.3 94.2 182.3 93.8 184.4 93.1 187.1 91.8 193.2 92.4 198.6 94.9 203.6L96.8 207.4 96.4 203.9C96.2 201.9 96.4 200.1 96.8 199.9 97.2 199.6 97.5 200.6 97.5 202.1 97.5 205.1 100.1 212.5 101.5 213.6 102.2 214.3 102.4 213.3 102.1 209.2 101.7 201.8 103.1 195.8 106.9 187.9 109.9 181.6 116.7 172 117.5 172.8 117.7 172.9 116.6 174.8 115.2 176.9 108.1 186.6 104.9 198.4 105.7 211.9L105.9 215.6 106.9 211.1C108.1 205.6 110.1 200.1 110.2 202.1 110.3 202.8 109.7 205.5 109.1 208.1 106.6 217.1 109 230.9 113.8 236.1 115.1 237.5 115.5 237.8 115.2 236.8 115 236 114.6 234 114.4 232.3 114 229.6 114.1 229.8 115.4 233.7 116.1 236.2 117.2 238.9 117.8 239.8 118.7 241.1 118.8 244.4 118.4 259.9 117.9 280.4 117.4 286.1 111.4 327.9 109.1 343.4 106.8 361.8 106.1 368.8 103.5 395.9 103.2 397.3 95.6 410.9 94 413.9 93 416.6 93 418.4 93 421 93.3 421.4 96.4 422.9L99.7 424.4 99.9 429.9 100.1 435.4 103.5 435.6C105.4 435.8 107.1 436.1 107.4 436.4 108.2 437.2 105.9 456.4 103.8 466.4 101.9 475.4 101.9 475.6 103.1 479.4 104.8 484.6 110 494.9 116.8 506.4 121.9 515.3 122.6 516 125.5 517L128.6 518 129.1 525.1C129.4 529 130.1 543.8 130.5 558 130.9 572.3 131.9 590.1 132.5 597.8 134.4 618.8 134.3 652 132.1 673.5 131.3 682.8 129.6 703.5 128.6 719.6 118.7 876.1 116.7 910.5 117.3 911.1 117.5 911.3 121.5 912.4 126.1 913.6 132.5 915.2 136 916.6 140.4 919.3 143.7 921.3 146.3 923.3 146.3 923.9 146.3 924.6 145.9 926.6 145.4 928.5 144.4 932.9 146.4 941.5 149.1 944.4 153.4 949.1 165.4 950.7 172.5 947.6 180 944.1 183 938.4 183 927.4L183 921.2 189.6 918.2C193.2 916.6 197.4 914.8 198.9 914.3 201.7 913.4 201.7 913.4 201.3 909.9 201.1 907.9 200.5 895.7 199.9 882.8 197.6 834.5 195.6 803.3 192.8 765.4 188.3 706.6 187.9 698.7 187.9 669 187.9 638.8 188.6 629.6 193.5 583.9 198.1 541.9 200.1 521.1 201 508.5 202.2 490.4 203.5 482.5 206.1 477.4L207.2 475.2 214.9 494.6C219.1 505.4 226.6 524.8 231.8 537.8 236.9 550.7 245.9 573.4 251.8 588 271.1 635.3 279.4 658.2 284.6 677.3 291.5 702.9 303.9 771.9 319.9 874.5 323.9 899.9 327.2 920.7 327.3 920.9 327.5 921 353.7 930.1 360.4 932.4 362.8 933.1 362.8 933.3 363.3 939.1 364.3 950.6 368 956.5 375.9 958.9 384.7 961.4 391.1 959.8 396.9 953.3 401.4 948.3 402.7 943.5 401.3 936.4 400.9 933.8 400.3 931.1 400.1 930.5 399.8 929.9 401.3 928.4 404.3 926.6 409.7 923 411.8 920.2 411.8 915.7 411.8 910 407.3 893.3 388.1 826.1 376.1 784.3 375.2 781.4 358.9 732.5 346.3 694.6 344.3 687.5 335.3 648 327.8 615.7 316.6 567.8 307.5 530 303.8 514.6 299.7 496.4 298.3 489.4 295.6 476.3 294.4 472.4 284.5 448.5L279.4 436.1 278.9 418.1C278.6 408.2 278.2 398.3 277.9 395.9 277.4 391.8 277.4 391.7 279.7 390.8 281.9 389.9 282.1 389.9 284.9 392.5 291 397.9 298.6 398.2 308.3 393.2L314.2 390.1 316.9 393.6C318.5 395.6 320 397.4 320.3 397.8 320.6 398.2 323.2 396.8 326 394.7 331.6 390.6 334.5 388.9 337.6 388.2 341.7 387.2 347.1 378.2 353.9 361.4 355.9 356.3 360 347 362.9 340.9 373.9 316.6 376.8 307.4 376.4 296.6L376.1 289.9 368.7 274.5C355.1 246.2 346.5 225.2 335.3 192.4 327.9 170.9 326 166.6 321.6 161.3 319.7 159 317.7 156.1 317.1 154.9 315 150.8 313 149.6 308.8 149.8 306.7 149.9 300.5 148.9 294.8 147.7 288.7 146.3 281.4 145.2 276.5 145 269.6 144.6 268.5 144.4 268.8 143.3 279.9 107.3 281.1 80.3 272.8 55.9 265.8 35.4 248.5 12.8 234.3 5.4 224.2 0.2 214.2-1.2 203.5 1.1ZM307.6 257.6C312.2 266.6 316.4 276.7 322.8 294.5 328.1 309.1 329.4 315.1 328.4 321 327.8 324.6 317.6 346.2 315.1 349.1 314.4 349.8 312.1 351.2 310 352.2 306 354 303.8 356.3 303.8 358.6 303.8 359.4 304.9 361.4 306.4 363L309.1 366 305.3 369.4C302.3 372.1 301.5 373.4 301.6 374.9 301.9 376.6 301.6 376.9 299.4 377.1 297.1 377.3 296.3 376.7 291.1 371.8 287.9 368.8 284.3 365.6 282.9 364.9 280.8 363.6 280.5 363 280 358.6 279 350.3 279.4 331.1 280.8 321.8 282.8 308.7 285.8 294.6 290.9 275.6 293.4 266.1 295.9 255.5 296.6 252 298.7 240.8 298.3 241.4 301.3 246 302.7 248.3 305.6 253.4 307.6 257.6ZM307.6 257.6',
  'path://M269 4.2C267.6 4.6 264.9 5.6 262.9 6.4 259.7 7.8 259.2 8 256.5 7.8 250 7.5 248.4 7.6 247.6 8.4 246.3 9.5 245.2 14.1 242.9 27.5 242.1 32.3 241.5 34.3 241 34.9 240.4 35.6 240.1 37 239.6 42.2 239.2 45.7 238.8 48.7 238.7 48.9 238.5 49 236.8 49.7 234.9 50.3 231.5 51.5 228.4 53.2 228.4 53.8 228.4 55.7 234 55.6 243.3 53.6 244.8 53.3 245.3 54.5 244.1 55.4 243.4 55.8 243.3 56.3 243.3 57.9 243.3 61.3 244.1 67.3 244.9 69.5 245.3 70.7 245.8 72.4 246 73.4 246.2 74.3 246.7 75.5 247 76 247.3 76.5 247.6 77.5 247.6 78.4 247.6 83.2 249.1 87.4 252.3 90.9L254.4 93.2 255.5 98.4C257.4 107.9 257.8 119.3 256.4 122.7 255.2 125.6 246.5 132.2 242.1 133.5 238.5 134.6 234.1 137.4 231.5 140.1 228.2 143.6 226.6 147.2 225.5 153.8 225 156.7 224.4 158.4 222.6 162.2 219.8 168 218.9 171.3 218.1 177.8 217.7 181 217.2 183.4 216.8 184.3 216.3 185.2 216 186.1 216 186.4 216 187.3 220.1 193.3 222 195 224.6 197.5 224.7 198.2 224.2 210.1 224 216.9 223.9 221.4 224.2 223.3 224.6 227 226.1 232.1 228.2 237.2 229.7 241 229.7 241.3 229.7 245.4L229.7 249.7 227.7 253.4C225.6 257.7 224.6 261 223.2 269 221.2 280 219.1 289.2 217.8 292.8 216.2 297.1 213.1 304.5 211.4 307.9 210.8 309.2 209.5 312.3 208.6 314.7 206.7 319.8 205.4 322.4 203.8 324.8 203.1 325.7 201.7 328.9 200.5 331.8 198.2 337.7 198.2 337.4 197.1 351.6 196.3 361.4 196.3 364.7 197.2 368.6 198.2 372.8 200.3 376.4 201.8 376.4 202.1 376.4 202.7 376 203.2 375.4 204.7 373.7 205.1 374.7 205.1 380.5 205.1 393.4 207.4 412.4 210.6 424.4 211.5 427.7 212.5 431.9 213 433.7L213.8 436.9 265.8 436.9 266 434.4C266.2 431.4 268.4 426.3 269.3 426.5 269.7 426.5 270.5 428.3 271.5 431.7L273.1 436.9 337.1 436.9 337.4 432.9C337.8 426.8 336.7 412.2 334.7 395.9 334.4 393.5 333 386.8 331.6 381 329.2 371.3 328.5 367.5 328 362 327.9 360.3 328.3 357.5 329.8 350 331.6 341.5 331.9 339.4 332.2 333.1 332.6 324 332.3 317.1 331.2 310.7 330.5 306.8 330.4 305.5 330.7 304.6 331 303.7 330.9 302.7 329.7 298.7 327.1 290.1 326.3 288.9 323 288.9 321.8 288.9 321.6 288.8 320.9 286.9 319.2 282.5 319.2 281.6 319.1 271 319.1 264.7 318.9 259.3 318.5 257.1 317.9 253 318.2 249.6 319.2 247.2 319.6 246.2 321.2 243.4 322.7 241.1 325.6 236.5 327.2 233.6 328.6 229.9 329.1 228.6 329.7 227.7 329.9 227.8 330.2 227.8 330.8 229.3 331.4 231 332.7 235 334.7 239 337 242 339.6 245.6 339.9 246.6 340.6 252.8 340.9 255.8 341.5 260 341.9 262.2 342.3 264.3 342.8 268.5 343 271.4 343.6 278 343.8 279.4 346.4 289.7 351.1 308.4 354 327.1 354 338 354 343.1 353.7 344 351.4 345.8 346.6 349.4 344.8 352.2 342 360.5 340.6 364.3 340.4 365.5 340.2 369.4 340.1 371.8 339.7 375.2 339.4 376.9L338.7 380 339.9 383.4C340.5 385.2 341.2 387.7 341.3 388.8 341.6 391.3 342.3 392 344 392 345.1 392 345.5 391.8 346 390.8 347.1 388.6 347.5 385.5 346.9 382 346.1 376.8 347.4 371.7 349.5 371.7 350.4 371.7 350.8 374.3 351 380.7 351.2 386.4 351.1 387.1 350.1 391.2 348.8 396.2 347.4 398.5 344 401 341.4 402.9 340.5 404.6 341.1 406.2 341.4 407 341.7 407.2 342.7 407.2 343.5 407.2 344.2 407.6 344.6 408.1L345.2 408.8 348.3 408C350 407.5 351.9 407 352.5 406.7 354.3 406.2 359.2 402.1 362.8 398.5 365.6 395.7 366.1 394.9 366.7 392.9 367 391.7 367.8 389.9 368.4 388.9 369 388 369.6 386.4 369.8 385.4 369.9 384.4 370.5 381.9 371.2 379.9L372.3 376.1 372.3 362.5C372.3 347.4 372.8 329.3 373.5 322.7 373.7 320.3 374.5 313.9 375.3 308.3 376.3 300.7 376.7 296.1 376.9 290.3 377.1 281 376.7 276.2 374.6 265.1 373.8 260.7 373 255.3 372.7 253.1 372.4 250.7 371.7 247.4 370.9 244.9 368.2 236.7 367.6 226.6 368.5 206.1 368.9 195.6 368.9 195.2 368.1 192 367.6 190 366.3 186.6 364.9 183.8 363.4 180.7 362.5 178.2 362.2 176.5 361.9 175.1 361.6 169.4 361.4 163.8 361.2 155.5 360.9 153.1 360.3 150.7 358.5 144.3 355.5 138.8 351.4 134.2 350.1 132.8 348.2 130.6 347 129.2 344.6 126.4 341.7 124.5 334.2 120.5 318.1 111.9 305.3 103.1 301.5 98 299.9 95.7 296.9 90.4 295.9 87.9 295.3 86.3 295.3 85.5 295.6 81.3 296 75.7 296.3 74.4 297.4 74.8 298.5 75.1 300.1 74.2 301.2 72.5 301.8 71.6 302.9 70.1 303.5 69.2 305.7 66.4 306.1 64.7 305.9 59.6 305.8 54.3 305.2 53 302.8 52.8 302 52.7 301.3 52.6 301.3 52.4 301.2 50.4 301.3 41.3 301.4 41.2 301.5 41.2 303.8 40.5 306.6 39.7 312.8 37.9 313.4 37.5 313.4 35.7 313.4 33.8 312 33.1 306.9 32.9L302.9 32.7 301.7 29.5C300.3 26 300 25.4 299.1 25.4 298.7 25.4 297.5 23.9 296.1 21.6 293 16.5 288.7 10 287 7.8 286.1 6.7 284.8 5.7 283.1 4.9 280.7 3.8 280.1 3.7 276 3.6 272.6 3.6 270.9 3.7 269 4.2ZM269 4.2 ',
  'path://M512 292.205897c80.855572 0 146.358821-65.503248 146.358821-146.358821C658.358821 65.503248 592.855572 0 512 0 431.144428 0 365.641179 65.503248 365.641179 146.358821 365.641179 227.214393 431.144428 292.205897 512 292.205897zM512 731.282359c-80.855572 0-146.358821 65.503248-146.358821 146.358821 0 80.855572 65.503248 146.358821 146.358821 146.358821 80.855572 0 146.358821-65.503248 146.358821-146.358821C658.358821 796.273863 592.855572 731.282359 512 731.282359z'
];


$(document).ready(function () {
    
    DramaCore.validateTokenCall(validateSetting());
    
    let sdt = moment().subtract(8, 'days').format("YYYY-MM-DD");
    let edt = moment().subtract(1, 'days').format("YYYY-MM-DD");
    dp = DramaCore.createDatePicker(sdt, edt);
    let key =prefix+"ga-" + sdt + "-" + edt ;
    //let defaultkey= "ga-" + sessionStorage.getItem(sdt) + "-" + sessionStorage.getItem(defaultEndDate);
    dp.value=[sdt,edt];  
    dp.addEventListener("change",function()
    {
      let _dayrange = this.getSelectedRange();
      let _result="" ;
     
        if (_dayrange.daySpan > 0) {
          let _sd = moment(this.startDate).format("YYYY-MM-DD");
          let _ed = moment(this.endDate).format("YYYY-MM-DD");
          //sessionStorage.setItem(defaultStartDate ,_sd);
          //sessionStorage.setItem(defaultEndDate, _ed);
          //sessionStorage.setItem('daySpan', _dayrange.daySpan);
          let url = GoogleremoteUrl + 'CustomGAReport';
          let type = "post";
          let para = {
            "MemberKey": memberKey,
            "StartDate": _sd,
            "EndDate": _ed,
            "LoginId": loginID,
            "ReqDimension1":CustomGARequest.GA_Dimensions1.join(),
            "ReqMetric1":CustomGARequest.GA_Metrics1.join(),
            "ReqDimension2":CustomGARequest.GA_Dimensions2.join(),
            "ReqMetric2":CustomGARequest.GA_Metrics2.join(),
            "ReqDimension3":CustomGARequest.GA_Dimensions3.join(),
            "ReqMetric3":CustomGARequest.GA_Metrics3.join(),
            "ReqDimension4":CustomGARequest.GA_Dimensions4.join(),
            "ReqMetric4":CustomGARequest.GA_Metrics4.join(),
            "ReqDimension5":CustomGARequest.GA_Dimensions5.join(),
            "ReqMetric5":CustomGARequest.GA_Metrics5.join()
          };
          if (_sd && _ed) {
            let newKey =prefix+ "ga-" + _sd + "-" + _ed + "-" + _dayrange.daySpan;
            let newecKey = prefix+"gaec-" + _sd + "-" + _ed + "-" + _dayrange.daySpan;
            
            if (localStorage.getItem(newKey)) {
              initCustomGAPage.widget();
              initCustomGAPage.chart();
              DramaCore.renderCustomGAChart(newKey);
              document.getElementById("_ad01").click();
            } else {
              url = GoogleremoteUrl + 'GAReport';
              
              para = {
                "MemberKey": memberKey,
                "StartDate": _sd,
                "EndDate": _ed,
                "LoginId": loginID
              };
              global_custom_Key=DramaCore.getRemoteData(url, type, para,newKey,fromGA);
              
              
              let timeoutID = window.setInterval(function(){
                if(global_custom_Key){
                  window.clearInterval(timeoutID);
                  let ecurl=GoogleremoteUrl + 'ECReport';
                  global_EC_Key= DramaCore.getSilenceRemoteData(ecurl, type, para,newecKey,fromEC);
                  initCustomGAPage.widget();
                  initCustomGAPage.chart();
                  DramaCore.renderCustomGAChart(global_custom_Key);
                  document.getElementById("_ad01").click();
                  global_custom_Key="";
                }
              },1000);
                 
            }

          }
        }
    });
   
 
});



var initCustomGAPage={
  widget:function(){
    let  widgetMetaData=[
      {id:'w1',icons:'e-icons session'},
      {id:'w2',icons:'e-icons views'},
      {id:'w3',icons:'e-icons profile'},
      {id:'w4',icons:'e-icons profile'},
      {id:'w5',icons:'e-icons avgtime'},
      {id:'w6',icons:'e-icons session1'},
      {id:'w7',icons:'e-icons profile1'},
      {id:'w8',icons:'e-icons bounce'}
    
  ];
  let  widgetMetaData2=[
    {id:'w9',icons:'e-icons session',chartid:'w9c'},
    {id:'w10',icons:'e-icons views',chartid:'w10c'},
    {id:'w11',icons:'e-icons profile',chartid:'w11c'},
    {id:'w12',icons:'e-icons profile',chartid:'w12c'},
    {id:'w13',icons:'e-icons avgtime',chartid:'w13c'},
    {id:'w14',icons:'e-icons session1',chartid:'w14c'},
    {id:'w15',icons:'e-icons profile1',chartid:'w15c'},
    {id:'w16',icons:'e-icons bounce',chartid:'w16c'}
  
];
var getWidgetStringl = ej.base.compile('<div class="col-sm-6 col-md-3 mb-1"> <div id=${id} class="card card_animation"> <div class="card-header widget-head "> <h5 class="widget-text align-middle"></h5> </div><div class="card-body"> <span class="${icons}"></span> <h5 class="widget-number align-middle"></h5> </div></div></div>');
  var getWidgetStringr = ej.base.compile('<div class="col-sm-6 col-md-3 mb-1"> <div id=${id} class="card card_animation"> <div class="card-header widget-head "> <h5 class="widget-text align-middle"></h5> </div><div class="card-body"> <span id="${chartid}" class="widgetchartbox"></span> <h5 class="widget-number align-middle"></h5> </div></div></div>');
  
  while (widgetitems_l.firstChild) {
    widgetitems_l.removeChild(widgetitems_l.firstChild);
  }
  widgetMetaData.forEach(data => {
    widgetitems_l.appendChild(getWidgetStringl(data)[0]);
  });
  while (widgetitems_r.firstChild) {
    widgetitems_r.removeChild(widgetitems_r.firstChild);
  }
  widgetMetaData2.forEach(data => {
    widgetitems_r.appendChild(getWidgetStringr(data)[0]);
  });
  },
  chart:function(){
      let  chartMetaData=[
          
          {id:'chart2',size:'col-md-12'},
          {id:'chart4',size:'col-md-12'},
          {id:'chart9',size:'col-md-4'},
          {id:'chart1',size:'col-md-4'},
          {id:'chart3',size:'col-md-4'},
          {id:'chart5',size:'col-md-4'},
          {id:'chart6',size:'col-md-4'},
          {id:'chart10',size:'col-md-4'},
          {id:'chart7',size:'col-md-4'},
          {id:'chart8',size:'col-md-4'},
          {id:'chart11',size:'col-md-4'}
      ];
      let getChartString = ej.base.compile('<div class="col-sm-12 ${size} mb-1 d-flex"> <div id=${id} class="card card_animation flex-fill"> <div class="card-header widget-head"> </div><div class="charts card-body chart-height"> </div> </div>  </div>');
      while (chartitems.firstChild) {
        chartitems.removeChild(chartitems.firstChild);
      }
      chartMetaData.forEach(data => {
          chartitems.appendChild(getChartString(data)[0]);
      });
  }

};

// 客製化表格欄位值


var CustomCharts ={
  chart1: function (data,location, rowMax, header,chartid) {
    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
    
    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[5].value;
    }
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
    let color = ["#4A90E2"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: []
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart2: function (data,location, rowMax, header,chartid) {

    let rst = data[location],
      dimension1 = [],
      metric1 = [],
      rowcount,
      metric2 = [];

    if (rst.ListResult.length > rowMax) {
      rowcount = rowMax;
    } else {
      rowcount = rst.ListResult.length;
    }
    for (let i = 0; i < rowcount; i++) {
      let arr = rst.ListResult[i];
      dimension1.push(arr.dimension[0]);
      metric1.push(arr.metrics[0].metric[3].value);
      metric2.push(arr.metrics[1].metric[3].value);
    }
    let color = ["#4A90E2", "#FB7507"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension1,
      value1: metric1,
      value2: metric2,
      color: color,
      legend: ["上期 7 日", "本期 7 日"]
    };

    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.doubleBarChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart3: function (data,location, rowMax, header,chartid) {

    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];

    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[6].value;
    }

    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
    let color = ["#FB7507"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: []
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart4: function (data,location, rowMax, header,chartid) {
    let rst = data[location],
      chartObj = {},
      list = [],
      metric = [];

    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj = {};
      chartObj.referral = rst.ListResult[i].dimension[0];

      for (let j = 0; j < rst.ListResult[i].metrics[1].metric.length; j++) {
        chartObj.sessions =Number( rst.ListResult[i].metrics[0].metric[0].value);
        chartObj.pageviewsPerSession = Number( rst.ListResult[i].metrics[0].metric[1].value);
        chartObj.newUsers = Number(rst.ListResult[i].metrics[0].metric[2].value);
        chartObj.users = Number(rst.ListResult[i].metrics[0].metric[3].value);
        chartObj.avgSessionDuration = rst.ListResult[i].metrics[0].metric[4].value;
        chartObj.avgTimeOnPage = Number(rst.ListResult[i].metrics[0].metric[5].value);
        chartObj.bounces =Number( rst.ListResult[i].metrics[0].metric[6].value);
        chartObj.uniquePageviews = Number(rst.ListResult[i].metrics[0].metric[6].value);

      }
      list.push(chartObj);

    }

    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      customiseCell:function(args){
        if (args.column.field === 'users') {
          let _var = args.data.users + ' 人';
          args.cell.querySelector("#gridTmp").textContent = _var;
          if (parseInt(args.data.users) >= refIndex1) {
            args.cell.querySelector("#gridTmp").classList.add("smileStatus");
      
          }
      
        }
      },
      gridColumn: customgridColumn
    };
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart4 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
  },
  chart5: function (data,location, rowMax, header,chartid) {
    let rst = data[location],
      _femalechartObj = {},
      _malechartObj = {},
      list = [],
      _femalenewUserCount = 0,
      _femaleuserCount = 0,
      _malenewUserCount = 0,
      _maleuserCount = 0,
      metric = [];
    for (let i = 0; i < rst.ListResult.length; i++) {

      switch (rst.ListResult[i].dimension[0]) {
        case "female":

          _femalenewUserCount += parseInt(rst.ListResult[i].metrics[0].metric[2].value);
          _femaleuserCount += parseInt(rst.ListResult[i].metrics[0].metric[3].value);

          break;
        case "male":

          _malenewUserCount += parseInt(rst.ListResult[i].metrics[0].metric[2].value);
          _maleuserCount += parseInt(rst.ListResult[i].metrics[0].metric[3].value);

          break;
      }


    }
    _femalechartObj.gender = "female";
    _femalechartObj.users = _femaleuserCount;
    _femalechartObj.newUsers = _femalenewUserCount;
    list.push(_femalechartObj);
    _malechartObj.gender = "male";
    _malechartObj.users = _maleuserCount;
    _malechartObj.newUsers = _malenewUserCount;
    list.push(_malechartObj);
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      symbol:symbols,
      color: [],

    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.doubleCustomSymbolBarChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart6: function (data,location, rowMax, header,chartid) {

    let rst = data[location],
    list = [],
    femaleUser =[],
    femaleNewUser =[],
    femaleAge = [],
    maleUser = [],
    maleNewUser = [],
    maleAge = [],
    femaleObj = {};
    maleObj = {};
  
  for(var i=0;i<rst.ListResult.length;i++){
    switch (rst.ListResult[i].dimension[0]) {
      case"female":
        femaleUser.push(rst.ListResult[i].metrics[0].metric[3].value);
        femaleNewUser.push(rst.ListResult[i].metrics[0].metric[2].value);
        femaleAge.push(rst.ListResult[i].dimension[1]);
      break;
      case"male":
        maleUser.push(-Math.abs(rst.ListResult[i].metrics[0].metric[3].value));//minus number
        maleNewUser.push(-Math.abs( rst.ListResult[i].metrics[0].metric[2].value));
        maleAge.push(rst.ListResult[i].dimension[1]);
      break;
    }
    
}
    femaleObj.gender="女性";
    femaleObj.user=femaleUser;
    femaleObj.seriesName="女性";
    femaleObj.seriesValue=femaleNewUser;
    femaleObj.yaxisValue=femaleAge;
    maleObj.gender="男性";
    maleObj.user=maleUser;
    maleObj.seriesName="男性";
    maleObj.seriesValue=maleNewUser;
    maleObj.yaxisValue=maleAge;

  let setting = {
    legend:["女性","男性"],
    cardHead: header,
    titleText: "",
    value1: femaleObj,
    value2: maleObj,
    color: ['rgba(255,130,130)', 'rgba(105,204,230)' ],

  };
  let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.doubleHorizontalSplitBarChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart7: function (data,location, rowMax, header,chartid) {
    let rst = data[location],
      totalUserbyDevice=0,
      chartObj = {},
      mobileAry = [],
      desktopAry = [],
      tabletAry = [];

      for(var i=0;i<rst.ListResult.length;i++){
        
        switch(rst.ListResult[i].dimension[0]){
          case"desktop":
            desktopAry[0]=(rst.ListResult[i].metrics[0].metric[3].value);
          break;
          case"mobile":
            mobileAry[0]=(rst.ListResult[i].metrics[0].metric[3].value);
          break;
          case"tablet":
            tabletAry[0]=(rst.ListResult[i].metrics[0].metric[3].value);
          break;
        }
        totalUserbyDevice +=parseInt(rst.ListResult[i].metrics[0].metric[3].value);
      }
      desktopAry[1]=(parseInt(desktopAry[0])/parseInt(totalUserbyDevice));
      desktopAry[2]=(1-(parseInt(desktopAry[0])/parseInt(totalUserbyDevice)));
      desktopAry[3]="桌機";
      mobileAry[1]=(parseInt(mobileAry[0])/parseInt(totalUserbyDevice));
      mobileAry[2]=(1-(parseInt(mobileAry[0])/parseInt(totalUserbyDevice)));
      mobileAry[3]="手機";
      tabletAry[1]=(parseInt(tabletAry[0])/parseInt(totalUserbyDevice));
      tabletAry[2]=(1-(parseInt(tabletAry[0])/parseInt(totalUserbyDevice)));
      tabletAry[3]="平板";
      let dataStyle = {
        normal: {
            label: {
                show: false
            },
            labelLine: {
                show: true
            },
            shadowBlur: 0,
            shadowColor: '#203665'
        }
    };
      let setting = {
        cardHead: header,
        titleText: "",
        value1: desktopAry,
        value2: mobileAry,
        value3: tabletAry,
        dataStyle:dataStyle,
        unit:"人",
        color1: ["#5c684f","#FB7507","#9b9557"],
        color2: ["#7ED321","#b57744","#F8E71C"],
    
      };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.tripleCirclePieChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart8: function (data,location, rowMax, header,chartid) {
    let rst = data[location],
      totalDurationTimebyDevice=0,
      chartObj = {},
      mobileAry = [],
      desktopAry = [],
      tabletAry = [];

      for(var i=0;i<rst.ListResult.length;i++){
        
        switch(rst.ListResult[i].dimension[0]){
          case"desktop":
            desktopAry[0]=Math.round((rst.ListResult[i].metrics[0].metric[4].value)/60);
          break;
          case"mobile":
            mobileAry[0]=Math.round((rst.ListResult[i].metrics[0].metric[4].value)/60);
            
          break;
          case"tablet":
            tabletAry[0]=Math.round((rst.ListResult[i].metrics[0].metric[4].value)/60);
          break;
        }
        totalDurationTimebyDevice +=parseInt(rst.ListResult[i].metrics[0].metric[4].value);
      }
      desktopAry[1]=(parseInt(desktopAry[0])/parseInt(totalDurationTimebyDevice));
      desktopAry[2]=(1-(parseInt(desktopAry[0])/parseInt(totalDurationTimebyDevice)));
      desktopAry[3]="桌機";
      mobileAry[1]=(parseInt(mobileAry[0])/parseInt(totalDurationTimebyDevice));
      mobileAry[2]=(1-(parseInt(mobileAry[0])/parseInt(totalDurationTimebyDevice)));
      mobileAry[3]="手機";
      tabletAry[1]=(parseInt(tabletAry[0])/parseInt(totalDurationTimebyDevice));
      tabletAry[2]=(1-(parseInt(tabletAry[0])/parseInt(totalDurationTimebyDevice)));
      tabletAry[3]="平板";
      let dataStyle = {
        normal: {
            label: {
                show: false
            },
            labelLine: {
                show: true
            },
            shadowBlur: 0,
            shadowColor: '#203665'
        }
    };
      let setting = {
        cardHead: header,
        titleText: "",
        value1: desktopAry,
        value2: mobileAry,
        value3: tabletAry,
        dataStyle:dataStyle,
        unit:"分",
        color1: ["#5c684f","#FB7507","#9b9557"],
        color2: ["#7ED321","#b57744","#F8E71C"],
    
      };
      let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.tripleCirclePieChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart9: function (data,location, rowMax, header,chartid) {
    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
    
    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[2].value;
    }
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
    let color = ["#7ED321"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: []
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart10: function (data,location, rowMax, header,chartid) {

    let rst = data[location],
    list = [],
    femaleBrowseTime =[],
    femaleAge = [],
    maleBrowseTime = [],
    maleAge = [],
    femaleObj = {};
    maleObj = {};
  
  for(var i=0;i<rst.ListResult.length;i++){
    switch (rst.ListResult[i].dimension[0]) {
      case"female":
      femaleBrowseTime.push(rst.ListResult[i].metrics[0].metric[5].value);
   
        femaleAge.push(rst.ListResult[i].dimension[1]);
      break;
      case"male":
      maleBrowseTime.push(-Math.abs(rst.ListResult[i].metrics[0].metric[5].value));//minus number
        
        maleAge.push(rst.ListResult[i].dimension[1]);
      break;
    }
    
}
    femaleObj.gender="女性";
    femaleObj.seriesName="女性";
    femaleObj.seriesValue=femaleBrowseTime;
    femaleObj.yaxisValue=femaleAge;
    maleObj.gender="男性";
    maleObj.seriesName="男性";
    maleObj.seriesValue=maleBrowseTime;
    maleObj.yaxisValue=maleAge;

  let setting = {
    legend:["女性","男性"],
    cardHead: header,
    titleText: "",
    value1: femaleObj,
    value2: maleObj,
    color: ['rgba(255,130,130)', 'rgba(105,204,230)' ],

  };
  let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.doubleHorizontalSplitBarChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart11: function (data,location, rowMax, header,chartid) {
    let rst = data[location],
      totalDurationTimebyDevice=0,
      chartObj = {},
      mobileAry = [],
      desktopAry = [],
      tabletAry = [];

      for(var i=0;i<rst.ListResult.length;i++){
        
        switch(rst.ListResult[i].dimension[0]){
          case"desktop":
            desktopAry[0]=Math.round((rst.ListResult[i].metrics[0].metric[5].value));
          break;
          case"mobile":
            mobileAry[0]=Math.round((rst.ListResult[i].metrics[0].metric[5].value));
            
          break;
          case"tablet":
            tabletAry[0]=Math.round((rst.ListResult[i].metrics[0].metric[5].value));
          break;
        }
        totalDurationTimebyDevice +=parseInt(rst.ListResult[i].metrics[0].metric[5].value);
      }
      desktopAry[1]=(parseInt(desktopAry[0])/parseInt(totalDurationTimebyDevice));
      desktopAry[2]=(1-(parseInt(desktopAry[0])/parseInt(totalDurationTimebyDevice)));
      desktopAry[3]="桌機";
      mobileAry[1]=(parseInt(mobileAry[0])/parseInt(totalDurationTimebyDevice));
      mobileAry[2]=(1-(parseInt(mobileAry[0])/parseInt(totalDurationTimebyDevice)));
      mobileAry[3]="手機";
      tabletAry[1]=(parseInt(tabletAry[0])/parseInt(totalDurationTimebyDevice));
      tabletAry[2]=(1-(parseInt(tabletAry[0])/parseInt(totalDurationTimebyDevice)));
      tabletAry[3]="平板";
      let dataStyle = {
        normal: {
            label: {
                show: false
            },
            labelLine: {
                show: true
            },
            shadowBlur: 0,
            shadowColor: '#203665'
        }
    };
      let setting = {
        cardHead: header,
        titleText: "",
        value1: desktopAry,
        value2: mobileAry,
        value3: tabletAry,
        dataStyle:dataStyle,
        unit:"秒",
        color1: ["#5c684f","#FB7507","#9b9557"],
        color2: ["#7ED321","#b57744","#F8E71C"],
    
      };
      let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.tripleCirclePieChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  }
};
function validateSetting(){
    let bearerToken =sessionStorage.getItem("token");
    let urlTo= AuthremoteUrl+"GetUserName";
    let Setting={
        async: true,
        type: "GET",
        url: urlTo,
        headers: {
            Authorization: "Bearer" +" "+ bearerToken
           
          }
    };
    return Setting;
}

