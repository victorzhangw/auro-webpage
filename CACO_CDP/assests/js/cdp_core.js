ej.base.enableRipple(true);
navData = [
  {
    id: 'nav005',
    iconText: '今日',
    navUrl: 'default.html',
    iconType: 'fas fa-plane-departure'
  },
  {
    id: 'nav010',
    iconText: '流量',
    navUrl: 'flow.html',
    iconType: 'fas fa-chart-line'
  },
  {
    id: 'nav020',
    iconText: '體驗',
    navUrl: 'interactive.html',
    iconType: 'far fa-hand-pointer'
  },
  {
    id: 'nav030',
    iconText: '交易GA',
    navUrl: 'Ecommerce.html',
    iconType: 'fas fa-dumpster-fire '
  },
  {
    id: 'nav030',
    iconText: '交易ERP',
    navUrl: 'EcommercePOS.html',
    iconType: 'fas fa-dumpster-fire '
  },
  {
    id: 'nav040',
    iconText: '標籤',
    navUrl: 'label.html',
    iconType: 'fas fa-tags'
  },

];
const CustomGARequest = {
  GA_Dimensions1: ["date", "sourceMedium"],
  GA_Metrics1: ["uniquePageviews", "bounceRate", "users", "newUsers", "sessions", "pageviewsPerSession", "avgSessionDuration", "avgTimeOnPage"],
  GA_Dimensions2: ["sourceMedium"],
  GA_Metrics2: ["uniquePageviews", "bounceRate", "users", "newUsers", "sessions", "pageviewsPerSession", "avgSessionDuration", "avgTimeOnPage"],
  GA_Dimensions3: ["userGender", "userAgeBracket"],
  GA_Metrics3: ["uniquePageviews", "bounceRate", "users", "newUsers", "sessions", "pageviewsPerSession", "avgSessionDuration", "avgTimeOnPage"],
  GA_Dimensions4: ["deviceCategory"],
  GA_Metrics4: ["uniquePageviews", "bounceRate", "users", "newUsers", "sessions", "pageviewsPerSession", "avgSessionDuration", "avgTimeOnPage"],
  GA_Dimensions5: [],
  GA_Metrics5: [],

};
const GoogleremoteUrl = sessionStorage.getItem("GoogleremoteUrl");
const TealeafremoteUrl = sessionStorage.getItem("TealeafremoteUrl");
const LabelremoteUrl = sessionStorage.getItem("LabelremoteUrl");
const memberKey = sessionStorage.getItem("memberKey");
const fromEC = "EC"; //指定由何種頁面帶入 key
const fromGA = "GA"; //指定由何種頁面帶入 key
const fromTealeaf = "Tealeaf"; //指定由何種頁面帶入 key
const fromLabel = "Label"; //指定由何種頁面帶入 key
const siteprefix = sessionStorage.getItem("sitePrefix");
/*
$(document).on({
  ajaxStart: function () {
    $("body").addClass("loading");
  },
  ajaxStop: function () {
    $("body").removeClass("loading");
  }
});*/

const sideNavitems = (document.getElementById('sideNav'));
const defaultStartDate = "defaultStartDate";
const defaultEndDate = "defaultEndDate";
const daySpan = "daySpan";
const loginID = "loginID";
const pagesets = "pagesetting"
const WidgetCounts = 8; // widget 總數
const InteractiveWidgetCounts = 12; // widget 總數
const InteractiveWidgetHead1 = "合計訂單金額"; // widget 總數
const InteractiveWidgetHead2 = "訂單數量"; // widget 總數
const InteractiveWidgetHead3 = "合計商品件數"; // widget 總數
const InteractiveWidgetHead4 = "平均訂單商品件數"; // widget 總數
const ECWidgetCounts = 4;
const defaultChartCounts = 6; // chart 總數
var global_custom_Key, global_EC_Key, global_Tealeaf_Key, global_Label_key;


var chartSetting = {
  singleBarChart: function (p) {
    let option = {
      title: {
        text: p.titleText
      },
      tooltip: {},
      legend: {
        data: p.legend
      },
      grid: {
        top: 20,

      },
      xAxis: {
        type: 'category',
        data: p.category,

        axisLabel: {
          interval: 0,
          rotate: 35,
          fontSize: 10,
          formatter: function (param) {

            var res = param.split("/");
            return res[1];
          }
        }


      },
      yAxis: {
        type: 'value',
        splitLine: { //控制軸線
          show: false,
        },
        axisLabel: {
          formatter: function (value, index) {
            // 將數值轉換為 K,M
            function intlFormat(num) {
              return new Intl.NumberFormat().format(Math.round(num * 10) / 10);
            }

            if (value >= 1000000)
              return intlFormat(value / 1000000) + 'M';
            if (value >= 1000)
              return intlFormat(value / 1000) + 'k';
            return intlFormat(value);

          }
        }

      },
      series: [{

        type: 'bar',
        barWidth: '50%',
        data: p.value,
        itemStyle: {
          normal: {
            barBorderRadius: 4,
            color: p.color[0]
          }
        },
        label: {
          normal: {
            show: true,
            position: 'inside',
            fontSize: 11,
          }
        },
      }]
    };
    return option;
  },
  doubleBarChart: function (p) {
    let option = {
      title: {

      },

      legend: {
        data: p.legend
      },
      grid: {
        left: '0',
        right: '1%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: {
        type: 'value',
        boundaryGap: [0, 0.1]
      },
      yAxis: {
        type: 'category',
        data: p.category
      },
      series: [{
        name: p.legend[0],
        type: 'bar',
        data: p.value1,
        itemStyle: {
          color: '#FB7507'

        }
      },
      {
        name: p.legend[1],
        type: 'bar',
        data: p.value2,
        itemStyle: {
          color: '#4A90E2'

        }
      }
      ]
    };
    return option;
  },
  doubleBarChartVertical: function (p) {

    let option = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      legend: {
        data: p.legend,

      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: {
        type: 'category',
        data: p.category,
        axisLabel: {
          fontSize: 10,
          color: '#333',
          margin: 4,
          interval: 0,
          formatter: function (val) {
            return val.split("").join("\n");
          }
        }
      },
      yAxis: {
        type: 'value',
        boundaryGap: [0, 0.01],
        axisLabel: {
          formatter: function (value, index) {
            // 將數值轉換為 K,M
            function intlFormat(num) {
              return new Intl.NumberFormat().format(Math.round(num * 10) / 10);
            }

            if (value >= 1000000)
              return intlFormat(value / 1000000) + 'M';
            if (value >= 1000)
              return intlFormat(value / 1000) + 'k';
            return intlFormat(value);

          }
        }
      },
      series: [

        {
          name: p.legend[0],
          type: 'bar',
          barMaxWidth: '25%',
          color: p.color[0],
          data: p.value1
        },
        {
          name: p.legend[1],
          type: 'bar',
          barMaxWidth: '25%',
          color: p.color[1],
          data: p.value2
        }
      ]
    };
    return option;
  },
  fourQuadrant: function (p) {

    let option = {
      tooltip: {
        trigger: 'item',
        axisPointer: {
          show: true,
          type: 'cross',
          lineStyle: {
            type: 'dashed',
            width: 1
          },
        },
        position: function (pos, params, dom, rect, size) {
          var obj = {
            top: 60
          };
          obj[['left', 'right'][+(pos[0] < size.viewSize[0] / 2)]] = 5;
          return obj;
        },
        formatter: function (obj) {
          if (obj.componentType == "series") {
            return '<div style="border-bottom: 1px solid rgba(255,255,255,.3); font-size: 18px;padding-bottom: 7px;margin-bottom: 7px">' +
              obj.name +
              '</div>' +
              '<span>' +
              p.legend[1] +
              '</span>' +
              ' : ' + obj.value[0] + p.formatterString[1] +
              '<br/>' +
              '<span>' +
              p.legend[0] +
              '</span>' +
              ' : ' + obj.value[1] + p.formatterString[0];
          }
        }
      },
      xAxis: {
        name: p.legend[1],
        nameLocation: 'middle',
        nameGap: 25,
        type: 'value',
        scale: true,
        axisLabel: {
          formatter: '{value} ',
          fontSize: 10

        },
        splitLine: {
          show: false
        },
        axisLine: {
          lineStyle: {
            color: p.color[0]
          }
        }
      },
      yAxis: {
        name: p.legend[0],

        type: 'value',
        scale: true,
        axisLabel: {
          formatter: '{value}' + p.formatterString[0],
          fontSize: 10
        },
        splitLine: {
          show: false
        },
        axisLine: {
          lineStyle: {
            color: p.color[0]
          }
        }
      },

      series: [{
        type: 'scatter',
        data: p.seriesdata,
        symbolSize: 12,

        markLine: {
          label: {
            normal: {
              formatter: function (params) {
                if (params.dataIndex == 1) {
                  return params.value + p.formatterString[0];
                } else if (params.dataIndex == 0) {
                  return params.value + p.formatterString[1];
                }
                return params.value;
              }
            }
          },
          lineStyle: {
            normal: {
              color: p.color[1],
              type: 'solid',
              width: 1,
            },
            emphasis: {
              color: p.color[2]
            }
          },
          data: [{
            xAxis: p.avg.xAvgLine,
            name: p.legend[1] + '平均線',
            itemStyle: {
              normal: {
                color: p.color[3],
              }
            }
          }, {
            yAxis: p.avg.yAvgLine,
            name: p.legend[0] + '平均線',
            itemStyle: {
              normal: {
                color: p.color[3],
              }
            }
          }]
        },
        color: p.color[4]

      }]
    };
    return option;
  },
  gridChart: function (p) {
    let pageSize = 5, pageCount = 5, allowResizing = false;
    if (p.pageSize) {
      pageSize = p.pageSize;

    }
    if (p.pageCount) {
      pageCount = p.pageCount;
    }
    if (p.allowResizing) {
      allowResizing = p.allowResizing;
    }

    ej.grids.Grid.Inject(ej.grids.Page, ej.grids.Sort, ej.grids.Filter);
    let grid = new ej.grids.Grid({
      dataSource: p.value,
      columns: p.gridColumn,
      rowHeight: 30,
      allowResizing: allowResizing,
      allowPaging: true,
      allowExcelExport: true,
      toolbar: [{ text: '匯出', tooltipText: 'Download excel', prefixIcon: "e-excelexport", id: 'ExcelExport' }],
      pageSettings: {
        pageSize: pageSize,
        pageCount: pageCount
      },
      allowSorting: true,
      queryCellInfo: p.customiseCell
    });
    grid.toolbarClick = function (args) {
      if (args.item.id === 'ExcelExport') {



        let excelExportProperties = {
          fileName: siteprefix + "_" + p.cardHead + "_資料時間_" + sessionStorage.getItem(defaultStartDate) + ".xlsx",
          //dataSource:list
        };
        // grid1.excelExport(getExcelExportProperties());
        grid.excelExport(excelExportProperties);


      }
    };
    return grid;
  },

  doubleCustomSymbolBarChart: function (p) {
    let bodyMaxA = (parseInt(p.value[0].users) + parseInt(p.value[1].users)); //指定圖形界限值
    let bodyMaxB = (parseInt(p.value[0].newUsers) + parseInt(p.value[1].newUsers)); //指定圖形界限值
    let labelSetting = {
      normal: {
        show: true,
        position: 'bottom',
        offset: [0, 10],
        formatter: function (param) {

          return (param.data.value2) + '人';
        },
        textStyle: {
          fontSize: 18,
          fontFamily: 'Microsoft JhengHei, PingFang TC',
          color: '#686868'
        }
      }
    };

    let markLineSetting = { //设置标线
      symbol: 'none',
      lineStyle: {
        normal: {
          opacity: 0.3
        }
      },
      data: [{
        type: 'max',
        label: {
          normal: {
            formatter: 'max: {c}'
          }
        }
      }, {
        type: 'min',
        label: {
          normal: {
            formatter: 'min: {c}'
          }
        }
      }]
    };
    option = {
      tooltip: {
        show: false, //鼠标放上去显示悬浮数据
      },
      legend: {
        data: ['訪客', '新訪客'],
        selectedMode: 'single',
        itemWidth: 10, //图例的宽度
        itemHeight: 10, //图例的高度
        itemGap: 30,
        orient: 'horizontal',
        left: 'center',
        top: '10px',
        icon: 'circle',
        // selectedMode: false, //取消图例上的点击事件
        textStyle: {
          color: '#808492'
        },
      },
      grid: {
        // left: '20%',
        // right: '20%',
        top: '20%',
        bottom: '20%',
        containLabel: true
      },
      xAxis: {
        data: ['a', 'x', 'b'],
        axisTick: {
          show: false
        },
        axisLine: {
          show: false
        },
        axisLabel: {
          show: false
        }
      },
      yAxis: {
        max: 100,

        splitLine: {
          show: false
        },
        axisTick: {
          // 刻度线
          show: false
        },
        axisLine: {
          // 轴线
          show: false
        },
        axisLabel: {
          // 轴坐标文字
          show: false
        }
      },
      series: [{
        name: '訪客',
        type: 'pictorialBar',
        symbolClip: true,
        symbolBoundingData: 100,
        label: labelSetting,
        data: [{
          value: p.value[0].users / bodyMaxA * 100,
          value2: p.value[0].users,
          symbol: p.symbol[0],

          itemStyle: {
            normal: {
              color: 'rgba(255,130,130)' //单独控制颜色
            }
          },
        },
        {

        },
        {
          value: p.value[1].users / bodyMaxA * 100,
          value2: p.value[1].users,
          symbol: p.symbol[1],

          itemStyle: {
            normal: {
              color: 'rgba(105,204,230)' //单独控制颜色
            }
          },
        }
        ],
        // markLine: markLineSetting,
        z: 10
      },
      {
        name: '新訪客',
        type: 'pictorialBar',
        symbolClip: true,
        symbolBoundingData: 100,
        label: labelSetting,
        data: [{
          value: p.value[0].newUsers / bodyMaxB * 100,
          value2: p.value[0].newUsers,
          symbol: p.symbol[0],

        },
        {},
        {
          value: p.value[1].newUsers / bodyMaxB * 100,
          value2: p.value[1].newUsers,
          symbol: p.symbol[1]
        }
        ],
        // markLine: markLineSetting,
        z: 10
      },
      {
        // 设置背景底色，不同的情况用这个
        name: 'full',
        type: 'pictorialBar', //异型柱状图 图片、SVG PathData
        symbolBoundingData: 100,
        animationDuration: 0,
        itemStyle: {
          normal: {
            color: '#ccc' //设置全部颜色，统一设置
          }
        },
        z: 10,
        data: [{
          itemStyle: {
            normal: {
              color: 'rgba(255,130,130,0.40)' //单独控制颜色
            }
          },
          value: 100,
          symbol: p.symbol[0],

        },
        {
          // 设置中间冒号
          itemStyle: {
            normal: {
              color: 'rgba(71, 211, 52,0.4)' //单独控制颜色
            }
          },
          value: 100,
          symbol: p.symbol[2],
          symbolSize: [8, '18%'],
          symbolOffset: [0, '-200%']
        },
        {
          itemStyle: {
            normal: {
              color: 'rgba(105,204,230,0.40)' //单独控制颜色
            }
          },
          value: 100,
          symbol: p.symbol[1],

        }
        ]
      }
      ]
    };
    return option;
  },
  doubleHorizontalSplitBarChart: function (p) {
    option = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      legend: {
        data: p.lengend,

      },

      color: p.color,
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: [{
        type: 'value',
        show: false
      }],
      yAxis: [{
        type: 'category',
        axisTick: {
          show: false
        },
        data: p.value1.yaxisValue.reverse()
      }],

      series: [

        {
          name: p.value1.seriesName,
          type: 'bar',
          stack: '合計',
          label: {
            normal: {
              show: true,
              position: 'right',
              fontSize: 11
            }
          },
          itemStyle: {
            normal: {
              barBorderRadius: 2,

            }
          },
          data: p.value1.seriesValue.reverse()
        },
        {
          name: p.value2.seriesName,
          type: 'bar',
          stack: '合計',
          itemStyle: {
            normal: {
              barBorderRadius: 2,

            }
          },
          label: {
            normal: {
              show: true,
              position: 'left',
              fontSize: 11,
              formatter: function (params) {
                return -params.value;
              }
            },

          },
          data: p.value2.seriesValue.reverse()
        }
      ]
    };
    return option;
  },
  tripleCirclePieChart: function (p) {
    let option = {
      //backgroundColor: "#20263f",
      series: [{
        name: 'circle1',
        type: 'pie',
        clockWise: true,
        radius: [43, 60],
        itemStyle: p.dataStyle,
        hoverAnimation: true,
        center: ['16%', '50%'],
        data: [{
          value: p.value1[1],
          label: {
            normal: {
              rich: {
                a: {
                  color: "#000",
                  align: 'center',
                  fontSize: 14,
                  fontWeight: "bold"
                },
                b: {
                  color: "#000",
                  align: 'center',
                  fontSize: 14
                }
              },
              formatter: function (params) {

                return "{b|" + p.value1[3] + "}\n\n" + "{a|" + p.value1[0] + p.unit + "}";
              },
              position: 'center',
              show: true,
              textStyle: {
                fontSize: '14',
                fontWeight: 'normal',
                color: '#fff'
              }
            }
          },
          itemStyle: {
            normal: {
              color: p.color2[0],
              shadowColor: p.color2[0],
              shadowBlur: 0
            }
          }
        }, {
          value: p.value1[2],
          name: 'invisible',
          itemStyle: {
            normal: {
              color: p.color1[0]
            },
            emphasis: {
              color: p.color1[0]
            }
          }
        }]
      }, {
        name: 'circle2',
        type: 'pie',
        clockWise: true,
        radius: [43, 60],
        itemStyle: p.dataStyle,
        hoverAnimation: false,
        center: ['50%', '50%'],
        data: [{
          value: p.value2[2],
          label: {
            normal: {
              rich: {
                a: {
                  color: "#000",
                  align: 'center',
                  fontSize: 14,
                  fontWeight: "bold"
                },
                b: {
                  color: "#000",
                  align: 'center',
                  fontSize: 14
                }
              },
              formatter: function (params) {
                return "{b|" + p.value2[3] + "}\n\n" + "{a|" + p.value2[0] + p.unit + "}";
              },
              position: 'center',
              show: true,
              textStyle: {
                fontSize: '14',
                fontWeight: 'normal',
                color: '#fff'
              }
            }
          },
          itemStyle: {
            normal: {
              color: p.color1[1],
              shadowColor: p.color1[1],
              shadowBlur: 0
            }
          }
        }, {
          value: p.value2[2],
          name: 'invisible',
          itemStyle: {
            normal: {
              color: p.color2[1]
            },
            emphasis: {
              color: p.color2[1]
            }
          }
        }]
      }, {
        name: 'circle3',
        type: 'pie',
        clockWise: false,
        radius: [43, 60],
        itemStyle: p.dataStyle,
        hoverAnimation: false,
        center: ['84%', '50%'],
        data: [{
          value: p.value3[2],
          label: {
            normal: {
              rich: {
                a: {
                  color: "#000",
                  align: 'center',
                  fontSize: 14,
                  fontWeight: "bold"
                },
                b: {
                  color: "#000",
                  align: 'center',
                  fontSize: 14
                }
              },
              formatter: function (params) {
                return "{b|" + p.value3[3] + "}\n\n" + "{a|" + p.value3[0] + p.unit + "}";
              },
              position: 'center',
              show: true,
              textStyle: {
                fontSize: '14',
                fontWeight: 'normal',
                color: '#fff'
              }
            }
          },
          itemStyle: {
            normal: {
              color: p.color1[2],
              shadowColor: p.color1[2],
              shadowBlur: 0
            }
          }
        }, {
          value: p.value1[2],
          name: 'invisible',
          itemStyle: {
            normal: {
              color: p.color2[2]
            },
            emphasis: {
              color: p.color2[2]
            }
          }
        }]
      }]
    };
    return option;

  },
  singleBarChartwithDataTable: function (p) {
    let option = {
      title: {
        text: p.titleText
      },
      tooltip: {},
      legend: {
        data: p.legend
      },
      toolbox: {
        show: true,
        feature: {
          dataView: {
            show: true,
            buttonColor: p.color[0],
            readOnly: true
          }
        },
        left: '80%',

        optionToContent: function (opt) {
          let headerStr = JSON.stringify(p.cardHead.toString().replace(/【|】|\s|\//gi, ""));
          //let headerStr = str.replace(/\[|\]/gi, "");
          let axisData = opt.xAxis[0].data;
          let series = opt.series;
          let table = '<table id="charttable' + p.chartid + '" class="table-bordered table-striped" style="width:95%;text-align:center;font-size:14px;">';
          table = table + '<tbody><tr>' + '<td>&nbsp;</td>' + '<td> <button class="btn btn-info btn-sm" onclick=DramaCore.exportCSV("charttable' + p.chartid + '",' + headerStr + ')>匯出</button></td>' + '</tr>';
          for (var i = 0, l = axisData.length; i < l; i++) {
            table += '<tr >' + '<td style="text-align:left">&nbsp;' + axisData[i] + '</td>' + '<td>' + series[0].data[i] + '</td>' + '</tr>';
          }
          table += '</tbody>';
          return table;
        }
      },
      grid: {
        top: 20,

      },
      xAxis: {
        type: 'category',
        data: p.category,

        axisLabel: {
          interval: 0,
          rotate: 35,
          fontSize: 10,
          formatter: function (param) {
            var res = param.split("/");
            return res[1];
          }
        }


      },
      yAxis: {
        type: 'value',
        splitLine: { //控制軸線
          show: false,
        }

      },
      series: [{

        type: 'bar',
        barWidth: '50%',
        data: p.value,
        itemStyle: {
          normal: {
            barBorderRadius: 4,
            color: p.color[0]
          }
        },
        label: {
          normal: {
            show: true,
            position: 'inside',
            fontSize: 11,
            formatter: function (params) {
              console.log(params);
              if (params.data >= 1000) {

                return DramaCore.numberWithCommas(params.value);
              }
            }
          },

        },
      }]
    };
    return option;
  },
  singleBarChartwithAvgLine: function (p) {
    option = {
      xAxis: {
        type: 'category',
        data: p.category,
        show: true,
        axisTick: {
          show: false
        },
        axisLabel: {
          fontSize: 9,
          color: '#333',
          rotate: 25,
        },
      },
      yAxis: {

        type: 'value'
      },
      tooltip: {
        trigger: 'axis'
      },
      series: [{
        data: p.value,
        barMaxWidth: '100%',
        itemStyle: {
          normal: {
            color: function (params) {
              var num = p.color.length;
              return p.color[params.dataIndex % num];
            }
          }
        },
        markPoint: {
          data: [{
            type: 'max',
            name: '最大值'
          },
          {
            type: 'min',
            name: '最小值'
          }
          ]
        },
        markLine: {
          data: [{
            type: 'average',
            name: '平均值'
          }]
        },
        type: 'bar'
      }]
    };
    return option;
  },
  twMapwithPieChart: function (p) {
    option = {
      /*
      title: {
          //text: '各城市/店鋪銷售量',
          //subtext: 'Ecommerce',
         // sublink: '',
          left: 'right'
      },*/
      tooltip: {
        trigger: 'item',
        showDelay: 0,
        transitionDuration: 0,
        formatter: function (params) {
          var value = (params.value + '').split('.');
          value = value[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,');
          return params.seriesName + '<br/>' + params.name + ': ' + value;
        }
      },
      visualMap: {

        min: 0,
        max: 29000,
        itemWidth: 14,
        inRange: {
          color: p.color
        },
        top: 'bottom',
        left: 'left',
        text: ['High', 'Low'], // 文本，默认为数值文本
        calculable: true
      },
      toolbox: {
        show: false,
        //orient: 'vertical',
        left: 'left',
        top: 'top',
        feature: {
          dataView: {
            readOnly: false
          },
          restore: {},
          saveAsImage: {}
        }
      },

      series: [{
        name: 'sales',
        type: 'map3D',
        roam: false,
        map: 'TAIWAN',
        aspectScale: 0.8,
        bottom: 40,
        left: 0,

        itemStyle: {
          emphasis: {
            label: {
              show: true
            }
          }
        },
        light: {
          main: {
            intensity: 1,
            shadow: true,
            alpha: 150,
            beta: 70
          }
        },
        ambient: {
          intensity: 0
        },
        postEffect: { //為畫面新增高光，景深，環境光遮蔽（SSAO），調色等效果
          enable: true, //是否開啟
          SSAO: { //環境光遮蔽
            radius: 1, //環境光遮蔽的取樣半徑。半徑越大效果越自然
            intensity: 1, //環境光遮蔽的強度
            enable: true
          }
        },
        temporalSuperSampling: { //分幀超取樣。在開啟 postEffect 後，WebGL 預設的 MSAA 會無法使用,分幀超取樣用來解決鋸齒的問題
          enable: true
        },
        viewControl: { //用於滑鼠的旋轉，縮放等視角控制
          distance: 208, //預設視角距離主體的距離
          zoomSensitivity: 0,
          rotateMouseButton: 'right', //旋轉操作使用的滑鼠按鍵
          alpha: 31 // 讓canvas在x軸有一定的傾斜角度
        },
        data: p.value1
      }]
    };
    return option;
  },
  singleCirclePiewithSymbol: function (p) {
    option = {
      backgroundColor: '#fff',
      tooltip: {
        trigger: 'item',
        formatter: '<span style="color:yellow">{b}</span></br> 金額：{c} </br>佔比： {d}%  '
      },
      graphic: {
        elements: [{
          type: 'image',
          style: {
            image: p.symbol,
            width: 90,
            height: 90,
          },
          left: 'center',
          top: '50%'
        }]
      },
      legend: {
        orient: 'vertical',
        x: '1%',
        y: 'bottom',
        itemWidth: 15,
        itemHeight: 15,
        align: 'right',
        textStyle: {
          fontSize: 14,
          color: '#000'
        },
        data: p.lengend
      },
      series: [{
        type: 'pie',
        radius: ['39%', '65%'],
        center: ['50%', '60%'],
        color: p.color2,
        data: p.value2,
        labelLine: {
          normal: {
            show: false,
            length: 20,
            length2: 20,
            lineStyle: {
              color: '#12EABE',
              width: 2
            }
          }
        },
        label: {
          normal: {
            show: false,

            rich: {
              b: {
                fontSize: 20,
                color: '#12EABE',
                align: 'left',
                padding: 4
              },
              hr: {
                borderColor: '#12EABE',
                width: '100%',
                borderWidth: 2,
                height: 0
              },
              d: {
                fontSize: 20,
                color: '#fff',
                align: 'left',
                padding: 4
              },
              c: {
                fontSize: 20,
                color: '#fff',
                align: 'left',
                padding: 4
              }
            }
          }
        }
      }]
    };
    return option;
  },
  wordCloud: function (p) {
    let toolbarShow = false;
    if (p.toolbar === true) {
      toolbarShow = true;
    }
    option = {
      tooltip: {},
      toolbox: {
        show: toolbarShow,
        feature: {
          dataView: {
            show: true,
            // buttonColor: p.color[0],
            readOnly: true
          }
        },
        left: '80%',

        optionToContent: function (opt) {
          let headerStr = JSON.stringify(p.cardHead.toString().replace(/【|】|\s|\//gi, ""));


          let table = '<table id="charttable' + p.chartid + '" class="table-bordered table-striped" style="width:95%;text-align:center;font-size:14px;">';
          table = table + '<tbody><tr>' + '<td>&nbsp;</td>' + '<td> <button class="btn btn-info btn-sm" onclick=DramaCore.exportCSV("charttable' + p.chartid + '",' + headerStr + ')>匯出</button></td>' + '</tr>';
          for (var i = 0, l = p.value.length; i < l; i++) {
            table += '<tr >' + '<td style="text-align:left">&nbsp;' + p.value[i].name + '</td>' + '<td>' + p.value[i].value + '</td>' + '</tr>';
          }
          table += '</tbody>';
          return table;
        }
      },
      series: [{
        type: 'wordCloud',
        gridSize: 2,
        sizeRange: [14, 60],
        rotationRange: [-45, 90],
        shape: 'diamond', // circle star heart .... see ref
        width: '100%',
        height: '100%',
        textStyle: {
          normal: {
            color: function () {
              return 'rgb(' + [
                Math.round(Math.random() * 160),
                Math.round(Math.random() * 160),
                Math.round(Math.random() * 160)
              ].join(',') + ')';
            }
          },
          emphasis: {
            shadowBlur: 10,
            shadowColor: '#333'
          }
        },
        data: p.value
      }]
    };
    return option;
  },
  funnelChart: function (p) {

    let toolbarShow = false;
    if (p.toolbar === true) {
      toolbarShow = true;
    }
    option = {
      color: p.color,
      tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} 人"
      },
      toolbox: {
        show: toolbarShow,
        feature: {
          dataView: {
            show: true,
            // buttonColor: p.color[0],
            readOnly: true
          }
        },
        left: '95%',
        top: 0,
        optionToContent: function (opt) {
          let headerStr = JSON.stringify(p.cardHead.toString().replace(/【|】|\s|\//gi, ""));
          //let headerStr = str.replace(/\[|\]/gi, "");


          let table = '<table id="charttable' + p.chartid + '" class="table-bordered table-striped" style="width:95%;text-align:center;font-size:14px;">';
          table = table + '<tbody><tr>' + '<td>&nbsp;</td>' + '<td> <button class="btn btn-info btn-sm" onclick=DramaCore.exportCSV("charttable' + p.chartid + '",' + headerStr + ')>匯出</button></td>' + '</tr>';
          for (var i = 0, l = p.metric.length; i < l; i++) {
            table += '<tr >' + '<td style="text-align:left">&nbsp;' + p.metric[i].name + '</td>' + '<td>' + p.metric[i].value + '</td>' + '</tr>';
          }
          table += '</tbody>';
          return table;
        }
      },
      legend: {
        data: p.legend,
        orient: "vertical",
        left: 0,
        bottom: 10,
        itemWidth: 15,
        itemHeight: 5,
        textStyle: {
          fontSize: 10
        },
      },
      series: [{
        name: ' ',
        type: 'funnel',
        height: '100%',
        left: '5%',
        top: 10,
        bottom: 0,
        width: '80%',
        label: {
          normal: {
            formatter: '{b}'
          },
          emphasis: {
            position: 'inside',
            formatter: ''
          }
        },
        labelLine: {
          normal: {
            show: false
          }
        },
        itemStyle: {
          normal: {
            opacity: 0.6
          }
        },
        data: p.metric
      },
      {
        name: '',
        type: 'funnel',
        height: '100%',
        left: '5%',
        top: 10,
        bottom: 0,
        width: '80%',
        maxSize: '85%',
        label: {
          normal: {
            position: 'left',
            formatter: '{c}人',
            textStyle: {
              color: '#ff0000',

            }
          },
          emphasis: {
            position: 'inside',
            formatter: ''
          }
        },
        itemStyle: {
          normal: {
            opacity: 0.5,
            borderColor: '#fff',
            borderWidth: 2
          }
        },
        data: p.metric
      }
      ]
    };
    return option;
  },
  syncSemipie: function (p) {
    let pieoption = {
      //Initializing Series
      series: [
        {
          dataSource: p.datasource,
          xName: 'name',
          yName: 'value',
          startAngle: 270,
          endAngle: 90,
          radius: '70%', explode: false,
          innerRadius: '40%',
          name: p.name,
          dataLabel: {
            visible: false, position: 'Inside',
            connectorStyle: { length: '10%' }, name: 'text',
            font: { size: '14px' }
          },
        }
      ],
      enableAnimation: true,
      //Initializing Tooltip
      tooltip: { enable: true, format: '${point.x} : <b>${point.y}</b>' },
      legendSettings: {
        visible: false,
      },
      //Initializing Title
      title: p.title,
      background: "rgba(255, 255, 255, 0)",
      height: '90px',
      width: '110px',

    };
    return pieoption;
  },
  sankey:function(p){
    option = {
      series: {
          type: 'sankey',
          layout: 'none',
          focusNodeAdjacency: 'allEdges',
          data: p.data,
          links:  p.links
      }
  };
  }
};
var DramaCore = {
  sideBar: function () {
    let getNavstring = ej.base.compile('<li id=${id}><a href=${navUrl} class="text-center "><i class="${iconType} sidebarIcon"></i><span class="sidebarText ">${iconText}</span> </a> </li>');
    navData.forEach(data => {
      sideNavitems.appendChild(getNavstring(data)[0]);
    });
  },
  loadCultureFiles: function (name) {
    var files = ['ca-gregorian.json', 'numbers.json', 'timeZoneNames.json'];
    var loader = ej.base.loadCldr;
    var loadCulture = function (prop) {
      var val, ajax;
      ajax = new ej.base.Ajax(localurl + 'assests/js/' + files[prop], 'GET', false);
      ajax.onSuccess = function (value) {
        val = value;
      };
      ajax.send();
      loader(JSON.parse(val));
    };
    for (var prop = 0; prop < files.length; prop++) {
      loadCulture(prop);
    }
  },
  // 建立日期區間選擇器
  createDatePicker: function (startDate, endDate) {
    L10n.load({
      'zh': {
        'daterangepicker': {
          placeholder: "輸入日期區間",
          text: startDate + " - " + endDate,
          startLabel: '起始日期',
          endLabel: '截止日期',
          applyText: '確認',
          cancelText: '取消',
          selectedDays: '選擇日期',
          days: '天',
          customRange: '自定義區間'

        }
      }
    });
    Date.prototype.GetFirstDayOfWeek = function () {
      return (new Date(this.setDate(this.getDate() - this.getDay() + (this.getDay() == 0 ? -6 : 1))));
    };
    Date.prototype.GetLastDayOfWeek = function () {
      return (new Date(this.setDate(this.getDate() - this.getDay() + 7)));
    };
    let today = new Date();
    var daterangepicker = new ej.calendars.DateRangePicker({
      locale: 'zh',
      presets: [{
        label: '昨日',
        start: startDate,
        end: endDate
      },
      {
        label: '本週',
        start: today.GetFirstDayOfWeek(),
        end: today.GetLastDayOfWeek()
      },

      {
        label: '本月',
        start: new Date(new Date(new Date().setDate(1)).toDateString()),
        end: new Date(new Date().toDateString())
      }


      ],
      format: "yyyy-MM-dd", // custom format 

    });
    daterangepicker.appendTo('#datepicker');
    return daterangepicker;
  },
  // 檢驗 Token
  validateTokenCall: function (settings) {
    let jqxhr = $.ajax(settings);
    jqxhr.done(function (response) {


    });
    //this section is executed when the server responds with error
    jqxhr.fail(function (err) {



    });
    //this section is always executed
    jqxhr.always(function () {
      if (jqxhr.status == "401") {
        document.location.href = localurl + loginUrl;

      }
    });
  },
  //數字加入千分號
  numberWithCommas: function (num) {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  },
  // 取得交易數據
  getRemoteData: function (url, type, para, key, from = "GA", async = true) {

    if (!localStorage.getItem(key)) {

      let settings = {
        "async": async,
        "crossDomain": true,
        "url": url,
        "method": type,
        "headers": {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        "data": para
      };
      $("body").addClass("loading");
      var ele = document.getElementById('footer');

      if (ele) {
        ele.style.visibility = "hidden";

      }
      $.ajax(settings).done(function (response) {
        $("body").removeClass("loading");
        let obj = JSON.parse(response);
        if (Object.keys(obj).length > 0) {
          let prefix =sessionStorage.getItem("sitePrefix");
          DramaCore.setLocalStorage(prefix,key,response)
          //localStorage.setItem(key, response);
          DramaCore.returnValue(key, from);
          if (ele) {
            ele.style.visibility = "visible";

          }

        }
      });

    }


  },
  getSilenceRemoteData: function (url, type, para, key, from = "GA", async = true) {

    if (!localStorage.getItem(key)) {
      let settings = {
        "async": async,
        "crossDomain": true,
        "url": url,
        "method": type,
        "headers": {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        "data": para
      };

      $.ajax(settings).done(function (response) {

        let obj = JSON.parse(response);
        if (Object.keys(obj).length > 0) {
          
          let prefix =sessionStorage.getItem("sitePrefix");
          DramaCore.setLocalStorage(prefix,key,response)
          //localStorage.setItem(key, response);
          //global_EC_Key = key;
          //DramaCore.returnValue(_result);
        }
      });
      DramaCore.returnValue(key, from);
    }


  },
  getGridData: function (url, type, para, key, from = "GA", async = true) {


    let settings = {
      "async": !1,
      "crossDomain": true,
      "url": url,
      "method": type,
      "headers": {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      "data": para
    };

    $.ajax(settings).done(function (response) {

      // let obj = JSON.parse(response);
      let value = DramaCore.returnValue(response);

      return value;
    });


  },
  returnValue: function (value, from) {
    switch (from) {
      case "EC":
        global_EC_Key = value;
        break;
      case "GA":
        global_custom_Key = value;
        break;
      case "Tealeaf":
        global_Tealeaf_Key = value;
        break;
      case "Label":

        global_Label_key = value;
        break;
      default:



        break;
    }
    return value;
  },
  // 繪製畫面
  renderDefaultGAChart: function (key) {
    try {

      let obj = JSON.parse(localStorage.getItem(key));
      let DefaultGAChartDataObj = obj[0].GA_Reports; //第一份報表
      // 填入 Widget
      let widget = DramaCore.appendDefaultWidgetElement(DefaultGAChartDataObj[0], WidgetCounts);
      let adBtnGroups = document.getElementsByClassName("btngroup");
      for (let i = 0; i < adBtnGroups.length; i++) {

        adBtnGroups[i].addEventListener('click', function (event) {
          let filterary = (event.srcElement.dataset.filter).split(",");
          for (let j = 0; j < adBtnGroups.length; j++) {
            adBtnGroups[j].style.color = "#fff";
            adBtnGroups[j].style.fontSize = "18px";
            adBtnGroups[j].style.fontWeight = "200";
          }
          event.srcElement.style.color = "#0B0500";
          event.srcElement.style.fontSize = "20px";
          event.srcElement.style.fontWeight = "700";
          DramaCore.appendDefaultWidgetElement(DefaultGAChartDataObj[0], WidgetCounts, 9, 1, 0, filterary);
        }, false);
      }

      // let widget2 = DramaCore.appendDefaultWidgetElement(DefaultGAChartDataObj[0], WidgetCounts,9,1,0,['google / cpc']);
      //0:date 1:AD 2:sourcemedium 3:campaign
      //(資料位置,資料筆數(0 : 不限制),資料說明)
      DefaultCharts.chart1(DefaultGAChartDataObj, 2, 5, "【來源 / 媒介】Top 5 交易次數 ", "chart1");
      DefaultCharts.chart2(DefaultGAChartDataObj, 2, 5, "【來源 / 媒介】Top 5 收益", "chart2");
      DefaultCharts.chart3(DefaultGAChartDataObj, 2, 5, "【來源 / 媒介】Top 5 加入會員", "chart3");
      DefaultCharts.chart4(DefaultGAChartDataObj, 3, 5, "【活動】Top 5 交易次數", "chart4");
      DefaultCharts.chart5(DefaultGAChartDataObj, 3, 5, "【活動】Top 5 收益", "chart5");
      DefaultCharts.chart6(DefaultGAChartDataObj, 3, 5, "【活動】Top 5 加入會員", "chart6");
      DefaultCharts.chart7(DefaultGAChartDataObj, 4, 0, "廣告活動成效分析", "chart7");
      DefaultCharts.chart8(DefaultGAChartDataObj, 1, 0, "廣告群組成效分析", "chart8");

    } catch (e) {
      console.info(e);
    }
  },
  renderCustomGAChart: function (key) {
    try {

      let obj = JSON.parse(localStorage.getItem(key));
      let CustomGAChartDataObj1 = obj[0].GA_Reports; //第一份報表
      let CustomGAChartDataObj2 = obj[1].GA_Reports; //第二份報表
      // 填入 Widget
      let widget = DramaCore.appendFlowWidgetElement(CustomGAChartDataObj1[0], WidgetCounts);
      let adBtnGroups = document.getElementsByClassName("btngroup");
      for (let i = 0; i < adBtnGroups.length; i++) {

        adBtnGroups[i].addEventListener('click', function (event) {
          let filterary = (event.srcElement.dataset.filter).split(",");
          for (let j = 0; j < adBtnGroups.length; j++) {
            adBtnGroups[j].style.color = "#fff";
            adBtnGroups[j].style.fontSize = "18px";
            adBtnGroups[j].style.fontWeight = "200";
          }
          event.srcElement.style.color = "#0B0500";
          event.srcElement.style.fontSize = "20px";
          event.srcElement.style.fontWeight = "700";
          DramaCore.appendFlowWidgetElement(CustomGAChartDataObj1[0], WidgetCounts, 9, 1, 0, filterary);
        }, false);
      }

      //0:date 1:sourcemedium 2: 3:campaign
      //(資料位置,資料筆數(0 : 不限制),資料說明)
      CustomCharts.chart1(CustomGAChartDataObj1, 4, 5, "Top 5 頁面平均停留時間(秒)", "chart1");
      CustomCharts.chart2(CustomGAChartDataObj2, 0, 10, "Top 10 【站外】導流客戶數( 7 日間比較)", "chart2");
      CustomCharts.chart3(CustomGAChartDataObj1, 4, 5, "Top 5 頁面跳出數", "chart3", "chart3");
      CustomCharts.chart4(CustomGAChartDataObj2, 0, 10, "【站外】導流分析表( 最近 7 日)", "chart4");
      CustomCharts.chart5(CustomGAChartDataObj1, 2, 10, "依【性別】合計訪客數", "chart5");
      CustomCharts.chart6(CustomGAChartDataObj1, 2, 10, "依【性別】 / 【年齡】合計新訪客數", "chart6");
      CustomCharts.chart7(CustomGAChartDataObj1, 3, 0, "依【設備】合計訪客數", "chart7");
      CustomCharts.chart8(CustomGAChartDataObj1, 3, 0, "依【設備】合計工作階段停留時間", "chart8");
      CustomCharts.chart9(CustomGAChartDataObj1, 4, 5, "Top 5 新訪客觀看頁面類別", "chart9");
      CustomCharts.chart10(CustomGAChartDataObj1, 2, 10, "依【性別】 / 【年齡】頁面瀏覽時間", "chart10");
      CustomCharts.chart11(CustomGAChartDataObj1, 3, 0, "依【設備】平均頁面瀏覽時間", "chart11");

    } catch (e) {
      console.info(e);
    }
  },
  renderGAECChart: function (key) {
    try {

      let obj = JSON.parse(localStorage.getItem(key));
      //console.log(obj);
      let CustomGAChartDataObj1 = obj[0].GA_Reports; //第一份報表
      let CustomGAChartDataObj3 = obj[3].GA_Reports; //第四份報表
      let CustomGAChartDataObj4 = obj[4].GA_Reports; //第五份報表

      // 填入 Widget
      let widget = DramaCore.appendWidgetElement(CustomGAChartDataObj1[0], ECWidgetCounts);


      //0:userType 
      //(資料位置,資料筆數(0 : 不限制),資料說明)
      ECChart.chart1(obj, 0, 0, "近四周 新訪客 / 回訪客 收益 ", "chart1");
      ECChart.chart2(CustomGAChartDataObj1, 2, 0, "台灣各城市營收 ", "chart2");
      ECChart.chart3(obj, 0, 0, "近四周 訪客 / 新訪客 購買量 ", "chart3");
      ECChart.chart4(CustomGAChartDataObj1, 1, 0, "平均訂單價值 / 跳出率 四象限分析 ", "chart4");
      ECChart.chart5(CustomGAChartDataObj1, 3, 0, "廣告來源 / 活動 分析 ", "chart5");
      ECChart.chart6(CustomGAChartDataObj1, 1, 0, "平均訂單價值 / 營收 四象限分析 ", "chart6");
      ECChart.chart7(CustomGAChartDataObj1, 4, 0, "單品-電子商務分析表", "chart7");
      ECChart.chart8(CustomGAChartDataObj4, 0, 0, "大分類-電子商務分析表", "chart8");
      ECChart.chart9(CustomGAChartDataObj4, 1, 0, "中分類-電子商務分析表", "chart9");
      ECChart.chart10(CustomGAChartDataObj1, 1, 0, "來源媒體電子商務分析表", "chart10");

    } catch (e) {
      console.info(e);
    }
  },
  renderPOSECChart: function (key, chartsDefine) {
    try {
      /*
      if(localStorage.getItem(pagesets)){
        let jsonObj = localStorage.getItem(pagesets);
        const chartList=jsonObj.map(item=>item)[0];
        
          chartList.forEach(function(ele,i){

            if(ele.pagekey==="EcommercePOS"){
              switch (ele.metakey){
                case 'chart':
              console.log(ele);
              break;
              }
        }

      })
      }else{
        document.location.href=sessionStorage.getItem("localurl")+"default.html";
      }*/

      let obj = JSON.parse(localStorage.getItem(key));
      let CustomGAChartDataObj1 = obj[0].GA_Reports; //第1份報表
      let CustomGAChartDataObj3 = obj[3].GA_Reports; //第4份報表
      let CustomGAChartDataObj5 = obj[4].GA_Reports; //第5份報表
      let CustomGAChartDataObj6 = obj[5].GA_Reports; //第6份報表

      let headwidget1 = DramaCore.appendGACustomWidgetElement(CustomGAChartDataObj1[4].ListResult, 0, '電商訂單金額', '#h1', 'sum');
      let headwidget2 = DramaCore.appendGACustomWidgetElement(CustomGAChartDataObj3[3].ListResult, 1, '電商結賬金額', '#h2', 'sum');
      let headwidget3 = DramaCore.appendGACustomWidgetElement(CustomGAChartDataObj3[3].ListResult, 0, '店鋪結賬金額', '#h3', 'sum');
      let headwidget4 = DramaCore.appendGACustomWidgetElement(CustomGAChartDataObj6[0].ListResult, 0, '會員等級分佈', '#h4', 'member');
      let headwidget5 = DramaCore.appendGACustomWidgetElement(CustomGAChartDataObj6[0].ListResult, 1, '會員等級分佈', '#h5', 'member');
      let headwidget6 = DramaCore.appendGACustomWidgetElement(CustomGAChartDataObj6[0].ListResult, 2, '會員等級分佈變化', '#h6', 'member');
      //0:userType 
      //(資料位置,資料筆數(0 : 不限制),資料說明)

      ECChart.chart7(CustomGAChartDataObj3, 1, 0, chartsDefine[0].title, chartsDefine[0].id);
      ECChart.chart8(CustomGAChartDataObj3, 0, 0, chartsDefine[1].title, chartsDefine[1].id);
      ECChart.chart9(CustomGAChartDataObj3, 2, 0, chartsDefine[2].title, chartsDefine[2].id);
      ECChart.chart10(CustomGAChartDataObj3, 3, 0, chartsDefine[3].title, chartsDefine[3].id);
      ECChart.chart11(CustomGAChartDataObj3, 4, 0, chartsDefine[4].title, chartsDefine[4].id);
      ECChart.chart12(CustomGAChartDataObj3, 5, 0, chartsDefine[5].title, chartsDefine[5].id);
      ECChart.chart13(CustomGAChartDataObj3, 6, 0, chartsDefine[6].title, chartsDefine[6].id);
      ECChart.chart14(CustomGAChartDataObj3, 7, 0, chartsDefine[7].title, chartsDefine[7].id);
      ECChart.chart15(CustomGAChartDataObj3, 8, 0, chartsDefine[8].title, chartsDefine[8].id);
      ECChart.chart16(CustomGAChartDataObj3, 9, 0, chartsDefine[9].title, chartsDefine[9].id);
      ECChart.chart17(CustomGAChartDataObj3, 10, 0, chartsDefine[10].title, chartsDefine[10].id);
      ECChart.chart18(CustomGAChartDataObj3, 11, 0, chartsDefine[11].title, chartsDefine[11].id);
    } catch (e) {
      console.info(e);
    }
  },
  renderTealeafChart: function (key) {
    try {
      let obj = JSON.parse(localStorage.getItem(key));

      let emptyObj = {};
      let widget1 = DramaCore.appendTealeafWidgetElement(obj[14], InteractiveWidgetCounts, '', 'type1', '#w', 1, 12);
      let widget2 = DramaCore.appendTealeafWidgetElement(obj[15], InteractiveWidgetCounts, '入站立即跳離', "type2",'#w',13,1);
      let widget3 = DramaCore.appendTealeafWidgetElement(obj[16], InteractiveWidgetCounts, '超商取貨金額超過限制即結束購買', 'type2','#w',14,1);
      let widget4 = DramaCore.appendTealeafWidgetElement(obj[17], InteractiveWidgetCounts, '提示訊息出現次數', 'type3','#w',15,1);
      //(資料位置,資料筆數(0 : 不限制),資料說明)
      TealeafChart.chart1A(emptyObj, 2, 0, "熱點圖 ", "chart1A");
      TealeafChart.chart1B(emptyObj, 2, 0, "熱點圖 ", "chart1B");
      TealeafChart.chart2A(emptyObj, 2, 0, "熱點圖 ", "chart2A");
      TealeafChart.chart2B(emptyObj, 2, 0, "熱點圖 ", "chart2B");
      TealeafChart.chart3A(emptyObj, 2, 0, "熱點圖 ", "chart3A");
      TealeafChart.chart3B(emptyObj, 2, 0, "熱點圖 ", "chart3B");
      TealeafChart.chart4(obj, 4, 0, "關鍵字 ", "chart4",'鍵入關鍵字');
      TealeafChart.chart5(obj, 6, 0, "搜索後熱門關鍵字 ", "chart5",'關鍵字結果頁網址');
      TealeafChart.chart6(obj, 7, 0, "Google 廣告 ", "chart6",'Google搜尋後點擊廣告進來的用戶轉換狀態');
      TealeafChart.chart7(obj, 5, 0, "站內搜尋  ", "chart7",'站內搜尋用戶轉換狀態');
      TealeafChart.chart8(obj, 10, 0, "UTM 廣告 ", "chart8",'點擊 UTM 廣告進來的用戶轉換狀態');
      TealeafChart.chart9(obj, 13, 0, "註冊會員 ", "chart9",'註冊會員流程');




    } catch (e) {
      console.info(e);
    }
  },
  //添加 Widget
  appendWidgetElement: function (widgetData, widgetcounts, startID = 1, ListResultPos = 0, metricPos = 0, filters = []) {

    let metric = widgetData.ListResult[ListResultPos].metrics[metricPos].metric;
    let metricName = widgetData.MetricHead;

    if (filters.length == 0) {
      for (let i = 0; i < widgetcounts; i++) {
        let j = i + startID;
        // 檢查是否已有標題
        if ($("#w" + j).children(".card-header").children(".widget-text").html() === "") {
          $("#w" + j).children(".card-header").children(".widget-text").text(metricName[i]);
        }

        $("#w" + j).children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(metric[i].value));

      }
    } else {
      for (let i = 0; i < widgetcounts; i++) {
        let j = i + startID;
        if (yourArray.indexOf("someString") > -1) {
          //In the array!
        }
        // 檢查是否已有標題
        if ($("#w" + j).children(".card-header").children(".widget-text").html() === "") {
          $("#w" + j).children(".card-header").children(".widget-text").text(metricName[i]);
        }

        $("#w" + j).children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(metric[i].value));

      }

    }

    return true;
  },
  appendDefaultWidgetElement: function (widgetData, widgetcounts, startID = 1, dimensionPos = 0, metricPos = 0, filters = []) {

    //let metric = widgetData.ListResult[ListResultPos].metrics[metricPos].metric;
    let metrics = [0, 0, 0, 0, 0, 0, 0, 0];
    let allMetrics = [0, 0, 0, 0, 0, 0, 0, 0];
    //let dimension = widgetData.ListResult[ListResultPos].metrics[metricPos].metric;
    let metricName = widgetData.MetricHead;

    if (filters.length == 0) {
      for (let i = 0; i < widgetData.ListResult.length; i++) {

        allMetrics[0] = Number(widgetData.ListResult[i].metrics[0].metric[0].value) + allMetrics[0];
        allMetrics[1] = Number(widgetData.ListResult[i].metrics[0].metric[1].value) + allMetrics[1];
        allMetrics[2] = Number(widgetData.ListResult[i].metrics[0].metric[2].value) + allMetrics[2];
        allMetrics[3] = Number(widgetData.ListResult[i].metrics[0].metric[3].value) + allMetrics[3];
        allMetrics[4] = Number(widgetData.ListResult[i].metrics[0].metric[4].value) + allMetrics[4];
        allMetrics[5] = Number(widgetData.ListResult[i].metrics[0].metric[5].value) + allMetrics[5];
        allMetrics[6] = Number(widgetData.ListResult[i].metrics[0].metric[6].value) + allMetrics[6];
        allMetrics[7] = Number(widgetData.ListResult[i].metrics[0].metric[7].value) + allMetrics[7];

      }
      allMetrics[5] = allMetrics[5] / widgetData.ListResult.length;
      allMetrics[6] = allMetrics[6] / widgetData.ListResult.length;
      allMetrics[7] = allMetrics[7] / widgetData.ListResult.length;

      for (let i = 0; i < widgetcounts; i++) {
        let j = i + startID;
        // 檢查是否已有標題
        if ($("#w" + j).children(".card-header").children(".widget-text").html() === "") {
          $("#w" + j).children(".card-header").children(".widget-text").text(metricName[i]);
        }

        $("#w" + j).children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(allMetrics[i].toFixed(1)));

      }
    } else {

      let k = 1;//進入計算指標
      for (let i = 0; i < widgetData.ListResult.length; i++) {

        allMetrics[0] = Number(widgetData.ListResult[i].metrics[0].metric[0].value) + allMetrics[0];
        allMetrics[1] = Number(widgetData.ListResult[i].metrics[0].metric[1].value) + allMetrics[1];
        allMetrics[2] = Number(widgetData.ListResult[i].metrics[0].metric[2].value) + allMetrics[2];
        allMetrics[3] = Number(widgetData.ListResult[i].metrics[0].metric[3].value) + allMetrics[3];
        allMetrics[4] = Number(widgetData.ListResult[i].metrics[0].metric[4].value) + allMetrics[4];
        allMetrics[5] = Number(widgetData.ListResult[i].metrics[0].metric[5].value) + allMetrics[5];
        allMetrics[6] = Number(widgetData.ListResult[i].metrics[0].metric[6].value) + allMetrics[6];
        allMetrics[7] = Number(widgetData.ListResult[i].metrics[0].metric[7].value) + allMetrics[7];
        if (filters.indexOf(widgetData.ListResult[i].dimension[dimensionPos]) > -1) {
          metrics[0] = Number(widgetData.ListResult[i].metrics[0].metric[0].value) + metrics[0];
          metrics[1] = Number(widgetData.ListResult[i].metrics[0].metric[1].value) + metrics[1];
          metrics[2] = Number(widgetData.ListResult[i].metrics[0].metric[2].value) + metrics[2];
          metrics[3] = Number(widgetData.ListResult[i].metrics[0].metric[3].value) + metrics[3];
          metrics[4] = Number(widgetData.ListResult[i].metrics[0].metric[4].value) + metrics[4];
          metrics[5] = Number(widgetData.ListResult[i].metrics[0].metric[5].value) + metrics[5];
          metrics[6] = Number(widgetData.ListResult[i].metrics[0].metric[6].value) + metrics[6];
          metrics[7] = Number(widgetData.ListResult[i].metrics[0].metric[7].value) + metrics[7];
          k = k + 1;


        }

      }
      allMetrics[5] = allMetrics[5] / widgetData.ListResult.length;
      allMetrics[6] = allMetrics[6] / widgetData.ListResult.length;
      allMetrics[7] = allMetrics[7] / widgetData.ListResult.length;
      metrics[5] = metrics[5] / k;
      metrics[6] = metrics[6] / k;
      metrics[7] = metrics[7] / k;
      for (let i = 0; i < widgetcounts; i++) {

        let j = i + startID;

        let pieobj = {}, allserialobj = {}, serialobj = {}, serialary = [];
        let ratio = 0;

        if (i < 5) {
          allserialobj.name = "全部(%)";
          serialobj.name = "廣告(%)";
          ratio = (metrics[i] / allMetrics[i]).toFixed(2);
          allserialobj.value = (1 - ratio) * 100;
          serialobj.value = ratio * 100;

        } else {
          allserialobj.name = "全部";
          serialobj.name = "廣告";
          allserialobj.value = (allMetrics[i]).toFixed(2);
          serialobj.value = (metrics[i]).toFixed(2);
        }
        serialary.push(allserialobj);
        serialary.push(serialobj);



        // serialobj.name="廣告";
        //  serialobj.value=metrics[i];
        // serialary.push(serialobj);

        pieobj.datasource = serialary;

        // ses.title='佔比'+j    ;           
        let pie = new ej.charts.AccumulationChart(chartSetting.syncSemipie(pieobj));

        pie.appendTo("#w" + j + "c");
        pie.removeSvg();
        pie.refreshSeries();
        pie.refreshChart();
        // 檢查是否已有標題
        if ($("#w" + j).children(".card-header").children(".widget-text").html() === "") {
          $("#w" + j).children(".card-header").children(".widget-text").text(metricName[i]);
        }
        $("#w" + j).children(".card-body").children(".widget-number").text("");
        $("#w" + j).children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(metrics[i].toFixed(1)));


      }

    }

    return true;
  },
  appendFlowWidgetElement: function (widgetData, widgetcounts, startID = 1, dimensionPos = 0, metricPos = 0, filters = []) {

    //let metric = widgetData.ListResult[ListResultPos].metrics[metricPos].metric;
    let metrics = [0, 0, 0, 0, 0, 0, 0, 0];
    let allMetrics = [0, 0, 0, 0, 0, 0, 0, 0];
    //let dimension = widgetData.ListResult[ListResultPos].metrics[metricPos].metric;
    let metricName = widgetData.MetricHead;

    if (filters.length == 0) {
      for (let i = 0; i < widgetData.ListResult.length; i++) {

        allMetrics[0] = Number(widgetData.ListResult[i].metrics[0].metric[0].value) + allMetrics[0];
        allMetrics[1] = Number(widgetData.ListResult[i].metrics[0].metric[1].value) + allMetrics[1];
        allMetrics[2] = Number(widgetData.ListResult[i].metrics[0].metric[2].value) + allMetrics[2];
        allMetrics[3] = Number(widgetData.ListResult[i].metrics[0].metric[3].value) + allMetrics[3];
        allMetrics[4] = Number(widgetData.ListResult[i].metrics[0].metric[4].value) + allMetrics[4];
        allMetrics[5] = Number(widgetData.ListResult[i].metrics[0].metric[5].value) + allMetrics[5];
        allMetrics[6] = Number(widgetData.ListResult[i].metrics[0].metric[6].value) + allMetrics[6];
        allMetrics[7] = Number(widgetData.ListResult[i].metrics[0].metric[7].value) + allMetrics[7];

      }
      allMetrics[5] = allMetrics[5] / widgetData.ListResult.length;
      allMetrics[6] = allMetrics[6] / widgetData.ListResult.length;
      allMetrics[7] = allMetrics[7] / widgetData.ListResult.length;

      for (let i = 0; i < widgetcounts; i++) {
        let j = i + startID;
        // 檢查是否已有標題
        if ($("#w" + j).children(".card-header").children(".widget-text").html() === "") {
          $("#w" + j).children(".card-header").children(".widget-text").text(metricName[i]);
        }

        $("#w" + j).children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(allMetrics[i].toFixed(1)));

      }
    } else {

      let k = 1;//進入計算指標
      for (let i = 0; i < widgetData.ListResult.length; i++) {

        allMetrics[0] = Number(widgetData.ListResult[i].metrics[0].metric[0].value) + allMetrics[0];
        allMetrics[1] = Number(widgetData.ListResult[i].metrics[0].metric[1].value) + allMetrics[1];
        allMetrics[2] = Number(widgetData.ListResult[i].metrics[0].metric[2].value) + allMetrics[2];
        allMetrics[3] = Number(widgetData.ListResult[i].metrics[0].metric[3].value) + allMetrics[3];
        allMetrics[4] = Number(widgetData.ListResult[i].metrics[0].metric[4].value) + allMetrics[4];
        allMetrics[5] = Number(widgetData.ListResult[i].metrics[0].metric[5].value) + allMetrics[5];
        allMetrics[6] = Number(widgetData.ListResult[i].metrics[0].metric[6].value) + allMetrics[6];
        allMetrics[7] = Number(widgetData.ListResult[i].metrics[0].metric[7].value) + allMetrics[7];
        if (filters.indexOf(widgetData.ListResult[i].dimension[dimensionPos]) > -1) {
          metrics[0] = Number(widgetData.ListResult[i].metrics[0].metric[0].value) + metrics[0];
          metrics[1] = Number(widgetData.ListResult[i].metrics[0].metric[1].value) + metrics[1];
          metrics[2] = Number(widgetData.ListResult[i].metrics[0].metric[2].value) + metrics[2];
          metrics[3] = Number(widgetData.ListResult[i].metrics[0].metric[3].value) + metrics[3];
          metrics[4] = Number(widgetData.ListResult[i].metrics[0].metric[4].value) + metrics[4];
          metrics[5] = Number(widgetData.ListResult[i].metrics[0].metric[5].value) + metrics[5];
          metrics[6] = Number(widgetData.ListResult[i].metrics[0].metric[6].value) + metrics[6];
          metrics[7] = Number(widgetData.ListResult[i].metrics[0].metric[7].value) + metrics[7];
          k = k + 1;


        }

      }
      allMetrics[5] = allMetrics[5] / widgetData.ListResult.length;
      allMetrics[6] = allMetrics[6] / widgetData.ListResult.length;
      allMetrics[7] = allMetrics[7] / widgetData.ListResult.length;
      metrics[5] = metrics[5] / k;
      metrics[6] = metrics[6] / k;
      metrics[7] = metrics[7] / k;
      for (let i = 0; i < widgetcounts; i++) {

        let j = i + startID;

        let pieobj = {}, allserialobj = {}, serialobj = {}, serialary = [];
        let ratio = 0;

        if (i < 5) {
          allserialobj.name = "全部(%)";
          serialobj.name = "廣告(%)";
          ratio = (metrics[i] / allMetrics[i]).toFixed(2);
          allserialobj.value = (1 - ratio) * 100;
          serialobj.value = ratio * 100;

        } else {
          allserialobj.name = "全部";
          serialobj.name = "廣告";
          allserialobj.value = (allMetrics[i]).toFixed(2);
          serialobj.value = (metrics[i]).toFixed(2);
        }
        serialary.push(allserialobj);
        serialary.push(serialobj);



        // serialobj.name="廣告";
        //  serialobj.value=metrics[i];
        // serialary.push(serialobj);

        pieobj.datasource = serialary;

        // ses.title='佔比'+j    ;           
        let pie = new ej.charts.AccumulationChart(chartSetting.syncSemipie(pieobj));

        pie.appendTo("#w" + j + "c");
        pie.removeSvg();
        pie.refreshSeries();
        pie.refreshChart();
        // 檢查是否已有標題
        if ($("#w" + j).children(".card-header").children(".widget-text").html() === "") {
          $("#w" + j).children(".card-header").children(".widget-text").text(metricName[i]);
        }
        $("#w" + j).children(".card-body").children(".widget-number").text("");
        $("#w" + j).children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(metrics[i].toFixed(1)));


      }

    }

    return true;
  },
  appendGACustomWidgetElement: function (widgetData, index, widgetName, widgetID, type) {
    /** type:sum(資料加總) */
    let metric = 0;
    let metricName = widgetName;
    let amount = 0;

    switch (type) {
      case 'sum':

        for (let i = 0; i < widgetData.length; i++) {
          let test = widgetData[i] + ".metrics[0].metric[index].value";
          try {
            metric = parseInt(widgetData[i].metrics[0].metric[index].value);
          }
          catch (e) {

            console.log(e);
          }
          metric = parseInt(widgetData[i].metrics[0].metric[index].value);

          amount = metric + amount;
        }
        $(widgetID).children(".card-header").children(".widget-text").text(metricName);
        $(widgetID).children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(amount));
        break;
      case 'member':
        let metricDate, metricRegular, metricVip, metricSvip;


        metricRegular = DramaCore.numberWithCommas(parseInt(widgetData[index].metrics[0].metric[0].value));
        metricVip = DramaCore.numberWithCommas(parseInt(widgetData[index].metrics[0].metric[1].value));
        metricSvip = DramaCore.numberWithCommas(parseInt(widgetData[index].metrics[0].metric[2].value));
        metricDate = widgetData[index].metrics[0].metric[3].value;
        if (metricDate == null) {
          metricDate = ''
          if (metricRegular > 0) {
            metricRegular = metricRegular.toString() + ' ' + '<i class="fas fa-arrow-up text-danger"></i>'
          } else if (metricRegular < 0) {
            metricRegular = (metricRegular * -1).toString() + ' ' + '<i class="fas fa-arrow-down text-primary"></i>'
          }
          if (metricVip > 0) {
            metricVip = metricVip.toString() + ' ' + '<i class="fas fa-arrow-up text-danger"></i>'
          } else if (metricVip < 0) {
            metricVip = (metricVip * -1).toString() + ' ' + '<i class="fas fa-arrow-down text-primary"></i>'
          }
          if (metricSvip > 0) {
            metricSvip = metricSvip.toString() + ' ' + '<i class="fas fa-arrow-up text-danger"></i>'
          } else if (metricSvip < 0) {
            metricSvip = (metricSvip * -1).toString() + ' ' + '<i class="fas fa-arrow-down text-primary"></i>'
          }

        } else {
          metricName = ' : ' + metricName
        }
        $(widgetID).children(".card-header").children(".widget-text").text(metricDate + metricName);
        $(widgetID).find("[data-member=regular]").html(metricRegular);
        $(widgetID).find("[data-member=vip]").html(metricVip);
        $(widgetID).find("[data-member=svip]").html(metricSvip);
        break;

    }


    return true;
  },
  //type : 計算方式,wPrefixID:widget ID 前綴值,startIndex:起始ID,wlength:widget 數量
  appendTealeafWidgetElement: function (widgetData, widgetcounts, cardheader, type, wPrefixID, startIndex=0, wlength=0) {

    let widgetAry = widgetData.Data;
    switch (type) {
      case 'type1':
        let desktopIndex = widgetAry.findIndex(element => element.Col1 === "Desktop");
        let tabletIndex = widgetAry.findIndex(element => element.Col1 === "Tablet");
        let mobilePhoneIndex = widgetAry.findIndex(element => element.Col1 === "MobilePhone");
        for (let i = 1; i <= widgetcounts; i++) {
          switch (i % 4) {
            case 1:
              $("#w" + i).children(".card-header").children(".widget-text").text(InteractiveWidgetHead1);
              break;
            case 2:
              $("#w" + i).children(".card-header").children(".widget-text").text(InteractiveWidgetHead2);
              break;
            case 3:
              $("#w" + i).children(".card-header").children(".widget-text").text(InteractiveWidgetHead3);
              break;
            case 0:
              $("#w" + i).children(".card-header").children(".widget-text").text(InteractiveWidgetHead4);
              break;
          }


        }
        if (desktopIndex >= 0) {
          $("#w1").children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(widgetAry[desktopIndex].Col2));
          $("#w2").children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(widgetAry[desktopIndex].Col3));
          $("#w3").children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(widgetAry[desktopIndex].Col4));
          $("#w4").children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(widgetAry[desktopIndex].Col5));
        } else {
          $("#w1").children(".card-body").children(".widget-number").text("0");
          $("#w2").children(".card-body").children(".widget-number").text("0");
          $("#w3").children(".card-body").children(".widget-number").text("0");
          $("#w4").children(".card-body").children(".widget-number").text("0");
        }
        if (mobilePhoneIndex >= 0) {

          $("#w5").children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(widgetAry[mobilePhoneIndex].Col2));
          $("#w6").children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(widgetAry[mobilePhoneIndex].Col3));
          $("#w7").children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(widgetAry[mobilePhoneIndex].Col4));
          $("#w8").children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(widgetAry[mobilePhoneIndex].Col5));

        } else {
          $("#w5").children(".card-body").children(".widget-number").text("0");
          $("#w6").children(".card-body").children(".widget-number").text("0");
          $("#w7").children(".card-body").children(".widget-number").text("0");
          $("#w8").children(".card-body").children(".widget-number").text("0");
        }
        if (tabletIndex >= 0) {

          $("#w9").children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(widgetAry[tabletIndex].Col2));
          $("#w10").children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(widgetAry[tabletIndex].Col3));
          $("#w11").children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(widgetAry[tabletIndex].Col4));
          $("#w12").children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(widgetAry[tabletIndex].Col5));
        } else {
          $("#w9").children(".card-body").children(".widget-number").text("0");
          $("#w10").children(".card-body").children(".widget-number").text("0");
          $("#w11").children(".card-body").children(".widget-number").text("0");
          $("#w12").children(".card-body").children(".widget-number").text("0");
        }
        break;
      case 'type2':
        
        for(let i=0;i<wlength;i++){
          startIndex+=i;
          $("#w" + startIndex).children(".card-header").children(".widget-text").text(cardheader);
          $(wPrefixID+startIndex.toString()).children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(widgetAry[0].Col2));
        }
        break;
      case 'type3':
        console.log(widgetAry[0].Col4);
        let hintNumber=0
        for(let i=0;i<widgetAry.length;i++){
          hintNumber+=parseInt(widgetAry[0].Col4)
        }
        for(let i=0;i<wlength;i++){
          startIndex+=i;
          $("#w" + startIndex).children(".card-header").children(".widget-text").text(cardheader);
          $(wPrefixID+startIndex.toString()).children(".card-body").children(".widget-number").text(DramaCore.numberWithCommas(hintNumber));
        }
        break;  
    }





    return true;
  },
  exportCSV(table_id, cardHead) {

    var rows = document.querySelectorAll('table#' + table_id + ' tr');
    // Construct csv
    var csv = [];
    //skip header row
    for (var i = 1; i < rows.length; i++) {
      var row = [], cols = rows[i].querySelectorAll('td, th');
      // skip header row
      for (var j = 0; j < cols.length; j++) {

        var data = cols[j].innerText.replace(/(\r\n|\n|\r)/gm, '').replace(/(\s\s)/gm, ' ')

        data = data.replace(/"/g, '""');
        // Push escaped string
        row.push('"' + data + '"');
      }
      csv.push(row.join(','));
    }
    var csv_string = csv.join('\n');
    var universalBOM = "\uFEFF";
    // Download it
    var filename = 'export_' + cardHead + '_' + new Date().toLocaleDateString() + '.csv';
    var link = document.createElement('a');
    link.style.display = 'none';
    link.setAttribute('target', '_blank');
    //CSV 加入 BOM 可以避免 EXCEL 開啟為亂碼
    link.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(universalBOM + csv_string));
    link.setAttribute('download', filename);
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  },
  setLocalStorage(siteprefix,key,value){
    try{
      localStorage.setItem(key,value);
    }catch(oException){
      if(oException.name == 'QuotaExceededError'){
        console.log('超出本地存储限额！');
        //如果历史信息不重要了，可清空后再设置
        //localStorage.clear();
        // Iterate over localStorage and insert the keys that meet the condition into arr
         let prefix =siteprefix+'-ga';

         let arr = []; // Array to hold the keys
        for (let i = 0; i < localStorage.length; i++){
            if (localStorage.key(i).substring(0,prefix.length) == prefix) {
                arr.push(localStorage.key(i));
            }
        }

        // Iterate over arr and remove the items by key
        for (let i = 0; i < arr.length; i++) {
            localStorage.removeItem(arr[i]);
        }
        localStorage.setItem(key,value);
      }
    }
  }



};