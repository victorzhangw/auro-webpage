const AuthremoteUrl = "https://cdp.aurocore.com/api/AuthenticationService/";
const localurl="http://caco.aurocore.com/CACO_CDP/";
loginUrl="/login.html";
var sitePrefix = "";
var siteDefine = [{
        "name": "caco",
        "prefix": "caco",
        "memberKey": "45488F6FA45856B7FA5184B3676B8",
        "GoogleremoteUrl": "https://cdp.aurocore.com/api/GoogleService/",
        "localurl": "http://caco.aurocore.com/CACO_CDP/caco/",
        "AuthremoteUrl": "https://cdp.aurocore.com/api/AuthenticationService/",
        "TealeafremoteUrl": "https://cdp.aurocore.com/api/TealeafEvent/",
        "LabelremoteUrl": "https://cdp.aurocore.com/api/GridProvider/",
        "ApiremoteUrl": "https://cdp.aurocore.com/api/",

    },
    {
        "name": "waveshine",
        "prefix": "wave",
        "memberKey": "45488F6FA45856B7FA5184B3676B8",
        "GoogleremoteUrl": "http://waveshinecdp.aurocore.com/api/GoogleService/",
        "localurl": "http://caco.aurocore.com/CACO_CDP/waveshine/",
        "AuthremoteUrl": "http://waveshinecdp.aurocore.com/api/AuthenticationService/",
        "TealeafremoteUrl": "http://waveshinecdp.aurocore.com/api/TealeafEvent/",
        "LabelremoteUrl": "http://waveshinecdp.aurocore.com/api/GridProvider/",
        "ApiremoteUrl": "http://waveshinecdp.aurocore.com/api/",
    },
    {
        "name": "demosite",
        "prefix": "demo",
        "memberKey": "45488F6FA45856B7FA5184B3676B8",
        "GoogleremoteUrl": "http://demo.aurocore.com/api/GoogleService/",
        "localurl": "http://demo.aurocore.com/democdp/demosite/",
        "AuthremoteUrl": "http://demo.aurocore.com/api/AuthenticationService/",
        "TealeafremoteUrl": "http://demo.aurocore.com/api/TealeafEvent/",
        "LabelremoteUrl": "http://demo.aurocore.com/api/GridProvider/",
        "ApiremoteUrl": "http://demo.aurocore.com/api/",
    }

];
