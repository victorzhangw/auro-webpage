#pragma checksum "D:\auro-webpage\CDPRazor\Pages\Shared\_Layout.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ac067cdc43a89904249e7273466d512a53528efe"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(CDPRazor.Pages.Shared.Pages_Shared__Layout), @"mvc.1.0.view", @"/Pages/Shared/_Layout.cshtml")]
namespace CDPRazor.Pages.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\auro-webpage\CDPRazor\Pages\_ViewImports.cshtml"
using CDPRazor;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ac067cdc43a89904249e7273466d512a53528efe", @"/Pages/Shared/_Layout.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9a225eff6e1b640779c4140df9a99199188610fc", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Shared__Layout : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<!DOCTYPE html>\r\n<html lang=\"zh\">\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ac067cdc43a89904249e7273466d512a53528efe2906", async() => {
                WriteLiteral(@"
    <title>登入</title>
    <meta charset=""utf-8"">
    <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
    <meta name=""description"" content=""CF_HAMI_CDP"">
    <meta name=""author"" content=""Aurocore"">
    <link href=""https://cdn.syncfusion.com/ej2/material.css"" rel=""stylesheet"">
    <link href=""https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"" rel=""stylesheet"">

    <link href=""./assests/css/common.css"" rel=""stylesheet"">
    <link href=""./assests/css/custom.css"" rel=""stylesheet"">
    <script src=""https://cdn.syncfusion.com/ej2/dist/ej2.min.js"" type=""text/javascript""></script>
    <script src=""https://code.jquery.com/jquery-3.4.1.min.js"" crossorigin=""anonymous""></script>

    <script src=""assests/dist/moment.js""></script>
    <script src=""assests/env.js""></script>
    <style>
        .mapR {
            background-image: url(""./assests/img/waveshine-bg.jpg"");
            height: 100%;
            background-position: center;
            backgroun");
                WriteLiteral(@"d-repeat: no-repeat;
            background-size: cover;
        }

        html,
        body {
            height: 100%;
            background-color: #fff;
            font-family: ""微軟正黑體"", ""PingFang TC"";
        }

        .fill {
            min-height: 100%;
            height: 100%;
        }

        .no-gutters {
            margin-right: 0;
            margin-left: 0;
        }

            .no-gutters > .col,
            .no-gutters > [class*=""col-""] {
                padding-right: 0;
                padding-left: 0;
            }

        .login-logo {
            display: inline-block;
            margin: 40px 40px;
            background-image: url(""./assests/img/aurocore.svg"");
            background-repeat: no-repeat;
            background-size: 100% 100%;
            height: 50px;
            width: 160px;
        }

        .welcomeHeroText {
            font-size: 42px;
            margin: -20px 40px;
        }

        .welcomeTitle {
          ");
                WriteLiteral(@"  font-size: 40px;
            margin: 20px 40px;
            font-family: ""Tiempos Fine"";
        }

        .loginform form {
            margin: 40px 40px;
            font-size: 16px;
        }

        .forgotpass {
            float: right;
            margin: 0;
        }

        .submit .submit-button {
            height: 42px;
            width: 100%;
            border: none;
            margin-top: 50px;
            background-color: hotpink;
            color: #FFF;
        }

            .submit .submit-button:hover {
                border-color: #BFDEFF;
                border-style: solid;
                ;
                border-width: 2px;
            }

        .privacy-rule {
            font-size: 8px;
        }

            .privacy-rule ul {
                list-style: none;
            }

                .privacy-rule ul li {
                    margin-top: 10px;
                    ;
                }

        .correct-lbl {
            d");
                WriteLiteral("isplay: none;\r\n        }\r\n\r\n        .error-lbl {\r\n            display: inline;\r\n            color: red;\r\n        }\r\n\r\n        ");
                WriteLiteral(@"@media screen and (min-width: 400px) and (max-width: 700px) {
            .welcomeHeroText {
                font-size: 30px;
                margin: -40px 40px;
            }

            .welcomeTitle {
                font-size: 24px;
                margin: 40px 40px;
                font-family: Tiempos Fine;
            }

            .loginform form {
                margin: 40px 40px;
            }
        }
    </style>
");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
