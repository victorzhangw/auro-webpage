﻿var stateObject = {
  "基隆市": {
    "仁愛區": [ "基隆" ],
    "信義區": [ "基隆愛買" ],
  },
  "台北市": {
    "士林區": [ "士林" ],
    "大安區": [ "SOGO" ],
	"中山區": [ "民權" ,"南京"],
	"中正區": [ "站前"],
    "文山區": [ "興隆" ],
    "萬華區": [ "西寧" ],
  },
  "新北市": {
    "三重區": [ "三重愛買" ],
    "土城區": [ "土城" ],
    "中和區": [ "環球" ],
    "汐止區": [ "汐止" ],
    "板橋區": [ "新埔" ],
    "淡水區": [ "淡水"],               
    "新店區": ["新店"],
    "新莊區": [ "幸福"],
    "樹林區": [ "樹林" ],
    "蘆洲區": [ "長榮" ],
  },
  "宜蘭縣": {
    "宜蘭市": [ "宜蘭" ],
  },

  "桃園市": {
    "中壢區": [ "中山","中壢","內壢"],
    "桃園區": [ "桃園","南平"],
    "平鎮區": [ "平鎮" ],
  },
  "新竹市": {
    "東區": [ "新竹" ],
  },
  "新竹縣": {
    "竹北市": [ "竹北" ],
  },
  "台中市": {
    "中區": [ "台中" ],
    "北區": [ "學士" ],
    "西屯區": [ "青海","中港愛買","中清"],
    "西區": [ "美村" ],
    "豐原區": [ "豐原","成功" ],
			
  },

  "南投縣": {
    "南投市": [ "南投" ],
  },
  "雲林縣": {
    "斗六市": [ "斗六" ],
  },
  "嘉義市": {
    "東區": [ "耐斯","北門" ],
    "西區": [ "嘉義" ],
  },
  
  "台南市": {
    "中西區": [ "安平" ],
    "北區": [ "民德" ],
    "永康區": [ "永康", ],
  },
  "高雄市": {
    "左營區": [ "左營" ],
    "鳳山區": [ "鳳山" ],
  },
  "屏東縣": {
    "屏東市": [ "屏東" ,"台糖"],
  },
}
window.onload = function () {
    var stateSel = document.getElementById("entry.1329025438"),
        countySel = document.getElementById("entry.874662454"),
        citySel = document.getElementById("entry.182018262");
    for (var state in stateObject) {
        stateSel.options[stateSel.options.length] = new Option(state, state);
    }
    stateSel.onchange = function () {
        countySel.length = 1; // remove all options bar first
        citySel.length = 1; // remove all options bar first
        if (this.selectedIndex < 1) return; // done   
        for (var county in stateObject[this.value]) {
            countySel.options[countySel.options.length] = new Option(county, county);
        }
    }
    stateSel.onchange(); // reset in case page is reloaded
    countySel.onchange = function () {
        citySel.length = 1; // remove all options bar first
        if (this.selectedIndex < 1) return; // done   
        var cities = stateObject[stateSel.value][this.value];
        for (var i = 0; i < cities.length; i++) {
            citySel.options[citySel.options.length] = new Option(cities[i], cities[i]);
        }
    }
}