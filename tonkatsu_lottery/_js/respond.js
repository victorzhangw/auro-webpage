/*! Respond.js v1.4.2: min/max-width media query polyfill਍ ⨀ 䌀漀瀀礀爀椀最栀琀 ㈀　㄀㐀 匀挀漀琀琀 䨀攀栀氀ഀഀ
 * Licensed under MIT਍ ⨀ 栀琀琀瀀㨀⼀⼀樀⸀洀瀀⼀爀攀猀瀀漀渀搀樀猀 ⨀⼀ഀഀ
਍⼀⨀℀ 洀愀琀挀栀䴀攀搀椀愀⠀⤀ 瀀漀氀礀昀椀氀氀 ⴀ 吀攀猀琀 愀 䌀匀匀 洀攀搀椀愀 琀礀瀀攀⼀焀甀攀爀礀 椀渀 䨀匀⸀ 䄀甀琀栀漀爀猀 ☀ 挀漀瀀礀爀椀最栀琀 ⠀挀⤀ ㈀　㄀㈀㨀 匀挀漀琀琀 䨀攀栀氀Ⰰ 倀愀甀氀 䤀爀椀猀栀Ⰰ 一椀挀栀漀氀愀猀 娀愀欀愀猀⸀ 䐀甀愀氀 䴀䤀吀⼀䈀匀䐀 氀椀挀攀渀猀攀 ⨀⼀ഀഀ
/*! NOTE: If you're already including a window.matchMedia polyfill via Modernizr or otherwise, you don't need this part */਍⠀昀甀渀挀琀椀漀渀⠀眀⤀ 笀ഀഀ
  "use strict";਍  眀⸀洀愀琀挀栀䴀攀搀椀愀 㴀 眀⸀洀愀琀挀栀䴀攀搀椀愀 簀簀 昀甀渀挀琀椀漀渀⠀搀漀挀Ⰰ 甀渀搀攀昀椀渀攀搀⤀ 笀ഀഀ
    var bool, docElem = doc.documentElement, refNode = docElem.firstElementChild || docElem.firstChild, fakeBody = doc.createElement("body"), div = doc.createElement("div");਍    搀椀瘀⸀椀搀 㴀 ∀洀焀ⴀ琀攀猀琀ⴀ㄀∀㬀ഀഀ
    div.style.cssText = "position:absolute;top:-100em";਍    昀愀欀攀䈀漀搀礀⸀猀琀礀氀攀⸀戀愀挀欀最爀漀甀渀搀 㴀 ∀渀漀渀攀∀㬀ഀഀ
    fakeBody.appendChild(div);਍    爀攀琀甀爀渀 昀甀渀挀琀椀漀渀⠀焀⤀ 笀ഀഀ
      div.innerHTML = '&shy;<style media="' + q + '"> #mq-test-1 { width: 42px; }</style>';਍      搀漀挀䔀氀攀洀⸀椀渀猀攀爀琀䈀攀昀漀爀攀⠀昀愀欀攀䈀漀搀礀Ⰰ 爀攀昀一漀搀攀⤀㬀ഀഀ
      bool = div.offsetWidth === 42;਍      搀漀挀䔀氀攀洀⸀爀攀洀漀瘀攀䌀栀椀氀搀⠀昀愀欀攀䈀漀搀礀⤀㬀ഀഀ
      return {਍        洀愀琀挀栀攀猀㨀 戀漀漀氀Ⰰഀഀ
        media: q਍      紀㬀ഀഀ
    };਍  紀⠀眀⸀搀漀挀甀洀攀渀琀⤀㬀ഀഀ
})(this);਍ഀഀ
(function(w) {਍  ∀甀猀攀 猀琀爀椀挀琀∀㬀ഀഀ
  var respond = {};਍  眀⸀爀攀猀瀀漀渀搀 㴀 爀攀猀瀀漀渀搀㬀ഀഀ
  respond.update = function() {};਍  瘀愀爀 爀攀焀甀攀猀琀儀甀攀甀攀 㴀 嬀崀Ⰰ 砀洀氀䠀琀琀瀀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var xmlhttpmethod = false;਍    琀爀礀 笀ഀഀ
      xmlhttpmethod = new w.XMLHttpRequest();਍    紀 挀愀琀挀栀 ⠀攀⤀ 笀ഀഀ
      xmlhttpmethod = new w.ActiveXObject("Microsoft.XMLHTTP");਍    紀ഀഀ
    return function() {਍      爀攀琀甀爀渀 砀洀氀栀琀琀瀀洀攀琀栀漀搀㬀ഀഀ
    };਍  紀⠀⤀Ⰰ 愀樀愀砀 㴀 昀甀渀挀琀椀漀渀⠀甀爀氀Ⰰ 挀愀氀氀戀愀挀欀⤀ 笀ഀഀ
    var req = xmlHttp();਍    椀昀 ⠀℀爀攀焀⤀ 笀ഀഀ
      return;਍    紀ഀഀ
    req.open("GET", url, true);਍    爀攀焀⸀漀渀爀攀愀搀礀猀琀愀琀攀挀栀愀渀最攀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
      if (req.readyState !== 4 || req.status !== 200 && req.status !== 304) {਍        爀攀琀甀爀渀㬀ഀഀ
      }਍      挀愀氀氀戀愀挀欀⠀爀攀焀⸀爀攀猀瀀漀渀猀攀吀攀砀琀⤀㬀ഀഀ
    };਍    椀昀 ⠀爀攀焀⸀爀攀愀搀礀匀琀愀琀攀 㴀㴀㴀 㐀⤀ 笀ഀഀ
      return;਍    紀ഀഀ
    req.send(null);਍  紀Ⰰ 椀猀唀渀猀甀瀀瀀漀爀琀攀搀䴀攀搀椀愀儀甀攀爀礀 㴀 昀甀渀挀琀椀漀渀⠀焀甀攀爀礀⤀ 笀ഀഀ
    return query.replace(respond.regex.minmaxwh, "").match(respond.regex.other);਍  紀㬀ഀഀ
  respond.ajax = ajax;਍  爀攀猀瀀漀渀搀⸀焀甀攀甀攀 㴀 爀攀焀甀攀猀琀儀甀攀甀攀㬀ഀഀ
  respond.unsupportedmq = isUnsupportedMediaQuery;਍  爀攀猀瀀漀渀搀⸀爀攀最攀砀 㴀 笀ഀഀ
    media: /@media[^\{]+\{([^\{\}]*\{[^\}\{]*\})+/gi,਍    欀攀礀昀爀愀洀攀猀㨀 ⼀䀀⠀㼀㨀尀ⴀ⠀㼀㨀漀簀洀漀稀簀眀攀戀欀椀琀⤀尀ⴀ⤀㼀欀攀礀昀爀愀洀攀猀嬀帀尀笀崀⬀尀笀⠀㼀㨀嬀帀尀笀尀紀崀⨀尀笀嬀帀尀紀尀笀崀⨀尀紀⤀⬀嬀帀尀紀崀⨀尀紀⼀最椀Ⰰഀഀ
    comments: /\/\*[^*]*\*+([^/][^*]*\*+)*\//gi,਍    甀爀氀猀㨀 ⼀⠀甀爀氀尀⠀⤀嬀✀∀崀㼀⠀嬀帀尀⼀尀⤀✀∀崀嬀帀㨀尀⤀✀∀崀⬀⤀嬀✀∀崀㼀⠀尀⤀⤀⼀最Ⰰഀഀ
    findStyles: /@media *([^\{]+)\{([\S\s]+?)$/,਍    漀渀氀礀㨀 ⼀⠀漀渀氀礀尀猀⬀⤀㼀⠀嬀愀ⴀ稀䄀ⴀ娀崀⬀⤀尀猀㼀⼀Ⰰഀഀ
    minw: /\(\s*min\-width\s*:\s*(\s*[0-9\.]+)(px|em)\s*\)/,਍    洀愀砀眀㨀 ⼀尀⠀尀猀⨀洀愀砀尀ⴀ眀椀搀琀栀尀猀⨀㨀尀猀⨀⠀尀猀⨀嬀　ⴀ㤀尀⸀崀⬀⤀⠀瀀砀簀攀洀⤀尀猀⨀尀⤀⼀Ⰰഀഀ
    minmaxwh: /\(\s*m(in|ax)\-(height|width)\s*:\s*(\s*[0-9\.]+)(px|em)\s*\)/gi,਍    漀琀栀攀爀㨀 ⼀尀⠀嬀帀尀⤀崀⨀尀⤀⼀最ഀഀ
  };਍  爀攀猀瀀漀渀搀⸀洀攀搀椀愀儀甀攀爀椀攀猀匀甀瀀瀀漀爀琀攀搀 㴀 眀⸀洀愀琀挀栀䴀攀搀椀愀 ☀☀ 眀⸀洀愀琀挀栀䴀攀搀椀愀⠀∀漀渀氀礀 愀氀氀∀⤀ ℀㴀㴀 渀甀氀氀 ☀☀ 眀⸀洀愀琀挀栀䴀攀搀椀愀⠀∀漀渀氀礀 愀氀氀∀⤀⸀洀愀琀挀栀攀猀㬀ഀഀ
  if (respond.mediaQueriesSupported) {਍    爀攀琀甀爀渀㬀ഀഀ
  }਍  瘀愀爀 搀漀挀 㴀 眀⸀搀漀挀甀洀攀渀琀Ⰰ 搀漀挀䔀氀攀洀 㴀 搀漀挀⸀搀漀挀甀洀攀渀琀䔀氀攀洀攀渀琀Ⰰ 洀攀搀椀愀猀琀礀氀攀猀 㴀 嬀崀Ⰰ 爀甀氀攀猀 㴀 嬀崀Ⰰ 愀瀀瀀攀渀搀攀搀䔀氀猀 㴀 嬀崀Ⰰ 瀀愀爀猀攀搀匀栀攀攀琀猀 㴀 笀紀Ⰰ 爀攀猀椀稀攀吀栀爀漀琀琀氀攀 㴀 ㌀　Ⰰ 栀攀愀搀 㴀 搀漀挀⸀最攀琀䔀氀攀洀攀渀琀猀䈀礀吀愀最一愀洀攀⠀∀栀攀愀搀∀⤀嬀　崀 簀簀 搀漀挀䔀氀攀洀Ⰰ 戀愀猀攀 㴀 搀漀挀⸀最攀琀䔀氀攀洀攀渀琀猀䈀礀吀愀最一愀洀攀⠀∀戀愀猀攀∀⤀嬀　崀Ⰰ 氀椀渀欀猀 㴀 栀攀愀搀⸀最攀琀䔀氀攀洀攀渀琀猀䈀礀吀愀最一愀洀攀⠀∀氀椀渀欀∀⤀Ⰰ 氀愀猀琀䌀愀氀氀Ⰰ 爀攀猀椀稀攀䐀攀昀攀爀Ⰰ 攀洀椀渀瀀砀Ⰰ 最攀琀䔀洀嘀愀氀甀攀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    var ret, div = doc.createElement("div"), body = doc.body, originalHTMLFontSize = docElem.style.fontSize, originalBodyFontSize = body && body.style.fontSize, fakeUsed = false;਍    搀椀瘀⸀猀琀礀氀攀⸀挀猀猀吀攀砀琀 㴀 ∀瀀漀猀椀琀椀漀渀㨀愀戀猀漀氀甀琀攀㬀昀漀渀琀ⴀ猀椀稀攀㨀㄀攀洀㬀眀椀搀琀栀㨀㄀攀洀∀㬀ഀഀ
    if (!body) {਍      戀漀搀礀 㴀 昀愀欀攀唀猀攀搀 㴀 搀漀挀⸀挀爀攀愀琀攀䔀氀攀洀攀渀琀⠀∀戀漀搀礀∀⤀㬀ഀഀ
      body.style.background = "none";਍    紀ഀഀ
    docElem.style.fontSize = "100%";਍    戀漀搀礀⸀猀琀礀氀攀⸀昀漀渀琀匀椀稀攀 㴀 ∀㄀　　─∀㬀ഀഀ
    body.appendChild(div);਍    椀昀 ⠀昀愀欀攀唀猀攀搀⤀ 笀ഀഀ
      docElem.insertBefore(body, docElem.firstChild);਍    紀ഀഀ
    ret = div.offsetWidth;਍    椀昀 ⠀昀愀欀攀唀猀攀搀⤀ 笀ഀഀ
      docElem.removeChild(body);਍    紀 攀氀猀攀 笀ഀഀ
      body.removeChild(div);਍    紀ഀഀ
    docElem.style.fontSize = originalHTMLFontSize;਍    椀昀 ⠀漀爀椀最椀渀愀氀䈀漀搀礀䘀漀渀琀匀椀稀攀⤀ 笀ഀഀ
      body.style.fontSize = originalBodyFontSize;਍    紀ഀഀ
    ret = eminpx = parseFloat(ret);਍    爀攀琀甀爀渀 爀攀琀㬀ഀഀ
  }, applyMedia = function(fromResize) {਍    瘀愀爀 渀愀洀攀 㴀 ∀挀氀椀攀渀琀圀椀搀琀栀∀Ⰰ 搀漀挀䔀氀攀洀倀爀漀瀀 㴀 搀漀挀䔀氀攀洀嬀渀愀洀攀崀Ⰰ 挀甀爀爀圀椀搀琀栀 㴀 搀漀挀⸀挀漀洀瀀愀琀䴀漀搀攀 㴀㴀㴀 ∀䌀匀匀㄀䌀漀洀瀀愀琀∀ ☀☀ 搀漀挀䔀氀攀洀倀爀漀瀀 簀簀 搀漀挀⸀戀漀搀礀嬀渀愀洀攀崀 簀簀 搀漀挀䔀氀攀洀倀爀漀瀀Ⰰ 猀琀礀氀攀䈀氀漀挀欀猀 㴀 笀紀Ⰰ 氀愀猀琀䰀椀渀欀 㴀 氀椀渀欀猀嬀氀椀渀欀猀⸀氀攀渀最琀栀 ⴀ ㄀崀Ⰰ 渀漀眀 㴀 渀攀眀 䐀愀琀攀⠀⤀⸀最攀琀吀椀洀攀⠀⤀㬀ഀഀ
    if (fromResize && lastCall && now - lastCall < resizeThrottle) {਍      眀⸀挀氀攀愀爀吀椀洀攀漀甀琀⠀爀攀猀椀稀攀䐀攀昀攀爀⤀㬀ഀഀ
      resizeDefer = w.setTimeout(applyMedia, resizeThrottle);਍      爀攀琀甀爀渀㬀ഀഀ
    } else {਍      氀愀猀琀䌀愀氀氀 㴀 渀漀眀㬀ഀഀ
    }਍    昀漀爀 ⠀瘀愀爀 椀 椀渀 洀攀搀椀愀猀琀礀氀攀猀⤀ 笀ഀഀ
      if (mediastyles.hasOwnProperty(i)) {਍        瘀愀爀 琀栀椀猀猀琀礀氀攀 㴀 洀攀搀椀愀猀琀礀氀攀猀嬀椀崀Ⰰ 洀椀渀 㴀 琀栀椀猀猀琀礀氀攀⸀洀椀渀眀Ⰰ 洀愀砀 㴀 琀栀椀猀猀琀礀氀攀⸀洀愀砀眀Ⰰ 洀椀渀渀甀氀氀 㴀 洀椀渀 㴀㴀㴀 渀甀氀氀Ⰰ 洀愀砀渀甀氀氀 㴀 洀愀砀 㴀㴀㴀 渀甀氀氀Ⰰ 攀洀 㴀 ∀攀洀∀㬀ഀഀ
        if (!!min) {਍          洀椀渀 㴀 瀀愀爀猀攀䘀氀漀愀琀⠀洀椀渀⤀ ⨀ ⠀洀椀渀⸀椀渀搀攀砀伀昀⠀攀洀⤀ 㸀 ⴀ㄀ 㼀 攀洀椀渀瀀砀 簀簀 最攀琀䔀洀嘀愀氀甀攀⠀⤀ 㨀 ㄀⤀㬀ഀഀ
        }਍        椀昀 ⠀℀℀洀愀砀⤀ 笀ഀഀ
          max = parseFloat(max) * (max.indexOf(em) > -1 ? eminpx || getEmValue() : 1);਍        紀ഀഀ
        if (!thisstyle.hasquery || (!minnull || !maxnull) && (minnull || currWidth >= min) && (maxnull || currWidth <= max)) {਍          椀昀 ⠀℀猀琀礀氀攀䈀氀漀挀欀猀嬀琀栀椀猀猀琀礀氀攀⸀洀攀搀椀愀崀⤀ 笀ഀഀ
            styleBlocks[thisstyle.media] = [];਍          紀ഀഀ
          styleBlocks[thisstyle.media].push(rules[thisstyle.rules]);਍        紀ഀഀ
      }਍    紀ഀഀ
    for (var j in appendedEls) {਍      椀昀 ⠀愀瀀瀀攀渀搀攀搀䔀氀猀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀樀⤀⤀ 笀ഀഀ
        if (appendedEls[j] && appendedEls[j].parentNode === head) {਍          栀攀愀搀⸀爀攀洀漀瘀攀䌀栀椀氀搀⠀愀瀀瀀攀渀搀攀搀䔀氀猀嬀樀崀⤀㬀ഀഀ
        }਍      紀ഀഀ
    }਍    愀瀀瀀攀渀搀攀搀䔀氀猀⸀氀攀渀最琀栀 㴀 　㬀ഀഀ
    for (var k in styleBlocks) {਍      椀昀 ⠀猀琀礀氀攀䈀氀漀挀欀猀⸀栀愀猀伀眀渀倀爀漀瀀攀爀琀礀⠀欀⤀⤀ 笀ഀഀ
        var ss = doc.createElement("style"), css = styleBlocks[k].join("\n");਍        猀猀⸀琀礀瀀攀 㴀 ∀琀攀砀琀⼀挀猀猀∀㬀ഀഀ
        ss.media = k;਍        栀攀愀搀⸀椀渀猀攀爀琀䈀攀昀漀爀攀⠀猀猀Ⰰ 氀愀猀琀䰀椀渀欀⸀渀攀砀琀匀椀戀氀椀渀最⤀㬀ഀഀ
        if (ss.styleSheet) {਍          猀猀⸀猀琀礀氀攀匀栀攀攀琀⸀挀猀猀吀攀砀琀 㴀 挀猀猀㬀ഀഀ
        } else {਍          猀猀⸀愀瀀瀀攀渀搀䌀栀椀氀搀⠀搀漀挀⸀挀爀攀愀琀攀吀攀砀琀一漀搀攀⠀挀猀猀⤀⤀㬀ഀഀ
        }਍        愀瀀瀀攀渀搀攀搀䔀氀猀⸀瀀甀猀栀⠀猀猀⤀㬀ഀഀ
      }਍    紀ഀഀ
  }, translate = function(styles, href, media) {਍    瘀愀爀 焀猀 㴀 猀琀礀氀攀猀⸀爀攀瀀氀愀挀攀⠀爀攀猀瀀漀渀搀⸀爀攀最攀砀⸀挀漀洀洀攀渀琀猀Ⰰ ∀∀⤀⸀爀攀瀀氀愀挀攀⠀爀攀猀瀀漀渀搀⸀爀攀最攀砀⸀欀攀礀昀爀愀洀攀猀Ⰰ ∀∀⤀⸀洀愀琀挀栀⠀爀攀猀瀀漀渀搀⸀爀攀最攀砀⸀洀攀搀椀愀⤀Ⰰ 焀氀 㴀 焀猀 ☀☀ 焀猀⸀氀攀渀最琀栀 簀簀 　㬀ഀഀ
    href = href.substring(0, href.lastIndexOf("/"));਍    瘀愀爀 爀攀瀀唀爀氀猀 㴀 昀甀渀挀琀椀漀渀⠀挀猀猀⤀ 笀ഀഀ
      return css.replace(respond.regex.urls, "$1" + href + "$2$3");਍    紀Ⰰ 甀猀攀䴀攀搀椀愀 㴀 ℀焀氀 ☀☀ 洀攀搀椀愀㬀ഀഀ
    if (href.length) {਍      栀爀攀昀 ⬀㴀 ∀⼀∀㬀ഀഀ
    }਍    椀昀 ⠀甀猀攀䴀攀搀椀愀⤀ 笀ഀഀ
      ql = 1;਍    紀ഀഀ
    for (var i = 0; i < ql; i++) {਍      瘀愀爀 昀甀氀氀焀Ⰰ 琀栀椀猀焀Ⰰ 攀愀挀栀焀Ⰰ 攀焀氀㬀ഀഀ
      if (useMedia) {਍        昀甀氀氀焀 㴀 洀攀搀椀愀㬀ഀഀ
        rules.push(repUrls(styles));਍      紀 攀氀猀攀 笀ഀഀ
        fullq = qs[i].match(respond.regex.findStyles) && RegExp.$1;਍        爀甀氀攀猀⸀瀀甀猀栀⠀刀攀最䔀砀瀀⸀␀㈀ ☀☀ 爀攀瀀唀爀氀猀⠀刀攀最䔀砀瀀⸀␀㈀⤀⤀㬀ഀഀ
      }਍      攀愀挀栀焀 㴀 昀甀氀氀焀⸀猀瀀氀椀琀⠀∀Ⰰ∀⤀㬀ഀഀ
      eql = eachq.length;਍      昀漀爀 ⠀瘀愀爀 樀 㴀 　㬀 樀 㰀 攀焀氀㬀 樀⬀⬀⤀ 笀ഀഀ
        thisq = eachq[j];਍        椀昀 ⠀椀猀唀渀猀甀瀀瀀漀爀琀攀搀䴀攀搀椀愀儀甀攀爀礀⠀琀栀椀猀焀⤀⤀ 笀ഀഀ
          continue;਍        紀ഀഀ
        mediastyles.push({਍          洀攀搀椀愀㨀 琀栀椀猀焀⸀猀瀀氀椀琀⠀∀⠀∀⤀嬀　崀⸀洀愀琀挀栀⠀爀攀猀瀀漀渀搀⸀爀攀最攀砀⸀漀渀氀礀⤀ ☀☀ 刀攀最䔀砀瀀⸀␀㈀ 簀簀 ∀愀氀氀∀Ⰰഀഀ
          rules: rules.length - 1,਍          栀愀猀焀甀攀爀礀㨀 琀栀椀猀焀⸀椀渀搀攀砀伀昀⠀∀⠀∀⤀ 㸀 ⴀ㄀Ⰰഀഀ
          minw: thisq.match(respond.regex.minw) && parseFloat(RegExp.$1) + (RegExp.$2 || ""),਍          洀愀砀眀㨀 琀栀椀猀焀⸀洀愀琀挀栀⠀爀攀猀瀀漀渀搀⸀爀攀最攀砀⸀洀愀砀眀⤀ ☀☀ 瀀愀爀猀攀䘀氀漀愀琀⠀刀攀最䔀砀瀀⸀␀㄀⤀ ⬀ ⠀刀攀最䔀砀瀀⸀␀㈀ 簀簀 ∀∀⤀ഀഀ
        });਍      紀ഀഀ
    }਍    愀瀀瀀氀礀䴀攀搀椀愀⠀⤀㬀ഀഀ
  }, makeRequests = function() {਍    椀昀 ⠀爀攀焀甀攀猀琀儀甀攀甀攀⸀氀攀渀最琀栀⤀ 笀ഀഀ
      var thisRequest = requestQueue.shift();਍      愀樀愀砀⠀琀栀椀猀刀攀焀甀攀猀琀⸀栀爀攀昀Ⰰ 昀甀渀挀琀椀漀渀⠀猀琀礀氀攀猀⤀ 笀ഀഀ
        translate(styles, thisRequest.href, thisRequest.media);਍        瀀愀爀猀攀搀匀栀攀攀琀猀嬀琀栀椀猀刀攀焀甀攀猀琀⸀栀爀攀昀崀 㴀 琀爀甀攀㬀ഀഀ
        w.setTimeout(function() {਍          洀愀欀攀刀攀焀甀攀猀琀猀⠀⤀㬀ഀഀ
        }, 0);਍      紀⤀㬀ഀഀ
    }਍  紀Ⰰ 爀椀瀀䌀匀匀 㴀 昀甀渀挀琀椀漀渀⠀⤀ 笀ഀഀ
    for (var i = 0; i < links.length; i++) {਍      瘀愀爀 猀栀攀攀琀 㴀 氀椀渀欀猀嬀椀崀Ⰰ 栀爀攀昀 㴀 猀栀攀攀琀⸀栀爀攀昀Ⰰ 洀攀搀椀愀 㴀 猀栀攀攀琀⸀洀攀搀椀愀Ⰰ 椀猀䌀匀匀 㴀 猀栀攀攀琀⸀爀攀氀 ☀☀ 猀栀攀攀琀⸀爀攀氀⸀琀漀䰀漀眀攀爀䌀愀猀攀⠀⤀ 㴀㴀㴀 ∀猀琀礀氀攀猀栀攀攀琀∀㬀ഀഀ
      if (!!href && isCSS && !parsedSheets[href]) {਍        椀昀 ⠀猀栀攀攀琀⸀猀琀礀氀攀匀栀攀攀琀 ☀☀ 猀栀攀攀琀⸀猀琀礀氀攀匀栀攀攀琀⸀爀愀眀䌀猀猀吀攀砀琀⤀ 笀ഀഀ
          translate(sheet.styleSheet.rawCssText, href, media);਍          瀀愀爀猀攀搀匀栀攀攀琀猀嬀栀爀攀昀崀 㴀 琀爀甀攀㬀ഀഀ
        } else {਍          椀昀 ⠀℀⼀帀⠀嬀愀ⴀ稀䄀ⴀ娀㨀崀⨀尀⼀尀⼀⤀⼀⸀琀攀猀琀⠀栀爀攀昀⤀ ☀☀ ℀戀愀猀攀 簀簀 栀爀攀昀⸀爀攀瀀氀愀挀攀⠀刀攀最䔀砀瀀⸀␀㄀Ⰰ ∀∀⤀⸀猀瀀氀椀琀⠀∀⼀∀⤀嬀　崀 㴀㴀㴀 眀⸀氀漀挀愀琀椀漀渀⸀栀漀猀琀⤀ 笀ഀഀ
            if (href.substring(0, 2) === "//") {਍              栀爀攀昀 㴀 眀⸀氀漀挀愀琀椀漀渀⸀瀀爀漀琀漀挀漀氀 ⬀ 栀爀攀昀㬀ഀഀ
            }਍            爀攀焀甀攀猀琀儀甀攀甀攀⸀瀀甀猀栀⠀笀ഀഀ
              href: href,਍              洀攀搀椀愀㨀 洀攀搀椀愀ഀഀ
            });਍          紀ഀഀ
        }਍      紀ഀഀ
    }਍    洀愀欀攀刀攀焀甀攀猀琀猀⠀⤀㬀ഀഀ
  };਍  爀椀瀀䌀匀匀⠀⤀㬀ഀഀ
  respond.update = ripCSS;਍  爀攀猀瀀漀渀搀⸀最攀琀䔀洀嘀愀氀甀攀 㴀 最攀琀䔀洀嘀愀氀甀攀㬀ഀഀ
  function callMedia() {਍    愀瀀瀀氀礀䴀攀搀椀愀⠀琀爀甀攀⤀㬀ഀഀ
  }਍  椀昀 ⠀眀⸀愀搀搀䔀瘀攀渀琀䰀椀猀琀攀渀攀爀⤀ 笀ഀഀ
    w.addEventListener("resize", callMedia, false);਍  紀 攀氀猀攀 椀昀 ⠀眀⸀愀琀琀愀挀栀䔀瘀攀渀琀⤀ 笀ഀഀ
    w.attachEvent("onresize", callMedia);਍  紀ഀഀ
})(this);