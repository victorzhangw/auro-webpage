DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();

const ecReporturl = GoogleremoteUrl + 'ECReport';
const gaReporturl = GoogleremoteUrl + 'CustomGAReport';
var ecKey,gaKey,dp,L10n = ej.base.L10n;
const twNorthRegionName=["基隆市","臺北市","新北市","桃園市","新竹市","新竹縣","宜蘭縣","台北縣"];
const twMiddleRegionName=["苗栗縣","臺中市","彰化縣","南投縣","雲林縣"];
const twSouthRegionName=["嘉義市","嘉義縣","臺南市","高雄市","屏東縣"];
const twEastRegionName=["花蓮縣","臺東縣"];


$(document).ready(function () {
  
    DramaCore.validateTokenCall(validateSetting());
    let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
    dp = DramaCore.createDatePicker(dt, dt);
    
    if(sessionStorage.getItem(defaultStartDate)&&sessionStorage.getItem(defaultEndDate)&&sessionStorage.getItem(daySpan) ){
      dp.value=[sessionStorage.getItem(defaultStartDate),sessionStorage.getItem(defaultEndDate)];  
      ecKey= "gaec-" + sessionStorage.getItem(defaultStartDate) + "-" + sessionStorage.getItem(defaultEndDate) + "-" + sessionStorage.getItem(daySpan);
        gaKey= "ga-" + sessionStorage.getItem(defaultStartDate) + "-" + sessionStorage.getItem(defaultEndDate) + "-" + sessionStorage.getItem(daySpan);
        if(localStorage.getItem(ecKey)){
            DramaCore.renderGAECChart(ecKey);
        }else{
            let type = "post";
            para = {
                "MemberKey": memberKey,
                "StartDate": sessionStorage.getItem(defaultStartDate),
                "EndDate": sessionStorage.getItem(defaultEndDate),
                "LoginId": loginID,
                "ReqDimension1":CustomGARequest.GA_Dimensions1.join(),
                "ReqMetric1":CustomGARequest.GA_Metrics1.join(),
                "ReqDimension2":CustomGARequest.GA_Dimensions2.join(),
                "ReqMetric2":CustomGARequest.GA_Metrics2.join(),
                "ReqDimension3":CustomGARequest.GA_Dimensions3.join(),
                "ReqMetric3":CustomGARequest.GA_Metrics3.join(),
                "ReqDimension4":CustomGARequest.GA_Dimensions4.join(),
                "ReqMetric4":CustomGARequest.GA_Metrics4.join(),
                "ReqDimension5":CustomGARequest.GA_Dimensions5.join(),
                "ReqMetric5":CustomGARequest.GA_Metrics5.join()
              };
                global_EC_Key=DramaCore.getRemoteData(ecReporturl, type, para,ecKey,fromEC);  
                
                let timeoutID = window.setInterval(function(){
                if(global_EC_Key){ 
                  
                  //global_custom_Key=DramaCore.getSilenceRemoteData(gaReporturl, type, para,gaKey);  
                  window.clearInterval(timeoutID);
                  DramaCore.renderGAECChart(global_EC_Key);
                  global_EC_Key="";
                }
              },500);
        }
    }

    dp.addEventListener("change",function()
    {
      let dayRange = this.getSelectedRange();
        if (dayRange.daySpan > 0) {
          let _sd = moment(this.startDate).format("YYYY-MM-DD");
          let _ed = moment(this.endDate).format("YYYY-MM-DD");
          sessionStorage.setItem(defaultStartDate ,_sd);
          sessionStorage.setItem(defaultEndDate, _ed);
          sessionStorage.setItem('daySpan', dayRange.daySpan);
          let type = "post";
          let para = {
            "MemberKey": memberKey,
            "StartDate": _sd,
            "EndDate": _ed,
            "LoginId": loginID,
            "ReqDimension1":CustomGARequest.GA_Dimensions1.join(),
            "ReqMetric1":CustomGARequest.GA_Metrics1.join(),
            "ReqDimension2":CustomGARequest.GA_Dimensions2.join(),
            "ReqMetric2":CustomGARequest.GA_Metrics2.join(),
            "ReqDimension3":CustomGARequest.GA_Dimensions3.join(),
            "ReqMetric3":CustomGARequest.GA_Metrics3.join(),
            "ReqDimension4":CustomGARequest.GA_Dimensions4.join(),
            "ReqMetric4":CustomGARequest.GA_Metrics4.join(),
            "ReqDimension5":CustomGARequest.GA_Dimensions5.join(),
            "ReqMetric5":CustomGARequest.GA_Metrics5.join()

          };
          if (_sd && _ed) {
            let newecKey = "gaec-" + _sd + "-" + _ed + "-" + dayRange.daySpan;
            let newgaKey = "ga-" + _sd + "-" + _ed + "-" + dayRange.daySpan;
            
            if (localStorage.getItem(newecKey)) {
                DramaCore.renderGAECChart(newecKey);
            } else {
             
              para = {
                "MemberKey": memberKey,
                "StartDate": _sd,
                "EndDate": _ed,
                "LoginId": loginID,
                "ReqDimension1":CustomGARequest.GA_Dimensions1.join(),
                "ReqMetric1":CustomGARequest.GA_Metrics1.join(),
                "ReqDimension2":CustomGARequest.GA_Dimensions2.join(),
                "ReqMetric2":CustomGARequest.GA_Metrics2.join(),
                "ReqDimension3":CustomGARequest.GA_Dimensions3.join(),
                "ReqMetric3":CustomGARequest.GA_Metrics3.join(),
                "ReqDimension4":CustomGARequest.GA_Dimensions4.join(),
                "ReqMetric4":CustomGARequest.GA_Metrics4.join(),
                "ReqDimension5":CustomGARequest.GA_Dimensions5.join(),
                "ReqMetric5":CustomGARequest.GA_Metrics5.join()
              };
              global_EC_Key=DramaCore.getRemoteData(ecReporturl, type, para,newecKey,fromEC);  
              let timeoutID = window.setInterval(function(){
                
                if(global_EC_Key){  
                  
                  global_custom_Key=DramaCore.getSilenceRemoteData(gaReporturl, type, para,newgaKey,fromGA); 
                  window.clearInterval(timeoutID);
                  DramaCore.renderGAECChart(global_EC_Key);
                  global_EC_Key="";
                }
              },500);
                 
            }

          }
        }
    });
});
var initDefaultECPage={
  widget:function(){
      var  widgetMetaData=[
          {id:'w1',icons:'e-icons session'},
          {id:'w2',icons:'e-icons views'},
          {id:'w3',icons:'e-icons profile'},
          {id:'w4',icons:'e-icons profile'},
          {id:'w5',icons:'e-icons avgtime'},
          {id:'w6',icons:'e-icons session1'},
          {id:'w7',icons:'e-icons profile1'},
          {id:'w8',icons:'e-icons bounce'}
        
      ];
      var getWidgetString = ej.base.compile('<div class="col-sm-12 col-md-3 mb-1"> <div id=${id} class="card card_animation"> <div class="card-header widget-head "> <h5 class="widget-text align-middle"></h5> </div><div class="card-body"> <span class="${icons}"></span> <h5 class="widget-number align-middle"></h5> </div></div></div>');
      
      while (widgetitems.firstChild) {
        widgetitems.removeChild(widgetitems.firstChild);
      }
      widgetMetaData.forEach(data => {
          widgetitems.appendChild(getWidgetString(data)[0]);
      });
  },
  chart:function(){
      let  chartMetaData=[
          {id:'chart1',size:'col-md-4'},
          {id:'chart2',size:'col-md-4'},
          {id:'chart3',size:'col-md-4'},
          {id:'chart4',size:'col-md-4'},
          {id:'chart5',size:'col-md-4'},
          {id:'chart6',size:'col-md-4'},
          {id:'chart7',size:'col-md-12'},
    
        
      ];
      let getChartString = ej.base.compile('<div class="col-sm-12 ${size} mb-1 d-flex"> <div id=${id} class="card card_animation flex-fill"> <div class="card-header widget-head"> </div><div class="charts card-body chart-height"> </div> </div>  </div>');
      while (chartitems.firstChild) {
        chartitems.removeChild(chartitems.firstChild);
      }
      chartMetaData.forEach(data => {
          chartitems.appendChild(getChartString(data)[0]);
      });
  }

};
var ECChart={
  chart1: function (data,location, rowMax, header,chartid) {
      rowMax += 1; // 因迴圈需求，資料筆數再加 1
      
      let rst1 = data[1].GA_Reports[location],rst2= data[2].GA_Reports[location],
        chartObj = {},
        legend=[],
        dimension = ["第一週","第二週","第三週","第四週"],
        metric1 = [],
        metric2 = [];

      for (let i = 0; i < rst1.ListResult.length; i++) {
          let arr = rst1.ListResult[i];
          if(arr.dimension[0]=="Returning Visitor"){
              legend.push("回訪客");
              for(let j=0 ;j< arr.metrics.length;j++){
              
                  metric2.push(arr.metrics[j].metric[1].value);
              }
              
          }else if(arr.dimension[0]=="New Visitor"){
              legend.push("新訪客");
              for(let j=0 ;j< arr.metrics.length;j++){
              
                  metric1.push(arr.metrics[j].metric[1].value);
              }
          }
          
         
          
        //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
        
      }
      for (let i = 0; i < rst2.ListResult.length; i++) {
        let arr = rst2.ListResult[i];
        if(arr.dimension[0]=="Returning Visitor"){
           
            for(let j=0 ;j< arr.metrics.length;j++){
            
                metric2.push(arr.metrics[j].metric[1].value);
            }
            
        }else if(arr.dimension[0]=="New Visitor"){
           
            for(let j=0 ;j< arr.metrics.length;j++){
            
                metric1.push(arr.metrics[j].metric[1].value);
            }
        }
        
       
        
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      
    }

      let color = ["#FB7507","#BD10E0"];
      let setting = {
        cardHead: header,
        titleText: "",
        category: dimension,
        value1: metric1,
        value2: metric2,
        color: color,
        legend: legend
      };
      let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
      let option = chartSetting.doubleBarChartVertical(setting);
      if (option && typeof option === "object") {
        console.info(chartid);
        chart.setOption(option, true);
        $("#"+chartid).children(".card-header").text(setting.cardHead);
  
      }
    },
  chart2: function (data,location, rowMax, header,chartid) {
    let rst = data[location];
    let legend=[],
        color=[],
        dimension=[],
        seriesdata=[],
        tmpObj1 = {},
        tmpObj2 = {},
        metric1 = [],
        metric2 = [];
    let symbol = "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNTYiIGhlaWdodD0iMTIyIiBmaWxsPSIjMjMxODE1Ij48cGF0aCBkPSJNMTEzLjE1MiA0My40OTRoLTYuMDc1bC0xNC4wNCAzNC4yNGg3LjkzMmwyLjcxNy02LjY0N2gxMi45OTdsMi43NzMgNi42NDdoNy45NzJsLTE0LjI3Ni0zNC4yNHptLjggMjEuMDQ3aC03LjU5NmwzLjc2LTkuMiAzLjgzNyA5LjJ6bS0yOS4yNSAxLjUzbC0xLjE1NC0uNjZjLTEuNjQyIDMuMjA1LTQuODk0IDUuNC04LjY0IDUuNC01LjQwNCAwLTkuNzgyLTQuNTQ2LTkuNzgyLTEwLjE0OFM2OS41MDUgNTAuNSA3NC45IDUwLjVjMy43NDUgMCA2Ljk5NyAyLjE4IDguNjQgNS40bDEuMTU0LS42NjUgNS4zMzYtMy4wODNjLTIuOS01LjU2Ni04LjU5Ni05LjM0My0xNS4xMjgtOS4zNDMtOS41IDAtMTcuMiA4LTE3LjIgMTcuODU0IDAgOS44NTQgNy43MDIgMTcuODUgMTcuMiAxNy44NSA2LjUzMyAwIDEyLjIxOC0zLjc4MyAxNS4xMjgtOS4zNWwtNS4zMzYtMy4wODR6bTk3LjgtMjMuMjcyYy05LjUgMC0xNy4yIDgtMTcuMiAxNy44NTQgMCA5Ljg1NCA3LjcgMTcuODUgMTcuMiAxNy44NXMxNy4yMDQtNy45OTYgMTcuMjA0LTE3Ljg1YzAtOS44NjMtNy43LTE3Ljg1NC0xNy4yMDQtMTcuODU0em0wIDI4LjAwMmMtNS4zOTcgMC05Ljc4LTQuNTQ2LTkuNzgtMTAuMTQ4czQuMzgzLTEwLjE1IDkuNzgtMTAuMTVjNS40MDcgMCA5Ljc4MyA0LjU0NCA5Ljc4MyAxMC4xNVMxODcuOSA3MC44IDE4Mi41MDMgNzAuOHptLTI2LjYyNy00LjczbC0xLjE0Ni0uNjZjLTEuNjUgMy4yMDUtNC45IDUuNC04LjY1IDUuNC01LjM5OCAwLTkuNzc3LTQuNTQ2LTkuNzc3LTEwLjE0OHM0LjM4LTEwLjE1IDkuNzc3LTEwLjE1YzMuNzUgMCA3IDIuMTggOC42NSA1LjRsMS4xNDYtLjY2NSA1LjMzMy0zLjA4M2MtMi45MTItNS41NjYtOC42LTkuMzQzLTE1LjEzLTkuMzQzLTkuNSAwLTE3LjE5NyA4LTE3LjE5NyAxNy44NTQgMCA5Ljg1NCA3LjY5NiAxNy44NSAxNy4xOTcgMTcuODUgNi41MzggMCAxMi4yMTctMy43ODMgMTUuMTMtOS4zNWwtNS4zMzMtMy4wODR6Ii8+PC9zdmc+";  
    let areaDimesnion=["北部地區","中部地區","南部地區","東部地區","其他"],areaMetric=[0,0,0,0,0];
    rst.ListResult.forEach(function(item){
          
    if(twNorthRegionName.indexOf(item.dimension[0])>-1){
      tmpObj1.name=item.dimension[0];
      tmpObj1.value= item.metrics[0].metric[1].value;
      tmpObj2.name=areaDimesnion[0];
      tmpObj2.value=Number.parseInt(item.metrics[0].metric[1].value);
      areaMetric[0]  +=Number.parseInt(item.metrics[0].metric[1].value); 

    }else if(twMiddleRegionName.indexOf(item.dimension[0])>-1){
      tmpObj1.name=item.dimension[0];
      tmpObj1.value= item.metrics[0].metric[1].value;
      tmpObj2.name=areaDimesnion[1];
      tmpObj2.value=Number.parseInt(item.metrics[0].metric[1].value);
      areaMetric[1]  +=Number.parseInt(item.metrics[0].metric[1].value); 
    }else if(twSouthRegionName.indexOf(item.dimension[0])>-1){
      tmpObj1.name=item.dimension[0];
      tmpObj1.value= item.metrics[0].metric[1].value;
      tmpObj2.name=areaDimesnion[2];
      tmpObj2.value=Number.parseInt(item.metrics[0].metric[1].value);
      areaMetric[2]  +=Number.parseInt(item.metrics[0].metric[1].value); 
    }else if(twEastRegionName.indexOf(item.dimension[0])>-1){
      tmpObj1.name=item.dimension[0];
      tmpObj1.value= item.metrics[0].metric[1].value;
      tmpObj2.name=areaDimesnion[3];
      tmpObj2.value=Number.parseInt(item.metrics[0].metric[1].value);
      areaMetric[3]  +=Number.parseInt(item.metrics[0].metric[1].value); 
    }else{
      tmpObj2.name=areaDimesnion[4];
      tmpObj2.value=Number.parseInt(item.metrics[0].metric[1].value);
      areaMetric[4]  +=Number.parseInt(item.metrics[0].metric[1].value); 
    }
    if(Object.getOwnPropertyNames(tmpObj1).length != 0 ){
      metric1.push(tmpObj1);
      tmpObj1={};
    }
    if(Object.getOwnPropertyNames(tmpObj2).length != 0 ){
      let _index = metric2.findIndex(item=>item.name===tmpObj2.name); 
      if(_index>-1){
        metric2[_index].value += Number.parseInt(tmpObj2.value); 
      }else{
        metric2.push(tmpObj2);
      }
      tmpObj2={};
    }
    
  });

  color=['#50E372', '#BD10E0', '#FB7507', '#4A90E2'];
  color2=['#FB7507', '#7ED321', '#4A90E2', '#BD10E0'];
  let setting = {
    cardHead: header,
    titleText: "",
    category: dimension,
    value1: metric1,
    value2: metric2,
    color: color,
    color2: color2,
    legend: areaDimesnion,
    symbol:symbol
  };
  
  let chart = echarts.init(document.getElementById(chartid+"L"));
  let option = chartSetting.twMapwithPieChart(setting);
  if (option && typeof option === "object") {
    console.info(chartid);
   
    $.get('./assests/js/taiwan.json', function (twJson) {
      echarts.registerMap('TAIWAN', twJson, {});       
      setTimeout(function(){chart.setOption(option);},100);
  });
  
    $("#"+chartid).children(".card-header").text(setting.cardHead);

  }
  let chartR = echarts.init(document.getElementById(chartid+"R"));
  let optionR = chartSetting.singleCirclePiewithSymbol(setting);
  if (optionR && typeof optionR === "object") {
    chartR.setOption(optionR);

  }
        
    },
  chart3: function (data,location, rowMax, header,chartid) {
      rowMax += 1; // 因迴圈需求，資料筆數再加 1
      
      let rst1 = data[1].GA_Reports[location],rst2= data[2].GA_Reports[location],
        chartObj = {},
        legend=[],
        dimension = ["第一週","第二週","第三週","第四週"],
        metric1 = [],
        metric2 = [];
      for (let i = 0; i < rst1.ListResult.length; i++) {
          let arr = rst1.ListResult[i];
          if(arr.dimension[0]=="Returning Visitor"){
              legend.push("回訪客");
              for(let j=0 ;j< arr.metrics.length;j++){
              
                  metric2.push(arr.metrics[j].metric[0].value);
              }
              
          }else if(arr.dimension[0]=="New Visitor"){
              legend.push("新訪客");
              for(let j=0 ;j< arr.metrics.length;j++){
              
                  metric1.push(arr.metrics[j].metric[0].value);
              }
          }
          
         
          
        //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
        
      }
      for (let i = 0; i < rst2.ListResult.length; i++) {
        let arr = rst2.ListResult[i];
        if(arr.dimension[0]=="Returning Visitor"){
           
            for(let j=0 ;j< arr.metrics.length;j++){
            
                metric2.push(arr.metrics[j].metric[0].value);
            }
            
        }else if(arr.dimension[0]=="New Visitor"){
           
            for(let j=0 ;j< arr.metrics.length;j++){
            
                metric1.push(arr.metrics[j].metric[0].value);
            }
        }
        
       
        
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      
    }

      let color = ["#4A90E2","#7ED321"];
      let setting = {
        cardHead: header,
        titleText: "",
        category: dimension,
        value1: metric1,
        value2: metric2,
        color: color,
        legend: legend
      };
      let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
      let option = chartSetting.doubleBarChartVertical(setting);
      if (option && typeof option === "object") {
        console.info(chartid);
        chart.setOption(option, true);
        $("#"+chartid).children(".card-header").text(setting.cardHead);
  
      }
    },
  chart4:function(data,location, rowMax, header,chartid){
    let rst = data[location];
    let legend=["跳出率","平均訂單價值"],
        color=["#3259B8","#626c91", "#d9def7","#b84a58","#FB7507"],
        dimension=[],
        seriesdata=[],
        avg = {},
        metric1 = [],
        metric2 = [];
    let _sum1=0;
    let _sum2=0;
    let _count=0;
      rst.ListResult.forEach(function(item) {
          let x =Number.parseFloat(item.metrics[0].metric[5].value);
          let y =Number.parseFloat(item.metrics[0].metric[3].value);
          let _obj={
            name:"",
            value:[]
          };
          if (y!=0){
            _count+=1;
            _sum1 += x;
          _sum2 += y;
          metric1.push(x);
          metric2.push(y);
          _obj.name=item.dimension[0];
          _obj.value.push(y);
          _obj.value.push(x);
          seriesdata.push(_obj);
          }
          
      });
      avg.xAvgLine=(_sum2 / _count).toFixed(2);
      avg.yAvgLine=(_sum1 / _count).toFixed(2);
  
      
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      seriesdata:seriesdata,
      value1: metric1,
      value2: metric2,
      avg:avg,
      formatterString:["%","元"],
      color: color,
      legend: legend
    };
   
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.fourQuadrant(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart5:function(data,location, rowMax, header,chartid){
    let rst = data[location],dimension=[],metric=[],legend=[];
    let color = ["#FB7507"];
    
    rst.ListResult.forEach(function(item){
      dimension.push(item.dimension[0]);
      metric.push(item.metrics[0].metric[1]);
    });
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: legend
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithAvgLine(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart6:function(data,location, rowMax, header,chartid){
    let rst = data[location];
    let legend=["電商轉換率","平均訂單價值"],
        color=["#3259B8","#626c91", "#d9def7","#b84a58","#FB7507"],
        dimension=[],
        seriesdata=[],
        avg = {},
        metric1 = [],
        metric2 = [];
    let _sum1=0;
    let _sum2=0;
    let _count=0;
      rst.ListResult.forEach(function(item) {
          let x =Number.parseFloat(item.metrics[0].metric[2].value);
          let y =Number.parseFloat(item.metrics[0].metric[3].value);
          let _obj={
            name:"",
            value:[]
          };
          if(y!=0){
            _count+=1;
            _sum1 += x;
            _sum2 += y;
            metric1.push(x);
            metric2.push(y);
            _obj.name=item.dimension[0];
            _obj.value.push(y);
            _obj.value.push(x);
            seriesdata.push(_obj);
          }
          
      });
      avg.xAvgLine=(_sum2 / _count).toFixed(2);
      avg.yAvgLine=(_sum1 / _count).toFixed(2);
     
      
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      seriesdata:seriesdata,
      value1: metric1,
      value2: metric2,
      avg:avg,
      formatterString:["%","元"],
      color: color,
      legend: legend
    };
   
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.fourQuadrant(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart7:function(data,location, rowMax, header,chartid){
    $('#chart7 .card-body').empty();
    const chart9Column= [{
      field: 'category',
      headerText: '品牌',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location],list=[];
    console.log(rst.ListResult);
    
    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category=rst.ListResult[0].dimension[i];
      chartObj.revenue =Number(rst.ListResult[0].metrics[0].metric[i].value) ;
      chartObj.ratio =Number(rst.ListResult[0].metrics[1].metric[i].value) ;
      list.push(chartObj);
    }
      console.log(list);
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart9Column
    };
    
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart7 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
    
  },
  chart8:function(data,location, rowMax, header,chartid){
    $('#chart8 .card-body').empty();
    const chart9Column= [{
      field: 'category',
      headerText: '類別',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location],list=[];
    console.log(rst.ListResult);
    
    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category=rst.ListResult[0].dimension[i];
      chartObj.revenue =Number(rst.ListResult[0].metrics[0].metric[i].value) ;
      chartObj.ratio =Number(rst.ListResult[0].metrics[1].metric[i].value) ;
      list.push(chartObj);
    }
      console.log(list);
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart9Column
    };
    
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart8 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
    
    
  },
  chart9:function(data,location, rowMax, header,chartid){
    $('#chart9 .card-body').empty();
    const chart9Column= [{
      field: 'category',
      headerText: '類別-顏色',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location],list=[];
    console.log(rst.ListResult);
    
    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category=rst.ListResult[0].dimension[i];
      chartObj.revenue =Number(rst.ListResult[0].metrics[0].metric[i].value) ;
      chartObj.ratio =Number(rst.ListResult[0].metrics[1].metric[i].value) ;
      list.push(chartObj);
    }
      console.log(list);
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart9Column
    };
    
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart9 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
  },
  chart10:function(data,location, rowMax, header,chartid){
    $('#chart10 .card-body').empty();
    const chart10Column= [{
      field: 'category',
      headerText: '通路',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location],list=[];
    
    
    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category=rst.ListResult[0].dimension[i];
      chartObj.revenue =Number(rst.ListResult[0].metrics[0].metric[i].value) ;
      chartObj.ratio =Number(rst.ListResult[0].metrics[1].metric[i].value) ;
      list.push(chartObj);
    }
      console.log(list);
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart10Column
    };
    
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart10 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
  },
  chart11:function(data,location, rowMax, header,chartid){
    $('#chart11 .card-body').empty();
    const chart11Column= [{
      field: 'category',
      headerText: '店鋪',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location],list=[];

    
    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category=rst.ListResult[0].dimension[i];
      chartObj.revenue =Number(rst.ListResult[0].metrics[0].metric[i].value) ;
      chartObj.ratio =Number(rst.ListResult[0].metrics[1].metric[i].value) ;
      list.push(chartObj);
    }
      console.log(list);
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart11Column
    };
    
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart11 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
  },
  chart12:function(data,location, rowMax, header,chartid){
    $('#chart12 .card-body').empty();
    const chart12Column= [
      {
        field: 'groupid',
        headerText: '群組編號',
        textAlign: 'left',
        width: 50,
        format: 'N'
      },{
      field: 'category',
      headerText: '產品群組',
      textAlign: 'left',
      width: 140,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 50,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '佔比',
      textAlign: 'left',
      width: 50,
      format: 'N'
    }];
    let rst = data[location],list=[];

    
    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category=rst.ListResult[0].dimension[i];
      chartObj.revenue =Number(rst.ListResult[0].metrics[0].metric[i].value) ;
      chartObj.ratio =Number(rst.ListResult[0].metrics[1].metric[i].value) ;
      chartObj.groupid =rst.ListResult[0].metrics[2].metric[i].value ;
      list.push(chartObj);
    }
      console.log(list);
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart12Column
    };
    
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart12 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
  },
  chart13:function(data,location, rowMax, header,chartid){
    $('#chart13 .card-body').empty();
    const chart13Column= [{
      field: 'category',
      headerText: '季節',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location],list=[];

    
    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category=rst.ListResult[0].dimension[i];
      chartObj.revenue =Number(rst.ListResult[0].metrics[0].metric[i].value) ;
      chartObj.ratio =Number(rst.ListResult[0].metrics[1].metric[i].value) ;
      list.push(chartObj);
    }
     
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart13Column
    };
    
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart13 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
  },
  chart14:function(data,location, rowMax, header,chartid){
    $('#chart14 .card-body').empty();
    const chart14Column= [{
      field: 'category',
      headerText: '季節-產品',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location],list=[];

    
    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category=rst.ListResult[0].dimension[i];
      chartObj.revenue =Number(rst.ListResult[0].metrics[0].metric[i].value) ;
      chartObj.ratio =Number(rst.ListResult[0].metrics[1].metric[i].value) ;
      list.push(chartObj);
    }
 
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart14Column
    };
    
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart14 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
  },
  chart15:function(data,location, rowMax, header,chartid){
    $('#chart15 .card-body').empty();
    const chart15Column= [{
      field: 'category',
      headerText: '性別',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 90,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '營收佔比',
      textAlign: 'left',
      width: 90,
      format: 'N'
    }];
    let rst = data[location],list=[];

    
    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category=rst.ListResult[0].dimension[i];
      chartObj.revenue =Number(rst.ListResult[0].metrics[0].metric[i].value) ;
      chartObj.ratio =Number(rst.ListResult[0].metrics[1].metric[i].value) ;
      list.push(chartObj);
    }
 
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart15Column
    };
    
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart15 .card-body');
    
    $("#"+chartid).children(".card-header").text(setting.cardHead);
  },
  chart16:function(data,location, rowMax, header,chartid){
    $('#chart16 .card-body').empty();
    const chart16Column= [
      {
        field: 'groupid',
        headerText: '性別',
        textAlign: 'left',
        width: 50,
        format: 'N'
      },{
      field: 'category',
      headerText: '產品群組',
      textAlign: 'left',
      width: 140,
      format: 'N'
    },
    {
      field: 'revenue',
      headerText: '營收',
      textAlign: 'left',
      width: 50,
      format: 'N'
    },
    {
      field: 'ratio',
      headerText: '佔比',
      textAlign: 'left',
      width: 50,
      format: 'N'
    }];
    let rst = data[location],list=[];

    
    for (let i = 0; i < rst.ListResult[0].dimension.length; i++) {
      chartObj = {};
      chartObj.category=rst.ListResult[0].dimension[i];
      chartObj.revenue =Number(rst.ListResult[0].metrics[0].metric[i].value) ;
      chartObj.ratio =Number(rst.ListResult[0].metrics[1].metric[i].value) ;
      chartObj.groupid =rst.ListResult[0].metrics[2].metric[i].value ;
      list.push(chartObj);
    }
     
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart16Column
    };
    
    let chart = chartSetting.gridChart(setting);
    
    chart.appendTo('#chart16 .card-body');
   
    $("#"+chartid).children(".card-header").text(setting.cardHead);
  }
};
function validateSetting(){
    let bearerToken =sessionStorage.getItem("token");
    let urlTo= AuthremoteUrl+"GetUserName";
    let Setting={
        async: true,
        type: "GET",
        url: urlTo,
        headers: {
            Authorization: "Bearer" +" "+ bearerToken
           
          }
    };
    return Setting;
}
function calcAmt(regionName){
  let tmpArr,stores,i=0,j=0,tmpList;
  var amtbyStore=0,nAmtbyRegion=0,mAmtbyRegion=0,sAmtbyRegion=0,eAmtbyRegion=0;
  switch (regionName){
    case "TAIWAN":
        for(i=0;i<salesbyStore.length;i++){
          stores= salesbyStore[i];
          switch(Object.keys(stores)[0]){
            case "基隆市":case "台北市":case "新北市": case "桃園市":case "新竹市":case "新竹縣":
                nAmtbyRegion =sumDataReduce(Object.values(Object.values(stores)[0]))+nAmtbyRegion;
                
              break;
            case "苗栗縣":case "台中市":case "彰化縣":case "南投縣":case "雲林縣":
                mAmtbyRegion =sumDataReduce(Object.values(Object.values(stores)[0]))+mAmtbyRegion;
                
              break;
            case "嘉義市":case "嘉義縣":case "台南市":case "高雄市":case "屏東縣":
                sAmtbyRegion =sumDataReduce(Object.values(Object.values(stores)[0]))+sAmtbyRegion;
               
                break;
            case "宜蘭縣":case "花蓮縣":case "台東縣":
                eAmtbyRegion =sumDataReduce(Object.values(Object.values(stores)[0]))+eAmtbyRegion;
             
                break;
          }
         
        }

        totalamtbyStore.push(nAmtbyRegion);
        totalamtbyStore.push(mAmtbyRegion);
        totalamtbyStore.push(sAmtbyRegion);
        totalamtbyStore.push(eAmtbyRegion);
        
    break;
    default:
        for(i=0;i<salesbyStore.length;i++){
          stores= salesbyStore[i];
          let cityName = Object.keys(stores);
       
          if(cityName[0]===regionName){
            amtbyStore=Object.values(stores);

            store_dim_data = Object.keys(amtbyStore[0]).sort(
              function(a, b){ //針對物件排序
                 
                  return amtbyStore[0][b] - amtbyStore[0][a];
              }
          );
     
         
           // store_dim_data = amtbyStore.map(item=>Object.keys(item));
            store_metrics_data=amtbyStore.map(item=>Object.values(item));
            
            store_metrics_data[0].sort(function(a, b) {
            return b-a;
          });
          //console.log(store_metrics_data);
            
            break;
          }

         
        }

  }
  
}  
