const L10n = ej.base.L10n;
const widgetitems = (document.getElementById('dataWidget'));
const chartitems = (document.getElementById('dataChart'));
DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();
L10n.load({
  'zh': {
    'daterangepicker': {
      placeholder: '選擇日期區間',
      startLabel: '起始日期',
      endLabel: '截止日期',
      applyText: '確認',
      cancelText: '取消',
      selectedDays: '選擇日期',
      days: '天',
      customRange: '自定義區間'

    }
  }
});
const tealeafReporturl = TealeafremoteUrl + 'TealeafReport';
const funnelLegendA = ["Google搜尋後點擊廣告","購買商品頁","確認購物清單","選擇付款方式","填寫運送方式","購物完成"];
const funnelLegendB = ["站內搜尋","購買商品頁","確認購物清單","選擇付款方式","填寫運送方式","購物完成"];
const funnelLegendC = ["UTM廣告","購買商品頁","確認購物清單","選擇付款方式","填寫運送方式","購物完成"];
const funnelLegendD = ["進入頁面","輸入帳號","輸入密碼","輸入密碼確認","輸入驗證碼","加入會員"];

$(document).ready(function () {
  //驗證
  //DramaCore.validateTokenCall(validateSetting());
  let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
  dp = DramaCore.createDatePicker(dt, dt);
  var tealeafKey = "tealeaf-" + sessionStorage.getItem(defaultStartDate) + "-" + sessionStorage.getItem(defaultEndDate) + "-" + sessionStorage.getItem(daySpan);
  if (sessionStorage.getItem(defaultStartDate) && sessionStorage.getItem(defaultEndDate) && sessionStorage.getItem(daySpan)) {
    dp.value = [sessionStorage.getItem(defaultStartDate), sessionStorage.getItem(defaultEndDate)];
    
    initInteractivePage.widget();
      initInteractivePage.chart();
    if (localStorage.getItem(tealeafKey)) {
      
      DramaCore.renderTealeafChart(tealeafKey);
    } else {
      console.log("A");
      let type = "post";
      para = {
        "MemberKey": memberKey,
        "StartDate": sessionStorage.getItem(defaultStartDate),
        "EndDate": sessionStorage.getItem(defaultEndDate),
        "LoginId": loginID,

      };
     // console.log("TEALEAFkey:"+tealeafKey);
      global_Tealeaf_Key = DramaCore.getRemoteData(tealeafReporturl, type, para, tealeafKey, fromTealeaf);
      let timeoutID = window.setInterval(function(){
        if(global_Tealeaf_Key){
          
          window.clearInterval(timeoutID);
          DramaCore.renderTealeafChart(global_Tealeaf_Key);
          global_Tealeaf_Key="";
        }
      },1000);
      
      

    }
  }

  dp.addEventListener("change", function () {
    let dayRange = this.getSelectedRange();
    if (dayRange.daySpan > 0) {
      let _sd = moment(this.startDate).format("YYYY-MM-DD");
      let _ed = moment(this.endDate).format("YYYY-MM-DD");
      sessionStorage.setItem(defaultStartDate, _sd);
      sessionStorage.setItem(defaultEndDate, _ed);
      sessionStorage.setItem('daySpan', dayRange.daySpan);
      let type = "post";
      let para = {
        "MemberKey": memberKey,
        "StartDate": _sd,
        "EndDate": _ed,
        "LoginId": loginID,
      };
      if (_sd && _ed) {
        let newtealeafKey = "tealeaf-" + _sd + "-" + _ed + "-" + dayRange.daySpan;


        if (localStorage.getItem(newtealeafKey)) {
          initInteractivePage.widget();
          initInteractivePage.chart();
          DramaCore.renderTealeafChart(newtealeafKey);
        } else {

          para = {
            "MemberKey": memberKey,
            "StartDate": _sd,
            "EndDate": _ed,
            "LoginId": loginID,

          };
          global_Tealeaf_Key = DramaCore.getRemoteData(tealeafReporturl, type, para, tealeafKey, fromTealeaf);
          let timeoutID = window.setInterval(function(){
            if(global_Tealeaf_Key){
              console.log(global_Tealeaf_Key);
              window.clearInterval(timeoutID);
              DramaCore.renderTealeafChart(global_Tealeaf_Key);
              global_Tealeaf_Key="";
            }
          },1000);
        }

      }
    }
  });
});


function randomGenerateColor(selector,index){
  let colorArray1=["#280E3B","#632A7E","#792A7E","#A13E97","#CC76B5","#D3B7D8"];
  let colorArray2=["#003853","#194b64","#3d82ab","#45b299","#43ccaa","#91d4c2"];
  let rtnColorCode;
  console.log(selector);
  //selector : fixed or random color array,sequence (1,2,3(random)): fix color array index 
  switch(selector){
    case 1:
      
      rtnColorCode= colorArray1[index];
      
      break;
    case 2:
      rtnColorCode= colorArray2[index];
      break;
    case 3:
      rtnColorCode= '#'+(Math.random()*0xFFFFFF<<0).toString(16);
      break;
  }
    return rtnColorCode;
}
var initInteractivePage={
  widget:function(){
      var  widgetMetaData=[
          {id:'w1',icons:'e-icons fas fa-desktop'},
          {id:'w2',icons:'e-icons fas fa-desktop'},
          {id:'w3',icons:'e-icons fas fa-desktop'},
          {id:'w4',icons:'e-icons fas fa-desktop'},
          {id:'w5',icons:'e-icons fas fa-mobile-alt'},
          {id:'w6',icons:'e-icons fas fa-mobile-alt'},
          {id:'w7',icons:'e-icons fas fa-mobile-alt'},
          {id:'w8',icons:'e-icons fas fa-mobile-alt'},
          {id:'w9',icons:'e-icons fas fa-tablet'},
          {id:'w10',icons:'e-icons fas fa-tablet'},
          {id:'w11',icons:'e-icons fas fa-tablet'},
          {id:'w12',icons:'e-icons fas fa-tablet'}
        
      ];
      var getWidgetString = ej.base.compile('<div class="col-sm-12 col-md-3 mb-1"> <div id=${id} class="card card_animation"> <div class="card-header widget-head "> <h5 class="widget-text align-middle"></h5> </div><div class="card-body"> <i class="${icons}"></i> <h5 class="widget-number align-middle"></h5> </div></div></div>');
      
      while (widgetitems.firstChild) {
        widgetitems.removeChild(widgetitems.firstChild);
      }
      widgetMetaData.forEach(data => {
          widgetitems.appendChild(getWidgetString(data)[0]);
      });
  },
  chart:function(){
      let  chartMetaData=[
          {id:'chart1A',size:'col-md-6'},
          {id:'chart1B',size:'col-md-6'},
          {id:'chart2A',size:'col-md-6'},
          {id:'chart2B',size:'col-md-6'},
          {id:'chart3A',size:'col-md-6'},
          {id:'chart3B',size:'col-md-6'},
          {id:'chart4',size:'col-md-4'},
          {id:'chart5',size:'col-md-4'},
          {id:'chart6',size:'col-md-4'},
          {id:'chart7',size:'col-md-4'},
          {id:'chart8',size:'col-md-4'},
          {id:'chart9',size:'col-md-4'}
    
        
      ];
      let getChartString = ej.base.compile('<div class="col-sm-12 ${size} mb-1 d-flex"> <div id=${id} class="card card_animation flex-fill"> <div class="card-header widget-head"> </div><div class="charts card-body chart-height"> </div> </div>  </div>');
      while (chartitems.firstChild) {
        chartitems.removeChild(chartitems.firstChild);
      }
      chartMetaData.forEach(data => {
          chartitems.appendChild(getChartString(data)[0]);
      });
  }

};
var TealeafChart = {
  chart1A: function (data, location, rowMax, header, chartid) {
    $("#"+chartid).children(".card-header").text(header);
    let link = "https://dailysee.net/caco_heatmap/image1A.html";
    var iframe = document.createElement('iframe');
    iframe.frameBorder=0;
    iframe.width="100%";
    iframe.height="100%";
    
    iframe.id="iframe1";
    iframe.setAttribute("src", link);
    document.getElementById(chartid).getElementsByClassName('card-body')[0].classList.add("iframeWrapper");
    document.getElementById(chartid).getElementsByClassName('card-body')[0].appendChild(iframe);
    
  },
  chart1B: function (data, location, rowMax, header, chartid) {
    $("#"+chartid).children(".card-header").text(header);
    let link = "https://dailysee.net/caco_heatmap/image1B.html";
    var iframe = document.createElement('iframe');
    iframe.frameBorder=0;
    iframe.width="100%";
    iframe.height="100%";
    
    iframe.id="iframe1";
    iframe.setAttribute("src", link);
    document.getElementById(chartid).getElementsByClassName('card-body')[0].classList.add("iframeWrapper");
    document.getElementById(chartid).getElementsByClassName('card-body')[0].appendChild(iframe);
    
  },
  chart2A: function (data, location, rowMax, header, chartid) {
    $("#"+chartid).children(".card-header").text(header);
    let link = "https://dailysee.net/caco_heatmap/image2A.html";
    var iframe = document.createElement('iframe');
    iframe.frameBorder=0;
    iframe.width="100%";
    iframe.height="100%";
    
    iframe.id="iframe1";
    iframe.setAttribute("src", link);
    document.getElementById(chartid).getElementsByClassName('card-body')[0].classList.add("iframeWrapper");
    document.getElementById(chartid).getElementsByClassName('card-body')[0].appendChild(iframe);
  },
  chart2B: function (data, location, rowMax, header, chartid) {
    $("#"+chartid).children(".card-header").text(header);
    let link = "https://dailysee.net/caco_heatmap/image2B.html";
    var iframe = document.createElement('iframe');
    iframe.frameBorder=0;
    iframe.width="100%";
    iframe.height="100%";
    
    iframe.id="iframe1";
    iframe.setAttribute("src", link);
    document.getElementById(chartid).getElementsByClassName('card-body')[0].classList.add("iframeWrapper");
    document.getElementById(chartid).getElementsByClassName('card-body')[0].appendChild(iframe);
  },
  chart3A: function (data, location, rowMax, header, chartid) {
    $("#"+chartid).children(".card-header").text(header);
    let link = "https://dailysee.net/caco_heatmap/image3A.html";
    var iframe = document.createElement('iframe');
    iframe.frameBorder=0;
    iframe.width="100%";
    iframe.height="100%";
    
    iframe.id="iframe1";
    iframe.setAttribute("src", link);
    document.getElementById(chartid).getElementsByClassName('card-body')[0].classList.add("iframeWrapper");
    document.getElementById(chartid).getElementsByClassName('card-body')[0].appendChild(iframe);
  },
  chart3B: function (data, location, rowMax, header, chartid) {
    $("#"+chartid).children(".card-header").text(header);
    let link = "https://dailysee.net/caco_heatmap/image3B.html";
    var iframe = document.createElement('iframe');
    iframe.frameBorder=0;
    iframe.width="100%";
    iframe.height="100%";
    
    iframe.id="iframe1";
    iframe.setAttribute("src", link);
    document.getElementById(chartid).getElementsByClassName('card-body')[0].classList.add("iframeWrapper");
    document.getElementById(chartid).getElementsByClassName('card-body')[0].appendChild(iframe);
  },
  chart4: function (data, location, rowMax, header, chartid) {
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
      for (var i = 0; i < rst.Data.length; i++){
        
        metric.push({
          name:rst.Data[i].Col1,
          value:Number(rst.Data[i].Col2)
        });		    			
      }		    


    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      legend: []
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.wordCloud(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart5: function (data, location, rowMax, header, chartid) {
    let rst = data[location];
    let dataAry=[0,0,0,0,0,0],metric=[];
    let colcorAry=[];
    console.log(rst);
    for(let i=0;i<rst.Data.length;i++){
      colcorAry.push(randomGenerateColor(1,i));
      dataAry[0]+=Number(rst.Data[i].Col1);
      dataAry[1]+=Number(rst.Data[i].Col2);
      dataAry[2]+=Number(rst.Data[i].Col3);
      dataAry[3]+=Number(rst.Data[i].Col4);
      dataAry[4]+=Number(rst.Data[i].Col5);
      dataAry[5]+=Number(rst.Data[i].Col6);
      
    }
    for(let i=0;i<funnelLegendA.length;i++){
      metric.push({
        name:funnelLegendA[i],
        value:dataAry[i]
      });		    			
    }
    let setting = {
      cardHead: header,
      titleText: "",
      color: colcorAry,
      metric: metric,
      legend: funnelLegendA
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.funnelChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      
      $("#"+chartid).children(".card-header").text(setting.cardHead);
      chart.setOption(option, true);
    }
  },
  chart6: function (data, location, rowMax, header, chartid) {
    let rst = data[location];
    let dataAry=[0,0,0,0,0,0],metric=[];
    let colcorAry=[];
    
    for(let i=0;i<rst.Data.length;i++){
      dataAry[0]+=Number(rst.Data[i].Col1);
      dataAry[1]+=Number(rst.Data[i].Col2);
      dataAry[2]+=Number(rst.Data[i].Col3);
      dataAry[3]+=Number(rst.Data[i].Col4);
      dataAry[4]+=Number(rst.Data[i].Col5);
      dataAry[5]+=Number(rst.Data[i].Col6);
      
    }
    for(let i=0;i<funnelLegendA.length;i++){
      colcorAry.push(randomGenerateColor(1,i));
      metric.push({
        name:funnelLegendB[i],
        value:dataAry[i]
      });		    			
    }
    let setting = {
      cardHead: header,
      titleText: "",
      color: colcorAry,
      metric: metric,
      legend: funnelLegendB
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.funnelChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      
      $("#"+chartid).children(".card-header").text(setting.cardHead);
      chart.setOption(option, true);
    }
    
  },
  chart7: function (data, location, rowMax, header, chartid) {
    let rst = data[location];
    let dataAry=[0,0,0,0,0,0],metric=[];
    let colcorAry=[];
    console.log(rst);
    for(let i=0;i<rst.Data.length;i++){
      dataAry[0]+=Number(rst.Data[i].Col1);
      dataAry[1]+=Number(rst.Data[i].Col2);
      dataAry[2]+=Number(rst.Data[i].Col3);
      dataAry[3]+=Number(rst.Data[i].Col4);
      dataAry[4]+=Number(rst.Data[i].Col5);
      dataAry[5]+=Number(rst.Data[i].Col6);
      
    }
    for(let i=0;i<funnelLegendC.length;i++){
      colcorAry.push(randomGenerateColor(2,i));
      metric.push({
        name:funnelLegendC[i],
        value:dataAry[i]
      });		    			
    }
    let setting = {
      cardHead: header,
      titleText: "",
      color: colcorAry,
      metric: metric,
      legend: funnelLegendB
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.funnelChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      
      $("#"+chartid).children(".card-header").text(setting.cardHead);
      chart.setOption(option, true);
    }
    
  },
  chart8: function (data, location, rowMax, header, chartid) {
    let rst = data[location];
    let dataAry=[0,0,0,0,0,0],metric=[];
    let colcorAry=[];
    
    for(let i=0;i<rst.Data.length;i++){
      dataAry[0]+=Number(rst.Data[i].Col1);
      dataAry[1]+=Number(rst.Data[i].Col2);
      dataAry[2]+=Number(rst.Data[i].Col3);
      dataAry[3]+=Number(rst.Data[i].Col4);
      dataAry[4]+=Number(rst.Data[i].Col5);
      dataAry[5]+=Number(rst.Data[i].Col6);
      
    }
    for(let i=0;i<funnelLegendD.length;i++){
      colcorAry.push(randomGenerateColor(2,i));
      metric.push({
        name:funnelLegendD[i],
        value:dataAry[i]
      });		    			
    }
    let setting = {
      cardHead: header,
      titleText: "",
      color: colcorAry,
      metric: metric,
      legend: funnelLegendD
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.funnelChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      
      $("#"+chartid).children(".card-header").text(setting.cardHead);
      chart.setOption(option, true);
    }
    
  },
  chart9: function (data, location, rowMax, header, chartid) {
    let rst = data[location],
      colorAry = [],
      dimension = [],
      metric = [];
      colorAry.push(randomGenerateColor(3,0));
    for(let i=0;i<rst.Data.length;i++){
      dimension.push(rst.Data[i].Col1);
      metric.push(rst.Data[i].Col2);
    }
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: colorAry,
      legend: []
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  }
};
