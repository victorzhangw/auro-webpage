var GA_Dimensions1=["date"];// dimension 空白表示為日期合計
var GA_Metrics1=["uniquePageviews","bounces","users","newUsers","sessions","pageviewsPerSession","avgSessionDuration","avgTimeOnPage"];
var GA_Dimensions2=["adGroup"];
var GA_Metrics2=["impressions","adClicks","adCost","CPM","CPC","costPerGoalConversion","costPerTransaction","RPC","ROAS"];
var GA_Dimensions3=["sourceMedium"];
var GA_Metrics3=["transactions","transactionRevenue","transactionsPerSession","revenuePerTransaction","goal1Completions","goal2Completions","goal3Completions","goal4Completions"];
var GA_Dimensions4=["campaign"];
var GA_Metrics4=["transactions","transactionRevenue","transactionsPerSession","revenuePerTransaction","goal1Completions","goal2Completions","goal3Completions","goal4Completions"];
var GA_Dimensions5=[];
var GA_Metrics5=[];
const defaultStartDate="defaultStartDate";
const defaultEndDate="defaultEndDate";
const loginID="loginID";
$(document).ready(function(){
    try{
        
        sessionStorage.clear();
        localStorage.clear();
    }
    catch(e){
        console.log(e);
    }
    let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
    let key = "default-" + dt + "-" + dt + "-" + "1";
    let url = GoogleremoteUrl + 'CustomGAReport';
    let type="POST";
    let para = {
        "MemberKey": memberKey,
        "StartDate": dt,
        "EndDate": dt,
        "LoginId": loginID,
        "ReqDimension1":GA_Dimensions1.join(),
        "ReqMetric1":GA_Metrics1.join(),
        "ReqDimension2":GA_Dimensions2.join(),
        "ReqMetric2":GA_Metrics2.join(),
        "ReqDimension3":GA_Dimensions3.join(),
        "ReqMetric3":GA_Metrics3.join(),
        "ReqDimension4":GA_Dimensions4.join(),
        "ReqMetric4":GA_Metrics4.join(),
        "ReqDimension5":GA_Dimensions5.join(),
        "ReqMetric5":GA_Metrics5.join(),
      };

    if (!localStorage.getItem(key)){
        //console.log(key);
        getRemoteData(url,type,para,key) ;
    }
});
$("#signinBtn").click(function(){
    let _username,_password,settings;
    let urlTo=AuthremoteUrl+"SignIn";
    _username= $("#username").val();
    _password= $("#password").val();
    
    if(_username && _password){
        $("#errlbl").removeClass("error-lbl");
        $("#errlbl").addClass("correct-lbl");
        let loginObj={
            Username:_username,
            Password:_password
        };
        settings={
            async: true,
            type: "POST",
            url: urlTo,
            data:loginObj
        };
        makeAjaxCall(settings);
        console.log(loginObj);
    }else{
        $("#errlbl").removeClass("correct-lbl");
        $("#errlbl").addClass("error-lbl");
    }
});
/*
$("#validateBtn").click(function(){
    let bearerToken =sessionStorage.getItem("token");
    console.log(bearerToken);
    let urlTo= AuthremoteUrl+"GetUserName";
    settings={
        async: true,
        type: "GET",
        url: urlTo,
        headers: {
            Authorization: "Bearer" +" "+ bearerToken
           
          }
    };
    validateAjaxCall(settings);
    
});*/
function makeAjaxCall(settings){
    var jqxhr = $.ajax(settings);
    //this section is executed when the server responds with no error 
    jqxhr.done(function(response){
        let _username =$("#username").val();
        $("#username").val("");
        $("#password").val("");
        sessionStorage.setItem('token', response);
        let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
        let key = "default-" + dt + "-" + dt + "-" + "1";
        let timeoutID = window.setInterval(function(){
       
         if (localStorage.getItem(key)){
             //console.log(key);
             window.clearInterval(timeoutID);
             sessionStorage.setItem(defaultStartDate, dt);
             sessionStorage.setItem(defaultEndDate, dt);
             sessionStorage.setItem(loginID,_username);
             sessionStorage.setItem('daySpan', "1");
             document.location.href=localurl+"default.html";
         }
        },1000);
    });
    //this section is executed when the server responds with error
    jqxhr.fail(function(err){
        $("#errlbl").removeClass("correct-lbl");
        $("#errlbl").addClass("error-lbl");
        $("#username").val("");
        $("#password").val("");
        

    });
    //this section is always executed
    jqxhr.always(function(){
        //here is how to access the response header
        console.log("getting header " + jqxhr.getResponseHeader('testHeader'));
    });
}
function validateAjaxCall(settings){
    var jqxhr = $.ajax(settings);
    jqxhr.done(function(response){
       console.log(response);
       let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
       let key = "default-" + dt + "-" + dt + "-" + "1";
       let timeoutID = window.setInterval(function(){
      
        if (localStorage.getItem(key)){
            console.log(key);
            window.clearInterval(timeoutID);
            document.location.href=localurl+"default.html";
        }
       },1000);
    });
    //this section is executed when the server responds with error
    jqxhr.fail(function(err){
     
        

    });
    //this section is always executed
    jqxhr.always(function(){
        if(jqxhr.status=="401"){
            document.location.href = localurl+loginUrl;
           
        }
    });
}

function getRemoteData(url, type, para,key) {
    let settings = {
        "async": true,
        "crossDomain": true,
        "url": url,
        "method": type,
        "headers": {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        "data": para
      };
  
      $.ajax(settings).done(function (response) {
        let obj = JSON.parse(response);
        if (Object.keys(obj).length > 0) {
          localStorage.setItem(key, response);
            console.log(obj);
        }
      });

  
}