ej.base.enableRipple(true);
navData = [
  {
    id: 'nav005',
    iconText: '今日',
    navUrl: 'default.html',
    iconType: 'fas fa-plane-departure'
  },
  {
    id: 'nav010',
    iconText: '流量',
    navUrl: 'flow.html',
    iconType: 'fas fa-chart-line'
  },
  {
    id: 'nav020',
    iconText: '體驗',
    navUrl: 'interactive.html',
    iconType: 'far fa-hand-pointer'
  },
  {
    id: 'nav030',
    iconText: '交易',
    navUrl: 'Ecommerce.html',
    iconType: 'fas fa-dumpster-fire '
  },
  {
    id: 'nav040',
    iconText: '標籤',
    navUrl: 'label.html',
    iconType: 'fas fa-tags'
  },

];
const CustomGARequest = {
  GA_Dimensions1: [],
  GA_Metrics1: ["uniquePageviews", "bounceRate", "users", "newUsers", "sessions", "pageviewsPerSession", "avgSessionDuration", "avgTimeOnPage"],
  GA_Dimensions2: ["sourceMedium"],
  GA_Metrics2: ["uniquePageviews", "bounceRate", "users", "newUsers", "sessions", "pageviewsPerSession", "avgSessionDuration", "avgTimeOnPage"],
  GA_Dimensions3: ["userGender", "userAgeBracket"],
  GA_Metrics3: ["uniquePageviews", "bounceRate", "users", "newUsers", "sessions", "pageviewsPerSession", "avgSessionDuration", "avgTimeOnPage"],
  GA_Dimensions4: ["deviceCategory"],
  GA_Metrics4: ["uniquePageviews", "bounceRate", "users", "newUsers", "sessions", "pageviewsPerSession", "avgSessionDuration", "avgTimeOnPage"],
  GA_Dimensions5: [],
  GA_Metrics5: [],

};
const fromEC = "EC"; //指定由何種頁面帶入 key
const fromGA = "GA"; //指定由何種頁面帶入 key
const fromTealeaf = "Tealeaf"; //指定由何種頁面帶入 key
const fromLabel = "Label"; //指定由何種頁面帶入 key
/*
$(document).on({
  ajaxStart: function () {
    $("body").addClass("loading");
  },
  ajaxStop: function () {
    $("body").removeClass("loading");
  }
});*/

const sideNavitems = (document.getElementById('sideNav'));
const defaultStartDate = "defaultStartDate";
const defaultEndDate = "defaultEndDate";
const daySpan = "daySpan";
const loginID = "loginID";

const WidgetCounts = 8; // widget 總數
const InteractiveWidgetCounts = 12; // widget 總數
const InteractiveWidgetHead1 = "合計訂單金額"; // widget 總數
const InteractiveWidgetHead2 = "訂單數量"; // widget 總數
const InteractiveWidgetHead3 = "合計商品件數"; // widget 總數
const InteractiveWidgetHead4 = "平均訂單商品件數"; // widget 總數
const ECWidgetCounts = 4;
const defaultChartCounts = 6; // chart 總數
var global_custom_Key, global_EC_Key, global_Tealeaf_Key, global_Label_key;


var chartSetting = {
  singleBarChart: function (p) {
    let option = {
      title: {
        text: p.titleText
      },
      tooltip: {},
      legend: {
        data: p.legend
      },
      grid: {
        top: 20,

      },
      xAxis: {
        type: 'category',
        data: p.category,

        axisLabel: {
          interval: 0,
          rotate: 35,
          fontSize: 10,
          formatter: function (param) {

            var res = param.split("/");
            return res[1];
          }
        }


      },
      yAxis: {
        type: 'value',
        splitLine: { //控制軸線
          show: false,
        },
        axisLabel: {
          formatter: function (value, index) {
            // 將數值轉換為 K,M
            function intlFormat(num) {
              return new Intl.NumberFormat().format(Math.round(num * 10) / 10);
            }

            if (value >= 1000000)
              return intlFormat(value / 1000000) + 'M';
            if (value >= 1000)
              return intlFormat(value / 1000) + 'k';
            return intlFormat(value);

          }
        }

      },
      series: [{

        type: 'bar',
        barWidth: '50%',
        data: p.value,
        itemStyle: {
          normal: {
            barBorderRadius: 4,
            color: p.color[0]
          }
        },
        label: {
          normal: {
            show: true,
            position: 'inside',
            fontSize: 11,
          }
        },
      }]
    };
    return option;
  },
  doubleBarChart: function (p) {
    let option = {
      title: {

      },

      legend: {
        data: p.legend
      },
      grid: {
        left: '0',
        right: '1%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: {
        type: 'value',
        boundaryGap: [0, 0.1]
      },
      yAxis: {
        type: 'category',
        data: p.category
      },
      series: [{
          name: p.legend[0],
          type: 'bar',
          data: p.value1,
          itemStyle: {
            color: '#FB7507'

          }
        },
        {
          name: p.legend[1],
          type: 'bar',
          data: p.value2,
          itemStyle: {
            color: '#4A90E2'

          }
        }
      ]
    };
    return option;
  },
  doubleBarChartVertical: function (p) {

    let option = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      legend: {
        data: p.legend,

      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: {
        type: 'category',
        data: p.category,
        axisLabel: {
          fontSize: 10,
          color: '#333',
          margin: 4,
          interval: 0,
          formatter: function (val) {
            return val.split("").join("\n");
          }
        }
      },
      yAxis: {
        type: 'value',
        boundaryGap: [0, 0.01],
        axisLabel: {
          formatter: function (value, index) {
            // 將數值轉換為 K,M
            function intlFormat(num) {
              return new Intl.NumberFormat().format(Math.round(num * 10) / 10);
            }

            if (value >= 1000000)
              return intlFormat(value / 1000000) + 'M';
            if (value >= 1000)
              return intlFormat(value / 1000) + 'k';
            return intlFormat(value);

          }
        }
      },
      series: [

        {
          name: p.legend[0],
          type: 'bar',
          barMaxWidth: '25%',
          color: p.color[0],
          data: p.value1
        },
        {
          name: p.legend[1],
          type: 'bar',
          barMaxWidth: '25%',
          color: p.color[1],
          data: p.value2
        }
      ]
    };
    return option;
  },
  fourQuadrant: function (p) {

    let option = {
      tooltip: {
        trigger: 'item',
        axisPointer: {
          show: true,
          type: 'cross',
          lineStyle: {
            type: 'dashed',
            width: 1
          },
        },
        position: function (pos, params, dom, rect, size) {
          var obj = {
            top: 60
          };
          obj[['left', 'right'][+(pos[0] < size.viewSize[0] / 2)]] = 5;
          return obj;
        },
        formatter: function (obj) {
          if (obj.componentType == "series") {
            return '<div style="border-bottom: 1px solid rgba(255,255,255,.3); font-size: 18px;padding-bottom: 7px;margin-bottom: 7px">' +
              obj.name +
              '</div>' +
              '<span>' +
              p.legend[1] +
              '</span>' +
              ' : ' + obj.value[0] + p.formatterString[1] +
              '<br/>' +
              '<span>' +
              p.legend[0] +
              '</span>' +
              ' : ' + obj.value[1] + p.formatterString[0];
          }
        }
      },
      xAxis: {
        name: p.legend[1],
        nameLocation: 'middle',
        nameGap: 25,
        type: 'value',
        scale: true,
        axisLabel: {
          formatter: '{value} ',
          fontSize: 10

        },
        splitLine: {
          show: false
        },
        axisLine: {
          lineStyle: {
            color: p.color[0]
          }
        }
      },
      yAxis: {
        name: p.legend[0],

        type: 'value',
        scale: true,
        axisLabel: {
          formatter: '{value}' + p.formatterString[0],
          fontSize: 10
        },
        splitLine: {
          show: false
        },
        axisLine: {
          lineStyle: {
            color: p.color[0]
          }
        }
      },

      series: [{
        type: 'scatter',
        data: p.seriesdata,
        symbolSize: 12,

        markLine: {
          label: {
            normal: {
              formatter: function (params) {
                if (params.dataIndex == 1) {
                  return params.value + p.formatterString[0];
                } else if (params.dataIndex == 0) {
                  return params.value + p.formatterString[1];
                }
                return params.value;
              }
            }
          },
          lineStyle: {
            normal: {
              color: p.color[1],
              type: 'solid',
              width: 1,
            },
            emphasis: {
              color: p.color[2]
            }
          },
          data: [{
            xAxis: p.avg.xAvgLine,
            name: p.legend[1] + '平均線',
            itemStyle: {
              normal: {
                color: p.color[3],
              }
            }
          }, {
            yAxis: p.avg.yAvgLine,
            name: p.legend[0] + '平均線',
            itemStyle: {
              normal: {
                color: p.color[3],
              }
            }
          }]
        },
        color: p.color[4]

      }]
    };
    return option;
  },
  gridChart: function (p) {

    ej.grids.Grid.Inject(ej.grids.Page, ej.grids.Sort, ej.grids.Filter);
    let grid = new ej.grids.Grid({
      dataSource: p.value,
      columns: p.gridColumn,
      rowHeight: 30,
      allowPaging: true,
      allowExcelExport: true,
      toolbar: [{ text: '匯出', tooltipText: 'Download excel',prefixIcon: "e-excelexport" ,id: 'ExcelExport' }],
      pageSettings: {
        pageSize: 5,
        pageCount:5
      },
      allowSorting: true,
      queryCellInfo: p.customiseCell
    });
    grid.toolbarClick = function(args){
      if (args.item.id === 'ExcelExport') {
                
       
        
        let excelExportProperties = {
            fileName:p.cardHead+"_資料時間_"+sessionStorage.getItem(defaultStartDate)+".xlsx",
            //dataSource:list
        };
        // grid1.excelExport(getExcelExportProperties());
        grid.excelExport(excelExportProperties);
        
        
     }
  }
    return grid;
  },
  
  doubleCustomSymbolBarChart: function (p) {
    let bodyMaxA = (parseInt(p.value[0].users) + parseInt(p.value[1].users)); //指定圖形界限值
    let bodyMaxB = (parseInt(p.value[0].newUsers) + parseInt(p.value[1].newUsers)); //指定圖形界限值
    let labelSetting = {
      normal: {
        show: true,
        position: 'bottom',
        offset: [0, 10],
        formatter: function (param) {

          return (param.data.value2) + '人';
        },
        textStyle: {
          fontSize: 18,
          fontFamily: 'Microsoft JhengHei, PingFang TC',
          color: '#686868'
        }
      }
    };

    let markLineSetting = { //设置标线
      symbol: 'none',
      lineStyle: {
        normal: {
          opacity: 0.3
        }
      },
      data: [{
        type: 'max',
        label: {
          normal: {
            formatter: 'max: {c}'
          }
        }
      }, {
        type: 'min',
        label: {
          normal: {
            formatter: 'min: {c}'
          }
        }
      }]
    };
    option = {
      tooltip: {
        show: false, //鼠标放上去显示悬浮数据
      },
      legend: {
        data: ['訪客', '新訪客'],
        selectedMode: 'single',
        itemWidth: 10, //图例的宽度
        itemHeight: 10, //图例的高度
        itemGap: 30,
        orient: 'horizontal',
        left: 'center',
        top: '10px',
        icon: 'circle',
        // selectedMode: false, //取消图例上的点击事件
        textStyle: {
          color: '#808492'
        },
      },
      grid: {
        // left: '20%',
        // right: '20%',
        top: '20%',
        bottom: '20%',
        containLabel: true
      },
      xAxis: {
        data: ['a', 'x', 'b'],
        axisTick: {
          show: false
        },
        axisLine: {
          show: false
        },
        axisLabel: {
          show: false
        }
      },
      yAxis: {
        max: 100,

        splitLine: {
          show: false
        },
        axisTick: {
          // 刻度线
          show: false
        },
        axisLine: {
          // 轴线
          show: false
        },
        axisLabel: {
          // 轴坐标文字
          show: false
        }
      },
      series: [{
          name: '訪客',
          type: 'pictorialBar',
          symbolClip: true,
          symbolBoundingData: 100,
          label: labelSetting,
          data: [{
              value: p.value[0].users / bodyMaxA * 100,
              value2: p.value[0].users,
              symbol: p.symbol[0],

              itemStyle: {
                normal: {
                  color: 'rgba(255,130,130)' //单独控制颜色
                }
              },
            },
            {

            },
            {
              value: p.value[1].users / bodyMaxA * 100,
              value2: p.value[1].users,
              symbol: p.symbol[1],

              itemStyle: {
                normal: {
                  color: 'rgba(105,204,230)' //单独控制颜色
                }
              },
            }
          ],
          // markLine: markLineSetting,
          z: 10
        },
        {
          name: '新訪客',
          type: 'pictorialBar',
          symbolClip: true,
          symbolBoundingData: 100,
          label: labelSetting,
          data: [{
              value: p.value[0].newUsers / bodyMaxB * 100,
              value2: p.value[0].newUsers,
              symbol: p.symbol[0],

            },
            {},
            {
              value: p.value[1].newUsers / bodyMaxB * 100,
              value2: p.value[1].newUsers,
              symbol: p.symbol[1]
            }
          ],
          // markLine: markLineSetting,
          z: 10
        },
        {
          // 设置背景底色，不同的情况用这个
          name: 'full',
          type: 'pictorialBar', //异型柱状图 图片、SVG PathData
          symbolBoundingData: 100,
          animationDuration: 0,
          itemStyle: {
            normal: {
              color: '#ccc' //设置全部颜色，统一设置
            }
          },
          z: 10,
          data: [{
              itemStyle: {
                normal: {
                  color: 'rgba(255,130,130,0.40)' //单独控制颜色
                }
              },
              value: 100,
              symbol: p.symbol[0],

            },
            {
              // 设置中间冒号
              itemStyle: {
                normal: {
                  color: 'rgba(71, 211, 52,0.4)' //单独控制颜色
                }
              },
              value: 100,
              symbol: p.symbol[2],
              symbolSize: [8, '18%'],
              symbolOffset: [0, '-200%']
            },
            {
              itemStyle: {
                normal: {
                  color: 'rgba(105,204,230,0.40)' //单独控制颜色
                }
              },
              value: 100,
              symbol: p.symbol[1],

            }
          ]
        }
      ]
    };
    return option;
  },
  doubleHorizontalSplitBarChart: function (p) {
    option = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      legend: {
        data: p.lengend,

      },

      color: ['rgba(255,130,130)', 'rgba(105,204,230)', ],
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: [{
        type: 'value',
        show: false
      }],
      yAxis: [{
        type: 'category',
        axisTick: {
          show: false
        },
        data: p.value1.age.reverse()
      }],

      series: [

        {
          name: '女性',
          type: 'bar',
          stack: '合計',
          label: {
            normal: {
              show: true,
              position: 'right',
              fontSize: 11
            }
          },
          itemStyle: {
            normal: {
              barBorderRadius: 4,

            }
          },
          data: p.value1.newUsers.reverse()
        },
        {
          name: '男性',
          type: 'bar',
          stack: '合計',
          itemStyle: {
            normal: {
              barBorderRadius: 4,

            }
          },
          label: {
            normal: {
              show: true,
              position: 'left',
              fontSize: 11,
              formatter: function (params) {
                return -params.value;
              }
            },

          },
          data: p.value2.newUsers.reverse()
        }
      ]
    };
    return option;
  },
  tripleCirclePieChart: function (p) {
    let option = {
      //backgroundColor: "#20263f",
      series: [{
        name: 'circle1',
        type: 'pie',
        clockWise: true,
        radius: [33, 45],
        itemStyle: p.dataStyle,
        hoverAnimation: true,
        center: ['16%', '50%'],
        data: [{
          value: p.value1[1],
          label: {
            normal: {
              rich: {
                a: {
                  color: "#000",
                  align: 'center',
                  fontSize: 12,
                  fontWeight: "bold"
                },
                b: {
                  color: "#000",
                  align: 'center',
                  fontSize: 12
                }
              },
              formatter: function (params) {

                return "{b|" + p.value1[3] + "}\n\n" + "{a|" + p.value1[0] + p.unit + "}";
              },
              position: 'center',
              show: true,
              textStyle: {
                fontSize: '14',
                fontWeight: 'normal',
                color: '#fff'
              }
            }
          },
          itemStyle: {
            normal: {
              color: p.color2[0],
              shadowColor: p.color2[0],
              shadowBlur: 0
            }
          }
        }, {
          value: p.value1[2],
          name: 'invisible',
          itemStyle: {
            normal: {
              color: p.color1[0]
            },
            emphasis: {
              color: p.color1[0]
            }
          }
        }]
      }, {
        name: 'circle2',
        type: 'pie',
        clockWise: true,
        radius: [33, 45],
        itemStyle: p.dataStyle,
        hoverAnimation: false,
        center: ['50%', '50%'],
        data: [{
          value: p.value2[2],
          label: {
            normal: {
              rich: {
                a: {
                  color: "#000",
                  align: 'center',
                  fontSize: 12,
                  fontWeight: "bold"
                },
                b: {
                  color: "#000",
                  align: 'center',
                  fontSize: 12
                }
              },
              formatter: function (params) {
                return "{b|" + p.value2[3] + "}\n\n" + "{a|" + p.value2[0] + p.unit + "}";
              },
              position: 'center',
              show: true,
              textStyle: {
                fontSize: '14',
                fontWeight: 'normal',
                color: '#fff'
              }
            }
          },
          itemStyle: {
            normal: {
              color: p.color1[1],
              shadowColor: p.color1[1],
              shadowBlur: 0
            }
          }
        }, {
          value: p.value2[2],
          name: 'invisible',
          itemStyle: {
            normal: {
              color: p.color2[1]
            },
            emphasis: {
              color: p.color2[1]
            }
          }
        }]
      }, {
        name: 'circle3',
        type: 'pie',
        clockWise: false,
        radius: [33, 45],
        itemStyle: p.dataStyle,
        hoverAnimation: false,
        center: ['84%', '50%'],
        data: [{
          value: p.value3[2],
          label: {
            normal: {
              rich: {
                a: {
                  color: "#000",
                  align: 'center',
                  fontSize: 12,
                  fontWeight: "bold"
                },
                b: {
                  color: "#000",
                  align: 'center',
                  fontSize: 12
                }
              },
              formatter: function (params) {
                return "{b|" + p.value3[3] + "}\n\n" + "{a|" + p.value3[0] + p.unit + "}";
              },
              position: 'center',
              show: true,
              textStyle: {
                fontSize: '14',
                fontWeight: 'normal',
                color: '#fff'
              }
            }
          },
          itemStyle: {
            normal: {
              color: p.color1[2],
              shadowColor: p.color1[2],
              shadowBlur: 0
            }
          }
        }, {
          value: p.value1[2],
          name: 'invisible',
          itemStyle: {
            normal: {
              color: p.color2[2]
            },
            emphasis: {
              color: p.color2[2]
            }
          }
        }]
      }]
    };
    return option;

  },
  singleBarChartwithDataTable: function (p) {
    let option = {
      title: {
        text: p.titleText
      },
      tooltip: {},
      legend: {
        data: p.legend
      },
      toolbox: {
        show: true,
        feature: {
          dataView: {
            show: true,
            buttonColor: p.color[0],
            readOnly: true
          }
        },
        left: '80%',

        optionToContent: function (opt) {

          let axisData = opt.xAxis[0].data;
          let series = opt.series;
          let table = '<table id="charttable" class="table-bordered table-striped" style="width:95%;text-align:center">';
          table = table + '<tbody><tr>' + '<td>&nbsp;</td>' + '<td> &nbsp;</td>' + '</tr>';
          for (var i = 0, l = axisData.length; i < l; i++) {
            table += '<tr >' + '<td style="text-align:left">&nbsp;' + axisData[i] + '</td>' + '<td>' + series[0].data[i] + '</td>' + '</tr>';
          }
          table += '</tbody>';
          return table;
        }
      },
      grid: {
        top: 20,

      },
      xAxis: {
        type: 'category',
        data: p.category,

        axisLabel: {
          interval: 0,
          rotate: 35,
          fontSize: 10,
          formatter: function (param) {
            var res = param.split("/");
            return res[1];
          }
        }


      },
      yAxis: {
        type: 'value',
        splitLine: { //控制軸線
          show: false,
        }

      },
      series: [{

        type: 'bar',
        barWidth: '50%',
        data: p.value,
        itemStyle: {
          normal: {
            barBorderRadius: 4,
            color: p.color[0]
          }
        },
        label: {
          normal: {
            show: true,
            position: 'inside',
            fontSize: 11,
          }
        },
      }]
    };
    return option;
  },
  singleBarChartwithAvgLine: function (p) {
    option = {
      xAxis: {
        type: 'category',
        data: p.category,
        show: true,
        axisTick: {
          show: false
        },
        axisLabel: {
          fontSize: 9,
          color: '#333',
          rotate: 25,
        },
      },
      yAxis: {

        type: 'value'
      },
      tooltip: {
        trigger: 'axis'
      },
      series: [{
        data: p.value,
        barMaxWidth: '100%',
        itemStyle: {
          normal: {
            color: function (params) {
              var num = p.color.length;
              return p.color[params.dataIndex % num];
            }
          }
        },
        markPoint: {
          data: [{
              type: 'max',
              name: '最大值'
            },
            {
              type: 'min',
              name: '最小值'
            }
          ]
        },
        markLine: {
          data: [{
            type: 'average',
            name: '平均值'
          }]
        },
        type: 'bar'
      }]
    };
    return option;
  },
  twMapwithPieChart: function (p) {
    option = {
      /*
      title: {
          //text: '各城市/店鋪銷售量',
          //subtext: 'Ecommerce',
         // sublink: '',
          left: 'right'
      },*/
      tooltip: {
        trigger: 'item',
        showDelay: 0,
        transitionDuration: 0,
        formatter: function (params) {
          var value = (params.value + '').split('.');
          value = value[0].replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g, '$1,');
          return params.seriesName + '<br/>' + params.name + ': ' + value;
        }
      },
      visualMap: {

        min: 0,
        max: 29000,
        itemWidth: 14,
        inRange: {
          color: p.color
        },
        top: 'bottom',
        left: 'left',
        text: ['High', 'Low'], // 文本，默认为数值文本
        calculable: true
      },
      toolbox: {
        show: false,
        //orient: 'vertical',
        left: 'left',
        top: 'top',
        feature: {
          dataView: {
            readOnly: false
          },
          restore: {},
          saveAsImage: {}
        }
      },

      series: [{
        name: 'sales',
        type: 'map3D',
        roam: false,
        map: 'TAIWAN',
        aspectScale: 0.8,
        bottom: 40,
        left: 0,

        itemStyle: {
          emphasis: {
            label: {
              show: true
            }
          }
        },
        light: {
          main: {
            intensity: 1,
            shadow: true,
            alpha: 150,
            beta: 70
          }
        },
        ambient: {
          intensity: 0
        },
        postEffect: { //為畫面新增高光，景深，環境光遮蔽（SSAO），調色等效果
          enable: true, //是否開啟
          SSAO: { //環境光遮蔽
            radius: 1, //環境光遮蔽的取樣半徑。半徑越大效果越自然
            intensity: 1, //環境光遮蔽的強度
            enable: true
          }
        },
        temporalSuperSampling: { //分幀超取樣。在開啟 postEffect 後，WebGL 預設的 MSAA 會無法使用,分幀超取樣用來解決鋸齒的問題
          enable: true
        },
        viewControl: { //用於滑鼠的旋轉，縮放等視角控制
          distance: 208, //預設視角距離主體的距離
          zoomSensitivity: 0,
          rotateMouseButton: 'right', //旋轉操作使用的滑鼠按鍵
          alpha: 31 // 讓canvas在x軸有一定的傾斜角度
        },
        data: p.value1
      }]
    };
    return option;
  },
  singleCirclePiewithSymbol: function (p) {
    option = {
      backgroundColor: '#fff',
      tooltip: {
        trigger: 'item',
        formatter: '<span style="color:yellow">{b}</span></br> 金額：{c} </br>佔比： {d}%  '
      },
      graphic: {
        elements: [{
          type: 'image',
          style: {
            image: p.symbol,
            width: 160,
            height: 90,
          },
          left: 'center',
          top: '50%'
        }]
      },
      legend: {
        orient: 'vertical',
        x: '1%',
        y: 'bottom',
        itemWidth: 15,
        itemHeight: 15,
        align: 'right',
        textStyle: {
          fontSize: 14,
          color: '#000'
        },
        data: p.lengend
      },
      series: [{
        type: 'pie',
        radius: ['39%', '65%'],
        center: ['50%', '60%'],
        color: p.color2,
        data: p.value2,
        labelLine: {
          normal: {
            show: false,
            length: 20,
            length2: 20,
            lineStyle: {
              color: '#12EABE',
              width: 2
            }
          }
        },
        label: {
          normal: {
            show: false,

            rich: {
              b: {
                fontSize: 20,
                color: '#12EABE',
                align: 'left',
                padding: 4
              },
              hr: {
                borderColor: '#12EABE',
                width: '100%',
                borderWidth: 2,
                height: 0
              },
              d: {
                fontSize: 20,
                color: '#fff',
                align: 'left',
                padding: 4
              },
              c: {
                fontSize: 20,
                color: '#fff',
                align: 'left',
                padding: 4
              }
            }
          }
        }
      }]
    };
    return option;
  },
  wordCloud: function (p) {

    option = {
      tooltip: {},
      series: [{
        type: 'wordCloud',
        gridSize: 2,
        sizeRange: [14, 60],
        rotationRange: [-45, 90],
        shape: 'diamond', // circle star heart .... see ref
        width: '100%',
        height: '100%',
        textStyle: {
          normal: {
            color: function () {
              return 'rgb(' + [
                Math.round(Math.random() * 160),
                Math.round(Math.random() * 160),
                Math.round(Math.random() * 160)
              ].join(',') + ')';
            }
          },
          emphasis: {
            shadowBlur: 10,
            shadowColor: '#333'
          }
        },
        data: p.value
      }]
    };
    return option;
  },
  funnelChart: function (p) {
    console.log(p);
    option = {
      color: p.color,
      tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} 人"
      },

      legend: {
        data: p.legend
      },
      series: [{
          name: ' ',
          type: 'funnel',
          left: '10%',
          height: '80%',
          width: '75%',
          label: {
            normal: {
              formatter: '{b}'
            },
            emphasis: {
              position: 'inside',
              formatter: ''
            }
          },
          labelLine: {
            normal: {
              show: false
            }
          },
          itemStyle: {
            normal: {
              opacity: 0.6
            }
          },
          data: p.metric
        },
        {
          name: '',
          type: 'funnel',
          height: '80%',
          left: '10%',
          width: '75%',
          maxSize: '80%',
          label: {
            normal: {
              position: 'left',
              formatter: '{c}人',
              textStyle: {
                color: '#ff0000',

              }
            },
            emphasis: {
              position: 'inside',
              formatter: ''
            }
          },
          itemStyle: {
            normal: {
              opacity: 0.5,
              borderColor: '#fff',
              borderWidth: 2
            }
          },
          data: p.metric
        }
      ]
    };
    return option;
  }
};
var DramaCore = {
  sideBar: function () {
    let getNavstring = ej.base.compile('<li id=${id}><a href=${navUrl} class="text-center "><i class="${iconType} sidebarIcon"></i><span class="sidebarText ">${iconText}</span> </a> </li>');
    navData.forEach(data => {
      sideNavitems.appendChild(getNavstring(data)[0]);
    });
  },
  loadCultureFiles: function (name) {
    var files = ['ca-gregorian.json', 'numbers.json', 'timeZoneNames.json'];
    var loader = ej.base.loadCldr;
    var loadCulture = function (prop) {
      var val, ajax;
      ajax = new ej.base.Ajax(localurl + 'assests/js/' + files[prop], 'GET', false);
      ajax.onSuccess = function (value) {
        val = value;
      };
      ajax.send();
      loader(JSON.parse(val));
    };
    for (var prop = 0; prop < files.length; prop++) {
      loadCulture(prop);
    }
  },
  // 建立日期區間選擇器
  createDatePicker: function (startDate, endDate) {
    L10n.load({
      'zh': {
        'daterangepicker': {
          placeholder: "輸入日期區間",
          text: startDate + " - " + endDate,
          startLabel: '起始日期',
          endLabel: '截止日期',
          applyText: '確認',
          cancelText: '取消',
          selectedDays: '選擇日期',
          days: '天',
          customRange: '自定義區間'

        }
      }
    });
    var daterangepicker = new ej.calendars.DateRangePicker({
      locale: 'zh',
      presets: [{
          label: '昨日',
          start: startDate,
          end: endDate
        },
        {
          label: '本週',
          start: new Date(new Date(new Date().setDate(new Date().getDate() - (new Date().getDay() + 7) % 7)).toDateString()),
          end: new Date(new Date(new Date().setDate(new Date(new Date().setDate((new Date().getDate() - (new Date().getDay() + 7) % 7)) + 6).getDate() + 6)).toDateString())
        },

        {
          label: '本月',
          start: new Date(new Date(new Date().setDate(1)).toDateString()),
          end: new Date(new Date().toDateString())
        }


      ],
      format: "yyyy-MM-dd", // custom format 

    });
    daterangepicker.appendTo('#datepicker');
    return daterangepicker;
  },
  // 檢驗 Token
  validateTokenCall: function (settings) {
    let jqxhr = $.ajax(settings);
    jqxhr.done(function (response) {


    });
    //this section is executed when the server responds with error
    jqxhr.fail(function (err) {



    });
    //this section is always executed
    jqxhr.always(function () {
      if (jqxhr.status == "401") {
        document.location.href = localurl + loginUrl;

      }
    });
  },
  // 取得交易數據
  getRemoteData: function (url, type, para, key, from = "GA", async = true) {

    if (!localStorage.getItem(key)) {
      let settings = {
        "async": async,
        "crossDomain": true,
        "url": url,
        "method": type,
        "headers": {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        "data": para
      };
      $("body").addClass("loading");
      var ele = document.getElementById('footer');

        if (ele) {
            ele.style.visibility = "hidden";

        }
      $.ajax(settings).done(function (response) {
        $("body").removeClass("loading");
        let obj = JSON.parse(response);
        if (Object.keys(obj).length > 0) {
          localStorage.setItem(key, response);
          DramaCore.returnValue(key, from);
          if (ele) {
            ele.style.visibility = "visible";

        }
          
        }
      });

    }


  },
  getSilenceRemoteData: function (url, type, para, key, from = "GA", async = true) {

    if (!localStorage.getItem(key)) {
      let settings = {
        "async": async,
        "crossDomain": true,
        "url": url,
        "method": type,
        "headers": {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        "data": para
      };

      $.ajax(settings).done(function (response) {

        let obj = JSON.parse(response);
        if (Object.keys(obj).length > 0) {
          localStorage.setItem(key, response);
          //global_EC_Key = key;
          //DramaCore.returnValue(_result);
        }
      });
      DramaCore.returnValue(key, from);
    }


  },
  getGridData: function (url, type, para, key, from = "GA", async = true) {


    let settings = {
      "async": !1,
      "crossDomain": true,
      "url": url,
      "method": type,
      "headers": {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      "data": para
    };

    $.ajax(settings).done(function (response) {

     // let obj = JSON.parse(response);
      let value=  DramaCore.returnValue(response);
      console.log("value:");
      console.log(value);
      return value;
    });


  },
  returnValue: function (value, from) {
    switch (from) {
      case "EC":
        global_EC_Key = value;
        break;
      case "GA":
        global_custom_Key = value;
        break;
      case "Tealeaf":
        global_Tealeaf_Key = value;
        break;
      case "Label":
       
        global_Label_key = value;
        break;
      default:
          
        
        
        break;
    }
    return value;
  },
  // 繪製畫面
  renderDefaultGAChart: function (key) {
    try {

      let obj = JSON.parse(localStorage.getItem(key));
      let DefaultGAChartDataObj = obj[0].GA_Reports; //第一份報表
      // 填入 Widget
      let widget = DramaCore.appendWidgetElement(DefaultGAChartDataObj[0], WidgetCounts);
      //0:date 1:AD 2:sourcemedium 3:campaign
      //(資料位置,資料筆數(0 : 不限制),資料說明)
      DefaultCharts.chart1(DefaultGAChartDataObj, 2, 10, "【來源 / 媒介】Top 10 交易次數 ", "chart1");
      DefaultCharts.chart2(DefaultGAChartDataObj, 2, 10, "【來源 / 媒介】Top 10 收益", "chart2");
      DefaultCharts.chart3(DefaultGAChartDataObj, 2, 10, "【來源 / 媒介】Top 10 加入會員", "chart3");
      DefaultCharts.chart4(DefaultGAChartDataObj, 3, 10, "【活動】Top 10 交易次數", "chart4");
      DefaultCharts.chart5(DefaultGAChartDataObj, 3, 10, "【活動】Top 10 收益", "chart5");
      DefaultCharts.chart6(DefaultGAChartDataObj, 3, 10, "【活動】Top 10 加入會員", "chart6");
      DefaultCharts.chart7(DefaultGAChartDataObj, 1, 0, "廣告成效分析", "chart7");

    } catch (e) {
      console.info(e);
    }
  },
  renderCustomGAChart: function (key) {
    try {

      let obj = JSON.parse(localStorage.getItem(key));
      let CustomGAChartDataObj1 = obj[0].GA_Reports; //第一份報表
      let CustomGAChartDataObj2 = obj[1].GA_Reports; //第二份報表
      // 填入 Widget
      let widget = DramaCore.appendWidgetElement(CustomGAChartDataObj1[0], WidgetCounts);
      //0:date 1:sourcemedium 2: 3:campaign
      //(資料位置,資料筆數(0 : 不限制),資料說明)
      CustomCharts.chart1(CustomGAChartDataObj1, 4, 5, "Top 5 頁面平均停留時間(秒)", "chart1");
      CustomCharts.chart2(CustomGAChartDataObj2, 0, 10, "Top 10 【站外】導流客戶數( 7 日間比較)", "chart2");
      CustomCharts.chart3(CustomGAChartDataObj1, 4, 5, "Top 5 頁面跳出數", "chart3", "chart3");
      CustomCharts.chart4(CustomGAChartDataObj2, 0, 10, "【站外】導流分析表( 最近 7 日)", "chart4");
      CustomCharts.chart5(CustomGAChartDataObj1, 2, 10, "依【性別】合計訪客數", "chart5");
      CustomCharts.chart6(CustomGAChartDataObj1, 2, 10, "依【性別】 / 【年齡】合計新訪客數", "chart6");
      CustomCharts.chart7(CustomGAChartDataObj1, 3, 0, "依【設備】合計訪客數", "chart7");
      CustomCharts.chart8(CustomGAChartDataObj1, 3, 0, "依【設備】合計工作階段停留時間", "chart8");

    } catch (e) {
      console.info(e);
    }
  },
  renderGAECChart: function (key) {
    try {
      
      let obj = JSON.parse(localStorage.getItem(key));
      console.log(obj);
      let CustomGAChartDataObj1 = obj[0].GA_Reports; //第一份報表
      let CustomGAChartDataObj3 = obj[3].GA_Reports; //第四份報表

      // 填入 Widget
      let widget = DramaCore.appendWidgetElement(CustomGAChartDataObj1[0], ECWidgetCounts);
      //0:userType 
      //(資料位置,資料筆數(0 : 不限制),資料說明)
      ECChart.chart1(obj, 0, 0, "近四周 新訪客 / 回訪客 收益 ", "chart1");
      ECChart.chart2(CustomGAChartDataObj1, 2, 0, "台灣各城市營收 ", "chart2");
      ECChart.chart3(obj, 0, 0, "近四周 訪客 / 新訪客 購買量 ", "chart3");
      ECChart.chart4(CustomGAChartDataObj1, 1, 0, "平均訂單價值 / 跳出率 四象限分析 ", "chart4");
      ECChart.chart5(CustomGAChartDataObj1, 3, 0, "廣告來源 / 活動 分析 ", "chart5");
      ECChart.chart6(CustomGAChartDataObj1, 1, 0, "平均訂單價值 / 營收 四象限分析 ", "chart6");
      ECChart.chart7(CustomGAChartDataObj3, 1, 0, "各品牌營收", "chart7");
      ECChart.chart8(CustomGAChartDataObj3, 0, 0, "各類別營收", "chart8");
      ECChart.chart9(CustomGAChartDataObj3, 2, 0, "各類別-顏色營收 ", "chart9");
      ECChart.chart10(CustomGAChartDataObj3, 3, 0, "各通路營收 ", "chart10");
      ECChart.chart11(CustomGAChartDataObj3, 4, 0, "各店鋪營收 ", "chart11");
      ECChart.chart12(CustomGAChartDataObj3, 5, 0, "產品群組營收 ", "chart12");
      ECChart.chart13(CustomGAChartDataObj3, 6, 0, "季節產品營收 ", "chart13");
      ECChart.chart14(CustomGAChartDataObj3, 7, 0, "季節產品群組營收 ", "chart14");
      ECChart.chart15(CustomGAChartDataObj3, 8, 0, "性別群組營收 ", "chart15");
      ECChart.chart16(CustomGAChartDataObj3, 9, 0, "性別與產品群組營收 ", "chart16");
    } catch (e) {
      console.info(e);
    }
  },
  renderTealeafChart: function (key) {
    try {
      let obj = JSON.parse(localStorage.getItem(key));

      let emptyObj = {};
      let widget = DramaCore.appendTealeafWidgetElement(obj[14], InteractiveWidgetCounts);
      //(資料位置,資料筆數(0 : 不限制),資料說明)
      TealeafChart.chart1A(emptyObj, 2, 0, "熱點圖 ", "chart1A");
      TealeafChart.chart1B(emptyObj, 2, 0, "熱點圖 ", "chart1B");
      TealeafChart.chart2A(emptyObj, 2, 0, "熱點圖 ", "chart2A");
      TealeafChart.chart2B(emptyObj, 2, 0, "熱點圖 ", "chart2B");
      TealeafChart.chart3A(emptyObj, 2, 0, "熱點圖 ", "chart3A");
      TealeafChart.chart3B(emptyObj, 2, 0, "熱點圖 ", "chart3B");
      TealeafChart.chart4(obj, 4, 0, "關鍵字 ", "chart4");
      TealeafChart.chart5(obj, 7, 0, "Google 廣告 ", "chart5");
      TealeafChart.chart6(obj, 5, 0, "站內搜尋  ", "chart6");
      TealeafChart.chart7(obj, 10, 0, "UTM 廣告 ", "chart7");
      TealeafChart.chart8(obj, 13, 0, "註冊會員 ", "chart8");
      TealeafChart.chart9(obj, 0, 0, "頁面瀏覽次數 ", "chart9");



    } catch (e) {
      console.info(e);
    }
  },
  //添加 Widget
  appendWidgetElement: function (widgetData, widgetcounts) {

    let metric = widgetData.ListResult[0].metrics[0].metric;
    let metricName = widgetData.MetricHead;

    for (let i = 0; i < widgetcounts; i++) {
      let j = i + 1;

      $("#w" + j).children(".card-header").children(".widget-text").text(metricName[i]);
      $("#w" + j).children(".card-body").children(".widget-number").text(metric[i].value);

    }
    return true;
  },
  appendTealeafWidgetElement: function (widgetData, widgetcounts) {

    let widgetAry = widgetData.Data;

    let desktopIndex = widgetAry.findIndex(element => element.Col1 === "Desktop");
    let tabletIndex = widgetAry.findIndex(element => element.Col1 === "Tablet");
    let mobilePhoneIndex = widgetAry.findIndex(element => element.Col1 === "MobilePhone");

    for (let i = 1; i <= widgetcounts; i++) {
      switch (i % 4) {
        case 1:
          $("#w" + i).children(".card-header").children(".widget-text").text(InteractiveWidgetHead1);
          break;
        case 2:
          $("#w" + i).children(".card-header").children(".widget-text").text(InteractiveWidgetHead2);
          break;
        case 3:
          $("#w" + i).children(".card-header").children(".widget-text").text(InteractiveWidgetHead3);
          break;
        case 0:
          $("#w" + i).children(".card-header").children(".widget-text").text(InteractiveWidgetHead4);
          break;
      }


    }
    if (desktopIndex >= 0) {
      $("#w1").children(".card-body").children(".widget-number").text(widgetAry[desktopIndex].Col2);
      $("#w2").children(".card-body").children(".widget-number").text(widgetAry[desktopIndex].Col3);
      $("#w3").children(".card-body").children(".widget-number").text(widgetAry[desktopIndex].Col4);
      $("#w4").children(".card-body").children(".widget-number").text(widgetAry[desktopIndex].Col5);
    } else {
      $("#w1").children(".card-body").children(".widget-number").text("0");
      $("#w2").children(".card-body").children(".widget-number").text("0");
      $("#w3").children(".card-body").children(".widget-number").text("0");
      $("#w4").children(".card-body").children(".widget-number").text("0");
    }
    if (mobilePhoneIndex >= 0) {

      $("#w5").children(".card-body").children(".widget-number").text(widgetAry[mobilePhoneIndex].Col2);
      $("#w6").children(".card-body").children(".widget-number").text(widgetAry[mobilePhoneIndex].Col3);
      $("#w7").children(".card-body").children(".widget-number").text(widgetAry[mobilePhoneIndex].Col4);
      $("#w8").children(".card-body").children(".widget-number").text(widgetAry[mobilePhoneIndex].Col5);

    } else {
      $("#w5").children(".card-body").children(".widget-number").text("0");
      $("#w6").children(".card-body").children(".widget-number").text("0");
      $("#w7").children(".card-body").children(".widget-number").text("0");
      $("#w8").children(".card-body").children(".widget-number").text("0");
    }
    if (tabletIndex >= 0) {

      $("#w9").children(".card-body").children(".widget-number").text(widgetAry[tabletIndex].Col2);
      $("#w10").children(".card-body").children(".widget-number").text(widgetAry[tabletIndex].Col3);
      $("#w11").children(".card-body").children(".widget-number").text(widgetAry[tabletIndex].Col4);
      $("#w12").children(".card-body").children(".widget-number").text(widgetAry[tabletIndex].Col5);
    } else {
      $("#w9").children(".card-body").children(".widget-number").text("0");
      $("#w10").children(".card-body").children(".widget-number").text("0");
      $("#w11").children(".card-body").children(".widget-number").text("0");
      $("#w12").children(".card-body").children(".widget-number").text("0");
    }


    return true;
  }


};