DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();
const widgetitems = (document.getElementById('dataWidget'));
const chartitems = (document.getElementById('dataChart'));
var refIndex1 = 30000; // 訪客數參考指標
var L10n = ej.base.L10n;
var dp,DataObj, Dataobj1;

const gridColumn = [{
    field: 'ADGroup',
    headerText: '廣告群組',
    textAlign: 'center',
    width: 100,
    format: 'N'
  },
  {
    field: 'impression',
    headerText: '曝光',
    textAlign: 'left',
    width: 100,
    format: 'N'
  },
  {
    field: 'adClick',
    width: 90,
    headerText: '廣告點擊',
    format: 'N'
  },
  {
    field: 'adCost',
    width: 100,
    headerText: '廣告成本',
    format: 'N',
    
  },
  {
    field: 'CPM',
    headerText: 'CPM',
    textAlign: 'left',
    width: 100,
    format: 'N'
  },
  {
    field: 'CPC',
    headerText: 'CPC',
    textAlign: 'left',
    width: 100,
    format: 'N'
  },
  {
    field: 'costPerGoalConversion',
    headerText: '廣告轉換成本',
    textAlign: 'left',
    width: 120,
    format: 'N'
  },
  {
    field: 'costPerTransaction',
    headerText: '交易成本',
    textAlign: 'left',
    width: 100,
    format: 'N'
  },
];
var customgridColumn = [{
  field: 'referral',
  headerText: '參考來源',
  textAlign: 'center',
  width: 100,
  type: 'string'
},
{
  field: 'sessions',
  width: 80,
  headerText: '工作階段',
  type: 'N'
},
{
  field: 'uniquePageviews',
  width: 100,
  headerText: '不重複瀏覽量',
  type: 'N'
},
{
  field: 'users',
  width: 100,
  headerText: '訪客數',
  type: 'string',
  template: '#gridTemplate'
},
{
  field: 'newUsers',
  headerText: '新訪客數',
  textAlign: 'left',
  width: 100,
  format: 'N'
},
{
  field: 'bounces',
  headerText: '跳出數',
  textAlign: 'left',
  width: 100,
  format: 'N'
},
{
  field: 'avgSessionDuration',
  headerText: '平均停留時間',
  textAlign: 'left',
  width: 100,
  format: 'string'
}
];



$(document).ready(function () {
    
    DramaCore.validateTokenCall(validateSetting());
    
    let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
    dp = DramaCore.createDatePicker(dt, dt);
    let key = "default-" + dt + "-" + dt + "-" + "1";
    let defaultkey= "default-" + sessionStorage.getItem(defaultStartDate) + "-" + sessionStorage.getItem(defaultEndDate) + "-" + sessionStorage.getItem(daySpan);
    dp.value=[sessionStorage.getItem(defaultStartDate),sessionStorage.getItem(defaultEndDate)];  
   
    if(defaultkey===key){
      
      initDefaultGAPage.widget();
      initDefaultGAPage.chart();
      DramaCore.renderDefaultGAChart(defaultkey);
    }else{
      console.log("custom-frameset");
      initCustomGAPage.widget();
      initCustomGAPage.chart();
      DramaCore.renderCustomGAChart(defaultkey);
    }
 
});


// url --> remote url ; type:post or get ; para:parameters
var initDefaultGAPage={
    widget:function(){
        var  widgetMetaData=[
            {id:'w1',icons:'e-icons session'},
            {id:'w2',icons:'e-icons views'},
            {id:'w3',icons:'e-icons profile'},
            {id:'w4',icons:'e-icons profile'},
            {id:'w5',icons:'e-icons avgtime'},
            {id:'w6',icons:'e-icons session1'},
            {id:'w7',icons:'e-icons profile1'},
            {id:'w8',icons:'e-icons bounce'}
          
        ];
        var getWidgetString = ej.base.compile('<div class="col-sm-12 col-md-3 mb-1"> <div id=${id} class="card card_animation"> <div class="card-header widget-head "> <h5 class="widget-text align-middle"></h5> </div><div class="card-body"> <span class="${icons}"></span> <h5 class="widget-number align-middle"></h5> </div></div></div>');
        
        while (widgetitems.firstChild) {
          widgetitems.removeChild(widgetitems.firstChild);
        }
        widgetMetaData.forEach(data => {
            widgetitems.appendChild(getWidgetString(data)[0]);
        });
    },
    chart:function(){
        let  chartMetaData=[
            {id:'chart1',size:'col-md-6'},
            {id:'chart2',size:'col-md-6'},
            {id:'chart3',size:'col-md-6'},
            {id:'chart4',size:'col-md-6'},
            {id:'chart5',size:'col-md-6'},
            {id:'chart6',size:'col-md-6'},
            {id:'chart7',size:'col-md-12'},
      
          
        ];
        let getChartString = ej.base.compile('<div class="col-sm-12 ${size} mb-1 d-flex"> <div id=${id} class="card card_animation flex-fill"> <div class="card-header widget-head"> </div><div class="charts card-body chart-height"> </div> </div>  </div>');
        while (chartitems.firstChild) {
          chartitems.removeChild(chartitems.firstChild);
        }
        chartMetaData.forEach(data => {
            chartitems.appendChild(getChartString(data)[0]);
        });
    }

};
var initCustomGAPage={
  widget:function(){
      var  widgetMetaData=[
          {id:'w1',icons:'e-icons session'},
          {id:'w2',icons:'e-icons views'},
          {id:'w3',icons:'e-icons profile'},
          {id:'w4',icons:'e-icons profile'},
          {id:'w5',icons:'e-icons avgtime'},
          {id:'w6',icons:'e-icons session1'},
          {id:'w7',icons:'e-icons profile1'},
          {id:'w8',icons:'e-icons bounce'}
        
      ];
      var getWidgetString = ej.base.compile('<div class="col-sm-12 col-md-3 mb-1"> <div id=${id} class="card card_animation"> <div class="card-header widget-head "> <h5 class="widget-text align-middle"></h5> </div><div class="card-body"> <span class="${icons}"></span> <h5 class="widget-number align-middle"></h5> </div></div></div>');
      while (widgetitems.firstChild) {
        widgetitems.removeChild(widgetitems.firstChild);
      }

      widgetMetaData.forEach(data => {
          widgetitems.appendChild(getWidgetString(data)[0]);
      });
  },
  chart:function(){
      let  chartMetaData=[
          {id:'chart1',size:'col-md-3'},
          {id:'chart2',size:'col-md-9'},
          {id:'chart3',size:'col-md-3'},
          {id:'chart4',size:'col-md-9'},
          {id:'chart5',size:'col-md-3'},
          {id:'chart6',size:'col-md-3'},
          {id:'chart7',size:'col-md-3'},
          {id:'chart8',size:'col-md-3'}
      ];
      let getChartString = ej.base.compile('<div class="col-sm-12 ${size} mb-1 d-flex"> <div id=${id} class="card card_animation flex-fill"> <div class="card-header widget-head"> </div><div class="charts card-body chart-height"> </div> </div>  </div>');
      while (chartitems.firstChild) {
        chartitems.removeChild(chartitems.firstChild);
      }
      chartMetaData.forEach(data => {
          chartitems.appendChild(getChartString(data)[0]);
      });
  }

};

// 客製化表格欄位值

var DefaultCharts = {
  chart1: function (data,location, rowMax, header,chartid) {
    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
      
    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[0].value;
    }
    
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
    
      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
 
    let color = ["#4A90E2"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: []
    };
    
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart2: function (data,location, rowMax, header,chartid) {
    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
      
    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[1].value;
    }
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
    let color = ["#7ED321"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: []
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart3: function (data,location, rowMax, header,chartid) {
    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
      
    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[4].value;
    }
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
    let color = ["#FB7507"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: []
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart4: function (data,location, rowMax, header,chartid) {
    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
      
    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[0].value;
    }
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
    let color = ["#4A90E2"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: []
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart5:function (data,location, rowMax, header,chartid) {
    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
     
    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[1].value;
    }
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
    let color = ["#7ED321"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: []
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart6:function (data,location, rowMax, header,chartid) {
    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
      
    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[4].value;
    }
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
    let color = ["#FB7507"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: []
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithDataTable(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart7: function (data,location, rowMax, header,chartid) {
    let rst = data[location],
    
      chartObj = {},
      list = [],
      metric = [];

    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj = {};
      chartObj.ADGroup=rst.ListResult[i].dimension[0];
      for (let j = 0; j < rst.ListResult[i].metrics[0].metric.length; j++) {
        chartObj.impression =Number(rst.ListResult[i].metrics[0].metric[0].value) ;
        chartObj.adClick = Number(rst.ListResult[i].metrics[0].metric[1].value) ;
        chartObj.adCost = Number(rst.ListResult[i].metrics[0].metric[2].value)  ;
        chartObj.CPM = Number(rst.ListResult[i].metrics[0].metric[3].value)  ;
        chartObj.CPC = Number(rst.ListResult[i].metrics[0].metric[4].value)  ;
        chartObj.costPerGoalConversion = Number(rst.ListResult[i].metrics[0].metric[5].value)  ;
        chartObj.costPerTransaction = Number(rst.ListResult[i].metrics[0].metric[6].value)  ;
   

      }
      list.push(chartObj);

    }
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: gridColumn
    };
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart7 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
  }
  
};
var CustomCharts ={
  chart1: function (data,location, rowMax, header,chartid) {
    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];
    
    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[5].value;
    }
    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
    let color = ["#4A90E2"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: []
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart2: function (data,location, rowMax, header,chartid) {

    let rst = data[location],
      dimension1 = [],
      metric1 = [],
      rowcount,
      metric2 = [];

    if (rst.ListResult.length > rowMax) {
      rowcount = rowMax;
    } else {
      rowcount = rst.ListResult.length;
    }
    for (let i = 0; i < rowcount; i++) {
      let arr = rst.ListResult[i];
      dimension1.push(arr.dimension[0]);
      metric1.push(arr.metrics[0].metric[3].value);
      metric2.push(arr.metrics[1].metric[3].value);
    }
    let color = ["#4A90E2", "#FB7507"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension1,
      value1: metric1,
      value2: metric2,
      color: color,
      legend: ["上期 7 日", "本期 7 日"]
    };

    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.doubleBarChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart3: function (data,location, rowMax, header,chartid) {

    rowMax += 1; // 因迴圈需求，資料筆數再加 1
    let rst = data[location],
      chartObj = {},
      dimension = [],
      metric = [];

    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj[rst.ListResult[i].dimension[0]] = rst.ListResult[i].metrics[0].metric[6].value;
    }

    let entries = Object.entries(chartObj);
    let sorted = entries.sort((a, b) => a[1] - b[1]);
    for (var i = sorted.length - 1; i > sorted.length - rowMax; i--) {
      let arr = sorted[i];
      dimension.push(arr[0]);
      metric.push(arr[1]);
    }
    let color = ["#FB7507"];
    let setting = {
      cardHead: header,
      titleText: "",
      category: dimension,
      value: metric,
      color: color,
      legend: []
    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart4: function (data,location, rowMax, header,chartid) {
    let rst = data[location],
      chartObj = {},
      list = [],
      metric = [];

    for (let i = 0; i < rst.ListResult.length; i++) {
      //rst.ListResult[i].dimension[0] --> 指定 Dimension 位置
      chartObj = {};
      chartObj.referral = rst.ListResult[i].dimension[0];

      for (let j = 0; j < rst.ListResult[i].metrics[1].metric.length; j++) {
        chartObj.sessions =Number( rst.ListResult[i].metrics[0].metric[0].value);
        chartObj.pageviewsPerSession = Number( rst.ListResult[i].metrics[0].metric[1].value);
        chartObj.newUsers = Number(rst.ListResult[i].metrics[0].metric[2].value);
        chartObj.users = Number(rst.ListResult[i].metrics[0].metric[3].value);
        chartObj.avgSessionDuration = rst.ListResult[i].metrics[0].metric[4].value;
        chartObj.avgTimeOnPage = Number(rst.ListResult[i].metrics[0].metric[5].value);
        chartObj.bounces =Number( rst.ListResult[i].metrics[0].metric[6].value);
        chartObj.uniquePageviews = Number(rst.ListResult[i].metrics[0].metric[6].value);

      }
      list.push(chartObj);

    }

    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      customiseCell:function(args){
        if (args.column.field === 'users') {
          let _var = args.data.users + ' 人';
          args.cell.querySelector("#gridTmp").textContent = _var;
          if (parseInt(args.data.users) >= refIndex1) {
            args.cell.querySelector("#gridTmp").classList.add("smileStatus");
      
          }
      
        }
      },
      gridColumn: customgridColumn
    };
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart4 .card-body');
    $("#"+chartid).children(".card-header").text(setting.cardHead);
  },
  chart5: function (data,location, rowMax, header,chartid) {
    let rst = data[location],
      _femalechartObj = {},
      _malechartObj = {},
      list = [],
      _femalenewUserCount = 0,
      _femaleuserCount = 0,
      _malenewUserCount = 0,
      _maleuserCount = 0,
      metric = [];
    for (let i = 0; i < rst.ListResult.length; i++) {

      switch (rst.ListResult[i].dimension[0]) {
        case "female":

          _femalenewUserCount += parseInt(rst.ListResult[i].metrics[0].metric[2].value);
          _femaleuserCount += parseInt(rst.ListResult[i].metrics[0].metric[3].value);

          break;
        case "male":

          _malenewUserCount += parseInt(rst.ListResult[i].metrics[0].metric[2].value);
          _maleuserCount += parseInt(rst.ListResult[i].metrics[0].metric[3].value);

          break;
      }


    }
    _femalechartObj.gender = "female";
    _femalechartObj.users = _femaleuserCount;
    _femalechartObj.newUsers = _femalenewUserCount;
    list.push(_femalechartObj);
    _malechartObj.gender = "male";
    _malechartObj.users = _maleuserCount;
    _malechartObj.newUsers = _malenewUserCount;
    list.push(_malechartObj);
    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      symbol:symbols,
      color: [],

    };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.doubleCustomSymbolBarChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart6: function (data,location, rowMax, header,chartid) {

    let rst = data[location],
    list = [],
    femaleUser =[],
    femaleNewUser =[],
    femaleAge = [],
    maleUser = [],
    maleNewUser = [],
    maleAge = [],
    femaleObj = {};
    maleObj = {};
  
  for(var i=0;i<rst.ListResult.length;i++){
    switch (rst.ListResult[i].dimension[0]) {
      case"female":
        femaleUser.push(rst.ListResult[i].metrics[0].metric[3].value);
        femaleNewUser.push(rst.ListResult[i].metrics[0].metric[2].value);
        femaleAge.push(rst.ListResult[i].dimension[1]);
      break;
      case"male":
        maleUser.push(-Math.abs(rst.ListResult[i].metrics[0].metric[3].value));//minus number
        maleNewUser.push(-Math.abs( rst.ListResult[i].metrics[0].metric[2].value));
        maleAge.push(rst.ListResult[i].dimension[1]);
      break;
    }
    
}
    femaleObj.gender="女性";
    femaleObj.user=femaleUser;
    femaleObj.newUsers=femaleNewUser;
    femaleObj.age=femaleAge;
    maleObj.gender="男性";
    maleObj.user=maleUser;
    maleObj.newUsers=maleNewUser;
    maleObj.age=maleAge;

  let setting = {
    legend:["女性","男性"],
    cardHead: header,
    titleText: "",
    value1: femaleObj,
    value2: maleObj,
    color: [],

  };
  let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.doubleHorizontalSplitBarChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart7: function (data,location, rowMax, header,chartid) {
    let rst = data[location],
      totalUserbyDevice=0,
      chartObj = {},
      mobileAry = [],
      desktopAry = [],
      tabletAry = [];

      for(var i=0;i<rst.ListResult.length;i++){
        
        switch(rst.ListResult[i].dimension[0]){
          case"desktop":
            desktopAry[0]=(rst.ListResult[i].metrics[0].metric[3].value);
          break;
          case"mobile":
            mobileAry[0]=(rst.ListResult[i].metrics[0].metric[3].value);
          break;
          case"tablet":
            tabletAry[0]=(rst.ListResult[i].metrics[0].metric[3].value);
          break;
        }
        totalUserbyDevice +=parseInt(rst.ListResult[i].metrics[0].metric[3].value);
      }
      desktopAry[1]=(parseInt(desktopAry[0])/parseInt(totalUserbyDevice));
      desktopAry[2]=(1-(parseInt(desktopAry[0])/parseInt(totalUserbyDevice)));
      desktopAry[3]="桌機";
      mobileAry[1]=(parseInt(mobileAry[0])/parseInt(totalUserbyDevice));
      mobileAry[2]=(1-(parseInt(mobileAry[0])/parseInt(totalUserbyDevice)));
      mobileAry[3]="手機";
      tabletAry[1]=(parseInt(tabletAry[0])/parseInt(totalUserbyDevice));
      tabletAry[2]=(1-(parseInt(tabletAry[0])/parseInt(totalUserbyDevice)));
      tabletAry[3]="平板";
      let dataStyle = {
        normal: {
            label: {
                show: false
            },
            labelLine: {
                show: true
            },
            shadowBlur: 0,
            shadowColor: '#203665'
        }
    };
      let setting = {
        cardHead: header,
        titleText: "",
        value1: desktopAry,
        value2: mobileAry,
        value3: tabletAry,
        dataStyle:dataStyle,
        unit:"人",
        color1: ["#5c684f","#FB7507","#9b9557"],
        color2: ["#7ED321","#b57744","#F8E71C"],
    
      };
    let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.tripleCirclePieChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  },
  chart8: function (data,location, rowMax, header,chartid) {
    let rst = data[location],
      totalDurationTimebyDevice=0,
      chartObj = {},
      mobileAry = [],
      desktopAry = [],
      tabletAry = [];

      for(var i=0;i<rst.ListResult.length;i++){
        
        switch(rst.ListResult[i].dimension[0]){
          case"desktop":
            desktopAry[0]=Math.round((rst.ListResult[i].metrics[0].metric[4].value)/60);
          break;
          case"mobile":
            mobileAry[0]=Math.round((rst.ListResult[i].metrics[0].metric[4].value)/60);
            
          break;
          case"tablet":
            tabletAry[0]=Math.round((rst.ListResult[i].metrics[0].metric[4].value)/60);
          break;
        }
        totalDurationTimebyDevice +=parseInt(rst.ListResult[i].metrics[0].metric[4].value);
      }
      desktopAry[1]=(parseInt(desktopAry[0])/parseInt(totalDurationTimebyDevice));
      desktopAry[2]=(1-(parseInt(desktopAry[0])/parseInt(totalDurationTimebyDevice)));
      desktopAry[3]="桌機";
      mobileAry[1]=(parseInt(mobileAry[0])/parseInt(totalDurationTimebyDevice));
      mobileAry[2]=(1-(parseInt(mobileAry[0])/parseInt(totalDurationTimebyDevice)));
      mobileAry[3]="手機";
      tabletAry[1]=(parseInt(tabletAry[0])/parseInt(totalDurationTimebyDevice));
      tabletAry[2]=(1-(parseInt(tabletAry[0])/parseInt(totalDurationTimebyDevice)));
      tabletAry[3]="平板";
      let dataStyle = {
        normal: {
            label: {
                show: false
            },
            labelLine: {
                show: true
            },
            shadowBlur: 0,
            shadowColor: '#203665'
        }
    };
      let setting = {
        cardHead: header,
        titleText: "",
        value1: desktopAry,
        value2: mobileAry,
        value3: tabletAry,
        dataStyle:dataStyle,
        unit:"分",
        color1: ["#5c684f","#FB7507","#9b9557"],
        color2: ["#7ED321","#b57744","#F8E71C"],
    
      };
      let chart = echarts.init(document.getElementById(chartid).getElementsByClassName('card-body')[0]);
    let option = chartSetting.tripleCirclePieChart(setting);
    if (option && typeof option === "object") {
      console.info(chartid);
      chart.setOption(option, true);
      $("#"+chartid).children(".card-header").text(setting.cardHead);

    }
  }
};
function validateSetting(){
    let bearerToken =sessionStorage.getItem("token");
    let urlTo= AuthremoteUrl+"GetUserName";
    let Setting={
        async: true,
        type: "GET",
        url: urlTo,
        headers: {
            Authorization: "Bearer" +" "+ bearerToken
           
          }
    };
    return Setting;
}

