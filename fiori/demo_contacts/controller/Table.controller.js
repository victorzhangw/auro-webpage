var host, protocol;
var ip;
var oMasterView;
//jQuery.sap.require("sap.ushell.Container");
sap.ui.define([
		'jquery.sap.global',
		"sap/m/MessageToast",
		"crudTrainingDemo/controller/BaseController",
		'sap/ui/model/json/JSONModel',
		"sap/ui/model/Filter",
		"sap/ui/model/FilterOperator"
	], function(jQuery, MessageToast, BaseController, JSONModel, Filter, FilterOperator) {
	"use strict";
	var lv_zid = "";
	var TableController = BaseController.extend("crudTrainingDemo.controller.Table", {
		
		onInit: function () {
      	  oMasterView = this;
    	  host = window.location.host;
		  protocol = window.location.protocol;
		  ip = protocol+"//"+host;
		  var oTable = this.byId("idPersonDetail");
		  this._oTable = oTable;
		  
          var oDirect_Delivery = ip+"/sap/opu/odata/sap/ZTRAINING_DEMO_SRV";
          // oDirect_Delivery = http://itlhana.htah.com.cn:50000/sap/opu/odata/sap/ZTRAINING_DEMO_SRV
          var ooDirect_DeliveryModel = new sap.ui.model.odata.ODataModel(oDirect_Delivery, true);
          ooDirect_DeliveryModel.read("/EtPersonSet?$format=json",{
                  success:function(data){
                 	    var array = data.results;
							var retailermodel_Direct = new sap.ui.model.json.JSONModel();
							retailermodel_Direct.setDefaultBindingMode("OneWay");
							retailermodel_Direct.setData(array);
							oMasterView.getView().setModel(retailermodel_Direct,"tableDisplayModel");
                },
                  error:function(){
                   sap.m.MessageToast.show("Record Update Error!", '', "Error");
                }
          });
		},

		/**
		 * Event handler when a table item gets pressed
		 * @param {sap.ui.base.Event} oEvent the table selectionChange event
		 * @public
		 */
		onDisplay: function(oEvent){
			var oSource = oEvent.getSource();
			this._showObject(oSource);
		},
		
		_showObject: function(oItem) {
			this.getRouter().navTo("detail", {
				oId : oItem.getBindingContext("tableDisplayModel").getProperty("Zid")
			});
		},
		
		onCreate: function(){
			  this.getRouter().navTo("create"); 

		},
		
		onSearch: function(oEvent) {
			if (oEvent.getParameters().refreshButtonPressed) {
				// Search field's 'refresh' button has been pressed.
				// This is visible if you select any master list item.
				// In this case no new search is triggered, we only
				// refresh the list binding.
				this.onRefresh();
			} else {
				var aFilter = [];
				var sQuery = oEvent.getParameter("query");

				if (sQuery && sQuery.length > 0) {
					aFilter = [new Filter("Zid", FilterOperator.Contains, sQuery)];
				}
				this._applySearch(aFilter);
			}

		},
		
		_applySearch: function(aFilter) {
			var oViewModel = this.getView().getModel("tableDisplayModel");
			this._oTable.getBinding("items").filter(aFilter);
		},
		
		onRefresh: function() {
			this._oTable.getBinding("items").refresh();
		},

		onNavBack : function(){
			this.oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");  
			this.oCrossAppNavigator.toExternal({  
				target : { shellHash : "#" }
			}); 
		},
		
		onExit : function(){
			var test1;
		},
		
		onMessage : function(){
			MessageToast.show("test OK!");
		}
	});
	
	

	return TableController;

});
