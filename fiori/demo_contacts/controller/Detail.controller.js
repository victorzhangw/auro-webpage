var host, protocol;
var ip;
var oMasterView;
var oMasterView2;
var postData;
sap.ui.define([
		"jquery.sap.global",
		"sap/m/MessageToast",
		"crudTrainingDemo/controller/BaseController",
		"sap/ui/model/json/JSONModel"
	], function(jQuery, MessageToast, BaseController, JSONModel) {
	"use strict";
	var oId,lv_zid;
	return BaseController.extend("crudTrainingDemo.controller.Detail", {

		onInit: function () {
		    
			oMasterView2 = this;
			
			var oRouter = this.getRouter();

			oRouter.getRoute("detail").attachMatched(this._onRouteMatched, this);

		},
		
		_onRouteMatched : function (oEvent) {
			var oArgs;
			var oView = this.getView();
			var zid, zname, zphone, zaddress;
			oArgs = oEvent.getParameter("arguments");
			oId = oArgs.oId.substr(0,6);
			lv_zid = oArgs.oId.substr(6);
	    	host = window.location.host;
			protocol = window.location.protocol;
			ip = protocol+"//"+host;
      	  OData.request({
  		  requestUri: ip+"/sap/opu/odata/sap/ZTRAINING_DEMO_SRV/EtPersonSet('"+oId+"')",
  		  method: "GET",
  		  headers: {
    		  "X-Requested-With" : "XMLHttpRequest",
    		  "Content-Type" : "application/atom+xml",
    		  "DataServiceVersion" : "2.0",
    		  "X-CSRF-Token" : "Fetch"
  		  }},
  		  
  		  function(data,response){
  			  
  			  postData = {
  					  Zid : data.Zid,
  					  Zname : data.Zname,
  					  Zphone : data.Zphone,
  					  Zaddress : data.Zaddress
  			  };
  			  var dataModel = new sap.ui.model.json.JSONModel();
  			  dataModel.setData(postData);
  		      oMasterView2.getView().setModel(dataModel, "detailModel");
  			  
  			  
	    	  host = window.location.host;
			  protocol = window.location.protocol;
			  ip = protocol+"//"+host;
	          var oDirect_Delivery = ip+"/sap/opu/odata/sap/ZTRAINING_DEMO_SRV";
	          // oDirect_Delivery = http://sapecq02:8000/sap/opu/odata/sap/ZTRAINING_DEMO_SRV
	          var ooDirect_DeliveryModel = new sap.ui.model.odata.ODataModel(oDirect_Delivery, true);
	          ooDirect_DeliveryModel.read("/EtPersonSet?$filter="+lv_zid,{
	                  success:function(data){
	                 	    var array = data.results;
							var retailermodel_Direct = new sap.ui.model.json.JSONModel();
							retailermodel_Direct.setData(array);
							oMasterView.getView().setModel(retailermodel_Direct,"tableDisplayModel");
	                },
	                  error:function(){
	                   sap.m.MessageToast.show("Record Update Error!", '', "Error");
	                }
	          });
  		  },
  		  
		  function(err){
  			  sap.m.MessageToast.show("Get Data Error!", '', "Error");
	      }  
  		  );
		},
		
		onEdit : function(){
			oMasterView2.getView().byId("delete").setVisible(false);
			oMasterView2.getView().byId("edit").setVisible(false);
			oMasterView2.getView().byId("cancel").setVisible(true);
			oMasterView2.getView().byId("save").setVisible(true);
			oMasterView2.getView().byId("name").setEnabled(true);
			oMasterView2.getView().byId("phone").setEnabled(true);
			oMasterView2.getView().byId("address").setEnabled(true);
		},
		
		onCancel : function(){
			oMasterView2.getView().byId("delete").setVisible(true);
			oMasterView2.getView().byId("edit").setVisible(true);
			oMasterView2.getView().byId("cancel").setVisible(false);
			oMasterView2.getView().byId("save").setVisible(false);
			oMasterView2.getView().byId("name").setEnabled(false);
			oMasterView2.getView().byId("phone").setEnabled(false);
			oMasterView2.getView().byId("address").setEnabled(false);
		},
		
		onSave : function(){
			
			  var oPage = this.getView().byId("employeePage");
			  
	          var oId = this.getView().byId("id").getValue();
	          
	          var name = this.getView().byId("name").getValue();
	        
	          var phone = this.getView().byId("phone").getValue();
	          
	          var address = this.getView().byId("address").getValue();
	          
	          var oThis = this;
	          
	    	  host = window.location.host;
			  protocol = window.location.protocol;
			  ip = protocol+"//"+host;
	          OData.request({
	        	  requestUri: ip+"/sap/opu/odata/sap/ZTRAINING_DEMO_SRV/",
	        	  method: "GET",
	        	  headers: {
	        		  "X-Requested-With" : "XMLHttpRequest",
	        		  "Content-Type" : "application/atom+xml",
	        		  "DataServiceVersion" : "2.0",
	        		  "X-CSRF-Token" : "Fetch"
	        	  }
	          },
	          function(data, response){
	        	  var header_xcsrf_token = response.headers['x-csrf-token'];
	        	  OData.request({
	        		  requestUri: ip+"/sap/opu/odata/sap/ZTRAINING_DEMO_SRV/EtPersonSet('"+oId+"')",
	        		  method: "PUT",
	        		  headers: {
		        		  "X-Requested-With" : "XMLHttpRequest",
		        		  "Content-Type" : "application/atom+xml",
		        		  "DataServiceVersion" : "2.0",
		        		  "Accept" : "application/atom+xml, application/atomsvc+xml, application/xml",
		        		  "X-CSRF-Token" : header_xcsrf_token
	        		  },
	        		  data: {
	        			  Zid : oId,
	        			  Zname : name,
	        			  Zphone : phone,
	        			  Zaddress : address
	        		  },
	        		  postData: {
	        			  Zid : oId,
	        			  Zname : name,
	        			  Zphone : phone,
	        			  Zaddress : address
	        		  }
	        		  },
	        		  
	        		  function(data, response){
	        			  
  						oMasterView2.getView().byId("delete").setVisible(true);
  						oMasterView2.getView().byId("edit").setVisible(true);
  						oMasterView2.getView().byId("cancel").setVisible(false);
  						oMasterView2.getView().byId("save").setVisible(false);
  						oMasterView2.getView().byId("name").setEnabled(false);
  						oMasterView2.getView().byId("phone").setEnabled(false);
  						oMasterView2.getView().byId("address").setEnabled(false);
  						sap.m.MessageToast.show("Data updated successfully");
  						
  						//Refresh table data
//  	        			var tableView = sap.ui.getCore().byId("table");
//  	        			tableView.getModel("tableDisplayModel").refresh;
	        	    	  host = window.location.host;
	        			  protocol = window.location.protocol;
	        			  ip = protocol+"//"+host;
	        	          var oDirect_Delivery = ip+"/sap/opu/odata/sap/ZTRAINING_DEMO_SRV";
	        	          // oDirect_Delivery = http://sapecq02:8000/sap/opu/odata/sap/ZTRAINING_DEMO_SRV
	        	          var ooDirect_DeliveryModel = new sap.ui.model.odata.ODataModel(oDirect_Delivery, true);
	        	          ooDirect_DeliveryModel.read("/EtPersonSet?$format=json",{
	        	                  success:function(data){
	        	                 	    var array = data.results;
      								var retailermodel_Direct = new sap.ui.model.json.JSONModel();
      								retailermodel_Direct.setData(array);
      								oMasterView.getView().setModel(retailermodel_Direct,"tableDisplayModel");
	        	                },
	        	                  error:function(){
	        	                   sap.m.MessageToast.show("Record Update Error!", '', "Error");
	        	                }
	        	          });
	        		  },
	        		  function(err){
	        			  sap.m.MessageToast.show("Update Error!", '', "Error");
	        		  }  
	        	  );
	        	  
	          },
	          
	          function(err){
	        	  var request = err.request;
	        	  var response = err.response;
	        	  sap.m.MessageToast.show("Error in Get -- Request"+request+"Response"+response);
	          }
	          )
		},
		
		onAfterRendering : function(){
			oMasterView2 = this;
			oMasterView2.getView().byId("delete").setVisible(true);
			oMasterView2.getView().byId("edit").setVisible(true);
			oMasterView2.getView().byId("cancel").setVisible(false);
			oMasterView2.getView().byId("save").setVisible(false);
			oMasterView2.getView().byId("name").setEnabled(false);
			oMasterView2.getView().byId("phone").setEnabled(false);
			oMasterView2.getView().byId("address").setEnabled(false);
		},
		
		onExit : function(){
			var test1;
		},
		
		onDelete : function(){
			
		  var oPage = this.getView().byId("employeePage");
		  
    	  host = window.location.host;
		  protocol = window.location.protocol;
		  ip = protocol+"//"+host;
		  
          OData.request({
        	  requestUri: ip+"/sap/opu/odata/sap/ZTRAINING_DEMO_SRV/EtPersonSet",
        	  method: "GET",
        	  headers: {
        		  "X-Requested-With" : "XMLHttpRequest",
        		  "Content-Type" : "application/atom+xml",
        		  "DataServiceVersion" : "2.0",
        		  "X-CSRF-Token" : "Fetch"
        	   }
	          },
	          function(data, response){
	        	  var header_xcsrf_token = response.headers['x-csrf-token'];
	        	  OData.request({
	        		  requestUri: ip+"/sap/opu/odata/sap/ZTRAINING_DEMO_SRV/EtPersonSet('"+postData.Zid+"')",
	        		  method: "DELETE",
	        		  headers: {
		        		  "X-Requested-With" : "XMLHttpRequest",
		        		  "Content-Type" : "application/atom+xml",
		        		  "DataServiceVersion" : "2.0",
		        		  "Accept" : "application/atom+xml, application/atomsvc+xml, application/xml",
		        		  "X-CSRF-Token" : header_xcsrf_token
	        		  }},

	        		  function(data, response){
	        			  var oSubDialog = new sap.m.Dialog({
	        				  title: "Deleted",
	        				  content: [
	        				            new sap.m.Label({
	        				            	text : "Data deleted successfully"
	        				            })
	        				            ]
	        			  });
	        			  oPage.setBusy(true);
	        			  oSubDialog.open();
	        			  oSubDialog.addButton(new sap.m.Button({
	        				  text : "OK",
	        				  press : function(){
	        					  oSubDialog.close();
	        					  oPage.setBusy(false);
			        	        //Back to list page
	        					  window.history.go(-1);
//  								  oMasterView2.getRouter().navTo("table");
	        				  }
	        			  }));
	        			  
	        			  //Refresh data on the table
	        	    	  host = window.location.host;
	        			  protocol = window.location.protocol;
	        			  ip = protocol+"//"+host;
	        	          var oDirect_Delivery = ip+"/sap/opu/odata/sap/ZTRAINING_DEMO_SRV";
	        	          // oDirect_Delivery = http://sapecq02:8000/sap/opu/odata/sap/ZTRAINING_DEMO_SRV
	        	          var ooDirect_DeliveryModel = new sap.ui.model.odata.ODataModel(oDirect_Delivery, true);
	        	          ooDirect_DeliveryModel.read("/EtPersonSet?$format=json",{
	        	                  success:function(data){
	        	                 	    var array = data.results;
        								var retailermodel_Direct = new sap.ui.model.json.JSONModel();
        								retailermodel_Direct.setData(array);
        								oMasterView.getView().setModel(retailermodel_Direct,"tableDisplayModel");
	        	                },
	        	                  error:function(){
	        	                   sap.m.MessageToast.show("Record Update Error!", '', "Error");
	        	                }
	        	          });

	        		  },
	        		  function(err){
	        			  sap.m.MessageToast.show("Get data failed", '', "Error");
	        		  }    
	        	  );
	          }
	       );
		}
	});
});
