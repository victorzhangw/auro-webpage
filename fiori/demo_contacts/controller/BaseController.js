sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History"
], function (Controller, History) {
	"use strict";

	return Controller.extend("crudTrainingDemo.controller.BaseController", {

		getRouter : function () {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		onNavBack: function (oEvent) {
			var oHistory, sPreviousHash;

			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();
			//var str = sPreviousHash.substr(0,7);
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("appHome", {}, true /*no history*/);
			}
		},
		
		onShareEmailPress: function() {
//			var oViewModel = (this.getModel("tableDisplayModel") || this.getModel("detailModel"));
			sap.m.URLHelper.triggerEmail(
				null,
				"<Email subject PLEASE REPLACE ACCORDING TO YOUR USE CASE>",
				"<Email body PLEASE REPLACE ACCORDING TO YOUR USE CASE>\r\n{0}"
			);
		}

	});

});
