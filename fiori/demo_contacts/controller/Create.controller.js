var host, protocol;
var ip;
var oMasterView,oMasterView1;
sap.ui.define([
		"jquery.sap.global",
		"sap/m/MessageToast",
		"crudTrainingDemo/controller/BaseController",
		"sap/ui/model/json/JSONModel"
	], function(jQuery, MessageToast, BaseController, JSONModel) {
	"use strict";

	return BaseController.extend("crudTrainingDemo.controller.Create", {

		onInit: function () {

		  oMasterView1 = this;
		  
		  var oView = this.getView();
		  
	      var oId = oView.byId("personId");
	      oId.setValue("");
	      oId.setEditable(true);
	      
	      var oId = oView.byId("nameId");
	      oId.setValue("");
	      oId.setEditable(true);
	    
	      var oId = oView.byId("phoneId");
	      oId.setValue("");
	      oId.setEditable(true);
	      
	      var oId = oView.byId("addressId");
	      oId.setValue("");
	      oId.setEditable(true);
	      
		},
		
		onClear : function(){
		  var oView = this.getView();
		  
	      var oId = oView.byId("personId");
	      oId.setValue("");
	      oId.setEditable(true);
	      oId.setEnabled(true);
	      
	      var oId = oView.byId("nameId");
	      oId.setValue("");
	      oId.setEditable(true);
	      oId.setEnabled(true);
	    
	      var oId = oView.byId("phoneId");
	      oId.setValue("");
	      oId.setEditable(true);
	      oId.setEnabled(true);
	      
	      var oId = oView.byId("addressId");
	      oId.setValue("");
	      oId.setEditable(true);
	      oId.setEnabled(true);
	      
	      this.getView().byId("save").setVisible(true);
	      this.getView().byId("save").setEnabled(true);
		},

		onSave : function(){
			
			  var oPage = this.getView().byId("employeePage");
			
			  var oId = this.getView().byId("personId").getValue();
	          
	          var name = this.getView().byId("nameId").getValue();
	        
	          var phone = this.getView().byId("phoneId").getValue();
	          
	          var address = this.getView().byId("addressId").getValue();
	          
	          var oThis = this;
	          
	    	  host = window.location.host;
			  protocol = window.location.protocol;
			  ip = protocol+"//"+host;
	          OData.request({
	        	  requestUri: ip+"/sap/opu/odata/sap/ZTRAINING_DEMO_SRV/",
	        	  method: "GET",
	        	  headers: {
	        		  "X-Requested-With" : "XMLHttpRequest",
	        		  "Content-Type" : "application/atom+xml",
	        		  "DataServiceVersion" : "2.0",
	        		  "X-CSRF-Token" : "Fetch"
	        	  }
	          },
	          function(data, response){
	        	  var header_xcsrf_token = response.headers['x-csrf-token'];
	        	  OData.request({
	        		  requestUri: ip+"/sap/opu/odata/sap/ZTRAINING_DEMO_SRV/EtPersonSet",
	        		  method: "POST",
	        		  headers: {
		        		  "X-Requested-With" : "XMLHttpRequest",
		        		  "Content-Type" : "application/atom+xml",
		        		  "DataServiceVersion" : "2.0",
		        		  "Accept" : "application/atom+xml, application/atomsvc+xml, application/xml",
		        		  "X-CSRF-Token" : header_xcsrf_token
	        		  },
	        		  data: {
	        			  Zid : oId,
	        			  Zname : name,
	        			  Zphone : phone,
	        			  Zaddress : address
	        		  }},
	        		  function(data, response){
	        			  sap.m.MessageToast.show("data created successfully!");
	        			    oMasterView1.getView().byId("save").setEnabled(false);
	  						oMasterView1.getView().byId("personId").setEnabled(false);
	  						oMasterView1.getView().byId("nameId").setEnabled(false);
	  						oMasterView1.getView().byId("phoneId").setEnabled(false);
	  						oMasterView1.getView().byId("addressId").setEnabled(false);
	        			  
	        			  //Refresh data on the table
	        			  //view.getModel("tableDisplayModel").refresh();
	        	    	  host = window.location.host;
	        			  protocol = window.location.protocol;
	        			  ip = protocol+"//"+host;
	        	          var oDirect_Delivery = ip+"/sap/opu/odata/sap/ZTRAINING_DEMO_SRV";
	        	          var ooDirect_DeliveryModel = new sap.ui.model.odata.ODataModel(oDirect_Delivery, true);
	        	          ooDirect_DeliveryModel.read("/EtPersonSet?$format=json",{
	        	                  success:function(data){
	        	                 	    var array = data.results;
	        								var retailermodel_Direct = new sap.ui.model.json.JSONModel();
	        								retailermodel_Direct.setData(array);
	        								oMasterView.getView().setModel(retailermodel_Direct,"tableDisplayModel");
	        	                },
	        	                  error:function(){
	        	                   sap.m.MessageToast.show("Record Update Error!", '', "Error");
	        	                }
	        	          });
	        		  },
	        		  function(err){
	        			  alert("Create Error");
	        		  }  
	        	  );
		}
	    )
		},
		onNavBack : function(){
			oMasterView1.onClear();
			window.history.go(-1);
		}

	});
});
