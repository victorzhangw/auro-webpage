DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();

const gaReporturl = GoogleremoteUrl + 'CustomGAReport';
var ecKey, gaKey, dp, L10n = ej.base.L10n;
const prefix = sessionStorage.getItem("sitePrefix") + "-";
const chartitems = (document.getElementById('dataChart'));
const appName = sessionStorage.getItem("appName");
const GA_Dimensions1 = ["adwordsCampaignID", "campaign"];
const GA_Metrics1 = ["adCost", "adClicks", "impressions", "CPM", "CPC", "CTR", "goal7Completions"];
const GA_Dimensions2 = ["date"];
const GA_Metrics2 = ["adCost", "adClicks", "impressions", "CPM", "CPC", "CTR", "goal7Completions"];
const GA_Dimensions3 = ["adGroup", "keyword"];
const GA_Metrics3 = ["adCost", "adClicks", "impressions", "CPM", "CPC", "CTR", "goal7Completions"];
const gridColumn = [
  {
    field: 'adCost',
    width: 100,
    textAlign: 'left',
    headerText: '廣告成本',
    format: 'N'
  },
  {
    field: 'adClicks',
    headerText: '廣告點擊',
    textAlign: 'left',
    width: 70,
    format: 'N'
  },
  {
    field: 'impressions',
    headerText: '曝光',
    textAlign: 'left',
    width: 70,
    format: 'N'
  },
  {
    field: 'CPM',
    headerText: 'CPM',
    textAlign: 'left',
    width: 70,
    format: 'N'
  },
  {
    field: 'CPC',
    headerText: 'CPC',
    textAlign: 'left',
    width: 70,
    format: 'N'
  },
  {
    field: 'CTR',
    headerText: 'CTR',
    textAlign: 'left',
    width: 70,
    format: 'N'
  }
];
let para = {
  "MemberKey": memberKey,
  "StartDate": sessionStorage.getItem(defaultStartDate),
  "EndDate": sessionStorage.getItem(defaultEndDate),
  "LoginId": loginID,
  "ViewId": sessionStorage.getItem("ViewId"),
  "ReqDimension1": GA_Dimensions1.join(),
  "ReqMetric1": GA_Metrics1.join(),
  "ReqDimension2": GA_Dimensions2.join(),
  "ReqMetric2": GA_Metrics2.join(),
  "ReqDimension3": GA_Dimensions3.join(),
  "ReqMetric3": GA_Metrics3.join()

};
$(document).ready(function () {
  let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
  dp = DramaCore.createDatePicker(dt, dt);

  DramaCore.validateTokenCall(validateSetting());

  let para = {
    "MemberKey": memberKey,
    "StartDate": sessionStorage.getItem(defaultStartDate),
    "EndDate": sessionStorage.getItem(defaultEndDate),
    "LoginId": loginID,
    "ViewId": sessionStorage.getItem("ViewId"),
    "ReqDimension1": GA_Dimensions1.join(),
    "ReqMetric1": GA_Metrics1.join(),
    "ReqDimension2": GA_Dimensions2.join(),
    "ReqMetric2": GA_Metrics2.join(),
    "ReqDimension3": GA_Dimensions3.join(),
    "ReqMetric3": GA_Metrics3.join()

  };

  if (sessionStorage.getItem(defaultStartDate) && sessionStorage.getItem(defaultEndDate) && sessionStorage.getItem(daySpan)) {
    dp.value = [sessionStorage.getItem(defaultStartDate), sessionStorage.getItem(defaultEndDate)];
    adKey = prefix + "gaad-" + sessionStorage.getItem(defaultStartDate) + "-" + sessionStorage.getItem(defaultEndDate) + "-" + sessionStorage.getItem(daySpan);
    console.log("adkey0", adKey)
    if (localStorage.getItem(adKey)) {

      initDefaultECPage.chart();
      DramaCore.renderHAMIGAECChart(adKey);
    } else {
      let type = "post";

      global_gaad_Key = DramaCore.getRemoteData(gaReporturl, type, para, adKey, fromEC);

      let timeoutID = window.setInterval(function () {
        console.log("global", global_gaad_Key)
        if (localStorage.getItem(adKey)) {
          initDefaultECPage.chart();
          DramaCore.renderHAMIGAECChart(adKey);
          global_gaad_Key = "";
          clearInterval(timeoutID);
          return;
        }
      }, 200);
    }
  }
  dp.addEventListener("change", function () {
    console.log("changed")
    let dayRange = this.getSelectedRange();
    if (dayRange.daySpan > 0) {
      let _sd = moment(this.startDate).format("YYYY-MM-DD");
      let _ed = moment(this.endDate).format("YYYY-MM-DD");
      let dsd = sessionStorage.getItem(defaultStartDate);
      let ded = sessionStorage.getItem(defaultEndDate);
      console.log(_sd);
      console.log(_ed);
      console.log(dayRange.daySpan);
      sessionStorage.setItem(defaultStartDate, _sd);
      sessionStorage.setItem(defaultEndDate, _ed);
      sessionStorage.setItem('daySpan', dayRange.daySpan);
      let type = "post";
      let para = {
        "MemberKey": memberKey,
        "StartDate": _sd,
        "EndDate": _ed,
        "LoginId": loginID,
        "ViewId": sessionStorage.getItem("ViewId"),
        "ReqDimension1": GA_Dimensions1.join(),
        "ReqMetric1": GA_Metrics1.join(),
        "ReqDimension2": GA_Dimensions2.join(),
        "ReqMetric2": GA_Metrics2.join(),
        "ReqDimension3": GA_Dimensions3.join(),
        "ReqMetric3": GA_Metrics3.join()

      };
      if (_sd && _ed) {
        let newadKey = prefix + "gaad-" + _sd + "-" + _ed + "-" + dayRange.daySpan;

        if (localStorage.getItem(newadKey)) {
          initDefaultECPage.chart();
          DramaCore.renderHAMIGAECChart(newadKey);
        } else {
          let type = "post";
          console.log("change new ad key", newadKey)
          global_gaad_Key = DramaCore.getRemoteData(gaReporturl, type, para, newadKey, fromEC);
          initDefaultECPage.chart();
          let timeoutID = window.setInterval(function () {

            if (localStorage.getItem(newadKey)) {

              initDefaultECPage.chart();
              DramaCore.renderHAMIGAECChart(newadKey);
              console.log("newadKey", newadKey)
              global_gaad_Key = "";
              clearInterval(timeoutID);
              return;
            }
          }, 200);
        }

      }

    }
  });

});
var initDefaultECPage = {
  widget: function () {

  },
  chart: function () {
    let chartMetaData = [
      { id: 'chart1', size: 'col-md-12' },
      { id: 'chart2', size: 'col-md-12' },
      { id: 'chart3', size: 'col-md-12' },
      { id: 'chart4', size: 'col-md-4' },
      { id: 'chart5', size: 'col-md-4' },
      { id: 'chart6', size: 'col-md-4' },
      { id: 'chart7', size: 'col-md-4' },
      { id: 'chart8', size: 'col-md-4' },
      { id: 'chart9', size: 'col-md-4' },
      { id: 'chart10', size: 'col-md-4' },
      { id: 'chart11', size: 'col-md-4' },
      { id: 'chart12', size: 'col-md-4' },

    ];
    let getChartString = ej.base.compile('<div class="col-sm-12 ${size} mb-1 d-flex"> <div id=${id} class="card card_animation flex-fill"> <div class="card-header widget-head"> </div><div class="charts card-body chart-height"> </div> </div>  </div>');
    while (chartitems.firstChild) {
      chartitems.removeChild(chartitems.firstChild);
    }
    chartMetaData.forEach(data => {
      chartitems.appendChild(getChartString(data)[0]);
    });
  }

};
var ECChart = {
  chart1: function (data, location, rowMax, header, chartid) {
    let chartObj = {},
      list = [],
      metricCPC = [],
      metricCPM = [],
      metricCTR = [],
      dimension = [];
    let dimGrid1 = {
      field: 'adwordsCampaignID',
      width: 100,
      textAlign: 'left',
      headerText: 'ADwords 活動Id',
      format: 'N'
    };
    let dimGrid2 = {
      field: 'campaign',
      width: 100,
      textAlign: 'left',
      headerText: '活動',
      format: 'N'
    };
    let chart1GridAry = gridColumn.slice();
    chart1GridAry.unshift(dimGrid1, dimGrid2)
    let report = data.ListResult;
    for (let i = 0; i < report.length; i++) {
      if (report[i].dimension[0] != "(not set)") {
        //report[i].dimension[0] --> 指定 Dimension 位置
        chartObj = {};
        chartObj.adwordsCampaignID = report[i].dimension[0];
        chartObj.campaign = report[i].dimension[1];
        chartObj.adCost = Number(report[i].metrics[0].metric[0].value);
        chartObj.adClicks = Number(report[i].metrics[0].metric[1].value);
        chartObj.impressions = Number(report[i].metrics[0].metric[2].value);
        chartObj.CPM = Number(report[i].metrics[0].metric[3].value);
        chartObj.CPC = Number(report[i].metrics[0].metric[4].value);
        chartObj.CTR = Number(report[i].metrics[0].metric[5].value);
        chartObj.goal7Completions = Number(report[i].metrics[0].metric[6].value)
        metricCPC.push(Number(report[i].metrics[0].metric[4].value));
        metricCPM.push(Number(report[i].metrics[0].metric[3].value));
        metricCTR.push(Number(report[i].metrics[0].metric[5].value));
        dimension.push(report[i].dimension[1]);
        for (let j = 0; j < report[i].metrics[0].metric.length; j++) {


        }
        list.push(chartObj);
      }


    }

    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart1GridAry,
      chartid: chartid
    };

    let echartsetting = {
      cardHead: "各廣告活動 CPC",
      titleText: "",
      value: metricCPC,
      category: dimension,
      color: ["#3259B8"],
      chartid: "chart4"
    };
    let echartsetting2 = {
      cardHead: "各廣告活動 CPM",
      titleText: "",
      value: metricCPM,
      category: dimension,
      color: ["#FB7507"],
      chartid: "chart5"
    };
    let echartsetting3 = {
      cardHead: "各廣告活動 CTR",
      titleText: "",
      value: metricCTR,
      category: dimension,
      color: ["#7ED321"],
      chartid: "chart6"
    };
    let chart = chartSetting.gridChart(setting);
    let option = chartSetting.singleBarChartwithAvgLine(echartsetting);
    let option2 = chartSetting.singleBarChartwithAvgLine(echartsetting2);
    let option3 = chartSetting.singleBarChartwithAvgLine(echartsetting3);
    let chart4 = echarts.init(document.getElementById("chart4").getElementsByClassName('card-body')[0]);
    let chart5 = echarts.init(document.getElementById("chart5").getElementsByClassName('card-body')[0]);
    let chart6 = echarts.init(document.getElementById("chart6").getElementsByClassName('card-body')[0]);
    if (option && typeof option === "object") {

      chart4.setOption(option, true);
      chart5.setOption(option2, true);
      chart6.setOption(option3, true);
      $("#chart4").children(".card-header").text(echartsetting.cardHead);
      $("#chart5").children(".card-header").text(echartsetting2.cardHead);
      $("#chart6").children(".card-header").text(echartsetting3.cardHead);

    }
    chart.appendTo('#chart1 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart2: function (data, location, rowMax, header, chartid) {

    let chartObj = {},
      list = [],
      metricCPC = [],
      metricCPM = [],
      metricCTR = [],
      dimension = [];
    let dimGrid1 = {
      field: 'date',
      width: 100,
      textAlign: 'left',
      headerText: '日期',
      format: 'N'
    };

    let chart1GridAry = gridColumn.slice();
    chart1GridAry.unshift(dimGrid1)
    let report = data.ListResult;


    for (let i = 0; i < report.length; i++) {
      if (report[i].dimension[0] != "(not set") {

        chartObj = {};
        chartObj.date = report[i].dimension[0];
        chartObj.adCost = Number(report[i].metrics[0].metric[0].value);
        chartObj.adClicks = Number(report[i].metrics[0].metric[1].value);
        chartObj.impressions = Number(report[i].metrics[0].metric[2].value);
        chartObj.CPM = Number(report[i].metrics[0].metric[3].value);
        chartObj.CPC = Number(report[i].metrics[0].metric[4].value);
        chartObj.CTR = Number(report[i].metrics[0].metric[5].value);
        chartObj.goal7Completions = Number(report[i].metrics[0].metric[6].value)
        metricCPC.push(Number(report[i].metrics[0].metric[4].value));
        metricCPM.push(Number(report[i].metrics[0].metric[3].value));
        metricCTR.push((Number(report[i].metrics[0].metric[5].value)));
        dimension.push(report[i].dimension[0]);
        for (let j = 0; j < report[i].metrics[0].metric.length; j++) {


        }
        list.push(chartObj);
      }


    }

    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart1GridAry,
      chartid: chartid
    };
    let echartsetting = {
      cardHead: "廣告活動 CPC 日報",
      titleText: "",
      value: metricCPC,
      category: dimension,
      color: ["#3259B8"],
      chartid: "chart4"
    };
    let echartsetting2 = {
      cardHead: "廣告活動 CPM 日報",
      titleText: "",
      value: metricCPM,
      category: dimension,
      color: ["#FB7507"],
      chartid: "chart5"
    };
    let echartsetting3 = {
      cardHead: "廣告活動 CTR 日報",
      titleText: "",
      value: metricCTR,
      category: dimension,
      color: ["#7ED321"],
      chartid: "chart6"
    };
    let chart = chartSetting.gridChart(setting);
    let option = chartSetting.singleBarChartwithAvgLine(echartsetting);
    let option2 = chartSetting.singleBarChartwithAvgLine(echartsetting2);
    let option3 = chartSetting.singleBarChartwithAvgLine(echartsetting3);
    let chart7 = echarts.init(document.getElementById("chart7").getElementsByClassName('card-body')[0]);
    let chart8 = echarts.init(document.getElementById("chart8").getElementsByClassName('card-body')[0]);
    let chart9 = echarts.init(document.getElementById("chart9").getElementsByClassName('card-body')[0]);
    if (option && typeof option === "object") {

      chart7.setOption(option, true);
      chart8.setOption(option2, true);
      chart9.setOption(option3, true);
      $("#chart7").children(".card-header").text(echartsetting.cardHead);
      $("#chart8").children(".card-header").text(echartsetting2.cardHead);
      $("#chart9").children(".card-header").text(echartsetting3.cardHead);

    }
    chart.appendTo('#chart2 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart3: function (data, location, rowMax, header, chartid) {
    let chartObj = {},
      list = [],
      metricCPC = [],
      metricCPM = [],
      metricCTR = [],
      dimension = [];
    let dimGrid1 = {
      field: 'adGroup',
      width: 100,
      textAlign: 'left',
      headerText: '廣告群組',
      format: 'N'
    };

    let chart1GridAry = gridColumn.slice();
    chart1GridAry.unshift(dimGrid1)
    let report = data.ListResult;

    for (let i = 0; i < report.length; i++) {
      if (report[i].dimension[0] != "(not set)") {

        chartObj = {};
        chartObj.adGroup = report[i].dimension[0];
        chartObj.keyword = report[i].dimension[1];
        chartObj.adCost = Number(report[i].metrics[0].metric[0].value);
        chartObj.adClicks = Number(report[i].metrics[0].metric[1].value);
        chartObj.impressions = Number(report[i].metrics[0].metric[2].value);
        chartObj.CPM = Number(report[i].metrics[0].metric[3].value);
        chartObj.CPC = Number(report[i].metrics[0].metric[4].value);
        chartObj.CTR = Number(report[i].metrics[0].metric[5].value);
        chartObj.goal7Completions = Number(report[i].metrics[0].metric[6].value)
        metricCPC.push(Number(report[i].metrics[0].metric[4].value));
        metricCPM.push(Number(report[i].metrics[0].metric[3].value));
        metricCTR.push(Number(report[i].metrics[0].metric[5].value));
        dimension.push(report[i].dimension[0]);
        for (let j = 0; j < report[i].metrics[0].metric.length; j++) {

        }
        list.push(chartObj);

      }


    }



    let setting = {
      cardHead: header,
      titleText: "",
      value: list,
      color: "",
      gridColumn: chart1GridAry,
      chartid: chartid
    };
    let echartsetting = {
      cardHead: "各廣告群組 CPC",
      titleText: "",
      value: metricCPC,
      category: dimension,
      color: ["#3259B8"],
      chartid: "chart10"
    };
    let echartsetting2 = {
      cardHead: "各廣告群組 CPM",
      titleText: "",
      value: metricCPM,
      category: dimension,
      color: ["#FB7507"],
      chartid: "chart11"
    };
    let echartsetting3 = {
      cardHead: "各廣告群組 CTR",
      titleText: "",
      value: metricCTR,
      category: dimension,
      color: ["#7ED321"],
      chartid: "chart12"
    };
    let chart = chartSetting.gridChart(setting);
    let option = chartSetting.singleBarChartwithAvgLine(echartsetting);
    let option2 = chartSetting.singleBarChartwithAvgLine(echartsetting2);
    let option3 = chartSetting.singleBarChartwithAvgLine(echartsetting3);
    let chart10 = echarts.init(document.getElementById("chart10").getElementsByClassName('card-body')[0]);
    let chart11 = echarts.init(document.getElementById("chart11").getElementsByClassName('card-body')[0]);
    let chart12 = echarts.init(document.getElementById("chart12").getElementsByClassName('card-body')[0]);
    if (option && typeof option === "object") {

      chart10.setOption(option, true);
      chart11.setOption(option2, true);
      chart12.setOption(option3, true);
      $("#chart10").children(".card-header").text(echartsetting.cardHead);
      $("#chart11").children(".card-header").text(echartsetting2.cardHead);
      $("#chart12").children(".card-header").text(echartsetting3.cardHead);

    }
    chart.appendTo('#chart3 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart5: function (data, location, rowMax, header, chartid) {

  },

}
function validateSetting() {
  let bearerToken = sessionStorage.getItem("token");
  let urlTo = AuthremoteUrl + "GetUserName";
  let Setting = {
    async: true,
    type: "GET",
    url: urlTo,
    headers: {
      Authorization: "Bearer" + " " + bearerToken

    }
  };
  return Setting;
}
