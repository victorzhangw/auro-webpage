DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();
var L10n = ej.base.L10n;
const chartitems = (document.getElementById('dataChart'));
const pointRange = [
  { 'id': 0, 'minpoint': 0, 'maxpoint': 0 },
  { 'id': 1, 'minpoint': 1, 'maxpoint': 100 },
  { 'id': 2, 'minpoint': 101, 'maxpoint': 200 },
  { 'id': 3, 'minpoint': 201, 'maxpoint': 300 },
  { 'id': 4, 'minpoint': 301, 'maxpoint': 400 },
  { 'id': 5, 'minpoint': 401, 'maxpoint': 500 },
  { 'id': 6, 'minpoint': 501, 'maxpoint': 600 },
  { 'id': 7, 'minpoint': 601, 'maxpoint': 700 },
  { 'id': 8, 'minpoint': 701, 'maxpoint': 800 },
  { 'id': 9, 'minpoint': 801, 'maxpoint': 900 },
  { 'id': 10, 'minpoint': 901, 'maxpoint': 1000 },
  { 'id': 11, 'minpoint': 1001, 'maxpoint': 1100 },
  { 'id': 12, 'minpoint': 1101, 'maxpoint': 1200 },
  { 'id': 13, 'minpoint': 1201, 'maxpoint': 1300 },
  { 'id': 14, 'minpoint': 1301, 'maxpoint': 1400 },
  { 'id': 15, 'minpoint': 1401, 'maxpoint': 1500 },
  { 'id': 16, 'minpoint': 1501, 'maxpoint': 1600 },
  { 'id': 17, 'minpoint': 1601, 'maxpoint': 1700 },
  { 'id': 18, 'minpoint': 1701, 'maxpoint': 1800 },
  { 'id': 19, 'minpoint': 1801, 'maxpoint': 1900 },
  { 'id': 20, 'minpoint': 1901, 'maxpoint': 2000 },
  { 'id': 21, 'minpoint': 2001, 'maxpoint': 999999999 },

];

var customerpoints = [
  {
    "RANGERANK": -1,
    "RANGERANKDESC": '未持有點數',
    "RANGECOUNT": 28001,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },

  {
    "RANGERANK": 0,
    "RANGERANKDESC": '0',
    "RANGECOUNT": 8536,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 1,
    "RANGERANKDESC": '1-100',
    "RANGECOUNT": 11725,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 2,
    "RANGERANKDESC": "101-200",
    "RANGECOUNT": 2006,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 3,
    "RANGERANKDESC": "201-300",
    "RANGECOUNT": 1193,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 4,
    "RANGERANKDESC": "301-400",
    "RANGECOUNT": 782,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 5,
    "RANGERANKDESC": "401-500",
    "RANGECOUNT": 1282,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 6,
    "RANGERANKDESC": "501-600",
    "RANGECOUNT": 1197,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 7,
    "RANGERANKDESC": "601-700",
    "RANGECOUNT": 656,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 8,
    "RANGERANKDESC": "701-800",
    "RANGECOUNT": 387,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 9,
    "RANGERANKDESC": "801-900",
    "RANGECOUNT": 329,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 10,
    "RANGERANKDESC": "901-1000",
    "RANGECOUNT": 635,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },

  {
    "RANGERANK": 11,
    "RANGERANKDESC": "1001-1100",
    "RANGECOUNT": 686,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 12,
    "RANGERANKDESC": "1101-1200",
    "RANGECOUNT": 269,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 13,
    "RANGERANKDESC": "1201-1300",
    "RANGECOUNT": 241,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 14,
    "RANGERANKDESC": "1301-1400",
    "RANGECOUNT": 136,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 15,
    "RANGERANKDESC": "1401-1500",
    "RANGECOUNT": 264,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 16,
    "RANGERANKDESC": "1501-1600",
    "RANGECOUNT": 261,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 17,
    "RANGERANKDESC": "1601-1700",
    "RANGECOUNT": 108,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 18,
    "RANGERANKDESC": "1701-1800",
    "RANGECOUNT": 152,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 19,
    "RANGERANKDESC": "1801-1900",
    "RANGECOUNT": 178,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 20,
    "RANGERANKDESC": "1901-2000",
    "RANGECOUNT": 204,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "RANGERANK": 21,
    "RANGERANKDESC": "2001-",
    "RANGECOUNT": 1970,
    "GRADESUM": 0,
    "GRADEAVG": 0
  }

];
var customerpointsAry = [[
  [53859, 0, "0"],
  [16421, 1806310, "1-100"],
  [2636, 579920, "101-200"],
  [1272, 419760, "201-300"],
  [924, 406560, "301-400"],
  [1601, 880550, "401-500"],
  [1804, 1190640, "501-600"],
  [897, 690690, "601-700"],
  [464, 408320, "701-800"],
  [355, 351450, "801-900"],
  [801, 881100, "901-1000"],
  [621, 751410, "1001-1100"],
  [99792, 131725440, "1101-1200"],
  [214, 306020, "1201-1300"],
  [171, 263340, "1301-1400"],
  [347, 572550, "1401-1500"],
  [242, 425920, "1501-1600"],
  [127, 237490, "1601-1700"],
  [142, 281160, "1701-1800"],
  [113, 236170, "1801-1900"],
  [230, 506000, "1901-2000"],
  [1788, 4130280, "2001-"]

]];
var weekcustomerpoints = [
  {
    "WEEKRANGE": "第一週",
    "WEEKRANGEDESC": "2021-11-08 ~ 2021-11-14",
    "WEEKRANGECOUNT": 33197,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "WEEKRANGE": "第二週",
    "WEEKRANGEDESC": "2021-11-01 ~ 2021-09-07",
    "WEEKRANGECOUNT": 35265,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "WEEKRANGE": "第三週",
    "WEEKRANGEDESC": "2021-10-25 ~ 2021-10-31",
    "WEEKRANGECOUNT": 24315,
    "GRADESUM": 0,
    "GRADEAVG": 0
  },
  {
    "WEEKRANGE": "第四週",
    "WEEKRANGEDESC": "2021-10-18 ~ 2021-10-24",
    "WEEKRANGECOUNT": 25107,
    "GRADESUM": 0,
    "GRADEAVG": 0
  }
]
var weekcustomerpoints2 = [
  {
    "MEMBERRANGE": -1,
    "MEMBERCOUNT1": 28001,
    "MEMBERCOUNT2": 28066,
    "MEMBERCOUNT3": 8528,
    "MEMBERCOUNT4": 1443
  },
  {
    "MEMBERRANGE": 0,
    "MEMBERCOUNT1": 8536,
    "MEMBERCOUNT2": 9087,
    "MEMBERCOUNT3": 6669,
    "MEMBERCOUNT4": 7749
  },
  {
    "MEMBERRANGE": 1,
    "MEMBERCOUNT1": 11725,
    "MEMBERCOUNT2": 12250,
    "MEMBERCOUNT3": 8047,
    "MEMBERCOUNT4": 9045
  },
  {
    "MEMBERRANGE": 2,
    "MEMBERCOUNT1": 2006,
    "MEMBERCOUNT2": 2445,
    "MEMBERCOUNT3": 1347,
    "MEMBERCOUNT4": 1455
  },
  {
    "MEMBERRANGE": 3,
    "MEMBERCOUNT1": 1193,
    "MEMBERCOUNT2": 1089,
    "MEMBERCOUNT3": 875,
    "MEMBERCOUNT4": 780
  },
  {
    "MEMBERRANGE": 4,
    "MEMBERCOUNT1": 782,
    "MEMBERCOUNT2": 718,
    "MEMBERCOUNT3": 595,
    "MEMBERCOUNT4": 475
  },
  {
    "MEMBERRANGE": 5,
    "MEMBERCOUNT1": 1282,
    "MEMBERCOUNT2": 1269,
    "MEMBERCOUNT3": 1060,
    "MEMBERCOUNT4": 781
  },
  {
    "MEMBERRANGE": 6,
    "MEMBERCOUNT1": 1197,
    "MEMBERCOUNT2": 1884,
    "MEMBERCOUNT3": 1021,
    "MEMBERCOUNT4": 784
  },
  {
    "MEMBERRANGE": 7,
    "MEMBERCOUNT1": 656,
    "MEMBERCOUNT2": 1090,
    "MEMBERCOUNT3": 495,
    "MEMBERCOUNT4": 429
  },
  {
    "MEMBERRANGE": 8,
    "MEMBERCOUNT1": 387,
    "MEMBERCOUNT2": 434,
    "MEMBERCOUNT3": 219,
    "MEMBERCOUNT4": 184
  },
  {
    "MEMBERRANGE": 9,
    "MEMBERCOUNT1": 329,
    "MEMBERCOUNT2": 273,
    "MEMBERCOUNT3": 219,
    "MEMBERCOUNT4": 150
  },
  {
    "MEMBERRANGE": 10,
    "MEMBERCOUNT1": 635,
    "MEMBERCOUNT2": 639,
    "MEMBERCOUNT3": 523,
    "MEMBERCOUNT4": 473
  },
  {
    "MEMBERRANGE": 11,
    "MEMBERCOUNT1": 686,
    "MEMBERCOUNT2": 629,
    "MEMBERCOUNT3": 526,
    "MEMBERCOUNT4": 443
  },
  {
    "MEMBERRANGE": 12,
    "MEMBERCOUNT1": 269,
    "MEMBERCOUNT2": 296,
    "MEMBERCOUNT3": 173,
    "MEMBERCOUNT4": 167
  },
  {
    "MEMBERRANGE": 13,
    "MEMBERCOUNT1": 241,
    "MEMBERCOUNT2": 192,
    "MEMBERCOUNT3": 133,
    "MEMBERCOUNT4": 129
  },
  {
    "MEMBERRANGE": 14,
    "MEMBERCOUNT1": 136,
    "MEMBERCOUNT2": 132,
    "MEMBERCOUNT3": 80,
    "MEMBERCOUNT4": 101
  },
  {
    "MEMBERRANGE": 15,
    "MEMBERCOUNT1": 264,
    "MEMBERCOUNT2": 245,
    "MEMBERCOUNT3": 235,
    "MEMBERCOUNT4": 177
  },
  {
    "MEMBERRANGE": 16,
    "MEMBERCOUNT1": 261,
    "MEMBERCOUNT2": 258,
    "MEMBERCOUNT3": 198,
    "MEMBERCOUNT4": 183
  },
  {
    "MEMBERRANGE": 17,
    "MEMBERCOUNT1": 108,
    "MEMBERCOUNT2": 123,
    "MEMBERCOUNT3": 91,
    "MEMBERCOUNT4": 73
  },
  {
    "MEMBERRANGE": 18,
    "MEMBERCOUNT1": 152,
    "MEMBERCOUNT2": 147,
    "MEMBERCOUNT3": 115,
    "MEMBERCOUNT4": 97
  },
  {
    "MEMBERRANGE": 19,
    "MEMBERCOUNT1": 178,
    "MEMBERCOUNT2": 157,
    "MEMBERCOUNT3": 89,
    "MEMBERCOUNT4": 99
  },
  {
    "MEMBERRANGE": 20,
    "MEMBERCOUNT1": 204,
    "MEMBERCOUNT2": 173,
    "MEMBERCOUNT3": 158,
    "MEMBERCOUNT4": 132
  },
  {
    "MEMBERRANGE": 21,
    "MEMBERCOUNT1": 1970,
    "MEMBERCOUNT2": 1735,
    "MEMBERCOUNT3": 1447,
    "MEMBERCOUNT4": 1201
  }
];
var weekcustomerpointsAry = [[
  [3050, 2358, '第一週'], [17217, 15505, '第二週'], [21562, 16898, '第三週'], [1264372, 136273, '第四週'], [30627, 13787, '本週目前']
]];
var tlcustomerpoints = [
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "ASUS",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 4
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "HTC",
    "OS": "ANDROID",
    "Location": "台中市",
    "cfPoint": 2
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "HTC",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 6
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "KOOBEE",
    "OS": "ANDROID",
    "Location": "新北市",
    "cfPoint": 1
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "LG",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 3
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "OPPO",
    "OS": "ANDROID",
    "Location": "台中市",
    "cfPoint": 1
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "OPPO",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 15
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "OPPO",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 2
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "SAMSUNG",
    "OS": "ANDROID",
    "Location": "彰化縣",
    "cfPoint": 1
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "SAMSUNG",
    "OS": "ANDROID",
    "Location": "苗栗縣",
    "cfPoint": 1
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "SAMSUNG",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 5
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "SAMSUNG",
    "OS": "ANDROID",
    "Location": "桃園市",
    "cfPoint": 1
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "SONY",
    "OS": "ANDROID",
    "Location": "台中市",
    "cfPoint": 1
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "SONY",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 10
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "SONY",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 1
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "XIAOMI",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 5
  },
  {
    "BrowserName": "CHROME MOBILE",
    "DeviceLogo": "",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 1
  },
  {
    "BrowserName": "SAFARI",
    "DeviceLogo": "APPLE",
    "OS": "IOS",
    "Location": "台中市",
    "cfPoint": 1
  },
  {
    "BrowserName": "SAFARI",
    "DeviceLogo": "APPLE",
    "OS": "IOS",
    "Location": "台北市",
    "cfPoint": 12
  },
  {
    "BrowserName": "SAFARI",
    "DeviceLogo": "APPLE",
    "OS": "IOS",
    "Location": "桃園市",
    "cfPoint": 1
  },
  {
    "BrowserName": "SAMSUNG BROWSER",
    "DeviceLogo": "SAMSUNG",
    "OS": "ANDROID",
    "Location": "南投縣",
    "cfPoint": 1
  },
  {
    "BrowserName": "SAMSUNG BROWSER",
    "DeviceLogo": "SAMSUNG",
    "OS": "ANDROID",
    "Location": "台北市",
    "cfPoint": 26
  }
];
var tlcustomerpoints2 = [
  {
    "RANGERANK": "1",
    "REFFEROR": "Google /Organic",
    "DATERANGE": "2021/10/17-2021/10/24",
    "ConsumePoints": "101",
  },
  {
    "RANGERANK": "15",
    "REFFEROR": "HAMIPAY",
    "DATERANGE": "2021/10/17-2021/10/24",
    "ConsumePoints": "2401",
  },
  {
    "RANGERANK": "10",
    "REFFEROR": "Google /Organic",
    "DATERANGE": "2021/10/17-2021/10/24",
    "ConsumePoints": "357",
  },
  {
    "RANGERANK": "11",
    "REFFEROR": "HAMIPOINT",
    "DATERANGE": "2021/10/17-2021/10/24",
    "ConsumePoints": "101",
  },
  {
    "RANGERANK": "2",
    "REFFEROR": "Google /Organic",
    "DATERANGE": "2021/10/17-2021/10/24",
    "ConsumePoints": "101",
  }
];
var piedata1 = [
  {
    "value": "2250",
    "name": "北部地區"

  },
  {
    "value": "1140",
    "name": "中部地區"

  },
  {
    "value": "1768",
    "name": "南部地區"

  }, {
    "value": "34",
    "name": "東部地區"

  },
];
var piedata2 = [
  {
    "value": "781",
    "name": "IOS"

  },
  {
    "value": "1140",
    "name": "ANDROID"

  },
  {
    "value": "1768",
    "name": "WINDOW"

  },
  {
    "value": "220",
    "name": "MACOS"

  },
  {
    "value": "34",
    "name": "OTHERS"

  }
];
const gridColumn1 = [
  {
    field: 'RANGERANK',
    width: 20,
    textAlign: 'left',
    headerText: '級距編號',
    format: 'N'
  },
  {
    field: 'RANGERANKDESC',
    headerText: '點數區間',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'RANGECOUNT',
    headerText: '區間人次',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'GRADESUM',
    headerText: '區間點持有數合計',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'GRADEAVG',
    headerText: '平均持有點數',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  }

];
const gridColumn2 = [
  {
    field: 'WEEKRANGE',
    width: 20,
    textAlign: 'left',
    headerText: '週次',
    format: 'N'
  },
  {
    field: 'WEEKRANGEDESC',
    headerText: '日期區間',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'WEEKRANGECOUNT',
    headerText: '區間人次',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'GRADESUM',
    headerText: '區間點數合計',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'GRADEAVG',
    headerText: '平均持有點數',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  }

];
const gridColumn3 = [
  {
    field: 'BrowserName',
    width: 20,
    textAlign: 'left',
    headerText: '瀏覽器',
    format: 'N'
  },
  {
    field: 'DeviceLogo',
    headerText: '品牌',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'OS',
    headerText: '作業系統',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'Location',
    headerText: '地理位置',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'cfPoint',
    headerText: '擁有點數',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  }

];
const gridColumn4 = [
  {
    field: 'RANGERANK',
    width: 20,
    textAlign: 'left',
    headerText: '級距編號',
    format: 'N'
  },
  {
    field: 'OS',
    headerText: '日期區間',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'ConsumePoints',
    headerText: '消耗點數',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  }



];
const gridColumn5 = [
  {
    field: 'RANGERANK',
    width: 20,
    textAlign: 'left',
    headerText: '級距編號',
    format: 'N'
  },
  {
    field: 'REFFEROR',
    width: 20,
    textAlign: 'left',
    headerText: '來源參考',
    format: 'N'
  },
  {
    field: 'DATERANGE',
    headerText: '日期區間',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'ConsumePoints',
    headerText: '消耗點數',
    textAlign: 'right',
    width: 50,
    format: 'N',
    visible: true
  }



];
const gridColumn6 = [
  {
    field: 'MEMBERRANGE',
    width: 20,
    textAlign: 'left',
    headerText: '級距編號',
    format: 'N'
  },
  {
    field: 'MEMBERCOUNT1',
    width: 20,
    textAlign: 'center',
    headerText: '第一週',
    format: 'N'
  },
  {
    field: 'MEMBERCOUNT2',
    headerText: '第二週',
    textAlign: 'center',
    width: 50,
    format: 'N',
    visible: true
  },
  {
    field: 'MEMBERCOUNT3',
    headerText: '第二週',
    textAlign: 'center',
    width: 50,
    format: 'N',
    visible: true
  }, {
    field: 'MEMBERCOUNT4',
    headerText: '第四週',
    textAlign: 'center',
    width: 50,
    format: 'N',
    visible: true
  }



];
var initPointsPage = {

  chart: function () {
    let chartMetaData = [
      { id: 'chart1', size: 'col-md-12' },
      { id: 'chart2', size: 'col-md-12' },
      { id: 'chart3', size: 'col-md-12' },
      { id: 'chart4', size: 'col-md-12' },
      { id: 'chart5', size: 'col-md-12' },
      { id: 'chart6', size: 'col-md-6' },
      { id: 'chart7', size: 'col-md-6' },
      { id: 'chart8', size: 'col-md-6' },
      { id: 'chart9', size: 'col-md-6' }




    ];
    let getChartString = ej.base.compile('<div class="col-sm-12 ${size} mb-1 d-flex"> <div id=${id} class="card card_animation flex-fill"> <div class="card-header widget-head"> </div><div class="charts card-body chart-height"> </div> </div>  </div>');
    while (chartitems.firstChild) {
      chartitems.removeChild(chartitems.firstChild);
    }
    chartMetaData.forEach(data => {
      chartitems.appendChild(getChartString(data)[0]);
    });
  },


};
var PointsCharts = {

  chart1: function (data, location, rowMax, header, chartid) {
    let setting = {
      cardHead: header,
      titleText: "",
      value: customerpoints,
      color: "",
      allowResizing: true,
      gridColumn: gridColumn1,
      chartid: 'chart1'
    };
    let chart = chartSetting.gridChart(setting);
    //let chart2 = chartSetting.doubleBarChartVertical(setting2);

    chart.appendTo('#chart1 .card-body');
    $("#chart1").children(".card-header").text(setting.titleText);
  },
  chart2: function (data, location, rowMax, header, chartid) {
    let setting = {
      cardHead: header,
      titleText: "四週有點人數變化",
      value: weekcustomerpoints,
      color: "",
      allowResizing: true,
      gridColumn: gridColumn2,
      chartid: 'chart2'
    };
    let chart = chartSetting.gridChart(setting);
    //let chart2 = chartSetting.doubleBarChartVertical(setting2);

    chart.appendTo('#chart2 .card-body');
    $("#chart2").children(".card-header").text(setting.titleText);

  },
  chart3: function (data, location, rowMax, header, chartid) {
    let setting = {
      cardHead: header,
      titleText: "四週各會員級距人數統計",
      value: weekcustomerpoints2,
      color: "",
      allowResizing: true,
      gridColumn: gridColumn6,
      chartid: 'chart3'
    };
    let chart = chartSetting.gridChart(setting);
    //let chart2 = chartSetting.doubleBarChartVertical(setting2);

    chart.appendTo('#chart3 .card-body');
    $("#chart3").children(".card-header").text(setting.titleText);

  },
  chart4: function (data, location, rowMax, header, chartid) {
    let setting = {
      cardHead: header,
      titleText: "",
      value: tlcustomerpoints,
      color: "",
      allowResizing: true,
      gridColumn: gridColumn3,
      chartid: 'chart4'
    };
    let chart = chartSetting.gridChart(setting);
    //let chart2 = chartSetting.doubleBarChartVertical(setting2);

    chart.appendTo('#chart4 .card-body');

  },
  chart5: function (data, location, rowMax, header, chartid) {
    let setting = {
      cardHead: header,
      titleText: "來源媒介點數消耗",
      value: tlcustomerpoints2,
      color: "",
      allowResizing: true,
      gridColumn: gridColumn5,
      chartid: 'chart5'
    };
    let chart = chartSetting.gridChart(setting);
    //let chart2 = chartSetting.doubleBarChartVertical(setting2);

    chart.appendTo('#chart5 .card-body');

  },
  chart6: function (data, location, rowMax, header, chartid) {
    let params = {
      cardHead: '級距氣泡圖',
      title: '',
      name: '級距',
      xAxisName: '總人數',
      yAxisName: '總點數',
      data: customerpointsAry

    };
    let chart6 = echarts.init(document.getElementById("chart6").getElementsByClassName('card-body')[0]);

    let option = chartSetting.bubbleCahrt(params);
    console.log(option)
    if (option && typeof option === "object") {
      chart6.setOption(option, true);
      $("#chart6").children(".card-header").text(params.cardHead);
    }
  },
  chart7: function (data, location, rowMax, header, chartid) {
    let params = {
      cardHead: '周氣泡圖',
      title: '',
      name: '級距',
      xAxisName: '總點數',
      yAxisName: '總人數',
      data: weekcustomerpointsAry

    };
    let chart = echarts.init(document.getElementById("chart7").getElementsByClassName('card-body')[0]);

    let option = chartSetting.bubbleCahrt(params);

    if (option && typeof option === "object") {
      chart.setOption(option, true);
      $("#chart7").children(".card-header").text(params.cardHead);
    }
  },
  chart8: function (data, location, rowMax, header, chartid) {
    let params = {
      cardHead: '地域消耗點數佔比',
      title: '',
      name: '級距',
      colorPalette: ["#0097FB",
        "#92E1FF",
        "#FFC227",
        "#30ECA6",
        "#FDFA4E",
        "#FF4848"],
      seriesData: piedata1

    };
    let chart = echarts.init(document.getElementById("chart8").getElementsByClassName('card-body')[0]);

    let option = chartSetting.simplePie(params);

    if (option && typeof option === "object") {
      chart.setOption(option, true);
      $("#chart8").children(".card-header").text(params.cardHead);
    }
  },
  chart9: function (data, location, rowMax, header, chartid) {
    let params = {
      cardHead: '作業系統消耗點數佔比',
      title: '',
      name: '級距',
      colorPalette: ["#0097FB",
        "#92E1FF",
        "#FFC227",
        "#30ECA6",
        "#FDFA4E",
        "#FF4848"],
      yAxisName: '總人數',
      seriesData: piedata2

    };
    let chart = echarts.init(document.getElementById("chart9").getElementsByClassName('card-body')[0]);

    let option = chartSetting.simplePie(params);

    if (option && typeof option === "object") {
      chart.setOption(option, true);
      $("#chart9").children(".card-header").text(params.cardHead);
    }
  }


};

$(document).ready(function () {
  let sdt = moment().subtract(1, 'days').format("YY-MM-DD");
  let edt = moment().subtract(1, 'days').format("YY-MM-DD");
  dp = DramaCore.createDatePicker(sdt, edt);
  initPointsPage.chart();
  PointsCharts.chart1();
  PointsCharts.chart2();
  PointsCharts.chart3();
  PointsCharts.chart4();
  PointsCharts.chart5();
  PointsCharts.chart6();
  PointsCharts.chart7();
  PointsCharts.chart8();
  PointsCharts.chart9();

});


// url --> remote url ; type:post or get ; para:parameters

