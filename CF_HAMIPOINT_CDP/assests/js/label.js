var L10n = ej.base.L10n;
var isLabel = false; // check  label exist; if label is exist,the manual paging not work ! 
const ApiremoteUrl = sessionStorage.getItem("ApiremoteUrl");
DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();
const memberGridpageSize = 25;
var checkedValue = [];
window.countries =
    [
        {
            "Id": 1,

            "FullName": "會員輪廓", hasChild: true, expanded: true
        },
        {
            "Id": 2,

            "FullName": "會員行為", hasChild: true, expanded: true
        },
        {
            "Id": 3,

            "FullName": "會員價值", hasChild: true, expanded: true
        },
        {
            "Id": 4,

            "FullName": "興趣", hasChild: true, expanded: true
        },
        {
            "Id": 5,
            "ParentId": 1,
            "FullName": "人口屬性", hasChild: true, expanded: true
        },
        {
            "Id": 6,
            "ParentId": 1,
            "FullName": "會員資料", hasChild: true, expanded: true
        },
        {
            "Id": 7,
            "ParentId": 2,
            "FullName": "進站行為", hasChild: true, expanded: true
        },
        {
            "Id": 8,
            "ParentId": 2,
            "FullName": "瀏覽頁面分類", hasChild: true, expanded: true
        },
        {
            "Id": 9,
            "ParentId": 2,
            "FullName": "購買行為", hasChild: true, expanded: true
        },
        {
            "Id": 10,
            "ParentId": 2,
            "FullName": "關鍵字人群", hasChild: true, expanded: true
        },
        {
            "Id": 11,
            "ParentId": 3,
            "FullName": "價值分群", hasChild: true, expanded: true
        },
        {
            "Id": 12,
            "ParentId": 3,
            "FullName": "單價分群", hasChild: true, expanded: true
        },
        {
            "Id": 13,
            "ParentId": 3,
            "FullName": "回訪分群", hasChild: true, expanded: true
        },
        {
            "Id": 14,
            "ParentId": 4,
            "FullName": "精品時尚"
        },
        {
            "Id": 15,
            "ParentId": 4,
            "FullName": "美食飲品"
        },
        {
            "Id": 16,
            "ParentId": 4,
            "FullName": "交通運輸"
        },
        {
            "Id": 17,
            "ParentId": 4,
            "FullName": "減重健身"
        },
        {
            "Id": 18,
            "ParentId": 4,
            "FullName": "媒體與新聞時事"
        },
        {
            "Id": 19,
            "ParentId": 4,
            "FullName": "戶外旅遊"
        },
        {
            "Id": 20,
            "ParentId": 4,
            "FullName": "居家生活"
        },
        {
            "Id": 21,
            "ParentId": 4,
            "FullName": "運動體育"
        },
        {
            "Id": 22,
            "ParentId": 4,
            "FullName": "生活休閒"
        },
        {
            "Id": 23,
            "ParentId": 4,
            "FullName": "影視娛樂"
        },
        {
            "Id": 24,
            "ParentId": 4,
            "FullName": "購物消費"
        },
        {
            "Id": 25,
            "ParentId": 4,
            "FullName": "醫學美容"
        },
        {
            "Id": 26,
            "ParentId": 4,
            "FullName": "身體保健"
        },
        {
            "Id": 27,
            "ParentId": 4,
            "FullName": "寵物生活"
        },
        {
            "Id": 28,
            "ParentId": 4,
            "FullName": "嬰幼保健"
        },
        {
            "Id": 29,
            "ParentId": 4,
            "FullName": "法律相關"
        },
        {
            "Id": 30,
            "ParentId": 4,
            "FullName": "投資理財"
        },
        {
            "Id": 31,
            "ParentId": 4,
            "FullName": "科技資訊"
        },
        {
            "Id": 32,
            "ParentId": 4,
            "FullName": "公益"
        },
        {
            "Id": 33,
            "ParentId": 4,
            "FullName": "文化展覽"
        },
        {
            "Id": 34,
            "ParentId": 4,
            "FullName": "動漫電競"
        },
        {
            "Id": 35,
            "ParentId": 4,
            "FullName": "學習教育"
        },
        {
            "Id": 36,
            "ParentId": 4,
            "FullName": "房屋建築"
        },
        {
            "Id": 43,
            "ParentId": 5,
            "FullName": "性別", hasChild: true, expanded: true
        },
        {
            "Id": 44,
            "ParentId": 5,
            "FullName": "年齡", hasChild: true, expanded: true
        },
        {
            "Id": 45,
            "ParentId": 5,
            "FullName": "裝置", hasChild: true, expanded: true
        },
        {
            "Id": 46,
            "ParentId": 5,
            "FullName": "居住地", hasChild: true, expanded: true
        },
        {
            "Id": 47,
            "ParentId": 6,
            "FullName": "身份", hasChild: true, expanded: true
        },
        {
            "Id": 48,
            "ParentId": 6,
            "FullName": "會員點數級距", hasChild: true, expanded: true
        },
        {
            "Id": 49,
            "ParentId": 7,
            "FullName": "進站頻率", hasChild: true, expanded: true
        },
        {
            "Id": 50,
            "ParentId": 7,
            "FullName": "交易頻率", hasChild: true, expanded: true
        },
        {
            "Id": 51,
            "ParentId": 8,
            "FullName": "一週內曾造訪特別企劃分頁"
        },
        {
            "Id": 52,
            "ParentId": 8,
            "FullName": "一週內曾造訪限時下殺分頁"
        },
        {
            "Id": 53,
            "ParentId": 8,
            "FullName": "一週內曾造訪美式賣場分頁"
        },
        {
            "Id": 54,
            "ParentId": 8,
            "FullName": "一週內曾造訪福利品分頁"
        },
        {
            "Id": 55,
            "ParentId": 8,
            "FullName": "一週內曾造訪3M旗艦館分頁"
        },
        {
            "Id": 56,
            "ParentId": 8,
            "FullName": "一週內曾造訪HomeKit專區分頁"
        },
        {
            "Id": 57,
            "ParentId": 8,
            "FullName": "一週內曾造訪美食/生鮮分頁"
        },
        {
            "Id": 58,
            "ParentId": 8,
            "FullName": "一週內曾造訪3C家電分頁"
        },
        {
            "Id": 59,
            "ParentId": 8,
            "FullName": "一週內曾造訪生活百貨分頁"
        },
        {
            "Id": 60,
            "ParentId": 8,
            "FullName": "一週內曾造訪票券/商品卡分頁"
        },
        {
            "Id": 61,
            "ParentId": 9,
            "FullName": "購買商品類別"
        },
        {
            "Id": 62,
            "ParentId": 9,
            "FullName": "加入購物車"
        },
        {
            "Id": 63,
            "ParentId": 9,
            "FullName": "結帳"
        },
        {
            "Id": 64,
            "ParentId": 9,
            "FullName": "付款方式", hasChild: true, expanded: true
        },
        {
            "Id": 65,
            "ParentId": 9,
            "FullName": "運送方式", hasChild: true, expanded: true
        },
        {
            "Id": 66,
            "ParentId": 10,
            "FullName": "搜尋後有點擊任額商品"
        },
        {
            "Id": 67,
            "ParentId": 10,
            "FullName": "搜尋後無點擊任何商品"
        },
        {
            "Id": 68,
            "ParentId": 11,
            "FullName": "高價值"
        },
        {
            "Id": 69,
            "ParentId": 11,
            "FullName": "中價值"
        },
        {
            "Id": 70,
            "ParentId": 11,
            "FullName": "低價值"
        },
        {
            "Id": 71,
            "ParentId": 12,
            "FullName": "高單價"
        },
        {
            "Id": 72,
            "ParentId": 12,
            "FullName": "中單價"
        },
        {
            "Id": 73,
            "ParentId": 12,
            "FullName": "低單價"
        },
        {
            "Id": 74,
            "ParentId": 13,
            "FullName": "常回購：180天 5次"
        },
        {
            "Id": 75,
            "ParentId": 13,
            "FullName": "一般回購率：180天 3-5次"
        },
        {
            "Id": 76,
            "ParentId": 13,
            "FullName": "極少回購：180天 低於3次"
        },
        {
            "Id": 77,
            "ParentId": 13,
            "FullName": "不回購：180天 0次"
        },


        {
            "Id": 201,
            "ParentId": 43,
            "FullName": "男"
        },
        {
            "Id": 202,
            "ParentId": 43,
            "FullName": "女"
        },
        {
            "Id": 203,
            "ParentId": 44,
            "FullName": "0~15"
        },
        {
            "Id": 204,
            "ParentId": 44,
            "FullName": "15~24"
        },
        {
            "Id": 205,
            "ParentId": 44,
            "FullName": "25~34"
        },
        {
            "Id": 206,
            "ParentId": 44,
            "FullName": "35~44"
        },
        {
            "Id": 207,
            "ParentId": 44,
            "FullName": "45~54"
        },
        {
            "Id": 208,
            "ParentId": 44,
            "FullName": "55以上"
        },
        {
            "Id": 209,
            "ParentId": 45,
            "FullName": "PC"
        },
        {
            "Id": 210,
            "ParentId": 45,
            "FullName": "Mobile"
        },
        {
            "Id": 211,
            "ParentId": 45,
            "FullName": "Tablet"
        },
        {
            "Id": 212,
            "ParentId": 45,
            "FullName": "其他"
        },
        {
            "Id": 213,
            "ParentId": 45,
            "FullName": "未知"
        },
        {
            "Id": 214,
            "ParentId": 46,
            "FullName": "台灣地區"
        },
        {
            "Id": 215,
            "ParentId": 46,
            "FullName": "海外地區"
        },
        {
            "Id": 216,
            "ParentId": 47,
            "FullName": "是會員"
        },
        {
            "Id": 217,
            "ParentId": 47,
            "FullName": "不是會員"
        },
        {
            "Id": 218,
            "ParentId": 47,
            "FullName": "有點數會員"
        },
        {
            "Id": 219,
            "ParentId": 47,
            "FullName": "無點數會員"
        },
        {
            "Id": 220,
            "ParentId": 48,
            "FullName": "500點內"
        },
        {
            "Id": 221,
            "ParentId": 48,
            "FullName": "500~1000點"
        },
        {
            "Id": 222,
            "ParentId": 48,
            "FullName": "1001~1500點"
        },
        {
            "Id": 223,
            "ParentId": 48,
            "FullName": "500點～2000點"
        },
        {
            "Id": 224,
            "ParentId": 48,
            "FullName": "2000點以上"
        },
        {
            "Id": 225,
            "ParentId": 49,
            "FullName": "重度使用者：180天 5次以上"
        },
        {
            "Id": 226,
            "ParentId": 49,
            "FullName": "中度使用者：180天 3-5次"
        },
        {
            "Id": 227,
            "ParentId": 49,
            "FullName": "輕度使用者：180天 低於3次"
        },
        {
            "Id": 228,
            "ParentId": 50,
            "FullName": "近三個月有交易過的"
        },
        {
            "Id": 229,
            "ParentId": 50,
            "FullName": "近六個月有交易過的"
        },
        {
            "Id": 230,
            "ParentId": 50,
            "FullName": "近九個月有交易過的"
        },
        {
            "Id": 231,
            "ParentId": 50,
            "FullName": "近一年有交易過的"
        },
        {
            "Id": 232,
            "ParentId": 50,
            "FullName": "一年以上未購買"
        },
        {
            "Id": 233,
            "ParentId": 64,
            "FullName": "貨到付款"
        },
        {
            "Id": 234,
            "ParentId": 64,
            "FullName": "信用卡"
        },
        {
            "Id": 235,
            "ParentId": 64,
            "FullName": "超商代碼"
        },
        {
            "Id": 236,
            "ParentId": 65,
            "FullName": "宅配"
        },
        {
            "Id": 237,
            "ParentId": 65,
            "FullName": "超商取貨"
        },
        {
            "Id": 238,
            "ParentId": 66,
            "FullName": "依照搜尋關鍵字做成"
        },
        {
            "Id": 239,
            "ParentId": 66,
            "FullName": "依照搜尋後回報沒有商品做成"
        }

    ];

ej.grids.Grid.Inject(ej.grids.Page, ej.grids.Sort, ej.grids.Filter, ej.grids.Resize);
var tagArr = [],
    listViewData, customerData, acrdnObj, grid1;
listViewData = window.countries;
var dashboard = new ej.layouts.DashboardLayout({
    columns: 16,
    allowDragging: false,
    cellSpacing: [2, 2],
    cellAspectRatio: 100 / 95,
    panels: [{
        'sizeX': 5,
        'sizeY': 1,
        'row': 0,
        'col': 0,
        content: '<br><div id=""><button id="secondSearch">標籤集群</button><button id="clearLabel" >清除選取</button></div>'
    },
    {
        'sizeX': 5,
        'sizeY': 12,
        'row': 1,
        'col': 0,
        header: '<div>標籤選擇器</div>',
        content: '<div id="selector2" class="chart-responsive" ><table id="pg3chkTabel" class=""></table>'
    },
    {
        'sizeX': 11,
        'sizeY': 13,
        'row': 0,
        'col': 5,
        header: '<div>清單列表</div>',
        content: '<div id="grid1" class="chart-responsive" ></div>'
    }



    ]
});
const gridUrl = ApiremoteUrl + "Customer" + "/CustomerTagList";
var labelCate = {
    "Source": "labelCategory",
    "Clause": "",
    "Limit": "0",
    "Offset": "0"
};
var memberData = {
    "Source": "memberlabel",
    "Clause": "",
    "Limit": memberGridpageSize,
    "Offset": "0"
};
var renderComponet = function renderDashBoardComponent() {
    try {
        acrdnObj = new ej.navigations.Accordion({
            expandMode: 'Single',
            width: '93%',
            items: getAccordinItems()

        });

        acrdnObj.appendTo('#pg3chkTabel');
        acrdnObj.addEventListener("expanded", function (e) {
            let isEmpty = document.getElementById(e.item.cssClass).innerHTML == "";
            //console.log(isEmpty);
            let i = e.item.cssClass.match(/\d+/g);
            if (isEmpty) {
                document.getElementById(e.item.cssClass).innerHTML = "<i></i>";
                //console.log(document.getElementById(e.item.cssClass));
                window['chklistObj' + i].appendTo('#' + e.item.cssClass);
            }

        });


        grid1 = new ej.grids.Grid({
            dataSource: customerData,
            //actionComplete: sort,
            allowExcelExport: true,
            toolbar: [{ text: '下載 CSV', tooltipText: 'Download CSV', prefixIcon: "e-csvexport", id: 'CsvExport' }, { text: '下載 Excel', tooltipText: 'Download excel', prefixIcon: "e-excelexport", id: 'ExcelExport' }],
            //toolbar: ["CsvExport"],
            Offline: true,
            columns: [
                {
                    field: 'tags',
                    headerText: '標籤',
                    textAlign: 'left',
                    width: 30,
                    format: 'string',
                    visible: true,
                    valueAccessor: tagsFormatter,
                    disableHtmlEncode: false

                },
                {
                    field: 'cftp',
                    headerText: 'CFTP 編號',
                    textAlign: 'left',
                    width: 40,
                    type: 'string'
                },
                {
                    field: 'cftuid',
                    width: 40,
                    headerText: 'CFTUID 編號',
                    type: 'string'
                },

                {
                    field: 'grade',
                    headerText: '點數級距',
                    textAlign: 'right',
                    width: 10,
                    format: 'string'
                },




            ],
            rowHeight: 28,
            height: "100%",
            allowPaging: true,
            pageSettings: {
                pageSize: memberGridpageSize
            },
            //allowSorting: true,
            allowTextWrap: true,
            textWrapSettings: {
                wrapMode: 'Content'
            },
            //queryCellInfo: customiseCell
        });

        grid1.dataBound = function () {
            // grid1.autoFitColumns(['cftp']);
            //let pkColumn = grid1.column[0];
            //pkColumn.isPrimaryKey='true';
        };
        //console.log(grid1);
        grid1.appendTo('#grid1');
        grid1.toolbarClick = function (args) {

            // console.log(grid1);
            if (args.item.id === 'CsvExport') {
                grid1.showSpinner();
                // console.log(grid1);
                grid1.columns[5].visible = false;
                let excelExportProperties = {
                    fileName: "資料時間:" + sessionStorage.getItem(defaultStartDate) + ".csv"
                };
                // grid1.excelExport(getExcelExportProperties());
                grid1.csvExport(excelExportProperties);

            }
            if (args.item.id === 'ExcelExport') {

                grid1.columns[5].visible = false;
                grid1.columns[6].visible = true;

                let excelExportProperties = {
                    fileName: "資料時間_" + sessionStorage.getItem(defaultStartDate) + ".xlsx"
                };
                // grid1.excelExport(getExcelExportProperties());
                grid1.excelExport(excelExportProperties);


            }

        };
        grid1.beforeExcelExport = () => {
            //console.log("before");

            grid1.showSpinner();
        };
        grid1.excelExportComplete = () => {
            grid1.hideSpinner();
            grid1.columns[5].visible = true;
            grid1.columns[6].visible = false;
        };
        grid1.csvExportComplete = () => {
            grid1.hideSpinner();
            grid1.columns[5].visible = true;
            grid1.columns[6].visible = false;
        };
        grid1.actionBegin = function (args) {
            console.log(args);
            switch (args.requestType) {
                case "paging":
                    if (!isLabel) {
                        let offset = (args.currentPage - 1) * memberGridpageSize;
                        // console.log(offset);
                        memberData.Offset = offset;
                        //customerData = JSON.parse(isSession(memberData).responseText);
                        // console.log(customerData);
                        grid1.dataSource = (customerData);
                    }

                    //grid1.refresh();
                    break;
            }
        };

    } catch (ex) {

    }


};
//Render initialized Accordion component

dashboard.appendTo('#editLayout');

$(document).ready(function () {
    let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
    let labelKey = "labelKey-" + dt + "-" + dt + "-" + "1";
    dp = DramaCore.createDatePicker(dt, dt);

    if (localStorage.getItem(labelKey)) {
        //listViewData = JSON.parse(localStorage.getItem(labelKey));
        customerData = JSON.parse(isSession(memberData).responseText);
        renderComponet();
    } else {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            redirect: 'follow'
        };
        customerData = JSON.parse(isSession(memberData).responseText);
        //global_Label_key = DramaCore.getRemoteData(gridUrl, "post", labelCate, labelKey, "Label");
        fetch(gridUrl, requestOptions)
            .then(response => response.text())
            .then(result => {
                customerData = JSON.parse(result);
                if (customerData == null) {
                    alert("設定日期無資料")
                    localStorage.setItem(siteKey, "0");
                } else {

                    // initFBPage.chart();
                    $(".footer").show();
                    $("#dataChart").show();
                    renderComponet();
                }
                $("body").removeClass("loading");

            })
            .catch(error => console.log('error', error));


    }



    button1 = new ej.buttons.Button({
        cssClass: 'e-warning'
    }, '#clearLabel');
    button2 = new ej.buttons.Button({
        cssClass: 'e-info'
    }, '#secondSearch');









});



function sort(args) {
    if (args.requestType === 'sorting') {
        for (var i = 0, a = grid.getColumns(); i < a.length; i++) {
            var columns = a[i];
            for (var j = 0, b = grid.sortSettings.columns; j < b.length; j++) {
                var sortcolumns = b[j];
                if (sortcolumns.field === columns.field) {
                    check(sortcolumns.field, true);
                    break;
                }
                else {
                    check(columns.field, false);
                }
            }
        }
    }
}
function isSession(data) {
    return $.ajax({
        type: "POST",
        url: gridUrl,
        data: data,
        dataType: "html",
        async: !1,
        error: function () {
            alert("資料庫維護中");
            document.location.href = localurl + loginUrl;

        }
    });
}
function getExcelExportProperties() {
    let Today = new Date();
    let exportDate = Today.getFullYear() + " 年 " + (Today.getMonth() + 1) + " 月 " + Today.getDate() + " 日";
    return {
        header: {
            headerRows: 3,
            rows: [{
                index: 1,
                cells: [{
                    index: 1,
                    colSpan: 7,
                    value: 'CACO',
                    style: {
                        fontColor: '#C25050',
                        fontSize: 25,
                        hAlign: 'Center',
                        bold: true
                    }
                }]
            },
            {
                index: 3,
                cells: [{
                    index: 1,
                    colSpan: 2,
                    value: "日期",
                    style: {
                        fontColor: '#C67878',
                        fontSize: 16,
                        bold: true
                    }
                },
                {
                    index: 4,
                    value: exportDate,
                    style: {
                        fontColor: '#C67878',
                        bold: true
                    }
                },

                ]
            },

            ]
        },



        fileName: "Member.xlsx"
    };
}

function tagsFormatter(field, data, column) {

    var tmp_Str = data.tags;
    var tmp_Str2 = '';
    var rtn_Str = '';
    try {
        if (data.tags == null) {
            data.tags = ''
        }

        var arr = tmp_Str.split(":");
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] !== "") {
                let tagcolor = "";

                switch (i % 3) {
                    case 0:
                        tagcolor = " tag-infoB";
                        break;
                    case 1:
                        tagcolor = " tag-infoY";
                        break;
                    case 2:
                        tagcolor = " tag-infoG";
                        break;

                }

                tmp_Str2 = '<span class="tagStyles  m-r-5 m-t-5' + ' ' + tagcolor + '">' + arr[i] + '</span>';
            }


            rtn_Str = tmp_Str2 + rtn_Str;
        }

    } catch (e) {
        console.log(e)
    }

    //console.log(rtn_Str)
    return rtn_Str;
}

function customiseCell(args) {
    //console.log(args.cell);
    if (args.column.field === 'duration') {
        var tmp_Duration = args.data.duration + '%';
        args.cell.querySelector("#bounce").textContent = tmp_Duration;
        if (parseInt(args.data.duration) <= refIndex1) {
            args.cell.querySelector("#bounce").classList.add("bouncestatus");
        }

    }
}
$("#secondSearch").on("click", function () {
    tagArr = window.checkList.selectedText;
    console.log('tagArr', tagArr);
    console.log('window.checkList', window.checkList.selectedText);
    if (tagArr.length > 0) {
        memberData.Clause = "";
        memberData.Offset = 0;
        memberData.Limit = 0;
        var predicate = new ej.data.Predicate('tags', 'contains', tagArr);
        console.log(predicate);

        if (tagArr.length > 1) {
            for (var i = 1; i < tagArr.length; i++) {

                predicate = predicate.or('tags', 'contains', tagArr[i]);
            }
        }
        memberData.Clause = tagArr.join('|');
        customerData = JSON.parse(isSession(memberData).responseText);
        var result = new ej.data.DataManager(customerData).executeLocal(new ej.data.Query().where(predicate));
        console.log('customerData', customerData);
        console.log('result', result);
        grid1.dataSource = result;
        grid1.allowSorting = true;
        grid1.goToPage(1);
        isLabel = true;
        memberData.Limit = memberGridpageSize;


    } else {
        memberData.Clause = "";
        memberData.Offset = 0;
        memberData.Limit = memberGridpageSize;
        isLabel = false;
        customerData = JSON.parse(isSession(memberData).responseText);
        grid1.dataSource = customerData[0];
        grid1.allowSorting = false;
    }
});
$("#clearLabel").on("click", function () {


    //var result = new ej.data.DataManager(customerData).executeLocal(new ej.data.Query());
    //grid1.dataSource = result;
    memberData.Clause = "";
    memberData.Offset = 0;
    memberData.Limit = memberGridpageSize;
    isLabel = false;
    customerData = JSON.parse(isSession(memberData).responseText);
    grid1.dataSource = customerData;
    grid1.allowSorting = false;
    window.checkList.clear();

});

function getCheckedValue() {

    let cbxTags = [];

    /*
    $('input:checkbox:checked[name="tag_active_col[]"]').each(function(i) {
         cbxTags[i] = this.value; 
        
    });*/

    for (let i = 1; i <= listViewData.length; i++) {
        if (typeof window['chklistObj' + i].value !== 'undefined' && window['chklistObj' + i].value.length > 0) {
            cbxTags = cbxTags.concat(window['chklistObj' + i].value);
            // console.log(window['chklistObj' + i].value);
        }


    }

    console.log(cbxTags);
    return cbxTags;
}

function getAccordinItems() {
    window.checkList = new ej.dropdowns.DropDownTree({
        fields: { dataSource: window.countries, value: 'Id', parentValue: 'ParentId', text: 'FullName', hasChildren: 'hasChild', },
        showCheckBox: true,
        placeholder: '選擇標籤',
        popupHeight: '650px',
        mode: 'Custom',
        width: '99%',
        treeSettings: { autoCheck: true },
        customTemplate: "${value.length} 標籤 被選中",
        select: function () {


        }
    });

    window.checkList.appendTo('#selector2');





}
function sortArray(arr) {
    var tempArr = [], n;
    for (var i in arr) {
        tempArr[i] = arr[i].match(/([^0-9]+)|([0-9]+)/g);
        for (var j in tempArr[i]) {
            if (!isNaN(n = parseInt(tempArr[i][j]))) {
                tempArr[i][j] = n;
            }
        }
    }
    tempArr.sort(function (x, y) {
        for (var i in x) {
            if (y.length < i || x[i] < y[i]) {
                return -1; // x is longer
            }
            if (x[i] > y[i]) {
                return 1;
            }
        }
        return 0;
    });
    for (var i in tempArr) {
        arr[i] = tempArr[i].join('');
    }
    return arr;
}