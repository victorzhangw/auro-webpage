DramaCore.loadCultureFiles('zh');
DramaCore.sideBar();

const chartitems = (document.getElementById('dataChart'));
const prefix = sessionStorage.getItem("sitePrefix");
const appName = sessionStorage.getItem("appName")
const ApiremoteUrl = sessionStorage.getItem("ApiremoteUrl");
var L10n = ej.base.L10n;
var dp, DataObj, Dataobj1;
const gridColumn = [
  {
    field: 'campaignName',
    width: 300,
    textAlign: 'left',
    headerText: '活動',
    format: 'N'
  },
  {
    field: 'campaignId',
    headerText: '活動Id',
    textAlign: 'left',
    width: 170,
    format: 'N',
    visible: false
  },
  {
    field: 'adsetId',
    headerText: '素材',
    textAlign: 'right',
    width: 90,
    format: 'N',
    visible: false
  },
  {
    field: 'adsetName',
    headerText: '廣告群組',
    textAlign: 'right',
    width: 90,
    format: 'N'
  },
  {
    field: 'adId',
    headerText: '廣告Id',
    textAlign: 'left',
    width: 170,
    format: 'N',
    visible: false
  },
  {
    field: 'adName',
    headerText: '廣告',
    textAlign: 'right',
    width: 90,
    format: 'N'
  },
  {
    field: 'spends',
    headerText: '花費',
    textAlign: 'right',
    width: 90,
    format: 'N'
  },
  {
    field: 'inlinelinkclicks',
    headerText: '點擊',
    textAlign: 'right',
    width: 90,
    format: 'N'
  },
  {
    field: 'impression',
    headerText: '曝光',
    textAlign: 'right',
    width: 90,
    format: 'N'
  },
  {
    field: 'reach',
    headerText: '觸及',
    textAlign: 'right',
    width: 90,
    format: 'N'
  },
  {
    field: 'inlinelinkclickctr',
    headerText: 'CTR',
    textAlign: 'right',
    width: 90,
    format: 'N'
  },
  {
    field: 'ecpc',
    headerText: 'CPC',
    textAlign: 'right',
    width: 90,
    format: 'N'
  },
  {
    field: 'ecpm',
    headerText: 'CPM',
    textAlign: 'right',
    width: 90,
    format: 'N'
  }
];

$(document).ready(function () {
  let sdt = moment().subtract(1, 'days').format("YYYY-MM-DD");
  let edt = moment().subtract(1, 'days').format("YYYY-MM-DD");
  DramaCore.validateTokenCall(validateSetting());
  let siteKey = prefix + "fb-" + sdt + "-" + edt;

  let dt = moment().subtract(1, 'days').format("YYYY-MM-DD");
  dp = DramaCore.createDatePicker(dt, dt);
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var raw = JSON.stringify({
    "StartDateTime": sdt,
    "EndDateTime": edt
  });
  initFBPage.chart();
  $(".footer").hide();
  $("#dataChart").hide();
  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
  };

  if (!localStorage.getItem(siteKey)) {

    fetch(ApiremoteUrl + "FB/FBDailyReport", requestOptions)
      .then(response => response.text())
      .then(result => {
        report = JSON.parse(result);
        if (report == null) {
          alert("本日無資料");
          localStorage.setItem(siteKey, "0");
        } else {
          localStorage.setItem(siteKey, result);
          $(".footer").show();
          $("#dataChart").show();
          //initFBPage.chart();
          DramaCore.renderFBChart(siteKey);

        }

        //spinner.setAttribute('hidden', '');

        $("body").removeClass("loading");

      })
      .catch(error => console.log('error', error));
  } else {

    let key = JSON.parse(localStorage.getItem(siteKey));

    if (key == null) {
      //alert("本日無資料");
      $(".footer").hide();
      $("#dataChart").hide();
    } else {
      $(".footer").show();
      $("#dataChart").show();
      //initFBPage.chart();
      console.log(siteKey);
      DramaCore.renderFBChart(siteKey);
    }

  }


  dp.addEventListener("change", function () {
    let dayRange = this.getSelectedRange();

    if (dayRange.daySpan > 0) {
      let _sd = moment(this.startDate).format("YYYY-MM-DD");
      let _ed = moment(this.endDate).format("YYYY-MM-DD");
      let _parsesd = moment(this.startDate).format("YYYY-MM-DD");
      let _parseed = moment(this.endDate).format("YYYY-MM-DD");

      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      var raw = JSON.stringify({
        "StartDateTime": _sd,
        "EndDateTime": _ed

      });

      var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
      };
      if (_sd && _ed) {

        if (_sd == _ed) {
          fetch(ApiremoteUrl + "FB/FBDailyReport", requestOptions)
            .then(response => response.text())
            .then(result => {
              report = JSON.parse(result);
              if (report == null) {
                alert("設定日期無資料")
                localStorage.setItem(siteKey, "0");
              } else {
                localStorage.setItem(siteKey, result);
                // initFBPage.chart();
                $(".footer").show();
                $("#dataChart").show();
                DramaCore.renderFBChart(siteKey);
              }
              $("body").removeClass("loading");

            })
            .catch(error => console.log('error', error));
        }

        if (moment(_parsesd, "YYYY-MM-DD").isAfter(_parseed, "YYYY-MM-DD")) {
          alert("起始日期要在結束日期之前!");
          //返回false
          return false;
        }
        if (moment(_parseed, "YYYY-MM-DD").isAfter(_parsesd, "YYYY-MM-DD")) {

          fetch(ApiremoteUrl + "FB/FBDailyReport", requestOptions)
            .then(response => response.text())
            .then(result => {
              report = JSON.parse(result);
              if (report == null) {

                alert("設定日期無資料")
              } else {
                siteKey = prefix + "fb-" + _parsesd + "-" + _parseed;
                localStorage.setItem(siteKey, result);
                //  initFBPage.chart();
                $(".footer").show();
                $("#dataChart").show();
                DramaCore.renderFBChart(siteKey);
              }
              $("body").removeClass("loading");

            })
            .catch(error => console.log('error', error));




        }
      }
    }
  });

});


// url --> remote url ; type:post or get ; para:parameters
var initFBPage = {
  widget: function () {



  },
  chart: function () {
    let chartMetaData = [
      { id: 'chart1', size: 'col-md-12' },
      { id: 'chart2', size: 'col-md-12' },
      { id: 'chart3', size: 'col-md-12' },
      { id: 'chart4', size: 'col-md-6' },
      { id: 'chart5', size: 'col-md-6' },
      { id: 'chart6', size: 'col-md-6' },
      { id: 'chart7', size: 'col-md-6' }


    ];
    let getChartString = ej.base.compile('<div class="col-sm-12 ${size} mb-1 d-flex"> <div id=${id} class="card card_animation flex-fill"> <div class="card-header widget-head"> </div><div class="charts card-body chart-height"> </div> </div>  </div>');
    while (chartitems.firstChild) {
      chartitems.removeChild(chartitems.firstChild);
    }
    chartMetaData.forEach(data => {
      chartitems.appendChild(getChartString(data)[0]);
    });
  },


};


// 客製化表格欄位值

var FBCharts = {

  chart1: function (data, location, rowMax, header, chartid) {
    let rst = data,
      cates = [],
      list = [],
      metric1 = [],
      metric2 = [],
      metric3 = [],
      metric4 = [],
      metric5 = [];

    var result = [];  // 存最終資料結果

    result = groupAndSum(rst, ['campaignId', 'campaignName', 'eventdate'], ['spends', 'impression', 'inlinelinkclicks', 'reach', 'cpc', 'inlinelinkclickctr']);
    for (let i = 0; i < result.length; i++) {

      result[i].ecpm = roundToTwo(Number(result[i].spends / result[i].impression));
      result[i].ecpm = roundToTwo(Number(result[i].spends / result[i].impression));
      metric1[i] = result[i].ecpm;
      cates[i] = result[i].campaignName;
      if (result[i].impression == 0) {

        result[i].ecpm = 0;
      }



    }



    let setting = {
      cardHead: header,
      titleText: "",
      value: result,
      color: "",
      allowResizing: true,
      gridColumn: gridColumn,
      chartid: chartid
    };


    let setting2 = {
      cardHead: "廣告 ECPM",
      titleText: "",

      value: metric1,

      category: cates,
      color: ["#3259B8"],
      gridColumn: gridColumn,
      chartid: "chart4"
    };
    let setting3 = {
      cardHead: "日期期間 點擊/花費 比較",
      titleText: "",

      legend: ["點擊", "花費"],
      value1: metric1,
      value2: metric3,

      category: cates,
      color: ["#4A90E2"],
      gridColumn: gridColumn,
      chartid: "chart5"
    };
    let setting4 = {
      cardHead: "日期期間 ECPM",
      titleText: "",

      value: metric4,

      category: cates,
      color: ["#3259B8"],
      gridColumn: gridColumn,
      chartid: "chart6"
    };
    let setting5 = {
      cardHead: "日期期間 曝光/花費 比較",
      titleText: "",
      legend: ["曝光", "花費"],
      value1: metric2,
      value2: metric3,
      category: cates,
      color: ["#4A90E2"],
      gridColumn: gridColumn,
      chartid: "chart7"
    };
    let chart = chartSetting.gridChart(setting);
    //let chart2 = chartSetting.doubleBarChartVertical(setting2);

    chart.appendTo('#chart1 .card-body');
    let chart2 = echarts.init(document.getElementById("chart4").getElementsByClassName('card-body')[0]);
    let chart3 = echarts.init(document.getElementById("chart5").getElementsByClassName('card-body')[0]);
    let chart4 = echarts.init(document.getElementById("chart6").getElementsByClassName('card-body')[0]);
    let chart5 = echarts.init(document.getElementById("chart7").getElementsByClassName('card-body')[0]);
    let option = chartSetting.singleBarChartwithAvgLine(setting2);
    let option2 = chartSetting.doubleBarChart(setting3);
    // let option3 = chartSetting.singleBarChartwithAvgLine(setting4);
    // let option4 = chartSetting.doubleBarChart(setting5);


    if (option && typeof option === "object") {

      chart2.setOption(option, true);
      chart3.setOption(option2, true);
      //  chart4.setOption(option3, true);
      //  chart5.setOption(option4, true);
      $("#chart4").children(".card-header").text(setting2.cardHead);
      $("#chart5").children(".card-header").text(setting3.cardHead);
      //  $("#chart6").children(".card-header").text(setting4.cardHead);
      // $("#chart7").children(".card-header").text(setting5.cardHead);

    }
    $("#" + chartid).children(".card-header").text(setting.cardHead);

  },
  chart2: function (data, location, rowMax, header, chartid) {


    let rst = data,
      chartObj = {},
      list = [],
      metric = [];

    /*
       var result = Enumerable.from(rst)
         .groupBy((x) => x.campaign_id + ':' + x.campaign + ':' + x.strategy)
         .select((x) => {
           return {
             campaign_id: x.first().campaign_id,
             campaign: x.first().campaign,
             clickrate: x.average((y) => (Number(y.clickrate)) | 0),
             ecpm: x.average((y) => (Number(y.ecpm)) | 0),
             ecpc: x.average((y) => (Number(y.ecpc)) | 0),
             spends: x.sum((y) => Number(y.spends) | 0),
             click: x.sum((y) => Number(y.click) | 0),
             impress: x.sum((y) => Number(y.impress) | 0)
           };
         })
         .toArray();*/
    let result;
    result = groupAndSum(rst, ['campaignId', 'adsetId', 'campaignName', 'adsetName'], ['spends', 'impression']);
    console.log('result', result);

    for (let i = 0; i < result.length; i++) {

      result[i].ecpm = roundToTwo((result[i].spends / result[i].impression));

      if (result[i].impression == 0) {

        result[i].ecpm = 0;
      }



    }
    let setting = {
      cardHead: header,
      titleText: "",
      value: result,
      color: "",
      gridColumn: gridColumn,
      allowResizing: true,
      chartid: chartid
    };
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart2 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  },
  chart3: function (data, location, rowMax, header, chartid) {
    let rst = data,
      chartObj = {},
      list = [],
      metric = [];

    /*
        var result = Enumerable.from(rst)
          .groupBy((x) => x.campaign_id + ':' + x.campaign + ':' + x.strategy + ':' + x.creative)
          .select((x) => {
            return {
              campaign_id: x.first().campaign_id,
              campaign: x.first().campaign,
              clickrate: x.average((y) => (Number(y.clickrate)) | 0),
              ecpm: x.average((y) => (Number(y.ecpm)) | 0),
              ecpc: x.average((y) => (Number(y.ecpc)) | 0),
              spends: x.sum((y) => Number(y.spends) | 0),
              click: x.sum((y) => Number(y.click) | 0),
              impress: x.sum((y) => Number(y.impress) | 0)
    
            };
          })
          .toArray();
    
    */
    let result;
    result = groupAndSum(rst, ['campaignId', 'adsetId', 'adId', 'campaignName', 'adsetName', 'adName'], ['spends', 'impression']);


    for (let i = 0; i < result.length; i++) {
      result[i].ecpm = roundToTwo((result[i].spends / result[i].impression));
      if (result[i].impression == 0) {

        result[i].ecpm = 0;
      }


    }
    let setting = {
      cardHead: header,
      titleText: "",
      value: result,
      color: "",
      allowResizing: true,
      gridColumn: gridColumn,
      chartid: chartid
    };
    let chart = chartSetting.gridChart(setting);
    chart.appendTo('#chart3 .card-body');
    $("#" + chartid).children(".card-header").text(setting.cardHead);
  }

};

function validateSetting() {
  let bearerToken = sessionStorage.getItem("token");
  let urlTo = AuthremoteUrl + "GetUserName";
  let Setting = {
    async: true,
    type: "GET",
    url: urlTo,
    headers: {
      Authorization: "Bearer" + " " + bearerToken

    }
  };
  return Setting;
}
function loadingScreen(responseTime) {
  var html = '<div id="load"></div>';
  $('#lodinghint').append(html);
  setTimeout(function () {
    $('#load').remove();
  }, responseTime);
}
function groupAndSum(arr, groupKeys, sumKeys) {
  return Object.values(
    arr.reduce((acc, curr) => {
      const group = groupKeys.map(k => curr[k]).join('-');
      acc[group] = acc[group] || Object.fromEntries(groupKeys.map(k => [k, curr[k]]).concat(sumKeys.map(k => [k, 0])));
      sumKeys.forEach(k => acc[group][k] += Number(curr[k]));
      return acc;
    }, {})
  );
}
function roundToTwo(num) {
  return +(Math.round(num + "e+2") + "e-2");
}